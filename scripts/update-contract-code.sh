#!/usr/bin/env bash

# Script that copies testing contracts and associated tests into the testing
# namespace.

set -e

HERE=$(pwd)
CONTRACT_BINARY_SUBDIR="$HERE/src/test/resources/com/partisiablockchain/language/realwasmbinder/contracts/"
CONTRACT_REPOS_SUBDIR="$HERE/target/example-contracts/"
BRANCH="main"

declare -a CONTRACT_REPOSITORIES=(
  "https://git@gitlab.com/partisiablockchain/language/contracts/internal.git"
  "https://git@gitlab.com/partisiablockchain/language/contracts/defi.git"
  "https://git@gitlab.com/partisiablockchain/language/example-contracts.git"
  "https://git@gitlab.com/partisiablockchain/language/contracts/zk-many-variables-test.git"
  "https://git@gitlab.com/partisiablockchain/language/contracts/testing-types.git"
)

fetch_contract() {
  mkdir -p "$CONTRACT_BINARY_SUBDIR" "$CONTRACT_REPOS_SUBDIR"

  # Fetch contracts
  pushd "$CONTRACT_REPOS_SUBDIR"
  for CONTRACT_REPO in "${CONTRACT_REPOSITORIES[@]}"; do
    git clone "${CONTRACT_REPO%%#*}" --branch "$BRANCH" --single-branch --no-tags
  done
  popd
}

# Compile contracts
compile_contracts() {
  export CARGO_NET_GIT_FETCH_WITH_CLI=true
  pushd "$CONTRACT_REPOS_SUBDIR"
  for contract_repo_dir in ./*; do
    pushd "$contract_repo_dir"

    local INNER_RUST_DIRECTORY
    INNER_RUST_DIRECTORY=''

    if cd rust; then
      INNER_RUST_DIRECTORY=true
    fi

    cargo partisia-contract build --release

    if [ "$INNER_RUST_DIRECTORY" = true ]; then
      cd ..
    fi

    popd
  done
  popd

  # Copy contracts and tests
  cp -v "$CONTRACT_REPOS_SUBDIR"/*/target/wasm32-unknown-unknown/release/*.{wasm,zkwa,abi} "$CONTRACT_BINARY_SUBDIR"
  cp -v "$CONTRACT_REPOS_SUBDIR"/*/rust/target/wasm32-unknown-unknown/release/*.{wasm,zkwa,abi} "$CONTRACT_BINARY_SUBDIR"
}

# zk-actions special variant contracts
compile_contracts_variants() {
  pushd "$CONTRACT_REPOS_SUBDIR/testing-types/rust/zk-actions"
  export CARGO_NET_GIT_FETCH_WITH_CLI=true
  cargo partisia-contract build --release --no-default-features
  popd
  cp -v "$CONTRACT_REPOS_SUBDIR"/testing-types/rust/target/wasm32-unknown-unknown/release/zk_actions.wasm "$CONTRACT_BINARY_SUBDIR/zk_actions_no_automatic.wasm"

  pushd "$CONTRACT_REPOS_SUBDIR/testing-types/rust/zk-actions"
  export CARGO_NET_GIT_FETCH_WITH_CLI=true
  cargo partisia-contract build --release --features throw_on_everything
  popd
  cp -v "$CONTRACT_REPOS_SUBDIR"/testing-types/rust/target/wasm32-unknown-unknown/release/zk_actions.wasm "$CONTRACT_BINARY_SUBDIR/zk_actions_throw_on_everything.wasm"
}

# Run
fetch_contract
compile_contracts
compile_contracts_variants
