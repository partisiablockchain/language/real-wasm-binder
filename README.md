# WASM REAL Binder

The REAL WASM contract binder allows the use of REAL WASM contracts on chain.

This binder is responsible for reacting to, and handling, actions from users or
[REAL nodes](https://gitlab.com/partisiablockchain/real/real-node).

## See also

* [real-protocol](https://gitlab.com/partisiablockchain/real/real-protocol): handles
  everything related to the concrete computations required during
  preprocessing.
* [real-node](https://gitlab.com/partisiablockchain/real/real-node): handles
  execution of the ZK contract (ZK computation).
* [real-preprocess](https://gitlab.com/partisiablockchain/real/real-preprocess):
  orchestrates preprocessing between [real-node](https://gitlab.com/partisiablockchain/real/real-node)s.

## Generating test references

If there are changes to the contract interactions, references can be regenerated by running tests with the
```shell
REGENERATE_REFERENCES=true
```
environment variable set.

If there are changes to the binder state, references can be regenerated by deleting old reference and running test 
again.