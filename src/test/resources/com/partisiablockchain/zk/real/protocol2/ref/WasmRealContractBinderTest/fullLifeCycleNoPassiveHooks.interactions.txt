Interaction reference for fullLifeCycleNoPassiveHooks

Interactions (10):
   1: create 0x00 (Fees: 800000 Zk + 3 Cpu)
   2: sendTripleBatch 0x12 (Fees: 0 Zk + 0 Cpu)
   3: sendTripleBatch 0x12 (Fees: 0 Zk + 0 Cpu)
   4: openInvocation 0x09 (Fees: 50500 Zk + 5 Cpu)
   5: sendOptimisticOutput 0x00 (Fees: 0 Zk + 0 Cpu)
   6: sendOptimisticOutput 0x00 (Fees: 0 Zk + 0 Cpu)
   7: sendOptimisticVerification 0x00 (Fees: 0 Zk + 0 Cpu)
   8: sendOptimisticVerification 0x00 (Fees: 0 Zk + 0 Cpu)
   9: openInvocation 0x09 (Fees: 0 Zk + 4 Cpu)
  10: openInvocation 0x09 (Fees: 0 Zk + 4 Cpu)
        -> 01a000000000000000000000000000000000000999 (Cost 3095 paid by contract)