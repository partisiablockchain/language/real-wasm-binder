Interaction reference for nodeRejectsPending

Interactions (5):
   1: addUnconfirmedInput 0x05 (Fees: 10640 Zk + 4 Cpu)
        -> 01a000000000000000000000000000000000000888 (Cost 58730 paid by contract)
   2: rejectInput 0x06 (Fees: 0 Zk + 0 Cpu)
        -> 01a000000000000000000000000000000000000001 (Cost 25155 paid by contract)
   3: invokeSelf 0x10 (Fees: 0 Zk + 3 Cpu)
   4: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   5: rejectInput 0x06 (Fees: 0 Zk + 0 Cpu)