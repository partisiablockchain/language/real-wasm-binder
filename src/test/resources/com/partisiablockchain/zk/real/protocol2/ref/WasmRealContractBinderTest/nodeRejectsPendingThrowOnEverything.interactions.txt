Interaction reference for nodeRejectsPendingThrowOnEverything

Interactions (4):
   1: addUnconfirmedInput 0x05 (Fees: 10640 Zk + 4 Cpu)
        -> 01a000000000000000000000000000000000000888 (Cost 58730 paid by contract)
   2: rejectInput 0x06 (Fees: 0 Zk + 0 Cpu)
        -> 01a000000000000000000000000000000000000001 (Cost 25155 paid by contract)
   3: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   4: rejectInput 0x06 (Fees: 0 Zk + 0 Cpu)