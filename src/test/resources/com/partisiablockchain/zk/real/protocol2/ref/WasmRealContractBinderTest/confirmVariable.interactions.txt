Interaction reference for confirmVariable

Interactions (5):
   1: addUnconfirmedInput 0x05 (Fees: 12560 Zk + 4 Cpu)
        -> 01a000000000000000000000000000000000000888 (Cost 58730 paid by contract)
   2: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   3: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   4: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
        -> 01a000000000000000000000000000000000000001 (Cost 25160 paid by contract)
   5: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)