Interaction reference for overlappingSimultaneousOpenings

Interactions (11):
   1: openInvocation 0x09 (Fees: 12600 Zk + 4 Cpu)
   2: openInvocation 0x09 (Fees: 12560 Zk + 4 Cpu)
   3: sendOpenVariableShares 0x01 (Fees: 0 Zk + 0 Cpu)
   4: sendOpenVariableShares 0x01 (Fees: 0 Zk + 0 Cpu)
   5: sendOpenVariableShares 0x01 (Fees: 0 Zk + 0 Cpu)
        -> 01a000000000000000000000000000000000000001 (Cost 25195 paid by contract)
   6: invokeSelf 0x0D (Fees: 0 Zk + 4 Cpu)
   7: sendOpenVariableShares 0x01 (Fees: 0 Zk + 0 Cpu)
   8: sendOpenVariableShares 0x01 (Fees: 0 Zk + 0 Cpu)
   9: sendOpenVariableShares 0x01 (Fees: 0 Zk + 0 Cpu)
        -> 01a000000000000000000000000000000000000001 (Cost 25175 paid by contract)
  10: invokeSelf 0x0D (Fees: 0 Zk + 3 Cpu)
  11: sendOpenVariableShares 0x01 (Fees: 0 Zk + 0 Cpu)