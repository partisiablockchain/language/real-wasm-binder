Interaction reference for confirmedInterleavedInputs

Interactions (8):
   1: addUnconfirmedInput 0x05 (Fees: 12560 Zk + 4 Cpu)
        -> 01a000000000000000000000000000000000000888 (Cost 58730 paid by contract)
   2: addUnconfirmedInput 0x05 (Fees: 10040 Zk + 4 Cpu)
   3: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   4: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   5: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   6: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
   7: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
        -> 01a000000000000000000000000000000000000001 (Cost 25160 paid by contract)
   8: confirmInput 0x03 (Fees: 0 Zk + 0 Cpu)
        -> 01a000000000000000000000000000000000000001 (Cost 25160 paid by contract)