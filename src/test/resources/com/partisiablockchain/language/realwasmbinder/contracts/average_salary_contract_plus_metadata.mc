(metacircuit
 (function %97
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (cast $0))
    (i32 $2 (num_variables))
    (i32 $3 (constant 1))
    (i32 $4 (add_wrapping $2 $3))
    (branch-always #1 $3 $4 $1))
  (block #1
    (inputs
      (i32 $6)
      (i32 $7)
      (sbi32 $8))
    (i1 $9 (less_than_signed $6 $7))
    (branch-if $9
      (0 #return $8)
      (1 #2 $6 $7 $8)))
  (block #2
    (inputs
      (i32 $10)
      (i32 $11)
      (sbi32 $12))
    (sbi32 $14 (load_variable $10))
    (sbi32 $15 (add_wrapping $12 $14))
    (i32 $16 (load_metadata $10))
    (sbi32 $17 (cast $16))
    (sbi32 $18 (add_wrapping $15 $17))
    (i32 $19 (constant 1))
    (i32 $20 (add_wrapping $10 $19))
    (branch-always #1 $20 $11 $18))))
