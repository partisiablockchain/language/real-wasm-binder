(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi10 $2 (constant 1))
    (sbi10 $3 (mult_wrapping_signed $2 $2))
    (sbi1 $4 (extract $3 1 0))
    (sbi2 $5 (bit_concat $4 $4))
    (sbi4 $6 (bit_concat $5 $5))
    (sbi8 $7 (bit_concat $6 $6))
    (sbi16 $8 (bit_concat $7 $7))
    (sbi32 $9 (bit_concat $8 $8))
    (branch-always #return $9))))
