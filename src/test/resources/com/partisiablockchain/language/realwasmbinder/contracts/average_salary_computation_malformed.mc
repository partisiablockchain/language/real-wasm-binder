(metacircuit
 (function %97
  (output sbi32)
  (block #0
    (i32 $0 (num_variables))
    (i32 $1 (constant 100))
    (i32 $2 (add_wrapping $0 $1))
    (i32 $3 (load_metadata $2))
    (sbi32 $4 (cast $3))
    (branch-always #return $4))
  ))
