(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (constant 0))
    (branch-always #1 $0 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $4))
    (i32 $5 (next_variable_id $3))
    (i32 $6 (constant 0))
    (i1 $7 (equal $5 $6))
    (i1 $8 (bitwise_not $7))
    (branch-if $8
      (0 #return $4)
      (1 #2 $5 $5 $4)))
  (block #2
    (inputs
      (i32 $9)
      (i32 $10)
      (sbi32 $11))
    (sbi32 $13 (load_variable $9))
    (sbi32 $15 (constant 0))
    (sbi1 $16 (equal $13 $15))
    (sbi1 $17 (bitwise_not $16))
    (sbi31 $18 (constant 0))
    (sbi32 $19 (bit_concat $18 $17))
    (sbi32 $20 (add_wrapping $11 $19))
    (branch-always #1 $10 $20))))
