(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (branch-always #1 $1))
  (block #1
    (inputs
      (i32 $2))
    (i32 $4 (next_variable_id $2))
    (i32 $5 (constant 0))
    (i1 $6 (equal $4 $5))
    (i1 $7 (bitwise_not $6))
    (branch-if $7
      (0 #return)
      (1 #2 $4 $4)))
  (block #2
    (inputs
      (i32 $8)
      (i32 $9))
    (sbi32 $12 (load_variable $8))
    (i0 $13 (output $12))
    (branch-always #1 $9))))
