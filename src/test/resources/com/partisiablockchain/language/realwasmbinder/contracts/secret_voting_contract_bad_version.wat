(module
  (type (;0;) (func (param i32)))
  (type (;1;) (func (param i32) (result i64)))
  (type (;2;) (func (param i32 i32) (result i32)))
  (type (;3;) (func (param i32 i32)))
  (type (;4;) (func (param i32 i32 i32) (result i32)))
  (type (;5;) (func (param i32 i32) (result i64)))
  (type (;6;) (func (param i32 i32 i32)))
  (type (;7;) (func (param i32 i32 i32 i32 i32)))
  (type (;8;) (func (param i32 i32 i32 i32)))
  (type (;9;) (func (param i32 i32 i32 i32) (result i32)))
  (type (;10;) (func))
  (type (;11;) (func (result i32)))
  (type (;12;) (func (param i32) (result i32)))
  (type (;13;) (func (param i32 i32 i32 i32 i32) (result i32)))
  (type (;14;) (func (param i32 i32 i32 i32 i32 i32 i32)))
  (type (;15;) (func (param i32 i32 i32 i32 i32 i32) (result i32)))
  (type (;16;) (func (param i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32) (result i32)))
  (type (;17;) (func (param i32 i32 i32 i32 i32 i32 i32) (result i32)))
  (type (;18;) (func (param i64 i32 i32) (result i32)))
  (import "pbc" "memmove" (func $pbc_lib::mem::pbc_memmove::hec21f375e382335d (type 4)))
  (import "pbc" "exit" (func $pbc_lib::exit::pbc_exit::h41aa82034cfaf2b3 (type 3)))
  (func $init (type 5) (param i32 i32) (result i64)
    (local i32 i32 i32 i32 i64 i32)
    global.get 0
    i32.const 352
    i32.sub
    local.tee 2
    global.set 0
    call $pbc_lib::exit::override_panic::hf8dda51c95006fec
    local.get 2
    local.get 1
    i32.store offset=4
    local.get 2
    local.get 0
    i32.store
    local.get 2
    i32.const 8
    i32.add
    local.get 2
    call $<pbc_contract_common::context::ContractContext_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7d4018930e3a3a34
    local.get 2
    i32.const 136
    i32.add
    local.get 2
    call $<pbc_contract_common::zk::ZkState<SecretVarMetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hf3d4c317bb0bdf2a
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 2
                i32.load offset=4
                local.tee 1
                i32.const 3
                i32.le_u
                br_if 0 (;@6;)
                local.get 2
                local.get 1
                i32.const -4
                i32.add
                i32.store offset=4
                local.get 2
                local.get 2
                i32.load
                local.tee 1
                i32.const 4
                i32.add
                i32.store
                local.get 1
                i32.load align=1
                local.set 3
                local.get 2
                i32.const 184
                i32.add
                local.get 2
                call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7022ecef7bbc5ec9
                local.get 2
                i32.load offset=4
                local.tee 4
                i32.const 3
                i32.le_u
                br_if 1 (;@5;)
                local.get 4
                i32.const -4
                i32.and
                i32.const 4
                i32.eq
                br_if 2 (;@4;)
                local.get 4
                i32.const 8
                i32.eq
                br_if 3 (;@3;)
                local.get 2
                i32.load
                local.tee 5
                i32.load align=1
                local.set 0
                local.get 5
                i32.load offset=4 align=1
                local.set 1
                local.get 2
                local.get 4
                i32.const -9
                i32.add
                local.tee 4
                i32.store offset=4
                local.get 2
                local.get 5
                i32.const 9
                i32.add
                i32.store
                block  ;; label = @7
                  local.get 4
                  br_if 0 (;@7;)
                  local.get 5
                  i32.load8_u offset=8
                  local.set 4
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 48
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 48
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 40
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 40
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 32
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 32
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 24
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 24
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 16
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 16
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 8
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 8
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  local.get 2
                  i64.load offset=8
                  i64.store offset=272
                  local.get 2
                  i64.load offset=64
                  local.set 6
                  local.get 2
                  i32.const 224
                  i32.add
                  i32.const 40
                  i32.add
                  local.get 2
                  i32.const 136
                  i32.add
                  i32.const 40
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 224
                  i32.add
                  i32.const 32
                  i32.add
                  local.get 2
                  i32.const 136
                  i32.add
                  i32.const 32
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 224
                  i32.add
                  i32.const 24
                  i32.add
                  local.get 2
                  i32.const 136
                  i32.add
                  i32.const 24
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 224
                  i32.add
                  i32.const 16
                  i32.add
                  local.get 2
                  i32.const 136
                  i32.add
                  i32.const 16
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 224
                  i32.add
                  i32.const 8
                  i32.add
                  local.get 2
                  i32.const 136
                  i32.add
                  i32.const 8
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  local.get 2
                  i64.load offset=136
                  i64.store offset=224
                  local.get 1
                  i32.eqz
                  br_if 5 (;@2;)
                  local.get 0
                  i32.const 24
                  i32.shl
                  local.get 0
                  i32.const 8
                  i32.shl
                  i32.const 16711680
                  i32.and
                  i32.or
                  local.get 0
                  i32.const 8
                  i32.shr_u
                  i32.const 65280
                  i32.and
                  local.get 0
                  i32.const 24
                  i32.shr_u
                  i32.or
                  i32.or
                  local.tee 0
                  local.get 1
                  i32.const 24
                  i32.shl
                  local.get 1
                  i32.const 8
                  i32.shl
                  i32.const 16711680
                  i32.and
                  i32.or
                  local.get 1
                  i32.const 8
                  i32.shr_u
                  i32.const 65280
                  i32.and
                  local.get 1
                  i32.const 24
                  i32.shr_u
                  i32.or
                  i32.or
                  local.tee 1
                  i32.gt_u
                  br_if 6 (;@1;)
                  local.get 2
                  i32.const 213
                  i32.add
                  local.get 2
                  i32.const 306
                  i32.add
                  i64.load align=1
                  i64.store align=1
                  local.get 2
                  i32.const 200
                  i32.add
                  i32.const 8
                  i32.add
                  local.tee 5
                  local.get 2
                  i32.const 301
                  i32.add
                  i64.load align=1
                  i64.store
                  local.get 2
                  local.get 2
                  i64.load offset=293 align=1
                  i64.store offset=200
                  local.get 2
                  i32.const 224
                  i32.add
                  call $core::ptr::drop_in_place<pbc_contract_common::zk::ZkState<secret_voting::SecretVarMetadata>>::h18297c83edefcfa2
                  local.get 2
                  i32.const 224
                  i32.add
                  call $pbc_contract_common::result_buffer::ContractResultBuffer::new::h7b510c27fb133391
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 16
                  i32.add
                  local.get 2
                  i32.const 200
                  i32.add
                  i32.const 16
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 272
                  i32.add
                  i32.const 8
                  i32.add
                  local.tee 7
                  local.get 5
                  i64.load
                  i64.store
                  local.get 2
                  i32.const 320
                  i32.add
                  local.get 2
                  i32.const 184
                  i32.add
                  i32.const 8
                  i32.add
                  i32.load
                  i32.store
                  local.get 2
                  local.get 2
                  i64.load offset=200
                  i64.store offset=272
                  local.get 2
                  local.get 2
                  i64.load offset=184
                  i64.store offset=312
                  local.get 2
                  i32.const 2
                  i32.store8 offset=344
                  local.get 2
                  local.get 4
                  i32.const 255
                  i32.and
                  i32.const 0
                  i32.ne
                  i32.store8 offset=332
                  local.get 2
                  local.get 1
                  i32.store offset=328
                  local.get 2
                  local.get 0
                  i32.store offset=324
                  local.get 2
                  local.get 6
                  local.get 3
                  i32.const 24
                  i32.shl
                  local.get 3
                  i32.const 8
                  i32.shl
                  i32.const 16711680
                  i32.and
                  i32.or
                  local.get 3
                  i32.const 8
                  i32.shr_u
                  i32.const 65280
                  i32.and
                  local.get 3
                  i32.const 24
                  i32.shr_u
                  i32.or
                  i32.or
                  i64.extend_i32_u
                  i64.add
                  local.tee 6
                  i64.store offset=296
                  local.get 2
                  local.get 6
                  i64.const 3600000
                  i64.add
                  i64.store offset=304
                  local.get 2
                  i32.const 224
                  i32.add
                  local.get 2
                  i32.const 272
                  i32.add
                  call $pbc_contract_common::result_buffer::ContractResultBuffer::write_state::h5b38c7bde86a79e9
                  local.get 7
                  local.get 2
                  i32.const 224
                  i32.add
                  i32.const 8
                  i32.add
                  i64.load
                  i64.store
                  local.get 2
                  local.get 2
                  i64.load offset=224
                  i64.store offset=272
                  local.get 2
                  i32.const 272
                  i32.add
                  call $pbc_contract_common::result_buffer::ContractResultBuffer::finalize_result_buffer::hccec9daac9270322
                  local.set 6
                  local.get 2
                  i32.const 352
                  i32.add
                  global.set 0
                  local.get 6
                  return
                end
                local.get 2
                i32.const 284
                i32.add
                i32.const 2
                i32.store
                local.get 2
                i32.const 292
                i32.add
                i32.const 1
                i32.store
                local.get 2
                i32.const 1048724
                i32.store offset=280
                local.get 2
                i32.const 0
                i32.store offset=272
                local.get 2
                i32.const 1
                i32.store offset=228
                local.get 2
                local.get 4
                i32.store offset=200
                local.get 2
                local.get 2
                i32.const 224
                i32.add
                i32.store offset=288
                local.get 2
                local.get 2
                i32.const 200
                i32.add
                i32.store offset=224
                local.get 2
                i32.const 272
                i32.add
                i32.const 1048740
                call $core::panicking::panic_fmt::h9d972fcdb087ce21
                unreachable
              end
              local.get 2
              i32.const 1050256
              i64.extend_i32_u
              i64.const 32
              i64.shl
              i64.const 2
              i64.or
              i64.store offset=272
              i32.const 1050460
              i32.const 43
              local.get 2
              i32.const 272
              i32.add
              i32.const 1050312
              i32.const 1050504
              call $core::result::unwrap_failed::h2b47cc7f7e98a508
              unreachable
            end
            local.get 2
            i32.const 1050256
            i64.extend_i32_u
            i64.const 32
            i64.shl
            i64.const 2
            i64.or
            i64.store offset=272
            i32.const 1050460
            i32.const 43
            local.get 2
            i32.const 272
            i32.add
            i32.const 1050312
            i32.const 1050504
            call $core::result::unwrap_failed::h2b47cc7f7e98a508
            unreachable
          end
          local.get 2
          i32.const 1050256
          i64.extend_i32_u
          i64.const 32
          i64.shl
          i64.const 2
          i64.or
          i64.store offset=272
          i32.const 1050460
          i32.const 43
          local.get 2
          i32.const 272
          i32.add
          i32.const 1050312
          i32.const 1050504
          call $core::result::unwrap_failed::h2b47cc7f7e98a508
          unreachable
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=272
        i32.const 1050536
        i32.const 42
        local.get 2
        i32.const 272
        i32.add
        i32.const 1050312
        i32.const 1050580
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      i32.const 1050088
      i32.const 38
      i32.const 1050144
      call $core::panicking::panic::h364c37174a08a6a4
      unreachable
    end
    i32.const 1050160
    i32.const 52
    i32.const 1050212
    call $core::panicking::panic::h364c37174a08a6a4
    unreachable)
  (func $zk_on_secret_input_40 (type 5) (param i32 i32) (result i64)
    (local i32 i32 i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 560
    i32.sub
    local.tee 2
    global.set 0
    call $pbc_lib::exit::override_panic::hf8dda51c95006fec
    local.get 2
    local.get 1
    i32.store offset=4
    local.get 2
    local.get 0
    i32.store
    local.get 2
    i32.const 8
    i32.add
    local.get 2
    call $<pbc_contract_common::context::ContractContext_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7d4018930e3a3a34
    local.get 2
    i32.const 136
    i32.add
    local.get 2
    call $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::h184f071a584d8cbb
    local.get 2
    i32.const 216
    i32.add
    local.get 2
    call $<pbc_contract_common::zk::ZkState<SecretVarMetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hf3d4c317bb0bdf2a
    block  ;; label = @1
      local.get 2
      i32.load offset=4
      local.tee 1
      br_if 0 (;@1;)
      local.get 2
      i32.const 264
      i32.add
      local.get 2
      i32.const 8
      i32.add
      i32.const 128
      call $memcpy
      drop
      local.get 2
      i32.const 392
      i32.add
      local.get 2
      i32.const 136
      i32.add
      i32.const 80
      call $memcpy
      drop
      local.get 2
      i32.const 472
      i32.add
      i32.const 40
      i32.add
      local.get 2
      i32.const 216
      i32.add
      i32.const 40
      i32.add
      i64.load
      i64.store
      local.get 2
      i32.const 472
      i32.add
      i32.const 32
      i32.add
      local.get 2
      i32.const 216
      i32.add
      i32.const 32
      i32.add
      i64.load
      i64.store
      local.get 2
      i32.const 472
      i32.add
      i32.const 24
      i32.add
      local.get 2
      i32.const 216
      i32.add
      i32.const 24
      i32.add
      i64.load
      i64.store
      local.get 2
      i32.const 472
      i32.add
      i32.const 16
      i32.add
      local.get 2
      i32.const 216
      i32.add
      i32.const 16
      i32.add
      i64.load
      i64.store
      local.get 2
      i32.const 472
      i32.add
      i32.const 8
      i32.add
      local.get 2
      i32.const 216
      i32.add
      i32.const 8
      i32.add
      i64.load
      i64.store
      local.get 2
      local.get 2
      i64.load offset=216
      i64.store offset=472
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i64.load offset=320
          local.get 2
          i64.load offset=416
          i64.ge_s
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 2
            i32.const 440
            i32.add
            i32.load
            local.tee 0
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 264
            i32.add
            i32.const 21
            i32.add
            local.set 3
            local.get 2
            i32.const 436
            i32.add
            i32.load
            local.set 1
            local.get 0
            i32.const 21
            i32.mul
            local.set 0
            local.get 2
            i32.const 286
            i32.add
            local.set 4
            local.get 2
            i32.load8_u offset=285
            local.tee 5
            i32.const 255
            i32.and
            local.set 6
            loop  ;; label = @5
              block  ;; label = @6
                local.get 1
                i32.load8_u
                local.get 6
                i32.ne
                br_if 0 (;@6;)
                local.get 1
                i32.const 1
                i32.add
                local.get 4
                i32.const 20
                call $memcmp
                i32.eqz
                br_if 4 (;@2;)
              end
              local.get 1
              i32.const 21
              i32.add
              local.set 1
              local.get 0
              i32.const -21
              i32.add
              local.tee 0
              br_if 0 (;@5;)
            end
          end
          local.get 2
          i32.const 548
          i32.add
          i32.const 1
          i32.store
          local.get 2
          i32.const 556
          i32.add
          i32.const 0
          i32.store
          local.get 2
          i32.const 1048916
          i32.store offset=544
          local.get 2
          i32.const 1048684
          i32.store offset=552
          local.get 2
          i32.const 0
          i32.store offset=536
          local.get 2
          i32.const 536
          i32.add
          i32.const 1048924
          call $core::panicking::panic_fmt::h9d972fcdb087ce21
          unreachable
        end
        local.get 2
        i32.const 520
        i32.add
        i32.const 12
        i32.add
        i32.const 2
        i32.store
        local.get 2
        i32.const 536
        i32.add
        i32.const 12
        i32.add
        i32.const 3
        i32.store
        local.get 2
        i32.const 556
        i32.add
        i32.const 2
        i32.store
        local.get 2
        i32.const 1048848
        i32.store offset=544
        local.get 2
        i32.const 0
        i32.store offset=536
        local.get 2
        local.get 2
        i32.const 320
        i32.add
        i32.store offset=528
        local.get 2
        i32.const 2
        i32.store offset=524
        local.get 2
        local.get 2
        i32.const 392
        i32.add
        i32.const 32
        i32.add
        i32.store offset=520
        local.get 2
        local.get 2
        i32.const 520
        i32.add
        i32.store offset=552
        local.get 2
        i32.const 536
        i32.add
        i32.const 1048872
        call $core::panicking::panic_fmt::h9d972fcdb087ce21
        unreachable
      end
      local.get 2
      i32.const 484
      i32.add
      i32.load
      local.set 7
      local.get 2
      i32.const 480
      i32.add
      i32.load
      local.set 8
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 496
            i32.add
            i32.load
            local.tee 1
            i32.eqz
            br_if 0 (;@4;)
            local.get 1
            i32.const 40
            i32.mul
            local.set 0
            local.get 2
            i32.const 472
            i32.add
            i32.const 20
            i32.add
            i32.load
            i32.const 5
            i32.add
            local.set 1
            local.get 5
            i32.const 255
            i32.and
            local.set 6
            loop  ;; label = @5
              block  ;; label = @6
                local.get 1
                i32.const -1
                i32.add
                i32.load8_u
                local.get 6
                i32.ne
                br_if 0 (;@6;)
                local.get 1
                local.get 4
                i32.const 20
                call $memcmp
                i32.eqz
                br_if 3 (;@3;)
              end
              local.get 1
              i32.const 40
              i32.add
              local.set 1
              local.get 0
              i32.const -40
              i32.add
              local.tee 0
              br_if 0 (;@5;)
            end
          end
          block  ;; label = @4
            local.get 7
            i32.eqz
            br_if 0 (;@4;)
            local.get 7
            i32.const 40
            i32.mul
            local.set 0
            local.get 8
            i32.const 5
            i32.add
            local.set 1
            local.get 5
            i32.const 255
            i32.and
            local.set 6
            loop  ;; label = @5
              block  ;; label = @6
                local.get 1
                i32.const -1
                i32.add
                i32.load8_u
                local.get 6
                i32.ne
                br_if 0 (;@6;)
                local.get 1
                local.get 4
                i32.const 20
                call $memcmp
                i32.eqz
                br_if 3 (;@3;)
              end
              local.get 1
              i32.const 40
              i32.add
              local.set 1
              local.get 0
              i32.const -40
              i32.add
              local.tee 0
              br_if 0 (;@5;)
            end
          end
          i32.const 4
          i32.const 4
          call $__rust_alloc
          local.tee 1
          br_if 1 (;@2;)
          i32.const 4
          i32.const 4
          call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
          unreachable
        end
        local.get 2
        i32.const 548
        i32.add
        i32.const 1
        i32.store
        local.get 2
        i32.const 556
        i32.add
        i32.const 1
        i32.store
        local.get 2
        i32.const 1049004
        i32.store offset=544
        local.get 2
        i32.const 0
        i32.store offset=536
        local.get 2
        i32.const 3
        i32.store offset=524
        local.get 2
        local.get 3
        i32.store offset=520
        local.get 2
        local.get 2
        i32.const 520
        i32.add
        i32.store offset=552
        local.get 2
        i32.const 536
        i32.add
        i32.const 1049012
        call $core::panicking::panic_fmt::h9d972fcdb087ce21
        unreachable
      end
      local.get 1
      i32.const 32
      i32.store
      local.get 2
      i32.const 472
      i32.add
      call $core::ptr::drop_in_place<pbc_contract_common::zk::ZkState<secret_voting::SecretVarMetadata>>::h18297c83edefcfa2
      local.get 2
      i32.const 392
      i32.add
      call $pbc_contract_common::result_buffer::ContractResultBuffer::new::h7b510c27fb133391
      local.get 2
      i32.const 264
      i32.add
      local.get 2
      i32.const 136
      i32.add
      i32.const 80
      call $memcpy
      drop
      local.get 2
      i32.const 392
      i32.add
      local.get 2
      i32.const 264
      i32.add
      call $pbc_contract_common::result_buffer::ContractResultBuffer::write_state::h5b38c7bde86a79e9
      local.get 2
      i32.const 0
      i32.store offset=272
      local.get 2
      i64.const 34359738368
      i64.store offset=264
      local.get 2
      i32.const 392
      i32.add
      local.get 2
      i32.const 264
      i32.add
      call $pbc_contract_common::result_buffer::ContractResultBuffer::write_events::he019054c70500fa3
      local.get 2
      i32.const 256
      i32.store16 offset=276
      local.get 2
      i32.const 1
      i32.store offset=272
      local.get 2
      local.get 1
      i32.store offset=268
      local.get 2
      i32.const 1
      i32.store offset=264
      local.get 2
      i32.const 392
      i32.add
      local.get 2
      i32.const 264
      i32.add
      call $pbc_contract_common::result_buffer::ContractResultBuffer::write_zk_input_def_result::h4b3d52bc270a47f7
      local.get 2
      i32.const 264
      i32.add
      i32.const 8
      i32.add
      local.get 2
      i32.const 392
      i32.add
      i32.const 8
      i32.add
      i64.load
      i64.store
      local.get 2
      local.get 2
      i64.load offset=392
      i64.store offset=264
      local.get 2
      i32.const 264
      i32.add
      call $pbc_contract_common::result_buffer::ContractResultBuffer::finalize_result_buffer::hccec9daac9270322
      local.set 9
      local.get 2
      i32.const 560
      i32.add
      global.set 0
      local.get 9
      return
    end
    local.get 2
    i32.const 276
    i32.add
    i32.const 2
    i32.store
    local.get 2
    i32.const 284
    i32.add
    i32.const 1
    i32.store
    local.get 2
    i32.const 1048724
    i32.store offset=272
    local.get 2
    i32.const 0
    i32.store offset=264
    local.get 2
    i32.const 1
    i32.store offset=396
    local.get 2
    local.get 1
    i32.store offset=472
    local.get 2
    local.get 2
    i32.const 392
    i32.add
    i32.store offset=280
    local.get 2
    local.get 2
    i32.const 472
    i32.add
    i32.store offset=392
    local.get 2
    i32.const 264
    i32.add
    i32.const 1048756
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $action_01 (type 5) (param i32 i32) (result i64)
    (local i32 i64)
    global.get 0
    i32.const 560
    i32.sub
    local.tee 2
    global.set 0
    call $pbc_lib::exit::override_panic::hf8dda51c95006fec
    local.get 2
    local.get 1
    i32.store offset=4
    local.get 2
    local.get 0
    i32.store
    local.get 2
    i32.const 8
    i32.add
    local.get 2
    call $<pbc_contract_common::context::ContractContext_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7d4018930e3a3a34
    local.get 2
    i32.const 136
    i32.add
    local.get 2
    call $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::h184f071a584d8cbb
    local.get 2
    i32.const 216
    i32.add
    local.get 2
    call $<pbc_contract_common::zk::ZkState<SecretVarMetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hf3d4c317bb0bdf2a
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.load offset=4
          local.tee 1
          br_if 0 (;@3;)
          local.get 2
          i32.const 264
          i32.add
          local.get 2
          i32.const 8
          i32.add
          i32.const 128
          call $memcpy
          drop
          local.get 2
          i32.const 392
          i32.add
          local.get 2
          i32.const 136
          i32.add
          i32.const 80
          call $memcpy
          drop
          local.get 2
          i32.const 472
          i32.add
          i32.const 40
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 40
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 472
          i32.add
          i32.const 32
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 32
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 472
          i32.add
          i32.const 24
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 24
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 472
          i32.add
          i32.const 16
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 16
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 472
          i32.add
          i32.const 8
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 8
          i32.add
          i64.load
          i64.store
          local.get 2
          local.get 2
          i64.load offset=216
          i64.store offset=472
          local.get 2
          i64.load offset=320
          local.get 2
          i64.load offset=424
          i64.lt_s
          br_if 1 (;@2;)
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.load8_u offset=472
              br_if 0 (;@5;)
              i32.const 28
              i32.const 4
              call $__rust_alloc
              local.tee 1
              i32.eqz
              br_if 4 (;@1;)
              i32.const 1
              i32.const 1
              call $__rust_alloc
              local.tee 0
              br_if 1 (;@4;)
              i32.const 1
              i32.const 1
              call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
              unreachable
            end
            local.get 2
            i32.const 1
            i32.store offset=556
            local.get 2
            i32.const 1
            i32.store offset=548
            local.get 2
            i32.const 1049200
            i32.store offset=544
            local.get 2
            i32.const 0
            i32.store offset=536
            local.get 2
            i32.const 4
            i32.store offset=524
            local.get 2
            local.get 2
            i32.const 520
            i32.add
            i32.store offset=552
            local.get 2
            local.get 2
            i32.const 472
            i32.add
            i32.store offset=520
            i32.const 0
            local.get 2
            i32.const 472
            i32.add
            i32.const 1048576
            local.get 2
            i32.const 536
            i32.add
            i32.const 1049208
            call $core::panicking::assert_failed::h261de8abfcde76a4
            unreachable
          end
          local.get 0
          i32.const 2
          i32.store8
          local.get 1
          i32.const 4
          i32.add
          local.get 0
          i32.const 1
          i32.add
          local.get 0
          call $<alloc::vec::Vec<T>_as_alloc::vec::spec_from_iter::SpecFromIter<T_I>>::from_iter::h246118b45cb54e1d
          local.get 1
          i32.const 24
          i32.add
          i32.const 0
          i32.store
          local.get 1
          i64.const 17179869184
          i64.store offset=16 align=4
          local.get 1
          i32.const 0
          i32.store8
          local.get 0
          i32.const 1
          i32.const 1
          call $__rust_dealloc
          local.get 2
          i32.const 472
          i32.add
          call $core::ptr::drop_in_place<pbc_contract_common::zk::ZkState<secret_voting::SecretVarMetadata>>::h18297c83edefcfa2
          local.get 2
          i32.const 392
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::new::h7b510c27fb133391
          local.get 2
          i32.const 264
          i32.add
          local.get 2
          i32.const 136
          i32.add
          i32.const 80
          call $memcpy
          drop
          local.get 2
          i32.const 392
          i32.add
          local.get 2
          i32.const 264
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::write_state::h5b38c7bde86a79e9
          local.get 2
          i32.const 0
          i32.store offset=272
          local.get 2
          i64.const 34359738368
          i64.store offset=264
          local.get 2
          i32.const 392
          i32.add
          local.get 2
          i32.const 264
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::write_events::he019054c70500fa3
          local.get 2
          i32.const 1
          i32.store offset=272
          local.get 2
          local.get 1
          i32.store offset=268
          local.get 2
          i32.const 1
          i32.store offset=264
          local.get 2
          i32.const 392
          i32.add
          local.get 2
          i32.const 264
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::write_zk_state_change::he57df4ae42921562
          local.get 2
          i32.const 264
          i32.add
          i32.const 8
          i32.add
          local.get 2
          i32.const 392
          i32.add
          i32.const 8
          i32.add
          i64.load
          i64.store
          local.get 2
          local.get 2
          i64.load offset=392
          i64.store offset=264
          local.get 2
          i32.const 264
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::finalize_result_buffer::hccec9daac9270322
          local.set 3
          local.get 2
          i32.const 560
          i32.add
          global.set 0
          local.get 3
          return
        end
        local.get 2
        i32.const 276
        i32.add
        i32.const 2
        i32.store
        local.get 2
        i32.const 284
        i32.add
        i32.const 1
        i32.store
        local.get 2
        i32.const 1048724
        i32.store offset=272
        local.get 2
        i32.const 0
        i32.store offset=264
        local.get 2
        i32.const 1
        i32.store offset=396
        local.get 2
        local.get 1
        i32.store offset=472
        local.get 2
        local.get 2
        i32.const 392
        i32.add
        i32.store offset=280
        local.get 2
        local.get 2
        i32.const 472
        i32.add
        i32.store offset=392
        local.get 2
        i32.const 264
        i32.add
        i32.const 1049028
        call $core::panicking::panic_fmt::h9d972fcdb087ce21
        unreachable
      end
      local.get 2
      i32.const 520
      i32.add
      i32.const 12
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 536
      i32.add
      i32.const 12
      i32.add
      i32.const 3
      i32.store
      local.get 2
      i32.const 556
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 1049104
      i32.store offset=544
      local.get 2
      i32.const 0
      i32.store offset=536
      local.get 2
      local.get 2
      i32.const 320
      i32.add
      i32.store offset=528
      local.get 2
      i32.const 2
      i32.store offset=524
      local.get 2
      local.get 2
      i32.const 392
      i32.add
      i32.const 32
      i32.add
      i32.store offset=520
      local.get 2
      local.get 2
      i32.const 520
      i32.add
      i32.store offset=552
      local.get 2
      i32.const 536
      i32.add
      i32.const 1049128
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    i32.const 28
    i32.const 4
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $zk_on_compute_complete (type 5) (param i32 i32) (result i64)
    (local i32 i64)
    global.get 0
    i32.const 384
    i32.sub
    local.tee 2
    global.set 0
    call $pbc_lib::exit::override_panic::hf8dda51c95006fec
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    i32.const 16
    i32.add
    local.get 2
    i32.const 8
    i32.add
    call $<pbc_contract_common::context::ContractContext_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7d4018930e3a3a34
    local.get 2
    i32.const 144
    i32.add
    local.get 2
    i32.const 8
    i32.add
    call $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::h184f071a584d8cbb
    local.get 2
    i32.const 224
    i32.add
    local.get 2
    i32.const 8
    i32.add
    call $<pbc_contract_common::zk::ZkState<SecretVarMetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hf3d4c317bb0bdf2a
    local.get 2
    i32.const 272
    i32.add
    local.get 2
    i32.const 8
    i32.add
    call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h496587b821992e5f
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.load offset=12
        local.tee 1
        br_if 0 (;@2;)
        local.get 2
        i32.const 304
        i32.add
        i32.const 40
        i32.add
        local.get 2
        i32.const 224
        i32.add
        i32.const 40
        i32.add
        i64.load
        i64.store
        local.get 2
        i32.const 304
        i32.add
        i32.const 32
        i32.add
        local.get 2
        i32.const 224
        i32.add
        i32.const 32
        i32.add
        i64.load
        i64.store
        local.get 2
        i32.const 304
        i32.add
        i32.const 24
        i32.add
        local.get 2
        i32.const 224
        i32.add
        i32.const 24
        i32.add
        i64.load
        i64.store
        local.get 2
        i32.const 304
        i32.add
        i32.const 16
        i32.add
        local.get 2
        i32.const 224
        i32.add
        i32.const 16
        i32.add
        i64.load
        i64.store
        local.get 2
        i32.const 304
        i32.add
        i32.const 8
        i32.add
        local.tee 0
        local.get 2
        i32.const 224
        i32.add
        i32.const 8
        i32.add
        i64.load
        i64.store
        local.get 2
        local.get 2
        i64.load offset=224
        i64.store offset=304
        i32.const 28
        i32.const 4
        call $__rust_alloc
        local.tee 1
        i32.eqz
        br_if 1 (;@1;)
        local.get 2
        i32.const 299
        i32.add
        local.get 2
        i32.const 272
        i32.add
        i32.const 8
        i32.add
        i32.load
        i32.store align=1
        local.get 1
        i32.const 4
        i32.store8
        local.get 2
        local.get 2
        i64.load offset=272
        i64.store offset=291 align=1
        local.get 1
        local.get 2
        i64.load offset=288 align=1
        i64.store offset=1 align=1
        local.get 1
        i32.const 8
        i32.add
        local.get 2
        i32.const 295
        i32.add
        i64.load align=1
        i64.store align=1
        local.get 2
        i32.const 304
        i32.add
        call $core::ptr::drop_in_place<pbc_contract_common::zk::ZkState<secret_voting::SecretVarMetadata>>::h18297c83edefcfa2
        local.get 2
        i32.const 288
        i32.add
        call $pbc_contract_common::result_buffer::ContractResultBuffer::new::h7b510c27fb133391
        local.get 2
        i32.const 304
        i32.add
        local.get 2
        i32.const 144
        i32.add
        i32.const 80
        call $memcpy
        drop
        local.get 2
        i32.const 288
        i32.add
        local.get 2
        i32.const 304
        i32.add
        call $pbc_contract_common::result_buffer::ContractResultBuffer::write_state::h5b38c7bde86a79e9
        local.get 2
        i32.const 0
        i32.store offset=312
        local.get 2
        i64.const 34359738368
        i64.store offset=304
        local.get 2
        i32.const 288
        i32.add
        local.get 2
        i32.const 304
        i32.add
        call $pbc_contract_common::result_buffer::ContractResultBuffer::write_events::he019054c70500fa3
        local.get 2
        i32.const 1
        i32.store offset=312
        local.get 2
        local.get 1
        i32.store offset=308
        local.get 2
        i32.const 1
        i32.store offset=304
        local.get 2
        i32.const 288
        i32.add
        local.get 2
        i32.const 304
        i32.add
        call $pbc_contract_common::result_buffer::ContractResultBuffer::write_zk_state_change::he57df4ae42921562
        local.get 0
        local.get 2
        i32.const 288
        i32.add
        i32.const 8
        i32.add
        i64.load
        i64.store
        local.get 2
        local.get 2
        i64.load offset=288
        i64.store offset=304
        local.get 2
        i32.const 304
        i32.add
        call $pbc_contract_common::result_buffer::ContractResultBuffer::finalize_result_buffer::hccec9daac9270322
        local.set 3
        local.get 2
        i32.const 384
        i32.add
        global.set 0
        local.get 3
        return
      end
      local.get 2
      i32.const 316
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 324
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 1048724
      i32.store offset=312
      local.get 2
      i32.const 0
      i32.store offset=304
      local.get 2
      i32.const 1
      i32.store offset=292
      local.get 2
      local.get 1
      i32.store offset=284
      local.get 2
      local.get 2
      i32.const 288
      i32.add
      i32.store offset=320
      local.get 2
      local.get 2
      i32.const 284
      i32.add
      i32.store offset=288
      local.get 2
      i32.const 304
      i32.add
      i32.const 1049224
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    i32.const 28
    i32.const 4
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $zk_on_variables_opened (type 5) (param i32 i32) (result i64)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 416
    i32.sub
    local.tee 2
    global.set 0
    call $pbc_lib::exit::override_panic::hf8dda51c95006fec
    local.get 2
    local.get 1
    i32.store offset=4
    local.get 2
    local.get 0
    i32.store
    local.get 2
    i32.const 8
    i32.add
    local.get 2
    call $<pbc_contract_common::context::ContractContext_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7d4018930e3a3a34
    local.get 2
    i32.const 136
    i32.add
    local.get 2
    call $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::h184f071a584d8cbb
    local.get 2
    i32.const 216
    i32.add
    local.get 2
    call $<pbc_contract_common::zk::ZkState<SecretVarMetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hf3d4c317bb0bdf2a
    local.get 2
    i32.const 264
    i32.add
    local.get 2
    call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h496587b821992e5f
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 2
                i32.load offset=4
                local.tee 1
                br_if 0 (;@6;)
                local.get 2
                i32.load8_u offset=196
                local.set 3
                local.get 2
                i32.load offset=192
                local.set 4
                local.get 2
                i32.load offset=188
                local.set 5
                local.get 2
                i32.load offset=184
                local.set 6
                local.get 2
                i32.const 280
                i32.add
                i32.const 40
                i32.add
                local.get 2
                i32.const 216
                i32.add
                i32.const 40
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 32
                i32.add
                local.get 2
                i32.const 216
                i32.add
                i32.const 32
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 24
                i32.add
                local.tee 0
                local.get 2
                i32.const 216
                i32.add
                i32.const 24
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 16
                i32.add
                local.get 2
                i32.const 216
                i32.add
                i32.const 16
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 8
                i32.add
                local.get 2
                i32.const 216
                i32.add
                i32.const 8
                i32.add
                i64.load
                i64.store
                local.get 2
                local.get 2
                i64.load offset=216
                i64.store offset=280
                local.get 2
                i32.load offset=268
                local.set 7
                local.get 2
                i32.load offset=264
                local.set 8
                local.get 2
                local.get 2
                i32.load offset=272
                local.tee 1
                i32.store offset=400
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 1
                    i32.const 1
                    i32.ne
                    br_if 0 (;@8;)
                    local.get 0
                    i32.load
                    local.tee 9
                    i32.const 40
                    i32.mul
                    local.set 10
                    local.get 7
                    i32.load
                    local.set 11
                    local.get 2
                    i32.const 300
                    i32.add
                    i32.load
                    local.set 0
                    i32.const 0
                    local.set 1
                    loop  ;; label = @9
                      local.get 10
                      local.get 1
                      i32.eq
                      br_if 4 (;@5;)
                      local.get 0
                      local.get 1
                      i32.add
                      local.set 12
                      local.get 1
                      i32.const 40
                      i32.add
                      local.set 1
                      local.get 12
                      i32.load
                      local.get 11
                      i32.ne
                      br_if 0 (;@9;)
                    end
                    local.get 0
                    local.get 1
                    i32.add
                    local.tee 1
                    i32.const -8
                    i32.add
                    i32.load
                    local.tee 12
                    i32.eqz
                    br_if 4 (;@4;)
                    local.get 1
                    i32.const -4
                    i32.add
                    i32.load
                    local.tee 1
                    i32.const 4
                    i32.ne
                    br_if 5 (;@3;)
                    local.get 12
                    i32.load align=1
                    local.set 10
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 9
                        br_if 0 (;@10;)
                        i32.const 0
                        local.set 1
                        br 1 (;@9;)
                      end
                      local.get 9
                      i32.const 40
                      i32.mul
                      i32.const -40
                      i32.add
                      local.tee 1
                      i32.const 40
                      i32.div_u
                      i32.const 1
                      i32.add
                      local.tee 12
                      i32.const 3
                      i32.and
                      local.set 11
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 1
                          i32.const 120
                          i32.ge_u
                          br_if 0 (;@11;)
                          i32.const 0
                          local.set 1
                          br 1 (;@10;)
                        end
                        local.get 12
                        i32.const 268435452
                        i32.and
                        local.set 12
                        i32.const 0
                        local.set 1
                        loop  ;; label = @11
                          local.get 1
                          local.get 0
                          i32.const 26
                          i32.add
                          i32.load8_u
                          i32.const 1
                          i32.eq
                          i32.add
                          local.get 0
                          i32.const 66
                          i32.add
                          i32.load8_u
                          i32.const 1
                          i32.eq
                          i32.add
                          local.get 0
                          i32.const 106
                          i32.add
                          i32.load8_u
                          i32.const 1
                          i32.eq
                          i32.add
                          local.get 0
                          i32.const 146
                          i32.add
                          i32.load8_u
                          i32.const 1
                          i32.eq
                          i32.add
                          local.set 1
                          local.get 0
                          i32.const 160
                          i32.add
                          local.set 0
                          local.get 12
                          i32.const -4
                          i32.add
                          local.tee 12
                          br_if 0 (;@11;)
                        end
                      end
                      local.get 11
                      i32.eqz
                      br_if 0 (;@9;)
                      local.get 11
                      i32.const 40
                      i32.mul
                      local.set 12
                      local.get 0
                      i32.const 26
                      i32.add
                      local.set 0
                      loop  ;; label = @10
                        local.get 1
                        local.get 0
                        i32.load8_u
                        i32.const 1
                        i32.eq
                        i32.add
                        local.set 1
                        local.get 0
                        i32.const 40
                        i32.add
                        local.set 0
                        local.get 12
                        i32.const -40
                        i32.add
                        local.tee 12
                        br_if 0 (;@10;)
                      end
                    end
                    local.get 6
                    local.get 1
                    local.get 3
                    i32.const 255
                    i32.and
                    select
                    local.tee 12
                    i32.eqz
                    br_if 6 (;@2;)
                    local.get 10
                    local.get 12
                    i32.gt_u
                    br_if 7 (;@1;)
                    i32.const 28
                    i32.const 4
                    call $__rust_alloc
                    local.tee 0
                    br_if 1 (;@7;)
                    i32.const 28
                    i32.const 4
                    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
                    unreachable
                  end
                  local.get 2
                  i32.const 0
                  i32.store offset=380
                  local.get 2
                  i32.const 1048684
                  i32.store offset=376
                  local.get 2
                  i32.const 1
                  i32.store offset=372
                  local.get 2
                  i32.const 1049300
                  i32.store offset=368
                  local.get 2
                  i32.const 0
                  i32.store offset=360
                  i32.const 0
                  local.get 2
                  i32.const 400
                  i32.add
                  i32.const 1049256
                  local.get 2
                  i32.const 360
                  i32.add
                  i32.const 1049308
                  call $core::panicking::assert_failed::h30c818d63c3cac54
                  unreachable
                end
                local.get 2
                local.get 10
                local.get 4
                i32.mul
                local.get 12
                local.get 5
                i32.mul
                i32.gt_u
                local.tee 12
                i32.store8 offset=408
                local.get 2
                local.get 1
                local.get 10
                i32.sub
                local.tee 1
                i32.store offset=404
                local.get 2
                local.get 10
                i32.store offset=400
                local.get 2
                i32.const 384
                i32.add
                local.get 2
                i32.const 400
                i32.add
                call $secret_voting::serialize::h6e00611e57e3db14
                local.get 2
                i32.const 371
                i32.add
                local.get 2
                i32.const 384
                i32.add
                i32.const 8
                i32.add
                i32.load
                i32.store align=1
                local.get 2
                local.get 2
                i64.load offset=384
                i64.store offset=363 align=1
                local.get 0
                i32.const 7
                i32.store8
                local.get 0
                local.get 2
                i64.load offset=360 align=1
                i64.store offset=1 align=1
                local.get 0
                i32.const 8
                i32.add
                local.get 2
                i32.const 360
                i32.add
                i32.const 7
                i32.add
                i64.load align=1
                i64.store align=1
                block  ;; label = @7
                  local.get 8
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 7
                  local.get 8
                  i32.const 2
                  i32.shl
                  i32.const 4
                  call $__rust_dealloc
                end
                local.get 2
                i32.const 280
                i32.add
                call $core::ptr::drop_in_place<pbc_contract_common::zk::ZkState<secret_voting::SecretVarMetadata>>::h18297c83edefcfa2
                local.get 2
                i32.const 360
                i32.add
                call $pbc_contract_common::result_buffer::ContractResultBuffer::new::h7b510c27fb133391
                local.get 2
                i32.const 280
                i32.add
                i32.const 40
                i32.add
                local.get 2
                i32.const 136
                i32.add
                i32.const 40
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 32
                i32.add
                local.get 2
                i32.const 136
                i32.add
                i32.const 32
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 24
                i32.add
                local.get 2
                i32.const 136
                i32.add
                i32.const 24
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 16
                i32.add
                local.get 2
                i32.const 136
                i32.add
                i32.const 16
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 8
                i32.add
                local.tee 11
                local.get 2
                i32.const 136
                i32.add
                i32.const 8
                i32.add
                i64.load
                i64.store
                local.get 2
                i32.const 280
                i32.add
                i32.const 63
                i32.add
                local.get 2
                i32.const 136
                i32.add
                i32.const 63
                i32.add
                i32.load8_u
                i32.store8
                local.get 2
                local.get 2
                i64.load offset=136
                i64.store offset=280
                local.get 2
                local.get 3
                i32.store8 offset=340
                local.get 2
                local.get 4
                i32.store offset=336
                local.get 2
                local.get 5
                i32.store offset=332
                local.get 2
                local.get 6
                i32.store offset=328
                local.get 2
                local.get 2
                i32.load16_u offset=197 align=1
                i32.store16 offset=341 align=1
                local.get 2
                i32.const 280
                i32.add
                i32.const 76
                i32.add
                local.get 2
                i32.const 136
                i32.add
                i32.const 76
                i32.add
                i32.load align=1
                i32.store align=1
                local.get 2
                local.get 12
                i32.store8 offset=352
                local.get 2
                local.get 1
                i32.store offset=348
                local.get 2
                local.get 10
                i32.store offset=344
                local.get 2
                local.get 2
                i32.load offset=209 align=1
                i32.store offset=353 align=1
                local.get 2
                i32.const 360
                i32.add
                local.get 2
                i32.const 280
                i32.add
                call $pbc_contract_common::result_buffer::ContractResultBuffer::write_state::h5b38c7bde86a79e9
                local.get 2
                i32.const 0
                i32.store offset=288
                local.get 2
                i64.const 34359738368
                i64.store offset=280
                local.get 2
                i32.const 360
                i32.add
                local.get 2
                i32.const 280
                i32.add
                call $pbc_contract_common::result_buffer::ContractResultBuffer::write_events::he019054c70500fa3
                local.get 2
                i32.const 1
                i32.store offset=288
                local.get 2
                local.get 0
                i32.store offset=284
                local.get 2
                i32.const 1
                i32.store offset=280
                local.get 2
                i32.const 360
                i32.add
                local.get 2
                i32.const 280
                i32.add
                call $pbc_contract_common::result_buffer::ContractResultBuffer::write_zk_state_change::he57df4ae42921562
                local.get 11
                local.get 2
                i32.const 360
                i32.add
                i32.const 8
                i32.add
                i64.load
                i64.store
                local.get 2
                local.get 2
                i64.load offset=360
                i64.store offset=280
                local.get 2
                i32.const 280
                i32.add
                call $pbc_contract_common::result_buffer::ContractResultBuffer::finalize_result_buffer::hccec9daac9270322
                local.set 13
                local.get 2
                i32.const 416
                i32.add
                global.set 0
                local.get 13
                return
              end
              local.get 2
              i32.const 292
              i32.add
              i32.const 2
              i32.store
              local.get 2
              i32.const 300
              i32.add
              i32.const 1
              i32.store
              local.get 2
              i32.const 1048724
              i32.store offset=288
              local.get 2
              i32.const 0
              i32.store offset=280
              local.get 2
              i32.const 1
              i32.store offset=364
              local.get 2
              local.get 1
              i32.store offset=400
              local.get 2
              local.get 2
              i32.const 360
              i32.add
              i32.store offset=296
              local.get 2
              local.get 2
              i32.const 400
              i32.add
              i32.store offset=360
              local.get 2
              i32.const 280
              i32.add
              i32.const 1049240
              call $core::panicking::panic_fmt::h9d972fcdb087ce21
              unreachable
            end
            i32.const 1048577
            i32.const 43
            i32.const 1048636
            call $core::panicking::panic::h364c37174a08a6a4
            unreachable
          end
          i32.const 1048577
          i32.const 43
          i32.const 1048652
          call $core::panicking::panic::h364c37174a08a6a4
          unreachable
        end
        i32.const 4
        local.get 1
        i32.const 1048668
        call $core::slice::<impl__T_>::copy_from_slice::len_mismatch_fail::hf6b61e468635f962
        unreachable
      end
      i32.const 1050088
      i32.const 38
      i32.const 1050144
      call $core::panicking::panic::h364c37174a08a6a4
      unreachable
    end
    i32.const 1050160
    i32.const 52
    i32.const 1050212
    call $core::panicking::panic::h364c37174a08a6a4
    unreachable)
  (func $zk_on_attestation_complete (type 5) (param i32 i32) (result i64)
    (local i32 i64)
    global.get 0
    i32.const 368
    i32.sub
    local.tee 2
    global.set 0
    call $pbc_lib::exit::override_panic::hf8dda51c95006fec
    local.get 2
    local.get 1
    i32.store offset=4
    local.get 2
    local.get 0
    i32.store
    local.get 2
    i32.const 8
    i32.add
    local.get 2
    call $<pbc_contract_common::context::ContractContext_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7d4018930e3a3a34
    local.get 2
    i32.const 136
    i32.add
    local.get 2
    call $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::h184f071a584d8cbb
    local.get 2
    i32.const 216
    i32.add
    local.get 2
    call $<pbc_contract_common::zk::ZkState<SecretVarMetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hf3d4c317bb0bdf2a
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.load offset=4
        local.tee 1
        i32.const 3
        i32.le_u
        br_if 0 (;@2;)
        local.get 2
        local.get 1
        i32.const -4
        i32.add
        local.tee 1
        i32.store offset=4
        local.get 2
        local.get 2
        i32.load
        i32.const 4
        i32.add
        i32.store
        block  ;; label = @3
          local.get 1
          br_if 0 (;@3;)
          local.get 2
          i32.const 288
          i32.add
          i32.const 40
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 40
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 288
          i32.add
          i32.const 32
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 32
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 288
          i32.add
          i32.const 24
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 24
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 288
          i32.add
          i32.const 16
          i32.add
          local.get 2
          i32.const 216
          i32.add
          i32.const 16
          i32.add
          i64.load
          i64.store
          local.get 2
          i32.const 288
          i32.add
          i32.const 8
          i32.add
          local.tee 0
          local.get 2
          i32.const 216
          i32.add
          i32.const 8
          i32.add
          i64.load
          i64.store
          local.get 2
          local.get 2
          i64.load offset=216
          i64.store offset=288
          i32.const 28
          i32.const 4
          call $__rust_alloc
          local.tee 1
          i32.eqz
          br_if 2 (;@1;)
          local.get 1
          i32.const 6
          i32.store8
          local.get 2
          i32.const 288
          i32.add
          call $core::ptr::drop_in_place<pbc_contract_common::zk::ZkState<secret_voting::SecretVarMetadata>>::h18297c83edefcfa2
          local.get 2
          i32.const 272
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::new::h7b510c27fb133391
          local.get 2
          i32.const 288
          i32.add
          local.get 2
          i32.const 136
          i32.add
          i32.const 80
          call $memcpy
          drop
          local.get 2
          i32.const 272
          i32.add
          local.get 2
          i32.const 288
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::write_state::h5b38c7bde86a79e9
          local.get 2
          i32.const 0
          i32.store offset=296
          local.get 2
          i64.const 34359738368
          i64.store offset=288
          local.get 2
          i32.const 272
          i32.add
          local.get 2
          i32.const 288
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::write_events::he019054c70500fa3
          local.get 2
          i32.const 1
          i32.store offset=296
          local.get 2
          local.get 1
          i32.store offset=292
          local.get 2
          i32.const 1
          i32.store offset=288
          local.get 2
          i32.const 272
          i32.add
          local.get 2
          i32.const 288
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::write_zk_state_change::he57df4ae42921562
          local.get 0
          local.get 2
          i32.const 272
          i32.add
          i32.const 8
          i32.add
          i64.load
          i64.store
          local.get 2
          local.get 2
          i64.load offset=272
          i64.store offset=288
          local.get 2
          i32.const 288
          i32.add
          call $pbc_contract_common::result_buffer::ContractResultBuffer::finalize_result_buffer::hccec9daac9270322
          local.set 3
          local.get 2
          i32.const 368
          i32.add
          global.set 0
          local.get 3
          return
        end
        local.get 2
        i32.const 300
        i32.add
        i32.const 2
        i32.store
        local.get 2
        i32.const 308
        i32.add
        i32.const 1
        i32.store
        local.get 2
        i32.const 1048724
        i32.store offset=296
        local.get 2
        i32.const 0
        i32.store offset=288
        local.get 2
        i32.const 1
        i32.store offset=276
        local.get 2
        local.get 1
        i32.store offset=268
        local.get 2
        local.get 2
        i32.const 272
        i32.add
        i32.store offset=304
        local.get 2
        local.get 2
        i32.const 268
        i32.add
        i32.store offset=272
        local.get 2
        i32.const 288
        i32.add
        i32.const 1049324
        call $core::panicking::panic_fmt::h9d972fcdb087ce21
        unreachable
      end
      local.get 2
      i32.const 1050256
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=288
      i32.const 1050460
      i32.const 43
      local.get 2
      i32.const 288
      i32.add
      i32.const 1050312
      i32.const 1050504
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    i32.const 28
    i32.const 4
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $<pbc_contract_common::context::ContractContext_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7d4018930e3a3a34 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i64 i32 i64 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    local.get 1
    call $<pbc_contract_common::address_internal::Address_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h9cee193dfccabb0c
    local.get 0
    i32.const 21
    i32.add
    local.get 1
    call $<pbc_contract_common::address_internal::Address_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h9cee193dfccabb0c
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.load offset=4
            local.tee 3
            i32.const 7
            i32.le_u
            br_if 0 (;@4;)
            local.get 1
            local.get 3
            i32.const -8
            i32.add
            local.tee 4
            i32.store offset=4
            local.get 1
            local.get 1
            i32.load
            local.tee 5
            i32.const 8
            i32.add
            i32.store
            local.get 4
            i32.const 7
            i32.le_u
            br_if 1 (;@3;)
            local.get 5
            i64.load align=1
            local.set 6
            local.get 1
            local.get 3
            i32.const -16
            i32.add
            local.tee 7
            i32.store offset=4
            local.get 1
            local.get 5
            i32.const 16
            i32.add
            local.tee 4
            i32.store
            local.get 7
            i32.const 31
            i32.le_u
            br_if 2 (;@2;)
            local.get 5
            i64.load offset=8 align=1
            local.set 8
            local.get 0
            local.get 4
            i64.load align=1
            i64.store offset=64 align=1
            local.get 1
            local.get 3
            i32.const -48
            i32.add
            local.tee 9
            i32.store offset=4
            local.get 1
            local.get 5
            i32.const 48
            i32.add
            local.tee 7
            i32.store
            local.get 0
            i32.const 88
            i32.add
            local.get 4
            i32.const 24
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 0
            i32.const 80
            i32.add
            local.get 4
            i32.const 16
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 0
            i32.const 72
            i32.add
            local.get 4
            i32.const 8
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 9
            i32.const 31
            i32.le_u
            br_if 3 (;@1;)
            local.get 0
            local.get 7
            i64.load align=1
            i64.store offset=96 align=1
            local.get 1
            local.get 3
            i32.const -80
            i32.add
            i32.store offset=4
            local.get 1
            local.get 5
            i32.const 80
            i32.add
            i32.store
            local.get 0
            i32.const 120
            i32.add
            local.get 7
            i32.const 24
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 0
            i32.const 112
            i32.add
            local.get 7
            i32.const 16
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 0
            i32.const 104
            i32.add
            local.get 7
            i32.const 8
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 0
            local.get 8
            i64.const 56
            i64.shl
            local.get 8
            i64.const 40
            i64.shl
            i64.const 71776119061217280
            i64.and
            i64.or
            local.get 8
            i64.const 24
            i64.shl
            i64.const 280375465082880
            i64.and
            local.get 8
            i64.const 8
            i64.shl
            i64.const 1095216660480
            i64.and
            i64.or
            i64.or
            local.get 8
            i64.const 8
            i64.shr_u
            i64.const 4278190080
            i64.and
            local.get 8
            i64.const 24
            i64.shr_u
            i64.const 16711680
            i64.and
            i64.or
            local.get 8
            i64.const 40
            i64.shr_u
            i64.const 65280
            i64.and
            local.get 8
            i64.const 56
            i64.shr_u
            i64.or
            i64.or
            i64.or
            i64.store offset=56
            local.get 0
            local.get 6
            i64.const 56
            i64.shl
            local.get 6
            i64.const 40
            i64.shl
            i64.const 71776119061217280
            i64.and
            i64.or
            local.get 6
            i64.const 24
            i64.shl
            i64.const 280375465082880
            i64.and
            local.get 6
            i64.const 8
            i64.shl
            i64.const 1095216660480
            i64.and
            i64.or
            i64.or
            local.get 6
            i64.const 8
            i64.shr_u
            i64.const 4278190080
            i64.and
            local.get 6
            i64.const 24
            i64.shr_u
            i64.const 16711680
            i64.and
            i64.or
            local.get 6
            i64.const 40
            i64.shr_u
            i64.const 65280
            i64.and
            local.get 6
            i64.const 56
            i64.shr_u
            i64.or
            i64.or
            i64.or
            i64.store offset=48
            local.get 2
            i32.const 16
            i32.add
            global.set 0
            return
          end
          local.get 2
          i32.const 1050256
          i64.extend_i32_u
          i64.const 32
          i64.shl
          i64.const 2
          i64.or
          i64.store offset=8
          i32.const 1050268
          i32.const 43
          local.get 2
          i32.const 8
          i32.add
          i32.const 1050312
          i32.const 1050428
          call $core::result::unwrap_failed::h2b47cc7f7e98a508
          unreachable
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=8
        i32.const 1050268
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050312
        i32.const 1050428
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 2
      i32.const 1051544
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=8
      i32.const 1051556
      i32.const 43
      local.get 2
      i32.const 8
      i32.add
      i32.const 1051600
      i32.const 1051724
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 2
    i32.const 1051544
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=8
    i32.const 1051556
    i32.const 43
    local.get 2
    i32.const 8
    i32.add
    i32.const 1051600
    i32.const 1051724
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<pbc_contract_common::zk::ZkClosed<MetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hc52b7da05bccc452 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.load offset=4
              local.tee 3
              i32.const 3
              i32.le_u
              br_if 0 (;@5;)
              local.get 1
              local.get 3
              i32.const -4
              i32.add
              i32.store offset=4
              local.get 1
              local.get 1
              i32.load
              local.tee 3
              i32.const 4
              i32.add
              i32.store
              local.get 3
              i32.load align=1
              local.set 4
              local.get 0
              i32.const 4
              i32.add
              local.get 1
              call $<pbc_contract_common::address_internal::Address_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h9cee193dfccabb0c
              local.get 1
              i32.load offset=4
              local.tee 5
              i32.eqz
              br_if 1 (;@4;)
              local.get 1
              local.get 5
              i32.const -1
              i32.add
              local.tee 6
              i32.store offset=4
              local.get 1
              local.get 1
              i32.load
              local.tee 3
              i32.const 1
              i32.add
              i32.store
              local.get 6
              i32.eqz
              br_if 2 (;@3;)
              local.get 3
              i32.load8_u
              local.set 7
              local.get 1
              local.get 5
              i32.const -2
              i32.add
              local.tee 6
              i32.store offset=4
              local.get 1
              local.get 3
              i32.const 2
              i32.add
              i32.store
              local.get 3
              i32.load8_u offset=1
              local.tee 8
              i32.const -1
              i32.add
              i32.const 255
              i32.and
              i32.const 2
              i32.ge_u
              br_if 3 (;@2;)
              local.get 6
              i32.eqz
              br_if 4 (;@1;)
              local.get 1
              local.get 5
              i32.const -3
              i32.add
              i32.store offset=4
              local.get 1
              local.get 3
              i32.const 3
              i32.add
              i32.store
              block  ;; label = @6
                block  ;; label = @7
                  local.get 3
                  i32.load8_u offset=2
                  br_if 0 (;@7;)
                  local.get 0
                  i32.const 32
                  i32.add
                  i32.const 0
                  i32.store
                  br 1 (;@6;)
                end
                local.get 0
                i32.const 28
                i32.add
                local.get 1
                call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h1dd5eb2af10ed63f
              end
              local.get 0
              local.get 8
              i32.store8 offset=26
              local.get 0
              local.get 7
              i32.const 255
              i32.and
              i32.const 0
              i32.ne
              i32.store8 offset=25
              local.get 0
              local.get 4
              i32.const 24
              i32.shl
              local.get 4
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 4
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 4
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              i32.store
              local.get 2
              i32.const 48
              i32.add
              global.set 0
              return
            end
            local.get 2
            i32.const 1050256
            i64.extend_i32_u
            i64.const 32
            i64.shl
            i64.const 2
            i64.or
            i64.store offset=16
            i32.const 1050460
            i32.const 43
            local.get 2
            i32.const 16
            i32.add
            i32.const 1050312
            i32.const 1050504
            call $core::result::unwrap_failed::h2b47cc7f7e98a508
            unreachable
          end
          local.get 2
          i32.const 1050256
          i64.extend_i32_u
          i64.const 32
          i64.shl
          i64.const 2
          i64.or
          i64.store offset=16
          i32.const 1050536
          i32.const 42
          local.get 2
          i32.const 16
          i32.add
          i32.const 1050312
          i32.const 1050580
          call $core::result::unwrap_failed::h2b47cc7f7e98a508
          unreachable
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=16
        i32.const 1050536
        i32.const 42
        local.get 2
        i32.const 16
        i32.add
        i32.const 1050312
        i32.const 1050580
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 2
      local.get 8
      i32.store8 offset=15
      local.get 2
      i32.const 28
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 36
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 1051492
      i32.store offset=24
      local.get 2
      i32.const 0
      i32.store offset=16
      local.get 2
      i32.const 5
      i32.store offset=44
      local.get 2
      local.get 2
      i32.const 40
      i32.add
      i32.store offset=32
      local.get 2
      local.get 2
      i32.const 15
      i32.add
      i32.store offset=40
      local.get 2
      i32.const 16
      i32.add
      i32.const 1051500
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    local.get 2
    i32.const 1050256
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=16
    i32.const 1050536
    i32.const 42
    local.get 2
    i32.const 16
    i32.add
    i32.const 1050312
    i32.const 1050580
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<pbc_contract_common::zk::ZkInputDef<MetadataT>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h850791524b2127e5 (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32)
    local.get 1
    i32.const 8
    i32.add
    i32.load
    local.set 3
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 2
      i32.load offset=8
      local.tee 4
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 4
      i32.const 4
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 4
    end
    local.get 2
    local.get 4
    i32.const 4
    i32.add
    local.tee 5
    i32.store offset=8
    local.get 2
    i32.load offset=4
    local.tee 6
    local.get 4
    i32.add
    local.get 3
    i32.const 24
    i32.shl
    local.get 3
    i32.const 8
    i32.shl
    i32.const 16711680
    i32.and
    i32.or
    local.get 3
    i32.const 8
    i32.shr_u
    i32.const 65280
    i32.and
    local.get 3
    i32.const 24
    i32.shr_u
    i32.or
    i32.or
    i32.store align=1
    block  ;; label = @1
      local.get 3
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 4
      i32.add
      i32.load
      local.set 4
      local.get 3
      i32.const 2
      i32.shl
      local.set 7
      loop  ;; label = @2
        local.get 4
        i32.load
        local.set 3
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load
            local.get 5
            i32.sub
            i32.const 3
            i32.le_u
            br_if 0 (;@4;)
            local.get 5
            local.set 8
            br 1 (;@3;)
          end
          local.get 2
          local.get 5
          i32.const 4
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
          local.get 2
          i32.load offset=4
          local.set 6
          local.get 2
          i32.load offset=8
          local.set 8
        end
        local.get 2
        local.get 8
        i32.const 4
        i32.add
        local.tee 5
        i32.store offset=8
        local.get 6
        local.get 8
        i32.add
        local.get 3
        i32.const 24
        i32.shl
        local.get 3
        i32.const 8
        i32.shl
        i32.const 16711680
        i32.and
        i32.or
        local.get 3
        i32.const 8
        i32.shr_u
        i32.const 65280
        i32.and
        local.get 3
        i32.const 24
        i32.shr_u
        i32.or
        i32.or
        i32.store align=1
        local.get 4
        i32.const 4
        i32.add
        local.set 4
        local.get 7
        i32.const -4
        i32.add
        local.tee 7
        br_if 0 (;@2;)
      end
    end
    local.get 1
    i32.load8_u offset=12
    local.set 7
    block  ;; label = @1
      local.get 2
      i32.load
      local.tee 4
      local.get 5
      i32.ne
      br_if 0 (;@1;)
      local.get 2
      local.get 5
      i32.const 1
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load
      local.set 4
      local.get 2
      i32.load offset=8
      local.set 5
    end
    local.get 2
    local.get 5
    i32.const 1
    i32.add
    local.tee 3
    i32.store offset=8
    local.get 2
    i32.load offset=4
    local.tee 8
    local.get 5
    i32.add
    local.get 7
    i32.store8
    local.get 1
    i32.load8_u offset=13
    local.set 5
    block  ;; label = @1
      local.get 4
      local.get 3
      i32.ne
      br_if 0 (;@1;)
      local.get 2
      local.get 4
      i32.const 1
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=4
      local.set 8
      local.get 2
      i32.load offset=8
      local.set 3
    end
    local.get 0
    i32.const 4
    i32.store8
    local.get 2
    local.get 3
    i32.const 1
    i32.add
    i32.store offset=8
    local.get 8
    local.get 3
    i32.add
    i32.const 1
    i32.const 2
    local.get 5
    i32.const 255
    i32.and
    i32.const 1
    i32.eq
    select
    i32.store8)
  (func $<pbc_contract_common::zk::ZkState<SecretVarMetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hf3d4c317bb0bdf2a (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.load offset=4
            local.tee 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 1
            local.get 3
            i32.const -1
            i32.add
            i32.store offset=4
            local.get 1
            local.get 1
            i32.load
            local.tee 3
            i32.const 1
            i32.add
            i32.store
            local.get 3
            i32.load8_u
            local.tee 4
            i32.const 5
            i32.ge_u
            br_if 1 (;@3;)
            local.get 0
            i32.const 4
            i32.add
            local.get 1
            call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h8dd223453ec1258b
            local.get 0
            i32.const 16
            i32.add
            local.get 1
            call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h8dd223453ec1258b
            local.get 0
            i32.const 28
            i32.add
            local.get 1
            call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hcbd21698f96c3b16
            local.get 1
            i32.load offset=4
            local.tee 5
            i32.const 3
            i32.le_u
            br_if 2 (;@2;)
            local.get 1
            local.get 5
            i32.const -4
            i32.add
            local.tee 3
            i32.store offset=4
            local.get 1
            local.get 1
            i32.load
            local.tee 6
            i32.const 4
            i32.add
            i32.store
            local.get 3
            i32.const 3
            i32.le_u
            br_if 3 (;@1;)
            local.get 6
            i32.load align=1
            local.set 3
            local.get 0
            local.get 4
            i32.store8
            local.get 1
            local.get 5
            i32.const -8
            i32.add
            i32.store offset=4
            local.get 1
            local.get 6
            i32.const 8
            i32.add
            i32.store
            local.get 0
            local.get 3
            i32.const 24
            i32.shl
            local.get 3
            i32.const 8
            i32.shl
            i32.const 16711680
            i32.and
            i32.or
            local.get 3
            i32.const 8
            i32.shr_u
            i32.const 65280
            i32.and
            local.get 3
            i32.const 24
            i32.shr_u
            i32.or
            i32.or
            i32.store offset=40
            local.get 0
            local.get 6
            i32.load offset=4 align=1
            local.tee 1
            i32.const 24
            i32.shl
            local.get 1
            i32.const 8
            i32.shl
            i32.const 16711680
            i32.and
            i32.or
            local.get 1
            i32.const 8
            i32.shr_u
            i32.const 65280
            i32.and
            local.get 1
            i32.const 24
            i32.shr_u
            i32.or
            i32.or
            i32.store offset=44
            local.get 2
            i32.const 48
            i32.add
            global.set 0
            return
          end
          local.get 2
          i32.const 1050256
          i64.extend_i32_u
          i64.const 32
          i64.shl
          i64.const 2
          i64.or
          i64.store offset=16
          i32.const 1050536
          i32.const 42
          local.get 2
          i32.const 16
          i32.add
          i32.const 1050312
          i32.const 1050580
          call $core::result::unwrap_failed::h2b47cc7f7e98a508
          unreachable
        end
        local.get 2
        local.get 4
        i32.store8 offset=15
        local.get 2
        i32.const 28
        i32.add
        i32.const 1
        i32.store
        local.get 2
        i32.const 36
        i32.add
        i32.const 1
        i32.store
        local.get 2
        i32.const 1049380
        i32.store offset=24
        local.get 2
        i32.const 0
        i32.store offset=16
        local.get 2
        i32.const 5
        i32.store offset=44
        local.get 2
        local.get 2
        i32.const 40
        i32.add
        i32.store offset=32
        local.get 2
        local.get 2
        i32.const 15
        i32.add
        i32.store offset=40
        local.get 2
        i32.const 16
        i32.add
        i32.const 1049496
        call $core::panicking::panic_fmt::h9d972fcdb087ce21
        unreachable
      end
      local.get 2
      i32.const 1050256
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=16
      i32.const 1050460
      i32.const 43
      local.get 2
      i32.const 16
      i32.add
      i32.const 1050312
      i32.const 1050504
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 2
    i32.const 1050256
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=16
    i32.const 1050460
    i32.const 43
    local.get 2
    i32.const 16
    i32.add
    i32.const 1050312
    i32.const 1050504
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<&T_as_core::fmt::Debug>::fmt::hafe90a216e33631b (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    call $<pbc_contract_common::zk::CalculationStatus_as_core::fmt::Debug>::fmt::hff1dc751f644adc3)
  (func $core::ptr::drop_in_place<std::io::error::Error>::hda585276532b6f31 (type 0) (param i32)
    (local i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load8_u
      i32.const 3
      i32.ne
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=4
      local.tee 1
      i32.load
      local.get 1
      i32.load offset=4
      i32.load
      call_indirect (type 0)
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 2
        i32.const 4
        i32.add
        i32.load
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load
        local.get 3
        local.get 2
        i32.const 8
        i32.add
        i32.load
        call $__rust_dealloc
      end
      local.get 0
      i32.load offset=4
      i32.const 12
      i32.const 4
      call $__rust_dealloc
    end)
  (func $core::ptr::drop_in_place<pbc_contract_common::zk::ZkState<secret_voting::SecretVarMetadata>>::h18297c83edefcfa2 (type 0) (param i32)
    (local i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.const 12
      i32.add
      i32.load
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 40
      i32.mul
      local.set 2
      local.get 0
      i32.const 8
      i32.add
      i32.load
      i32.const 28
      i32.add
      local.set 1
      loop  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const 4
          i32.add
          i32.load
          local.tee 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.load
          local.tee 4
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          local.get 4
          i32.const 1
          call $__rust_dealloc
        end
        local.get 1
        i32.const 40
        i32.add
        local.set 1
        local.get 2
        i32.const -40
        i32.add
        local.tee 2
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      local.get 0
      i32.load offset=4
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 8
      i32.add
      i32.load
      local.get 1
      i32.const 40
      i32.mul
      i32.const 4
      call $__rust_dealloc
    end
    block  ;; label = @1
      local.get 0
      i32.const 24
      i32.add
      i32.load
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 40
      i32.mul
      local.set 2
      local.get 0
      i32.const 20
      i32.add
      i32.load
      i32.const 28
      i32.add
      local.set 1
      loop  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const 4
          i32.add
          i32.load
          local.tee 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.load
          local.tee 4
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          local.get 4
          i32.const 1
          call $__rust_dealloc
        end
        local.get 1
        i32.const 40
        i32.add
        local.set 1
        local.get 2
        i32.const -40
        i32.add
        local.tee 2
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      local.get 0
      i32.load offset=16
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 20
      i32.add
      i32.load
      local.get 1
      i32.const 40
      i32.mul
      i32.const 4
      call $__rust_dealloc
    end
    block  ;; label = @1
      local.get 0
      i32.const 36
      i32.add
      i32.load
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 28
      i32.mul
      local.set 2
      local.get 0
      i32.const 32
      i32.add
      i32.load
      i32.const 20
      i32.add
      local.set 1
      loop  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const -16
          i32.add
          i32.load
          local.tee 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.const -12
          i32.add
          i32.load
          local.get 3
          i32.const 65
          i32.mul
          i32.const 1
          call $__rust_dealloc
        end
        block  ;; label = @3
          local.get 1
          i32.const -4
          i32.add
          i32.load
          local.tee 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.load
          local.get 3
          i32.const 1
          call $__rust_dealloc
        end
        local.get 1
        i32.const 28
        i32.add
        local.set 1
        local.get 2
        i32.const -28
        i32.add
        local.tee 2
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      local.get 0
      i32.load offset=28
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 32
      i32.add
      i32.load
      local.get 1
      i32.const 28
      i32.mul
      i32.const 4
      call $__rust_dealloc
    end)
  (func $<pbc_contract_common::address_internal::Address_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h9cee193dfccabb0c (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.load offset=4
          local.tee 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.load
          local.tee 4
          i32.load8_u
          local.tee 5
          i32.const 4
          i32.ge_u
          br_if 1 (;@2;)
          local.get 3
          i32.const -1
          i32.add
          i32.const 19
          i32.le_u
          br_if 2 (;@1;)
          local.get 0
          local.get 5
          i32.store8
          local.get 0
          local.get 4
          i64.load offset=1 align=1
          i64.store offset=1 align=1
          local.get 1
          local.get 3
          i32.const -21
          i32.add
          i32.store offset=4
          local.get 1
          local.get 4
          i32.const 21
          i32.add
          i32.store
          local.get 0
          i32.const 17
          i32.add
          local.get 4
          i32.const 17
          i32.add
          i32.load align=1
          i32.store align=1
          local.get 0
          i32.const 9
          i32.add
          local.get 4
          i32.const 9
          i32.add
          i64.load align=1
          i64.store align=1
          local.get 2
          i32.const 48
          i32.add
          global.set 0
          return
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=16
        i32.const 1050536
        i32.const 42
        local.get 2
        i32.const 16
        i32.add
        i32.const 1050312
        i32.const 1050580
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 2
      local.get 5
      i32.store8 offset=15
      local.get 2
      i32.const 28
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 36
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 1049552
      i32.store offset=24
      local.get 2
      i32.const 0
      i32.store offset=16
      local.get 2
      i32.const 5
      i32.store offset=44
      local.get 2
      local.get 2
      i32.const 40
      i32.add
      i32.store offset=32
      local.get 2
      local.get 2
      i32.const 15
      i32.add
      i32.store offset=40
      local.get 2
      i32.const 16
      i32.add
      i32.const 1049680
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    local.get 2
    i32.const 1051544
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=16
    i32.const 1051556
    i32.const 43
    local.get 2
    i32.const 16
    i32.add
    i32.const 1051600
    i32.const 1051724
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<pbc_contract_common::address_internal::Address_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::hb4633c84a59d7c02 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.load offset=4
          local.tee 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.load
          local.tee 4
          i32.load8_u
          local.tee 5
          i32.const 4
          i32.ge_u
          br_if 1 (;@2;)
          local.get 3
          i32.const -1
          i32.add
          i32.const 19
          i32.le_u
          br_if 2 (;@1;)
          local.get 0
          local.get 5
          i32.store8
          local.get 0
          local.get 4
          i64.load offset=1 align=1
          i64.store offset=1 align=1
          local.get 1
          local.get 3
          i32.const -21
          i32.add
          i32.store offset=4
          local.get 1
          local.get 4
          i32.const 21
          i32.add
          i32.store
          local.get 0
          i32.const 17
          i32.add
          local.get 4
          i32.const 17
          i32.add
          i32.load align=1
          i32.store align=1
          local.get 0
          i32.const 9
          i32.add
          local.get 4
          i32.const 9
          i32.add
          i64.load align=1
          i64.store align=1
          local.get 2
          i32.const 48
          i32.add
          global.set 0
          return
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=16
        i32.const 1050536
        i32.const 42
        local.get 2
        i32.const 16
        i32.add
        i32.const 1050312
        i32.const 1050580
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 2
      local.get 5
      i32.store8 offset=15
      local.get 2
      i32.const 28
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 36
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 1049552
      i32.store offset=24
      local.get 2
      i32.const 0
      i32.store offset=16
      local.get 2
      i32.const 5
      i32.store offset=44
      local.get 2
      local.get 2
      i32.const 40
      i32.add
      i32.store offset=32
      local.get 2
      local.get 2
      i32.const 15
      i32.add
      i32.store offset=40
      local.get 2
      i32.const 16
      i32.add
      i32.const 1049696
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    local.get 2
    i32.const 1050076
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=16
    i32.const 1049852
    i32.const 43
    local.get 2
    i32.const 16
    i32.add
    i32.const 1049896
    i32.const 1050032
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $pbc_contract_common::result_buffer::ContractResultBuffer::write_state::h5b38c7bde86a79e9 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    i32.const 1
    i32.store8 offset=7
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.load8_u offset=12
        i32.const 1
        i32.gt_u
        br_if 0 (;@2;)
        local.get 0
        i32.const 2
        i32.store8 offset=12
        block  ;; label = @3
          local.get 0
          i32.load
          local.get 0
          i32.load offset=8
          local.tee 3
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
          local.get 0
          i32.load offset=8
          local.set 3
        end
        local.get 0
        i32.const 4
        i32.add
        local.tee 4
        i32.load
        local.get 3
        i32.add
        i32.const 1
        i32.store8
        local.get 0
        local.get 3
        i32.const 1
        i32.add
        local.tee 5
        i32.store offset=8
        local.get 5
        local.set 6
        block  ;; label = @3
          local.get 0
          i32.load
          local.get 5
          i32.sub
          i32.const 3
          i32.gt_u
          br_if 0 (;@3;)
          local.get 0
          local.get 5
          i32.const 4
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
          local.get 0
          i32.load offset=8
          local.set 6
        end
        local.get 0
        local.get 6
        i32.const 4
        i32.add
        i32.store offset=8
        local.get 4
        i32.load
        local.get 6
        i32.add
        i32.const 0
        i32.store align=1
        local.get 2
        i32.const 8
        i32.add
        local.get 1
        local.get 0
        call $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_write_to::h0259a84b5392ed7d
        block  ;; label = @3
          local.get 2
          i64.load8_u offset=8
          local.tee 7
          i64.const 4
          i64.eq
          br_if 0 (;@3;)
          local.get 2
          local.get 2
          i64.load32_u offset=9 align=1
          local.get 2
          i64.load16_u offset=13 align=1
          local.get 2
          i64.load8_u offset=15
          i64.const 16
          i64.shl
          i64.or
          i64.const 32
          i64.shl
          i64.or
          i64.const 8
          i64.shl
          local.get 7
          i64.or
          local.tee 7
          i64.store offset=32
          br 2 (;@1;)
        end
        local.get 2
        i32.const 32
        i32.add
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.get 0
        i32.load offset=8
        local.tee 0
        local.get 5
        local.get 0
        local.get 3
        i32.sub
        i32.const -5
        i32.add
        call $pbc_contract_common::result_buffer::write_u32_be_at_idx::h17d1ca3092b4244e
        block  ;; label = @3
          local.get 2
          i32.load8_u offset=32
          i32.const 4
          i32.eq
          br_if 0 (;@3;)
          local.get 2
          i64.load offset=32
          local.set 7
          br 2 (;@1;)
        end
        block  ;; label = @3
          local.get 1
          i32.load offset=40
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.const 44
          i32.add
          i32.load
          local.get 0
          i32.const 21
          i32.mul
          i32.const 1
          call $__rust_dealloc
        end
        local.get 2
        i32.const 48
        i32.add
        global.set 0
        return
      end
      local.get 2
      i32.const 32
      i32.add
      i32.const 12
      i32.add
      i32.const 8
      i32.store
      local.get 2
      i32.const 8
      i32.add
      i32.const 12
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 28
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 1050908
      i32.store offset=16
      local.get 2
      i32.const 2
      i32.store offset=12
      local.get 2
      i32.const 1050924
      i32.store offset=8
      local.get 2
      local.get 0
      i32.const 12
      i32.add
      i32.store offset=40
      local.get 2
      i32.const 8
      i32.store offset=36
      local.get 2
      local.get 2
      i32.const 32
      i32.add
      i32.store offset=24
      local.get 2
      local.get 2
      i32.const 7
      i32.add
      i32.store offset=32
      local.get 2
      i32.const 8
      i32.add
      i32.const 1050988
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    local.get 2
    local.get 7
    i64.store offset=8
    i32.const 1050596
    i32.const 43
    local.get 2
    i32.const 8
    i32.add
    i32.const 1050640
    i32.const 1050772
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $pbc_contract_common::result_buffer::ContractResultBuffer::write_zk_input_def_result::h4b3d52bc270a47f7 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    i32.const 18
    i32.store8 offset=7
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.load8_u offset=12
        i32.const 18
        i32.gt_u
        br_if 0 (;@2;)
        local.get 0
        i32.const 19
        i32.store8 offset=12
        block  ;; label = @3
          local.get 0
          i32.load
          local.get 0
          i32.load offset=8
          local.tee 3
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
          local.get 0
          i32.load offset=8
          local.set 3
        end
        local.get 0
        i32.const 4
        i32.add
        local.tee 4
        i32.load
        local.get 3
        i32.add
        i32.const 18
        i32.store8
        local.get 0
        local.get 3
        i32.const 1
        i32.add
        local.tee 5
        i32.store offset=8
        local.get 5
        local.set 6
        block  ;; label = @3
          local.get 0
          i32.load
          local.get 5
          i32.sub
          i32.const 3
          i32.gt_u
          br_if 0 (;@3;)
          local.get 0
          local.get 5
          i32.const 4
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
          local.get 0
          i32.load offset=8
          local.set 6
        end
        local.get 0
        local.get 6
        i32.const 4
        i32.add
        i32.store offset=8
        local.get 4
        i32.load
        local.get 6
        i32.add
        i32.const 0
        i32.store align=1
        local.get 2
        i32.const 8
        i32.add
        local.get 1
        local.get 0
        call $<pbc_contract_common::zk::ZkInputDef<MetadataT>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h850791524b2127e5
        block  ;; label = @3
          local.get 2
          i64.load8_u offset=8
          local.tee 7
          i64.const 4
          i64.eq
          br_if 0 (;@3;)
          local.get 2
          local.get 2
          i64.load32_u offset=9 align=1
          local.get 2
          i64.load16_u offset=13 align=1
          local.get 2
          i64.load8_u offset=15
          i64.const 16
          i64.shl
          i64.or
          i64.const 32
          i64.shl
          i64.or
          i64.const 8
          i64.shl
          local.get 7
          i64.or
          local.tee 7
          i64.store offset=32
          br 2 (;@1;)
        end
        local.get 2
        i32.const 32
        i32.add
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.get 0
        i32.load offset=8
        local.tee 0
        local.get 5
        local.get 0
        local.get 3
        i32.sub
        i32.const -5
        i32.add
        call $pbc_contract_common::result_buffer::write_u32_be_at_idx::h17d1ca3092b4244e
        block  ;; label = @3
          local.get 2
          i32.load8_u offset=32
          i32.const 4
          i32.eq
          br_if 0 (;@3;)
          local.get 2
          i64.load offset=32
          local.set 7
          br 2 (;@1;)
        end
        block  ;; label = @3
          local.get 1
          i32.load
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.load offset=4
          local.get 0
          i32.const 2
          i32.shl
          i32.const 4
          call $__rust_dealloc
        end
        local.get 2
        i32.const 48
        i32.add
        global.set 0
        return
      end
      local.get 2
      i32.const 32
      i32.add
      i32.const 12
      i32.add
      i32.const 8
      i32.store
      local.get 2
      i32.const 8
      i32.add
      i32.const 12
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 28
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 1050908
      i32.store offset=16
      local.get 2
      i32.const 2
      i32.store offset=12
      local.get 2
      i32.const 1050924
      i32.store offset=8
      local.get 2
      local.get 0
      i32.const 12
      i32.add
      i32.store offset=40
      local.get 2
      i32.const 8
      i32.store offset=36
      local.get 2
      local.get 2
      i32.const 32
      i32.add
      i32.store offset=24
      local.get 2
      local.get 2
      i32.const 7
      i32.add
      i32.store offset=32
      local.get 2
      i32.const 8
      i32.add
      i32.const 1050988
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    local.get 2
    local.get 7
    i64.store offset=8
    i32.const 1050596
    i32.const 43
    local.get 2
    i32.const 8
    i32.add
    i32.const 1050640
    i32.const 1051004
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $pbc_traits::readwrite_state::impl_vec::static_sized_content_write_to::hd012205ad8f72b36 (type 6) (param i32 i32 i32)
    (local i32 i64 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    local.get 1
    i64.load align=4
    local.tee 4
    i64.const 32
    i64.shr_u
    i32.wrap_i64
    local.set 5
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 2
      i32.load offset=8
      local.tee 1
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 1
      i32.const 4
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 1
    end
    local.get 2
    local.get 1
    i32.const 4
    i32.add
    local.tee 6
    i32.store offset=8
    local.get 2
    i32.load offset=4
    local.tee 7
    local.get 1
    i32.add
    local.get 5
    i32.store align=1
    local.get 3
    local.get 4
    i64.store
    block  ;; label = @1
      block  ;; label = @2
        local.get 4
        i32.wrap_i64
        local.tee 8
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        i32.load offset=4
        local.set 9
        i32.const 21
        local.set 10
        i32.const 1
        local.set 1
        loop  ;; label = @3
          local.get 1
          local.get 10
          local.get 1
          i32.sub
          local.tee 5
          local.get 5
          i32.ctz
          i32.shr_u
          local.tee 5
          local.get 1
          local.get 5
          i32.gt_u
          select
          local.set 10
          local.get 1
          local.get 5
          local.get 1
          local.get 5
          i32.lt_u
          select
          local.tee 5
          local.set 1
          local.get 10
          local.get 5
          i32.ne
          br_if 0 (;@3;)
        end
        block  ;; label = @3
          local.get 10
          i32.const 1
          i32.eq
          br_if 0 (;@3;)
          local.get 10
          i32.eqz
          br_if 2 (;@1;)
          i32.const 1049808
          i32.const 25
          i32.const 1049836
          call $core::panicking::panic::h364c37174a08a6a4
          unreachable
        end
        block  ;; label = @3
          local.get 2
          i32.load
          local.get 6
          i32.sub
          local.get 9
          i32.const 21
          i32.mul
          local.tee 1
          i32.ge_u
          br_if 0 (;@3;)
          local.get 2
          local.get 6
          local.get 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
          local.get 2
          i32.load offset=4
          local.set 7
          local.get 2
          i32.load offset=8
          local.set 6
        end
        local.get 7
        local.get 6
        i32.add
        local.get 8
        local.get 1
        call $memcpy
        drop
        local.get 2
        local.get 6
        local.get 1
        i32.add
        i32.store offset=8
      end
      local.get 0
      i32.const 4
      i32.store8
      local.get 3
      i32.const 16
      i32.add
      global.set 0
      return
    end
    i32.const 1049808
    i32.const 25
    i32.const 1049792
    call $core::panicking::panic::h364c37174a08a6a4
    unreachable)
  (func $pbc_traits::readwrite_state::impl_vec::static_sized_content_read_from::hdafbc1240f13888f (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.load offset=4
            local.tee 3
            i32.const 3
            i32.le_u
            br_if 0 (;@4;)
            i32.const 1
            local.set 4
            i32.const 1
            local.set 5
            block  ;; label = @5
              local.get 1
              i32.load
              local.tee 6
              i32.load align=1
              local.tee 7
              i32.eqz
              br_if 0 (;@5;)
              local.get 7
              i32.const 102261126
              i32.gt_u
              br_if 2 (;@3;)
              local.get 7
              i32.const 21
              i32.mul
              local.tee 8
              i32.const 0
              i32.lt_s
              br_if 2 (;@3;)
              local.get 7
              i32.const 102261127
              i32.lt_u
              local.set 9
              block  ;; label = @6
                block  ;; label = @7
                  local.get 8
                  br_if 0 (;@7;)
                  local.get 9
                  local.set 5
                  br 1 (;@6;)
                end
                local.get 8
                local.get 9
                call $__rust_alloc
                local.set 5
              end
              local.get 5
              i32.eqz
              br_if 3 (;@2;)
            end
            local.get 3
            i32.const -4
            i32.add
            local.set 8
            local.get 6
            i32.const 4
            i32.add
            local.set 9
            i32.const 21
            local.set 6
            loop  ;; label = @5
              local.get 4
              local.get 6
              local.get 4
              i32.sub
              local.tee 3
              local.get 3
              i32.ctz
              i32.shr_u
              local.tee 3
              local.get 4
              local.get 3
              i32.gt_u
              select
              local.set 6
              local.get 4
              local.get 3
              local.get 4
              local.get 3
              i32.lt_u
              select
              local.tee 3
              local.set 4
              local.get 6
              local.get 3
              i32.ne
              br_if 0 (;@5;)
            end
            block  ;; label = @5
              local.get 6
              i32.const 1
              i32.eq
              br_if 0 (;@5;)
              local.get 6
              i32.eqz
              br_if 4 (;@1;)
              i32.const 1051360
              i32.const 25
              i32.const 1051388
              call $core::panicking::panic::h364c37174a08a6a4
              unreachable
            end
            block  ;; label = @5
              local.get 8
              local.get 7
              i32.const 21
              i32.mul
              local.tee 4
              i32.lt_u
              br_if 0 (;@5;)
              block  ;; label = @6
                block  ;; label = @7
                  local.get 4
                  i32.const 1
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 5
                  local.get 9
                  local.get 4
                  call $memcpy
                  drop
                  br 1 (;@6;)
                end
                local.get 5
                local.get 9
                i32.load8_u
                i32.store8
              end
              local.get 0
              local.get 7
              i32.store offset=8
              local.get 0
              local.get 5
              i32.store offset=4
              local.get 0
              local.get 7
              i32.store
              local.get 1
              local.get 8
              local.get 4
              i32.sub
              i32.store offset=4
              local.get 1
              local.get 9
              local.get 4
              i32.add
              i32.store
              local.get 2
              i32.const 16
              i32.add
              global.set 0
              return
            end
            local.get 2
            i32.const 1051240
            i64.extend_i32_u
            i64.const 32
            i64.shl
            i64.const 2
            i64.or
            i64.store offset=8
            i32.const 1051136
            i32.const 43
            local.get 2
            i32.const 8
            i32.add
            i32.const 1051180
            i32.const 1051196
            call $core::result::unwrap_failed::h2b47cc7f7e98a508
            unreachable
          end
          local.get 2
          i32.const 1050256
          i64.extend_i32_u
          i64.const 32
          i64.shl
          i64.const 2
          i64.or
          i64.store offset=8
          i32.const 1050460
          i32.const 43
          local.get 2
          i32.const 8
          i32.add
          i32.const 1050312
          i32.const 1050520
          call $core::result::unwrap_failed::h2b47cc7f7e98a508
          unreachable
        end
        call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
        unreachable
      end
      local.get 8
      local.get 9
      call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
      unreachable
    end
    i32.const 1051360
    i32.const 25
    i32.const 1051332
    call $core::panicking::panic::h364c37174a08a6a4
    unreachable)
  (func $<&T_as_core::fmt::Debug>::fmt::h01863fc6dc583618 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.set 0
    block  ;; label = @1
      local.get 1
      call $core::fmt::Formatter::debug_lower_hex::h768a8b81c9cb249b
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 1
        call $core::fmt::Formatter::debug_upper_hex::hdedc73e262a99d1d
        br_if 0 (;@2;)
        local.get 0
        local.get 1
        call $core::fmt::num::imp::<impl_core::fmt::Display_for_u32>::fmt::h8b2af792c13f3dfc
        return
      end
      local.get 0
      local.get 1
      call $core::fmt::num::<impl_core::fmt::UpperHex_for_i32>::fmt::h2165261741388ccd
      return
    end
    local.get 0
    local.get 1
    call $core::fmt::num::<impl_core::fmt::LowerHex_for_i32>::fmt::hcca31281b8baf66e)
  (func $core::ptr::drop_in_place<&usize>::h740950a52e001245 (type 0) (param i32))
  (func $core::panicking::assert_failed::h261de8abfcde76a4 (type 7) (param i32 i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 5
    global.set 0
    local.get 5
    local.get 2
    i32.store offset=4
    local.get 5
    local.get 1
    i32.store
    local.get 5
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 3
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 5
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 3
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 5
    local.get 3
    i64.load align=4
    i64.store offset=8
    local.get 0
    local.get 5
    i32.const 1051404
    local.get 5
    i32.const 4
    i32.add
    i32.const 1051404
    local.get 5
    i32.const 8
    i32.add
    local.get 4
    call $core::panicking::assert_failed_inner::h499c6b26752f6efd
    unreachable)
  (func $core::panicking::assert_failed::h30c818d63c3cac54 (type 7) (param i32 i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 5
    global.set 0
    local.get 5
    local.get 2
    i32.store offset=4
    local.get 5
    local.get 1
    i32.store
    local.get 5
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 3
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 5
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 3
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 5
    local.get 3
    i64.load align=4
    i64.store offset=8
    local.get 0
    local.get 5
    i32.const 1051420
    local.get 5
    i32.const 4
    i32.add
    i32.const 1051420
    local.get 5
    i32.const 8
    i32.add
    local.get 4
    call $core::panicking::assert_failed_inner::h499c6b26752f6efd
    unreachable)
  (func $secret_voting::serialize::h6e00611e57e3db14 (type 3) (param i32 i32)
    (local i32 i32)
    local.get 0
    i32.const 0
    i32.store offset=8
    local.get 0
    i64.const 4294967296
    i64.store align=4
    local.get 1
    i32.load
    local.set 2
    local.get 0
    i32.const 0
    i32.const 4
    call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=8
    i32.add
    local.get 2
    i32.store align=1
    local.get 0
    local.get 0
    i32.load offset=8
    i32.const 4
    i32.add
    local.tee 2
    i32.store offset=8
    local.get 1
    i32.load offset=4
    local.set 3
    block  ;; label = @1
      local.get 0
      i32.load
      local.get 2
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.const 4
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 0
      i32.load offset=8
      local.set 2
    end
    local.get 0
    i32.load offset=4
    local.get 2
    i32.add
    local.get 3
    i32.store align=1
    local.get 0
    local.get 0
    i32.load offset=8
    i32.const 4
    i32.add
    local.tee 2
    i32.store offset=8
    local.get 1
    i32.load8_u offset=8
    local.set 1
    block  ;; label = @1
      local.get 0
      i32.load
      local.get 2
      i32.ne
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.const 1
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 0
      i32.load offset=8
      local.set 2
    end
    local.get 0
    i32.load offset=4
    local.get 2
    i32.add
    local.get 1
    i32.store8
    local.get 0
    local.get 0
    i32.load offset=8
    i32.const 1
    i32.add
    i32.store offset=8)
  (func $<secret_voting::VoteResult_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::hb02462b506590930 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.load offset=4
          local.tee 3
          i32.const 3
          i32.le_u
          br_if 0 (;@3;)
          local.get 3
          i32.const -4
          i32.and
          i32.const 4
          i32.eq
          br_if 1 (;@2;)
          local.get 3
          i32.const 8
          i32.eq
          br_if 2 (;@1;)
          local.get 1
          i32.load
          local.tee 4
          i32.load align=1
          local.set 5
          local.get 0
          local.get 4
          i32.load offset=4 align=1
          i32.store offset=4
          local.get 0
          local.get 5
          i32.store
          local.get 1
          local.get 3
          i32.const -9
          i32.add
          i32.store offset=4
          local.get 1
          local.get 4
          i32.const 9
          i32.add
          i32.store
          local.get 0
          local.get 4
          i32.load8_u offset=8
          i32.const 0
          i32.ne
          i32.store8 offset=8
          local.get 2
          i32.const 16
          i32.add
          global.set 0
          return
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=8
        i32.const 1050460
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050312
        i32.const 1050520
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 2
      i32.const 1050256
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=8
      i32.const 1050460
      i32.const 43
      local.get 2
      i32.const 8
      i32.add
      i32.const 1050312
      i32.const 1050520
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 2
    i32.const 1050256
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=8
    i32.const 1050536
    i32.const 42
    local.get 2
    i32.const 8
    i32.add
    i32.const 1050312
    i32.const 1050580
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<secret_voting::VoteResult_as_pbc_traits::readwrite_state::ReadWriteState>::state_write_to::h83300c55ff9d5401 (type 6) (param i32 i32 i32)
    (local i32 i32)
    local.get 1
    i32.load
    local.set 3
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 2
      i32.load offset=8
      local.tee 4
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 4
      i32.const 4
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 4
    end
    local.get 2
    i32.load offset=4
    local.get 4
    i32.add
    local.get 3
    i32.store align=1
    local.get 2
    local.get 2
    i32.load offset=8
    i32.const 4
    i32.add
    local.tee 4
    i32.store offset=8
    local.get 1
    i32.load offset=4
    local.set 3
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 4
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 4
      i32.const 4
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 4
    end
    local.get 2
    i32.load offset=4
    local.get 4
    i32.add
    local.get 3
    i32.store align=1
    local.get 2
    local.get 2
    i32.load offset=8
    i32.const 4
    i32.add
    local.tee 4
    i32.store offset=8
    local.get 1
    i32.load8_u offset=8
    local.set 1
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 4
      i32.ne
      br_if 0 (;@1;)
      local.get 2
      local.get 4
      i32.const 1
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 4
    end
    local.get 2
    i32.load offset=4
    local.get 4
    i32.add
    local.get 1
    i32.store8
    local.get 0
    i32.const 4
    i32.store8
    local.get 2
    local.get 2
    i32.load offset=8
    i32.const 1
    i32.add
    i32.store offset=8)
  (func $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::h184f071a584d8cbb (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i64 i64 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    local.get 1
    call $<pbc_contract_common::address_internal::Address_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::hb4633c84a59d7c02
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 1
                i32.load offset=4
                local.tee 3
                i32.const 7
                i32.le_u
                br_if 0 (;@6;)
                local.get 1
                local.get 3
                i32.const -8
                i32.add
                local.tee 4
                i32.store offset=4
                local.get 1
                local.get 1
                i32.load
                local.tee 5
                i32.const 8
                i32.add
                i32.store
                local.get 4
                i32.const 7
                i32.le_u
                br_if 1 (;@5;)
                local.get 5
                i64.load align=1
                local.set 6
                local.get 1
                local.get 3
                i32.const -16
                i32.add
                i32.store offset=4
                local.get 1
                local.get 5
                i32.const 16
                i32.add
                i32.store
                local.get 5
                i64.load offset=8 align=1
                local.set 7
                local.get 0
                i32.const 40
                i32.add
                local.get 1
                call $pbc_traits::readwrite_state::impl_vec::static_sized_content_read_from::hdafbc1240f13888f
                local.get 1
                i32.load offset=4
                local.tee 3
                i32.const 3
                i32.le_u
                br_if 2 (;@4;)
                local.get 3
                i32.const -4
                i32.and
                i32.const 4
                i32.eq
                br_if 3 (;@3;)
                local.get 1
                i32.load
                local.tee 5
                i32.load align=1
                local.set 8
                local.get 1
                local.get 3
                i32.const -8
                i32.add
                local.tee 4
                i32.store offset=4
                local.get 1
                local.get 5
                i32.const 8
                i32.add
                i32.store
                local.get 4
                i32.eqz
                br_if 4 (;@2;)
                local.get 5
                i32.load offset=4 align=1
                local.set 9
                local.get 1
                local.get 3
                i32.const -9
                i32.add
                local.tee 4
                i32.store offset=4
                local.get 1
                local.get 5
                i32.const 9
                i32.add
                i32.store
                local.get 4
                i32.eqz
                br_if 5 (;@1;)
                local.get 5
                i32.load8_u offset=8
                local.set 4
                local.get 1
                local.get 3
                i32.const -10
                i32.add
                i32.store offset=4
                local.get 1
                local.get 5
                i32.const 10
                i32.add
                i32.store
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 5
                    i32.load8_u offset=9
                    br_if 0 (;@8;)
                    local.get 0
                    i32.const 72
                    i32.add
                    i32.const 2
                    i32.store8
                    br 1 (;@7;)
                  end
                  local.get 0
                  i32.const 64
                  i32.add
                  local.get 1
                  call $<secret_voting::VoteResult_as_pbc_traits::readwrite_state::ReadWriteState>::state_read_from::hb02462b506590930
                end
                local.get 0
                local.get 8
                i32.store offset=52
                local.get 0
                local.get 7
                i64.store offset=32
                local.get 0
                local.get 6
                i64.store offset=24
                local.get 0
                i32.const 56
                i32.add
                local.get 9
                i32.store
                local.get 0
                i32.const 60
                i32.add
                local.get 4
                i32.const 255
                i32.and
                i32.const 0
                i32.ne
                i32.store8
                local.get 2
                i32.const 16
                i32.add
                global.set 0
                return
              end
              local.get 2
              i32.const 1050256
              i64.extend_i32_u
              i64.const 32
              i64.shl
              i64.const 2
              i64.or
              i64.store offset=8
              i32.const 1050268
              i32.const 43
              local.get 2
              i32.const 8
              i32.add
              i32.const 1050312
              i32.const 1050444
              call $core::result::unwrap_failed::h2b47cc7f7e98a508
              unreachable
            end
            local.get 2
            i32.const 1050256
            i64.extend_i32_u
            i64.const 32
            i64.shl
            i64.const 2
            i64.or
            i64.store offset=8
            i32.const 1050268
            i32.const 43
            local.get 2
            i32.const 8
            i32.add
            i32.const 1050312
            i32.const 1050444
            call $core::result::unwrap_failed::h2b47cc7f7e98a508
            unreachable
          end
          local.get 2
          i32.const 1050256
          i64.extend_i32_u
          i64.const 32
          i64.shl
          i64.const 2
          i64.or
          i64.store offset=8
          i32.const 1050460
          i32.const 43
          local.get 2
          i32.const 8
          i32.add
          i32.const 1050312
          i32.const 1050520
          call $core::result::unwrap_failed::h2b47cc7f7e98a508
          unreachable
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=8
        i32.const 1050460
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050312
        i32.const 1050520
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 2
      i32.const 1050256
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=8
      i32.const 1050536
      i32.const 42
      local.get 2
      i32.const 8
      i32.add
      i32.const 1050312
      i32.const 1050580
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 2
    i32.const 1050256
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=8
    i32.const 1050536
    i32.const 42
    local.get 2
    i32.const 8
    i32.add
    i32.const 1050312
    i32.const 1050580
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<secret_voting::ContractState_as_pbc_traits::readwrite_state::ReadWriteState>::state_write_to::h0259a84b5392ed7d (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i64)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    i32.const 16777986
    local.get 1
    i32.load8_u
    i32.const 3
    i32.shl
    i32.const 16
    i32.xor
    i32.const 248
    i32.and
    i32.shr_u
    local.set 4
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 2
      i32.load offset=8
      local.tee 5
      i32.ne
      br_if 0 (;@1;)
      local.get 2
      local.get 5
      i32.const 1
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 5
    end
    local.get 2
    i32.load offset=4
    local.get 5
    i32.add
    local.get 4
    i32.store8
    local.get 2
    local.get 2
    i32.load offset=8
    i32.const 1
    i32.add
    local.tee 5
    i32.store offset=8
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 5
      i32.sub
      i32.const 19
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 5
      i32.const 20
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 5
    end
    local.get 2
    i32.load offset=4
    local.get 5
    i32.add
    local.tee 5
    local.get 1
    i64.load offset=1 align=1
    i64.store align=1
    local.get 5
    i32.const 16
    i32.add
    local.get 1
    i32.const 17
    i32.add
    i32.load align=1
    i32.store align=1
    local.get 5
    i32.const 8
    i32.add
    local.get 1
    i32.const 9
    i32.add
    i64.load align=1
    i64.store align=1
    local.get 2
    local.get 2
    i32.load offset=8
    i32.const 20
    i32.add
    local.tee 5
    i32.store offset=8
    local.get 1
    i64.load offset=24
    local.set 6
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 5
      i32.sub
      i32.const 7
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 5
      i32.const 8
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 5
    end
    local.get 2
    i32.load offset=4
    local.get 5
    i32.add
    local.get 6
    i64.store align=1
    local.get 2
    local.get 2
    i32.load offset=8
    i32.const 8
    i32.add
    local.tee 5
    i32.store offset=8
    local.get 1
    i64.load offset=32
    local.set 6
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 5
      i32.sub
      i32.const 7
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 5
      i32.const 8
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
      local.get 2
      i32.load offset=8
      local.set 5
    end
    local.get 2
    i32.load offset=4
    local.get 5
    i32.add
    local.get 6
    i64.store align=1
    local.get 2
    local.get 2
    i32.load offset=8
    i32.const 8
    i32.add
    i32.store offset=8
    local.get 3
    local.get 1
    i32.const 44
    i32.add
    i64.load align=4
    i64.store offset=8
    local.get 3
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $pbc_traits::readwrite_state::impl_vec::static_sized_content_write_to::hd012205ad8f72b36
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 3
                i32.load8_u
                i32.const 4
                i32.eq
                br_if 0 (;@6;)
                local.get 3
                i64.load
                local.tee 6
                i64.const 255
                i64.and
                i64.const 4
                i64.ne
                br_if 1 (;@5;)
              end
              local.get 1
              i32.load offset=52
              local.set 4
              block  ;; label = @6
                local.get 2
                i32.load
                local.get 2
                i32.load offset=8
                local.tee 5
                i32.sub
                i32.const 3
                i32.gt_u
                br_if 0 (;@6;)
                local.get 2
                local.get 5
                i32.const 4
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
                local.get 2
                i32.load offset=8
                local.set 5
              end
              local.get 2
              i32.load offset=4
              local.get 5
              i32.add
              local.get 4
              i32.store align=1
              local.get 2
              local.get 2
              i32.load offset=8
              i32.const 4
              i32.add
              local.tee 5
              i32.store offset=8
              local.get 1
              i32.const 56
              i32.add
              i32.load
              local.set 4
              block  ;; label = @6
                local.get 2
                i32.load
                local.get 5
                i32.sub
                i32.const 3
                i32.gt_u
                br_if 0 (;@6;)
                local.get 2
                local.get 5
                i32.const 4
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
                local.get 2
                i32.load offset=8
                local.set 5
              end
              local.get 2
              i32.load offset=4
              local.get 5
              i32.add
              local.get 4
              i32.store align=1
              local.get 2
              local.get 2
              i32.load offset=8
              i32.const 4
              i32.add
              local.tee 5
              i32.store offset=8
              local.get 1
              i32.const 60
              i32.add
              i32.load8_u
              local.set 4
              block  ;; label = @6
                local.get 2
                i32.load
                local.get 5
                i32.ne
                br_if 0 (;@6;)
                local.get 2
                local.get 5
                i32.const 1
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
                local.get 2
                i32.load offset=8
                local.set 5
              end
              local.get 2
              i32.load offset=4
              local.get 5
              i32.add
              local.get 4
              i32.store8
              local.get 2
              local.get 2
              i32.load offset=8
              i32.const 1
              i32.add
              local.tee 5
              i32.store offset=8
              local.get 2
              i32.load
              local.set 4
              local.get 1
              i32.const 72
              i32.add
              i32.load8_u
              i32.const 2
              i32.ne
              br_if 1 (;@4;)
              block  ;; label = @6
                local.get 4
                local.get 5
                i32.ne
                br_if 0 (;@6;)
                local.get 2
                local.get 5
                i32.const 1
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
                local.get 2
                i32.load offset=8
                local.set 5
              end
              local.get 2
              i32.load offset=4
              local.get 5
              i32.add
              i32.const 0
              i32.store8
              local.get 2
              local.get 2
              i32.load offset=8
              i32.const 1
              i32.add
              i32.store offset=8
              br 2 (;@3;)
            end
            local.get 0
            local.get 6
            i64.store align=4
            br 3 (;@1;)
          end
          local.get 1
          i32.const 64
          i32.add
          local.set 1
          block  ;; label = @4
            local.get 4
            local.get 5
            i32.ne
            br_if 0 (;@4;)
            local.get 2
            local.get 5
            i32.const 1
            call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
            local.get 2
            i32.load offset=8
            local.set 5
          end
          local.get 2
          i32.load offset=4
          local.get 5
          i32.add
          i32.const 1
          i32.store8
          local.get 2
          local.get 2
          i32.load offset=8
          i32.const 1
          i32.add
          i32.store offset=8
          local.get 3
          i32.const 8
          i32.add
          local.get 1
          local.get 2
          call $<secret_voting::VoteResult_as_pbc_traits::readwrite_state::ReadWriteState>::state_write_to::h83300c55ff9d5401
          local.get 3
          i32.load8_u offset=8
          i32.const 4
          i32.eq
          br_if 0 (;@3;)
          local.get 3
          i64.load offset=8
          local.tee 6
          i64.const 255
          i64.and
          i64.const 4
          i64.ne
          br_if 1 (;@2;)
        end
        local.get 0
        i32.const 4
        i32.store8
        br 1 (;@1;)
      end
      local.get 0
      local.get 6
      i64.store align=4
    end
    local.get 3
    i32.const 16
    i32.add
    global.set 0)
  (func $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h1dd5eb2af10ed63f (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.load offset=4
          local.tee 3
          i32.const 3
          i32.le_u
          br_if 0 (;@3;)
          local.get 1
          local.get 3
          i32.const -4
          i32.add
          i32.store offset=4
          local.get 1
          local.get 1
          i32.load
          local.tee 4
          i32.const 4
          i32.add
          i32.store
          block  ;; label = @4
            block  ;; label = @5
              local.get 4
              i32.load align=1
              local.tee 5
              i32.const 24
              i32.shl
              local.get 5
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 5
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 5
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              local.tee 6
              i32.const 128
              local.get 6
              i32.const 128
              i32.lt_u
              select
              local.tee 7
              br_if 0 (;@5;)
              i32.const 1
              local.set 8
              br 1 (;@4;)
            end
            local.get 7
            i32.const 1
            call $__rust_alloc
            local.tee 8
            i32.eqz
            br_if 2 (;@2;)
          end
          i32.const 0
          local.set 9
          local.get 0
          i32.const 0
          i32.store offset=8
          local.get 0
          local.get 8
          i32.store offset=4
          local.get 0
          local.get 7
          i32.store
          block  ;; label = @4
            local.get 5
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            i32.const -5
            i32.add
            local.set 5
            local.get 4
            i32.const 5
            i32.add
            local.set 3
            loop  ;; label = @5
              local.get 5
              i32.const -1
              i32.eq
              br_if 4 (;@1;)
              local.get 1
              local.get 5
              i32.store offset=4
              local.get 1
              local.get 3
              i32.store
              local.get 3
              i32.const -1
              i32.add
              i32.load8_u
              local.set 4
              block  ;; label = @6
                local.get 9
                local.get 0
                i32.load
                i32.ne
                br_if 0 (;@6;)
                local.get 0
                local.get 9
                call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::hdef54ec21e34b448
                local.get 0
                i32.load offset=8
                local.set 9
              end
              local.get 0
              i32.load offset=4
              local.get 9
              i32.add
              local.get 4
              i32.store8
              local.get 0
              local.get 0
              i32.load offset=8
              i32.const 1
              i32.add
              local.tee 9
              i32.store offset=8
              local.get 5
              i32.const -1
              i32.add
              local.set 5
              local.get 3
              i32.const 1
              i32.add
              local.set 3
              local.get 6
              i32.const -1
              i32.add
              local.tee 6
              br_if 0 (;@5;)
            end
          end
          local.get 2
          i32.const 16
          i32.add
          global.set 0
          return
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=8
        i32.const 1050460
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050312
        i32.const 1050504
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 7
      i32.const 1
      call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
      unreachable
    end
    local.get 2
    i32.const 1050256
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=8
    i32.const 1050536
    i32.const 42
    local.get 2
    i32.const 8
    i32.add
    i32.const 1050312
    i32.const 1050580
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h496587b821992e5f (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.load offset=4
          local.tee 3
          i32.const 3
          i32.le_u
          br_if 0 (;@3;)
          local.get 1
          local.get 3
          i32.const -4
          i32.add
          local.tee 4
          i32.store offset=4
          i32.const 4
          local.set 5
          local.get 1
          local.get 1
          i32.load
          local.tee 6
          i32.const 4
          i32.add
          i32.store
          block  ;; label = @4
            local.get 6
            i32.load align=1
            local.tee 7
            i32.const 24
            i32.shl
            local.get 7
            i32.const 8
            i32.shl
            i32.const 16711680
            i32.and
            i32.or
            local.get 7
            i32.const 8
            i32.shr_u
            i32.const 65280
            i32.and
            local.get 7
            i32.const 24
            i32.shr_u
            i32.or
            i32.or
            local.tee 8
            i32.const 128
            local.get 8
            i32.const 128
            i32.lt_u
            select
            local.tee 9
            i32.eqz
            br_if 0 (;@4;)
            local.get 9
            i32.const 2
            i32.shl
            local.tee 3
            i32.const 4
            call $__rust_alloc
            local.tee 5
            i32.eqz
            br_if 2 (;@2;)
          end
          i32.const 0
          local.set 3
          local.get 0
          i32.const 0
          i32.store offset=8
          local.get 0
          local.get 5
          i32.store offset=4
          local.get 0
          local.get 9
          i32.store
          block  ;; label = @4
            local.get 7
            i32.eqz
            br_if 0 (;@4;)
            local.get 6
            i32.const 8
            i32.add
            local.set 7
            loop  ;; label = @5
              local.get 4
              i32.const 3
              i32.le_u
              br_if 4 (;@1;)
              local.get 1
              local.get 7
              i32.store
              local.get 1
              local.get 4
              i32.const -4
              i32.add
              local.tee 4
              i32.store offset=4
              local.get 7
              i32.const -4
              i32.add
              i32.load align=1
              local.tee 6
              i32.const 24
              i32.shl
              local.get 6
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 6
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 6
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              local.set 6
              block  ;; label = @6
                local.get 3
                local.get 0
                i32.load
                i32.ne
                br_if 0 (;@6;)
                local.get 0
                local.get 3
                call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::h7dbd839d9d266f73
                local.get 0
                i32.load offset=8
                local.set 3
              end
              local.get 0
              i32.load offset=4
              local.get 3
              i32.const 2
              i32.shl
              i32.add
              local.get 6
              i32.store
              local.get 0
              local.get 0
              i32.load offset=8
              i32.const 1
              i32.add
              local.tee 3
              i32.store offset=8
              local.get 7
              i32.const 4
              i32.add
              local.set 7
              local.get 8
              i32.const -1
              i32.add
              local.tee 8
              br_if 0 (;@5;)
            end
          end
          local.get 2
          i32.const 16
          i32.add
          global.set 0
          return
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=8
        i32.const 1050460
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050312
        i32.const 1050504
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 3
      i32.const 4
      call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
      unreachable
    end
    local.get 2
    i32.const 1050256
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=8
    i32.const 1050460
    i32.const 43
    local.get 2
    i32.const 8
    i32.add
    i32.const 1050312
    i32.const 1050504
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h7022ecef7bbc5ec9 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 3
        i32.const 3
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        local.get 3
        i32.const -4
        i32.add
        i32.store offset=4
        local.get 1
        local.get 1
        i32.load
        local.tee 3
        i32.const 4
        i32.add
        i32.store
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.load align=1
            local.tee 3
            i32.const 24
            i32.shl
            local.get 3
            i32.const 8
            i32.shl
            i32.const 16711680
            i32.and
            i32.or
            local.get 3
            i32.const 8
            i32.shr_u
            i32.const 65280
            i32.and
            local.get 3
            i32.const 24
            i32.shr_u
            i32.or
            i32.or
            local.tee 4
            i32.const 128
            local.get 4
            i32.const 128
            i32.lt_u
            select
            local.tee 5
            br_if 0 (;@4;)
            i32.const 1
            local.set 6
            br 1 (;@3;)
          end
          local.get 5
          i32.const 21
          i32.mul
          local.tee 7
          i32.const 1
          call $__rust_alloc
          local.tee 6
          i32.eqz
          br_if 2 (;@1;)
        end
        local.get 0
        i32.const 0
        i32.store offset=8
        local.get 0
        local.get 6
        i32.store offset=4
        local.get 0
        local.get 5
        i32.store
        block  ;; label = @3
          local.get 3
          i32.eqz
          br_if 0 (;@3;)
          loop  ;; label = @4
            local.get 2
            i32.const 8
            i32.add
            local.get 1
            call $<pbc_contract_common::address_internal::Address_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h9cee193dfccabb0c
            block  ;; label = @5
              local.get 0
              i32.load offset=8
              local.tee 3
              local.get 0
              i32.load
              i32.ne
              br_if 0 (;@5;)
              local.get 0
              local.get 3
              call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::he0d920f733adce36
              local.get 0
              i32.load offset=8
              local.set 3
            end
            local.get 0
            i32.load offset=4
            local.get 3
            i32.const 21
            i32.mul
            i32.add
            local.tee 5
            local.get 2
            i64.load offset=8
            i64.store align=1
            local.get 5
            i32.const 8
            i32.add
            local.get 2
            i32.const 8
            i32.add
            i32.const 8
            i32.add
            i64.load
            i64.store align=1
            local.get 5
            i32.const 13
            i32.add
            local.get 2
            i32.const 8
            i32.add
            i32.const 13
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 0
            local.get 3
            i32.const 1
            i32.add
            i32.store offset=8
            local.get 4
            i32.const -1
            i32.add
            local.tee 4
            br_if 0 (;@4;)
          end
        end
        local.get 2
        i32.const 32
        i32.add
        global.set 0
        return
      end
      local.get 2
      i32.const 1050256
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=8
      i32.const 1050460
      i32.const 43
      local.get 2
      i32.const 8
      i32.add
      i32.const 1050312
      i32.const 1050504
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 7
    i32.const 1
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h8dd223453ec1258b (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 3
        i32.const 3
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        local.get 3
        i32.const -4
        i32.add
        i32.store offset=4
        i32.const 4
        local.set 4
        local.get 1
        local.get 1
        i32.load
        local.tee 3
        i32.const 4
        i32.add
        i32.store
        block  ;; label = @3
          local.get 3
          i32.load align=1
          local.tee 3
          i32.const 24
          i32.shl
          local.get 3
          i32.const 8
          i32.shl
          i32.const 16711680
          i32.and
          i32.or
          local.get 3
          i32.const 8
          i32.shr_u
          i32.const 65280
          i32.and
          local.get 3
          i32.const 24
          i32.shr_u
          i32.or
          i32.or
          local.tee 5
          i32.const 128
          local.get 5
          i32.const 128
          i32.lt_u
          select
          local.tee 6
          i32.eqz
          br_if 0 (;@3;)
          local.get 6
          i32.const 40
          i32.mul
          local.tee 7
          i32.const 4
          call $__rust_alloc
          local.tee 4
          i32.eqz
          br_if 2 (;@1;)
        end
        local.get 0
        i32.const 0
        i32.store offset=8
        local.get 0
        local.get 4
        i32.store offset=4
        local.get 0
        local.get 6
        i32.store
        block  ;; label = @3
          local.get 3
          i32.eqz
          br_if 0 (;@3;)
          loop  ;; label = @4
            local.get 2
            i32.const 8
            i32.add
            local.get 1
            call $<pbc_contract_common::zk::ZkClosed<MetadataT>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hc52b7da05bccc452
            block  ;; label = @5
              local.get 0
              i32.load offset=8
              local.tee 4
              local.get 0
              i32.load
              i32.ne
              br_if 0 (;@5;)
              local.get 0
              local.get 4
              call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::hf0aaf4bc3c260b6c
              local.get 0
              i32.load offset=8
              local.set 4
            end
            local.get 0
            i32.load offset=4
            local.get 4
            i32.const 40
            i32.mul
            i32.add
            local.tee 3
            local.get 2
            i64.load offset=8
            i64.store align=4
            local.get 3
            i32.const 8
            i32.add
            local.get 2
            i32.const 8
            i32.add
            i32.const 8
            i32.add
            i64.load
            i64.store align=4
            local.get 3
            i32.const 16
            i32.add
            local.get 2
            i32.const 8
            i32.add
            i32.const 16
            i32.add
            i64.load
            i64.store align=4
            local.get 3
            i32.const 24
            i32.add
            local.get 2
            i32.const 8
            i32.add
            i32.const 24
            i32.add
            i64.load
            i64.store align=4
            local.get 3
            i32.const 32
            i32.add
            local.get 2
            i32.const 8
            i32.add
            i32.const 32
            i32.add
            i64.load
            i64.store align=4
            local.get 0
            local.get 4
            i32.const 1
            i32.add
            i32.store offset=8
            local.get 5
            i32.const -1
            i32.add
            local.tee 5
            br_if 0 (;@4;)
          end
        end
        local.get 2
        i32.const 48
        i32.add
        global.set 0
        return
      end
      local.get 2
      i32.const 1050256
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=8
      i32.const 1050460
      i32.const 43
      local.get 2
      i32.const 8
      i32.add
      i32.const 1050312
      i32.const 1050504
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 7
    i32.const 4
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hbe56fec966826924 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 80
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.load offset=4
              local.tee 3
              i32.const 3
              i32.le_u
              br_if 0 (;@5;)
              local.get 1
              local.get 3
              i32.const -4
              i32.add
              local.tee 4
              i32.store offset=4
              local.get 1
              local.get 1
              i32.load
              local.tee 5
              i32.const 4
              i32.add
              local.tee 3
              i32.store
              block  ;; label = @6
                block  ;; label = @7
                  local.get 5
                  i32.load align=1
                  local.tee 6
                  i32.const 24
                  i32.shl
                  local.get 6
                  i32.const 8
                  i32.shl
                  i32.const 16711680
                  i32.and
                  i32.or
                  local.get 6
                  i32.const 8
                  i32.shr_u
                  i32.const 65280
                  i32.and
                  local.get 6
                  i32.const 24
                  i32.shr_u
                  i32.or
                  i32.or
                  local.tee 7
                  i32.const 128
                  local.get 7
                  i32.const 128
                  i32.lt_u
                  select
                  local.tee 8
                  br_if 0 (;@7;)
                  i32.const 1
                  local.set 9
                  br 1 (;@6;)
                end
                local.get 8
                i32.const 65
                i32.mul
                local.tee 5
                i32.const 1
                call $__rust_alloc
                local.tee 9
                i32.eqz
                br_if 2 (;@4;)
              end
              i32.const 0
              local.set 5
              local.get 0
              i32.const 0
              i32.store offset=8
              local.get 0
              local.get 9
              i32.store offset=4
              local.get 0
              local.get 8
              i32.store
              block  ;; label = @6
                local.get 6
                i32.eqz
                br_if 0 (;@6;)
                local.get 2
                i32.const 8
                i32.add
                i32.const 32
                i32.add
                local.set 6
                local.get 2
                i32.const 8
                i32.add
                i32.const 24
                i32.add
                local.set 8
                local.get 2
                i32.const 8
                i32.add
                i32.const 16
                i32.add
                local.set 9
                local.get 2
                i32.const 8
                i32.add
                i32.const 8
                i32.add
                local.set 10
                loop  ;; label = @7
                  local.get 4
                  i32.eqz
                  br_if 4 (;@3;)
                  local.get 4
                  i32.const -1
                  i32.add
                  local.tee 4
                  i32.const 31
                  i32.le_u
                  br_if 5 (;@2;)
                  local.get 3
                  i32.load8_u
                  local.set 11
                  local.get 8
                  local.get 3
                  i32.const 25
                  i32.add
                  i64.load align=1
                  i64.store
                  local.get 9
                  local.get 3
                  i32.const 17
                  i32.add
                  i64.load align=1
                  i64.store
                  local.get 10
                  local.get 3
                  i32.const 9
                  i32.add
                  i64.load align=1
                  i64.store
                  local.get 2
                  local.get 3
                  i64.load offset=1 align=1
                  i64.store offset=8
                  local.get 4
                  i32.const -32
                  i32.add
                  local.tee 4
                  i32.const 31
                  i32.le_u
                  br_if 6 (;@1;)
                  local.get 6
                  local.get 3
                  i64.load offset=33 align=1
                  i64.store align=1
                  local.get 1
                  local.get 4
                  i32.const -32
                  i32.add
                  local.tee 4
                  i32.store offset=4
                  local.get 1
                  local.get 3
                  i32.const 65
                  i32.add
                  local.tee 12
                  i32.store
                  local.get 6
                  i32.const 24
                  i32.add
                  local.get 3
                  i32.const 57
                  i32.add
                  i64.load align=1
                  i64.store align=1
                  local.get 6
                  i32.const 16
                  i32.add
                  local.get 3
                  i32.const 49
                  i32.add
                  i64.load align=1
                  i64.store align=1
                  local.get 6
                  i32.const 8
                  i32.add
                  local.get 3
                  i32.const 41
                  i32.add
                  i64.load align=1
                  i64.store align=1
                  block  ;; label = @8
                    local.get 5
                    local.get 0
                    i32.load
                    i32.ne
                    br_if 0 (;@8;)
                    local.get 0
                    local.get 5
                    call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::he790951035f87438
                    local.get 0
                    i32.load offset=8
                    local.set 5
                  end
                  local.get 0
                  i32.load offset=4
                  local.get 5
                  i32.const 65
                  i32.mul
                  i32.add
                  local.tee 3
                  local.get 2
                  i64.load offset=8
                  i64.store align=1
                  local.get 3
                  local.get 11
                  i32.store8 offset=64
                  local.get 3
                  i32.const 8
                  i32.add
                  local.get 10
                  i64.load
                  i64.store align=1
                  local.get 3
                  i32.const 16
                  i32.add
                  local.get 9
                  i64.load
                  i64.store align=1
                  local.get 3
                  i32.const 24
                  i32.add
                  local.get 8
                  i64.load
                  i64.store align=1
                  local.get 3
                  i32.const 32
                  i32.add
                  local.get 6
                  i64.load
                  i64.store align=1
                  local.get 3
                  i32.const 40
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 40
                  i32.add
                  i64.load
                  i64.store align=1
                  local.get 3
                  i32.const 48
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 48
                  i32.add
                  i64.load
                  i64.store align=1
                  local.get 3
                  i32.const 56
                  i32.add
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 56
                  i32.add
                  i64.load
                  i64.store align=1
                  local.get 0
                  local.get 5
                  i32.const 1
                  i32.add
                  local.tee 5
                  i32.store offset=8
                  local.get 12
                  local.set 3
                  local.get 7
                  i32.const -1
                  i32.add
                  local.tee 7
                  br_if 0 (;@7;)
                end
              end
              local.get 2
              i32.const 80
              i32.add
              global.set 0
              return
            end
            local.get 2
            i32.const 1050256
            i64.extend_i32_u
            i64.const 32
            i64.shl
            i64.const 2
            i64.or
            i64.store offset=8
            i32.const 1050460
            i32.const 43
            local.get 2
            i32.const 8
            i32.add
            i32.const 1050312
            i32.const 1050504
            call $core::result::unwrap_failed::h2b47cc7f7e98a508
            unreachable
          end
          local.get 5
          i32.const 1
          call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
          unreachable
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=72
        i32.const 1050536
        i32.const 42
        local.get 2
        i32.const 72
        i32.add
        i32.const 1050312
        i32.const 1050580
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 2
      i32.const 1051544
      i64.extend_i32_u
      i64.const 32
      i64.shl
      i64.const 2
      i64.or
      i64.store offset=72
      i32.const 1051556
      i32.const 43
      local.get 2
      i32.const 72
      i32.add
      i32.const 1051600
      i32.const 1051724
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 2
    i32.const 1051544
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=72
    i32.const 1051556
    i32.const 43
    local.get 2
    i32.const 72
    i32.add
    i32.const 1051600
    i32.const 1051724
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hcbd21698f96c3b16 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.load offset=4
          local.tee 3
          i32.const 3
          i32.le_u
          br_if 0 (;@3;)
          local.get 1
          local.get 3
          i32.const -4
          i32.add
          i32.store offset=4
          i32.const 4
          local.set 4
          local.get 1
          local.get 1
          i32.load
          local.tee 3
          i32.const 4
          i32.add
          i32.store
          block  ;; label = @4
            local.get 3
            i32.load align=1
            local.tee 3
            i32.const 24
            i32.shl
            local.get 3
            i32.const 8
            i32.shl
            i32.const 16711680
            i32.and
            i32.or
            local.get 3
            i32.const 8
            i32.shr_u
            i32.const 65280
            i32.and
            local.get 3
            i32.const 24
            i32.shr_u
            i32.or
            i32.or
            local.tee 5
            i32.const 128
            local.get 5
            i32.const 128
            i32.lt_u
            select
            local.tee 6
            i32.eqz
            br_if 0 (;@4;)
            local.get 6
            i32.const 28
            i32.mul
            local.tee 7
            i32.const 4
            call $__rust_alloc
            local.tee 4
            i32.eqz
            br_if 2 (;@2;)
          end
          local.get 0
          i32.const 0
          i32.store offset=8
          local.get 0
          local.get 4
          i32.store offset=4
          local.get 0
          local.get 6
          i32.store
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 8
            i32.add
            i32.const 16
            i32.add
            local.set 6
            local.get 2
            i32.const 8
            i32.add
            i32.const 4
            i32.or
            local.set 7
            loop  ;; label = @5
              local.get 1
              i32.load offset=4
              local.tee 3
              i32.const 3
              i32.le_u
              br_if 4 (;@1;)
              local.get 1
              local.get 3
              i32.const -4
              i32.add
              i32.store offset=4
              local.get 1
              local.get 1
              i32.load
              local.tee 3
              i32.const 4
              i32.add
              i32.store
              local.get 3
              i32.load align=1
              local.set 3
              local.get 7
              local.get 1
              call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::hbe56fec966826924
              local.get 6
              local.get 1
              call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::ReadRPC>::rpc_read_from::h1dd5eb2af10ed63f
              local.get 2
              local.get 3
              i32.const 24
              i32.shl
              local.get 3
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 3
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 3
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              i32.store offset=8
              block  ;; label = @6
                local.get 0
                i32.load offset=8
                local.tee 3
                local.get 0
                i32.load
                i32.ne
                br_if 0 (;@6;)
                local.get 0
                local.get 3
                call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::hd4c9872e3ab1b4d0
                local.get 0
                i32.load offset=8
                local.set 3
              end
              local.get 0
              i32.load offset=4
              local.get 3
              i32.const 28
              i32.mul
              i32.add
              local.tee 4
              local.get 2
              i64.load offset=8
              i64.store align=4
              local.get 4
              i32.const 8
              i32.add
              local.get 2
              i32.const 8
              i32.add
              i32.const 8
              i32.add
              i64.load
              i64.store align=4
              local.get 4
              i32.const 16
              i32.add
              local.get 6
              i64.load
              i64.store align=4
              local.get 4
              i32.const 24
              i32.add
              local.get 2
              i32.const 8
              i32.add
              i32.const 24
              i32.add
              i32.load
              i32.store
              local.get 0
              local.get 3
              i32.const 1
              i32.add
              i32.store offset=8
              local.get 5
              i32.const -1
              i32.add
              local.tee 5
              br_if 0 (;@5;)
            end
          end
          local.get 2
          i32.const 48
          i32.add
          global.set 0
          return
        end
        local.get 2
        i32.const 1050256
        i64.extend_i32_u
        i64.const 32
        i64.shl
        i64.const 2
        i64.or
        i64.store offset=8
        i32.const 1050460
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050312
        i32.const 1050504
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 7
      i32.const 4
      call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
      unreachable
    end
    local.get 2
    i32.const 1050256
    i64.extend_i32_u
    i64.const 32
    i64.shl
    i64.const 2
    i64.or
    i64.store offset=40
    i32.const 1050460
    i32.const 43
    local.get 2
    i32.const 40
    i32.add
    i32.const 1050312
    i32.const 1050504
    call $core::result::unwrap_failed::h2b47cc7f7e98a508
    unreachable)
  (func $<alloc::vec::Vec<T>_as_alloc::vec::spec_from_iter::SpecFromIter<T_I>>::from_iter::h246118b45cb54e1d (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    local.get 1
    local.get 2
    i32.sub
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            local.get 2
            i32.ne
            br_if 0 (;@4;)
            local.get 0
            i32.const 4
            i32.store offset=4
            local.get 0
            local.get 4
            i32.store
            local.get 0
            i32.const 8
            i32.add
            local.set 5
            i32.const 0
            local.set 0
            br 1 (;@3;)
          end
          local.get 4
          i32.const 178956970
          i32.gt_u
          br_if 1 (;@2;)
          local.get 4
          i32.const 12
          i32.mul
          local.tee 6
          i32.const 0
          i32.lt_s
          br_if 1 (;@2;)
          local.get 4
          i32.const 178956971
          i32.lt_u
          i32.const 2
          i32.shl
          local.set 5
          block  ;; label = @4
            block  ;; label = @5
              local.get 6
              br_if 0 (;@5;)
              local.get 5
              local.set 7
              br 1 (;@4;)
            end
            local.get 6
            local.get 5
            call $__rust_alloc
            local.set 7
          end
          local.get 7
          i32.eqz
          br_if 2 (;@1;)
          local.get 0
          i32.const 0
          i32.store offset=8
          local.get 0
          local.get 7
          i32.store offset=4
          local.get 0
          local.get 4
          i32.store
          local.get 0
          i32.const 8
          i32.add
          local.set 5
          i32.const 0
          local.set 0
          loop  ;; label = @4
            local.get 3
            i32.const 8
            i32.add
            local.tee 4
            i32.const 0
            i32.store
            local.get 3
            i64.const 4294967296
            i64.store
            local.get 2
            local.get 0
            i32.add
            i32.load8_u
            local.set 6
            local.get 3
            i32.const 0
            i32.const 1
            call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533
            local.get 3
            i32.load offset=4
            local.get 4
            i32.load
            local.tee 4
            i32.add
            i32.const 1
            i32.const 2
            local.get 6
            i32.const 1
            i32.eq
            select
            i32.store8
            local.get 7
            local.get 3
            i64.load
            i64.store align=4
            local.get 7
            i32.const 8
            i32.add
            local.get 4
            i32.const 1
            i32.add
            i32.store
            local.get 7
            i32.const 12
            i32.add
            local.set 7
            local.get 2
            local.get 0
            i32.const 1
            i32.add
            local.tee 0
            i32.add
            local.get 1
            i32.ne
            br_if 0 (;@4;)
          end
        end
        local.get 5
        local.get 0
        i32.store
        local.get 3
        i32.const 16
        i32.add
        global.set 0
        return
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 6
    local.get 5
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $alloc::raw_vec::finish_grow::h54895925b0829b77 (type 8) (param i32 i32 i32 i32)
    (local i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  i32.const 0
                  i32.lt_s
                  br_if 0 (;@7;)
                  local.get 3
                  i32.load offset=8
                  i32.eqz
                  br_if 2 (;@5;)
                  local.get 3
                  i32.load offset=4
                  local.tee 4
                  br_if 1 (;@6;)
                  local.get 1
                  br_if 3 (;@4;)
                  local.get 2
                  local.set 3
                  br 4 (;@3;)
                end
                local.get 0
                i32.const 8
                i32.add
                i32.const 0
                i32.store
                br 5 (;@1;)
              end
              local.get 3
              i32.load
              local.get 4
              local.get 2
              local.get 1
              call $__rust_realloc
              local.set 3
              br 2 (;@3;)
            end
            local.get 1
            br_if 0 (;@4;)
            local.get 2
            local.set 3
            br 1 (;@3;)
          end
          local.get 1
          local.get 2
          call $__rust_alloc
          local.set 3
        end
        block  ;; label = @3
          local.get 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.store offset=4
          local.get 0
          i32.const 8
          i32.add
          local.get 1
          i32.store
          local.get 0
          i32.const 0
          i32.store
          return
        end
        local.get 0
        local.get 1
        i32.store offset=4
        local.get 0
        i32.const 8
        i32.add
        local.get 2
        i32.store
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 0
      i32.const 8
      i32.add
      i32.const 0
      i32.store
    end
    local.get 0
    i32.const 1
    i32.store)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::h7dbd839d9d266f73 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 4
        local.get 1
        i32.const 4
        i32.gt_u
        select
        local.tee 1
        i32.const 2
        i32.shl
        local.set 4
        local.get 1
        i32.const 536870912
        i32.lt_u
        i32.const 2
        i32.shl
        local.set 5
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 4
            i32.store offset=24
            local.get 2
            local.get 3
            i32.const 2
            i32.shl
            i32.store offset=20
            local.get 2
            local.get 0
            i32.load offset=4
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 4
        local.get 5
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h54895925b0829b77
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::hd4c9872e3ab1b4d0 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 4
        local.get 1
        i32.const 4
        i32.gt_u
        select
        local.tee 1
        i32.const 28
        i32.mul
        local.set 4
        local.get 1
        i32.const 76695845
        i32.lt_u
        i32.const 2
        i32.shl
        local.set 5
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 4
            i32.store offset=24
            local.get 2
            local.get 3
            i32.const 28
            i32.mul
            i32.store offset=20
            local.get 2
            local.get 0
            i32.load offset=4
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 4
        local.get 5
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h54895925b0829b77
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::hdef54ec21e34b448 (type 3) (param i32 i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 8
        local.get 1
        i32.const 8
        i32.gt_u
        select
        local.tee 1
        i32.const -1
        i32.xor
        i32.const 31
        i32.shr_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 1
            i32.store offset=24
            local.get 2
            local.get 3
            i32.store offset=20
            local.get 2
            local.get 0
            i32.load offset=4
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 1
        local.get 4
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h54895925b0829b77
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::he0d920f733adce36 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 4
        local.get 1
        i32.const 4
        i32.gt_u
        select
        local.tee 1
        i32.const 21
        i32.mul
        local.set 4
        local.get 1
        i32.const 102261127
        i32.lt_u
        local.set 5
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 1
            i32.store offset=24
            local.get 2
            local.get 3
            i32.const 21
            i32.mul
            i32.store offset=20
            local.get 2
            local.get 0
            i32.load offset=4
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 4
        local.get 5
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h54895925b0829b77
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::he790951035f87438 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 4
        local.get 1
        i32.const 4
        i32.gt_u
        select
        local.tee 1
        i32.const 65
        i32.mul
        local.set 4
        local.get 1
        i32.const 33038210
        i32.lt_u
        local.set 5
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 1
            i32.store offset=24
            local.get 2
            local.get 3
            i32.const 65
            i32.mul
            i32.store offset=20
            local.get 2
            local.get 0
            i32.load offset=4
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 4
        local.get 5
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h54895925b0829b77
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::hf0aaf4bc3c260b6c (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 4
        local.get 1
        i32.const 4
        i32.gt_u
        select
        local.tee 1
        i32.const 40
        i32.mul
        local.set 4
        local.get 1
        i32.const 53687092
        i32.lt_u
        i32.const 2
        i32.shl
        local.set 5
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 4
            i32.store offset=24
            local.get 2
            local.get 3
            i32.const 40
            i32.mul
            i32.store offset=20
            local.get 2
            local.get 0
            i32.load offset=4
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 4
        local.get 5
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h54895925b0829b77
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::h7f850581b740c533 (type 6) (param i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        local.get 2
        i32.add
        local.tee 2
        local.get 1
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 1
        i32.const 1
        i32.shl
        local.tee 4
        local.get 2
        local.get 4
        local.get 2
        i32.gt_u
        select
        local.tee 2
        i32.const 8
        local.get 2
        i32.const 8
        i32.gt_u
        select
        local.tee 2
        i32.const -1
        i32.xor
        i32.const 31
        i32.shr_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            i32.const 1
            i32.store offset=24
            local.get 3
            local.get 1
            i32.store offset=20
            local.get 3
            local.get 0
            i32.load offset=4
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 3
          i32.const 0
          i32.store offset=24
        end
        local.get 3
        local.get 2
        local.get 4
        local.get 3
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h54895925b0829b77
        local.get 3
        i32.load offset=4
        local.set 1
        block  ;; label = @3
          local.get 3
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 2
          i32.store
          local.get 0
          local.get 1
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 3
    i32.const 32
    i32.add
    global.set 0)
  (func $__rust_alloc (type 2) (param i32 i32) (result i32)
    (local i32)
    local.get 0
    local.get 1
    call $__rdl_alloc
    local.set 2
    local.get 2
    return)
  (func $__rust_dealloc (type 6) (param i32 i32 i32)
    local.get 0
    local.get 1
    local.get 2
    call $__rdl_dealloc
    return)
  (func $__rust_realloc (type 9) (param i32 i32 i32 i32) (result i32)
    (local i32)
    local.get 0
    local.get 1
    local.get 2
    local.get 3
    call $__rdl_realloc
    local.set 4
    local.get 4
    return)
  (func $__rust_alloc_error_handler (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $__rg_oom
    return)
  (func $memcpy (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    local.get 1
    local.get 2
    call $pbc_lib::mem::pbc_memmove::hec21f375e382335d)
  (func $<&T_as_core::fmt::Display>::fmt::hf14af99385a76015 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 1
    call $<str_as_core::fmt::Display>::fmt::h4e369854c5064a2e)
  (func $core::ops::function::FnOnce::call_once__vtable.shim__::h470ded2ab9fe471e (type 3) (param i32 i32)
    local.get 1
    local.get 1
    call $pbc_lib::exit::override_panic::__closure__::h16eb93735f5177dd)
  (func $pbc_lib::exit::override_panic::__closure__::h16eb93735f5177dd (type 3) (param i32 i32)
    (local i32 i32 i32 i64)
    global.get 0
    i32.const 96
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    i32.const 8
    i32.add
    local.get 1
    call $core::panic::panic_info::PanicInfo::payload::hbd7d38a06ae537e7
    local.get 2
    i32.load offset=8
    local.tee 3
    local.get 2
    i32.load offset=12
    i32.load offset=12
    local.tee 4
    call_indirect (type 1)
    local.set 5
    block  ;; label = @1
      block  ;; label = @2
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 5
        i64.const -8527728395957036344
        i64.ne
        br_if 0 (;@2;)
        local.get 2
        local.get 3
        i32.load
        i32.store offset=16
        local.get 3
        i32.load offset=4
        local.set 4
        br 1 (;@1;)
      end
      local.get 3
      local.get 4
      call_indirect (type 1)
      local.set 5
      block  ;; label = @2
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 5
        i64.const 359314774325844787
        i64.ne
        br_if 0 (;@2;)
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.set 4
        local.get 2
        local.get 3
        i32.const 4
        i32.add
        i32.load
        i32.store offset=16
        br 1 (;@1;)
      end
      local.get 2
      i32.const 1051740
      i32.store offset=16
      i32.const 0
      local.set 4
    end
    local.get 2
    local.get 4
    i32.store offset=20
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          call $core::panic::panic_info::PanicInfo::location::h29f7f91081e7b35d
          local.tee 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 2
          local.get 3
          i64.load align=4
          i64.store offset=40
          local.get 3
          i32.load offset=8
          local.set 3
          local.get 2
          i32.const 68
          i32.add
          i32.const 1
          i32.store
          local.get 2
          i32.const 12
          i32.store offset=60
          local.get 2
          local.get 3
          i32.store offset=36
          local.get 2
          local.get 2
          i32.const 40
          i32.add
          i32.store offset=56
          local.get 2
          local.get 2
          i32.const 36
          i32.add
          i32.store offset=64
          local.get 2
          i32.const 2
          i32.store offset=92
          local.get 2
          i32.const 2
          i32.store offset=84
          local.get 2
          i32.const 1051784
          i32.store offset=80
          local.get 2
          i32.const 0
          i32.store offset=72
          local.get 2
          local.get 2
          i32.const 56
          i32.add
          i32.store offset=88
          local.get 2
          i32.const 24
          i32.add
          local.get 2
          i32.const 72
          i32.add
          call $alloc::fmt::format::format_inner::h92d046d33b5f4798
          br 1 (;@2;)
        end
        i32.const 16
        i32.const 1
        call $__rust_alloc
        local.tee 3
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        i32.const 0
        i64.load offset=1051764 align=1
        i64.store align=1
        local.get 3
        i32.const 8
        i32.add
        i32.const 0
        i64.load offset=1051772 align=1
        i64.store align=1
        local.get 2
        local.get 3
        i32.store offset=28
        local.get 2
        i32.const 16
        i32.store offset=24
        local.get 2
        i32.const 16
        i32.store offset=32
      end
      local.get 2
      i32.const 68
      i32.add
      i32.const 12
      i32.store
      local.get 2
      i32.const 13
      i32.store offset=60
      local.get 2
      local.get 2
      i32.const 16
      i32.add
      i32.store offset=64
      local.get 2
      local.get 2
      i32.const 24
      i32.add
      i32.store offset=56
      local.get 2
      i32.const 2
      i32.store offset=92
      local.get 2
      i32.const 2
      i32.store offset=84
      local.get 2
      i32.const 1051804
      i32.store offset=80
      local.get 2
      i32.const 0
      i32.store offset=72
      local.get 2
      local.get 2
      i32.const 56
      i32.add
      i32.store offset=88
      local.get 2
      i32.const 40
      i32.add
      local.get 2
      i32.const 72
      i32.add
      call $alloc::fmt::format::format_inner::h92d046d33b5f4798
      local.get 2
      i32.load offset=44
      local.tee 3
      local.get 2
      i32.load offset=48
      call $pbc_lib::exit::pbc_exit::h41aa82034cfaf2b3
      block  ;; label = @2
        local.get 2
        i32.load offset=40
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 1
        i32.const 1
        call $__rust_dealloc
      end
      block  ;; label = @2
        local.get 2
        i32.load offset=24
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 2
        i32.load offset=28
        local.get 3
        i32.const 1
        call $__rust_dealloc
      end
      local.get 2
      i32.const 96
      i32.add
      global.set 0
      return
    end
    i32.const 16
    i32.const 1
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $core::ptr::drop_in_place<pbc_lib::exit::override_panic::__closure__>::h021e10777ad761e0 (type 0) (param i32))
  (func $<alloc::string::String_as_core::fmt::Display>::fmt::h3e52ce3ad8d356c1 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.const 4
    i32.add
    i32.load
    local.get 0
    i32.const 8
    i32.add
    i32.load
    local.get 1
    call $<str_as_core::fmt::Display>::fmt::h4e369854c5064a2e)
  (func $wasm_exit (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $pbc_lib::exit::pbc_exit::h41aa82034cfaf2b3)
  (func $pbc_lib::exit::override_panic::hf8dda51c95006fec (type 10)
    i32.const 1
    i32.const 1051740
    call $std::panicking::set_hook::h9a45cfeeb8c46eb6)
  (func $alloc::raw_vec::finish_grow::hf3f61f46f717d4b9 (type 8) (param i32 i32 i32 i32)
    (local i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  i32.const 0
                  i32.lt_s
                  br_if 0 (;@7;)
                  local.get 3
                  i32.load offset=8
                  i32.eqz
                  br_if 2 (;@5;)
                  local.get 3
                  i32.load offset=4
                  local.tee 4
                  br_if 1 (;@6;)
                  local.get 1
                  br_if 3 (;@4;)
                  local.get 2
                  local.set 3
                  br 4 (;@3;)
                end
                local.get 0
                i32.const 8
                i32.add
                i32.const 0
                i32.store
                br 5 (;@1;)
              end
              local.get 3
              i32.load
              local.get 4
              local.get 2
              local.get 1
              call $__rust_realloc
              local.set 3
              br 2 (;@3;)
            end
            local.get 1
            br_if 0 (;@4;)
            local.get 2
            local.set 3
            br 1 (;@3;)
          end
          local.get 1
          local.get 2
          call $__rust_alloc
          local.set 3
        end
        block  ;; label = @3
          local.get 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.store offset=4
          local.get 0
          i32.const 8
          i32.add
          local.get 1
          i32.store
          local.get 0
          i32.const 0
          i32.store
          return
        end
        local.get 0
        local.get 1
        i32.store offset=4
        local.get 0
        i32.const 8
        i32.add
        local.get 2
        i32.store
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 0
      i32.const 8
      i32.add
      i32.const 0
      i32.store
    end
    local.get 0
    i32.const 1
    i32.store)
  (func $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e (type 6) (param i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        local.get 2
        i32.add
        local.tee 2
        local.get 1
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 1
        i32.const 1
        i32.shl
        local.tee 4
        local.get 2
        local.get 4
        local.get 2
        i32.gt_u
        select
        local.tee 2
        i32.const 8
        local.get 2
        i32.const 8
        i32.gt_u
        select
        local.tee 2
        i32.const -1
        i32.xor
        i32.const 31
        i32.shr_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            i32.const 1
            i32.store offset=24
            local.get 3
            local.get 1
            i32.store offset=20
            local.get 3
            local.get 0
            i32.const 4
            i32.add
            i32.load
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 3
          i32.const 0
          i32.store offset=24
        end
        local.get 3
        local.get 2
        local.get 4
        local.get 3
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::hf3f61f46f717d4b9
        local.get 3
        i32.load offset=4
        local.set 1
        block  ;; label = @3
          local.get 3
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 2
          i32.store
          local.get 0
          i32.const 4
          i32.add
          local.get 1
          i32.store
          br 2 (;@1;)
        end
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 3
    i32.const 32
    i32.add
    global.set 0)
  (func $<pbc_contract_common::events::Interaction_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::hc4e9ecfb67c83e42 (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    i32.const 16777986
    local.get 1
    i32.load8_u offset=28
    i32.const 3
    i32.shl
    i32.const 16
    i32.xor
    i32.const 248
    i32.and
    i32.shr_u
    local.set 4
    block  ;; label = @1
      local.get 2
      i32.load
      local.tee 5
      local.get 2
      i32.load offset=8
      local.tee 6
      i32.ne
      br_if 0 (;@1;)
      local.get 2
      local.get 6
      i32.const 1
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
      local.get 2
      i32.load
      local.set 5
      local.get 2
      i32.load offset=8
      local.set 6
    end
    local.get 2
    local.get 6
    i32.const 1
    i32.add
    local.tee 7
    i32.store offset=8
    local.get 2
    i32.load offset=4
    local.tee 8
    local.get 6
    i32.add
    local.get 4
    i32.store8
    block  ;; label = @1
      local.get 5
      local.get 7
      i32.sub
      i32.const 19
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 7
      i32.const 20
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
      local.get 2
      i32.load offset=4
      local.set 8
      local.get 2
      i32.load offset=8
      local.set 7
    end
    local.get 8
    local.get 7
    i32.add
    local.tee 6
    i32.const 16
    i32.add
    local.get 1
    i32.const 45
    i32.add
    i32.load align=1
    i32.store align=1
    local.get 6
    i32.const 8
    i32.add
    local.get 1
    i32.const 37
    i32.add
    i64.load align=1
    i64.store align=1
    local.get 6
    local.get 1
    i32.const 29
    i32.add
    i64.load align=1
    i64.store align=1
    local.get 2
    local.get 7
    i32.const 20
    i32.add
    local.tee 6
    i32.store offset=8
    local.get 1
    i32.const 24
    i32.add
    i32.load
    local.set 8
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 6
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 6
      i32.const 4
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
      local.get 2
      i32.load offset=8
      local.set 6
    end
    local.get 2
    local.get 6
    i32.const 4
    i32.add
    local.tee 7
    i32.store offset=8
    local.get 2
    i32.load offset=4
    local.tee 9
    local.get 6
    i32.add
    local.get 8
    i32.const 24
    i32.shl
    local.get 8
    i32.const 8
    i32.shl
    i32.const 16711680
    i32.and
    i32.or
    local.get 8
    i32.const 8
    i32.shr_u
    i32.const 65280
    i32.and
    local.get 8
    i32.const 24
    i32.shr_u
    i32.or
    i32.or
    i32.store align=1
    block  ;; label = @1
      local.get 8
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 20
      i32.add
      i32.load
      local.set 6
      loop  ;; label = @2
        local.get 6
        i32.load8_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load
            local.get 7
            i32.eq
            br_if 0 (;@4;)
            local.get 7
            local.set 5
            br 1 (;@3;)
          end
          local.get 2
          local.get 7
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=4
          local.set 9
          local.get 2
          i32.load offset=8
          local.set 5
        end
        local.get 2
        local.get 5
        i32.const 1
        i32.add
        local.tee 7
        i32.store offset=8
        local.get 9
        local.get 5
        i32.add
        local.get 4
        i32.store8
        local.get 6
        i32.const 1
        i32.add
        local.set 6
        local.get 8
        i32.const -1
        i32.add
        local.tee 8
        br_if 0 (;@2;)
      end
    end
    local.get 1
    i32.load8_u offset=49
    local.set 6
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 7
      i32.ne
      br_if 0 (;@1;)
      local.get 2
      local.get 7
      i32.const 1
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
      local.get 2
      i32.load offset=8
      local.set 7
    end
    local.get 2
    local.get 7
    i32.const 1
    i32.add
    i32.store offset=8
    local.get 2
    i32.load offset=4
    local.get 7
    i32.add
    local.get 6
    i32.store8
    local.get 3
    i32.const 8
    i32.add
    local.get 1
    local.get 2
    call $<core::option::Option<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h15097e10f262f21c
    block  ;; label = @1
      block  ;; label = @2
        local.get 3
        i64.load8_u offset=8
        local.tee 10
        i64.const 4
        i64.ne
        br_if 0 (;@2;)
        local.get 0
        i32.const 4
        i32.store8
        br 1 (;@1;)
      end
      local.get 0
      local.get 3
      i64.load32_u offset=9 align=1
      local.get 3
      i64.load16_u offset=13 align=1
      local.get 3
      i64.load8_u offset=15
      i64.const 16
      i64.shl
      i64.or
      i64.const 32
      i64.shl
      i64.or
      i64.const 8
      i64.shl
      local.get 10
      i64.or
      i64.store align=4
    end
    local.get 3
    i32.const 16
    i32.add
    global.set 0)
  (func $<pbc_contract_common::events::EventGroup_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::hb1314657eef35dc6 (type 6) (param i32 i32 i32)
    (local i32 i64 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    i32.const 8
    i32.add
    local.get 1
    i32.const 16
    i32.add
    local.get 2
    call $<core::option::Option<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h2c0212f98c5f2f6e
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 3
              i64.load8_u offset=8
              local.tee 4
              i64.const 4
              i64.ne
              br_if 0 (;@5;)
              local.get 3
              i32.const 8
              i32.add
              local.get 1
              local.get 2
              call $<core::option::Option<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h15097e10f262f21c
              local.get 3
              i64.load8_u offset=8
              local.tee 4
              i64.const 4
              i64.ne
              br_if 2 (;@3;)
              local.get 1
              i32.const 48
              i32.add
              i32.load
              local.set 5
              block  ;; label = @6
                local.get 2
                i32.load
                local.get 2
                i32.load offset=8
                local.tee 6
                i32.sub
                i32.const 3
                i32.gt_u
                br_if 0 (;@6;)
                local.get 2
                local.get 6
                i32.const 4
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                local.get 2
                i32.load offset=8
                local.set 6
              end
              local.get 2
              local.get 6
              i32.const 4
              i32.add
              i32.store offset=8
              local.get 2
              i32.load offset=4
              local.get 6
              i32.add
              local.get 5
              i32.const 24
              i32.shl
              local.get 5
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 5
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 5
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              i32.store align=1
              block  ;; label = @6
                local.get 5
                i32.eqz
                br_if 0 (;@6;)
                local.get 1
                i32.const 44
                i32.add
                i32.load
                local.set 6
                local.get 5
                i32.const 56
                i32.mul
                local.set 5
                loop  ;; label = @7
                  local.get 3
                  local.get 6
                  local.get 2
                  call $<pbc_contract_common::events::Interaction_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::hc4e9ecfb67c83e42
                  local.get 3
                  i32.load8_u
                  i32.const 4
                  i32.ne
                  br_if 3 (;@4;)
                  local.get 6
                  i32.const 56
                  i32.add
                  local.set 6
                  local.get 5
                  i32.const -56
                  i32.add
                  local.tee 5
                  br_if 0 (;@7;)
                end
              end
              local.get 3
              i32.const 8
              i32.add
              local.get 1
              i32.const 28
              i32.add
              local.get 2
              call $<core::option::Option<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h8a834961de5d8dba
              local.get 3
              i64.load8_u offset=8
              local.tee 4
              i64.const 4
              i64.ne
              br_if 3 (;@2;)
              local.get 0
              i32.const 4
              i32.store8
              br 4 (;@1;)
            end
            local.get 0
            local.get 3
            i64.load32_u offset=9 align=1
            local.get 3
            i64.load16_u offset=13 align=1
            local.get 3
            i64.load8_u offset=15
            i64.const 16
            i64.shl
            i64.or
            i64.const 32
            i64.shl
            i64.or
            i64.const 8
            i64.shl
            local.get 4
            i64.or
            i64.store align=4
            br 3 (;@1;)
          end
          local.get 3
          local.get 3
          i64.load
          i64.store offset=8
          i32.const 1051820
          i32.const 43
          local.get 3
          i32.const 8
          i32.add
          i32.const 1051864
          i32.const 1051988
          call $core::result::unwrap_failed::h2b47cc7f7e98a508
          unreachable
        end
        local.get 0
        local.get 3
        i64.load32_u offset=9 align=1
        local.get 3
        i64.load16_u offset=13 align=1
        local.get 3
        i64.load8_u offset=15
        i64.const 16
        i64.shl
        i64.or
        i64.const 32
        i64.shl
        i64.or
        i64.const 8
        i64.shl
        local.get 4
        i64.or
        i64.store align=4
        br 1 (;@1;)
      end
      local.get 0
      local.get 3
      i64.load32_u offset=9 align=1
      local.get 3
      i64.load16_u offset=13 align=1
      local.get 3
      i64.load8_u offset=15
      i64.const 16
      i64.shl
      i64.or
      i64.const 32
      i64.shl
      i64.or
      i64.const 8
      i64.shl
      local.get 4
      i64.or
      i64.store align=4
    end
    local.get 3
    i32.const 16
    i32.add
    global.set 0)
  (func $core::ptr::drop_in_place<std::io::error::Error>::h9307e7ae4e21f669 (type 0) (param i32)
    (local i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load8_u
      i32.const 3
      i32.ne
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=4
      local.tee 1
      i32.load
      local.get 1
      i32.load offset=4
      i32.load
      call_indirect (type 0)
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 2
        i32.const 4
        i32.add
        i32.load
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load
        local.get 3
        local.get 2
        i32.const 8
        i32.add
        i32.load
        call $__rust_dealloc
      end
      local.get 0
      i32.load offset=4
      i32.const 12
      i32.const 4
      call $__rust_dealloc
    end)
  (func $<alloc::vec::Vec<T_A>_as_core::ops::drop::Drop>::drop::h8bb281a5ef22bb5f (type 0) (param i32)
    (local i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=8
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=4
      local.tee 2
      local.get 1
      i32.const 28
      i32.mul
      i32.add
      local.set 3
      loop  ;; label = @2
        local.get 2
        local.tee 4
        i32.const 28
        i32.add
        local.set 2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 4
                  i32.load8_u
                  br_table 1 (;@6;) 4 (;@3;) 4 (;@3;) 4 (;@3;) 2 (;@5;) 3 (;@4;) 4 (;@3;) 0 (;@7;)
                end
                local.get 4
                i32.load offset=4
                local.tee 0
                i32.eqz
                br_if 3 (;@3;)
                local.get 4
                i32.const 8
                i32.add
                i32.load
                local.get 0
                i32.const 1
                call $__rust_dealloc
                br 3 (;@3;)
              end
              local.get 4
              i32.const 8
              i32.add
              local.set 5
              block  ;; label = @6
                local.get 4
                i32.const 12
                i32.add
                i32.load
                local.tee 1
                i32.eqz
                br_if 0 (;@6;)
                local.get 5
                i32.load
                local.set 0
                local.get 1
                i32.const 12
                i32.mul
                local.set 1
                loop  ;; label = @7
                  block  ;; label = @8
                    local.get 0
                    i32.load
                    local.tee 6
                    i32.eqz
                    br_if 0 (;@8;)
                    local.get 0
                    i32.const 4
                    i32.add
                    i32.load
                    local.get 6
                    i32.const 1
                    call $__rust_dealloc
                  end
                  local.get 0
                  i32.const 12
                  i32.add
                  local.set 0
                  local.get 1
                  i32.const -12
                  i32.add
                  local.tee 1
                  br_if 0 (;@7;)
                end
              end
              block  ;; label = @6
                local.get 4
                i32.load offset=4
                local.tee 0
                i32.eqz
                br_if 0 (;@6;)
                local.get 5
                i32.load
                local.get 0
                i32.const 12
                i32.mul
                i32.const 4
                call $__rust_dealloc
              end
              local.get 4
              i32.const 20
              i32.add
              local.set 5
              block  ;; label = @6
                local.get 4
                i32.const 24
                i32.add
                i32.load
                local.tee 1
                i32.eqz
                br_if 0 (;@6;)
                local.get 5
                i32.load
                local.set 0
                local.get 1
                i32.const 12
                i32.mul
                local.set 1
                loop  ;; label = @7
                  block  ;; label = @8
                    local.get 0
                    i32.load
                    local.tee 6
                    i32.eqz
                    br_if 0 (;@8;)
                    local.get 0
                    i32.const 4
                    i32.add
                    i32.load
                    local.get 6
                    i32.const 1
                    call $__rust_dealloc
                  end
                  local.get 0
                  i32.const 12
                  i32.add
                  local.set 0
                  local.get 1
                  i32.const -12
                  i32.add
                  local.tee 1
                  br_if 0 (;@7;)
                end
              end
              local.get 4
              i32.load offset=16
              local.tee 0
              i32.eqz
              br_if 2 (;@3;)
              local.get 5
              i32.load
              local.get 0
              i32.const 12
              i32.mul
              i32.const 4
              call $__rust_dealloc
              br 2 (;@3;)
            end
            local.get 4
            i32.load offset=4
            local.tee 0
            i32.eqz
            br_if 1 (;@3;)
            local.get 4
            i32.const 8
            i32.add
            i32.load
            local.get 0
            i32.const 2
            i32.shl
            i32.const 4
            call $__rust_dealloc
            br 1 (;@3;)
          end
          local.get 4
          i32.load offset=4
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 4
          i32.const 8
          i32.add
          i32.load
          local.get 0
          i32.const 2
          i32.shl
          i32.const 4
          call $__rust_dealloc
        end
        local.get 2
        local.get 3
        i32.ne
        br_if 0 (;@2;)
      end
    end)
  (func $<alloc::vec::Vec<T_A>_as_core::ops::drop::Drop>::drop::h93aca5bcf8cb086a (type 0) (param i32)
    (local i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=8
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=4
      local.tee 0
      local.get 1
      i32.const 56
      i32.mul
      i32.add
      local.set 2
      loop  ;; label = @2
        block  ;; label = @3
          local.get 0
          local.tee 3
          i32.const 20
          i32.add
          i32.load
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          i32.load offset=16
          local.tee 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.const 1
          call $__rust_dealloc
        end
        local.get 3
        i32.const 44
        i32.add
        local.set 4
        block  ;; label = @3
          local.get 3
          i32.const 48
          i32.add
          i32.load
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          i32.const 56
          i32.mul
          local.set 1
          local.get 4
          i32.load
          i32.const 20
          i32.add
          local.set 0
          loop  ;; label = @4
            block  ;; label = @5
              local.get 0
              i32.const -4
              i32.add
              i32.load
              local.tee 5
              i32.eqz
              br_if 0 (;@5;)
              local.get 0
              i32.load
              local.get 5
              i32.const 1
              call $__rust_dealloc
            end
            local.get 0
            i32.const 56
            i32.add
            local.set 0
            local.get 1
            i32.const -56
            i32.add
            local.tee 1
            br_if 0 (;@4;)
          end
        end
        block  ;; label = @3
          local.get 3
          i32.load offset=40
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 4
          i32.load
          local.get 0
          i32.const 56
          i32.mul
          i32.const 8
          call $__rust_dealloc
        end
        local.get 3
        i32.const 56
        i32.add
        local.set 0
        block  ;; label = @3
          local.get 3
          i32.const 32
          i32.add
          i32.load
          local.tee 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          i32.load offset=28
          local.tee 5
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          local.get 5
          i32.const 1
          call $__rust_dealloc
        end
        local.get 0
        local.get 2
        i32.ne
        br_if 0 (;@2;)
      end
    end)
  (func $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::hf6050eee554f24b3 (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    local.get 1
    i32.const 8
    i32.add
    i32.load
    local.set 3
    block  ;; label = @1
      local.get 2
      i32.load
      local.get 2
      i32.load offset=8
      local.tee 4
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 2
      local.get 4
      i32.const 4
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
      local.get 2
      i32.load offset=8
      local.set 4
    end
    local.get 2
    local.get 4
    i32.const 4
    i32.add
    local.tee 5
    i32.store offset=8
    local.get 2
    i32.load offset=4
    local.get 4
    i32.add
    local.get 3
    i32.const 24
    i32.shl
    local.get 3
    i32.const 8
    i32.shl
    i32.const 16711680
    i32.and
    i32.or
    local.get 3
    i32.const 8
    i32.shr_u
    i32.const 65280
    i32.and
    local.get 3
    i32.const 24
    i32.shr_u
    i32.or
    i32.or
    i32.store align=1
    block  ;; label = @1
      local.get 3
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 4
      i32.add
      i32.load
      local.tee 6
      local.get 3
      i32.const 12
      i32.mul
      i32.add
      local.set 7
      loop  ;; label = @2
        local.get 6
        i32.const 8
        i32.add
        i32.load
        local.set 1
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load
            local.get 5
            i32.sub
            i32.const 3
            i32.le_u
            br_if 0 (;@4;)
            local.get 5
            local.set 3
            br 1 (;@3;)
          end
          local.get 2
          local.get 5
          i32.const 4
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=8
          local.set 3
        end
        local.get 2
        local.get 3
        i32.const 4
        i32.add
        local.tee 5
        i32.store offset=8
        local.get 2
        i32.load offset=4
        local.tee 8
        local.get 3
        i32.add
        local.get 1
        i32.const 24
        i32.shl
        local.get 1
        i32.const 8
        i32.shl
        i32.const 16711680
        i32.and
        i32.or
        local.get 1
        i32.const 8
        i32.shr_u
        i32.const 65280
        i32.and
        local.get 1
        i32.const 24
        i32.shr_u
        i32.or
        i32.or
        i32.store align=1
        block  ;; label = @3
          local.get 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 6
          i32.const 4
          i32.add
          i32.load
          local.set 3
          loop  ;; label = @4
            local.get 3
            i32.load8_u
            local.set 9
            block  ;; label = @5
              block  ;; label = @6
                local.get 2
                i32.load
                local.get 5
                i32.eq
                br_if 0 (;@6;)
                local.get 5
                local.set 4
                br 1 (;@5;)
              end
              local.get 2
              local.get 5
              i32.const 1
              call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
              local.get 2
              i32.load offset=4
              local.set 8
              local.get 2
              i32.load offset=8
              local.set 4
            end
            local.get 2
            local.get 4
            i32.const 1
            i32.add
            local.tee 5
            i32.store offset=8
            local.get 8
            local.get 4
            i32.add
            local.get 9
            i32.store8
            local.get 3
            i32.const 1
            i32.add
            local.set 3
            local.get 1
            i32.const -1
            i32.add
            local.tee 1
            br_if 0 (;@4;)
          end
        end
        local.get 6
        i32.const 12
        i32.add
        local.tee 6
        local.get 7
        i32.ne
        br_if 0 (;@2;)
      end
    end
    local.get 0
    i32.const 4
    i32.store8)
  (func $<pbc_contract_common::zk::ZkStateChange_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::he31ef6701d79cef6 (type 6) (param i32 i32 i32)
    (local i32 i32 i64 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 1
                        i32.load8_u
                        br_table 0 (;@10;) 1 (;@9;) 2 (;@8;) 3 (;@7;) 4 (;@6;) 8 (;@2;) 7 (;@3;) 6 (;@4;) 0 (;@10;)
                      end
                      block  ;; label = @10
                        local.get 2
                        i32.load
                        local.get 2
                        i32.load offset=8
                        local.tee 4
                        i32.ne
                        br_if 0 (;@10;)
                        local.get 2
                        local.get 4
                        i32.const 1
                        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                        local.get 2
                        i32.load offset=8
                        local.set 4
                      end
                      local.get 2
                      local.get 4
                      i32.const 1
                      i32.add
                      i32.store offset=8
                      local.get 2
                      i32.load offset=4
                      local.get 4
                      i32.add
                      i32.const 8
                      i32.store8
                      local.get 3
                      i32.const 8
                      i32.add
                      local.get 1
                      i32.const 4
                      i32.add
                      local.get 2
                      call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::hf6050eee554f24b3
                      local.get 3
                      i64.load8_u offset=8
                      local.tee 5
                      i64.const 4
                      i64.ne
                      br_if 4 (;@5;)
                      local.get 0
                      local.get 1
                      i32.const 16
                      i32.add
                      local.get 2
                      call $<alloc::vec::Vec<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::hf6050eee554f24b3
                      br 8 (;@1;)
                    end
                    block  ;; label = @9
                      local.get 2
                      i32.load
                      local.get 2
                      i32.load offset=8
                      local.tee 4
                      i32.ne
                      br_if 0 (;@9;)
                      local.get 2
                      local.get 4
                      i32.const 1
                      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                      local.get 2
                      i32.load offset=8
                      local.set 4
                    end
                    local.get 2
                    i32.load offset=4
                    local.get 4
                    i32.add
                    i32.const 1
                    i32.store8
                    local.get 2
                    local.get 4
                    i32.const 1
                    i32.add
                    local.tee 6
                    i32.store offset=8
                    local.get 1
                    i32.load offset=4
                    local.set 4
                    block  ;; label = @9
                      local.get 2
                      i32.load
                      local.get 6
                      i32.sub
                      i32.const 3
                      i32.gt_u
                      br_if 0 (;@9;)
                      local.get 2
                      local.get 6
                      i32.const 4
                      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                      local.get 2
                      i32.load offset=8
                      local.set 6
                    end
                    local.get 0
                    i32.const 4
                    i32.store8
                    local.get 2
                    local.get 6
                    i32.const 4
                    i32.add
                    i32.store offset=8
                    local.get 2
                    i32.load offset=4
                    local.get 6
                    i32.add
                    local.get 4
                    i32.const 24
                    i32.shl
                    local.get 4
                    i32.const 8
                    i32.shl
                    i32.const 16711680
                    i32.and
                    i32.or
                    local.get 4
                    i32.const 8
                    i32.shr_u
                    i32.const 65280
                    i32.and
                    local.get 4
                    i32.const 24
                    i32.shr_u
                    i32.or
                    i32.or
                    i32.store align=1
                    br 7 (;@1;)
                  end
                  block  ;; label = @8
                    local.get 2
                    i32.load
                    local.get 2
                    i32.load offset=8
                    local.tee 4
                    i32.ne
                    br_if 0 (;@8;)
                    local.get 2
                    local.get 4
                    i32.const 1
                    call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                    local.get 2
                    i32.load offset=8
                    local.set 4
                  end
                  local.get 2
                  i32.load offset=4
                  local.get 4
                  i32.add
                  i32.const 2
                  i32.store8
                  local.get 2
                  local.get 4
                  i32.const 1
                  i32.add
                  local.tee 6
                  i32.store offset=8
                  local.get 1
                  i32.load offset=24
                  local.set 4
                  block  ;; label = @8
                    local.get 2
                    i32.load
                    local.get 6
                    i32.sub
                    i32.const 3
                    i32.gt_u
                    br_if 0 (;@8;)
                    local.get 2
                    local.get 6
                    i32.const 4
                    call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                    local.get 2
                    i32.load offset=8
                    local.set 6
                  end
                  local.get 2
                  i32.load offset=4
                  local.get 6
                  i32.add
                  local.get 4
                  i32.const 24
                  i32.shl
                  local.get 4
                  i32.const 8
                  i32.shl
                  i32.const 16711680
                  i32.and
                  i32.or
                  local.get 4
                  i32.const 8
                  i32.shr_u
                  i32.const 65280
                  i32.and
                  local.get 4
                  i32.const 24
                  i32.shr_u
                  i32.or
                  i32.or
                  i32.store align=1
                  local.get 2
                  local.get 6
                  i32.const 4
                  i32.add
                  local.tee 4
                  i32.store offset=8
                  i32.const 16777986
                  local.get 1
                  i32.load8_u offset=1
                  i32.const 3
                  i32.shl
                  i32.const 16
                  i32.xor
                  i32.const 248
                  i32.and
                  i32.shr_u
                  local.set 7
                  block  ;; label = @8
                    local.get 2
                    i32.load
                    local.tee 8
                    local.get 4
                    i32.ne
                    br_if 0 (;@8;)
                    local.get 2
                    local.get 4
                    i32.const 1
                    call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                    local.get 2
                    i32.load
                    local.set 8
                    local.get 2
                    i32.load offset=8
                    local.set 4
                  end
                  local.get 2
                  local.get 4
                  i32.const 1
                  i32.add
                  local.tee 6
                  i32.store offset=8
                  local.get 2
                  i32.load offset=4
                  local.tee 9
                  local.get 4
                  i32.add
                  local.get 7
                  i32.store8
                  block  ;; label = @8
                    local.get 8
                    local.get 6
                    i32.sub
                    i32.const 19
                    i32.gt_u
                    br_if 0 (;@8;)
                    local.get 2
                    local.get 6
                    i32.const 20
                    call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                    local.get 2
                    i32.load offset=4
                    local.set 9
                    local.get 2
                    i32.load offset=8
                    local.set 6
                  end
                  local.get 0
                  i32.const 4
                  i32.store8
                  local.get 2
                  local.get 6
                  i32.const 20
                  i32.add
                  i32.store offset=8
                  local.get 9
                  local.get 6
                  i32.add
                  local.tee 2
                  i32.const 16
                  i32.add
                  local.get 1
                  i32.const 18
                  i32.add
                  i32.load align=1
                  i32.store align=1
                  local.get 2
                  i32.const 8
                  i32.add
                  local.get 1
                  i32.const 10
                  i32.add
                  i64.load align=1
                  i64.store align=1
                  local.get 2
                  local.get 1
                  i32.const 2
                  i32.add
                  i64.load align=1
                  i64.store align=1
                  br 6 (;@1;)
                end
                block  ;; label = @7
                  local.get 2
                  i32.load
                  local.get 2
                  i32.load offset=8
                  local.tee 4
                  i32.ne
                  br_if 0 (;@7;)
                  local.get 2
                  local.get 4
                  i32.const 1
                  call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                  local.get 2
                  i32.load offset=8
                  local.set 4
                end
                local.get 2
                i32.load offset=4
                local.get 4
                i32.add
                i32.const 3
                i32.store8
                local.get 2
                local.get 4
                i32.const 1
                i32.add
                local.tee 6
                i32.store offset=8
                local.get 1
                i32.load offset=4
                local.set 4
                block  ;; label = @7
                  local.get 2
                  i32.load
                  local.get 6
                  i32.sub
                  i32.const 3
                  i32.gt_u
                  br_if 0 (;@7;)
                  local.get 2
                  local.get 6
                  i32.const 4
                  call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                  local.get 2
                  i32.load offset=8
                  local.set 6
                end
                local.get 0
                i32.const 4
                i32.store8
                local.get 2
                local.get 6
                i32.const 4
                i32.add
                i32.store offset=8
                local.get 2
                i32.load offset=4
                local.get 6
                i32.add
                local.get 4
                i32.const 24
                i32.shl
                local.get 4
                i32.const 8
                i32.shl
                i32.const 16711680
                i32.and
                i32.or
                local.get 4
                i32.const 8
                i32.shr_u
                i32.const 65280
                i32.and
                local.get 4
                i32.const 24
                i32.shr_u
                i32.or
                i32.or
                i32.store align=1
                br 5 (;@1;)
              end
              block  ;; label = @6
                local.get 2
                i32.load
                local.get 2
                i32.load offset=8
                local.tee 4
                i32.ne
                br_if 0 (;@6;)
                local.get 2
                local.get 4
                i32.const 1
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                local.get 2
                i32.load offset=8
                local.set 4
              end
              local.get 2
              i32.load offset=4
              local.get 4
              i32.add
              i32.const 4
              i32.store8
              local.get 2
              local.get 4
              i32.const 1
              i32.add
              local.tee 8
              i32.store offset=8
              local.get 1
              i32.const 12
              i32.add
              i32.load
              local.set 4
              block  ;; label = @6
                local.get 2
                i32.load
                local.get 8
                i32.sub
                i32.const 3
                i32.gt_u
                br_if 0 (;@6;)
                local.get 2
                local.get 8
                i32.const 4
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                local.get 2
                i32.load offset=8
                local.set 8
              end
              local.get 2
              local.get 8
              i32.const 4
              i32.add
              local.tee 6
              i32.store offset=8
              local.get 2
              i32.load offset=4
              local.tee 7
              local.get 8
              i32.add
              local.get 4
              i32.const 24
              i32.shl
              local.get 4
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 4
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 4
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              i32.store align=1
              block  ;; label = @6
                local.get 4
                i32.eqz
                br_if 0 (;@6;)
                local.get 1
                i32.const 8
                i32.add
                i32.load
                local.set 1
                local.get 4
                i32.const 2
                i32.shl
                local.set 9
                loop  ;; label = @7
                  local.get 1
                  i32.load
                  local.set 4
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 2
                      i32.load
                      local.get 6
                      i32.sub
                      i32.const 3
                      i32.le_u
                      br_if 0 (;@9;)
                      local.get 6
                      local.set 8
                      br 1 (;@8;)
                    end
                    local.get 2
                    local.get 6
                    i32.const 4
                    call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                    local.get 2
                    i32.load offset=4
                    local.set 7
                    local.get 2
                    i32.load offset=8
                    local.set 8
                  end
                  local.get 2
                  local.get 8
                  i32.const 4
                  i32.add
                  local.tee 6
                  i32.store offset=8
                  local.get 7
                  local.get 8
                  i32.add
                  local.get 4
                  i32.const 24
                  i32.shl
                  local.get 4
                  i32.const 8
                  i32.shl
                  i32.const 16711680
                  i32.and
                  i32.or
                  local.get 4
                  i32.const 8
                  i32.shr_u
                  i32.const 65280
                  i32.and
                  local.get 4
                  i32.const 24
                  i32.shr_u
                  i32.or
                  i32.or
                  i32.store align=1
                  local.get 1
                  i32.const 4
                  i32.add
                  local.set 1
                  local.get 9
                  i32.const -4
                  i32.add
                  local.tee 9
                  br_if 0 (;@7;)
                end
              end
              local.get 0
              i32.const 4
              i32.store8
              br 4 (;@1;)
            end
            local.get 0
            local.get 3
            i64.load32_u offset=9 align=1
            local.get 3
            i64.load16_u offset=13 align=1
            local.get 3
            i64.load8_u offset=15
            i64.const 16
            i64.shl
            i64.or
            i64.const 32
            i64.shl
            i64.or
            i64.const 8
            i64.shl
            local.get 5
            i64.or
            i64.store align=4
            br 3 (;@1;)
          end
          block  ;; label = @4
            local.get 2
            i32.load
            local.get 2
            i32.load offset=8
            local.tee 4
            i32.ne
            br_if 0 (;@4;)
            local.get 2
            local.get 4
            i32.const 1
            call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
            local.get 2
            i32.load offset=8
            local.set 4
          end
          local.get 2
          i32.load offset=4
          local.get 4
          i32.add
          i32.const 7
          i32.store8
          local.get 2
          local.get 4
          i32.const 1
          i32.add
          local.tee 6
          i32.store offset=8
          local.get 1
          i32.const 12
          i32.add
          i32.load
          local.set 8
          block  ;; label = @4
            local.get 2
            i32.load
            local.get 6
            i32.sub
            i32.const 3
            i32.gt_u
            br_if 0 (;@4;)
            local.get 2
            local.get 6
            i32.const 4
            call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
            local.get 2
            i32.load offset=8
            local.set 6
          end
          local.get 2
          local.get 6
          i32.const 4
          i32.add
          local.tee 4
          i32.store offset=8
          local.get 2
          i32.load offset=4
          local.tee 7
          local.get 6
          i32.add
          local.get 8
          i32.const 24
          i32.shl
          local.get 8
          i32.const 8
          i32.shl
          i32.const 16711680
          i32.and
          i32.or
          local.get 8
          i32.const 8
          i32.shr_u
          i32.const 65280
          i32.and
          local.get 8
          i32.const 24
          i32.shr_u
          i32.or
          i32.or
          i32.store align=1
          block  ;; label = @4
            local.get 8
            i32.eqz
            br_if 0 (;@4;)
            local.get 1
            i32.const 8
            i32.add
            i32.load
            local.set 6
            loop  ;; label = @5
              local.get 6
              i32.load8_u
              local.set 9
              block  ;; label = @6
                block  ;; label = @7
                  local.get 2
                  i32.load
                  local.get 4
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 4
                  local.set 1
                  br 1 (;@6;)
                end
                local.get 2
                local.get 4
                i32.const 1
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                local.get 2
                i32.load offset=4
                local.set 7
                local.get 2
                i32.load offset=8
                local.set 1
              end
              local.get 2
              local.get 1
              i32.const 1
              i32.add
              local.tee 4
              i32.store offset=8
              local.get 7
              local.get 1
              i32.add
              local.get 9
              i32.store8
              local.get 6
              i32.const 1
              i32.add
              local.set 6
              local.get 8
              i32.const -1
              i32.add
              local.tee 8
              br_if 0 (;@5;)
            end
          end
          local.get 0
          i32.const 4
          i32.store8
          br 2 (;@1;)
        end
        block  ;; label = @3
          local.get 2
          i32.load
          local.get 2
          i32.load offset=8
          local.tee 4
          i32.ne
          br_if 0 (;@3;)
          local.get 2
          local.get 4
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=8
          local.set 4
        end
        local.get 0
        i32.const 4
        i32.store8
        local.get 2
        local.get 4
        i32.const 1
        i32.add
        i32.store offset=8
        local.get 2
        i32.load offset=4
        local.get 4
        i32.add
        i32.const 6
        i32.store8
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 2
        i32.load
        local.get 2
        i32.load offset=8
        local.tee 4
        i32.ne
        br_if 0 (;@2;)
        local.get 2
        local.get 4
        i32.const 1
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load offset=8
        local.set 4
      end
      local.get 2
      i32.load offset=4
      local.get 4
      i32.add
      i32.const 5
      i32.store8
      local.get 2
      local.get 4
      i32.const 1
      i32.add
      local.tee 8
      i32.store offset=8
      local.get 1
      i32.const 12
      i32.add
      i32.load
      local.set 4
      block  ;; label = @2
        local.get 2
        i32.load
        local.get 8
        i32.sub
        i32.const 3
        i32.gt_u
        br_if 0 (;@2;)
        local.get 2
        local.get 8
        i32.const 4
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load offset=8
        local.set 8
      end
      local.get 2
      local.get 8
      i32.const 4
      i32.add
      local.tee 6
      i32.store offset=8
      local.get 2
      i32.load offset=4
      local.tee 7
      local.get 8
      i32.add
      local.get 4
      i32.const 24
      i32.shl
      local.get 4
      i32.const 8
      i32.shl
      i32.const 16711680
      i32.and
      i32.or
      local.get 4
      i32.const 8
      i32.shr_u
      i32.const 65280
      i32.and
      local.get 4
      i32.const 24
      i32.shr_u
      i32.or
      i32.or
      i32.store align=1
      block  ;; label = @2
        local.get 4
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.const 8
        i32.add
        i32.load
        local.set 1
        local.get 4
        i32.const 2
        i32.shl
        local.set 9
        loop  ;; label = @3
          local.get 1
          i32.load
          local.set 4
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.load
              local.get 6
              i32.sub
              i32.const 3
              i32.le_u
              br_if 0 (;@5;)
              local.get 6
              local.set 8
              br 1 (;@4;)
            end
            local.get 2
            local.get 6
            i32.const 4
            call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
            local.get 2
            i32.load offset=4
            local.set 7
            local.get 2
            i32.load offset=8
            local.set 8
          end
          local.get 2
          local.get 8
          i32.const 4
          i32.add
          local.tee 6
          i32.store offset=8
          local.get 7
          local.get 8
          i32.add
          local.get 4
          i32.const 24
          i32.shl
          local.get 4
          i32.const 8
          i32.shl
          i32.const 16711680
          i32.and
          i32.or
          local.get 4
          i32.const 8
          i32.shr_u
          i32.const 65280
          i32.and
          local.get 4
          i32.const 24
          i32.shr_u
          i32.or
          i32.or
          i32.store align=1
          local.get 1
          i32.const 4
          i32.add
          local.set 1
          local.get 9
          i32.const -4
          i32.add
          local.tee 9
          br_if 0 (;@3;)
        end
      end
      local.get 0
      i32.const 4
      i32.store8
    end
    local.get 3
    i32.const 16
    i32.add
    global.set 0)
  (func $<pbc_contract_common::zk::CalculationStatus_as_core::fmt::Debug>::fmt::hff1dc751f644adc3 (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              i32.load8_u
              br_table 0 (;@5;) 1 (;@4;) 2 (;@3;) 3 (;@2;) 4 (;@1;) 0 (;@5;)
            end
            local.get 1
            i32.const 1052043
            i32.const 7
            call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
            return
          end
          local.get 1
          i32.const 1052032
          i32.const 11
          call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
          return
        end
        local.get 1
        i32.const 1052026
        i32.const 6
        call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
        return
      end
      local.get 1
      i32.const 1052008
      i32.const 18
      call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
      return
    end
    local.get 1
    i32.const 1052004
    i32.const 4
    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8)
  (func $pbc_contract_common::result_buffer::write_u32_be_at_idx::h17d1ca3092b4244e (type 7) (param i32 i32 i32 i32 i32)
    (local i32)
    local.get 3
    i32.const 4
    i32.add
    local.set 5
    block  ;; label = @1
      block  ;; label = @2
        local.get 3
        i32.const -5
        i32.gt_u
        br_if 0 (;@2;)
        local.get 5
        local.get 2
        i32.gt_u
        br_if 1 (;@1;)
        local.get 0
        i32.const 4
        i32.store8
        local.get 1
        local.get 3
        i32.add
        local.get 4
        i32.const 24
        i32.shl
        local.get 4
        i32.const 8
        i32.shl
        i32.const 16711680
        i32.and
        i32.or
        local.get 4
        i32.const 8
        i32.shr_u
        i32.const 65280
        i32.and
        local.get 4
        i32.const 24
        i32.shr_u
        i32.or
        i32.or
        i32.store align=1
        return
      end
      local.get 3
      local.get 5
      i32.const 1052164
      call $core::slice::index::slice_index_order_fail::h2a2e64aa62065ee1
      unreachable
    end
    local.get 5
    local.get 2
    i32.const 1052164
    call $core::slice::index::slice_end_index_len_fail::h06e641a35ef26e6c
    unreachable)
  (func $pbc_contract_common::result_buffer::ContractResultBuffer::new::h7b510c27fb133391 (type 0) (param i32)
    (local i32)
    block  ;; label = @1
      i32.const 10240
      i32.const 1
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 10240
      i32.const 1
      call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
      unreachable
    end
    local.get 0
    i32.const 0
    i32.store8 offset=12
    local.get 0
    i32.const 4
    i32.store offset=8
    local.get 0
    local.get 1
    i32.store offset=4
    local.get 0
    i32.const 10240
    i32.store
    local.get 1
    i32.const 0
    i32.store align=1)
  (func $pbc_contract_common::result_buffer::ContractResultBuffer::write_events::he019054c70500fa3 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.load offset=8
              local.tee 3
              i32.eqz
              br_if 0 (;@5;)
              local.get 2
              i32.const 2
              i32.store8 offset=7
              local.get 0
              i32.load8_u offset=12
              i32.const 2
              i32.gt_u
              br_if 1 (;@4;)
              local.get 0
              i32.const 3
              i32.store8 offset=12
              block  ;; label = @6
                local.get 0
                i32.load
                local.get 0
                i32.load offset=8
                local.tee 4
                i32.ne
                br_if 0 (;@6;)
                local.get 0
                local.get 4
                i32.const 1
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                local.get 0
                i32.load offset=8
                local.set 4
              end
              local.get 0
              i32.const 4
              i32.add
              local.tee 5
              i32.load
              local.get 4
              i32.add
              i32.const 2
              i32.store8
              local.get 0
              local.get 4
              i32.const 1
              i32.add
              local.tee 6
              i32.store offset=8
              local.get 6
              local.set 7
              block  ;; label = @6
                local.get 0
                i32.load
                local.get 6
                i32.sub
                i32.const 3
                i32.gt_u
                br_if 0 (;@6;)
                local.get 0
                local.get 6
                i32.const 4
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                local.get 0
                i32.load offset=8
                local.set 7
              end
              local.get 5
              i32.load
              local.get 7
              i32.add
              i32.const 0
              i32.store align=1
              local.get 0
              local.get 7
              i32.const 4
              i32.add
              local.tee 7
              i32.store offset=8
              block  ;; label = @6
                local.get 0
                i32.load
                local.get 7
                i32.sub
                i32.const 3
                i32.gt_u
                br_if 0 (;@6;)
                local.get 0
                local.get 7
                i32.const 4
                call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
                local.get 0
                i32.load offset=8
                local.set 7
              end
              local.get 0
              local.get 7
              i32.const 4
              i32.add
              i32.store offset=8
              local.get 0
              i32.const 4
              i32.add
              i32.load
              local.get 7
              i32.add
              local.get 3
              i32.const 24
              i32.shl
              local.get 3
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 3
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 3
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              i32.store align=1
              local.get 3
              i32.const 56
              i32.mul
              local.set 7
              local.get 1
              i32.const 4
              i32.add
              i32.load
              local.set 3
              loop  ;; label = @6
                local.get 2
                i32.const 32
                i32.add
                local.get 3
                local.get 0
                call $<pbc_contract_common::events::EventGroup_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::hb1314657eef35dc6
                local.get 2
                i32.load8_u offset=32
                i32.const 4
                i32.ne
                br_if 3 (;@3;)
                local.get 3
                i32.const 56
                i32.add
                local.set 3
                local.get 7
                i32.const -56
                i32.add
                local.tee 7
                br_if 0 (;@6;)
              end
              local.get 4
              i32.const 5
              i32.add
              local.set 3
              local.get 6
              i32.const -5
              i32.gt_u
              br_if 3 (;@2;)
              local.get 3
              local.get 0
              i32.load offset=8
              local.tee 7
              i32.gt_u
              br_if 4 (;@1;)
              local.get 0
              i32.const 4
              i32.add
              i32.load
              local.get 6
              i32.add
              local.get 7
              local.get 4
              i32.sub
              i32.const -5
              i32.add
              local.tee 3
              i32.const 24
              i32.shl
              local.get 3
              i32.const 8
              i32.shl
              i32.const 16711680
              i32.and
              i32.or
              local.get 3
              i32.const 8
              i32.shr_u
              i32.const 65280
              i32.and
              local.get 3
              i32.const 24
              i32.shr_u
              i32.or
              i32.or
              i32.store align=1
            end
            local.get 1
            call $<alloc::vec::Vec<T_A>_as_core::ops::drop::Drop>::drop::h93aca5bcf8cb086a
            block  ;; label = @5
              local.get 1
              i32.load
              local.tee 3
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              i32.const 4
              i32.add
              i32.load
              local.get 3
              i32.const 56
              i32.mul
              i32.const 8
              call $__rust_dealloc
            end
            local.get 2
            i32.const 48
            i32.add
            global.set 0
            return
          end
          local.get 2
          i32.const 32
          i32.add
          i32.const 12
          i32.add
          i32.const 8
          i32.store
          local.get 2
          i32.const 8
          i32.add
          i32.const 12
          i32.add
          i32.const 2
          i32.store
          local.get 2
          i32.const 28
          i32.add
          i32.const 2
          i32.store
          local.get 2
          i32.const 1052300
          i32.store offset=16
          local.get 2
          i32.const 2
          i32.store offset=12
          local.get 2
          i32.const 1052316
          i32.store offset=8
          local.get 2
          local.get 0
          i32.const 12
          i32.add
          i32.store offset=40
          local.get 2
          i32.const 8
          i32.store offset=36
          local.get 2
          local.get 2
          i32.const 32
          i32.add
          i32.store offset=24
          local.get 2
          local.get 2
          i32.const 7
          i32.add
          i32.store offset=32
          local.get 2
          i32.const 8
          i32.add
          i32.const 1052380
          call $core::panicking::panic_fmt::h9d972fcdb087ce21
          unreachable
        end
        local.get 2
        local.get 2
        i64.load offset=32
        i64.store offset=8
        i32.const 1051820
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1051864
        i32.const 1051988
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 6
      local.get 3
      i32.const 1052164
      call $core::slice::index::slice_index_order_fail::h2a2e64aa62065ee1
      unreachable
    end
    local.get 3
    local.get 7
    i32.const 1052164
    call $core::slice::index::slice_end_index_len_fail::h06e641a35ef26e6c
    unreachable)
  (func $pbc_contract_common::result_buffer::ContractResultBuffer::finalize_result_buffer::hccec9daac9270322 (type 1) (param i32) (result i64)
    (local i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=8
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=4
      local.tee 0
      local.get 1
      i32.const -4
      i32.add
      local.tee 2
      i32.const 24
      i32.shl
      local.get 2
      i32.const 8
      i32.shl
      i32.const 16711680
      i32.and
      i32.or
      local.get 2
      i32.const 8
      i32.shr_u
      i32.const 65280
      i32.and
      local.get 2
      i32.const 24
      i32.shr_u
      i32.or
      i32.or
      local.tee 2
      i32.store8
      local.get 1
      i32.const 1
      i32.eq
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.const 8
      i32.shr_u
      i32.store8 offset=1
      local.get 1
      i32.const 2
      i32.eq
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.const 16
      i32.shr_u
      i32.store8 offset=2
      local.get 1
      i32.const 3
      i32.eq
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.const 24
      i32.shr_u
      i32.store8 offset=3
      local.get 0
      i64.extend_i32_u
      return
    end
    local.get 1
    local.get 1
    i32.const 1052396
    call $core::panicking::panic_bounds_check::h929177d2c8f5de7b
    unreachable)
  (func $pbc_contract_common::result_buffer::ContractResultBuffer::write_zk_state_change::he57df4ae42921562 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    i32.const 17
    i32.store8 offset=7
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.load8_u offset=12
            i32.const 17
            i32.gt_u
            br_if 0 (;@4;)
            local.get 0
            i32.const 18
            i32.store8 offset=12
            block  ;; label = @5
              local.get 0
              i32.load
              local.get 0
              i32.load offset=8
              local.tee 3
              i32.ne
              br_if 0 (;@5;)
              local.get 0
              local.get 3
              i32.const 1
              call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
              local.get 0
              i32.load offset=8
              local.set 3
            end
            local.get 0
            i32.const 4
            i32.add
            local.tee 4
            i32.load
            local.get 3
            i32.add
            i32.const 17
            i32.store8
            local.get 0
            local.get 3
            i32.const 1
            i32.add
            local.tee 5
            i32.store offset=8
            local.get 5
            local.set 6
            block  ;; label = @5
              local.get 0
              i32.load
              local.get 5
              i32.sub
              i32.const 3
              i32.gt_u
              br_if 0 (;@5;)
              local.get 0
              local.get 5
              i32.const 4
              call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
              local.get 0
              i32.load offset=8
              local.set 6
            end
            local.get 4
            i32.load
            local.get 6
            i32.add
            i32.const 0
            i32.store align=1
            local.get 0
            local.get 6
            i32.const 4
            i32.add
            local.tee 4
            i32.store offset=8
            local.get 1
            i32.const 8
            i32.add
            i32.load
            local.set 6
            block  ;; label = @5
              local.get 0
              i32.load
              local.get 4
              i32.sub
              i32.const 3
              i32.gt_u
              br_if 0 (;@5;)
              local.get 0
              local.get 4
              i32.const 4
              call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
              local.get 0
              i32.load offset=8
              local.set 4
            end
            local.get 0
            local.get 4
            i32.const 4
            i32.add
            local.tee 7
            i32.store offset=8
            local.get 0
            i32.const 4
            i32.add
            local.tee 8
            i32.load
            local.get 4
            i32.add
            local.get 6
            i32.const 24
            i32.shl
            local.get 6
            i32.const 8
            i32.shl
            i32.const 16711680
            i32.and
            i32.or
            local.get 6
            i32.const 8
            i32.shr_u
            i32.const 65280
            i32.and
            local.get 6
            i32.const 24
            i32.shr_u
            i32.or
            i32.or
            i32.store align=1
            local.get 1
            i32.const 4
            i32.add
            i32.load
            local.set 9
            block  ;; label = @5
              local.get 6
              i32.eqz
              br_if 0 (;@5;)
              local.get 6
              i32.const 28
              i32.mul
              local.set 4
              local.get 9
              local.set 6
              loop  ;; label = @6
                local.get 2
                i32.const 32
                i32.add
                local.get 6
                local.get 0
                call $<pbc_contract_common::zk::ZkStateChange_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::he31ef6701d79cef6
                local.get 2
                i32.load8_u offset=32
                i32.const 4
                i32.ne
                br_if 3 (;@3;)
                local.get 6
                i32.const 28
                i32.add
                local.set 6
                local.get 4
                i32.const -28
                i32.add
                local.tee 4
                br_if 0 (;@6;)
              end
              local.get 0
              i32.load offset=8
              local.set 7
            end
            local.get 3
            i32.const 5
            i32.add
            local.set 6
            local.get 5
            i32.const -5
            i32.gt_u
            br_if 2 (;@2;)
            local.get 6
            local.get 7
            i32.gt_u
            br_if 3 (;@1;)
            local.get 8
            i32.load
            local.get 5
            i32.add
            local.get 7
            local.get 3
            i32.sub
            i32.const -5
            i32.add
            local.tee 6
            i32.const 24
            i32.shl
            local.get 6
            i32.const 8
            i32.shl
            i32.const 16711680
            i32.and
            i32.or
            local.get 6
            i32.const 8
            i32.shr_u
            i32.const 65280
            i32.and
            local.get 6
            i32.const 24
            i32.shr_u
            i32.or
            i32.or
            i32.store align=1
            local.get 1
            call $<alloc::vec::Vec<T_A>_as_core::ops::drop::Drop>::drop::h8bb281a5ef22bb5f
            block  ;; label = @5
              local.get 1
              i32.load
              local.tee 6
              i32.eqz
              br_if 0 (;@5;)
              local.get 9
              local.get 6
              i32.const 28
              i32.mul
              i32.const 4
              call $__rust_dealloc
            end
            local.get 2
            i32.const 48
            i32.add
            global.set 0
            return
          end
          local.get 2
          i32.const 32
          i32.add
          i32.const 12
          i32.add
          i32.const 8
          i32.store
          local.get 2
          i32.const 8
          i32.add
          i32.const 12
          i32.add
          i32.const 2
          i32.store
          local.get 2
          i32.const 28
          i32.add
          i32.const 2
          i32.store
          local.get 2
          i32.const 1052300
          i32.store offset=16
          local.get 2
          i32.const 2
          i32.store offset=12
          local.get 2
          i32.const 1052316
          i32.store offset=8
          local.get 2
          local.get 0
          i32.const 12
          i32.add
          i32.store offset=40
          local.get 2
          i32.const 8
          i32.store offset=36
          local.get 2
          local.get 2
          i32.const 32
          i32.add
          i32.store offset=24
          local.get 2
          local.get 2
          i32.const 7
          i32.add
          i32.store offset=32
          local.get 2
          i32.const 8
          i32.add
          i32.const 1052380
          call $core::panicking::panic_fmt::h9d972fcdb087ce21
          unreachable
        end
        local.get 2
        local.get 2
        i64.load offset=32
        i64.store offset=8
        i32.const 1051820
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1051864
        i32.const 1051988
        call $core::result::unwrap_failed::h2b47cc7f7e98a508
        unreachable
      end
      local.get 5
      local.get 6
      i32.const 1052164
      call $core::slice::index::slice_index_order_fail::h2a2e64aa62065ee1
      unreachable
    end
    local.get 6
    local.get 7
    i32.const 1052164
    call $core::slice::index::slice_end_index_len_fail::h06e641a35ef26e6c
    unreachable)
  (func $<core::option::Option<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h15097e10f262f21c (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i64)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i64.load
        i64.const 0
        i64.ne
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 2
          i32.load
          local.get 2
          i32.load offset=8
          local.tee 1
          i32.ne
          br_if 0 (;@3;)
          local.get 2
          local.get 1
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=8
          local.set 1
        end
        local.get 2
        local.get 1
        i32.const 1
        i32.add
        i32.store offset=8
        local.get 2
        i32.load offset=4
        local.get 1
        i32.add
        i32.const 0
        i32.store8
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 2
        i32.load
        local.tee 3
        local.get 2
        i32.load offset=8
        local.tee 4
        i32.ne
        br_if 0 (;@2;)
        local.get 2
        local.get 4
        i32.const 1
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load
        local.set 3
        local.get 2
        i32.load offset=8
        local.set 4
      end
      local.get 2
      local.get 4
      i32.const 1
      i32.add
      local.tee 5
      i32.store offset=8
      local.get 2
      i32.load offset=4
      local.tee 6
      local.get 4
      i32.add
      i32.const 1
      i32.store8
      local.get 1
      i64.load offset=8
      local.set 7
      block  ;; label = @2
        local.get 3
        local.get 5
        i32.sub
        i32.const 7
        i32.gt_u
        br_if 0 (;@2;)
        local.get 2
        local.get 5
        i32.const 8
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load offset=4
        local.set 6
        local.get 2
        i32.load offset=8
        local.set 5
      end
      local.get 2
      local.get 5
      i32.const 8
      i32.add
      i32.store offset=8
      local.get 6
      local.get 5
      i32.add
      local.get 7
      i64.const 56
      i64.shl
      local.get 7
      i64.const 40
      i64.shl
      i64.const 71776119061217280
      i64.and
      i64.or
      local.get 7
      i64.const 24
      i64.shl
      i64.const 280375465082880
      i64.and
      local.get 7
      i64.const 8
      i64.shl
      i64.const 1095216660480
      i64.and
      i64.or
      i64.or
      local.get 7
      i64.const 8
      i64.shr_u
      i64.const 4278190080
      i64.and
      local.get 7
      i64.const 24
      i64.shr_u
      i64.const 16711680
      i64.and
      i64.or
      local.get 7
      i64.const 40
      i64.shr_u
      i64.const 65280
      i64.and
      local.get 7
      i64.const 56
      i64.shr_u
      i64.or
      i64.or
      i64.or
      i64.store align=1
    end
    local.get 0
    i32.const 4
    i32.store8)
  (func $<core::option::Option<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h2c0212f98c5f2f6e (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 4
        i32.add
        i32.load
        local.tee 3
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 2
          i32.load
          local.get 2
          i32.load offset=8
          local.tee 3
          i32.ne
          br_if 0 (;@3;)
          local.get 2
          local.get 3
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=8
          local.set 3
        end
        local.get 2
        local.get 3
        i32.const 1
        i32.add
        i32.store offset=8
        local.get 2
        i32.load offset=4
        local.get 3
        i32.add
        i32.const 0
        i32.store8
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 2
        i32.load
        local.tee 4
        local.get 2
        i32.load offset=8
        local.tee 5
        i32.ne
        br_if 0 (;@2;)
        local.get 2
        local.get 5
        i32.const 1
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load
        local.set 4
        local.get 2
        i32.load offset=8
        local.set 5
      end
      local.get 2
      local.get 5
      i32.const 1
      i32.add
      local.tee 6
      i32.store offset=8
      local.get 2
      i32.load offset=4
      local.tee 7
      local.get 5
      i32.add
      i32.const 1
      i32.store8
      local.get 1
      i32.const 8
      i32.add
      i32.load
      local.set 5
      block  ;; label = @2
        local.get 4
        local.get 6
        i32.sub
        i32.const 3
        i32.gt_u
        br_if 0 (;@2;)
        local.get 2
        local.get 6
        i32.const 4
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load offset=4
        local.set 7
        local.get 2
        i32.load offset=8
        local.set 6
      end
      local.get 2
      local.get 6
      i32.const 4
      i32.add
      local.tee 1
      i32.store offset=8
      local.get 7
      local.get 6
      i32.add
      local.get 5
      i32.const 24
      i32.shl
      local.get 5
      i32.const 8
      i32.shl
      i32.const 16711680
      i32.and
      i32.or
      local.get 5
      i32.const 8
      i32.shr_u
      i32.const 65280
      i32.and
      local.get 5
      i32.const 24
      i32.shr_u
      i32.or
      i32.or
      i32.store align=1
      local.get 5
      i32.eqz
      br_if 0 (;@1;)
      loop  ;; label = @2
        local.get 3
        i32.load8_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load
            local.get 1
            i32.eq
            br_if 0 (;@4;)
            local.get 1
            local.set 6
            br 1 (;@3;)
          end
          local.get 2
          local.get 1
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=4
          local.set 7
          local.get 2
          i32.load offset=8
          local.set 6
        end
        local.get 2
        local.get 6
        i32.const 1
        i32.add
        local.tee 1
        i32.store offset=8
        local.get 7
        local.get 6
        i32.add
        local.get 4
        i32.store8
        local.get 3
        i32.const 1
        i32.add
        local.set 3
        local.get 5
        i32.const -1
        i32.add
        local.tee 5
        br_if 0 (;@2;)
      end
    end
    local.get 0
    i32.const 4
    i32.store8)
  (func $<core::option::Option<T>_as_pbc_traits::readwrite_rpc::WriteRPC>::rpc_write_to::h8a834961de5d8dba (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 4
        i32.add
        i32.load
        local.tee 3
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 2
          i32.load
          local.get 2
          i32.load offset=8
          local.tee 3
          i32.ne
          br_if 0 (;@3;)
          local.get 2
          local.get 3
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=8
          local.set 3
        end
        local.get 2
        local.get 3
        i32.const 1
        i32.add
        i32.store offset=8
        local.get 2
        i32.load offset=4
        local.get 3
        i32.add
        i32.const 0
        i32.store8
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 2
        i32.load
        local.tee 4
        local.get 2
        i32.load offset=8
        local.tee 5
        i32.ne
        br_if 0 (;@2;)
        local.get 2
        local.get 5
        i32.const 1
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load
        local.set 4
        local.get 2
        i32.load offset=8
        local.set 5
      end
      local.get 2
      local.get 5
      i32.const 1
      i32.add
      local.tee 6
      i32.store offset=8
      local.get 2
      i32.load offset=4
      local.tee 7
      local.get 5
      i32.add
      i32.const 1
      i32.store8
      local.get 1
      i32.const 8
      i32.add
      i32.load
      local.set 5
      block  ;; label = @2
        local.get 4
        local.get 6
        i32.sub
        i32.const 3
        i32.gt_u
        br_if 0 (;@2;)
        local.get 2
        local.get 6
        i32.const 4
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
        local.get 2
        i32.load offset=4
        local.set 7
        local.get 2
        i32.load offset=8
        local.set 6
      end
      local.get 2
      local.get 6
      i32.const 4
      i32.add
      local.tee 1
      i32.store offset=8
      local.get 7
      local.get 6
      i32.add
      local.get 5
      i32.const 24
      i32.shl
      local.get 5
      i32.const 8
      i32.shl
      i32.const 16711680
      i32.and
      i32.or
      local.get 5
      i32.const 8
      i32.shr_u
      i32.const 65280
      i32.and
      local.get 5
      i32.const 24
      i32.shr_u
      i32.or
      i32.or
      i32.store align=1
      local.get 5
      i32.eqz
      br_if 0 (;@1;)
      loop  ;; label = @2
        local.get 3
        i32.load8_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load
            local.get 1
            i32.eq
            br_if 0 (;@4;)
            local.get 1
            local.set 6
            br 1 (;@3;)
          end
          local.get 2
          local.get 1
          i32.const 1
          call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hc140bcde1f184c8e
          local.get 2
          i32.load offset=4
          local.set 7
          local.get 2
          i32.load offset=8
          local.set 6
        end
        local.get 2
        local.get 6
        i32.const 1
        i32.add
        local.tee 1
        i32.store offset=8
        local.get 7
        local.get 6
        i32.add
        local.get 4
        i32.store8
        local.get 3
        i32.const 1
        i32.add
        local.set 3
        local.get 5
        i32.const -1
        i32.add
        local.tee 5
        br_if 0 (;@2;)
      end
    end
    local.get 0
    i32.const 4
    i32.store8)
  (func $<&T_as_core::fmt::Debug>::fmt::h24cfd342c4663119 (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.load
            i32.load8_u
            br_table 0 (;@4;) 1 (;@3;) 2 (;@2;) 3 (;@1;) 0 (;@4;)
          end
          local.get 1
          i32.const 1052450
          i32.const 7
          call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
          return
        end
        local.get 1
        i32.const 1052436
        i32.const 14
        call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
        return
      end
      local.get 1
      i32.const 1052422
      i32.const 14
      call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
      return
    end
    local.get 1
    i32.const 1052412
    i32.const 10
    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8)
  (func $core::ptr::drop_in_place<&_u8__20_>::h9faf78432464bb3e (type 0) (param i32))
  (func $<pbc_contract_common::address_internal::Address_as_core::fmt::Debug>::fmt::h9bea144fc5b613ad (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    local.get 0
    i32.const 1
    i32.add
    i32.store offset=12
    local.get 1
    i32.const 1052457
    i32.const 7
    i32.const 1052464
    i32.const 12
    local.get 2
    i32.const 8
    i32.add
    i32.const 1052476
    i32.const 1052492
    i32.const 10
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052504
    call $core::fmt::Formatter::debug_struct_field2_finish::h953b04c6ca022536
    local.set 0
    local.get 2
    i32.const 16
    i32.add
    global.set 0
    local.get 0)
  (func $core::ptr::drop_in_place<&u8>::hf8f42853aa9727f1 (type 0) (param i32))
  (func $core::array::<impl_core::fmt::Debug_for__T__N_>::fmt::h352ce09ea12b5eeb (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 1
    call $core::fmt::Formatter::debug_list::hda9252dcaf123f75
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 1
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 2
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 3
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 4
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 5
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 6
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 7
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 8
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 9
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 10
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 11
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 12
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 13
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 14
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 15
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 16
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 17
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 18
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    local.get 0
    i32.const 19
    i32.add
    i32.store offset=12
    local.get 2
    local.get 2
    i32.const 12
    i32.add
    i32.const 1052520
    call $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0
    drop
    local.get 2
    call $core::fmt::builders::DebugList::finish::h03a58eb4e118cf5e
    local.set 0
    local.get 2
    i32.const 16
    i32.add
    global.set 0
    local.get 0)
  (func $<&T_as_core::fmt::Debug>::fmt::h4efa132c19f330b8 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.set 0
    block  ;; label = @1
      local.get 1
      call $core::fmt::Formatter::debug_lower_hex::h768a8b81c9cb249b
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 1
        call $core::fmt::Formatter::debug_upper_hex::hdedc73e262a99d1d
        br_if 0 (;@2;)
        local.get 0
        local.get 1
        call $core::fmt::num::imp::<impl_core::fmt::Display_for_u8>::fmt::h2ce6ac9345632f4e
        return
      end
      local.get 0
      local.get 1
      call $core::fmt::num::<impl_core::fmt::UpperHex_for_i8>::fmt::h7ed37fd7cd13cf31
      return
    end
    local.get 0
    local.get 1
    call $core::fmt::num::<impl_core::fmt::LowerHex_for_i8>::fmt::h770642123b33a41b)
  (func $<&T_as_core::fmt::Debug>::fmt::h51188a6b196986bf (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    call $core::array::<impl_core::fmt::Debug_for__T__N_>::fmt::h352ce09ea12b5eeb)
  (func $<T_as_core::any::Any>::type_id::h12342e39968a4047 (type 1) (param i32) (result i64)
    i64.const 8330237566129496815)
  (func $<T_as_core::any::Any>::type_id::h91d619ab643bea16 (type 1) (param i32) (result i64)
    i64.const -8527728395957036344)
  (func $<T_as_core::any::Any>::type_id::ha426e9c360ec53b5 (type 1) (param i32) (result i64)
    i64.const 359314774325844787)
  (func $<&T_as_core::fmt::Debug>::fmt::h0228b32a2cf5ef52 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.tee 0
    i32.load
    local.get 1
    local.get 0
    i32.const 4
    i32.add
    i32.load
    i32.load offset=12
    call_indirect (type 2))
  (func $<&T_as_core::fmt::Debug>::fmt::h99a333b24ab754d0 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 1
    call $<str_as_core::fmt::Debug>::fmt::hcef99b0bb07b7e61)
  (func $<&T_as_core::fmt::Debug>::fmt::hcbeb5a74998e7fbc (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    call $<std::io::error::ErrorKind_as_core::fmt::Debug>::fmt::h9f08f3a56148ea47)
  (func $<std::io::error::ErrorKind_as_core::fmt::Debug>::fmt::h9f08f3a56148ea47 (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  block  ;; label = @16
                                    block  ;; label = @17
                                      block  ;; label = @18
                                        block  ;; label = @19
                                          block  ;; label = @20
                                            block  ;; label = @21
                                              block  ;; label = @22
                                                block  ;; label = @23
                                                  block  ;; label = @24
                                                    block  ;; label = @25
                                                      block  ;; label = @26
                                                        block  ;; label = @27
                                                          block  ;; label = @28
                                                            block  ;; label = @29
                                                              block  ;; label = @30
                                                                block  ;; label = @31
                                                                  block  ;; label = @32
                                                                    block  ;; label = @33
                                                                      block  ;; label = @34
                                                                        block  ;; label = @35
                                                                          block  ;; label = @36
                                                                            block  ;; label = @37
                                                                              block  ;; label = @38
                                                                                block  ;; label = @39
                                                                                  block  ;; label = @40
                                                                                    block  ;; label = @41
                                                                                      local.get 0
                                                                                      i32.load8_u
                                                                                      br_table 0 (;@41;) 1 (;@40;) 2 (;@39;) 3 (;@38;) 4 (;@37;) 5 (;@36;) 6 (;@35;) 7 (;@34;) 8 (;@33;) 9 (;@32;) 10 (;@31;) 11 (;@30;) 12 (;@29;) 13 (;@28;) 14 (;@27;) 15 (;@26;) 16 (;@25;) 17 (;@24;) 18 (;@23;) 19 (;@22;) 20 (;@21;) 21 (;@20;) 22 (;@19;) 23 (;@18;) 24 (;@17;) 25 (;@16;) 26 (;@15;) 27 (;@14;) 28 (;@13;) 29 (;@12;) 30 (;@11;) 31 (;@10;) 32 (;@9;) 33 (;@8;) 34 (;@7;) 35 (;@6;) 36 (;@5;) 37 (;@4;) 38 (;@3;) 39 (;@2;) 40 (;@1;) 0 (;@41;)
                                                                                    end
                                                                                    local.get 1
                                                                                    i32.const 1053591
                                                                                    i32.const 8
                                                                                    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                                    return
                                                                                  end
                                                                                  local.get 1
                                                                                  i32.const 1053575
                                                                                  i32.const 16
                                                                                  call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                                  return
                                                                                end
                                                                                local.get 1
                                                                                i32.const 1053558
                                                                                i32.const 17
                                                                                call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                                return
                                                                              end
                                                                              local.get 1
                                                                              i32.const 1053543
                                                                              i32.const 15
                                                                              call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                              return
                                                                            end
                                                                            local.get 1
                                                                            i32.const 1053528
                                                                            i32.const 15
                                                                            call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                            return
                                                                          end
                                                                          local.get 1
                                                                          i32.const 1053510
                                                                          i32.const 18
                                                                          call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                          return
                                                                        end
                                                                        local.get 1
                                                                        i32.const 1053493
                                                                        i32.const 17
                                                                        call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                        return
                                                                      end
                                                                      local.get 1
                                                                      i32.const 1053481
                                                                      i32.const 12
                                                                      call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                      return
                                                                    end
                                                                    local.get 1
                                                                    i32.const 1053472
                                                                    i32.const 9
                                                                    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                    return
                                                                  end
                                                                  local.get 1
                                                                  i32.const 1053456
                                                                  i32.const 16
                                                                  call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                  return
                                                                end
                                                                local.get 1
                                                                i32.const 1053445
                                                                i32.const 11
                                                                call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                                return
                                                              end
                                                              local.get 1
                                                              i32.const 1053435
                                                              i32.const 10
                                                              call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                              return
                                                            end
                                                            local.get 1
                                                            i32.const 1053422
                                                            i32.const 13
                                                            call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                            return
                                                          end
                                                          local.get 1
                                                          i32.const 1053412
                                                          i32.const 10
                                                          call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                          return
                                                        end
                                                        local.get 1
                                                        i32.const 1053399
                                                        i32.const 13
                                                        call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                        return
                                                      end
                                                      local.get 1
                                                      i32.const 1053387
                                                      i32.const 12
                                                      call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                      return
                                                    end
                                                    local.get 1
                                                    i32.const 1053370
                                                    i32.const 17
                                                    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                    return
                                                  end
                                                  local.get 1
                                                  i32.const 1053352
                                                  i32.const 18
                                                  call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                  return
                                                end
                                                local.get 1
                                                i32.const 1053338
                                                i32.const 14
                                                call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                                return
                                              end
                                              local.get 1
                                              i32.const 1053316
                                              i32.const 22
                                              call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                              return
                                            end
                                            local.get 1
                                            i32.const 1053304
                                            i32.const 12
                                            call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                            return
                                          end
                                          local.get 1
                                          i32.const 1053293
                                          i32.const 11
                                          call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                          return
                                        end
                                        local.get 1
                                        i32.const 1053285
                                        i32.const 8
                                        call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                        return
                                      end
                                      local.get 1
                                      i32.const 1053276
                                      i32.const 9
                                      call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                      return
                                    end
                                    local.get 1
                                    i32.const 1053265
                                    i32.const 11
                                    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                    return
                                  end
                                  local.get 1
                                  i32.const 1053254
                                  i32.const 11
                                  call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                  return
                                end
                                local.get 1
                                i32.const 1053231
                                i32.const 23
                                call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                                return
                              end
                              local.get 1
                              i32.const 1053219
                              i32.const 12
                              call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                              return
                            end
                            local.get 1
                            i32.const 1053207
                            i32.const 12
                            call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                            return
                          end
                          local.get 1
                          i32.const 1053189
                          i32.const 18
                          call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                          return
                        end
                        local.get 1
                        i32.const 1053181
                        i32.const 8
                        call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                        return
                      end
                      local.get 1
                      i32.const 1053167
                      i32.const 14
                      call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                      return
                    end
                    local.get 1
                    i32.const 1053155
                    i32.const 12
                    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                    return
                  end
                  local.get 1
                  i32.const 1053140
                  i32.const 15
                  call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                  return
                end
                local.get 1
                i32.const 1053121
                i32.const 19
                call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
                return
              end
              local.get 1
              i32.const 1053110
              i32.const 11
              call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
              return
            end
            local.get 1
            i32.const 1053012
            i32.const 11
            call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
            return
          end
          local.get 1
          i32.const 1053097
          i32.const 13
          call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
          return
        end
        local.get 1
        i32.const 1053086
        i32.const 11
        call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
        return
      end
      local.get 1
      i32.const 1053081
      i32.const 5
      call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8
      return
    end
    local.get 1
    i32.const 1053068
    i32.const 13
    call $core::fmt::Formatter::write_str::hbcb394ab6bee87b8)
  (func $core::fmt::num::<impl_core::fmt::Debug_for_i32>::fmt::hf65dabd27e3ccd74 (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        call $core::fmt::Formatter::debug_lower_hex::h768a8b81c9cb249b
        br_if 0 (;@2;)
        local.get 1
        call $core::fmt::Formatter::debug_upper_hex::hdedc73e262a99d1d
        br_if 1 (;@1;)
        local.get 0
        local.get 1
        call $core::fmt::num::imp::<impl_core::fmt::Display_for_i32>::fmt::hac307c85eb8fe9bf
        return
      end
      local.get 0
      local.get 1
      call $core::fmt::num::<impl_core::fmt::LowerHex_for_i32>::fmt::hcca31281b8baf66e
      return
    end
    local.get 0
    local.get 1
    call $core::fmt::num::<impl_core::fmt::UpperHex_for_i32>::fmt::h2165261741388ccd)
  (func $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hcde05aeac72d7667 (type 6) (param i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        local.get 2
        i32.add
        local.tee 2
        local.get 1
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 1
        i32.const 1
        i32.shl
        local.tee 4
        local.get 2
        local.get 4
        local.get 2
        i32.gt_u
        select
        local.tee 2
        i32.const 8
        local.get 2
        i32.const 8
        i32.gt_u
        select
        local.tee 2
        i32.const -1
        i32.xor
        i32.const 31
        i32.shr_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            i32.const 1
            i32.store offset=24
            local.get 3
            local.get 1
            i32.store offset=20
            local.get 3
            local.get 0
            i32.const 4
            i32.add
            i32.load
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 3
          i32.const 0
          i32.store offset=24
        end
        local.get 3
        local.get 2
        local.get 4
        local.get 3
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h0f9bc122bf178001
        local.get 3
        i32.load offset=4
        local.set 1
        block  ;; label = @3
          local.get 3
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 2
          i32.store
          local.get 0
          local.get 1
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 3
    i32.const 32
    i32.add
    global.set 0)
  (func $std::panicking::panic_count::is_zero_slow_path::h104e28d68691b5d5 (type 11) (result i32)
    i32.const 0
    i32.load offset=1057940
    i32.eqz)
  (func $core::ptr::drop_in_place<&mut_std::io::Write::write_fmt::Adapter<alloc::vec::Vec<u8>>>::he1857990d75d318b (type 0) (param i32))
  (func $core::ptr::drop_in_place<alloc::string::String>::h159fe17ec9cfec50 (type 0) (param i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.get 1
      i32.const 1
      call $__rust_dealloc
    end)
  (func $core::ptr::drop_in_place<std::panicking::begin_panic_handler::PanicPayload>::h8fce5688cee52563 (type 0) (param i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load
      local.tee 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      local.get 0
      i32.const 1
      call $__rust_dealloc
    end)
  (func $<&mut_W_as_core::fmt::Write>::write_char::ha9236e4544a32b1b (type 2) (param i32 i32) (result i32)
    (local i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.load
    local.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const 128
            i32.lt_u
            br_if 0 (;@4;)
            local.get 2
            i32.const 0
            i32.store offset=12
            local.get 1
            i32.const 2048
            i32.ge_u
            br_if 1 (;@3;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 192
            i32.or
            i32.store8 offset=12
            i32.const 2
            local.set 1
            br 2 (;@2;)
          end
          block  ;; label = @4
            local.get 0
            i32.load offset=8
            local.tee 3
            local.get 0
            i32.load
            i32.ne
            br_if 0 (;@4;)
            local.get 0
            local.get 3
            call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::h0c5d9aea6eccd184
            local.get 0
            i32.load offset=8
            local.set 3
          end
          local.get 0
          local.get 3
          i32.const 1
          i32.add
          i32.store offset=8
          local.get 0
          i32.load offset=4
          local.get 3
          i32.add
          local.get 1
          i32.store8
          br 2 (;@1;)
        end
        block  ;; label = @3
          local.get 1
          i32.const 65536
          i32.lt_u
          br_if 0 (;@3;)
          local.get 2
          local.get 1
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=15
          local.get 2
          local.get 1
          i32.const 6
          i32.shr_u
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=14
          local.get 2
          local.get 1
          i32.const 12
          i32.shr_u
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=13
          local.get 2
          local.get 1
          i32.const 18
          i32.shr_u
          i32.const 7
          i32.and
          i32.const 240
          i32.or
          i32.store8 offset=12
          i32.const 4
          local.set 1
          br 1 (;@2;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=14
        local.get 2
        local.get 1
        i32.const 12
        i32.shr_u
        i32.const 224
        i32.or
        i32.store8 offset=12
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        i32.const 3
        local.set 1
      end
      block  ;; label = @2
        local.get 0
        i32.load
        local.get 0
        i32.load offset=8
        local.tee 3
        i32.sub
        local.get 1
        i32.ge_u
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        local.get 1
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hcde05aeac72d7667
        local.get 0
        i32.load offset=8
        local.set 3
      end
      local.get 0
      i32.load offset=4
      local.get 3
      i32.add
      local.get 2
      i32.const 12
      i32.add
      local.get 1
      call $memcpy
      drop
      local.get 0
      local.get 3
      local.get 1
      i32.add
      i32.store offset=8
    end
    local.get 2
    i32.const 16
    i32.add
    global.set 0
    i32.const 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::h0c5d9aea6eccd184 (type 3) (param i32 i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 8
        local.get 1
        i32.const 8
        i32.gt_u
        select
        local.tee 1
        i32.const -1
        i32.xor
        i32.const 31
        i32.shr_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 1
            i32.store offset=24
            local.get 2
            local.get 3
            i32.store offset=20
            local.get 2
            local.get 0
            i32.const 4
            i32.add
            i32.load
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 1
        local.get 4
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h0f9bc122bf178001
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $<&mut_W_as_core::fmt::Write>::write_fmt::h0ba0eb71505b98bf (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 0
    i32.load
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1052536
    local.get 2
    i32.const 8
    i32.add
    call $core::fmt::write::hb366ddae040241f7
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 1)
  (func $<&mut_W_as_core::fmt::Write>::write_str::ha30ded41f6d9416e (type 4) (param i32 i32 i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 0
      i32.load
      local.get 0
      i32.load offset=8
      local.tee 3
      i32.sub
      local.get 2
      i32.ge_u
      br_if 0 (;@1;)
      local.get 0
      local.get 3
      local.get 2
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hcde05aeac72d7667
      local.get 0
      i32.load offset=8
      local.set 3
    end
    local.get 0
    i32.load offset=4
    local.get 3
    i32.add
    local.get 1
    local.get 2
    call $memcpy
    drop
    local.get 0
    local.get 3
    local.get 2
    i32.add
    i32.store offset=8
    i32.const 0)
  (func $<alloc::string::String_as_core::fmt::Debug>::fmt::h750f0b4a8b532594 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.const 4
    i32.add
    i32.load
    local.get 0
    i32.const 8
    i32.add
    i32.load
    local.get 1
    call $<str_as_core::fmt::Debug>::fmt::hcef99b0bb07b7e61)
  (func $alloc::raw_vec::finish_grow::h0f9bc122bf178001 (type 8) (param i32 i32 i32 i32)
    (local i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  i32.const 0
                  i32.lt_s
                  br_if 0 (;@7;)
                  local.get 3
                  i32.load offset=8
                  i32.eqz
                  br_if 2 (;@5;)
                  local.get 3
                  i32.load offset=4
                  local.tee 4
                  br_if 1 (;@6;)
                  local.get 1
                  br_if 3 (;@4;)
                  local.get 2
                  local.set 3
                  br 4 (;@3;)
                end
                local.get 0
                i32.const 8
                i32.add
                i32.const 0
                i32.store
                br 5 (;@1;)
              end
              local.get 3
              i32.load
              local.get 4
              local.get 2
              local.get 1
              call $__rust_realloc
              local.set 3
              br 2 (;@3;)
            end
            local.get 1
            br_if 0 (;@4;)
            local.get 2
            local.set 3
            br 1 (;@3;)
          end
          local.get 1
          local.get 2
          call $__rust_alloc
          local.set 3
        end
        block  ;; label = @3
          local.get 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.store offset=4
          local.get 0
          i32.const 8
          i32.add
          local.get 1
          i32.store
          local.get 0
          i32.const 0
          i32.store
          return
        end
        local.get 0
        local.get 1
        i32.store offset=4
        local.get 0
        i32.const 8
        i32.add
        local.get 2
        i32.store
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 0
      i32.const 8
      i32.add
      i32.const 0
      i32.store
    end
    local.get 0
    i32.const 1
    i32.store)
  (func $dlmalloc::Dlmalloc<A>::malloc::hd8efb8c25e917a05 (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.const 9
              i32.lt_u
              br_if 0 (;@5;)
              i32.const 16
              i32.const 8
              call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
              local.get 1
              i32.gt_u
              br_if 1 (;@4;)
              br 2 (;@3;)
            end
            local.get 0
            call $dlmalloc::dlmalloc::Dlmalloc<A>::malloc::h6c2f92ea78996c54
            local.set 2
            br 2 (;@2;)
          end
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 1
        end
        call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
        local.tee 3
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 4
        i32.const 20
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 5
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 6
        i32.const 0
        local.set 2
        i32.const 0
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        i32.const 2
        i32.shl
        i32.sub
        local.tee 7
        local.get 3
        local.get 6
        local.get 4
        local.get 5
        i32.add
        i32.add
        i32.sub
        i32.const -65544
        i32.add
        i32.const -9
        i32.and
        i32.const -3
        i32.add
        local.tee 3
        local.get 7
        local.get 3
        i32.lt_u
        select
        local.get 1
        i32.sub
        local.get 0
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        i32.const 16
        local.get 0
        i32.const 4
        i32.add
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        i32.const -5
        i32.add
        local.get 0
        i32.gt_u
        select
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.tee 4
        i32.add
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        i32.add
        i32.const -4
        i32.add
        call $dlmalloc::dlmalloc::Dlmalloc<A>::malloc::h6c2f92ea78996c54
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        call $dlmalloc::dlmalloc::Chunk::from_mem::hfab12920c40b4363
        local.set 0
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const -1
            i32.add
            local.tee 2
            local.get 3
            i32.and
            br_if 0 (;@4;)
            local.get 0
            local.set 1
            br 1 (;@3;)
          end
          local.get 2
          local.get 3
          i32.add
          i32.const 0
          local.get 1
          i32.sub
          i32.and
          call $dlmalloc::dlmalloc::Chunk::from_mem::hfab12920c40b4363
          local.set 2
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 3
          local.get 0
          call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
          local.get 2
          i32.const 0
          local.get 1
          local.get 2
          local.get 0
          i32.sub
          local.get 3
          i32.gt_u
          select
          i32.add
          local.tee 1
          local.get 0
          i32.sub
          local.tee 2
          i32.sub
          local.set 3
          block  ;; label = @4
            local.get 0
            call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
            br_if 0 (;@4;)
            local.get 1
            local.get 3
            call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
            local.get 0
            local.get 2
            call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
            local.get 0
            local.get 2
            call $dlmalloc::dlmalloc::Dlmalloc<A>::dispose_chunk::ha50595baecb6fca0
            br 1 (;@3;)
          end
          local.get 0
          i32.load
          local.set 0
          local.get 1
          local.get 3
          i32.store offset=4
          local.get 1
          local.get 0
          local.get 2
          i32.add
          i32.store
        end
        local.get 1
        call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
        br_if 1 (;@1;)
        local.get 1
        call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
        local.tee 0
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.get 4
        i32.add
        i32.le_u
        br_if 1 (;@1;)
        local.get 1
        local.get 4
        call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
        local.set 2
        local.get 1
        local.get 4
        call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
        local.get 2
        local.get 0
        local.get 4
        i32.sub
        local.tee 0
        call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
        local.get 2
        local.get 0
        call $dlmalloc::dlmalloc::Dlmalloc<A>::dispose_chunk::ha50595baecb6fca0
        br 1 (;@1;)
      end
      local.get 2
      return
    end
    local.get 1
    call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
    local.set 0
    local.get 1
    call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
    drop
    local.get 0)
  (func $dlmalloc::dlmalloc::Dlmalloc<A>::malloc::h6c2f92ea78996c54 (type 12) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          i32.const 245
          i32.lt_u
          br_if 0 (;@3;)
          call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
          local.tee 2
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 3
          i32.const 20
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 4
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 5
          i32.const 0
          local.set 6
          i32.const 0
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          i32.const 2
          i32.shl
          i32.sub
          local.tee 7
          local.get 2
          local.get 5
          local.get 3
          local.get 4
          i32.add
          i32.add
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 2
          local.get 7
          local.get 2
          i32.lt_u
          select
          local.get 0
          i32.le_u
          br_if 2 (;@1;)
          local.get 0
          i32.const 4
          i32.add
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 2
          i32.const 0
          i32.load offset=1057900
          i32.eqz
          br_if 1 (;@2;)
          i32.const 0
          local.set 8
          block  ;; label = @4
            local.get 2
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            i32.const 31
            local.set 8
            local.get 2
            i32.const 16777215
            i32.gt_u
            br_if 0 (;@4;)
            local.get 2
            i32.const 6
            local.get 2
            i32.const 8
            i32.shr_u
            i32.clz
            local.tee 0
            i32.sub
            i32.shr_u
            i32.const 1
            i32.and
            local.get 0
            i32.const 1
            i32.shl
            i32.sub
            i32.const 62
            i32.add
            local.set 8
          end
          i32.const 0
          local.get 2
          i32.sub
          local.set 6
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 8
                i32.const 2
                i32.shl
                i32.const 1057488
                i32.add
                i32.load
                local.tee 0
                i32.eqz
                br_if 0 (;@6;)
                local.get 2
                local.get 8
                call $dlmalloc::dlmalloc::leftshift_for_tree_index::h2319f6a1e784a603
                i32.shl
                local.set 5
                i32.const 0
                local.set 4
                i32.const 0
                local.set 3
                loop  ;; label = @7
                  block  ;; label = @8
                    local.get 0
                    call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
                    call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
                    local.tee 7
                    local.get 2
                    i32.lt_u
                    br_if 0 (;@8;)
                    local.get 7
                    local.get 2
                    i32.sub
                    local.tee 7
                    local.get 6
                    i32.ge_u
                    br_if 0 (;@8;)
                    local.get 7
                    local.set 6
                    local.get 0
                    local.set 3
                    local.get 7
                    br_if 0 (;@8;)
                    i32.const 0
                    local.set 6
                    local.get 0
                    local.set 3
                    br 3 (;@5;)
                  end
                  local.get 0
                  i32.const 20
                  i32.add
                  i32.load
                  local.tee 7
                  local.get 4
                  local.get 7
                  local.get 0
                  local.get 5
                  i32.const 29
                  i32.shr_u
                  i32.const 4
                  i32.and
                  i32.add
                  i32.const 16
                  i32.add
                  i32.load
                  local.tee 0
                  i32.ne
                  select
                  local.get 4
                  local.get 7
                  select
                  local.set 4
                  local.get 5
                  i32.const 1
                  i32.shl
                  local.set 5
                  local.get 0
                  br_if 0 (;@7;)
                end
                block  ;; label = @7
                  local.get 4
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 4
                  local.set 0
                  br 2 (;@5;)
                end
                local.get 3
                br_if 2 (;@4;)
              end
              i32.const 0
              local.set 3
              i32.const 1
              local.get 8
              i32.shl
              call $dlmalloc::dlmalloc::left_bits::h5ec3bcf18952f1c3
              i32.const 0
              i32.load offset=1057900
              i32.and
              local.tee 0
              i32.eqz
              br_if 3 (;@2;)
              local.get 0
              call $dlmalloc::dlmalloc::least_bit::he9078852a9401cf2
              i32.ctz
              i32.const 2
              i32.shl
              i32.const 1057488
              i32.add
              i32.load
              local.tee 0
              i32.eqz
              br_if 3 (;@2;)
            end
            loop  ;; label = @5
              local.get 0
              local.get 3
              local.get 0
              call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
              call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
              local.tee 4
              local.get 2
              i32.ge_u
              local.get 4
              local.get 2
              i32.sub
              local.tee 4
              local.get 6
              i32.lt_u
              i32.and
              local.tee 5
              select
              local.set 3
              local.get 4
              local.get 6
              local.get 5
              select
              local.set 6
              local.get 0
              call $dlmalloc::dlmalloc::TreeChunk::leftmost_child::h5ca0ee09cb0b660e
              local.tee 0
              br_if 0 (;@5;)
            end
            local.get 3
            i32.eqz
            br_if 2 (;@2;)
          end
          block  ;; label = @4
            i32.const 0
            i32.load offset=1057904
            local.tee 0
            local.get 2
            i32.lt_u
            br_if 0 (;@4;)
            local.get 6
            local.get 0
            local.get 2
            i32.sub
            i32.ge_u
            br_if 2 (;@2;)
          end
          local.get 3
          call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
          local.tee 0
          local.get 2
          call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
          local.set 4
          local.get 3
          call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
          block  ;; label = @4
            block  ;; label = @5
              local.get 6
              i32.const 16
              i32.const 8
              call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
              i32.lt_u
              br_if 0 (;@5;)
              local.get 0
              local.get 2
              call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
              local.get 4
              local.get 6
              call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
              block  ;; label = @6
                local.get 6
                i32.const 256
                i32.lt_u
                br_if 0 (;@6;)
                local.get 4
                local.get 6
                call $dlmalloc::dlmalloc::Dlmalloc<A>::insert_large_chunk::hdf12b63cafdb133b
                br 2 (;@4;)
              end
              local.get 6
              i32.const -8
              i32.and
              i32.const 1057632
              i32.add
              local.set 3
              block  ;; label = @6
                block  ;; label = @7
                  i32.const 0
                  i32.load offset=1057896
                  local.tee 5
                  i32.const 1
                  local.get 6
                  i32.const 3
                  i32.shr_u
                  i32.shl
                  local.tee 6
                  i32.and
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 3
                  i32.load offset=8
                  local.set 6
                  br 1 (;@6;)
                end
                i32.const 0
                local.get 5
                local.get 6
                i32.or
                i32.store offset=1057896
                local.get 3
                local.set 6
              end
              local.get 3
              local.get 4
              i32.store offset=8
              local.get 6
              local.get 4
              i32.store offset=12
              local.get 4
              local.get 3
              i32.store offset=12
              local.get 4
              local.get 6
              i32.store offset=8
              br 1 (;@4;)
            end
            local.get 0
            local.get 6
            local.get 2
            i32.add
            call $dlmalloc::dlmalloc::Chunk::set_inuse_and_pinuse::h3be720dca1d3c5b7
          end
          local.get 0
          call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
          local.tee 6
          i32.eqz
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
        i32.const 16
        local.get 0
        i32.const 4
        i32.add
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        i32.const -5
        i32.add
        local.get 0
        i32.gt_u
        select
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      i32.const 0
                      i32.load offset=1057896
                      local.tee 4
                      local.get 2
                      i32.const 3
                      i32.shr_u
                      local.tee 6
                      i32.shr_u
                      local.tee 0
                      i32.const 3
                      i32.and
                      br_if 0 (;@9;)
                      local.get 2
                      i32.const 0
                      i32.load offset=1057904
                      i32.le_u
                      br_if 7 (;@2;)
                      local.get 0
                      br_if 1 (;@8;)
                      i32.const 0
                      i32.load offset=1057900
                      local.tee 0
                      i32.eqz
                      br_if 7 (;@2;)
                      local.get 0
                      call $dlmalloc::dlmalloc::least_bit::he9078852a9401cf2
                      i32.ctz
                      i32.const 2
                      i32.shl
                      i32.const 1057488
                      i32.add
                      i32.load
                      local.tee 3
                      call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
                      call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
                      local.get 2
                      i32.sub
                      local.set 6
                      block  ;; label = @10
                        local.get 3
                        call $dlmalloc::dlmalloc::TreeChunk::leftmost_child::h5ca0ee09cb0b660e
                        local.tee 0
                        i32.eqz
                        br_if 0 (;@10;)
                        loop  ;; label = @11
                          local.get 0
                          call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
                          call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
                          local.get 2
                          i32.sub
                          local.tee 4
                          local.get 6
                          local.get 4
                          local.get 6
                          i32.lt_u
                          local.tee 4
                          select
                          local.set 6
                          local.get 0
                          local.get 3
                          local.get 4
                          select
                          local.set 3
                          local.get 0
                          call $dlmalloc::dlmalloc::TreeChunk::leftmost_child::h5ca0ee09cb0b660e
                          local.tee 0
                          br_if 0 (;@11;)
                        end
                      end
                      local.get 3
                      call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
                      local.tee 0
                      local.get 2
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      local.set 4
                      local.get 3
                      call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
                      local.get 6
                      i32.const 16
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      i32.lt_u
                      br_if 5 (;@4;)
                      local.get 4
                      call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
                      local.set 4
                      local.get 0
                      local.get 2
                      call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
                      local.get 4
                      local.get 6
                      call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
                      i32.const 0
                      i32.load offset=1057904
                      local.tee 7
                      i32.eqz
                      br_if 4 (;@5;)
                      local.get 7
                      i32.const -8
                      i32.and
                      i32.const 1057632
                      i32.add
                      local.set 5
                      i32.const 0
                      i32.load offset=1057912
                      local.set 3
                      i32.const 0
                      i32.load offset=1057896
                      local.tee 8
                      i32.const 1
                      local.get 7
                      i32.const 3
                      i32.shr_u
                      i32.shl
                      local.tee 7
                      i32.and
                      i32.eqz
                      br_if 2 (;@7;)
                      local.get 5
                      i32.load offset=8
                      local.set 7
                      br 3 (;@6;)
                    end
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 0
                        i32.const -1
                        i32.xor
                        i32.const 1
                        i32.and
                        local.get 6
                        i32.add
                        local.tee 2
                        i32.const 3
                        i32.shl
                        local.tee 3
                        i32.const 1057640
                        i32.add
                        i32.load
                        local.tee 0
                        i32.const 8
                        i32.add
                        i32.load
                        local.tee 6
                        local.get 3
                        i32.const 1057632
                        i32.add
                        local.tee 3
                        i32.eq
                        br_if 0 (;@10;)
                        local.get 6
                        local.get 3
                        i32.store offset=12
                        local.get 3
                        local.get 6
                        i32.store offset=8
                        br 1 (;@9;)
                      end
                      i32.const 0
                      local.get 4
                      i32.const -2
                      local.get 2
                      i32.rotl
                      i32.and
                      i32.store offset=1057896
                    end
                    local.get 0
                    local.get 2
                    i32.const 3
                    i32.shl
                    call $dlmalloc::dlmalloc::Chunk::set_inuse_and_pinuse::h3be720dca1d3c5b7
                    local.get 0
                    call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                    local.set 6
                    br 7 (;@1;)
                  end
                  block  ;; label = @8
                    block  ;; label = @9
                      i32.const 1
                      local.get 6
                      i32.const 31
                      i32.and
                      local.tee 6
                      i32.shl
                      call $dlmalloc::dlmalloc::left_bits::h5ec3bcf18952f1c3
                      local.get 0
                      local.get 6
                      i32.shl
                      i32.and
                      call $dlmalloc::dlmalloc::least_bit::he9078852a9401cf2
                      i32.ctz
                      local.tee 6
                      i32.const 3
                      i32.shl
                      local.tee 4
                      i32.const 1057640
                      i32.add
                      i32.load
                      local.tee 0
                      i32.const 8
                      i32.add
                      i32.load
                      local.tee 3
                      local.get 4
                      i32.const 1057632
                      i32.add
                      local.tee 4
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 3
                      local.get 4
                      i32.store offset=12
                      local.get 4
                      local.get 3
                      i32.store offset=8
                      br 1 (;@8;)
                    end
                    i32.const 0
                    i32.const 0
                    i32.load offset=1057896
                    i32.const -2
                    local.get 6
                    i32.rotl
                    i32.and
                    i32.store offset=1057896
                  end
                  local.get 0
                  local.get 2
                  call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
                  local.get 0
                  local.get 2
                  call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                  local.tee 4
                  local.get 6
                  i32.const 3
                  i32.shl
                  local.get 2
                  i32.sub
                  local.tee 5
                  call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
                  block  ;; label = @8
                    i32.const 0
                    i32.load offset=1057904
                    local.tee 3
                    i32.eqz
                    br_if 0 (;@8;)
                    local.get 3
                    i32.const -8
                    i32.and
                    i32.const 1057632
                    i32.add
                    local.set 6
                    i32.const 0
                    i32.load offset=1057912
                    local.set 2
                    block  ;; label = @9
                      block  ;; label = @10
                        i32.const 0
                        i32.load offset=1057896
                        local.tee 7
                        i32.const 1
                        local.get 3
                        i32.const 3
                        i32.shr_u
                        i32.shl
                        local.tee 3
                        i32.and
                        i32.eqz
                        br_if 0 (;@10;)
                        local.get 6
                        i32.load offset=8
                        local.set 3
                        br 1 (;@9;)
                      end
                      i32.const 0
                      local.get 7
                      local.get 3
                      i32.or
                      i32.store offset=1057896
                      local.get 6
                      local.set 3
                    end
                    local.get 6
                    local.get 2
                    i32.store offset=8
                    local.get 3
                    local.get 2
                    i32.store offset=12
                    local.get 2
                    local.get 6
                    i32.store offset=12
                    local.get 2
                    local.get 3
                    i32.store offset=8
                  end
                  i32.const 0
                  local.get 4
                  i32.store offset=1057912
                  i32.const 0
                  local.get 5
                  i32.store offset=1057904
                  local.get 0
                  call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                  local.set 6
                  br 6 (;@1;)
                end
                i32.const 0
                local.get 8
                local.get 7
                i32.or
                i32.store offset=1057896
                local.get 5
                local.set 7
              end
              local.get 5
              local.get 3
              i32.store offset=8
              local.get 7
              local.get 3
              i32.store offset=12
              local.get 3
              local.get 5
              i32.store offset=12
              local.get 3
              local.get 7
              i32.store offset=8
            end
            i32.const 0
            local.get 4
            i32.store offset=1057912
            i32.const 0
            local.get 6
            i32.store offset=1057904
            br 1 (;@3;)
          end
          local.get 0
          local.get 6
          local.get 2
          i32.add
          call $dlmalloc::dlmalloc::Chunk::set_inuse_and_pinuse::h3be720dca1d3c5b7
        end
        local.get 0
        call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
        local.tee 6
        br_if 1 (;@1;)
      end
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        i32.const 0
                        i32.load offset=1057904
                        local.tee 6
                        local.get 2
                        i32.ge_u
                        br_if 0 (;@10;)
                        i32.const 0
                        i32.load offset=1057908
                        local.tee 0
                        local.get 2
                        i32.gt_u
                        br_if 2 (;@8;)
                        local.get 1
                        i32.const 1057488
                        local.get 2
                        call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
                        local.tee 0
                        i32.sub
                        local.get 0
                        i32.const 8
                        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                        i32.add
                        i32.const 20
                        i32.const 8
                        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                        i32.add
                        i32.const 16
                        i32.const 8
                        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                        i32.add
                        i32.const 8
                        i32.add
                        i32.const 65536
                        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                        call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::alloc::hacc9295cf6d00ed7
                        local.get 1
                        i32.load
                        local.tee 6
                        br_if 1 (;@9;)
                        i32.const 0
                        local.set 6
                        br 9 (;@1;)
                      end
                      i32.const 0
                      i32.load offset=1057912
                      local.set 0
                      block  ;; label = @10
                        local.get 6
                        local.get 2
                        i32.sub
                        local.tee 6
                        i32.const 16
                        i32.const 8
                        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                        i32.ge_u
                        br_if 0 (;@10;)
                        i32.const 0
                        i32.const 0
                        i32.store offset=1057912
                        i32.const 0
                        i32.load offset=1057904
                        local.set 2
                        i32.const 0
                        i32.const 0
                        i32.store offset=1057904
                        local.get 0
                        local.get 2
                        call $dlmalloc::dlmalloc::Chunk::set_inuse_and_pinuse::h3be720dca1d3c5b7
                        local.get 0
                        call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                        local.set 6
                        br 9 (;@1;)
                      end
                      local.get 0
                      local.get 2
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      local.set 3
                      i32.const 0
                      local.get 6
                      i32.store offset=1057904
                      i32.const 0
                      local.get 3
                      i32.store offset=1057912
                      local.get 3
                      local.get 6
                      call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
                      local.get 0
                      local.get 2
                      call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
                      local.get 0
                      call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                      local.set 6
                      br 8 (;@1;)
                    end
                    local.get 1
                    i32.load offset=8
                    local.set 8
                    i32.const 0
                    i32.const 0
                    i32.load offset=1057920
                    local.get 1
                    i32.load offset=4
                    local.tee 5
                    i32.add
                    local.tee 0
                    i32.store offset=1057920
                    i32.const 0
                    i32.const 0
                    i32.load offset=1057924
                    local.tee 3
                    local.get 0
                    local.get 3
                    local.get 0
                    i32.gt_u
                    select
                    i32.store offset=1057924
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          i32.const 0
                          i32.load offset=1057916
                          i32.eqz
                          br_if 0 (;@11;)
                          i32.const 1057616
                          local.set 0
                          loop  ;; label = @12
                            local.get 6
                            local.get 0
                            call $dlmalloc::dlmalloc::Segment::top::h0fee0944dfd91eaf
                            i32.eq
                            br_if 2 (;@10;)
                            local.get 0
                            i32.load offset=8
                            local.tee 0
                            br_if 0 (;@12;)
                            br 3 (;@9;)
                          end
                        end
                        i32.const 0
                        i32.load offset=1057932
                        local.tee 0
                        i32.eqz
                        br_if 3 (;@7;)
                        local.get 6
                        local.get 0
                        i32.lt_u
                        br_if 3 (;@7;)
                        br 7 (;@3;)
                      end
                      local.get 0
                      call $dlmalloc::dlmalloc::Segment::is_extern::h4499bcf5769b22f0
                      br_if 0 (;@9;)
                      local.get 0
                      call $dlmalloc::dlmalloc::Segment::sys_flags::hc6d1c33f548edf0c
                      local.get 8
                      i32.ne
                      br_if 0 (;@9;)
                      local.get 0
                      i32.const 0
                      i32.load offset=1057916
                      call $dlmalloc::dlmalloc::Segment::holds::h0fb9a9c5bb17b581
                      br_if 3 (;@6;)
                    end
                    i32.const 0
                    i32.const 0
                    i32.load offset=1057932
                    local.tee 0
                    local.get 6
                    local.get 6
                    local.get 0
                    i32.gt_u
                    select
                    i32.store offset=1057932
                    local.get 6
                    local.get 5
                    i32.add
                    local.set 3
                    i32.const 1057616
                    local.set 0
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          loop  ;; label = @12
                            local.get 0
                            i32.load
                            local.get 3
                            i32.eq
                            br_if 1 (;@11;)
                            local.get 0
                            i32.load offset=8
                            local.tee 0
                            br_if 0 (;@12;)
                            br 2 (;@10;)
                          end
                        end
                        local.get 0
                        call $dlmalloc::dlmalloc::Segment::is_extern::h4499bcf5769b22f0
                        br_if 0 (;@10;)
                        local.get 0
                        call $dlmalloc::dlmalloc::Segment::sys_flags::hc6d1c33f548edf0c
                        local.get 8
                        i32.eq
                        br_if 1 (;@9;)
                      end
                      i32.const 0
                      i32.load offset=1057916
                      local.set 3
                      i32.const 1057616
                      local.set 0
                      block  ;; label = @10
                        loop  ;; label = @11
                          block  ;; label = @12
                            local.get 0
                            i32.load
                            local.get 3
                            i32.gt_u
                            br_if 0 (;@12;)
                            local.get 0
                            call $dlmalloc::dlmalloc::Segment::top::h0fee0944dfd91eaf
                            local.get 3
                            i32.gt_u
                            br_if 2 (;@10;)
                          end
                          local.get 0
                          i32.load offset=8
                          local.tee 0
                          br_if 0 (;@11;)
                        end
                        i32.const 0
                        local.set 0
                      end
                      local.get 0
                      call $dlmalloc::dlmalloc::Segment::top::h0fee0944dfd91eaf
                      local.tee 4
                      i32.const 20
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.tee 9
                      i32.sub
                      i32.const -23
                      i32.add
                      local.set 0
                      local.get 3
                      local.get 0
                      local.get 0
                      call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                      local.tee 7
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.get 7
                      i32.sub
                      i32.add
                      local.tee 0
                      local.get 0
                      local.get 3
                      i32.const 16
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      i32.add
                      i32.lt_u
                      select
                      local.tee 7
                      call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                      local.set 10
                      local.get 7
                      local.get 9
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      local.set 0
                      call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
                      local.tee 11
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.set 12
                      i32.const 20
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.set 13
                      i32.const 16
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.set 14
                      i32.const 0
                      local.get 6
                      local.get 6
                      call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                      local.tee 15
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.get 15
                      i32.sub
                      local.tee 16
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      local.tee 15
                      i32.store offset=1057916
                      i32.const 0
                      local.get 11
                      local.get 5
                      i32.add
                      local.get 14
                      local.get 12
                      local.get 13
                      i32.add
                      i32.add
                      local.get 16
                      i32.add
                      i32.sub
                      local.tee 11
                      i32.store offset=1057908
                      local.get 15
                      local.get 11
                      i32.const 1
                      i32.or
                      i32.store offset=4
                      call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
                      local.tee 12
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.set 13
                      i32.const 20
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.set 14
                      i32.const 16
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      local.set 16
                      local.get 15
                      local.get 11
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      local.get 16
                      local.get 14
                      local.get 13
                      local.get 12
                      i32.sub
                      i32.add
                      i32.add
                      i32.store offset=4
                      i32.const 0
                      i32.const 2097152
                      i32.store offset=1057928
                      local.get 7
                      local.get 9
                      call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
                      i32.const 0
                      i64.load offset=1057616 align=4
                      local.set 17
                      local.get 10
                      i32.const 8
                      i32.add
                      i32.const 0
                      i64.load offset=1057624 align=4
                      i64.store align=4
                      local.get 10
                      local.get 17
                      i64.store align=4
                      i32.const 0
                      local.get 8
                      i32.store offset=1057628
                      i32.const 0
                      local.get 5
                      i32.store offset=1057620
                      i32.const 0
                      local.get 6
                      i32.store offset=1057616
                      i32.const 0
                      local.get 10
                      i32.store offset=1057624
                      loop  ;; label = @10
                        local.get 0
                        i32.const 4
                        call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                        local.set 6
                        local.get 0
                        call $dlmalloc::dlmalloc::Chunk::fencepost_head::h0aeaad6175aa31b5
                        i32.store offset=4
                        local.get 6
                        local.set 0
                        local.get 6
                        i32.const 4
                        i32.add
                        local.get 4
                        i32.lt_u
                        br_if 0 (;@10;)
                      end
                      local.get 7
                      local.get 3
                      i32.eq
                      br_if 7 (;@2;)
                      local.get 7
                      local.get 3
                      i32.sub
                      local.set 0
                      local.get 3
                      local.get 0
                      local.get 3
                      local.get 0
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      call $dlmalloc::dlmalloc::Chunk::set_free_with_pinuse::h28f791aa3db5b3cf
                      block  ;; label = @10
                        local.get 0
                        i32.const 256
                        i32.lt_u
                        br_if 0 (;@10;)
                        local.get 3
                        local.get 0
                        call $dlmalloc::dlmalloc::Dlmalloc<A>::insert_large_chunk::hdf12b63cafdb133b
                        br 8 (;@2;)
                      end
                      local.get 0
                      i32.const -8
                      i32.and
                      i32.const 1057632
                      i32.add
                      local.set 6
                      block  ;; label = @10
                        block  ;; label = @11
                          i32.const 0
                          i32.load offset=1057896
                          local.tee 4
                          i32.const 1
                          local.get 0
                          i32.const 3
                          i32.shr_u
                          i32.shl
                          local.tee 0
                          i32.and
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 6
                          i32.load offset=8
                          local.set 0
                          br 1 (;@10;)
                        end
                        i32.const 0
                        local.get 4
                        local.get 0
                        i32.or
                        i32.store offset=1057896
                        local.get 6
                        local.set 0
                      end
                      local.get 6
                      local.get 3
                      i32.store offset=8
                      local.get 0
                      local.get 3
                      i32.store offset=12
                      local.get 3
                      local.get 6
                      i32.store offset=12
                      local.get 3
                      local.get 0
                      i32.store offset=8
                      br 7 (;@2;)
                    end
                    local.get 0
                    i32.load
                    local.set 4
                    local.get 0
                    local.get 6
                    i32.store
                    local.get 0
                    local.get 0
                    i32.load offset=4
                    local.get 5
                    i32.add
                    i32.store offset=4
                    local.get 6
                    call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                    local.tee 0
                    i32.const 8
                    call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                    local.set 3
                    local.get 4
                    call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                    local.tee 5
                    i32.const 8
                    call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                    local.set 7
                    local.get 6
                    local.get 3
                    local.get 0
                    i32.sub
                    i32.add
                    local.tee 6
                    local.get 2
                    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                    local.set 3
                    local.get 6
                    local.get 2
                    call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
                    local.get 4
                    local.get 7
                    local.get 5
                    i32.sub
                    i32.add
                    local.tee 0
                    local.get 2
                    local.get 6
                    i32.add
                    i32.sub
                    local.set 2
                    block  ;; label = @9
                      local.get 0
                      i32.const 0
                      i32.load offset=1057916
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 0
                      i32.const 0
                      i32.load offset=1057912
                      i32.eq
                      br_if 4 (;@5;)
                      local.get 0
                      call $dlmalloc::dlmalloc::Chunk::inuse::h7a66feebace6547a
                      br_if 5 (;@4;)
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 0
                          call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
                          local.tee 4
                          i32.const 256
                          i32.lt_u
                          br_if 0 (;@11;)
                          local.get 0
                          call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
                          br 1 (;@10;)
                        end
                        block  ;; label = @11
                          local.get 0
                          i32.const 12
                          i32.add
                          i32.load
                          local.tee 5
                          local.get 0
                          i32.const 8
                          i32.add
                          i32.load
                          local.tee 7
                          i32.eq
                          br_if 0 (;@11;)
                          local.get 7
                          local.get 5
                          i32.store offset=12
                          local.get 5
                          local.get 7
                          i32.store offset=8
                          br 1 (;@10;)
                        end
                        i32.const 0
                        i32.const 0
                        i32.load offset=1057896
                        i32.const -2
                        local.get 4
                        i32.const 3
                        i32.shr_u
                        i32.rotl
                        i32.and
                        i32.store offset=1057896
                      end
                      local.get 4
                      local.get 2
                      i32.add
                      local.set 2
                      local.get 0
                      local.get 4
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      local.set 0
                      br 5 (;@4;)
                    end
                    i32.const 0
                    local.get 3
                    i32.store offset=1057916
                    i32.const 0
                    i32.const 0
                    i32.load offset=1057908
                    local.get 2
                    i32.add
                    local.tee 0
                    i32.store offset=1057908
                    local.get 3
                    local.get 0
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 6
                    call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                    local.set 6
                    br 7 (;@1;)
                  end
                  i32.const 0
                  local.get 0
                  local.get 2
                  i32.sub
                  local.tee 6
                  i32.store offset=1057908
                  i32.const 0
                  i32.const 0
                  i32.load offset=1057916
                  local.tee 0
                  local.get 2
                  call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                  local.tee 3
                  i32.store offset=1057916
                  local.get 3
                  local.get 6
                  i32.const 1
                  i32.or
                  i32.store offset=4
                  local.get 0
                  local.get 2
                  call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
                  local.get 0
                  call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
                  local.set 6
                  br 6 (;@1;)
                end
                i32.const 0
                local.get 6
                i32.store offset=1057932
                br 3 (;@3;)
              end
              local.get 0
              local.get 0
              i32.load offset=4
              local.get 5
              i32.add
              i32.store offset=4
              i32.const 0
              i32.load offset=1057916
              i32.const 0
              i32.load offset=1057908
              local.get 5
              i32.add
              call $dlmalloc::dlmalloc::Dlmalloc<A>::init_top::hd19f596040fbda30
              br 3 (;@2;)
            end
            i32.const 0
            local.get 3
            i32.store offset=1057912
            i32.const 0
            i32.const 0
            i32.load offset=1057904
            local.get 2
            i32.add
            local.tee 0
            i32.store offset=1057904
            local.get 3
            local.get 0
            call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
            local.get 6
            call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
            local.set 6
            br 3 (;@1;)
          end
          local.get 3
          local.get 2
          local.get 0
          call $dlmalloc::dlmalloc::Chunk::set_free_with_pinuse::h28f791aa3db5b3cf
          block  ;; label = @4
            local.get 2
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 3
            local.get 2
            call $dlmalloc::dlmalloc::Dlmalloc<A>::insert_large_chunk::hdf12b63cafdb133b
            local.get 6
            call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
            local.set 6
            br 3 (;@1;)
          end
          local.get 2
          i32.const -8
          i32.and
          i32.const 1057632
          i32.add
          local.set 0
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              i32.load offset=1057896
              local.tee 4
              i32.const 1
              local.get 2
              i32.const 3
              i32.shr_u
              i32.shl
              local.tee 2
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 0
              i32.load offset=8
              local.set 2
              br 1 (;@4;)
            end
            i32.const 0
            local.get 4
            local.get 2
            i32.or
            i32.store offset=1057896
            local.get 0
            local.set 2
          end
          local.get 0
          local.get 3
          i32.store offset=8
          local.get 2
          local.get 3
          i32.store offset=12
          local.get 3
          local.get 0
          i32.store offset=12
          local.get 3
          local.get 2
          i32.store offset=8
          local.get 6
          call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
          local.set 6
          br 2 (;@1;)
        end
        i32.const 0
        i32.const 4095
        i32.store offset=1057936
        i32.const 0
        local.get 8
        i32.store offset=1057628
        i32.const 0
        local.get 5
        i32.store offset=1057620
        i32.const 0
        local.get 6
        i32.store offset=1057616
        i32.const 0
        i32.const 1057632
        i32.store offset=1057644
        i32.const 0
        i32.const 1057640
        i32.store offset=1057652
        i32.const 0
        i32.const 1057632
        i32.store offset=1057640
        i32.const 0
        i32.const 1057648
        i32.store offset=1057660
        i32.const 0
        i32.const 1057640
        i32.store offset=1057648
        i32.const 0
        i32.const 1057656
        i32.store offset=1057668
        i32.const 0
        i32.const 1057648
        i32.store offset=1057656
        i32.const 0
        i32.const 1057664
        i32.store offset=1057676
        i32.const 0
        i32.const 1057656
        i32.store offset=1057664
        i32.const 0
        i32.const 1057672
        i32.store offset=1057684
        i32.const 0
        i32.const 1057664
        i32.store offset=1057672
        i32.const 0
        i32.const 1057680
        i32.store offset=1057692
        i32.const 0
        i32.const 1057672
        i32.store offset=1057680
        i32.const 0
        i32.const 1057688
        i32.store offset=1057700
        i32.const 0
        i32.const 1057680
        i32.store offset=1057688
        i32.const 0
        i32.const 1057696
        i32.store offset=1057708
        i32.const 0
        i32.const 1057688
        i32.store offset=1057696
        i32.const 0
        i32.const 1057696
        i32.store offset=1057704
        i32.const 0
        i32.const 1057704
        i32.store offset=1057716
        i32.const 0
        i32.const 1057704
        i32.store offset=1057712
        i32.const 0
        i32.const 1057712
        i32.store offset=1057724
        i32.const 0
        i32.const 1057712
        i32.store offset=1057720
        i32.const 0
        i32.const 1057720
        i32.store offset=1057732
        i32.const 0
        i32.const 1057720
        i32.store offset=1057728
        i32.const 0
        i32.const 1057728
        i32.store offset=1057740
        i32.const 0
        i32.const 1057728
        i32.store offset=1057736
        i32.const 0
        i32.const 1057736
        i32.store offset=1057748
        i32.const 0
        i32.const 1057736
        i32.store offset=1057744
        i32.const 0
        i32.const 1057744
        i32.store offset=1057756
        i32.const 0
        i32.const 1057744
        i32.store offset=1057752
        i32.const 0
        i32.const 1057752
        i32.store offset=1057764
        i32.const 0
        i32.const 1057752
        i32.store offset=1057760
        i32.const 0
        i32.const 1057760
        i32.store offset=1057772
        i32.const 0
        i32.const 1057768
        i32.store offset=1057780
        i32.const 0
        i32.const 1057760
        i32.store offset=1057768
        i32.const 0
        i32.const 1057776
        i32.store offset=1057788
        i32.const 0
        i32.const 1057768
        i32.store offset=1057776
        i32.const 0
        i32.const 1057784
        i32.store offset=1057796
        i32.const 0
        i32.const 1057776
        i32.store offset=1057784
        i32.const 0
        i32.const 1057792
        i32.store offset=1057804
        i32.const 0
        i32.const 1057784
        i32.store offset=1057792
        i32.const 0
        i32.const 1057800
        i32.store offset=1057812
        i32.const 0
        i32.const 1057792
        i32.store offset=1057800
        i32.const 0
        i32.const 1057808
        i32.store offset=1057820
        i32.const 0
        i32.const 1057800
        i32.store offset=1057808
        i32.const 0
        i32.const 1057816
        i32.store offset=1057828
        i32.const 0
        i32.const 1057808
        i32.store offset=1057816
        i32.const 0
        i32.const 1057824
        i32.store offset=1057836
        i32.const 0
        i32.const 1057816
        i32.store offset=1057824
        i32.const 0
        i32.const 1057832
        i32.store offset=1057844
        i32.const 0
        i32.const 1057824
        i32.store offset=1057832
        i32.const 0
        i32.const 1057840
        i32.store offset=1057852
        i32.const 0
        i32.const 1057832
        i32.store offset=1057840
        i32.const 0
        i32.const 1057848
        i32.store offset=1057860
        i32.const 0
        i32.const 1057840
        i32.store offset=1057848
        i32.const 0
        i32.const 1057856
        i32.store offset=1057868
        i32.const 0
        i32.const 1057848
        i32.store offset=1057856
        i32.const 0
        i32.const 1057864
        i32.store offset=1057876
        i32.const 0
        i32.const 1057856
        i32.store offset=1057864
        i32.const 0
        i32.const 1057872
        i32.store offset=1057884
        i32.const 0
        i32.const 1057864
        i32.store offset=1057872
        i32.const 0
        i32.const 1057880
        i32.store offset=1057892
        i32.const 0
        i32.const 1057872
        i32.store offset=1057880
        i32.const 0
        i32.const 1057880
        i32.store offset=1057888
        call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
        local.tee 3
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 4
        i32.const 20
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 7
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 8
        i32.const 0
        local.get 6
        local.get 6
        call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
        local.tee 0
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.get 0
        i32.sub
        local.tee 10
        call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
        local.tee 0
        i32.store offset=1057916
        i32.const 0
        local.get 3
        local.get 5
        i32.add
        local.get 8
        local.get 4
        local.get 7
        i32.add
        i32.add
        local.get 10
        i32.add
        i32.sub
        local.tee 6
        i32.store offset=1057908
        local.get 0
        local.get 6
        i32.const 1
        i32.or
        i32.store offset=4
        call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
        local.tee 3
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 4
        i32.const 20
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 5
        i32.const 16
        i32.const 8
        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
        local.set 7
        local.get 0
        local.get 6
        call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
        local.get 7
        local.get 5
        local.get 4
        local.get 3
        i32.sub
        i32.add
        i32.add
        i32.store offset=4
        i32.const 0
        i32.const 2097152
        i32.store offset=1057928
      end
      i32.const 0
      local.set 6
      i32.const 0
      i32.load offset=1057908
      local.tee 0
      local.get 2
      i32.le_u
      br_if 0 (;@1;)
      i32.const 0
      local.get 0
      local.get 2
      i32.sub
      local.tee 6
      i32.store offset=1057908
      i32.const 0
      i32.const 0
      i32.load offset=1057916
      local.tee 0
      local.get 2
      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
      local.tee 3
      i32.store offset=1057916
      local.get 3
      local.get 6
      i32.const 1
      i32.or
      i32.store offset=4
      local.get 0
      local.get 2
      call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a
      local.get 0
      call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
      local.set 6
    end
    local.get 1
    i32.const 16
    i32.add
    global.set 0
    local.get 6)
  (func $dlmalloc::dlmalloc::Dlmalloc<A>::dispose_chunk::ha50595baecb6fca0 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    local.get 0
    local.get 1
    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $dlmalloc::dlmalloc::Chunk::pinuse::h031627e231cdc5f4
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
              br_if 0 (;@5;)
              local.get 3
              local.get 1
              i32.add
              local.set 1
              local.get 0
              local.get 3
              call $dlmalloc::dlmalloc::Chunk::minus_offset::h5ff294662793880c
              local.tee 0
              i32.const 0
              i32.load offset=1057912
              i32.ne
              br_if 1 (;@4;)
              local.get 2
              i32.load offset=4
              i32.const 3
              i32.and
              i32.const 3
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1057904
              local.get 0
              local.get 1
              local.get 2
              call $dlmalloc::dlmalloc::Chunk::set_free_with_pinuse::h28f791aa3db5b3cf
              return
            end
            i32.const 1057488
            local.get 0
            local.get 3
            i32.sub
            local.get 3
            local.get 1
            i32.add
            i32.const 16
            i32.add
            local.tee 0
            call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::free::h86340b43ec294341
            i32.eqz
            br_if 2 (;@2;)
            i32.const 0
            i32.const 0
            i32.load offset=1057920
            local.get 0
            i32.sub
            i32.store offset=1057920
            return
          end
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 0
            call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 0
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.tee 5
            i32.eq
            br_if 0 (;@4;)
            local.get 5
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 5
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1057896
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1057896
        end
        block  ;; label = @3
          local.get 2
          call $dlmalloc::dlmalloc::Chunk::cinuse::h9e5860a710e1f9c9
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          local.get 2
          call $dlmalloc::dlmalloc::Chunk::set_free_with_pinuse::h28f791aa3db5b3cf
          br 2 (;@1;)
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 0
            i32.load offset=1057916
            i32.eq
            br_if 0 (;@4;)
            local.get 2
            i32.const 0
            i32.load offset=1057912
            i32.ne
            br_if 1 (;@3;)
            i32.const 0
            local.get 0
            i32.store offset=1057912
            i32.const 0
            i32.const 0
            i32.load offset=1057904
            local.get 1
            i32.add
            local.tee 1
            i32.store offset=1057904
            local.get 0
            local.get 1
            call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
            return
          end
          i32.const 0
          local.get 0
          i32.store offset=1057916
          i32.const 0
          i32.const 0
          i32.load offset=1057908
          local.get 1
          i32.add
          local.tee 1
          i32.store offset=1057908
          local.get 0
          local.get 1
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 0
          i32.const 0
          i32.load offset=1057912
          i32.ne
          br_if 1 (;@2;)
          i32.const 0
          i32.const 0
          i32.store offset=1057904
          i32.const 0
          i32.const 0
          i32.store offset=1057912
          return
        end
        local.get 2
        call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
        local.tee 3
        local.get 1
        i32.add
        local.set 1
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 2
            call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 2
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 2
            i32.const 8
            i32.add
            i32.load
            local.tee 2
            i32.eq
            br_if 0 (;@4;)
            local.get 2
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 2
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1057896
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1057896
        end
        local.get 0
        local.get 1
        call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
        local.get 0
        i32.const 0
        i32.load offset=1057912
        i32.ne
        br_if 1 (;@1;)
        i32.const 0
        local.get 1
        i32.store offset=1057904
      end
      return
    end
    block  ;; label = @1
      local.get 1
      i32.const 256
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      call $dlmalloc::dlmalloc::Dlmalloc<A>::insert_large_chunk::hdf12b63cafdb133b
      return
    end
    local.get 1
    i32.const -8
    i32.and
    i32.const 1057632
    i32.add
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1057896
        local.tee 3
        i32.const 1
        local.get 1
        i32.const 3
        i32.shr_u
        i32.shl
        local.tee 1
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 2
        i32.load offset=8
        local.set 1
        br 1 (;@1;)
      end
      i32.const 0
      local.get 3
      local.get 1
      i32.or
      i32.store offset=1057896
      local.get 2
      local.set 1
    end
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 1
    local.get 0
    i32.store offset=12
    local.get 0
    local.get 2
    i32.store offset=12
    local.get 0
    local.get 1
    i32.store offset=8)
  (func $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e (type 0) (param i32)
    (local i32 i32 i32 i32 i32)
    local.get 0
    i32.load offset=24
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $dlmalloc::dlmalloc::TreeChunk::next::h00c76645d0f5976a
          local.get 0
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          i32.const 20
          i32.const 16
          local.get 0
          i32.const 20
          i32.add
          local.tee 2
          i32.load
          local.tee 3
          select
          i32.add
          i32.load
          local.tee 4
          br_if 1 (;@2;)
          i32.const 0
          local.set 3
          br 2 (;@1;)
        end
        local.get 0
        call $dlmalloc::dlmalloc::TreeChunk::prev::h830942eb7f12976d
        local.tee 4
        local.get 0
        call $dlmalloc::dlmalloc::TreeChunk::next::h00c76645d0f5976a
        local.tee 3
        call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
        i32.store offset=12
        local.get 3
        local.get 4
        call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
        i32.store offset=8
        br 1 (;@1;)
      end
      local.get 2
      local.get 0
      i32.const 16
      i32.add
      local.get 3
      select
      local.set 2
      loop  ;; label = @2
        local.get 2
        local.set 5
        block  ;; label = @3
          local.get 4
          local.tee 3
          i32.const 20
          i32.add
          local.tee 2
          i32.load
          local.tee 4
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          local.set 2
          local.get 3
          i32.load offset=16
          local.set 4
        end
        local.get 4
        br_if 0 (;@2;)
      end
      local.get 5
      i32.const 0
      i32.store
    end
    block  ;; label = @1
      local.get 1
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          i32.load offset=28
          i32.const 2
          i32.shl
          i32.const 1057488
          i32.add
          local.tee 4
          i32.load
          local.get 0
          i32.eq
          br_if 0 (;@3;)
          local.get 1
          i32.const 16
          i32.const 20
          local.get 1
          i32.load offset=16
          local.get 0
          i32.eq
          select
          i32.add
          local.get 3
          i32.store
          local.get 3
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
        local.get 4
        local.get 3
        i32.store
        local.get 3
        br_if 0 (;@2;)
        i32.const 0
        i32.const 0
        i32.load offset=1057900
        i32.const -2
        local.get 0
        i32.load offset=28
        i32.rotl
        i32.and
        i32.store offset=1057900
        return
      end
      local.get 3
      local.get 1
      i32.store offset=24
      block  ;; label = @2
        local.get 0
        i32.load offset=16
        local.tee 4
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 4
        i32.store offset=16
        local.get 4
        local.get 3
        i32.store offset=24
      end
      local.get 0
      i32.const 20
      i32.add
      i32.load
      local.tee 4
      i32.eqz
      br_if 0 (;@1;)
      local.get 3
      i32.const 20
      i32.add
      local.get 4
      i32.store
      local.get 4
      local.get 3
      i32.store offset=24
      return
    end)
  (func $dlmalloc::dlmalloc::Dlmalloc<A>::insert_large_chunk::hdf12b63cafdb133b (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32)
    i32.const 0
    local.set 2
    block  ;; label = @1
      local.get 1
      i32.const 256
      i32.lt_u
      br_if 0 (;@1;)
      i32.const 31
      local.set 2
      local.get 1
      i32.const 16777215
      i32.gt_u
      br_if 0 (;@1;)
      local.get 1
      i32.const 6
      local.get 1
      i32.const 8
      i32.shr_u
      i32.clz
      local.tee 2
      i32.sub
      i32.shr_u
      i32.const 1
      i32.and
      local.get 2
      i32.const 1
      i32.shl
      i32.sub
      i32.const 62
      i32.add
      local.set 2
    end
    local.get 0
    i64.const 0
    i64.store offset=16 align=4
    local.get 0
    local.get 2
    i32.store offset=28
    local.get 2
    i32.const 2
    i32.shl
    i32.const 1057488
    i32.add
    local.set 3
    local.get 0
    call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              i32.load offset=1057900
              local.tee 5
              i32.const 1
              local.get 2
              i32.shl
              local.tee 6
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 3
              i32.load
              local.set 5
              local.get 2
              call $dlmalloc::dlmalloc::leftshift_for_tree_index::h2319f6a1e784a603
              local.set 2
              local.get 5
              call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
              call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
              local.get 1
              i32.ne
              br_if 1 (;@4;)
              local.get 5
              local.set 2
              br 2 (;@3;)
            end
            i32.const 0
            local.get 5
            local.get 6
            i32.or
            i32.store offset=1057900
            local.get 3
            local.get 0
            i32.store
            local.get 0
            local.get 3
            i32.store offset=24
            br 3 (;@1;)
          end
          local.get 1
          local.get 2
          i32.shl
          local.set 3
          loop  ;; label = @4
            local.get 5
            local.get 3
            i32.const 29
            i32.shr_u
            i32.const 4
            i32.and
            i32.add
            i32.const 16
            i32.add
            local.tee 6
            i32.load
            local.tee 2
            i32.eqz
            br_if 2 (;@2;)
            local.get 3
            i32.const 1
            i32.shl
            local.set 3
            local.get 2
            local.set 5
            local.get 2
            call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
            call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
            local.get 1
            i32.ne
            br_if 0 (;@4;)
          end
        end
        local.get 2
        call $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f
        local.tee 2
        i32.load offset=8
        local.tee 3
        local.get 4
        i32.store offset=12
        local.get 2
        local.get 4
        i32.store offset=8
        local.get 4
        local.get 2
        i32.store offset=12
        local.get 4
        local.get 3
        i32.store offset=8
        local.get 0
        i32.const 0
        i32.store offset=24
        return
      end
      local.get 6
      local.get 0
      i32.store
      local.get 0
      local.get 5
      i32.store offset=24
    end
    local.get 4
    local.get 4
    i32.store offset=8
    local.get 4
    local.get 4
    i32.store offset=12)
  (func $dlmalloc::dlmalloc::Dlmalloc<A>::release_unused_segments::he6aa823ad463a901 (type 11) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    i32.const 0
    local.set 0
    i32.const 0
    local.set 1
    block  ;; label = @1
      i32.const 0
      i32.load offset=1057624
      local.tee 2
      i32.eqz
      br_if 0 (;@1;)
      i32.const 1057616
      local.set 3
      i32.const 0
      local.set 1
      i32.const 0
      local.set 0
      loop  ;; label = @2
        local.get 2
        local.tee 4
        i32.load offset=8
        local.set 2
        local.get 4
        i32.load offset=4
        local.set 5
        local.get 4
        i32.load
        local.set 6
        block  ;; label = @3
          block  ;; label = @4
            i32.const 1057488
            local.get 4
            i32.const 12
            i32.add
            i32.load
            i32.const 1
            i32.shr_u
            call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::can_release_part::h638a2c6d1b0640ff
            i32.eqz
            br_if 0 (;@4;)
            local.get 4
            call $dlmalloc::dlmalloc::Segment::is_extern::h4499bcf5769b22f0
            br_if 0 (;@4;)
            local.get 6
            local.get 6
            call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
            local.tee 7
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.get 7
            i32.sub
            i32.add
            local.tee 7
            call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
            local.set 8
            call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
            local.tee 9
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.set 10
            i32.const 20
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.set 11
            i32.const 16
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.set 12
            local.get 7
            call $dlmalloc::dlmalloc::Chunk::inuse::h7a66feebace6547a
            br_if 0 (;@4;)
            local.get 7
            local.get 8
            i32.add
            local.get 6
            local.get 9
            local.get 5
            i32.add
            local.get 10
            local.get 11
            i32.add
            local.get 12
            i32.add
            i32.sub
            i32.add
            i32.lt_u
            br_if 0 (;@4;)
            block  ;; label = @5
              block  ;; label = @6
                local.get 7
                i32.const 0
                i32.load offset=1057912
                i32.eq
                br_if 0 (;@6;)
                local.get 7
                call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
                br 1 (;@5;)
              end
              i32.const 0
              i32.const 0
              i32.store offset=1057904
              i32.const 0
              i32.const 0
              i32.store offset=1057912
            end
            block  ;; label = @5
              i32.const 1057488
              local.get 6
              local.get 5
              call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::free::h86340b43ec294341
              br_if 0 (;@5;)
              local.get 7
              local.get 8
              call $dlmalloc::dlmalloc::Dlmalloc<A>::insert_large_chunk::hdf12b63cafdb133b
              br 1 (;@4;)
            end
            i32.const 0
            i32.const 0
            i32.load offset=1057920
            local.get 5
            i32.sub
            i32.store offset=1057920
            local.get 3
            local.get 2
            i32.store offset=8
            local.get 5
            local.get 1
            i32.add
            local.set 1
            br 1 (;@3;)
          end
          local.get 4
          local.set 3
        end
        local.get 0
        i32.const 1
        i32.add
        local.set 0
        local.get 2
        br_if 0 (;@2;)
      end
    end
    i32.const 0
    local.get 0
    i32.const 4095
    local.get 0
    i32.const 4095
    i32.gt_u
    select
    i32.store offset=1057936
    local.get 1)
  (func $dlmalloc::dlmalloc::Dlmalloc<A>::free::h9921c2b76eae2534 (type 0) (param i32)
    (local i32 i32 i32 i32 i32 i32)
    local.get 0
    call $dlmalloc::dlmalloc::Chunk::from_mem::hfab12920c40b4363
    local.set 0
    local.get 0
    local.get 0
    call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
    local.tee 1
    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $dlmalloc::dlmalloc::Chunk::pinuse::h031627e231cdc5f4
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
              br_if 0 (;@5;)
              local.get 3
              local.get 1
              i32.add
              local.set 1
              local.get 0
              local.get 3
              call $dlmalloc::dlmalloc::Chunk::minus_offset::h5ff294662793880c
              local.tee 0
              i32.const 0
              i32.load offset=1057912
              i32.ne
              br_if 1 (;@4;)
              local.get 2
              i32.load offset=4
              i32.const 3
              i32.and
              i32.const 3
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1057904
              local.get 0
              local.get 1
              local.get 2
              call $dlmalloc::dlmalloc::Chunk::set_free_with_pinuse::h28f791aa3db5b3cf
              return
            end
            i32.const 1057488
            local.get 0
            local.get 3
            i32.sub
            local.get 3
            local.get 1
            i32.add
            i32.const 16
            i32.add
            local.tee 0
            call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::free::h86340b43ec294341
            i32.eqz
            br_if 2 (;@2;)
            i32.const 0
            i32.const 0
            i32.load offset=1057920
            local.get 0
            i32.sub
            i32.store offset=1057920
            return
          end
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 0
            call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 0
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.tee 5
            i32.eq
            br_if 0 (;@4;)
            local.get 5
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 5
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1057896
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1057896
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            call $dlmalloc::dlmalloc::Chunk::cinuse::h9e5860a710e1f9c9
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            local.get 1
            local.get 2
            call $dlmalloc::dlmalloc::Chunk::set_free_with_pinuse::h28f791aa3db5b3cf
            br 1 (;@3;)
          end
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 2
                  i32.const 0
                  i32.load offset=1057916
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 2
                  i32.const 0
                  i32.load offset=1057912
                  i32.ne
                  br_if 1 (;@6;)
                  i32.const 0
                  local.get 0
                  i32.store offset=1057912
                  i32.const 0
                  i32.const 0
                  i32.load offset=1057904
                  local.get 1
                  i32.add
                  local.tee 1
                  i32.store offset=1057904
                  local.get 0
                  local.get 1
                  call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
                  return
                end
                i32.const 0
                local.get 0
                i32.store offset=1057916
                i32.const 0
                i32.const 0
                i32.load offset=1057908
                local.get 1
                i32.add
                local.tee 1
                i32.store offset=1057908
                local.get 0
                local.get 1
                i32.const 1
                i32.or
                i32.store offset=4
                local.get 0
                i32.const 0
                i32.load offset=1057912
                i32.eq
                br_if 1 (;@5;)
                br 2 (;@4;)
              end
              local.get 2
              call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
              local.tee 3
              local.get 1
              i32.add
              local.set 1
              block  ;; label = @6
                block  ;; label = @7
                  local.get 3
                  i32.const 256
                  i32.lt_u
                  br_if 0 (;@7;)
                  local.get 2
                  call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
                  br 1 (;@6;)
                end
                block  ;; label = @7
                  local.get 2
                  i32.const 12
                  i32.add
                  i32.load
                  local.tee 4
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.load
                  local.tee 2
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 2
                  local.get 4
                  i32.store offset=12
                  local.get 4
                  local.get 2
                  i32.store offset=8
                  br 1 (;@6;)
                end
                i32.const 0
                i32.const 0
                i32.load offset=1057896
                i32.const -2
                local.get 3
                i32.const 3
                i32.shr_u
                i32.rotl
                i32.and
                i32.store offset=1057896
              end
              local.get 0
              local.get 1
              call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
              local.get 0
              i32.const 0
              i32.load offset=1057912
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1057904
              br 3 (;@2;)
            end
            i32.const 0
            i32.const 0
            i32.store offset=1057904
            i32.const 0
            i32.const 0
            i32.store offset=1057912
          end
          i32.const 0
          i32.load offset=1057928
          local.get 1
          i32.ge_u
          br_if 1 (;@2;)
          call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
          local.tee 0
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 1
          i32.const 20
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 2
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 3
          i32.const 0
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          i32.const 2
          i32.shl
          i32.sub
          local.tee 4
          local.get 0
          local.get 3
          local.get 1
          local.get 2
          i32.add
          i32.add
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 0
          local.get 4
          local.get 0
          i32.lt_u
          select
          i32.eqz
          br_if 1 (;@2;)
          i32.const 0
          i32.load offset=1057916
          i32.eqz
          br_if 1 (;@2;)
          call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
          local.tee 0
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 1
          i32.const 20
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 3
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 4
          i32.const 0
          local.set 2
          block  ;; label = @4
            i32.const 0
            i32.load offset=1057908
            local.tee 5
            local.get 4
            local.get 3
            local.get 1
            local.get 0
            i32.sub
            i32.add
            i32.add
            local.tee 0
            i32.le_u
            br_if 0 (;@4;)
            local.get 5
            local.get 0
            i32.const -1
            i32.xor
            i32.add
            i32.const -65536
            i32.and
            local.set 3
            i32.const 0
            i32.load offset=1057916
            local.set 1
            i32.const 1057616
            local.set 0
            block  ;; label = @5
              loop  ;; label = @6
                block  ;; label = @7
                  local.get 0
                  i32.load
                  local.get 1
                  i32.gt_u
                  br_if 0 (;@7;)
                  local.get 0
                  call $dlmalloc::dlmalloc::Segment::top::h0fee0944dfd91eaf
                  local.get 1
                  i32.gt_u
                  br_if 2 (;@5;)
                end
                local.get 0
                i32.load offset=8
                local.tee 0
                br_if 0 (;@6;)
              end
              i32.const 0
              local.set 0
            end
            i32.const 0
            local.set 2
            local.get 0
            call $dlmalloc::dlmalloc::Segment::is_extern::h4499bcf5769b22f0
            br_if 0 (;@4;)
            i32.const 1057488
            local.get 0
            i32.const 12
            i32.add
            i32.load
            i32.const 1
            i32.shr_u
            call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::can_release_part::h638a2c6d1b0640ff
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            i32.load offset=4
            local.get 3
            i32.lt_u
            br_if 0 (;@4;)
            i32.const 1057616
            local.set 1
            loop  ;; label = @5
              local.get 0
              local.get 1
              call $dlmalloc::dlmalloc::Segment::holds::h0fb9a9c5bb17b581
              br_if 1 (;@4;)
              local.get 1
              i32.load offset=8
              local.tee 1
              br_if 0 (;@5;)
            end
            i32.const 1057488
            local.get 0
            i32.load
            local.get 0
            i32.load offset=4
            local.tee 1
            local.get 1
            local.get 3
            i32.sub
            call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::free_part::hf1736efad8f20a0e
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            local.get 0
            i32.load offset=4
            local.get 3
            i32.sub
            i32.store offset=4
            i32.const 0
            i32.const 0
            i32.load offset=1057920
            local.get 3
            i32.sub
            i32.store offset=1057920
            i32.const 0
            i32.load offset=1057908
            local.set 1
            i32.const 0
            i32.load offset=1057916
            local.set 0
            i32.const 0
            local.get 0
            local.get 0
            call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
            local.tee 2
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.get 2
            i32.sub
            local.tee 2
            call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
            local.tee 0
            i32.store offset=1057916
            i32.const 0
            local.get 1
            local.get 3
            local.get 2
            i32.add
            i32.sub
            local.tee 1
            i32.store offset=1057908
            local.get 0
            local.get 1
            i32.const 1
            i32.or
            i32.store offset=4
            call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
            local.tee 2
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.set 4
            i32.const 20
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.set 5
            i32.const 16
            i32.const 8
            call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
            local.set 6
            local.get 0
            local.get 1
            call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
            local.get 6
            local.get 5
            local.get 4
            local.get 2
            i32.sub
            i32.add
            i32.add
            i32.store offset=4
            i32.const 0
            i32.const 2097152
            i32.store offset=1057928
            local.get 3
            local.set 2
          end
          local.get 2
          i32.const 0
          call $dlmalloc::dlmalloc::Dlmalloc<A>::release_unused_segments::he6aa823ad463a901
          i32.sub
          i32.ne
          br_if 1 (;@2;)
          i32.const 0
          i32.load offset=1057908
          i32.const 0
          i32.load offset=1057928
          i32.le_u
          br_if 1 (;@2;)
          i32.const 0
          i32.const -1
          i32.store offset=1057928
          return
        end
        local.get 1
        i32.const 256
        i32.lt_u
        br_if 1 (;@1;)
        local.get 0
        local.get 1
        call $dlmalloc::dlmalloc::Dlmalloc<A>::insert_large_chunk::hdf12b63cafdb133b
        i32.const 0
        i32.const 0
        i32.load offset=1057936
        i32.const -1
        i32.add
        local.tee 0
        i32.store offset=1057936
        local.get 0
        br_if 0 (;@2;)
        call $dlmalloc::dlmalloc::Dlmalloc<A>::release_unused_segments::he6aa823ad463a901
        drop
        return
      end
      return
    end
    local.get 1
    i32.const -8
    i32.and
    i32.const 1057632
    i32.add
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1057896
        local.tee 3
        i32.const 1
        local.get 1
        i32.const 3
        i32.shr_u
        i32.shl
        local.tee 1
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 2
        i32.load offset=8
        local.set 1
        br 1 (;@1;)
      end
      i32.const 0
      local.get 3
      local.get 1
      i32.or
      i32.store offset=1057896
      local.get 2
      local.set 1
    end
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 1
    local.get 0
    i32.store offset=12
    local.get 0
    local.get 2
    i32.store offset=12
    local.get 0
    local.get 1
    i32.store offset=8)
  (func $dlmalloc::dlmalloc::Dlmalloc<A>::init_top::hd19f596040fbda30 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    local.get 0
    local.get 0
    call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0
    local.tee 2
    i32.const 8
    call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
    local.get 2
    i32.sub
    local.tee 2
    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
    local.set 0
    i32.const 0
    local.get 1
    local.get 2
    i32.sub
    local.tee 1
    i32.store offset=1057908
    i32.const 0
    local.get 0
    i32.store offset=1057916
    local.get 0
    local.get 1
    i32.const 1
    i32.or
    i32.store offset=4
    call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
    local.tee 2
    i32.const 8
    call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
    local.set 3
    i32.const 20
    i32.const 8
    call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
    local.set 4
    i32.const 16
    i32.const 8
    call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
    local.set 5
    local.get 0
    local.get 1
    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
    local.get 5
    local.get 4
    local.get 3
    local.get 2
    i32.sub
    i32.add
    i32.add
    i32.store offset=4
    i32.const 0
    i32.const 2097152
    i32.store offset=1057928)
  (func $<std::io::error::Error_as_core::fmt::Debug>::fmt::hf93b1043f036c001 (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    call $std::io::error::<impl_core::fmt::Debug_for_std::io::error::repr_unpacked::Repr>::fmt::h88f52e94995b1dcf)
  (func $std::io::error::<impl_core::fmt::Debug_for_std::io::error::repr_unpacked::Repr>::fmt::h88f52e94995b1dcf (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 0
                i32.load8_u
                br_table 0 (;@6;) 1 (;@5;) 2 (;@4;) 3 (;@3;) 0 (;@6;)
              end
              local.get 2
              local.get 0
              i32.load offset=4
              i32.store offset=12
              local.get 2
              i32.const 16
              i32.add
              local.get 1
              i32.const 1052656
              i32.const 2
              call $core::fmt::Formatter::debug_struct::h8a11139ff0c9eae6
              local.get 2
              i32.const 16
              i32.add
              i32.const 1052658
              i32.const 4
              local.get 2
              i32.const 12
              i32.add
              i32.const 1052664
              call $core::fmt::builders::DebugStruct::field::h12418a0ca165575b
              local.set 0
              local.get 2
              i32.const 40
              i32.store8 offset=31
              local.get 0
              i32.const 1052608
              i32.const 4
              local.get 2
              i32.const 31
              i32.add
              i32.const 1052612
              call $core::fmt::builders::DebugStruct::field::h12418a0ca165575b
              local.set 1
              i32.const 20
              i32.const 1
              call $__rust_alloc
              local.tee 0
              i32.eqz
              br_if 4 (;@1;)
              local.get 0
              i32.const 16
              i32.add
              i32.const 0
              i32.load offset=1053615 align=1
              i32.store align=1
              local.get 0
              i32.const 8
              i32.add
              i32.const 0
              i64.load offset=1053607 align=1
              i64.store align=1
              local.get 0
              i32.const 0
              i64.load offset=1053599 align=1
              i64.store align=1
              local.get 2
              i32.const 20
              i32.store offset=40
              local.get 2
              local.get 0
              i32.store offset=36
              local.get 2
              i32.const 20
              i32.store offset=32
              local.get 1
              i32.const 1052628
              i32.const 7
              local.get 2
              i32.const 32
              i32.add
              i32.const 1052680
              call $core::fmt::builders::DebugStruct::field::h12418a0ca165575b
              call $core::fmt::builders::DebugStruct::finish::h28db02fb670680d5
              local.set 0
              local.get 2
              i32.load offset=32
              local.tee 1
              i32.eqz
              br_if 3 (;@2;)
              local.get 2
              i32.load offset=36
              local.get 1
              i32.const 1
              call $__rust_dealloc
              br 3 (;@2;)
            end
            local.get 2
            local.get 0
            i32.load8_u offset=1
            i32.store8 offset=16
            local.get 2
            i32.const 32
            i32.add
            local.get 1
            i32.const 1052652
            i32.const 4
            call $core::fmt::Formatter::debug_tuple::h959e9f235d4eb107
            local.get 2
            i32.const 32
            i32.add
            local.get 2
            i32.const 16
            i32.add
            i32.const 1052612
            call $core::fmt::builders::DebugTuple::field::he30af5de70b50698
            call $core::fmt::builders::DebugTuple::finish::hbbc9fdf689f78815
            local.set 0
            br 2 (;@2;)
          end
          local.get 0
          i32.load offset=4
          local.set 0
          local.get 2
          i32.const 32
          i32.add
          local.get 1
          i32.const 1052603
          i32.const 5
          call $core::fmt::Formatter::debug_struct::h8a11139ff0c9eae6
          local.get 2
          i32.const 32
          i32.add
          i32.const 1052608
          i32.const 4
          local.get 0
          i32.const 8
          i32.add
          i32.const 1052612
          call $core::fmt::builders::DebugStruct::field::h12418a0ca165575b
          i32.const 1052628
          i32.const 7
          local.get 0
          i32.const 1052636
          call $core::fmt::builders::DebugStruct::field::h12418a0ca165575b
          call $core::fmt::builders::DebugStruct::finish::h28db02fb670680d5
          local.set 0
          br 1 (;@2;)
        end
        local.get 2
        local.get 0
        i32.load offset=4
        local.tee 0
        i32.const 8
        i32.add
        i32.store offset=16
        local.get 2
        local.get 0
        i32.store offset=32
        local.get 1
        i32.const 1053040
        i32.const 6
        i32.const 1052608
        i32.const 4
        local.get 2
        i32.const 16
        i32.add
        i32.const 1053024
        i32.const 1053046
        i32.const 5
        local.get 2
        i32.const 32
        i32.add
        i32.const 1053052
        call $core::fmt::Formatter::debug_struct_field2_finish::h953b04c6ca022536
        local.set 0
      end
      local.get 2
      i32.const 48
      i32.add
      global.set 0
      local.get 0
      return
    end
    i32.const 20
    i32.const 1
    call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
    unreachable)
  (func $std::sys_common::backtrace::__rust_end_short_backtrace::h227361e053771d9e (type 0) (param i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    global.set 0
    local.get 1
    i32.const 8
    i32.add
    local.get 0
    i32.const 8
    i32.add
    i32.load
    i32.store
    local.get 1
    local.get 0
    i64.load align=4
    i64.store
    local.get 1
    call $std::panicking::begin_panic_handler::__closure__::he16e52e9a7dddeb1
    unreachable)
  (func $std::panicking::begin_panic_handler::__closure__::he16e52e9a7dddeb1 (type 0) (param i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    global.set 0
    local.get 0
    i32.load
    local.tee 2
    i32.const 20
    i32.add
    i32.load
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 12
            i32.add
            i32.load
            br_table 0 (;@4;) 1 (;@3;) 3 (;@1;)
          end
          local.get 3
          br_if 2 (;@1;)
          i32.const 1052560
          local.set 2
          i32.const 0
          local.set 3
          br 1 (;@2;)
        end
        local.get 3
        br_if 1 (;@1;)
        local.get 2
        i32.load offset=8
        local.tee 2
        i32.load offset=4
        local.set 3
        local.get 2
        i32.load
        local.set 2
      end
      local.get 1
      local.get 3
      i32.store offset=4
      local.get 1
      local.get 2
      i32.store
      local.get 1
      i32.const 1052976
      local.get 0
      i32.load offset=4
      local.tee 2
      call $core::panic::panic_info::PanicInfo::message::h39f920514669bb09
      local.get 0
      i32.load offset=8
      local.get 2
      call $core::panic::panic_info::PanicInfo::can_unwind::he45aae146040bf73
      call $std::panicking::rust_panic_with_hook::hd000e9fb43b5781d
      unreachable
    end
    local.get 1
    i32.const 0
    i32.store offset=4
    local.get 1
    local.get 2
    i32.store offset=12
    local.get 1
    i32.const 1052956
    local.get 0
    i32.load offset=4
    local.tee 2
    call $core::panic::panic_info::PanicInfo::message::h39f920514669bb09
    local.get 0
    i32.load offset=8
    local.get 2
    call $core::panic::panic_info::PanicInfo::can_unwind::he45aae146040bf73
    call $std::panicking::rust_panic_with_hook::hd000e9fb43b5781d
    unreachable)
  (func $std::alloc::default_alloc_error_hook::h01841b0b6d4df8eb (type 3) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      i32.const 0
      i32.load8_u offset=1057460
      i32.eqz
      br_if 0 (;@1;)
      local.get 2
      i32.const 20
      i32.add
      i32.const 2
      i32.store
      local.get 2
      i32.const 28
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 1052732
      i32.store offset=16
      local.get 2
      i32.const 0
      i32.store offset=8
      local.get 2
      i32.const 1
      i32.store offset=36
      local.get 2
      local.get 0
      i32.store offset=44
      local.get 2
      local.get 2
      i32.const 32
      i32.add
      i32.store offset=24
      local.get 2
      local.get 2
      i32.const 44
      i32.add
      i32.store offset=32
      local.get 2
      i32.const 8
      i32.add
      i32.const 1052772
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    local.get 2
    i32.const 48
    i32.add
    global.set 0)
  (func $__rdl_alloc (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    call $dlmalloc::Dlmalloc<A>::malloc::hd8efb8c25e917a05)
  (func $__rdl_dealloc (type 6) (param i32 i32 i32)
    local.get 0
    call $dlmalloc::dlmalloc::Dlmalloc<A>::free::h9921c2b76eae2534)
  (func $__rdl_realloc (type 9) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 9
            i32.lt_u
            br_if 0 (;@4;)
            local.get 3
            local.get 2
            call $dlmalloc::Dlmalloc<A>::malloc::hd8efb8c25e917a05
            local.tee 2
            br_if 1 (;@3;)
            i32.const 0
            return
          end
          call $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5
          local.tee 1
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 4
          i32.const 20
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 5
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 6
          i32.const 0
          local.set 2
          i32.const 0
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          i32.const 2
          i32.shl
          i32.sub
          local.tee 7
          local.get 1
          local.get 6
          local.get 4
          local.get 5
          i32.add
          i32.add
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 1
          local.get 7
          local.get 1
          i32.lt_u
          select
          local.get 3
          i32.le_u
          br_if 1 (;@2;)
          i32.const 16
          local.get 3
          i32.const 4
          i32.add
          i32.const 16
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          i32.const -5
          i32.add
          local.get 3
          i32.gt_u
          select
          i32.const 8
          call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
          local.set 4
          local.get 0
          call $dlmalloc::dlmalloc::Chunk::from_mem::hfab12920c40b4363
          local.set 1
          local.get 1
          local.get 1
          call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
          local.tee 5
          call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
          local.set 6
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 1
                          call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
                          br_if 0 (;@11;)
                          local.get 5
                          local.get 4
                          i32.ge_u
                          br_if 1 (;@10;)
                          local.get 6
                          i32.const 0
                          i32.load offset=1057916
                          i32.eq
                          br_if 2 (;@9;)
                          local.get 6
                          i32.const 0
                          i32.load offset=1057912
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 6
                          call $dlmalloc::dlmalloc::Chunk::cinuse::h9e5860a710e1f9c9
                          br_if 7 (;@4;)
                          local.get 6
                          call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
                          local.tee 7
                          local.get 5
                          i32.add
                          local.tee 5
                          local.get 4
                          i32.lt_u
                          br_if 7 (;@4;)
                          local.get 5
                          local.get 4
                          i32.sub
                          local.set 8
                          local.get 7
                          i32.const 256
                          i32.lt_u
                          br_if 4 (;@7;)
                          local.get 6
                          call $dlmalloc::dlmalloc::Dlmalloc<A>::unlink_large_chunk::hb9b09f5b80b6743e
                          br 5 (;@6;)
                        end
                        local.get 1
                        call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
                        local.set 5
                        local.get 4
                        i32.const 256
                        i32.lt_u
                        br_if 6 (;@4;)
                        block  ;; label = @11
                          local.get 5
                          local.get 4
                          i32.const 4
                          i32.add
                          i32.lt_u
                          br_if 0 (;@11;)
                          local.get 5
                          local.get 4
                          i32.sub
                          i32.const 131073
                          i32.lt_u
                          br_if 6 (;@5;)
                        end
                        i32.const 1057488
                        local.get 1
                        local.get 1
                        i32.load
                        local.tee 6
                        i32.sub
                        local.get 5
                        local.get 6
                        i32.add
                        i32.const 16
                        i32.add
                        local.tee 7
                        local.get 4
                        i32.const 31
                        i32.add
                        i32.const 1057488
                        call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::page_size::h244e7e67d7e17c1f
                        call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                        local.tee 5
                        i32.const 1
                        call $<dlmalloc::sys::System_as_dlmalloc::Allocator>::remap::hed1e3b2cb052631c
                        local.tee 4
                        i32.eqz
                        br_if 6 (;@4;)
                        local.get 4
                        local.get 6
                        i32.add
                        local.tee 1
                        local.get 5
                        local.get 6
                        i32.sub
                        local.tee 3
                        i32.const -16
                        i32.add
                        local.tee 2
                        i32.store offset=4
                        call $dlmalloc::dlmalloc::Chunk::fencepost_head::h0aeaad6175aa31b5
                        local.set 0
                        local.get 1
                        local.get 2
                        call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                        local.get 0
                        i32.store offset=4
                        local.get 1
                        local.get 3
                        i32.const -12
                        i32.add
                        call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                        i32.const 0
                        i32.store offset=4
                        i32.const 0
                        i32.const 0
                        i32.load offset=1057920
                        local.get 5
                        local.get 7
                        i32.sub
                        i32.add
                        local.tee 3
                        i32.store offset=1057920
                        i32.const 0
                        i32.const 0
                        i32.load offset=1057932
                        local.tee 2
                        local.get 4
                        local.get 4
                        local.get 2
                        i32.gt_u
                        select
                        i32.store offset=1057932
                        i32.const 0
                        i32.const 0
                        i32.load offset=1057924
                        local.tee 2
                        local.get 3
                        local.get 2
                        local.get 3
                        i32.gt_u
                        select
                        i32.store offset=1057924
                        br 9 (;@1;)
                      end
                      local.get 5
                      local.get 4
                      i32.sub
                      local.tee 5
                      i32.const 16
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      i32.lt_u
                      br_if 4 (;@5;)
                      local.get 1
                      local.get 4
                      call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                      local.set 6
                      local.get 1
                      local.get 4
                      call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
                      local.get 6
                      local.get 5
                      call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
                      local.get 6
                      local.get 5
                      call $dlmalloc::dlmalloc::Dlmalloc<A>::dispose_chunk::ha50595baecb6fca0
                      br 4 (;@5;)
                    end
                    i32.const 0
                    i32.load offset=1057908
                    local.get 5
                    i32.add
                    local.tee 5
                    local.get 4
                    i32.le_u
                    br_if 4 (;@4;)
                    local.get 1
                    local.get 4
                    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                    local.set 6
                    local.get 1
                    local.get 4
                    call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
                    local.get 6
                    local.get 5
                    local.get 4
                    i32.sub
                    local.tee 4
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    i32.const 0
                    local.get 4
                    i32.store offset=1057908
                    i32.const 0
                    local.get 6
                    i32.store offset=1057916
                    br 3 (;@5;)
                  end
                  i32.const 0
                  i32.load offset=1057904
                  local.get 5
                  i32.add
                  local.tee 5
                  local.get 4
                  i32.lt_u
                  br_if 3 (;@4;)
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 5
                      local.get 4
                      i32.sub
                      local.tee 6
                      i32.const 16
                      i32.const 8
                      call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                      i32.ge_u
                      br_if 0 (;@9;)
                      local.get 1
                      local.get 5
                      call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
                      i32.const 0
                      local.set 6
                      i32.const 0
                      local.set 5
                      br 1 (;@8;)
                    end
                    local.get 1
                    local.get 4
                    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                    local.tee 5
                    local.get 6
                    call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                    local.set 7
                    local.get 1
                    local.get 4
                    call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
                    local.get 5
                    local.get 6
                    call $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14
                    local.get 7
                    call $dlmalloc::dlmalloc::Chunk::clear_pinuse::h6d812c2aaa28dc4c
                  end
                  i32.const 0
                  local.get 5
                  i32.store offset=1057912
                  i32.const 0
                  local.get 6
                  i32.store offset=1057904
                  br 2 (;@5;)
                end
                block  ;; label = @7
                  local.get 6
                  i32.const 12
                  i32.add
                  i32.load
                  local.tee 9
                  local.get 6
                  i32.const 8
                  i32.add
                  i32.load
                  local.tee 6
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 6
                  local.get 9
                  i32.store offset=12
                  local.get 9
                  local.get 6
                  i32.store offset=8
                  br 1 (;@6;)
                end
                i32.const 0
                i32.const 0
                i32.load offset=1057896
                i32.const -2
                local.get 7
                i32.const 3
                i32.shr_u
                i32.rotl
                i32.and
                i32.store offset=1057896
              end
              block  ;; label = @6
                local.get 8
                i32.const 16
                i32.const 8
                call $dlmalloc::dlmalloc::align_up::h56307c410374cfa0
                i32.lt_u
                br_if 0 (;@6;)
                local.get 1
                local.get 4
                call $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794
                local.set 5
                local.get 1
                local.get 4
                call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
                local.get 5
                local.get 8
                call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
                local.get 5
                local.get 8
                call $dlmalloc::dlmalloc::Dlmalloc<A>::dispose_chunk::ha50595baecb6fca0
                br 1 (;@5;)
              end
              local.get 1
              local.get 5
              call $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b
            end
            local.get 1
            br_if 3 (;@1;)
          end
          local.get 3
          call $dlmalloc::dlmalloc::Dlmalloc<A>::malloc::h6c2f92ea78996c54
          local.tee 4
          i32.eqz
          br_if 1 (;@2;)
          local.get 4
          local.get 0
          local.get 1
          call $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85
          i32.const -8
          i32.const -4
          local.get 1
          call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
          select
          i32.add
          local.tee 2
          local.get 3
          local.get 2
          local.get 3
          i32.lt_u
          select
          call $memcpy
          local.set 3
          local.get 0
          call $dlmalloc::dlmalloc::Dlmalloc<A>::free::h9921c2b76eae2534
          local.get 3
          return
        end
        local.get 2
        local.get 0
        local.get 1
        local.get 3
        local.get 1
        local.get 3
        i32.lt_u
        select
        call $memcpy
        drop
        local.get 0
        call $dlmalloc::dlmalloc::Dlmalloc<A>::free::h9921c2b76eae2534
      end
      local.get 2
      return
    end
    local.get 1
    call $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8
    drop
    local.get 1
    call $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0)
  (func $std::panicking::set_hook::h9a45cfeeb8c46eb6 (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          i32.const 0
          i32.load offset=1057484
          i32.const 2147483647
          i32.and
          i32.eqz
          br_if 0 (;@3;)
          call $std::panicking::panic_count::is_zero_slow_path::h104e28d68691b5d5
          i32.eqz
          br_if 1 (;@2;)
        end
        i32.const 0
        i32.load offset=1057468
        local.set 3
        i32.const 0
        i32.const -1
        i32.store offset=1057468
        local.get 3
        br_if 1 (;@1;)
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              i32.load offset=1057484
              i32.const 2147483647
              i32.and
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1057480
              local.set 4
              i32.const 0
              local.get 1
              i32.store offset=1057480
              i32.const 0
              i32.load offset=1057476
              local.set 3
              i32.const 0
              local.get 0
              i32.store offset=1057476
              br 1 (;@4;)
            end
            call $std::panicking::panic_count::is_zero_slow_path::h104e28d68691b5d5
            local.set 5
            i32.const 0
            i32.load offset=1057480
            local.set 4
            i32.const 0
            local.get 1
            i32.store offset=1057480
            i32.const 0
            i32.load offset=1057476
            local.set 3
            i32.const 0
            local.get 0
            i32.store offset=1057476
            local.get 5
            i32.eqz
            br_if 1 (;@3;)
          end
          i32.const 0
          i32.load offset=1057484
          i32.const 2147483647
          i32.and
          i32.eqz
          br_if 0 (;@3;)
          call $std::panicking::panic_count::is_zero_slow_path::h104e28d68691b5d5
          br_if 0 (;@3;)
          i32.const 0
          i32.const 1
          i32.store8 offset=1057472
        end
        i32.const 0
        i32.const 0
        i32.store offset=1057468
        block  ;; label = @3
          local.get 3
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          local.get 4
          i32.load
          call_indirect (type 0)
          local.get 4
          i32.const 4
          i32.add
          i32.load
          local.tee 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          local.get 1
          local.get 4
          i32.const 8
          i32.add
          i32.load
          call $__rust_dealloc
        end
        local.get 2
        i32.const 32
        i32.add
        global.set 0
        return
      end
      local.get 2
      i32.const 20
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i32.const 28
      i32.add
      i32.const 0
      i32.store
      local.get 2
      i32.const 1052840
      i32.store offset=16
      local.get 2
      i32.const 1052560
      i32.store offset=24
      local.get 2
      i32.const 0
      i32.store offset=8
      local.get 2
      i32.const 8
      i32.add
      i32.const 1052876
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    unreachable
    unreachable)
  (func $rust_begin_unwind (type 0) (param i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        call $core::panic::panic_info::PanicInfo::location::h29f7f91081e7b35d
        local.tee 2
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        call $core::panic::panic_info::PanicInfo::message::h39f920514669bb09
        local.tee 3
        i32.eqz
        br_if 1 (;@1;)
        local.get 1
        local.get 2
        i32.store offset=8
        local.get 1
        local.get 0
        i32.store offset=4
        local.get 1
        local.get 3
        i32.store
        local.get 1
        call $std::sys_common::backtrace::__rust_end_short_backtrace::h227361e053771d9e
        unreachable
      end
      i32.const 1052560
      i32.const 43
      i32.const 1052908
      call $core::panicking::panic::h364c37174a08a6a4
      unreachable
    end
    i32.const 1052560
    i32.const 43
    i32.const 1052892
    call $core::panicking::panic::h364c37174a08a6a4
    unreachable)
  (func $<std::panicking::begin_panic_handler::PanicPayload_as_core::panic::BoxMeUp>::take_box::hbe454e3d7aec7cab (type 3) (param i32 i32)
    (local i32 i32 i32 i64)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      local.get 1
      i32.load offset=4
      br_if 0 (;@1;)
      local.get 1
      i32.load offset=12
      local.set 3
      local.get 2
      i32.const 8
      i32.add
      i32.const 8
      i32.add
      local.tee 4
      i32.const 0
      i32.store
      local.get 2
      i64.const 4294967296
      i64.store offset=8
      local.get 2
      local.get 2
      i32.const 8
      i32.add
      i32.store offset=20
      local.get 2
      i32.const 24
      i32.add
      i32.const 16
      i32.add
      local.get 3
      i32.const 16
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      i32.const 24
      i32.add
      i32.const 8
      i32.add
      local.get 3
      i32.const 8
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      local.get 3
      i64.load align=4
      i64.store offset=24
      local.get 2
      i32.const 20
      i32.add
      i32.const 1052536
      local.get 2
      i32.const 24
      i32.add
      call $core::fmt::write::hb366ddae040241f7
      drop
      local.get 1
      i32.const 8
      i32.add
      local.get 4
      i32.load
      i32.store
      local.get 1
      local.get 2
      i64.load offset=8
      i64.store align=4
    end
    local.get 1
    i64.load align=4
    local.set 5
    local.get 1
    i64.const 4294967296
    i64.store align=4
    local.get 2
    i32.const 24
    i32.add
    i32.const 8
    i32.add
    local.tee 3
    local.get 1
    i32.const 8
    i32.add
    local.tee 1
    i32.load
    i32.store
    local.get 1
    i32.const 0
    i32.store
    local.get 2
    local.get 5
    i64.store offset=24
    block  ;; label = @1
      i32.const 12
      i32.const 4
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 12
      i32.const 4
      call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
      unreachable
    end
    local.get 1
    local.get 2
    i64.load offset=24
    i64.store align=4
    local.get 1
    i32.const 8
    i32.add
    local.get 3
    i32.load
    i32.store
    local.get 0
    i32.const 1052924
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store
    local.get 2
    i32.const 48
    i32.add
    global.set 0)
  (func $<std::panicking::begin_panic_handler::PanicPayload_as_core::panic::BoxMeUp>::get::h72f881a6fbed83d0 (type 3) (param i32 i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      local.get 1
      i32.load offset=4
      br_if 0 (;@1;)
      local.get 1
      i32.load offset=12
      local.set 3
      local.get 2
      i32.const 8
      i32.add
      i32.const 8
      i32.add
      local.tee 4
      i32.const 0
      i32.store
      local.get 2
      i64.const 4294967296
      i64.store offset=8
      local.get 2
      local.get 2
      i32.const 8
      i32.add
      i32.store offset=20
      local.get 2
      i32.const 24
      i32.add
      i32.const 16
      i32.add
      local.get 3
      i32.const 16
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      i32.const 24
      i32.add
      i32.const 8
      i32.add
      local.get 3
      i32.const 8
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      local.get 3
      i64.load align=4
      i64.store offset=24
      local.get 2
      i32.const 20
      i32.add
      i32.const 1052536
      local.get 2
      i32.const 24
      i32.add
      call $core::fmt::write::hb366ddae040241f7
      drop
      local.get 1
      i32.const 8
      i32.add
      local.get 4
      i32.load
      i32.store
      local.get 1
      local.get 2
      i64.load offset=8
      i64.store align=4
    end
    local.get 0
    i32.const 1052924
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store
    local.get 2
    i32.const 48
    i32.add
    global.set 0)
  (func $<std::panicking::begin_panic_handler::StrPanicPayload_as_core::panic::BoxMeUp>::take_box::h606d68c05a7922b1 (type 3) (param i32 i32)
    (local i32 i32)
    local.get 1
    i32.load offset=4
    local.set 2
    local.get 1
    i32.load
    local.set 3
    block  ;; label = @1
      i32.const 8
      i32.const 4
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 8
      i32.const 4
      call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
      unreachable
    end
    local.get 1
    local.get 2
    i32.store offset=4
    local.get 1
    local.get 3
    i32.store
    local.get 0
    i32.const 1052940
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $<std::panicking::begin_panic_handler::StrPanicPayload_as_core::panic::BoxMeUp>::get::hade4f92ab6640ba2 (type 3) (param i32 i32)
    local.get 0
    i32.const 1052940
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $std::panicking::rust_panic_with_hook::hd000e9fb43b5781d (type 7) (param i32 i32 i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 5
    global.set 0
    i32.const 0
    i32.const 0
    i32.load offset=1057484
    local.tee 6
    i32.const 1
    i32.add
    i32.store offset=1057484
    block  ;; label = @1
      block  ;; label = @2
        local.get 6
        i32.const 0
        i32.lt_s
        br_if 0 (;@2;)
        i32.const 0
        i32.const 0
        i32.load offset=1057940
        i32.const 1
        i32.add
        local.tee 6
        i32.store offset=1057940
        local.get 6
        i32.const 2
        i32.gt_u
        br_if 0 (;@2;)
        local.get 5
        local.get 4
        i32.store8 offset=24
        local.get 5
        local.get 3
        i32.store offset=20
        local.get 5
        local.get 2
        i32.store offset=16
        local.get 5
        i32.const 1052996
        i32.store offset=12
        local.get 5
        i32.const 1052560
        i32.store offset=8
        i32.const 0
        i32.load offset=1057468
        local.tee 3
        i32.const -1
        i32.le_s
        br_if 0 (;@2;)
        i32.const 0
        local.get 3
        i32.const 1
        i32.add
        local.tee 3
        i32.store offset=1057468
        block  ;; label = @3
          i32.const 0
          i32.load offset=1057476
          i32.eqz
          br_if 0 (;@3;)
          local.get 5
          local.get 0
          local.get 1
          i32.load offset=16
          call_indirect (type 3)
          local.get 5
          local.get 5
          i64.load
          i64.store offset=8
          i32.const 0
          i32.load offset=1057476
          local.get 5
          i32.const 8
          i32.add
          i32.const 0
          i32.load offset=1057480
          i32.load offset=20
          call_indirect (type 3)
          i32.const 0
          i32.load offset=1057468
          local.set 3
        end
        i32.const 0
        local.get 3
        i32.const -1
        i32.add
        i32.store offset=1057468
        local.get 6
        i32.const 1
        i32.gt_u
        br_if 0 (;@2;)
        local.get 4
        br_if 1 (;@1;)
      end
      unreachable
      unreachable
    end
    local.get 0
    local.get 1
    call $rust_panic
    unreachable)
  (func $rust_panic (type 3) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    i32.const 8
    i32.add
    call $__rust_start_panic
    drop
    unreachable
    unreachable)
  (func $__rg_oom (type 3) (param i32 i32)
    (local i32)
    local.get 0
    local.get 1
    i32.const 0
    i32.load offset=1057464
    local.tee 2
    i32.const 23
    local.get 2
    select
    call_indirect (type 3)
    unreachable
    unreachable)
  (func $__rust_start_panic (type 12) (param i32) (result i32)
    unreachable
    unreachable)
  (func $dlmalloc::dlmalloc::align_up::h56307c410374cfa0 (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.add
    i32.const -1
    i32.add
    i32.const 0
    local.get 1
    i32.sub
    i32.and)
  (func $dlmalloc::dlmalloc::left_bits::h5ec3bcf18952f1c3 (type 12) (param i32) (result i32)
    local.get 0
    i32.const 1
    i32.shl
    local.tee 0
    i32.const 0
    local.get 0
    i32.sub
    i32.or)
  (func $dlmalloc::dlmalloc::least_bit::he9078852a9401cf2 (type 12) (param i32) (result i32)
    i32.const 0
    local.get 0
    i32.sub
    local.get 0
    i32.and)
  (func $dlmalloc::dlmalloc::leftshift_for_tree_index::h2319f6a1e784a603 (type 12) (param i32) (result i32)
    i32.const 0
    i32.const 25
    local.get 0
    i32.const 1
    i32.shr_u
    i32.sub
    local.get 0
    i32.const 31
    i32.eq
    select)
  (func $dlmalloc::dlmalloc::Chunk::fencepost_head::h0aeaad6175aa31b5 (type 11) (result i32)
    i32.const 7)
  (func $dlmalloc::dlmalloc::Chunk::size::hf35681b1fe7e2d85 (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const -8
    i32.and)
  (func $dlmalloc::dlmalloc::Chunk::cinuse::h9e5860a710e1f9c9 (type 12) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=4
    i32.const 2
    i32.and
    i32.const 1
    i32.shr_u)
  (func $dlmalloc::dlmalloc::Chunk::pinuse::h031627e231cdc5f4 (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.and)
  (func $dlmalloc::dlmalloc::Chunk::clear_pinuse::h6d812c2aaa28dc4c (type 0) (param i32)
    local.get 0
    local.get 0
    i32.load offset=4
    i32.const -2
    i32.and
    i32.store offset=4)
  (func $dlmalloc::dlmalloc::Chunk::inuse::h7a66feebace6547a (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const 3
    i32.and
    i32.const 1
    i32.ne)
  (func $dlmalloc::dlmalloc::Chunk::mmapped::he24c50abadb2abe8 (type 12) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=4
    i32.const 3
    i32.and
    i32.eqz)
  (func $dlmalloc::dlmalloc::Chunk::set_inuse::h9c548d76b99f9c0b (type 3) (param i32 i32)
    local.get 0
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.and
    local.get 1
    i32.or
    i32.const 2
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.tee 0
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.or
    i32.store offset=4)
  (func $dlmalloc::dlmalloc::Chunk::set_inuse_and_pinuse::h3be720dca1d3c5b7 (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 3
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.tee 1
    local.get 1
    i32.load offset=4
    i32.const 1
    i32.or
    i32.store offset=4)
  (func $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_inuse_chunk::h8e0d2f0d198bec4a (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 3
    i32.or
    i32.store offset=4)
  (func $dlmalloc::dlmalloc::Chunk::set_size_and_pinuse_of_free_chunk::h7a3950705dcf2e14 (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 1
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.get 1
    i32.store)
  (func $dlmalloc::dlmalloc::Chunk::set_free_with_pinuse::h28f791aa3db5b3cf (type 6) (param i32 i32 i32)
    local.get 2
    local.get 2
    i32.load offset=4
    i32.const -2
    i32.and
    i32.store offset=4
    local.get 0
    local.get 1
    i32.const 1
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.get 1
    i32.store)
  (func $dlmalloc::dlmalloc::Chunk::plus_offset::h09e3adf638348794 (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.add)
  (func $dlmalloc::dlmalloc::Chunk::minus_offset::h5ff294662793880c (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.sub)
  (func $dlmalloc::dlmalloc::Chunk::to_mem::h74c7c7639f4422c0 (type 12) (param i32) (result i32)
    local.get 0
    i32.const 8
    i32.add)
  (func $dlmalloc::dlmalloc::Chunk::mem_offset::h357bd0f1992935c5 (type 11) (result i32)
    i32.const 8)
  (func $dlmalloc::dlmalloc::Chunk::from_mem::hfab12920c40b4363 (type 12) (param i32) (result i32)
    local.get 0
    i32.const -8
    i32.add)
  (func $dlmalloc::dlmalloc::TreeChunk::leftmost_child::h5ca0ee09cb0b660e (type 12) (param i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=16
      local.tee 1
      br_if 0 (;@1;)
      local.get 0
      i32.const 20
      i32.add
      i32.load
      local.set 1
    end
    local.get 1)
  (func $dlmalloc::dlmalloc::TreeChunk::chunk::h0b8b52a5e7194e4f (type 12) (param i32) (result i32)
    local.get 0)
  (func $dlmalloc::dlmalloc::TreeChunk::next::h00c76645d0f5976a (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=12)
  (func $dlmalloc::dlmalloc::TreeChunk::prev::h830942eb7f12976d (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=8)
  (func $dlmalloc::dlmalloc::Segment::is_extern::h4499bcf5769b22f0 (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=12
    i32.const 1
    i32.and)
  (func $dlmalloc::dlmalloc::Segment::sys_flags::hc6d1c33f548edf0c (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=12
    i32.const 1
    i32.shr_u)
  (func $dlmalloc::dlmalloc::Segment::holds::h0fb9a9c5bb17b581 (type 2) (param i32 i32) (result i32)
    (local i32 i32)
    i32.const 0
    local.set 2
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 3
      local.get 1
      i32.gt_u
      br_if 0 (;@1;)
      local.get 3
      local.get 0
      i32.load offset=4
      i32.add
      local.get 1
      i32.gt_u
      local.set 2
    end
    local.get 2)
  (func $dlmalloc::dlmalloc::Segment::top::h0fee0944dfd91eaf (type 12) (param i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    i32.add)
  (func $<dlmalloc::sys::System_as_dlmalloc::Allocator>::alloc::hacc9295cf6d00ed7 (type 6) (param i32 i32 i32)
    (local i32)
    local.get 2
    i32.const 16
    i32.shr_u
    memory.grow
    local.set 3
    local.get 0
    i32.const 0
    i32.store offset=8
    local.get 0
    i32.const 0
    local.get 2
    i32.const -65536
    i32.and
    local.get 3
    i32.const -1
    i32.eq
    local.tee 2
    select
    i32.store offset=4
    local.get 0
    i32.const 0
    local.get 3
    i32.const 16
    i32.shl
    local.get 2
    select
    i32.store)
  (func $<dlmalloc::sys::System_as_dlmalloc::Allocator>::remap::hed1e3b2cb052631c (type 13) (param i32 i32 i32 i32 i32) (result i32)
    i32.const 0)
  (func $<dlmalloc::sys::System_as_dlmalloc::Allocator>::free_part::hf1736efad8f20a0e (type 9) (param i32 i32 i32 i32) (result i32)
    i32.const 0)
  (func $<dlmalloc::sys::System_as_dlmalloc::Allocator>::free::h86340b43ec294341 (type 4) (param i32 i32 i32) (result i32)
    i32.const 0)
  (func $<dlmalloc::sys::System_as_dlmalloc::Allocator>::can_release_part::h638a2c6d1b0640ff (type 2) (param i32 i32) (result i32)
    i32.const 0)
  (func $<dlmalloc::sys::System_as_dlmalloc::Allocator>::page_size::h244e7e67d7e17c1f (type 12) (param i32) (result i32)
    i32.const 65536)
  (func $core::ptr::drop_in_place<&u8>::hff2bf1f19be4017e (type 0) (param i32))
  (func $<&mut_W_as_core::fmt::Write>::write_char::h3033d36aeb6e7159 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    call $alloc::string::String::push::h0e8dc821fe5a3568
    i32.const 0)
  (func $alloc::string::String::push::h0e8dc821fe5a3568 (type 3) (param i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.const 128
              i32.lt_u
              br_if 0 (;@5;)
              local.get 2
              i32.const 0
              i32.store offset=12
              local.get 1
              i32.const 2048
              i32.lt_u
              br_if 1 (;@4;)
              local.get 1
              i32.const 65536
              i32.ge_u
              br_if 2 (;@3;)
              local.get 2
              local.get 1
              i32.const 63
              i32.and
              i32.const 128
              i32.or
              i32.store8 offset=14
              local.get 2
              local.get 1
              i32.const 12
              i32.shr_u
              i32.const 224
              i32.or
              i32.store8 offset=12
              local.get 2
              local.get 1
              i32.const 6
              i32.shr_u
              i32.const 63
              i32.and
              i32.const 128
              i32.or
              i32.store8 offset=13
              i32.const 3
              local.set 1
              br 3 (;@2;)
            end
            block  ;; label = @5
              local.get 0
              i32.load offset=8
              local.tee 3
              local.get 0
              i32.load
              i32.ne
              br_if 0 (;@5;)
              local.get 0
              local.get 3
              call $alloc::raw_vec::RawVec<T_A>::reserve_for_push::h4fa8232b64da9695
              local.get 0
              i32.load offset=8
              local.set 3
            end
            local.get 0
            local.get 3
            i32.const 1
            i32.add
            i32.store offset=8
            local.get 0
            i32.load offset=4
            local.get 3
            i32.add
            local.get 1
            i32.store8
            br 3 (;@1;)
          end
          local.get 2
          local.get 1
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=13
          local.get 2
          local.get 1
          i32.const 6
          i32.shr_u
          i32.const 192
          i32.or
          i32.store8 offset=12
          i32.const 2
          local.set 1
          br 1 (;@2;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=15
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=14
        local.get 2
        local.get 1
        i32.const 12
        i32.shr_u
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 18
        i32.shr_u
        i32.const 7
        i32.and
        i32.const 240
        i32.or
        i32.store8 offset=12
        i32.const 4
        local.set 1
      end
      block  ;; label = @2
        local.get 0
        i32.load
        local.get 0
        i32.load offset=8
        local.tee 3
        i32.sub
        local.get 1
        i32.ge_u
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        local.get 1
        call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hb101542837732c95
        local.get 0
        i32.load offset=8
        local.set 3
      end
      local.get 0
      i32.load offset=4
      local.get 3
      i32.add
      local.get 2
      i32.const 12
      i32.add
      local.get 1
      call $memcpy
      drop
      local.get 0
      local.get 3
      local.get 1
      i32.add
      i32.store offset=8
    end
    local.get 2
    i32.const 16
    i32.add
    global.set 0)
  (func $<&mut_W_as_core::fmt::Write>::write_fmt::h40b3918c42512df5 (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 0
    i32.load
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1053620
    local.get 2
    i32.const 8
    i32.add
    call $core::fmt::write::hb366ddae040241f7
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 1)
  (func $<&mut_W_as_core::fmt::Write>::write_str::h6bdcd609f50a704a (type 4) (param i32 i32 i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 0
      i32.load
      local.get 0
      i32.load offset=8
      local.tee 3
      i32.sub
      local.get 2
      i32.ge_u
      br_if 0 (;@1;)
      local.get 0
      local.get 3
      local.get 2
      call $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hb101542837732c95
      local.get 0
      i32.load offset=8
      local.set 3
    end
    local.get 0
    i32.load offset=4
    local.get 3
    i32.add
    local.get 1
    local.get 2
    call $memcpy
    drop
    local.get 0
    local.get 3
    local.get 2
    i32.add
    i32.store offset=8
    i32.const 0)
  (func $alloc::raw_vec::RawVec<T_A>::reserve::do_reserve_and_handle::hb101542837732c95 (type 6) (param i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        local.get 2
        i32.add
        local.tee 2
        local.get 1
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 1
        i32.const 1
        i32.shl
        local.tee 4
        local.get 2
        local.get 4
        local.get 2
        i32.gt_u
        select
        local.tee 2
        i32.const 8
        local.get 2
        i32.const 8
        i32.gt_u
        select
        local.tee 2
        i32.const -1
        i32.xor
        i32.const 31
        i32.shr_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            i32.const 1
            i32.store offset=24
            local.get 3
            local.get 1
            i32.store offset=20
            local.get 3
            local.get 0
            i32.const 4
            i32.add
            i32.load
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 3
          i32.const 0
          i32.store offset=24
        end
        local.get 3
        local.get 2
        local.get 4
        local.get 3
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h33fca59df56875af
        local.get 3
        i32.load offset=4
        local.set 1
        block  ;; label = @3
          local.get 3
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 2
          i32.store
          local.get 0
          local.get 1
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 3
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::raw_vec::finish_grow::h33fca59df56875af (type 8) (param i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 1
                    i32.const 0
                    i32.lt_s
                    br_if 0 (;@8;)
                    local.get 3
                    i32.load offset=8
                    br_if 1 (;@7;)
                    local.get 1
                    br_if 2 (;@6;)
                    i32.const 1
                    local.set 2
                    br 4 (;@4;)
                  end
                  local.get 0
                  i32.const 8
                  i32.add
                  i32.const 0
                  i32.store
                  br 6 (;@1;)
                end
                block  ;; label = @7
                  local.get 3
                  i32.load offset=4
                  local.tee 2
                  br_if 0 (;@7;)
                  block  ;; label = @8
                    local.get 1
                    br_if 0 (;@8;)
                    i32.const 1
                    local.set 2
                    br 4 (;@4;)
                  end
                  local.get 1
                  i32.const 1
                  call $__rust_alloc
                  local.set 2
                  br 2 (;@5;)
                end
                local.get 3
                i32.load
                local.get 2
                i32.const 1
                local.get 1
                call $__rust_realloc
                local.set 2
                br 1 (;@5;)
              end
              local.get 1
              i32.const 1
              call $__rust_alloc
              local.set 2
            end
            local.get 2
            i32.eqz
            br_if 1 (;@3;)
          end
          local.get 0
          local.get 2
          i32.store offset=4
          local.get 0
          i32.const 8
          i32.add
          local.get 1
          i32.store
          local.get 0
          i32.const 0
          i32.store
          return
        end
        local.get 0
        local.get 1
        i32.store offset=4
        local.get 0
        i32.const 8
        i32.add
        i32.const 1
        i32.store
        local.get 0
        i32.const 1
        i32.store
        return
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 0
      i32.const 8
      i32.add
      i32.const 0
      i32.store
    end
    local.get 0
    i32.const 1
    i32.store)
  (func $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $alloc::alloc::handle_alloc_error::rt_error::h17b448b39cea00a0
    unreachable)
  (func $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3 (type 10)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 0
    global.set 0
    local.get 0
    i32.const 20
    i32.add
    i32.const 1
    i32.store
    local.get 0
    i32.const 28
    i32.add
    i32.const 0
    i32.store
    local.get 0
    i32.const 1053692
    i32.store offset=16
    local.get 0
    i32.const 1053644
    i32.store offset=24
    local.get 0
    i32.const 0
    i32.store offset=8
    local.get 0
    i32.const 8
    i32.add
    i32.const 1053700
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $alloc::raw_vec::RawVec<T_A>::reserve_for_push::h4fa8232b64da9695 (type 3) (param i32 i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.add
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.tee 3
        i32.const 1
        i32.shl
        local.tee 4
        local.get 1
        local.get 4
        local.get 1
        i32.gt_u
        select
        local.tee 1
        i32.const 8
        local.get 1
        i32.const 8
        i32.gt_u
        select
        local.tee 1
        i32.const -1
        i32.xor
        i32.const 31
        i32.shr_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            i32.const 1
            i32.store offset=24
            local.get 2
            local.get 3
            i32.store offset=20
            local.get 2
            local.get 0
            i32.const 4
            i32.add
            i32.load
            i32.store offset=16
            br 1 (;@3;)
          end
          local.get 2
          i32.const 0
          i32.store offset=24
        end
        local.get 2
        local.get 1
        local.get 4
        local.get 2
        i32.const 16
        i32.add
        call $alloc::raw_vec::finish_grow::h33fca59df56875af
        local.get 2
        i32.load offset=4
        local.set 3
        block  ;; label = @3
          local.get 2
          i32.load
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          i32.store
          local.get 0
          local.get 3
          i32.store offset=4
          br 2 (;@1;)
        end
        local.get 2
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.const -2147483647
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 0
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0)
  (func $alloc::alloc::handle_alloc_error::rt_error::h17b448b39cea00a0 (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $__rust_alloc_error_handler
    unreachable)
  (func $alloc::fmt::format::format_inner::h92d046d33b5f4798 (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 1
                i32.const 12
                i32.add
                i32.load
                local.tee 3
                i32.eqz
                br_if 0 (;@6;)
                local.get 1
                i32.load offset=8
                local.set 4
                local.get 3
                i32.const -1
                i32.add
                i32.const 536870911
                i32.and
                local.tee 3
                i32.const 1
                i32.add
                local.tee 5
                i32.const 7
                i32.and
                local.set 6
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 3
                    i32.const 7
                    i32.ge_u
                    br_if 0 (;@8;)
                    i32.const 0
                    local.set 5
                    local.get 4
                    local.set 3
                    br 1 (;@7;)
                  end
                  local.get 4
                  i32.const 60
                  i32.add
                  local.set 3
                  local.get 5
                  i32.const 1073741816
                  i32.and
                  local.set 7
                  i32.const 0
                  local.set 5
                  loop  ;; label = @8
                    local.get 3
                    i32.load
                    local.get 3
                    i32.const -8
                    i32.add
                    i32.load
                    local.get 3
                    i32.const -16
                    i32.add
                    i32.load
                    local.get 3
                    i32.const -24
                    i32.add
                    i32.load
                    local.get 3
                    i32.const -32
                    i32.add
                    i32.load
                    local.get 3
                    i32.const -40
                    i32.add
                    i32.load
                    local.get 3
                    i32.const -48
                    i32.add
                    i32.load
                    local.get 3
                    i32.const -56
                    i32.add
                    i32.load
                    local.get 5
                    i32.add
                    i32.add
                    i32.add
                    i32.add
                    i32.add
                    i32.add
                    i32.add
                    i32.add
                    local.set 5
                    local.get 3
                    i32.const 64
                    i32.add
                    local.set 3
                    local.get 7
                    i32.const -8
                    i32.add
                    local.tee 7
                    br_if 0 (;@8;)
                  end
                  local.get 3
                  i32.const -60
                  i32.add
                  local.set 3
                end
                block  ;; label = @7
                  local.get 6
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 3
                  i32.const 4
                  i32.add
                  local.set 3
                  loop  ;; label = @8
                    local.get 3
                    i32.load
                    local.get 5
                    i32.add
                    local.set 5
                    local.get 3
                    i32.const 8
                    i32.add
                    local.set 3
                    local.get 6
                    i32.const -1
                    i32.add
                    local.tee 6
                    br_if 0 (;@8;)
                  end
                end
                local.get 1
                i32.const 20
                i32.add
                i32.load
                br_if 1 (;@5;)
                local.get 5
                local.set 6
                br 3 (;@3;)
              end
              i32.const 0
              local.set 5
              local.get 1
              i32.const 20
              i32.add
              i32.load
              br_if 1 (;@4;)
              i32.const 1
              local.set 3
              br 4 (;@1;)
            end
            local.get 4
            i32.load offset=4
            br_if 0 (;@4;)
            local.get 5
            i32.const 16
            i32.lt_u
            br_if 2 (;@2;)
          end
          local.get 5
          local.get 5
          i32.add
          local.tee 6
          local.get 5
          i32.lt_u
          br_if 1 (;@2;)
        end
        local.get 6
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            local.get 6
            i32.const -1
            i32.le_s
            br_if 0 (;@4;)
            local.get 6
            i32.const 1
            call $__rust_alloc
            local.tee 3
            i32.eqz
            br_if 1 (;@3;)
            local.get 6
            local.set 5
            br 3 (;@1;)
          end
          call $alloc::raw_vec::capacity_overflow::h9b8fdd50660a9bc3
          unreachable
        end
        local.get 6
        i32.const 1
        call $alloc::alloc::handle_alloc_error::h80087a6d84fb07bd
        unreachable
      end
      i32.const 1
      local.set 3
      i32.const 0
      local.set 5
    end
    local.get 0
    i32.const 0
    i32.store offset=8
    local.get 0
    local.get 3
    i32.store offset=4
    local.get 0
    local.get 5
    i32.store
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 2
    i32.const 16
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 16
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=16
    block  ;; label = @1
      local.get 2
      i32.const 12
      i32.add
      i32.const 1053620
      local.get 2
      i32.const 16
      i32.add
      call $core::fmt::write::hb366ddae040241f7
      i32.eqz
      br_if 0 (;@1;)
      i32.const 1053716
      i32.const 51
      local.get 2
      i32.const 40
      i32.add
      i32.const 1053768
      i32.const 1053808
      call $core::result::unwrap_failed::h2b47cc7f7e98a508
      unreachable
    end
    local.get 2
    i32.const 48
    i32.add
    global.set 0)
  (func $core::ops::function::FnOnce::call_once::h0502d0d495ff7cd0 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    drop
    loop (result i32)  ;; label = @1
      br 0 (;@1;)
    end)
  (func $core::ptr::drop_in_place<&core::iter::adapters::copied::Copied<core::slice::iter::Iter<u8>>>::hecbac7fec775a8ae (type 0) (param i32))
  (func $core::panicking::panic_fmt::h9d972fcdb087ce21 (type 3) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    i32.const 1
    i32.store8 offset=24
    local.get 2
    local.get 1
    i32.store offset=20
    local.get 2
    local.get 0
    i32.store offset=16
    local.get 2
    i32.const 1053948
    i32.store offset=12
    local.get 2
    i32.const 1053824
    i32.store offset=8
    local.get 2
    i32.const 8
    i32.add
    call $rust_begin_unwind
    unreachable)
  (func $core::panicking::panic_bounds_check::h929177d2c8f5de7b (type 6) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 8
    i32.add
    i32.const 12
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 32
    i32.add
    i32.const 12
    i32.add
    i32.const 1
    i32.store
    local.get 3
    i32.const 1053932
    i32.store offset=16
    local.get 3
    i32.const 0
    i32.store offset=8
    local.get 3
    i32.const 1
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.store offset=40
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf (type 6) (param i32 i32 i32)
    local.get 0
    local.get 1
    local.get 2
    call $core::slice::index::slice_start_index_len_fail_rt::h14fdf9184104ebe9
    unreachable)
  (func $core::slice::index::slice_end_index_len_fail::h06e641a35ef26e6c (type 6) (param i32 i32 i32)
    local.get 0
    local.get 1
    local.get 2
    call $core::slice::index::slice_end_index_len_fail_rt::hb4a41039aad0214c
    unreachable)
  (func $core::fmt::Formatter::pad::ha508305181e859c2 (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32)
    local.get 0
    i32.load offset=16
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          i32.load offset=8
          local.tee 4
          i32.const 1
          i32.eq
          br_if 0 (;@3;)
          local.get 3
          i32.const 1
          i32.ne
          br_if 1 (;@2;)
        end
        block  ;; label = @3
          local.get 3
          i32.const 1
          i32.ne
          br_if 0 (;@3;)
          local.get 1
          local.get 2
          i32.add
          local.set 5
          local.get 0
          i32.const 20
          i32.add
          i32.load
          i32.const 1
          i32.add
          local.set 6
          i32.const 0
          local.set 7
          local.get 1
          local.set 8
          block  ;; label = @4
            loop  ;; label = @5
              local.get 8
              local.set 3
              local.get 6
              i32.const -1
              i32.add
              local.tee 6
              i32.eqz
              br_if 1 (;@4;)
              local.get 3
              local.get 5
              i32.eq
              br_if 2 (;@3;)
              block  ;; label = @6
                block  ;; label = @7
                  local.get 3
                  i32.load8_s
                  local.tee 9
                  i32.const -1
                  i32.le_s
                  br_if 0 (;@7;)
                  local.get 3
                  i32.const 1
                  i32.add
                  local.set 8
                  local.get 9
                  i32.const 255
                  i32.and
                  local.set 9
                  br 1 (;@6;)
                end
                local.get 3
                i32.load8_u offset=1
                i32.const 63
                i32.and
                local.set 8
                local.get 9
                i32.const 31
                i32.and
                local.set 10
                block  ;; label = @7
                  local.get 9
                  i32.const -33
                  i32.gt_u
                  br_if 0 (;@7;)
                  local.get 10
                  i32.const 6
                  i32.shl
                  local.get 8
                  i32.or
                  local.set 9
                  local.get 3
                  i32.const 2
                  i32.add
                  local.set 8
                  br 1 (;@6;)
                end
                local.get 8
                i32.const 6
                i32.shl
                local.get 3
                i32.load8_u offset=2
                i32.const 63
                i32.and
                i32.or
                local.set 8
                block  ;; label = @7
                  local.get 9
                  i32.const -16
                  i32.ge_u
                  br_if 0 (;@7;)
                  local.get 8
                  local.get 10
                  i32.const 12
                  i32.shl
                  i32.or
                  local.set 9
                  local.get 3
                  i32.const 3
                  i32.add
                  local.set 8
                  br 1 (;@6;)
                end
                local.get 8
                i32.const 6
                i32.shl
                local.get 3
                i32.load8_u offset=3
                i32.const 63
                i32.and
                i32.or
                local.get 10
                i32.const 18
                i32.shl
                i32.const 1835008
                i32.and
                i32.or
                local.tee 9
                i32.const 1114112
                i32.eq
                br_if 3 (;@3;)
                local.get 3
                i32.const 4
                i32.add
                local.set 8
              end
              local.get 7
              local.get 3
              i32.sub
              local.get 8
              i32.add
              local.set 7
              local.get 9
              i32.const 1114112
              i32.ne
              br_if 0 (;@5;)
              br 2 (;@3;)
            end
          end
          local.get 3
          local.get 5
          i32.eq
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 3
            i32.load8_s
            local.tee 8
            i32.const -1
            i32.gt_s
            br_if 0 (;@4;)
            local.get 8
            i32.const -32
            i32.lt_u
            br_if 0 (;@4;)
            local.get 8
            i32.const -16
            i32.lt_u
            br_if 0 (;@4;)
            local.get 3
            i32.load8_u offset=2
            i32.const 63
            i32.and
            i32.const 6
            i32.shl
            local.get 3
            i32.load8_u offset=1
            i32.const 63
            i32.and
            i32.const 12
            i32.shl
            i32.or
            local.get 3
            i32.load8_u offset=3
            i32.const 63
            i32.and
            i32.or
            local.get 8
            i32.const 255
            i32.and
            i32.const 18
            i32.shl
            i32.const 1835008
            i32.and
            i32.or
            i32.const 1114112
            i32.eq
            br_if 1 (;@3;)
          end
          block  ;; label = @4
            block  ;; label = @5
              local.get 7
              i32.eqz
              br_if 0 (;@5;)
              block  ;; label = @6
                local.get 7
                local.get 2
                i32.lt_u
                br_if 0 (;@6;)
                i32.const 0
                local.set 3
                local.get 7
                local.get 2
                i32.eq
                br_if 1 (;@5;)
                br 2 (;@4;)
              end
              i32.const 0
              local.set 3
              local.get 1
              local.get 7
              i32.add
              i32.load8_s
              i32.const -64
              i32.lt_s
              br_if 1 (;@4;)
            end
            local.get 1
            local.set 3
          end
          local.get 7
          local.get 2
          local.get 3
          select
          local.set 2
          local.get 3
          local.get 1
          local.get 3
          select
          local.set 1
        end
        block  ;; label = @3
          local.get 4
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.get 1
          local.get 2
          local.get 0
          i32.load offset=4
          i32.load offset=12
          call_indirect (type 4)
          return
        end
        local.get 0
        i32.const 12
        i32.add
        i32.load
        local.set 7
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 16
            i32.lt_u
            br_if 0 (;@4;)
            local.get 1
            local.get 2
            call $core::str::count::do_count_chars::h047305ee9bcb28cd
            local.set 8
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 2
            br_if 0 (;@4;)
            i32.const 0
            local.set 8
            br 1 (;@3;)
          end
          local.get 2
          i32.const 3
          i32.and
          local.set 9
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.const -1
              i32.add
              i32.const 3
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              local.set 8
              local.get 1
              local.set 3
              br 1 (;@4;)
            end
            local.get 2
            i32.const -4
            i32.and
            local.set 6
            i32.const 0
            local.set 8
            local.get 1
            local.set 3
            loop  ;; label = @5
              local.get 8
              local.get 3
              i32.load8_s
              i32.const -65
              i32.gt_s
              i32.add
              local.get 3
              i32.load8_s offset=1
              i32.const -65
              i32.gt_s
              i32.add
              local.get 3
              i32.load8_s offset=2
              i32.const -65
              i32.gt_s
              i32.add
              local.get 3
              i32.load8_s offset=3
              i32.const -65
              i32.gt_s
              i32.add
              local.set 8
              local.get 3
              i32.const 4
              i32.add
              local.set 3
              local.get 6
              i32.const -4
              i32.add
              local.tee 6
              br_if 0 (;@5;)
            end
          end
          local.get 9
          i32.eqz
          br_if 0 (;@3;)
          loop  ;; label = @4
            local.get 8
            local.get 3
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.set 8
            local.get 3
            i32.const 1
            i32.add
            local.set 3
            local.get 9
            i32.const -1
            i32.add
            local.tee 9
            br_if 0 (;@4;)
          end
        end
        block  ;; label = @3
          local.get 7
          local.get 8
          i32.le_u
          br_if 0 (;@3;)
          local.get 7
          local.get 8
          i32.sub
          local.tee 8
          local.set 6
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                i32.const 0
                local.get 0
                i32.load8_u offset=32
                local.tee 3
                local.get 3
                i32.const 3
                i32.eq
                select
                i32.const 3
                i32.and
                local.tee 3
                br_table 2 (;@4;) 0 (;@6;) 1 (;@5;) 2 (;@4;)
              end
              i32.const 0
              local.set 6
              local.get 8
              local.set 3
              br 1 (;@4;)
            end
            local.get 8
            i32.const 1
            i32.shr_u
            local.set 3
            local.get 8
            i32.const 1
            i32.add
            i32.const 1
            i32.shr_u
            local.set 6
          end
          local.get 3
          i32.const 1
          i32.add
          local.set 3
          local.get 0
          i32.const 4
          i32.add
          i32.load
          local.set 9
          local.get 0
          i32.load offset=28
          local.set 8
          local.get 0
          i32.load
          local.set 0
          block  ;; label = @4
            loop  ;; label = @5
              local.get 3
              i32.const -1
              i32.add
              local.tee 3
              i32.eqz
              br_if 1 (;@4;)
              local.get 0
              local.get 8
              local.get 9
              i32.load offset=16
              call_indirect (type 2)
              i32.eqz
              br_if 0 (;@5;)
            end
            i32.const 1
            return
          end
          i32.const 1
          local.set 3
          local.get 8
          i32.const 1114112
          i32.eq
          br_if 2 (;@1;)
          local.get 0
          local.get 1
          local.get 2
          local.get 9
          i32.load offset=12
          call_indirect (type 4)
          br_if 2 (;@1;)
          i32.const 0
          local.set 3
          loop  ;; label = @4
            block  ;; label = @5
              local.get 6
              local.get 3
              i32.ne
              br_if 0 (;@5;)
              local.get 6
              local.get 6
              i32.lt_u
              return
            end
            local.get 3
            i32.const 1
            i32.add
            local.set 3
            local.get 0
            local.get 8
            local.get 9
            i32.load offset=16
            call_indirect (type 2)
            i32.eqz
            br_if 0 (;@4;)
          end
          local.get 3
          i32.const -1
          i32.add
          local.get 6
          i32.lt_u
          return
        end
        local.get 0
        i32.load
        local.get 1
        local.get 2
        local.get 0
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        return
      end
      local.get 0
      i32.load
      local.get 1
      local.get 2
      local.get 0
      i32.load offset=4
      i32.load offset=12
      call_indirect (type 4)
      local.set 3
    end
    local.get 3)
  (func $core::panicking::panic::h364c37174a08a6a4 (type 6) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    i32.const 12
    i32.add
    i32.const 1
    i32.store
    local.get 3
    i32.const 20
    i32.add
    i32.const 0
    i32.store
    local.get 3
    i32.const 1053824
    i32.store offset=16
    local.get 3
    i32.const 0
    i32.store
    local.get 3
    local.get 1
    i32.store offset=28
    local.get 3
    local.get 0
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 24
    i32.add
    i32.store offset=8
    local.get 3
    local.get 2
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $core::slice::index::slice_index_order_fail::h2a2e64aa62065ee1 (type 6) (param i32 i32 i32)
    local.get 0
    local.get 1
    local.get 2
    call $core::slice::index::slice_index_order_fail_rt::h8f57d30e89768042
    unreachable)
  (func $core::fmt::num::imp::<impl_core::fmt::Display_for_u32>::fmt::h8b2af792c13f3dfc (type 2) (param i32 i32) (result i32)
    local.get 0
    i64.load32_u
    i32.const 1
    local.get 1
    call $core::fmt::num::imp::fmt_u64::h0e41c13141ae31b8)
  (func $core::fmt::num::<impl_core::fmt::Debug_for_u32>::fmt::he402446d3318df8f (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 128
    i32.sub
    local.tee 2
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.load offset=24
              local.tee 3
              i32.const 16
              i32.and
              br_if 0 (;@5;)
              local.get 3
              i32.const 32
              i32.and
              br_if 1 (;@4;)
              local.get 0
              i64.load32_u
              i32.const 1
              local.get 1
              call $core::fmt::num::imp::fmt_u64::h0e41c13141ae31b8
              local.set 0
              br 4 (;@1;)
            end
            local.get 0
            i32.load
            local.set 0
            i32.const 0
            local.set 3
            loop  ;; label = @5
              local.get 2
              local.get 3
              i32.add
              i32.const 127
              i32.add
              i32.const 48
              i32.const 87
              local.get 0
              i32.const 15
              i32.and
              local.tee 4
              i32.const 10
              i32.lt_u
              select
              local.get 4
              i32.add
              i32.store8
              local.get 3
              i32.const -1
              i32.add
              local.set 3
              local.get 0
              i32.const 15
              i32.gt_u
              local.set 4
              local.get 0
              i32.const 4
              i32.shr_u
              local.set 0
              local.get 4
              br_if 0 (;@5;)
            end
            local.get 3
            i32.const 128
            i32.add
            local.tee 0
            i32.const 129
            i32.ge_u
            br_if 1 (;@3;)
            local.get 1
            i32.const 1
            i32.const 1054216
            i32.const 2
            local.get 2
            local.get 3
            i32.add
            i32.const 128
            i32.add
            i32.const 0
            local.get 3
            i32.sub
            call $core::fmt::Formatter::pad_integral::hb3903a931cea1b46
            local.set 0
            br 3 (;@1;)
          end
          local.get 0
          i32.load
          local.set 0
          i32.const 0
          local.set 3
          loop  ;; label = @4
            local.get 2
            local.get 3
            i32.add
            i32.const 127
            i32.add
            i32.const 48
            i32.const 55
            local.get 0
            i32.const 15
            i32.and
            local.tee 4
            i32.const 10
            i32.lt_u
            select
            local.get 4
            i32.add
            i32.store8
            local.get 3
            i32.const -1
            i32.add
            local.set 3
            local.get 0
            i32.const 15
            i32.gt_u
            local.set 4
            local.get 0
            i32.const 4
            i32.shr_u
            local.set 0
            local.get 4
            br_if 0 (;@4;)
          end
          local.get 3
          i32.const 128
          i32.add
          local.tee 0
          i32.const 129
          i32.ge_u
          br_if 1 (;@2;)
          local.get 1
          i32.const 1
          i32.const 1054216
          i32.const 2
          local.get 2
          local.get 3
          i32.add
          i32.const 128
          i32.add
          i32.const 0
          local.get 3
          i32.sub
          call $core::fmt::Formatter::pad_integral::hb3903a931cea1b46
          local.set 0
          br 2 (;@1;)
        end
        local.get 0
        i32.const 128
        i32.const 1054200
        call $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf
        unreachable
      end
      local.get 0
      i32.const 128
      i32.const 1054200
      call $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf
      unreachable
    end
    local.get 2
    i32.const 128
    i32.add
    global.set 0
    local.get 0)
  (func $core::fmt::write::hb366ddae040241f7 (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    i32.const 3
    i32.store8 offset=40
    local.get 3
    i64.const 137438953472
    i64.store offset=32
    i32.const 0
    local.set 4
    local.get 3
    i32.const 0
    i32.store offset=24
    local.get 3
    i32.const 0
    i32.store offset=16
    local.get 3
    local.get 1
    i32.store offset=12
    local.get 3
    local.get 0
    i32.store offset=8
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load
            local.tee 5
            br_if 0 (;@4;)
            local.get 2
            i32.const 20
            i32.add
            i32.load
            local.tee 0
            i32.eqz
            br_if 1 (;@3;)
            local.get 2
            i32.load offset=16
            local.set 1
            local.get 0
            i32.const 3
            i32.shl
            local.set 6
            local.get 0
            i32.const -1
            i32.add
            i32.const 536870911
            i32.and
            i32.const 1
            i32.add
            local.set 4
            local.get 2
            i32.load offset=8
            local.set 0
            loop  ;; label = @5
              block  ;; label = @6
                local.get 0
                i32.const 4
                i32.add
                i32.load
                local.tee 7
                i32.eqz
                br_if 0 (;@6;)
                local.get 3
                i32.load offset=8
                local.get 0
                i32.load
                local.get 7
                local.get 3
                i32.load offset=12
                i32.load offset=12
                call_indirect (type 4)
                br_if 4 (;@2;)
              end
              local.get 1
              i32.load
              local.get 3
              i32.const 8
              i32.add
              local.get 1
              i32.const 4
              i32.add
              i32.load
              call_indirect (type 2)
              br_if 3 (;@2;)
              local.get 1
              i32.const 8
              i32.add
              local.set 1
              local.get 0
              i32.const 8
              i32.add
              local.set 0
              local.get 6
              i32.const -8
              i32.add
              local.tee 6
              br_if 0 (;@5;)
              br 2 (;@3;)
            end
          end
          local.get 2
          i32.load offset=4
          local.tee 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.const 5
          i32.shl
          local.set 8
          local.get 1
          i32.const -1
          i32.add
          i32.const 134217727
          i32.and
          i32.const 1
          i32.add
          local.set 4
          local.get 2
          i32.load offset=8
          local.set 0
          i32.const 0
          local.set 6
          loop  ;; label = @4
            block  ;; label = @5
              local.get 0
              i32.const 4
              i32.add
              i32.load
              local.tee 1
              i32.eqz
              br_if 0 (;@5;)
              local.get 3
              i32.load offset=8
              local.get 0
              i32.load
              local.get 1
              local.get 3
              i32.load offset=12
              i32.load offset=12
              call_indirect (type 4)
              br_if 3 (;@2;)
            end
            local.get 3
            local.get 5
            local.get 6
            i32.add
            local.tee 1
            i32.const 28
            i32.add
            i32.load8_u
            i32.store8 offset=40
            local.get 3
            local.get 1
            i32.const 20
            i32.add
            i64.load align=4
            i64.store offset=32
            local.get 1
            i32.const 16
            i32.add
            i32.load
            local.set 9
            local.get 2
            i32.load offset=16
            local.set 10
            i32.const 0
            local.set 11
            i32.const 0
            local.set 7
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  i32.const 12
                  i32.add
                  i32.load
                  br_table 1 (;@6;) 0 (;@7;) 2 (;@5;) 1 (;@6;)
                end
                local.get 9
                i32.const 3
                i32.shl
                local.set 12
                i32.const 0
                local.set 7
                local.get 10
                local.get 12
                i32.add
                local.tee 12
                i32.const 4
                i32.add
                i32.load
                i32.const 48
                i32.ne
                br_if 1 (;@5;)
                local.get 12
                i32.load
                i32.load
                local.set 9
              end
              i32.const 1
              local.set 7
            end
            local.get 3
            local.get 9
            i32.store offset=20
            local.get 3
            local.get 7
            i32.store offset=16
            local.get 1
            i32.const 8
            i32.add
            i32.load
            local.set 7
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  i32.const 4
                  i32.add
                  i32.load
                  br_table 1 (;@6;) 0 (;@7;) 2 (;@5;) 1 (;@6;)
                end
                local.get 7
                i32.const 3
                i32.shl
                local.set 9
                local.get 10
                local.get 9
                i32.add
                local.tee 9
                i32.const 4
                i32.add
                i32.load
                i32.const 48
                i32.ne
                br_if 1 (;@5;)
                local.get 9
                i32.load
                i32.load
                local.set 7
              end
              i32.const 1
              local.set 11
            end
            local.get 3
            local.get 7
            i32.store offset=28
            local.get 3
            local.get 11
            i32.store offset=24
            local.get 10
            local.get 1
            i32.load
            i32.const 3
            i32.shl
            i32.add
            local.tee 1
            i32.load
            local.get 3
            i32.const 8
            i32.add
            local.get 1
            i32.load offset=4
            call_indirect (type 2)
            br_if 2 (;@2;)
            local.get 0
            i32.const 8
            i32.add
            local.set 0
            local.get 8
            local.get 6
            i32.const 32
            i32.add
            local.tee 6
            i32.ne
            br_if 0 (;@4;)
          end
        end
        block  ;; label = @3
          local.get 4
          local.get 2
          i32.const 12
          i32.add
          i32.load
          i32.ge_u
          br_if 0 (;@3;)
          local.get 3
          i32.load offset=8
          local.get 2
          i32.load offset=8
          local.get 4
          i32.const 3
          i32.shl
          i32.add
          local.tee 1
          i32.load
          local.get 1
          i32.load offset=4
          local.get 3
          i32.load offset=12
          i32.load offset=12
          call_indirect (type 4)
          br_if 1 (;@2;)
        end
        i32.const 0
        local.set 1
        br 1 (;@1;)
      end
      i32.const 1
      local.set 1
    end
    local.get 3
    i32.const 48
    i32.add
    global.set 0
    local.get 1)
  (func $<core::ops::range::Range<Idx>_as_core::fmt::Debug>::fmt::hdf186d101772be86 (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    i32.const 1
    local.set 3
    block  ;; label = @1
      local.get 0
      local.get 1
      call $core::fmt::num::<impl_core::fmt::Debug_for_u32>::fmt::he402446d3318df8f
      br_if 0 (;@1;)
      local.get 1
      i32.load offset=4
      local.set 4
      local.get 1
      i32.load
      local.set 5
      local.get 2
      i32.const 0
      i32.store offset=28
      local.get 2
      i32.const 1053824
      i32.store offset=24
      i32.const 1
      local.set 3
      local.get 2
      i32.const 1
      i32.store offset=20
      local.get 2
      i32.const 1053872
      i32.store offset=16
      local.get 2
      i32.const 0
      i32.store offset=8
      local.get 5
      local.get 4
      local.get 2
      i32.const 8
      i32.add
      call $core::fmt::write::hb366ddae040241f7
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      local.get 1
      call $core::fmt::num::<impl_core::fmt::Debug_for_u32>::fmt::he402446d3318df8f
      local.set 3
    end
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 3)
  (func $<T_as_core::any::Any>::type_id::he0c39bc4489521d2 (type 1) (param i32) (result i64)
    i64.const 8330237566129496815)
  (func $core::slice::memchr::memchr_aligned::hb22334f2708d3709 (type 8) (param i32 i32 i32 i32)
    (local i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 3
            i32.add
            i32.const -4
            i32.and
            local.tee 4
            local.get 2
            i32.eq
            br_if 0 (;@4;)
            local.get 4
            local.get 2
            i32.sub
            local.tee 4
            local.get 3
            local.get 4
            local.get 3
            i32.lt_u
            select
            local.tee 4
            i32.eqz
            br_if 0 (;@4;)
            i32.const 0
            local.set 5
            local.get 1
            i32.const 255
            i32.and
            local.set 6
            i32.const 1
            local.set 7
            loop  ;; label = @5
              local.get 2
              local.get 5
              i32.add
              i32.load8_u
              local.get 6
              i32.eq
              br_if 4 (;@1;)
              local.get 4
              local.get 5
              i32.const 1
              i32.add
              local.tee 5
              i32.ne
              br_if 0 (;@5;)
            end
            local.get 4
            local.get 3
            i32.const -8
            i32.add
            local.tee 8
            i32.gt_u
            br_if 2 (;@2;)
            br 1 (;@3;)
          end
          local.get 3
          i32.const -8
          i32.add
          local.set 8
          i32.const 0
          local.set 4
        end
        local.get 1
        i32.const 255
        i32.and
        i32.const 16843009
        i32.mul
        local.set 5
        block  ;; label = @3
          loop  ;; label = @4
            local.get 2
            local.get 4
            i32.add
            local.tee 7
            i32.load
            local.get 5
            i32.xor
            local.tee 6
            i32.const -1
            i32.xor
            local.get 6
            i32.const -16843009
            i32.add
            i32.and
            i32.const -2139062144
            i32.and
            br_if 1 (;@3;)
            local.get 7
            i32.const 4
            i32.add
            i32.load
            local.get 5
            i32.xor
            local.tee 6
            i32.const -1
            i32.xor
            local.get 6
            i32.const -16843009
            i32.add
            i32.and
            i32.const -2139062144
            i32.and
            br_if 1 (;@3;)
            local.get 4
            i32.const 8
            i32.add
            local.tee 4
            local.get 8
            i32.le_u
            br_if 0 (;@4;)
          end
        end
        local.get 4
        local.get 3
        i32.le_u
        br_if 0 (;@2;)
        local.get 4
        local.get 3
        i32.const 1054536
        call $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf
        unreachable
      end
      i32.const 0
      local.set 7
      block  ;; label = @2
        local.get 4
        local.get 3
        i32.eq
        br_if 0 (;@2;)
        local.get 1
        i32.const 255
        i32.and
        local.set 5
        loop  ;; label = @3
          block  ;; label = @4
            local.get 2
            local.get 4
            i32.add
            i32.load8_u
            local.get 5
            i32.ne
            br_if 0 (;@4;)
            local.get 4
            local.set 5
            i32.const 1
            local.set 7
            br 3 (;@1;)
          end
          local.get 3
          local.get 4
          i32.const 1
          i32.add
          local.tee 4
          i32.ne
          br_if 0 (;@3;)
        end
      end
      local.get 3
      local.set 5
    end
    local.get 0
    local.get 5
    i32.store offset=4
    local.get 0
    local.get 7
    i32.store)
  (func $core::fmt::builders::DebugStruct::field::h12418a0ca165575b (type 13) (param i32 i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i64 i64)
    global.get 0
    i32.const 64
    i32.sub
    local.tee 5
    global.set 0
    i32.const 1
    local.set 6
    block  ;; label = @1
      local.get 0
      i32.load8_u offset=4
      br_if 0 (;@1;)
      local.get 0
      i32.load8_u offset=5
      local.set 7
      block  ;; label = @2
        local.get 0
        i32.load
        local.tee 8
        i32.load offset=24
        local.tee 9
        i32.const 4
        i32.and
        br_if 0 (;@2;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load
        i32.const 1054157
        i32.const 1054159
        local.get 7
        i32.const 255
        i32.and
        local.tee 7
        select
        i32.const 2
        i32.const 3
        local.get 7
        select
        local.get 8
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load
        local.get 1
        local.get 2
        local.get 8
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load
        i32.const 1054104
        i32.const 2
        local.get 8
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        local.get 3
        local.get 8
        local.get 4
        i32.load offset=12
        call_indirect (type 2)
        local.set 6
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 7
        i32.const 255
        i32.and
        br_if 0 (;@2;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load
        i32.const 1054152
        i32.const 3
        local.get 8
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        local.get 8
        i32.load offset=24
        local.set 9
      end
      i32.const 1
      local.set 6
      local.get 5
      i32.const 1
      i32.store8 offset=23
      local.get 5
      i32.const 1054124
      i32.store offset=28
      local.get 5
      local.get 8
      i64.load align=4
      i64.store offset=8
      local.get 5
      local.get 5
      i32.const 23
      i32.add
      i32.store offset=16
      local.get 8
      i64.load offset=8 align=4
      local.set 10
      local.get 8
      i64.load offset=16 align=4
      local.set 11
      local.get 5
      local.get 8
      i32.load8_u offset=32
      i32.store8 offset=56
      local.get 5
      local.get 8
      i32.load offset=28
      i32.store offset=52
      local.get 5
      local.get 9
      i32.store offset=48
      local.get 5
      local.get 11
      i64.store offset=40
      local.get 5
      local.get 10
      i64.store offset=32
      local.get 5
      local.get 5
      i32.const 8
      i32.add
      i32.store offset=24
      local.get 5
      i32.const 8
      i32.add
      local.get 1
      local.get 2
      call $<core::fmt::builders::PadAdapter_as_core::fmt::Write>::write_str::h5c98716d5a71cc25
      br_if 0 (;@1;)
      local.get 5
      i32.const 8
      i32.add
      i32.const 1054104
      i32.const 2
      call $<core::fmt::builders::PadAdapter_as_core::fmt::Write>::write_str::h5c98716d5a71cc25
      br_if 0 (;@1;)
      local.get 3
      local.get 5
      i32.const 24
      i32.add
      local.get 4
      i32.load offset=12
      call_indirect (type 2)
      br_if 0 (;@1;)
      local.get 5
      i32.load offset=24
      i32.const 1054155
      i32.const 2
      local.get 5
      i32.load offset=28
      i32.load offset=12
      call_indirect (type 4)
      local.set 6
    end
    local.get 0
    i32.const 1
    i32.store8 offset=5
    local.get 0
    local.get 6
    i32.store8 offset=4
    local.get 5
    i32.const 64
    i32.add
    global.set 0
    local.get 0)
  (func $<&T_as_core::fmt::Display>::fmt::h51e2cb33c6de9b00 (type 2) (param i32 i32) (result i32)
    local.get 1
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    call $core::fmt::Formatter::pad::ha508305181e859c2)
  (func $core::panic::panic_info::PanicInfo::payload::hbd7d38a06ae537e7 (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i64.load align=4
    i64.store)
  (func $core::panic::panic_info::PanicInfo::message::h39f920514669bb09 (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=8)
  (func $core::panic::panic_info::PanicInfo::location::h29f7f91081e7b35d (type 12) (param i32) (result i32)
    local.get 0
    i32.load offset=12)
  (func $core::panic::panic_info::PanicInfo::can_unwind::he45aae146040bf73 (type 12) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=16)
  (func $core::panicking::assert_failed_inner::h499c6b26752f6efd (type 14) (param i32 i32 i32 i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 112
    i32.sub
    local.tee 7
    global.set 0
    local.get 7
    local.get 2
    i32.store offset=12
    local.get 7
    local.get 1
    i32.store offset=8
    local.get 7
    local.get 4
    i32.store offset=20
    local.get 7
    local.get 3
    i32.store offset=16
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.const 255
            i32.and
            br_table 0 (;@4;) 1 (;@3;) 2 (;@2;) 0 (;@4;)
          end
          local.get 7
          i32.const 1053973
          i32.store offset=24
          i32.const 2
          local.set 2
          br 2 (;@1;)
        end
        local.get 7
        i32.const 1053971
        i32.store offset=24
        i32.const 2
        local.set 2
        br 1 (;@1;)
      end
      local.get 7
      i32.const 1053964
      i32.store offset=24
      i32.const 7
      local.set 2
    end
    local.get 7
    local.get 2
    i32.store offset=28
    block  ;; label = @1
      local.get 5
      i32.load offset=8
      br_if 0 (;@1;)
      local.get 7
      i32.const 56
      i32.add
      i32.const 20
      i32.add
      i32.const 49
      i32.store
      local.get 7
      i32.const 56
      i32.add
      i32.const 12
      i32.add
      i32.const 49
      i32.store
      local.get 7
      i32.const 88
      i32.add
      i32.const 12
      i32.add
      i32.const 4
      i32.store
      local.get 7
      i32.const 88
      i32.add
      i32.const 20
      i32.add
      i32.const 3
      i32.store
      local.get 7
      i32.const 1054072
      i32.store offset=96
      local.get 7
      i32.const 0
      i32.store offset=88
      local.get 7
      i32.const 50
      i32.store offset=60
      local.get 7
      local.get 7
      i32.const 56
      i32.add
      i32.store offset=104
      local.get 7
      local.get 7
      i32.const 16
      i32.add
      i32.store offset=72
      local.get 7
      local.get 7
      i32.const 8
      i32.add
      i32.store offset=64
      local.get 7
      local.get 7
      i32.const 24
      i32.add
      i32.store offset=56
      local.get 7
      i32.const 88
      i32.add
      local.get 6
      call $core::panicking::panic_fmt::h9d972fcdb087ce21
      unreachable
    end
    local.get 7
    i32.const 32
    i32.add
    i32.const 16
    i32.add
    local.get 5
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 7
    i32.const 32
    i32.add
    i32.const 8
    i32.add
    local.get 5
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 7
    local.get 5
    i64.load align=4
    i64.store offset=32
    local.get 7
    i32.const 88
    i32.add
    i32.const 12
    i32.add
    i32.const 4
    i32.store
    local.get 7
    i32.const 88
    i32.add
    i32.const 20
    i32.add
    i32.const 4
    i32.store
    local.get 7
    i32.const 84
    i32.add
    i32.const 51
    i32.store
    local.get 7
    i32.const 56
    i32.add
    i32.const 20
    i32.add
    i32.const 49
    i32.store
    local.get 7
    i32.const 56
    i32.add
    i32.const 12
    i32.add
    i32.const 49
    i32.store
    local.get 7
    i32.const 1054036
    i32.store offset=96
    local.get 7
    i32.const 0
    i32.store offset=88
    local.get 7
    i32.const 50
    i32.store offset=60
    local.get 7
    local.get 7
    i32.const 56
    i32.add
    i32.store offset=104
    local.get 7
    local.get 7
    i32.const 32
    i32.add
    i32.store offset=80
    local.get 7
    local.get 7
    i32.const 16
    i32.add
    i32.store offset=72
    local.get 7
    local.get 7
    i32.const 8
    i32.add
    i32.store offset=64
    local.get 7
    local.get 7
    i32.const 24
    i32.add
    i32.store offset=56
    local.get 7
    i32.const 88
    i32.add
    local.get 6
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $<&T_as_core::fmt::Debug>::fmt::h2b7d9e319f3644e6 (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    local.get 0
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 2))
  (func $<core::fmt::Arguments_as_core::fmt::Display>::fmt::h94ab3c4e57d83582 (type 2) (param i32 i32) (result i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    local.get 1
    i32.load offset=4
    local.set 3
    local.get 1
    i32.load
    local.set 1
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 0
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 0
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 0
    i64.load align=4
    i64.store offset=8
    local.get 1
    local.get 3
    local.get 2
    i32.const 8
    i32.add
    call $core::fmt::write::hb366ddae040241f7
    local.set 0
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 0)
  (func $core::result::unwrap_failed::h2b47cc7f7e98a508 (type 7) (param i32 i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 64
    i32.sub
    local.tee 5
    global.set 0
    local.get 5
    local.get 1
    i32.store offset=12
    local.get 5
    local.get 0
    i32.store offset=8
    local.get 5
    local.get 3
    i32.store offset=20
    local.get 5
    local.get 2
    i32.store offset=16
    local.get 5
    i32.const 24
    i32.add
    i32.const 12
    i32.add
    i32.const 2
    i32.store
    local.get 5
    i32.const 44
    i32.add
    i32.const 2
    i32.store
    local.get 5
    i32.const 48
    i32.add
    i32.const 12
    i32.add
    i32.const 49
    i32.store
    local.get 5
    i32.const 1054108
    i32.store offset=32
    local.get 5
    i32.const 0
    i32.store offset=24
    local.get 5
    i32.const 50
    i32.store offset=52
    local.get 5
    local.get 5
    i32.const 48
    i32.add
    i32.store offset=40
    local.get 5
    local.get 5
    i32.const 16
    i32.add
    i32.store offset=56
    local.get 5
    local.get 5
    i32.const 8
    i32.add
    i32.store offset=48
    local.get 5
    i32.const 24
    i32.add
    local.get 4
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $<core::fmt::builders::PadAdapter_as_core::fmt::Write>::write_str::h5c98716d5a71cc25 (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    i64.const 42949672961
    i64.store offset=32
    local.get 3
    local.get 2
    i32.store offset=28
    i32.const 0
    local.set 4
    local.get 3
    i32.const 0
    i32.store offset=24
    local.get 3
    local.get 2
    i32.store offset=20
    local.get 3
    local.get 1
    i32.store offset=16
    local.get 3
    local.get 2
    i32.store offset=12
    local.get 3
    i32.const 0
    i32.store offset=8
    local.get 0
    i32.load offset=4
    local.set 5
    local.get 0
    i32.load
    local.set 6
    local.get 0
    i32.load offset=8
    local.set 7
    i32.const 0
    local.set 8
    i32.const 0
    local.set 9
    block  ;; label = @1
      loop  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 9
            i32.const 255
            i32.and
            br_if 0 (;@4;)
            block  ;; label = @5
              local.get 8
              local.get 2
              i32.gt_u
              br_if 0 (;@5;)
              loop  ;; label = @6
                local.get 1
                local.get 8
                i32.add
                local.set 10
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 2
                    local.get 8
                    i32.sub
                    local.tee 11
                    i32.const 8
                    i32.lt_u
                    br_if 0 (;@8;)
                    local.get 3
                    i32.const 10
                    local.get 10
                    local.get 11
                    call $core::slice::memchr::memchr_aligned::hb22334f2708d3709
                    local.get 3
                    i32.load offset=4
                    local.set 0
                    local.get 3
                    i32.load
                    local.set 10
                    br 1 (;@7;)
                  end
                  i32.const 0
                  local.set 0
                  block  ;; label = @8
                    local.get 11
                    br_if 0 (;@8;)
                    i32.const 0
                    local.set 10
                    br 1 (;@7;)
                  end
                  loop  ;; label = @8
                    block  ;; label = @9
                      local.get 10
                      local.get 0
                      i32.add
                      i32.load8_u
                      i32.const 10
                      i32.ne
                      br_if 0 (;@9;)
                      i32.const 1
                      local.set 10
                      br 2 (;@7;)
                    end
                    local.get 11
                    local.get 0
                    i32.const 1
                    i32.add
                    local.tee 0
                    i32.ne
                    br_if 0 (;@8;)
                  end
                  i32.const 0
                  local.set 10
                  local.get 11
                  local.set 0
                end
                block  ;; label = @7
                  local.get 10
                  i32.const 1
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 2
                  local.set 8
                  br 2 (;@5;)
                end
                block  ;; label = @7
                  local.get 8
                  local.get 0
                  i32.add
                  local.tee 0
                  i32.const 1
                  i32.add
                  local.tee 8
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 8
                  local.get 2
                  i32.gt_u
                  br_if 0 (;@7;)
                  local.get 1
                  local.get 0
                  i32.add
                  i32.load8_u
                  i32.const 10
                  i32.ne
                  br_if 0 (;@7;)
                  i32.const 0
                  local.set 9
                  local.get 8
                  local.set 12
                  local.get 8
                  local.set 0
                  br 4 (;@3;)
                end
                local.get 8
                local.get 2
                i32.le_u
                br_if 0 (;@6;)
              end
            end
            i32.const 1
            local.set 9
            local.get 4
            local.set 12
            local.get 2
            local.set 0
            local.get 4
            local.get 2
            i32.ne
            br_if 1 (;@3;)
          end
          i32.const 0
          local.set 0
          br 2 (;@1;)
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 7
            i32.load8_u
            i32.eqz
            br_if 0 (;@4;)
            local.get 6
            i32.const 1054148
            i32.const 4
            local.get 5
            i32.load offset=12
            call_indirect (type 4)
            br_if 1 (;@3;)
          end
          local.get 1
          local.get 4
          i32.add
          local.set 11
          local.get 0
          local.get 4
          i32.sub
          local.set 10
          i32.const 0
          local.set 13
          block  ;; label = @4
            local.get 0
            local.get 4
            i32.eq
            br_if 0 (;@4;)
            local.get 10
            local.get 11
            i32.add
            i32.const -1
            i32.add
            i32.load8_u
            i32.const 10
            i32.eq
            local.set 13
          end
          local.get 7
          local.get 13
          i32.store8
          local.get 12
          local.set 4
          local.get 6
          local.get 11
          local.get 10
          local.get 5
          i32.load offset=12
          call_indirect (type 4)
          i32.eqz
          br_if 1 (;@2;)
        end
      end
      i32.const 1
      local.set 0
    end
    local.get 3
    i32.const 48
    i32.add
    global.set 0
    local.get 0)
  (func $core::fmt::builders::DebugStruct::finish::h28db02fb670680d5 (type 12) (param i32) (result i32)
    (local i32 i32)
    local.get 0
    i32.load8_u offset=4
    local.set 1
    block  ;; label = @1
      local.get 0
      i32.load8_u offset=5
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 255
      i32.and
      local.set 2
      i32.const 1
      local.set 1
      block  ;; label = @2
        local.get 2
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 0
          i32.load
          local.tee 1
          i32.load8_u offset=24
          i32.const 4
          i32.and
          br_if 0 (;@3;)
          local.get 1
          i32.load
          i32.const 1054163
          i32.const 2
          local.get 1
          i32.load offset=4
          i32.load offset=12
          call_indirect (type 4)
          local.set 1
          br 1 (;@2;)
        end
        local.get 1
        i32.load
        i32.const 1054162
        i32.const 1
        local.get 1
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        local.set 1
      end
      local.get 0
      local.get 1
      i32.store8 offset=4
    end
    local.get 1
    i32.const 255
    i32.and
    i32.const 0
    i32.ne)
  (func $core::fmt::builders::DebugTuple::field::he30af5de70b50698 (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i64 i64)
    global.get 0
    i32.const 64
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.load8_u offset=8
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load
        local.set 4
        i32.const 1
        local.set 5
        br 1 (;@1;)
      end
      local.get 0
      i32.load
      local.set 4
      block  ;; label = @2
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.tee 6
        i32.load offset=24
        local.tee 7
        i32.const 4
        i32.and
        br_if 0 (;@2;)
        i32.const 1
        local.set 5
        local.get 6
        i32.load
        i32.const 1054157
        i32.const 1054167
        local.get 4
        select
        i32.const 2
        i32.const 1
        local.get 4
        select
        local.get 6
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        local.get 1
        local.get 6
        local.get 2
        i32.load offset=12
        call_indirect (type 2)
        local.set 5
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 4
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 6
          i32.load
          i32.const 1054165
          i32.const 2
          local.get 6
          i32.load offset=4
          i32.load offset=12
          call_indirect (type 4)
          i32.eqz
          br_if 0 (;@3;)
          i32.const 1
          local.set 5
          i32.const 0
          local.set 4
          br 2 (;@1;)
        end
        local.get 6
        i32.load offset=24
        local.set 7
      end
      i32.const 1
      local.set 5
      local.get 3
      i32.const 1
      i32.store8 offset=23
      local.get 3
      i32.const 1054124
      i32.store offset=28
      local.get 3
      local.get 6
      i64.load align=4
      i64.store offset=8
      local.get 3
      local.get 3
      i32.const 23
      i32.add
      i32.store offset=16
      local.get 6
      i64.load offset=8 align=4
      local.set 8
      local.get 6
      i64.load offset=16 align=4
      local.set 9
      local.get 3
      local.get 6
      i32.load8_u offset=32
      i32.store8 offset=56
      local.get 3
      local.get 6
      i32.load offset=28
      i32.store offset=52
      local.get 3
      local.get 7
      i32.store offset=48
      local.get 3
      local.get 9
      i64.store offset=40
      local.get 3
      local.get 8
      i64.store offset=32
      local.get 3
      local.get 3
      i32.const 8
      i32.add
      i32.store offset=24
      local.get 1
      local.get 3
      i32.const 24
      i32.add
      local.get 2
      i32.load offset=12
      call_indirect (type 2)
      br_if 0 (;@1;)
      local.get 3
      i32.load offset=24
      i32.const 1054155
      i32.const 2
      local.get 3
      i32.load offset=28
      i32.load offset=12
      call_indirect (type 4)
      local.set 5
    end
    local.get 0
    local.get 5
    i32.store8 offset=8
    local.get 0
    local.get 4
    i32.const 1
    i32.add
    i32.store
    local.get 3
    i32.const 64
    i32.add
    global.set 0
    local.get 0)
  (func $core::fmt::builders::DebugTuple::finish::hbbc9fdf689f78815 (type 12) (param i32) (result i32)
    (local i32 i32 i32)
    local.get 0
    i32.load8_u offset=8
    local.set 1
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 255
      i32.and
      local.set 3
      i32.const 1
      local.set 1
      block  ;; label = @2
        local.get 3
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.const 1
              i32.ne
              br_if 0 (;@5;)
              local.get 0
              i32.load8_u offset=9
              i32.const 255
              i32.and
              br_if 1 (;@4;)
            end
            local.get 0
            i32.load offset=4
            local.set 3
            br 1 (;@3;)
          end
          local.get 0
          i32.const 4
          i32.add
          i32.load
          local.tee 3
          i32.load8_u offset=24
          i32.const 4
          i32.and
          br_if 0 (;@3;)
          i32.const 1
          local.set 1
          local.get 3
          i32.load
          i32.const 1054168
          i32.const 1
          local.get 3
          i32.load offset=4
          i32.load offset=12
          call_indirect (type 4)
          br_if 1 (;@2;)
        end
        local.get 3
        i32.load
        i32.const 1053867
        i32.const 1
        local.get 3
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        local.set 1
      end
      local.get 0
      local.get 1
      i32.store8 offset=8
    end
    local.get 1
    i32.const 255
    i32.and
    i32.const 0
    i32.ne)
  (func $core::fmt::builders::DebugInner::entry::hde775d554eea5cf2 (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i64 i64)
    global.get 0
    i32.const 64
    i32.sub
    local.tee 3
    global.set 0
    i32.const 1
    local.set 4
    block  ;; label = @1
      local.get 0
      i32.load8_u offset=4
      br_if 0 (;@1;)
      local.get 0
      i32.load8_u offset=5
      local.set 4
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              i32.load
              local.tee 5
              i32.load offset=24
              local.tee 6
              i32.const 4
              i32.and
              br_if 0 (;@5;)
              local.get 4
              i32.const 255
              i32.and
              br_if 1 (;@4;)
              br 3 (;@2;)
            end
            local.get 4
            i32.const 255
            i32.and
            br_if 1 (;@3;)
            i32.const 1
            local.set 4
            local.get 5
            i32.load
            i32.const 1054169
            i32.const 1
            local.get 5
            i32.load offset=4
            i32.load offset=12
            call_indirect (type 4)
            br_if 3 (;@1;)
            local.get 5
            i32.load offset=24
            local.set 6
            br 1 (;@3;)
          end
          i32.const 1
          local.set 4
          local.get 5
          i32.load
          i32.const 1054157
          i32.const 2
          local.get 5
          i32.load offset=4
          i32.load offset=12
          call_indirect (type 4)
          i32.eqz
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
        i32.const 1
        local.set 4
        local.get 3
        i32.const 1
        i32.store8 offset=23
        local.get 3
        i32.const 1054124
        i32.store offset=28
        local.get 3
        local.get 5
        i64.load align=4
        i64.store offset=8
        local.get 3
        local.get 3
        i32.const 23
        i32.add
        i32.store offset=16
        local.get 5
        i64.load offset=8 align=4
        local.set 7
        local.get 5
        i64.load offset=16 align=4
        local.set 8
        local.get 3
        local.get 5
        i32.load8_u offset=32
        i32.store8 offset=56
        local.get 3
        local.get 5
        i32.load offset=28
        i32.store offset=52
        local.get 3
        local.get 6
        i32.store offset=48
        local.get 3
        local.get 8
        i64.store offset=40
        local.get 3
        local.get 7
        i64.store offset=32
        local.get 3
        local.get 3
        i32.const 8
        i32.add
        i32.store offset=24
        local.get 1
        local.get 3
        i32.const 24
        i32.add
        local.get 2
        i32.load offset=12
        call_indirect (type 2)
        br_if 1 (;@1;)
        local.get 3
        i32.load offset=24
        i32.const 1054155
        i32.const 2
        local.get 3
        i32.load offset=28
        i32.load offset=12
        call_indirect (type 4)
        local.set 4
        br 1 (;@1;)
      end
      local.get 1
      local.get 5
      local.get 2
      i32.load offset=12
      call_indirect (type 2)
      local.set 4
    end
    local.get 0
    i32.const 1
    i32.store8 offset=5
    local.get 0
    local.get 4
    i32.store8 offset=4
    local.get 3
    i32.const 64
    i32.add
    global.set 0)
  (func $core::fmt::builders::DebugSet::entry::h7dc7deb3f8909bb0 (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    local.get 1
    local.get 2
    call $core::fmt::builders::DebugInner::entry::hde775d554eea5cf2
    local.get 0)
  (func $core::fmt::builders::DebugList::finish::h03a58eb4e118cf5e (type 12) (param i32) (result i32)
    (local i32)
    i32.const 1
    local.set 1
    block  ;; label = @1
      local.get 0
      i32.load8_u offset=4
      br_if 0 (;@1;)
      local.get 0
      i32.load
      local.tee 0
      i32.load
      i32.const 1054171
      i32.const 1
      local.get 0
      i32.const 4
      i32.add
      i32.load
      i32.load offset=12
      call_indirect (type 4)
      local.set 1
    end
    local.get 1)
  (func $core::fmt::Formatter::pad_integral::hb3903a931cea1b46 (type 15) (param i32 i32 i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.eqz
        br_if 0 (;@2;)
        i32.const 43
        i32.const 1114112
        local.get 0
        i32.load offset=24
        local.tee 6
        i32.const 1
        i32.and
        local.tee 1
        select
        local.set 7
        local.get 1
        local.get 5
        i32.add
        local.set 8
        br 1 (;@1;)
      end
      local.get 5
      i32.const 1
      i32.add
      local.set 8
      local.get 0
      i32.load offset=24
      local.set 6
      i32.const 45
      local.set 7
    end
    block  ;; label = @1
      block  ;; label = @2
        local.get 6
        i32.const 4
        i32.and
        br_if 0 (;@2;)
        i32.const 0
        local.set 2
        br 1 (;@1;)
      end
      block  ;; label = @2
        block  ;; label = @3
          local.get 3
          i32.const 16
          i32.lt_u
          br_if 0 (;@3;)
          local.get 2
          local.get 3
          call $core::str::count::do_count_chars::h047305ee9bcb28cd
          local.set 9
          br 1 (;@2;)
        end
        block  ;; label = @3
          local.get 3
          br_if 0 (;@3;)
          i32.const 0
          local.set 9
          br 1 (;@2;)
        end
        local.get 3
        i32.const 3
        i32.and
        local.set 10
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.const -1
            i32.add
            i32.const 3
            i32.ge_u
            br_if 0 (;@4;)
            i32.const 0
            local.set 9
            local.get 2
            local.set 1
            br 1 (;@3;)
          end
          local.get 3
          i32.const -4
          i32.and
          local.set 11
          i32.const 0
          local.set 9
          local.get 2
          local.set 1
          loop  ;; label = @4
            local.get 9
            local.get 1
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.get 1
            i32.load8_s offset=1
            i32.const -65
            i32.gt_s
            i32.add
            local.get 1
            i32.load8_s offset=2
            i32.const -65
            i32.gt_s
            i32.add
            local.get 1
            i32.load8_s offset=3
            i32.const -65
            i32.gt_s
            i32.add
            local.set 9
            local.get 1
            i32.const 4
            i32.add
            local.set 1
            local.get 11
            i32.const -4
            i32.add
            local.tee 11
            br_if 0 (;@4;)
          end
        end
        local.get 10
        i32.eqz
        br_if 0 (;@2;)
        loop  ;; label = @3
          local.get 9
          local.get 1
          i32.load8_s
          i32.const -65
          i32.gt_s
          i32.add
          local.set 9
          local.get 1
          i32.const 1
          i32.add
          local.set 1
          local.get 10
          i32.const -1
          i32.add
          local.tee 10
          br_if 0 (;@3;)
        end
      end
      local.get 9
      local.get 8
      i32.add
      local.set 8
    end
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.load offset=8
        br_if 0 (;@2;)
        i32.const 1
        local.set 1
        local.get 0
        i32.load
        local.tee 9
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.tee 0
        local.get 7
        local.get 2
        local.get 3
        call $core::fmt::Formatter::pad_integral::write_prefix::hcc3ab26bdaf983e2
        br_if 1 (;@1;)
        local.get 9
        local.get 4
        local.get 5
        local.get 0
        i32.load offset=12
        call_indirect (type 4)
        return
      end
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 0
                i32.const 12
                i32.add
                i32.load
                local.tee 11
                local.get 8
                i32.le_u
                br_if 0 (;@6;)
                local.get 6
                i32.const 8
                i32.and
                br_if 4 (;@2;)
                local.get 11
                local.get 8
                i32.sub
                local.tee 9
                local.set 11
                i32.const 1
                local.get 0
                i32.load8_u offset=32
                local.tee 1
                local.get 1
                i32.const 3
                i32.eq
                select
                i32.const 3
                i32.and
                local.tee 1
                br_table 3 (;@3;) 1 (;@5;) 2 (;@4;) 3 (;@3;)
              end
              i32.const 1
              local.set 1
              local.get 0
              i32.load
              local.tee 9
              local.get 0
              i32.const 4
              i32.add
              i32.load
              local.tee 0
              local.get 7
              local.get 2
              local.get 3
              call $core::fmt::Formatter::pad_integral::write_prefix::hcc3ab26bdaf983e2
              br_if 4 (;@1;)
              local.get 9
              local.get 4
              local.get 5
              local.get 0
              i32.load offset=12
              call_indirect (type 4)
              return
            end
            i32.const 0
            local.set 11
            local.get 9
            local.set 1
            br 1 (;@3;)
          end
          local.get 9
          i32.const 1
          i32.shr_u
          local.set 1
          local.get 9
          i32.const 1
          i32.add
          i32.const 1
          i32.shr_u
          local.set 11
        end
        local.get 1
        i32.const 1
        i32.add
        local.set 1
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.set 10
        local.get 0
        i32.load offset=28
        local.set 9
        local.get 0
        i32.load
        local.set 0
        block  ;; label = @3
          loop  ;; label = @4
            local.get 1
            i32.const -1
            i32.add
            local.tee 1
            i32.eqz
            br_if 1 (;@3;)
            local.get 0
            local.get 9
            local.get 10
            i32.load offset=16
            call_indirect (type 2)
            i32.eqz
            br_if 0 (;@4;)
          end
          i32.const 1
          return
        end
        i32.const 1
        local.set 1
        local.get 9
        i32.const 1114112
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        local.get 10
        local.get 7
        local.get 2
        local.get 3
        call $core::fmt::Formatter::pad_integral::write_prefix::hcc3ab26bdaf983e2
        br_if 1 (;@1;)
        local.get 0
        local.get 4
        local.get 5
        local.get 10
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        i32.const 0
        local.set 1
        block  ;; label = @3
          loop  ;; label = @4
            block  ;; label = @5
              local.get 11
              local.get 1
              i32.ne
              br_if 0 (;@5;)
              local.get 11
              local.set 1
              br 2 (;@3;)
            end
            local.get 1
            i32.const 1
            i32.add
            local.set 1
            local.get 0
            local.get 9
            local.get 10
            i32.load offset=16
            call_indirect (type 2)
            i32.eqz
            br_if 0 (;@4;)
          end
          local.get 1
          i32.const -1
          i32.add
          local.set 1
        end
        local.get 1
        local.get 11
        i32.lt_u
        local.set 1
        br 1 (;@1;)
      end
      local.get 0
      i32.load offset=28
      local.set 6
      local.get 0
      i32.const 48
      i32.store offset=28
      local.get 0
      i32.load8_u offset=32
      local.set 12
      i32.const 1
      local.set 1
      local.get 0
      i32.const 1
      i32.store8 offset=32
      local.get 0
      i32.load
      local.tee 9
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 10
      local.get 7
      local.get 2
      local.get 3
      call $core::fmt::Formatter::pad_integral::write_prefix::hcc3ab26bdaf983e2
      br_if 0 (;@1;)
      local.get 11
      local.get 8
      i32.sub
      i32.const 1
      i32.add
      local.set 1
      block  ;; label = @2
        loop  ;; label = @3
          local.get 1
          i32.const -1
          i32.add
          local.tee 1
          i32.eqz
          br_if 1 (;@2;)
          local.get 9
          i32.const 48
          local.get 10
          i32.load offset=16
          call_indirect (type 2)
          i32.eqz
          br_if 0 (;@3;)
        end
        i32.const 1
        return
      end
      i32.const 1
      local.set 1
      local.get 9
      local.get 4
      local.get 5
      local.get 10
      i32.load offset=12
      call_indirect (type 4)
      br_if 0 (;@1;)
      local.get 0
      local.get 12
      i32.store8 offset=32
      local.get 0
      local.get 6
      i32.store offset=28
      i32.const 0
      return
    end
    local.get 1)
  (func $core::fmt::Write::write_char::h37130539227c56c9 (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    i32.const 0
    i32.store offset=12
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const 128
            i32.lt_u
            br_if 0 (;@4;)
            local.get 1
            i32.const 2048
            i32.lt_u
            br_if 1 (;@3;)
            local.get 1
            i32.const 65536
            i32.ge_u
            br_if 2 (;@2;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=14
            local.get 2
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 224
            i32.or
            i32.store8 offset=12
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            i32.const 3
            local.set 1
            br 3 (;@1;)
          end
          local.get 2
          local.get 1
          i32.store8 offset=12
          i32.const 1
          local.set 1
          br 2 (;@1;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 192
        i32.or
        i32.store8 offset=12
        i32.const 2
        local.set 1
        br 1 (;@1;)
      end
      local.get 2
      local.get 1
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=15
      local.get 2
      local.get 1
      i32.const 6
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=14
      local.get 2
      local.get 1
      i32.const 12
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=13
      local.get 2
      local.get 1
      i32.const 18
      i32.shr_u
      i32.const 7
      i32.and
      i32.const 240
      i32.or
      i32.store8 offset=12
      i32.const 4
      local.set 1
    end
    local.get 0
    local.get 2
    i32.const 12
    i32.add
    local.get 1
    call $<core::fmt::builders::PadAdapter_as_core::fmt::Write>::write_str::h5c98716d5a71cc25
    local.set 1
    local.get 2
    i32.const 16
    i32.add
    global.set 0
    local.get 1)
  (func $core::fmt::Write::write_fmt::h31e9ffb45358f2e7 (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 0
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1054420
    local.get 2
    i32.const 8
    i32.add
    call $core::fmt::write::hb366ddae040241f7
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 1)
  (func $<&mut_W_as_core::fmt::Write>::write_str::h299ec16a7091eca3 (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    local.get 2
    call $<core::fmt::builders::PadAdapter_as_core::fmt::Write>::write_str::h5c98716d5a71cc25)
  (func $<&mut_W_as_core::fmt::Write>::write_char::h150313a070e73827 (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.load
    local.set 0
    local.get 2
    i32.const 0
    i32.store offset=12
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const 128
            i32.lt_u
            br_if 0 (;@4;)
            local.get 1
            i32.const 2048
            i32.lt_u
            br_if 1 (;@3;)
            local.get 1
            i32.const 65536
            i32.ge_u
            br_if 2 (;@2;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=14
            local.get 2
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 224
            i32.or
            i32.store8 offset=12
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            i32.const 3
            local.set 1
            br 3 (;@1;)
          end
          local.get 2
          local.get 1
          i32.store8 offset=12
          i32.const 1
          local.set 1
          br 2 (;@1;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 192
        i32.or
        i32.store8 offset=12
        i32.const 2
        local.set 1
        br 1 (;@1;)
      end
      local.get 2
      local.get 1
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=15
      local.get 2
      local.get 1
      i32.const 6
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=14
      local.get 2
      local.get 1
      i32.const 12
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=13
      local.get 2
      local.get 1
      i32.const 18
      i32.shr_u
      i32.const 7
      i32.and
      i32.const 240
      i32.or
      i32.store8 offset=12
      i32.const 4
      local.set 1
    end
    local.get 0
    local.get 2
    i32.const 12
    i32.add
    local.get 1
    call $<core::fmt::builders::PadAdapter_as_core::fmt::Write>::write_str::h5c98716d5a71cc25
    local.set 1
    local.get 2
    i32.const 16
    i32.add
    global.set 0
    local.get 1)
  (func $<&mut_W_as_core::fmt::Write>::write_fmt::h4c061b74e85ac905 (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 0
    i32.load
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1054420
    local.get 2
    i32.const 8
    i32.add
    call $core::fmt::write::hb366ddae040241f7
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 1)
  (func $core::str::count::do_count_chars::h047305ee9bcb28cd (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.const 3
        i32.add
        i32.const -4
        i32.and
        local.tee 2
        local.get 0
        i32.sub
        local.tee 3
        local.get 1
        i32.gt_u
        br_if 0 (;@2;)
        local.get 3
        i32.const 4
        i32.gt_u
        br_if 0 (;@2;)
        local.get 1
        local.get 3
        i32.sub
        local.tee 4
        i32.const 4
        i32.lt_u
        br_if 0 (;@2;)
        local.get 4
        i32.const 3
        i32.and
        local.set 5
        i32.const 0
        local.set 6
        i32.const 0
        local.set 1
        block  ;; label = @3
          local.get 2
          local.get 0
          i32.eq
          br_if 0 (;@3;)
          local.get 3
          i32.const 3
          i32.and
          local.set 7
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              local.get 0
              i32.const -1
              i32.xor
              i32.add
              i32.const 3
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              local.set 1
              local.get 0
              local.set 2
              br 1 (;@4;)
            end
            local.get 3
            i32.const -4
            i32.and
            local.set 8
            i32.const 0
            local.set 1
            local.get 0
            local.set 2
            loop  ;; label = @5
              local.get 1
              local.get 2
              i32.load8_s
              i32.const -65
              i32.gt_s
              i32.add
              local.get 2
              i32.load8_s offset=1
              i32.const -65
              i32.gt_s
              i32.add
              local.get 2
              i32.load8_s offset=2
              i32.const -65
              i32.gt_s
              i32.add
              local.get 2
              i32.load8_s offset=3
              i32.const -65
              i32.gt_s
              i32.add
              local.set 1
              local.get 2
              i32.const 4
              i32.add
              local.set 2
              local.get 8
              i32.const -4
              i32.add
              local.tee 8
              br_if 0 (;@5;)
            end
          end
          local.get 7
          i32.eqz
          br_if 0 (;@3;)
          loop  ;; label = @4
            local.get 1
            local.get 2
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.set 1
            local.get 2
            i32.const 1
            i32.add
            local.set 2
            local.get 7
            i32.const -1
            i32.add
            local.tee 7
            br_if 0 (;@4;)
          end
        end
        local.get 0
        local.get 3
        i32.add
        local.set 0
        block  ;; label = @3
          local.get 5
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 4
          i32.const -4
          i32.and
          i32.add
          local.tee 2
          i32.load8_s
          i32.const -65
          i32.gt_s
          local.set 6
          local.get 5
          i32.const 1
          i32.eq
          br_if 0 (;@3;)
          local.get 6
          local.get 2
          i32.load8_s offset=1
          i32.const -65
          i32.gt_s
          i32.add
          local.set 6
          local.get 5
          i32.const 2
          i32.eq
          br_if 0 (;@3;)
          local.get 6
          local.get 2
          i32.load8_s offset=2
          i32.const -65
          i32.gt_s
          i32.add
          local.set 6
        end
        local.get 4
        i32.const 2
        i32.shr_u
        local.set 3
        local.get 6
        local.get 1
        i32.add
        local.set 7
        loop  ;; label = @3
          local.get 0
          local.set 6
          local.get 3
          i32.eqz
          br_if 2 (;@1;)
          local.get 3
          i32.const 192
          local.get 3
          i32.const 192
          i32.lt_u
          select
          local.tee 4
          i32.const 3
          i32.and
          local.set 5
          local.get 4
          i32.const 2
          i32.shl
          local.set 9
          block  ;; label = @4
            block  ;; label = @5
              local.get 4
              i32.const 252
              i32.and
              local.tee 10
              br_if 0 (;@5;)
              i32.const 0
              local.set 2
              br 1 (;@4;)
            end
            local.get 6
            local.get 10
            i32.const 2
            i32.shl
            i32.add
            local.set 8
            i32.const 0
            local.set 2
            local.get 6
            local.set 0
            loop  ;; label = @5
              local.get 0
              i32.eqz
              br_if 1 (;@4;)
              local.get 0
              i32.const 12
              i32.add
              i32.load
              local.tee 1
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 1
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 0
              i32.const 8
              i32.add
              i32.load
              local.tee 1
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 1
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 0
              i32.const 4
              i32.add
              i32.load
              local.tee 1
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 1
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 0
              i32.load
              local.tee 1
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 1
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 2
              i32.add
              i32.add
              i32.add
              i32.add
              local.set 2
              local.get 0
              i32.const 16
              i32.add
              local.tee 0
              local.get 8
              i32.ne
              br_if 0 (;@5;)
            end
          end
          local.get 3
          local.get 4
          i32.sub
          local.set 3
          local.get 6
          local.get 9
          i32.add
          local.set 0
          local.get 2
          i32.const 8
          i32.shr_u
          i32.const 16711935
          i32.and
          local.get 2
          i32.const 16711935
          i32.and
          i32.add
          i32.const 65537
          i32.mul
          i32.const 16
          i32.shr_u
          local.get 7
          i32.add
          local.set 7
          local.get 5
          i32.eqz
          br_if 0 (;@3;)
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 6
            br_if 0 (;@4;)
            i32.const 0
            local.set 2
            br 1 (;@3;)
          end
          local.get 6
          local.get 10
          i32.const 2
          i32.shl
          i32.add
          local.set 0
          local.get 5
          i32.const -1
          i32.add
          i32.const 1073741823
          i32.and
          local.tee 2
          i32.const 1
          i32.add
          local.tee 8
          i32.const 3
          i32.and
          local.set 1
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.const 3
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              local.set 2
              br 1 (;@4;)
            end
            local.get 8
            i32.const 2147483644
            i32.and
            local.set 8
            i32.const 0
            local.set 2
            loop  ;; label = @5
              local.get 0
              i32.const 12
              i32.add
              i32.load
              local.tee 3
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 3
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 0
              i32.const 8
              i32.add
              i32.load
              local.tee 3
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 3
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 0
              i32.const 4
              i32.add
              i32.load
              local.tee 3
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 3
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 0
              i32.load
              local.tee 3
              i32.const -1
              i32.xor
              i32.const 7
              i32.shr_u
              local.get 3
              i32.const 6
              i32.shr_u
              i32.or
              i32.const 16843009
              i32.and
              local.get 2
              i32.add
              i32.add
              i32.add
              i32.add
              local.set 2
              local.get 0
              i32.const 16
              i32.add
              local.set 0
              local.get 8
              i32.const -4
              i32.add
              local.tee 8
              br_if 0 (;@5;)
            end
          end
          local.get 1
          i32.eqz
          br_if 0 (;@3;)
          loop  ;; label = @4
            local.get 0
            i32.load
            local.tee 8
            i32.const -1
            i32.xor
            i32.const 7
            i32.shr_u
            local.get 8
            i32.const 6
            i32.shr_u
            i32.or
            i32.const 16843009
            i32.and
            local.get 2
            i32.add
            local.set 2
            local.get 0
            i32.const 4
            i32.add
            local.set 0
            local.get 1
            i32.const -1
            i32.add
            local.tee 1
            br_if 0 (;@4;)
          end
        end
        local.get 2
        i32.const 8
        i32.shr_u
        i32.const 16711935
        i32.and
        local.get 2
        i32.const 16711935
        i32.and
        i32.add
        i32.const 65537
        i32.mul
        i32.const 16
        i32.shr_u
        local.get 7
        i32.add
        return
      end
      block  ;; label = @2
        local.get 1
        br_if 0 (;@2;)
        i32.const 0
        return
      end
      local.get 1
      i32.const 3
      i32.and
      local.set 2
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const -1
          i32.add
          i32.const 3
          i32.ge_u
          br_if 0 (;@3;)
          i32.const 0
          local.set 7
          br 1 (;@2;)
        end
        local.get 1
        i32.const -4
        i32.and
        local.set 1
        i32.const 0
        local.set 7
        loop  ;; label = @3
          local.get 7
          local.get 0
          i32.load8_s
          i32.const -65
          i32.gt_s
          i32.add
          local.get 0
          i32.load8_s offset=1
          i32.const -65
          i32.gt_s
          i32.add
          local.get 0
          i32.load8_s offset=2
          i32.const -65
          i32.gt_s
          i32.add
          local.get 0
          i32.load8_s offset=3
          i32.const -65
          i32.gt_s
          i32.add
          local.set 7
          local.get 0
          i32.const 4
          i32.add
          local.set 0
          local.get 1
          i32.const -4
          i32.add
          local.tee 1
          br_if 0 (;@3;)
        end
      end
      local.get 2
      i32.eqz
      br_if 0 (;@1;)
      loop  ;; label = @2
        local.get 7
        local.get 0
        i32.load8_s
        i32.const -65
        i32.gt_s
        i32.add
        local.set 7
        local.get 0
        i32.const 1
        i32.add
        local.set 0
        local.get 2
        i32.const -1
        i32.add
        local.tee 2
        br_if 0 (;@2;)
      end
    end
    local.get 7)
  (func $core::fmt::Formatter::pad_integral::write_prefix::hcc3ab26bdaf983e2 (type 13) (param i32 i32 i32 i32 i32) (result i32)
    (local i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.const 1114112
          i32.eq
          br_if 0 (;@3;)
          i32.const 1
          local.set 5
          local.get 0
          local.get 2
          local.get 1
          i32.load offset=16
          call_indirect (type 2)
          br_if 1 (;@2;)
        end
        local.get 3
        br_if 1 (;@1;)
        i32.const 0
        local.set 5
      end
      local.get 5
      return
    end
    local.get 0
    local.get 3
    local.get 4
    local.get 1
    i32.load offset=12
    call_indirect (type 4))
  (func $core::str::slice_error_fail::heda988cb317c5e8e (type 7) (param i32 i32 i32 i32 i32)
    local.get 0
    local.get 1
    local.get 2
    local.get 3
    local.get 4
    call $core::str::slice_error_fail_rt::he2de3b4cdeda08cc
    unreachable)
  (func $core::fmt::Formatter::write_str::hbcb394ab6bee87b8 (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    local.get 2
    local.get 0
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 4))
  (func $core::fmt::Formatter::debug_lower_hex::h768a8b81c9cb249b (type 12) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=24
    i32.const 16
    i32.and
    i32.const 4
    i32.shr_u)
  (func $core::fmt::Formatter::debug_upper_hex::hdedc73e262a99d1d (type 12) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=24
    i32.const 32
    i32.and
    i32.const 5
    i32.shr_u)
  (func $core::fmt::Formatter::debug_struct::h8a11139ff0c9eae6 (type 8) (param i32 i32 i32 i32)
    local.get 1
    i32.load
    local.get 2
    local.get 3
    local.get 1
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 4)
    local.set 3
    local.get 0
    i32.const 0
    i32.store8 offset=5
    local.get 0
    local.get 3
    i32.store8 offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $core::fmt::Formatter::debug_struct_field2_finish::h953b04c6ca022536 (type 16) (param i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 11
    global.set 0
    local.get 0
    i32.load
    local.get 1
    local.get 2
    local.get 0
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 4)
    local.set 2
    local.get 11
    i32.const 0
    i32.store8 offset=13
    local.get 11
    local.get 2
    i32.store8 offset=12
    local.get 11
    local.get 0
    i32.store offset=8
    local.get 11
    i32.const 8
    i32.add
    local.get 3
    local.get 4
    local.get 5
    local.get 6
    call $core::fmt::builders::DebugStruct::field::h12418a0ca165575b
    local.get 7
    local.get 8
    local.get 9
    local.get 10
    call $core::fmt::builders::DebugStruct::field::h12418a0ca165575b
    local.set 2
    local.get 11
    i32.load8_u offset=12
    local.set 0
    block  ;; label = @1
      local.get 11
      i32.load8_u offset=13
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 255
      i32.and
      local.set 1
      i32.const 1
      local.set 0
      local.get 1
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 2
        i32.load
        local.tee 0
        i32.load8_u offset=24
        i32.const 4
        i32.and
        br_if 0 (;@2;)
        local.get 0
        i32.load
        i32.const 1054163
        i32.const 2
        local.get 0
        i32.load offset=4
        i32.load offset=12
        call_indirect (type 4)
        local.set 0
        br 1 (;@1;)
      end
      local.get 0
      i32.load
      i32.const 1054162
      i32.const 1
      local.get 0
      i32.load offset=4
      i32.load offset=12
      call_indirect (type 4)
      local.set 0
    end
    local.get 11
    i32.const 16
    i32.add
    global.set 0
    local.get 0
    i32.const 255
    i32.and
    i32.const 0
    i32.ne)
  (func $core::fmt::Formatter::debug_tuple::h959e9f235d4eb107 (type 8) (param i32 i32 i32 i32)
    local.get 0
    local.get 1
    i32.load
    local.get 2
    local.get 3
    local.get 1
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 4)
    i32.store8 offset=8
    local.get 0
    local.get 1
    i32.store offset=4
    local.get 0
    local.get 3
    i32.eqz
    i32.store8 offset=9
    local.get 0
    i32.const 0
    i32.store)
  (func $core::fmt::Formatter::debug_list::hda9252dcaf123f75 (type 3) (param i32 i32)
    (local i32)
    local.get 1
    i32.load
    i32.const 1054170
    i32.const 1
    local.get 1
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 4)
    local.set 2
    local.get 0
    i32.const 0
    i32.store8 offset=5
    local.get 0
    local.get 2
    i32.store8 offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $<str_as_core::fmt::Debug>::fmt::hcef99b0bb07b7e61 (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.load
          local.tee 3
          i32.const 34
          local.get 2
          i32.load offset=4
          local.tee 4
          i32.load offset=16
          local.tee 5
          call_indirect (type 2)
          br_if 0 (;@3;)
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              br_if 0 (;@5;)
              i32.const 0
              local.set 2
              br 1 (;@4;)
            end
            local.get 0
            local.get 1
            i32.add
            local.set 6
            i32.const 0
            local.set 2
            local.get 0
            local.set 7
            i32.const 0
            local.set 8
            block  ;; label = @5
              loop  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 7
                    local.tee 9
                    i32.load8_s
                    local.tee 10
                    i32.const -1
                    i32.le_s
                    br_if 0 (;@8;)
                    local.get 9
                    i32.const 1
                    i32.add
                    local.set 7
                    local.get 10
                    i32.const 255
                    i32.and
                    local.set 11
                    br 1 (;@7;)
                  end
                  local.get 9
                  i32.load8_u offset=1
                  i32.const 63
                  i32.and
                  local.set 12
                  local.get 10
                  i32.const 31
                  i32.and
                  local.set 13
                  block  ;; label = @8
                    local.get 10
                    i32.const -33
                    i32.gt_u
                    br_if 0 (;@8;)
                    local.get 13
                    i32.const 6
                    i32.shl
                    local.get 12
                    i32.or
                    local.set 11
                    local.get 9
                    i32.const 2
                    i32.add
                    local.set 7
                    br 1 (;@7;)
                  end
                  local.get 12
                  i32.const 6
                  i32.shl
                  local.get 9
                  i32.load8_u offset=2
                  i32.const 63
                  i32.and
                  i32.or
                  local.set 12
                  local.get 9
                  i32.const 3
                  i32.add
                  local.set 7
                  block  ;; label = @8
                    local.get 10
                    i32.const -16
                    i32.ge_u
                    br_if 0 (;@8;)
                    local.get 12
                    local.get 13
                    i32.const 12
                    i32.shl
                    i32.or
                    local.set 11
                    br 1 (;@7;)
                  end
                  local.get 12
                  i32.const 6
                  i32.shl
                  local.get 7
                  i32.load8_u
                  i32.const 63
                  i32.and
                  i32.or
                  local.get 13
                  i32.const 18
                  i32.shl
                  i32.const 1835008
                  i32.and
                  i32.or
                  local.tee 11
                  i32.const 1114112
                  i32.eq
                  br_if 2 (;@5;)
                  local.get 9
                  i32.const 4
                  i32.add
                  local.set 7
                end
                i32.const 1114114
                local.set 10
                i32.const 48
                local.set 14
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  local.get 11
                                  br_table 6 (;@9;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 2 (;@13;) 4 (;@11;) 1 (;@14;) 1 (;@14;) 3 (;@12;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 1 (;@14;) 5 (;@10;) 0 (;@15;)
                                end
                                local.get 11
                                i32.const 92
                                i32.eq
                                br_if 4 (;@10;)
                              end
                              block  ;; label = @14
                                local.get 11
                                call $core::unicode::unicode_data::grapheme_extend::lookup::h7909c05300f45458
                                br_if 0 (;@14;)
                                local.get 11
                                call $core::unicode::printable::is_printable::heeef4bd093e6052d
                                br_if 6 (;@8;)
                              end
                              local.get 11
                              i32.const 1114113
                              i32.eq
                              br_if 5 (;@8;)
                              local.get 11
                              i32.const 1
                              i32.or
                              i32.clz
                              i32.const 2
                              i32.shr_u
                              i32.const 7
                              i32.xor
                              local.set 14
                              local.get 11
                              local.set 10
                              br 4 (;@9;)
                            end
                            i32.const 116
                            local.set 14
                            br 3 (;@9;)
                          end
                          i32.const 114
                          local.set 14
                          br 2 (;@9;)
                        end
                        i32.const 110
                        local.set 14
                        br 1 (;@9;)
                      end
                      local.get 11
                      local.set 14
                    end
                    local.get 8
                    local.get 2
                    i32.lt_u
                    br_if 1 (;@7;)
                    block  ;; label = @9
                      local.get 2
                      i32.eqz
                      br_if 0 (;@9;)
                      block  ;; label = @10
                        local.get 2
                        local.get 1
                        i32.lt_u
                        br_if 0 (;@10;)
                        local.get 2
                        local.get 1
                        i32.eq
                        br_if 1 (;@9;)
                        br 3 (;@7;)
                      end
                      local.get 0
                      local.get 2
                      i32.add
                      i32.load8_s
                      i32.const -64
                      i32.lt_s
                      br_if 2 (;@7;)
                    end
                    block  ;; label = @9
                      local.get 8
                      i32.eqz
                      br_if 0 (;@9;)
                      block  ;; label = @10
                        local.get 8
                        local.get 1
                        i32.lt_u
                        br_if 0 (;@10;)
                        local.get 8
                        local.get 1
                        i32.ne
                        br_if 3 (;@7;)
                        br 1 (;@9;)
                      end
                      local.get 0
                      local.get 8
                      i32.add
                      i32.load8_s
                      i32.const -65
                      i32.le_s
                      br_if 2 (;@7;)
                    end
                    block  ;; label = @9
                      local.get 3
                      local.get 0
                      local.get 2
                      i32.add
                      local.get 8
                      local.get 2
                      i32.sub
                      local.get 4
                      i32.load offset=12
                      call_indirect (type 4)
                      i32.eqz
                      br_if 0 (;@9;)
                      i32.const 1
                      return
                    end
                    i32.const 5
                    local.set 13
                    loop  ;; label = @9
                      local.get 13
                      local.set 15
                      local.get 10
                      local.set 2
                      i32.const 1114113
                      local.set 10
                      i32.const 92
                      local.set 12
                      block  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  local.get 2
                                  i32.const -1114112
                                  i32.add
                                  i32.const 3
                                  local.get 2
                                  i32.const 1114111
                                  i32.gt_u
                                  select
                                  br_table 2 (;@13;) 1 (;@14;) 5 (;@10;) 0 (;@15;) 2 (;@13;)
                                end
                                i32.const 0
                                local.set 13
                                i32.const 125
                                local.set 12
                                local.get 2
                                local.set 10
                                block  ;; label = @15
                                  block  ;; label = @16
                                    block  ;; label = @17
                                      local.get 15
                                      i32.const 255
                                      i32.and
                                      br_table 4 (;@13;) 7 (;@10;) 5 (;@12;) 0 (;@17;) 1 (;@16;) 2 (;@15;) 4 (;@13;)
                                    end
                                    i32.const 2
                                    local.set 13
                                    i32.const 123
                                    local.set 12
                                    br 5 (;@11;)
                                  end
                                  i32.const 3
                                  local.set 13
                                  i32.const 117
                                  local.set 12
                                  br 4 (;@11;)
                                end
                                i32.const 4
                                local.set 13
                                i32.const 92
                                local.set 12
                                br 3 (;@11;)
                              end
                              i32.const 1114112
                              local.set 10
                              local.get 14
                              local.set 12
                              local.get 15
                              local.set 13
                              local.get 14
                              i32.const 1114112
                              i32.ne
                              br_if 3 (;@10;)
                            end
                            i32.const 1
                            local.set 2
                            block  ;; label = @13
                              local.get 11
                              i32.const 128
                              i32.lt_u
                              br_if 0 (;@13;)
                              i32.const 2
                              local.set 2
                              local.get 11
                              i32.const 2048
                              i32.lt_u
                              br_if 0 (;@13;)
                              i32.const 3
                              i32.const 4
                              local.get 11
                              i32.const 65536
                              i32.lt_u
                              select
                              local.set 2
                            end
                            local.get 2
                            local.get 8
                            i32.add
                            local.set 2
                            br 4 (;@8;)
                          end
                          local.get 15
                          i32.const 1
                          local.get 14
                          select
                          local.set 13
                          i32.const 48
                          i32.const 87
                          local.get 2
                          local.get 14
                          i32.const 2
                          i32.shl
                          i32.shr_u
                          i32.const 15
                          i32.and
                          local.tee 10
                          i32.const 10
                          i32.lt_u
                          select
                          local.get 10
                          i32.add
                          local.set 12
                          local.get 14
                          i32.const -1
                          i32.add
                          i32.const 0
                          local.get 14
                          select
                          local.set 14
                        end
                        local.get 2
                        local.set 10
                      end
                      local.get 3
                      local.get 12
                      local.get 5
                      call_indirect (type 2)
                      i32.eqz
                      br_if 0 (;@9;)
                    end
                    i32.const 1
                    return
                  end
                  local.get 8
                  local.get 9
                  i32.sub
                  local.get 7
                  i32.add
                  local.set 8
                  local.get 7
                  local.get 6
                  i32.ne
                  br_if 1 (;@6;)
                  br 2 (;@5;)
                end
              end
              local.get 0
              local.get 1
              local.get 2
              local.get 8
              i32.const 1054472
              call $core::str::slice_error_fail::heda988cb317c5e8e
              unreachable
            end
            block  ;; label = @5
              local.get 2
              br_if 0 (;@5;)
              i32.const 0
              local.set 2
              br 1 (;@4;)
            end
            block  ;; label = @5
              local.get 2
              local.get 1
              i32.lt_u
              br_if 0 (;@5;)
              local.get 2
              local.get 1
              i32.eq
              br_if 1 (;@4;)
              br 4 (;@1;)
            end
            local.get 0
            local.get 2
            i32.add
            i32.load8_s
            i32.const -65
            i32.le_s
            br_if 3 (;@1;)
          end
          local.get 3
          local.get 0
          local.get 2
          i32.add
          local.get 1
          local.get 2
          i32.sub
          local.get 4
          i32.load offset=12
          call_indirect (type 4)
          i32.eqz
          br_if 1 (;@2;)
        end
        i32.const 1
        return
      end
      local.get 3
      i32.const 34
      local.get 5
      call_indirect (type 2)
      return
    end
    local.get 0
    local.get 1
    local.get 2
    local.get 1
    i32.const 1054488
    call $core::str::slice_error_fail::heda988cb317c5e8e
    unreachable)
  (func $core::unicode::unicode_data::grapheme_extend::lookup::h7909c05300f45458 (type 12) (param i32) (result i32)
    (local i32 i32 i32 i32 i32)
    local.get 0
    i32.const 11
    i32.shl
    local.set 1
    i32.const 0
    local.set 2
    i32.const 33
    local.set 3
    i32.const 33
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        loop  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              i32.const -1
              local.get 3
              i32.const 1
              i32.shr_u
              local.get 2
              i32.add
              local.tee 5
              i32.const 2
              i32.shl
              i32.const 1056584
              i32.add
              i32.load
              i32.const 11
              i32.shl
              local.tee 3
              local.get 1
              i32.ne
              local.get 3
              local.get 1
              i32.lt_u
              select
              local.tee 3
              i32.const 1
              i32.ne
              br_if 0 (;@5;)
              local.get 5
              local.set 4
              br 1 (;@4;)
            end
            local.get 3
            i32.const 255
            i32.and
            i32.const 255
            i32.ne
            br_if 2 (;@2;)
            local.get 5
            i32.const 1
            i32.add
            local.set 2
          end
          local.get 4
          local.get 2
          i32.sub
          local.set 3
          local.get 4
          local.get 2
          i32.gt_u
          br_if 0 (;@3;)
          br 2 (;@1;)
        end
      end
      local.get 5
      i32.const 1
      i32.add
      local.set 2
    end
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.const 32
              i32.gt_u
              br_if 0 (;@5;)
              local.get 2
              i32.const 2
              i32.shl
              local.tee 1
              i32.const 1056584
              i32.add
              i32.load
              i32.const 21
              i32.shr_u
              local.set 4
              local.get 2
              i32.const 32
              i32.ne
              br_if 1 (;@4;)
              i32.const 727
              local.set 5
              i32.const 31
              local.set 2
              br 2 (;@3;)
            end
            local.get 2
            i32.const 33
            i32.const 1057444
            call $core::panicking::panic_bounds_check::h929177d2c8f5de7b
            unreachable
          end
          local.get 1
          i32.const 1056588
          i32.add
          i32.load
          i32.const 21
          i32.shr_u
          local.set 5
          local.get 2
          i32.eqz
          br_if 1 (;@2;)
          local.get 2
          i32.const -1
          i32.add
          local.set 2
        end
        local.get 2
        i32.const 2
        i32.shl
        i32.const 1056584
        i32.add
        i32.load
        i32.const 2097151
        i32.and
        local.set 2
        br 1 (;@1;)
      end
      i32.const 0
      local.set 2
    end
    block  ;; label = @1
      local.get 5
      local.get 4
      i32.const -1
      i32.xor
      i32.add
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 2
      i32.sub
      local.set 3
      local.get 4
      i32.const 727
      local.get 4
      i32.const 727
      i32.gt_u
      select
      local.set 1
      local.get 5
      i32.const -1
      i32.add
      local.set 5
      i32.const 0
      local.set 2
      loop  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            local.get 4
            i32.eq
            br_if 0 (;@4;)
            local.get 2
            local.get 4
            i32.const 1056716
            i32.add
            i32.load8_u
            i32.add
            local.tee 2
            local.get 3
            i32.le_u
            br_if 1 (;@3;)
            br 3 (;@1;)
          end
          local.get 1
          i32.const 727
          i32.const 1057444
          call $core::panicking::panic_bounds_check::h929177d2c8f5de7b
          unreachable
        end
        local.get 5
        local.get 4
        i32.const 1
        i32.add
        local.tee 4
        i32.ne
        br_if 0 (;@2;)
      end
      local.get 5
      local.set 4
    end
    local.get 4
    i32.const 1
    i32.and)
  (func $core::unicode::printable::is_printable::heeef4bd093e6052d (type 12) (param i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.const 32
      i32.ge_u
      br_if 0 (;@1;)
      i32.const 0
      return
    end
    i32.const 1
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.const 127
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.const 65536
        i32.lt_u
        br_if 1 (;@1;)
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.const 131072
            i32.lt_u
            br_if 0 (;@4;)
            block  ;; label = @5
              local.get 0
              i32.const -205744
              i32.add
              i32.const 712016
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              return
            end
            block  ;; label = @5
              local.get 0
              i32.const -201547
              i32.add
              i32.const 5
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              return
            end
            block  ;; label = @5
              local.get 0
              i32.const -195102
              i32.add
              i32.const 1506
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              return
            end
            block  ;; label = @5
              local.get 0
              i32.const -191457
              i32.add
              i32.const 3103
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              return
            end
            block  ;; label = @5
              local.get 0
              i32.const -183970
              i32.add
              i32.const 14
              i32.ge_u
              br_if 0 (;@5;)
              i32.const 0
              return
            end
            block  ;; label = @5
              local.get 0
              i32.const -2
              i32.and
              i32.const 178206
              i32.ne
              br_if 0 (;@5;)
              i32.const 0
              return
            end
            local.get 0
            i32.const -32
            i32.and
            i32.const 173792
            i32.ne
            br_if 1 (;@3;)
            i32.const 0
            return
          end
          local.get 0
          i32.const 1055802
          i32.const 44
          i32.const 1055890
          i32.const 196
          i32.const 1056086
          i32.const 450
          call $core::unicode::printable::check::hf41dd63b2bbe64a8
          return
        end
        i32.const 0
        local.set 1
        local.get 0
        i32.const -177978
        i32.add
        i32.const 6
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.const -1114112
        i32.add
        i32.const -196112
        i32.lt_u
        local.set 1
      end
      local.get 1
      return
    end
    local.get 0
    i32.const 1055132
    i32.const 40
    i32.const 1055212
    i32.const 287
    i32.const 1055499
    i32.const 303
    call $core::unicode::printable::check::hf41dd63b2bbe64a8)
  (func $<str_as_core::fmt::Display>::fmt::h4e369854c5064a2e (type 4) (param i32 i32 i32) (result i32)
    local.get 2
    local.get 0
    local.get 1
    call $core::fmt::Formatter::pad::ha508305181e859c2)
  (func $<char_as_core::fmt::Debug>::fmt::hf7e974522a789b5c (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    i32.const 1
    local.set 2
    block  ;; label = @1
      local.get 1
      i32.load
      local.tee 3
      i32.const 39
      local.get 1
      i32.load offset=4
      i32.load offset=16
      local.tee 4
      call_indirect (type 2)
      br_if 0 (;@1;)
      i32.const 1114114
      local.set 2
      i32.const 48
      local.set 5
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 0
                        i32.load
                        local.tee 1
                        br_table 8 (;@2;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 2 (;@8;) 4 (;@6;) 1 (;@9;) 1 (;@9;) 3 (;@7;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 5 (;@5;) 0 (;@10;)
                      end
                      local.get 1
                      i32.const 92
                      i32.eq
                      br_if 4 (;@5;)
                    end
                    local.get 1
                    call $core::unicode::unicode_data::grapheme_extend::lookup::h7909c05300f45458
                    i32.eqz
                    br_if 4 (;@4;)
                    local.get 1
                    i32.const 1
                    i32.or
                    i32.clz
                    i32.const 2
                    i32.shr_u
                    i32.const 7
                    i32.xor
                    local.set 5
                    br 5 (;@3;)
                  end
                  i32.const 116
                  local.set 5
                  br 5 (;@2;)
                end
                i32.const 114
                local.set 5
                br 4 (;@2;)
              end
              i32.const 110
              local.set 5
              br 3 (;@2;)
            end
            local.get 1
            local.set 5
            br 2 (;@2;)
          end
          i32.const 1114113
          local.set 2
          block  ;; label = @4
            local.get 1
            call $core::unicode::printable::is_printable::heeef4bd093e6052d
            i32.eqz
            br_if 0 (;@4;)
            local.get 1
            local.set 5
            br 2 (;@2;)
          end
          local.get 1
          i32.const 1
          i32.or
          i32.clz
          i32.const 2
          i32.shr_u
          i32.const 7
          i32.xor
          local.set 5
        end
        local.get 1
        local.set 2
      end
      i32.const 5
      local.set 6
      loop  ;; label = @2
        local.get 6
        local.set 7
        local.get 2
        local.set 1
        i32.const 1114113
        local.set 2
        i32.const 92
        local.set 0
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 1
                    i32.const -1114112
                    i32.add
                    i32.const 3
                    local.get 1
                    i32.const 1114111
                    i32.gt_u
                    select
                    br_table 2 (;@6;) 1 (;@7;) 5 (;@3;) 0 (;@8;) 2 (;@6;)
                  end
                  i32.const 0
                  local.set 6
                  i32.const 125
                  local.set 0
                  local.get 1
                  local.set 2
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 7
                        i32.const 255
                        i32.and
                        br_table 4 (;@6;) 7 (;@3;) 5 (;@5;) 0 (;@10;) 1 (;@9;) 2 (;@8;) 4 (;@6;)
                      end
                      i32.const 2
                      local.set 6
                      i32.const 123
                      local.set 0
                      br 5 (;@4;)
                    end
                    i32.const 3
                    local.set 6
                    i32.const 117
                    local.set 0
                    br 4 (;@4;)
                  end
                  i32.const 4
                  local.set 6
                  i32.const 92
                  local.set 0
                  br 3 (;@4;)
                end
                i32.const 1114112
                local.set 2
                local.get 5
                local.set 0
                local.get 7
                local.set 6
                local.get 5
                i32.const 1114112
                i32.ne
                br_if 3 (;@3;)
              end
              local.get 3
              i32.const 39
              local.get 4
              call_indirect (type 2)
              local.set 2
              br 4 (;@1;)
            end
            local.get 7
            i32.const 1
            local.get 5
            select
            local.set 6
            i32.const 48
            i32.const 87
            local.get 1
            local.get 5
            i32.const 2
            i32.shl
            i32.shr_u
            i32.const 15
            i32.and
            local.tee 2
            i32.const 10
            i32.lt_u
            select
            local.get 2
            i32.add
            local.set 0
            local.get 5
            i32.const -1
            i32.add
            i32.const 0
            local.get 5
            select
            local.set 5
          end
          local.get 1
          local.set 2
        end
        local.get 3
        local.get 0
        local.get 4
        call_indirect (type 2)
        i32.eqz
        br_if 0 (;@2;)
      end
      i32.const 1
      return
    end
    local.get 2)
  (func $core::slice::index::slice_start_index_len_fail_rt::h14fdf9184104ebe9 (type 6) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 8
    i32.add
    i32.const 12
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 32
    i32.add
    i32.const 12
    i32.add
    i32.const 1
    i32.store
    local.get 3
    i32.const 1054604
    i32.store offset=16
    local.get 3
    i32.const 0
    i32.store offset=8
    local.get 3
    i32.const 1
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=40
    local.get 3
    local.get 3
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $core::slice::index::slice_end_index_len_fail_rt::hb4a41039aad0214c (type 6) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 8
    i32.add
    i32.const 12
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 32
    i32.add
    i32.const 12
    i32.add
    i32.const 1
    i32.store
    local.get 3
    i32.const 1054636
    i32.store offset=16
    local.get 3
    i32.const 0
    i32.store offset=8
    local.get 3
    i32.const 1
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=40
    local.get 3
    local.get 3
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $core::slice::index::slice_index_order_fail_rt::h8f57d30e89768042 (type 6) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 8
    i32.add
    i32.const 12
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 32
    i32.add
    i32.const 12
    i32.add
    i32.const 1
    i32.store
    local.get 3
    i32.const 1054688
    i32.store offset=16
    local.get 3
    i32.const 0
    i32.store offset=8
    local.get 3
    i32.const 1
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=40
    local.get 3
    local.get 3
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $core::slice::<impl__T_>::copy_from_slice::len_mismatch_fail::hf6b61e468635f962 (type 6) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 8
    i32.add
    i32.const 12
    i32.add
    i32.const 3
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 32
    i32.add
    i32.const 12
    i32.add
    i32.const 1
    i32.store
    local.get 3
    i32.const 1054768
    i32.store offset=16
    local.get 3
    i32.const 0
    i32.store offset=8
    local.get 3
    i32.const 1
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.store offset=40
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $core::panicking::panic_fmt::h9d972fcdb087ce21
    unreachable)
  (func $core::fmt::num::imp::<impl_core::fmt::Display_for_u8>::fmt::h2ce6ac9345632f4e (type 2) (param i32 i32) (result i32)
    local.get 0
    i64.load8_u
    i32.const 1
    local.get 1
    call $core::fmt::num::imp::fmt_u64::h0e41c13141ae31b8)
  (func $core::fmt::num::<impl_core::fmt::UpperHex_for_i8>::fmt::h7ed37fd7cd13cf31 (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 128
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.load8_u
    local.set 3
    i32.const 0
    local.set 0
    loop  ;; label = @1
      local.get 2
      local.get 0
      i32.add
      i32.const 127
      i32.add
      i32.const 48
      i32.const 55
      local.get 3
      i32.const 15
      i32.and
      local.tee 4
      i32.const 10
      i32.lt_u
      select
      local.get 4
      i32.add
      i32.store8
      local.get 0
      i32.const -1
      i32.add
      local.set 0
      local.get 3
      i32.const 255
      i32.and
      local.tee 4
      i32.const 4
      i32.shr_u
      local.set 3
      local.get 4
      i32.const 15
      i32.gt_u
      br_if 0 (;@1;)
    end
    block  ;; label = @1
      local.get 0
      i32.const 128
      i32.add
      local.tee 3
      i32.const 129
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      i32.const 128
      i32.const 1054200
      call $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf
      unreachable
    end
    local.get 1
    i32.const 1
    i32.const 1054216
    i32.const 2
    local.get 2
    local.get 0
    i32.add
    i32.const 128
    i32.add
    i32.const 0
    local.get 0
    i32.sub
    call $core::fmt::Formatter::pad_integral::hb3903a931cea1b46
    local.set 0
    local.get 2
    i32.const 128
    i32.add
    global.set 0
    local.get 0)
  (func $core::str::slice_error_fail_rt::he2de3b4cdeda08cc (type 7) (param i32 i32 i32 i32 i32)
    (local i32 i32 i32 i32)
    global.get 0
    i32.const 112
    i32.sub
    local.tee 5
    global.set 0
    local.get 5
    local.get 3
    i32.store offset=12
    local.get 5
    local.get 2
    i32.store offset=8
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  i32.const 257
                  i32.lt_u
                  br_if 0 (;@7;)
                  i32.const 0
                  local.set 6
                  loop  ;; label = @8
                    local.get 0
                    local.get 6
                    i32.add
                    local.set 7
                    local.get 6
                    i32.const -1
                    i32.add
                    local.tee 8
                    local.set 6
                    local.get 7
                    i32.const 256
                    i32.add
                    i32.load8_s
                    i32.const -65
                    i32.le_s
                    br_if 0 (;@8;)
                  end
                  local.get 8
                  i32.const 257
                  i32.add
                  local.tee 6
                  local.get 1
                  i32.lt_u
                  br_if 2 (;@5;)
                  local.get 1
                  i32.const -257
                  i32.add
                  local.get 8
                  i32.ne
                  br_if 4 (;@3;)
                  local.get 5
                  local.get 6
                  i32.store offset=20
                  br 1 (;@6;)
                end
                local.get 5
                local.get 1
                i32.store offset=20
              end
              local.get 5
              local.get 0
              i32.store offset=16
              i32.const 0
              local.set 6
              i32.const 1053824
              local.set 7
              br 1 (;@4;)
            end
            local.get 0
            local.get 8
            i32.add
            i32.const 257
            i32.add
            i32.load8_s
            i32.const -65
            i32.le_s
            br_if 1 (;@3;)
            local.get 5
            local.get 6
            i32.store offset=20
            local.get 5
            local.get 0
            i32.store offset=16
            i32.const 5
            local.set 6
            i32.const 1054792
            local.set 7
          end
          local.get 5
          local.get 6
          i32.store offset=28
          local.get 5
          local.get 7
          i32.store offset=24
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              local.get 1
              i32.gt_u
              local.tee 6
              br_if 0 (;@5;)
              local.get 3
              local.get 1
              i32.gt_u
              br_if 0 (;@5;)
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 2
                      local.get 3
                      i32.gt_u
                      br_if 0 (;@9;)
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 2
                          i32.eqz
                          br_if 0 (;@11;)
                          block  ;; label = @12
                            local.get 2
                            local.get 1
                            i32.lt_u
                            br_if 0 (;@12;)
                            local.get 2
                            local.get 1
                            i32.eq
                            br_if 1 (;@11;)
                            br 2 (;@10;)
                          end
                          local.get 0
                          local.get 2
                          i32.add
                          i32.load8_s
                          i32.const -64
                          i32.lt_s
                          br_if 1 (;@10;)
                        end
                        local.get 3
                        local.set 2
                      end
                      local.get 5
                      local.get 2
                      i32.store offset=32
                      local.get 1
                      local.set 6
                      block  ;; label = @10
                        local.get 2
                        local.get 1
                        i32.ge_u
                        br_if 0 (;@10;)
                        local.get 2
                        i32.const 1
                        i32.add
                        local.tee 6
                        i32.const 0
                        local.get 2
                        i32.const -3
                        i32.add
                        local.tee 7
                        local.get 7
                        local.get 2
                        i32.gt_u
                        select
                        local.tee 7
                        i32.lt_u
                        br_if 6 (;@4;)
                        local.get 0
                        local.get 6
                        i32.add
                        local.get 0
                        local.get 7
                        i32.add
                        i32.sub
                        local.set 6
                        loop  ;; label = @11
                          local.get 6
                          i32.const -1
                          i32.add
                          local.set 6
                          local.get 0
                          local.get 2
                          i32.add
                          local.set 7
                          local.get 2
                          i32.const -1
                          i32.add
                          local.tee 8
                          local.set 2
                          local.get 7
                          i32.load8_s
                          i32.const -64
                          i32.lt_s
                          br_if 0 (;@11;)
                        end
                        local.get 8
                        i32.const 1
                        i32.add
                        local.set 6
                      end
                      block  ;; label = @10
                        local.get 6
                        i32.eqz
                        br_if 0 (;@10;)
                        block  ;; label = @11
                          local.get 6
                          local.get 1
                          i32.lt_u
                          br_if 0 (;@11;)
                          local.get 6
                          local.get 1
                          i32.eq
                          br_if 1 (;@10;)
                          br 10 (;@1;)
                        end
                        local.get 0
                        local.get 6
                        i32.add
                        i32.load8_s
                        i32.const -65
                        i32.le_s
                        br_if 9 (;@1;)
                      end
                      local.get 6
                      local.get 1
                      i32.eq
                      br_if 7 (;@2;)
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 0
                          local.get 6
                          i32.add
                          local.tee 0
                          i32.load8_s
                          local.tee 7
                          i32.const -1
                          i32.gt_s
                          br_if 0 (;@11;)
                          local.get 0
                          i32.load8_u offset=1
                          i32.const 63
                          i32.and
                          local.set 2
                          local.get 7
                          i32.const 31
                          i32.and
                          local.set 8
                          local.get 7
                          i32.const -33
                          i32.gt_u
                          br_if 1 (;@10;)
                          local.get 8
                          i32.const 6
                          i32.shl
                          local.get 2
                          i32.or
                          local.set 0
                          br 4 (;@7;)
                        end
                        local.get 5
                        local.get 7
                        i32.const 255
                        i32.and
                        i32.store offset=36
                        i32.const 1
                        local.set 7
                        br 4 (;@6;)
                      end
                      local.get 2
                      i32.const 6
                      i32.shl
                      local.get 0
                      i32.load8_u offset=2
                      i32.const 63
                      i32.and
                      i32.or
                      local.set 2
                      local.get 7
                      i32.const -16
                      i32.ge_u
                      br_if 1 (;@8;)
                      local.get 2
                      local.get 8
                      i32.const 12
                      i32.shl
                      i32.or
                      local.set 0
                      br 2 (;@7;)
                    end
                    local.get 5
                    i32.const 100
                    i32.add
                    i32.const 50
                    i32.store
                    local.get 5
                    i32.const 72
                    i32.add
                    i32.const 20
                    i32.add
                    i32.const 50
                    i32.store
                    local.get 5
                    i32.const 72
                    i32.add
                    i32.const 12
                    i32.add
                    i32.const 1
                    i32.store
                    local.get 5
                    i32.const 48
                    i32.add
                    i32.const 12
                    i32.add
                    i32.const 4
                    i32.store
                    local.get 5
                    i32.const 48
                    i32.add
                    i32.const 20
                    i32.add
                    i32.const 4
                    i32.store
                    local.get 5
                    i32.const 1054892
                    i32.store offset=56
                    local.get 5
                    i32.const 0
                    i32.store offset=48
                    local.get 5
                    i32.const 1
                    i32.store offset=76
                    local.get 5
                    local.get 5
                    i32.const 72
                    i32.add
                    i32.store offset=64
                    local.get 5
                    local.get 5
                    i32.const 24
                    i32.add
                    i32.store offset=96
                    local.get 5
                    local.get 5
                    i32.const 16
                    i32.add
                    i32.store offset=88
                    local.get 5
                    local.get 5
                    i32.const 12
                    i32.add
                    i32.store offset=80
                    local.get 5
                    local.get 5
                    i32.const 8
                    i32.add
                    i32.store offset=72
                    local.get 5
                    i32.const 48
                    i32.add
                    local.get 4
                    call $core::panicking::panic_fmt::h9d972fcdb087ce21
                    unreachable
                  end
                  local.get 2
                  i32.const 6
                  i32.shl
                  local.get 0
                  i32.load8_u offset=3
                  i32.const 63
                  i32.and
                  i32.or
                  local.get 8
                  i32.const 18
                  i32.shl
                  i32.const 1835008
                  i32.and
                  i32.or
                  local.tee 0
                  i32.const 1114112
                  i32.eq
                  br_if 5 (;@2;)
                end
                local.get 5
                local.get 0
                i32.store offset=36
                i32.const 1
                local.set 7
                local.get 0
                i32.const 128
                i32.lt_u
                br_if 0 (;@6;)
                i32.const 2
                local.set 7
                local.get 0
                i32.const 2048
                i32.lt_u
                br_if 0 (;@6;)
                i32.const 3
                i32.const 4
                local.get 0
                i32.const 65536
                i32.lt_u
                select
                local.set 7
              end
              local.get 5
              local.get 6
              i32.store offset=40
              local.get 5
              local.get 7
              local.get 6
              i32.add
              i32.store offset=44
              local.get 5
              i32.const 48
              i32.add
              i32.const 12
              i32.add
              i32.const 5
              i32.store
              local.get 5
              i32.const 48
              i32.add
              i32.const 20
              i32.add
              i32.const 5
              i32.store
              local.get 5
              i32.const 108
              i32.add
              i32.const 50
              i32.store
              local.get 5
              i32.const 100
              i32.add
              i32.const 50
              i32.store
              local.get 5
              i32.const 72
              i32.add
              i32.const 20
              i32.add
              i32.const 52
              i32.store
              local.get 5
              i32.const 72
              i32.add
              i32.const 12
              i32.add
              i32.const 53
              i32.store
              local.get 5
              i32.const 1054976
              i32.store offset=56
              local.get 5
              i32.const 0
              i32.store offset=48
              local.get 5
              i32.const 1
              i32.store offset=76
              local.get 5
              local.get 5
              i32.const 72
              i32.add
              i32.store offset=64
              local.get 5
              local.get 5
              i32.const 24
              i32.add
              i32.store offset=104
              local.get 5
              local.get 5
              i32.const 16
              i32.add
              i32.store offset=96
              local.get 5
              local.get 5
              i32.const 40
              i32.add
              i32.store offset=88
              local.get 5
              local.get 5
              i32.const 36
              i32.add
              i32.store offset=80
              local.get 5
              local.get 5
              i32.const 32
              i32.add
              i32.store offset=72
              local.get 5
              i32.const 48
              i32.add
              local.get 4
              call $core::panicking::panic_fmt::h9d972fcdb087ce21
              unreachable
            end
            local.get 5
            local.get 2
            local.get 3
            local.get 6
            select
            i32.store offset=40
            local.get 5
            i32.const 48
            i32.add
            i32.const 12
            i32.add
            i32.const 3
            i32.store
            local.get 5
            i32.const 48
            i32.add
            i32.const 20
            i32.add
            i32.const 3
            i32.store
            local.get 5
            i32.const 72
            i32.add
            i32.const 20
            i32.add
            i32.const 50
            i32.store
            local.get 5
            i32.const 72
            i32.add
            i32.const 12
            i32.add
            i32.const 50
            i32.store
            local.get 5
            i32.const 1054832
            i32.store offset=56
            local.get 5
            i32.const 0
            i32.store offset=48
            local.get 5
            i32.const 1
            i32.store offset=76
            local.get 5
            local.get 5
            i32.const 72
            i32.add
            i32.store offset=64
            local.get 5
            local.get 5
            i32.const 24
            i32.add
            i32.store offset=88
            local.get 5
            local.get 5
            i32.const 16
            i32.add
            i32.store offset=80
            local.get 5
            local.get 5
            i32.const 40
            i32.add
            i32.store offset=72
            local.get 5
            i32.const 48
            i32.add
            local.get 4
            call $core::panicking::panic_fmt::h9d972fcdb087ce21
            unreachable
          end
          local.get 7
          local.get 6
          i32.const 1055044
          call $core::slice::index::slice_index_order_fail::h2a2e64aa62065ee1
          unreachable
        end
        local.get 0
        local.get 1
        i32.const 0
        local.get 6
        local.get 4
        call $core::str::slice_error_fail::heda988cb317c5e8e
        unreachable
      end
      i32.const 1053824
      i32.const 43
      local.get 4
      call $core::panicking::panic::h364c37174a08a6a4
      unreachable
    end
    local.get 0
    local.get 1
    local.get 6
    local.get 1
    local.get 4
    call $core::str::slice_error_fail::heda988cb317c5e8e
    unreachable)
  (func $core::unicode::printable::check::hf41dd63b2bbe64a8 (type 17) (param i32 i32 i32 i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    i32.const 1
    local.set 7
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        local.get 2
        i32.const 1
        i32.shl
        i32.add
        local.set 8
        local.get 0
        i32.const 65280
        i32.and
        i32.const 8
        i32.shr_u
        local.set 9
        i32.const 0
        local.set 10
        local.get 0
        i32.const 255
        i32.and
        local.set 11
        loop  ;; label = @3
          local.get 1
          i32.const 2
          i32.add
          local.set 12
          local.get 10
          local.get 1
          i32.load8_u offset=1
          local.tee 2
          i32.add
          local.set 13
          block  ;; label = @4
            local.get 1
            i32.load8_u
            local.tee 1
            local.get 9
            i32.eq
            br_if 0 (;@4;)
            local.get 1
            local.get 9
            i32.gt_u
            br_if 2 (;@2;)
            local.get 13
            local.set 10
            local.get 12
            local.set 1
            local.get 12
            local.get 8
            i32.eq
            br_if 2 (;@2;)
            br 1 (;@3;)
          end
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 13
                local.get 10
                i32.lt_u
                br_if 0 (;@6;)
                local.get 13
                local.get 4
                i32.gt_u
                br_if 1 (;@5;)
                local.get 3
                local.get 10
                i32.add
                local.set 1
                loop  ;; label = @7
                  local.get 2
                  i32.eqz
                  br_if 3 (;@4;)
                  local.get 2
                  i32.const -1
                  i32.add
                  local.set 2
                  local.get 1
                  i32.load8_u
                  local.set 10
                  local.get 1
                  i32.const 1
                  i32.add
                  local.set 1
                  local.get 10
                  local.get 11
                  i32.ne
                  br_if 0 (;@7;)
                end
                i32.const 0
                local.set 7
                br 5 (;@1;)
              end
              local.get 10
              local.get 13
              i32.const 1055100
              call $core::slice::index::slice_index_order_fail::h2a2e64aa62065ee1
              unreachable
            end
            local.get 13
            local.get 4
            i32.const 1055100
            call $core::slice::index::slice_end_index_len_fail::h06e641a35ef26e6c
            unreachable
          end
          local.get 13
          local.set 10
          local.get 12
          local.set 1
          local.get 12
          local.get 8
          i32.ne
          br_if 0 (;@3;)
        end
      end
      local.get 6
      i32.eqz
      br_if 0 (;@1;)
      local.get 5
      local.get 6
      i32.add
      local.set 11
      local.get 0
      i32.const 65535
      i32.and
      local.set 1
      i32.const 1
      local.set 7
      block  ;; label = @2
        loop  ;; label = @3
          local.get 5
          i32.const 1
          i32.add
          local.set 10
          block  ;; label = @4
            block  ;; label = @5
              local.get 5
              i32.load8_u
              local.tee 2
              i32.const 24
              i32.shl
              i32.const 24
              i32.shr_s
              local.tee 13
              i32.const 0
              i32.lt_s
              br_if 0 (;@5;)
              local.get 10
              local.set 5
              br 1 (;@4;)
            end
            local.get 10
            local.get 11
            i32.eq
            br_if 2 (;@2;)
            local.get 13
            i32.const 127
            i32.and
            i32.const 8
            i32.shl
            local.get 5
            i32.load8_u offset=1
            i32.or
            local.set 2
            local.get 5
            i32.const 2
            i32.add
            local.set 5
          end
          local.get 1
          local.get 2
          i32.sub
          local.tee 1
          i32.const 0
          i32.lt_s
          br_if 2 (;@1;)
          local.get 7
          i32.const 1
          i32.xor
          local.set 7
          local.get 5
          local.get 11
          i32.ne
          br_if 0 (;@3;)
          br 2 (;@1;)
        end
      end
      i32.const 1053824
      i32.const 43
      i32.const 1055116
      call $core::panicking::panic::h364c37174a08a6a4
      unreachable
    end
    local.get 7
    i32.const 1
    i32.and)
  (func $core::fmt::num::<impl_core::fmt::LowerHex_for_i32>::fmt::hcca31281b8baf66e (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 128
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.load
    local.set 0
    i32.const 0
    local.set 3
    loop  ;; label = @1
      local.get 2
      local.get 3
      i32.add
      i32.const 127
      i32.add
      i32.const 48
      i32.const 87
      local.get 0
      i32.const 15
      i32.and
      local.tee 4
      i32.const 10
      i32.lt_u
      select
      local.get 4
      i32.add
      i32.store8
      local.get 3
      i32.const -1
      i32.add
      local.set 3
      local.get 0
      i32.const 15
      i32.gt_u
      local.set 4
      local.get 0
      i32.const 4
      i32.shr_u
      local.set 0
      local.get 4
      br_if 0 (;@1;)
    end
    block  ;; label = @1
      local.get 3
      i32.const 128
      i32.add
      local.tee 0
      i32.const 129
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 128
      i32.const 1054200
      call $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf
      unreachable
    end
    local.get 1
    i32.const 1
    i32.const 1054216
    i32.const 2
    local.get 2
    local.get 3
    i32.add
    i32.const 128
    i32.add
    i32.const 0
    local.get 3
    i32.sub
    call $core::fmt::Formatter::pad_integral::hb3903a931cea1b46
    local.set 0
    local.get 2
    i32.const 128
    i32.add
    global.set 0
    local.get 0)
  (func $core::fmt::num::<impl_core::fmt::LowerHex_for_i8>::fmt::h770642123b33a41b (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 128
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.load8_u
    local.set 3
    i32.const 0
    local.set 0
    loop  ;; label = @1
      local.get 2
      local.get 0
      i32.add
      i32.const 127
      i32.add
      i32.const 48
      i32.const 87
      local.get 3
      i32.const 15
      i32.and
      local.tee 4
      i32.const 10
      i32.lt_u
      select
      local.get 4
      i32.add
      i32.store8
      local.get 0
      i32.const -1
      i32.add
      local.set 0
      local.get 3
      i32.const 255
      i32.and
      local.tee 4
      i32.const 4
      i32.shr_u
      local.set 3
      local.get 4
      i32.const 15
      i32.gt_u
      br_if 0 (;@1;)
    end
    block  ;; label = @1
      local.get 0
      i32.const 128
      i32.add
      local.tee 3
      i32.const 129
      i32.lt_u
      br_if 0 (;@1;)
      local.get 3
      i32.const 128
      i32.const 1054200
      call $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf
      unreachable
    end
    local.get 1
    i32.const 1
    i32.const 1054216
    i32.const 2
    local.get 2
    local.get 0
    i32.add
    i32.const 128
    i32.add
    i32.const 0
    local.get 0
    i32.sub
    call $core::fmt::Formatter::pad_integral::hb3903a931cea1b46
    local.set 0
    local.get 2
    i32.const 128
    i32.add
    global.set 0
    local.get 0)
  (func $core::fmt::num::imp::fmt_u64::h0e41c13141ae31b8 (type 18) (param i64 i32 i32) (result i32)
    (local i32 i32 i64 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    i32.const 39
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i64.const 10000
        i64.ge_u
        br_if 0 (;@2;)
        local.get 0
        local.set 5
        br 1 (;@1;)
      end
      i32.const 39
      local.set 4
      loop  ;; label = @2
        local.get 3
        i32.const 9
        i32.add
        local.get 4
        i32.add
        local.tee 6
        i32.const -4
        i32.add
        local.get 0
        local.get 0
        i64.const 10000
        i64.div_u
        local.tee 5
        i64.const 10000
        i64.mul
        i64.sub
        i32.wrap_i64
        local.tee 7
        i32.const 65535
        i32.and
        i32.const 100
        i32.div_u
        local.tee 8
        i32.const 1
        i32.shl
        i32.const 1054218
        i32.add
        i32.load16_u align=1
        i32.store16 align=1
        local.get 6
        i32.const -2
        i32.add
        local.get 7
        local.get 8
        i32.const 100
        i32.mul
        i32.sub
        i32.const 65535
        i32.and
        i32.const 1
        i32.shl
        i32.const 1054218
        i32.add
        i32.load16_u align=1
        i32.store16 align=1
        local.get 4
        i32.const -4
        i32.add
        local.set 4
        local.get 0
        i64.const 99999999
        i64.gt_u
        local.set 6
        local.get 5
        local.set 0
        local.get 6
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      local.get 5
      i32.wrap_i64
      local.tee 6
      i32.const 99
      i32.le_u
      br_if 0 (;@1;)
      local.get 3
      i32.const 9
      i32.add
      local.get 4
      i32.const -2
      i32.add
      local.tee 4
      i32.add
      local.get 5
      i32.wrap_i64
      local.tee 6
      local.get 6
      i32.const 65535
      i32.and
      i32.const 100
      i32.div_u
      local.tee 6
      i32.const 100
      i32.mul
      i32.sub
      i32.const 65535
      i32.and
      i32.const 1
      i32.shl
      i32.const 1054218
      i32.add
      i32.load16_u align=1
      i32.store16 align=1
    end
    block  ;; label = @1
      block  ;; label = @2
        local.get 6
        i32.const 10
        i32.lt_u
        br_if 0 (;@2;)
        local.get 3
        i32.const 9
        i32.add
        local.get 4
        i32.const -2
        i32.add
        local.tee 4
        i32.add
        local.get 6
        i32.const 1
        i32.shl
        i32.const 1054218
        i32.add
        i32.load16_u align=1
        i32.store16 align=1
        br 1 (;@1;)
      end
      local.get 3
      i32.const 9
      i32.add
      local.get 4
      i32.const -1
      i32.add
      local.tee 4
      i32.add
      local.get 6
      i32.const 48
      i32.add
      i32.store8
    end
    local.get 2
    local.get 1
    i32.const 1053824
    i32.const 0
    local.get 3
    i32.const 9
    i32.add
    local.get 4
    i32.add
    i32.const 39
    local.get 4
    i32.sub
    call $core::fmt::Formatter::pad_integral::hb3903a931cea1b46
    local.set 4
    local.get 3
    i32.const 48
    i32.add
    global.set 0
    local.get 4)
  (func $core::fmt::num::<impl_core::fmt::UpperHex_for_i32>::fmt::h2165261741388ccd (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 128
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.load
    local.set 0
    i32.const 0
    local.set 3
    loop  ;; label = @1
      local.get 2
      local.get 3
      i32.add
      i32.const 127
      i32.add
      i32.const 48
      i32.const 55
      local.get 0
      i32.const 15
      i32.and
      local.tee 4
      i32.const 10
      i32.lt_u
      select
      local.get 4
      i32.add
      i32.store8
      local.get 3
      i32.const -1
      i32.add
      local.set 3
      local.get 0
      i32.const 15
      i32.gt_u
      local.set 4
      local.get 0
      i32.const 4
      i32.shr_u
      local.set 0
      local.get 4
      br_if 0 (;@1;)
    end
    block  ;; label = @1
      local.get 3
      i32.const 128
      i32.add
      local.tee 0
      i32.const 129
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 128
      i32.const 1054200
      call $core::slice::index::slice_start_index_len_fail::h017efd9f56dcabdf
      unreachable
    end
    local.get 1
    i32.const 1
    i32.const 1054216
    i32.const 2
    local.get 2
    local.get 3
    i32.add
    i32.const 128
    i32.add
    i32.const 0
    local.get 3
    i32.sub
    call $core::fmt::Formatter::pad_integral::hb3903a931cea1b46
    local.set 0
    local.get 2
    i32.const 128
    i32.add
    global.set 0
    local.get 0)
  (func $core::fmt::num::imp::<impl_core::fmt::Display_for_i32>::fmt::hac307c85eb8fe9bf (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.tee 0
    i64.extend_i32_u
    i64.const 0
    local.get 0
    i64.extend_i32_s
    i64.sub
    local.get 0
    i32.const -1
    i32.gt_s
    local.tee 0
    select
    local.get 0
    local.get 1
    call $core::fmt::num::imp::fmt_u64::h0e41c13141ae31b8)
  (func $core::fmt::num::imp::<impl_core::fmt::Display_for_i64>::fmt::h73c3d24a1f291db5 (type 2) (param i32 i32) (result i32)
    (local i64 i64)
    local.get 0
    i64.load
    local.tee 2
    local.get 2
    i64.const 63
    i64.shr_s
    local.tee 3
    i64.xor
    local.get 3
    i64.sub
    local.get 2
    i64.const -1
    i64.gt_s
    local.get 1
    call $core::fmt::num::imp::fmt_u64::h0e41c13141ae31b8)
  (func $<core::fmt::Error_as_core::fmt::Debug>::fmt::h11523cd722b21e74 (type 2) (param i32 i32) (result i32)
    local.get 1
    i32.load
    i32.const 1056576
    i32.const 5
    local.get 1
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 4))
  (func $compiler_builtins::mem::memcmp::h8c8560f9c41f9e07 (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32)
    i32.const 0
    local.set 3
    block  ;; label = @1
      local.get 2
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        loop  ;; label = @3
          local.get 0
          i32.load8_u
          local.tee 4
          local.get 1
          i32.load8_u
          local.tee 5
          i32.ne
          br_if 1 (;@2;)
          local.get 0
          i32.const 1
          i32.add
          local.set 0
          local.get 1
          i32.const 1
          i32.add
          local.set 1
          local.get 2
          i32.const -1
          i32.add
          local.tee 2
          i32.eqz
          br_if 2 (;@1;)
          br 0 (;@3;)
        end
      end
      local.get 4
      local.get 5
      i32.sub
      local.set 3
    end
    local.get 3)
  (func $memcmp (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    local.get 1
    local.get 2
    call $compiler_builtins::mem::memcmp::h8c8560f9c41f9e07)
  (table (;0;) 62 62 funcref)
  (memory (;0;) 17)
  (global (;0;) (mut i32) (i32.const 1048576))
  (global (;1;) i32 (i32.const 1048684))
  (global (;2;) i32 (i32.const 1048684))
  (global (;3;) i32 (i32.const 1057944))
  (global (;4;) i32 (i32.const 1057952))
  (export "memory" (memory 0))
  (export "init" (func $init))
  (export "zk_on_secret_input_40" (func $zk_on_secret_input_40))
  (export "__PBC_VERSION_CLIENT_5_0_0" (global 1))
  (export "action_01" (func $action_01))
  (export "zk_on_compute_complete" (func $zk_on_compute_complete))
  (export "zk_on_variables_opened" (func $zk_on_variables_opened))
  (export "zk_on_attestation_complete" (func $zk_on_attestation_complete))
  (export "__PBC_VERSION_BINDER_4_0_0" (global 2))
  (export "wasm_exit" (func $wasm_exit))
  (export "__data_end" (global 3))
  (export "__heap_base" (global 4))
  (elem (;0;) (i32.const 1) func $core::fmt::num::imp::<impl_core::fmt::Display_for_u32>::fmt::h8b2af792c13f3dfc $core::fmt::num::imp::<impl_core::fmt::Display_for_i64>::fmt::h73c3d24a1f291db5 $<pbc_contract_common::address_internal::Address_as_core::fmt::Debug>::fmt::h9bea144fc5b613ad $<pbc_contract_common::zk::CalculationStatus_as_core::fmt::Debug>::fmt::hff1dc751f644adc3 $core::fmt::num::imp::<impl_core::fmt::Display_for_u8>::fmt::h2ce6ac9345632f4e $core::ptr::drop_in_place<std::io::error::Error>::hda585276532b6f31 $<std::io::error::Error_as_core::fmt::Debug>::fmt::hf93b1043f036c001 $core::fmt::num::<impl_core::fmt::LowerHex_for_i8>::fmt::h770642123b33a41b $core::ptr::drop_in_place<&usize>::h740950a52e001245 $<&T_as_core::fmt::Debug>::fmt::hafe90a216e33631b $<&T_as_core::fmt::Debug>::fmt::h01863fc6dc583618 $<&T_as_core::fmt::Display>::fmt::hf14af99385a76015 $<alloc::string::String_as_core::fmt::Display>::fmt::h3e52ce3ad8d356c1 $core::ptr::drop_in_place<pbc_lib::exit::override_panic::__closure__>::h021e10777ad761e0 $core::ops::function::FnOnce::call_once__vtable.shim__::h470ded2ab9fe471e $pbc_lib::exit::override_panic::__closure__::h16eb93735f5177dd $core::ptr::drop_in_place<std::io::error::Error>::h9307e7ae4e21f669 $core::ptr::drop_in_place<&_u8__20_>::h9faf78432464bb3e $<&T_as_core::fmt::Debug>::fmt::h24cfd342c4663119 $<&T_as_core::fmt::Debug>::fmt::h51188a6b196986bf $core::ptr::drop_in_place<&u8>::hf8f42853aa9727f1 $<&T_as_core::fmt::Debug>::fmt::h4efa132c19f330b8 $std::alloc::default_alloc_error_hook::h01841b0b6d4df8eb $core::ptr::drop_in_place<&mut_std::io::Write::write_fmt::Adapter<alloc::vec::Vec<u8>>>::he1857990d75d318b $<&mut_W_as_core::fmt::Write>::write_str::ha30ded41f6d9416e $<&mut_W_as_core::fmt::Write>::write_char::ha9236e4544a32b1b $<&mut_W_as_core::fmt::Write>::write_fmt::h0ba0eb71505b98bf $<std::io::error::ErrorKind_as_core::fmt::Debug>::fmt::h9f08f3a56148ea47 $<&T_as_core::fmt::Debug>::fmt::h99a333b24ab754d0 $core::fmt::num::<impl_core::fmt::Debug_for_i32>::fmt::hf65dabd27e3ccd74 $core::ptr::drop_in_place<alloc::string::String>::h159fe17ec9cfec50 $<alloc::string::String_as_core::fmt::Debug>::fmt::h750f0b4a8b532594 $<T_as_core::any::Any>::type_id::ha426e9c360ec53b5 $<T_as_core::any::Any>::type_id::h91d619ab643bea16 $core::ptr::drop_in_place<std::panicking::begin_panic_handler::PanicPayload>::h8fce5688cee52563 $<std::panicking::begin_panic_handler::PanicPayload_as_core::panic::BoxMeUp>::take_box::hbe454e3d7aec7cab $<std::panicking::begin_panic_handler::PanicPayload_as_core::panic::BoxMeUp>::get::h72f881a6fbed83d0 $<std::panicking::begin_panic_handler::StrPanicPayload_as_core::panic::BoxMeUp>::take_box::h606d68c05a7922b1 $<std::panicking::begin_panic_handler::StrPanicPayload_as_core::panic::BoxMeUp>::get::hade4f92ab6640ba2 $<T_as_core::any::Any>::type_id::h12342e39968a4047 $<&T_as_core::fmt::Debug>::fmt::hcbeb5a74998e7fbc $<&T_as_core::fmt::Debug>::fmt::h0228b32a2cf5ef52 $core::ptr::drop_in_place<&u8>::hff2bf1f19be4017e $<&mut_W_as_core::fmt::Write>::write_str::h6bdcd609f50a704a $<&mut_W_as_core::fmt::Write>::write_char::h3033d36aeb6e7159 $<&mut_W_as_core::fmt::Write>::write_fmt::h40b3918c42512df5 $<core::fmt::Error_as_core::fmt::Debug>::fmt::h11523cd722b21e74 $core::ops::function::FnOnce::call_once::h0502d0d495ff7cd0 $<&T_as_core::fmt::Debug>::fmt::h2b7d9e319f3644e6 $<&T_as_core::fmt::Display>::fmt::h51e2cb33c6de9b00 $<core::fmt::Arguments_as_core::fmt::Display>::fmt::h94ab3c4e57d83582 $<core::ops::range::Range<Idx>_as_core::fmt::Debug>::fmt::hdf186d101772be86 $<char_as_core::fmt::Debug>::fmt::hf7e974522a789b5c $core::ptr::drop_in_place<&core::iter::adapters::copied::Copied<core::slice::iter::Iter<u8>>>::hecbac7fec775a8ae $<T_as_core::any::Any>::type_id::he0c39bc4489521d2 $<core::fmt::builders::PadAdapter_as_core::fmt::Write>::write_str::h5c98716d5a71cc25 $core::fmt::Write::write_char::h37130539227c56c9 $core::fmt::Write::write_fmt::h31e9ffb45358f2e7 $<&mut_W_as_core::fmt::Write>::write_str::h299ec16a7091eca3 $<&mut_W_as_core::fmt::Write>::write_char::h150313a070e73827 $<&mut_W_as_core::fmt::Write>::write_fmt::h4c061b74e85ac905)
  (data (;0;) (i32.const 1048576) "\00called `Option::unwrap()` on a `None` valuesrc/contract.rs\00,\00\10\00\0f\00\00\006\01\00\00?\00\00\00,\00\10\00\0f\00\00\008\01\00\007\00\00\00,\00\10\00\0f\00\00\008\01\00\00\0c\00\00\00Input data too long;  bytes remaining\00\00\00l\00\10\00\15\00\00\00\81\00\10\00\10\00\00\00,\00\10\00\0f\00\00\00~\00\00\00\01\00\00\00,\00\10\00\0f\00\00\00\97\00\00\00\01\00\00\00Not allowed to vote after the deadline at  ms UTC, current time is  ms UTC\00\00\c4\00\10\00*\00\00\00\ee\00\10\00\19\00\00\00\07\01\10\00\07\00\00\00,\00\10\00\0f\00\00\00\a1\00\00\00\05\00\00\00Only voters can send votes.\008\01\10\00\1b\00\00\00,\00\10\00\0f\00\00\00\a7\00\00\00\05\00\00\00Each voter is only allowed to send one vote variable. Sender: \00\00l\01\10\00>\00\00\00,\00\10\00\0f\00\00\00\ab\00\00\00\05\00\00\00,\00\10\00\0f\00\00\00\c3\00\00\00\01\00\00\00Vote counting cannot start before specified starting time \00\00\d4\01\10\00:\00\00\00\ee\00\10\00\19\00\00\00\07\01\10\00\07\00\00\00,\00\10\00\0f\00\00\00\c9\00\00\00\05\00\00\00Vote counting must start from Waiting state, but was \00\00\008\02\10\005\00\00\00,\00\10\00\0f\00\00\00\cf\00\00\00\05\00\00\00,\00\10\00\0f\00\00\00\e2\00\00\00\01\00\00\00,\00\10\00\0f\00\00\00\f5\00\00\00\01\00\00\00\01\00\00\00Unexpected number of output variables\00\00\00\ac\02\10\00%\00\00\00,\00\10\00\0f\00\00\00\fc\00\00\00\05\00\00\00,\00\10\00\0f\00\00\00&\01\00\00\01\00\00\00No known enum value with discriminant \00\00\fc\02\10\00&\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_contract_common\5csrc\5czk\5cmod.rs\00,\03\10\00k\00\00\00_\00\00\00 \00\00\00No known enum value with discriminant \00\00\a8\03\10\00&\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_contract_common\5csrc\5caddress_internal.rs\00\00\00\d8\03\10\00u\00\00\00\1f\00\00\00N\00\00\00\d8\03\10\00u\00\00\00\1f\00\00\00>\00\00\00/rustc/fc594f15669680fa70d255faec3ca3fb507c3405/library/core/src/slice/mod.rs\00\00\00p\04\10\00M\00\00\00\81\0d\00\00\19\00\00\00attempt to divide by zero\00\00\00p\04\10\00M\00\00\00\85\0d\00\00\16\00\00\00called `Result::unwrap()` on an `Err` value\00\06\00\00\00\08\00\00\00\04\00\00\00\07\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_traits\5csrc\5creadwrite_state\5cimpl_misc.rs\00\00\008\05\10\00u\00\00\00\af\00\00\00\09\00\00\00failed to fill whole buffer\00\c0\05\10\00\1b\00\00\00%\00\00\00assertion failed: 0 < self.denominatorsrc\5cfraction.rs\00\00\00\0e\06\10\00\0f\00\00\00\13\00\00\00\09\00\00\00assertion failed: self.numerator <= self.denominator\0e\06\10\00\0f\00\00\00\14\00\00\00\09\00\00\00failed to fill whole buffer\00t\06\10\00\1b\00\00\00%\00\00\00Encountered end of stream while reading i64\00\06\00\00\00\08\00\00\00\04\00\00\00\07\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_traits\5csrc\5cread_int.rs\d8\06\10\00d\00\00\00H\00\00\00\05\00\00\00\d8\06\10\00d\00\00\00T\00\00\00\05\00\00\00Encountered end of stream while reading u32\00\d8\06\10\00d\00\00\00J\00\00\00\05\00\00\00\d8\06\10\00d\00\00\00V\00\00\00\05\00\00\00Encountered end of stream while reading u8\00\00\d8\06\10\00d\00\00\00\5c\00\00\00\05\00\00\00called `Result::unwrap()` on an `Err` value\00\06\00\00\00\08\00\00\00\04\00\00\00\07\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_contract_common\5csrc\5cresult_buffer.rs\00\00 \08\10\00r\00\00\00h\00\00\00\09\00\00\00Duplicated or incorrectly ordered sections. Tried to write section with id 0x, but expected section id of at least 0x\00\00\00\a4\08\10\00M\00\00\00\f1\08\10\00(\00\00\00\00\00\00\00\02\00\00\00\00\00\00\00\00\00\00\00\02\00\00\00\08\00\00\00 \00\00\00\03\00\00\00\01\00\00\00\02\00\00\00\00\00\00\00\00\00\00\00\02\00\00\00\08\00\00\00 \00\00\00\03\00\00\00 \08\10\00r\00\00\00O\00\00\00\09\00\00\00 \08\10\00r\00\00\00\9c\00\00\00\09\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_traits\5csrc\5creadwrite_state\5cimpl_vec.rscalled `Result::unwrap()` on an `Err` value\00\06\00\00\00\08\00\00\00\04\00\00\00\07\00\00\00\8c\09\10\00t\00\00\00b\00\00\00\0d\00\00\00failed to fill whole buffer\00L\0a\10\00\1b\00\00\00%\00\00\00/rustc/fc594f15669680fa70d255faec3ca3fb507c3405/library/core/src/slice/mod.rs\00\00\00t\0a\10\00M\00\00\00\81\0d\00\00\19\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00attempt to divide by zero\00\00\00t\0a\10\00M\00\00\00\85\0d\00\00\16\00\00\00\09\00\00\00\04\00\00\00\04\00\00\00\0a\00\00\00\09\00\00\00\04\00\00\00\04\00\00\00\0b\00\00\00src/contract.rsNo known enum value with discriminant \00\00\00;\0b\10\00&\00\00\00,\0b\10\00\0f\00\00\00,\00\00\00\0a\00\00\00failed to fill whole buffer\00|\0b\10\00\1b\00\00\00%\00\00\00called `Result::unwrap()` on an `Err` value\00\06\00\00\00\08\00\00\00\04\00\00\00\07\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_traits\5csrc\5creadwrite_rpc.rs\00\00\00\e0\0b\10\00i\00\00\00\ac\00\00\00\09\00\00\00\0e\00\00\00\00\00\00\00\01\00\00\00\0f\00\00\00\10\00\00\00\10\00\00\00unknown location:\00\00\00\5c\0c\10\00\00\00\00\00\84\0c\10\00\01\00\00\00: \00\00\5c\0c\10\00\00\00\00\00\98\0c\10\00\02\00\00\00called `Result::unwrap()` on an `Err` value\00\11\00\00\00\08\00\00\00\04\00\00\00\07\00\00\00C:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_traits\5csrc\5creadwrite_rpc.rs\00\00\00\e8\0c\10\00i\00\00\00?\00\00\00\0d\00\00\00DoneMaliciousBehaviourOutputCalculatingWaitingC:\5cUsers\5cSpare\5c.cargo\5cgit\5ccheckouts\5ccontract-sdk-2efc860d37b7eb43\5cd2914a8\5cpbc_contract_common\5csrc\5cresult_buffer.rs\92\0d\10\00r\00\00\00\0b\00\00\00\05\00\00\00Duplicated or incorrectly ordered sections. Tried to write section with id 0x, but expected section id of at least 0x\00\00\00\14\0e\10\00M\00\00\00a\0e\10\00(\00\00\00\00\00\00\00\02\00\00\00\00\00\00\00\00\00\00\00\02\00\00\00\08\00\00\00 \00\00\00\03\00\00\00\01\00\00\00\02\00\00\00\00\00\00\00\00\00\00\00\02\00\00\00\08\00\00\00 \00\00\00\03\00\00\00\92\0d\10\00r\00\00\00O\00\00\00\09\00\00\00\92\0d\10\00r\00\00\00\8c\00\00\00\0d\00\00\00ZkContractPublicContractSystemContractAccountAddressaddress_type\12\00\00\00\04\00\00\00\04\00\00\00\13\00\00\00identifier\00\00\12\00\00\00\04\00\00\00\04\00\00\00\14\00\00\00\15\00\00\00\04\00\00\00\04\00\00\00\16\00\00\00\18\00\00\00\04\00\00\00\04\00\00\00\19\00\00\00\1a\00\00\00\1b\00\00\00called `Option::unwrap()` on a `None` valueErrorkind\18\00\00\00\01\00\00\00\01\00\00\00\1c\00\00\00message\00\18\00\00\00\08\00\00\00\04\00\00\00\1d\00\00\00KindOscode\00\00\18\00\00\00\04\00\00\00\04\00\00\00\1e\00\00\00\1f\00\00\00\0c\00\00\00\04\00\00\00 \00\00\00memory allocation of  bytes failed\0a\00\18\10\10\00\15\00\00\00-\10\10\00\0e\00\00\00library/std/src/alloc.rsL\10\10\00\18\00\00\00U\01\00\00\09\00\00\00cannot modify the panic hook from a panicking threadt\10\10\004\00\00\00library/std/src/panicking.rs\b0\10\10\00\1c\00\00\00\86\00\00\00\09\00\00\00\b0\10\10\00\1c\00\00\00>\02\00\00\0f\00\00\00\b0\10\10\00\1c\00\00\00=\02\00\00\0f\00\00\00\1f\00\00\00\0c\00\00\00\04\00\00\00!\00\00\00\18\00\00\00\08\00\00\00\04\00\00\00\22\00\00\00#\00\00\00\10\00\00\00\04\00\00\00$\00\00\00%\00\00\00\18\00\00\00\08\00\00\00\04\00\00\00&\00\00\00'\00\00\00\18\00\00\00\00\00\00\00\01\00\00\00(\00\00\00Unsupported\00\18\00\00\00\04\00\00\00\04\00\00\00)\00\00\00Customerror\00\18\00\00\00\04\00\00\00\04\00\00\00*\00\00\00UncategorizedOtherOutOfMemoryUnexpectedEofInterruptedArgumentListTooLongInvalidFilenameTooManyLinksCrossesDevicesDeadlockExecutableFileBusyResourceBusyFileTooLargeFilesystemQuotaExceededNotSeekableStorageFullWriteZeroTimedOutInvalidDataInvalidInputStaleNetworkFileHandleFilesystemLoopReadOnlyFilesystemDirectoryNotEmptyIsADirectoryNotADirectoryWouldBlockAlreadyExistsBrokenPipeNetworkDownAddrNotAvailableAddrInUseNotConnectedConnectionAbortedNetworkUnreachableHostUnreachableConnectionResetConnectionRefusedPermissionDeniedNotFoundoperation successful\00+\00\00\00\04\00\00\00\04\00\00\00,\00\00\00-\00\00\00.\00\00\00library/alloc/src/raw_vec.rscapacity overflow\00\00\00\e8\13\10\00\11\00\00\00\cc\13\10\00\1c\00\00\00\06\02\00\00\05\00\00\00a formatting trait implementation returned an error\00+\00\00\00\00\00\00\00\01\00\00\00/\00\00\00library/alloc/src/fmt.rsX\14\10\00\18\00\00\00d\02\00\00\09\00\00\00called `Option::unwrap()` on a `None` value)..\00\00\ac\14\10\00\02\00\00\00index out of bounds: the len is  but the index is \00\00\b8\14\10\00 \00\00\00\d8\14\10\00\12\00\00\006\00\00\00\00\00\00\00\01\00\00\007\00\00\00matches!===assertion failed: `(left  right)`\0a  left: ``,\0a right: ``: \00\00\00\17\15\10\00\19\00\00\000\15\10\00\12\00\00\00B\15\10\00\0c\00\00\00N\15\10\00\03\00\00\00`\00\00\00\17\15\10\00\19\00\00\000\15\10\00\12\00\00\00B\15\10\00\0c\00\00\00t\15\10\00\01\00\00\00: \00\00\80\14\10\00\00\00\00\00\98\15\10\00\02\00\00\006\00\00\00\0c\00\00\00\04\00\00\008\00\00\009\00\00\00:\00\00\00     {\0a,\0a,  { } }(\0a(,\0a[]library/core/src/fmt/num.rs\00\dc\15\10\00\1b\00\00\00e\00\00\00\14\00\00\000x00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899\00\006\00\00\00\04\00\00\00\04\00\00\00;\00\00\00<\00\00\00=\00\00\00library/core/src/fmt/mod.rs\00\ec\16\10\00\1b\00\00\00z\09\00\00\1e\00\00\00\ec\16\10\00\1b\00\00\00\81\09\00\00\16\00\00\00library/core/src/slice/memchr.rs(\17\10\00 \00\00\00h\00\00\00'\00\00\00range start index  out of range for slice of length X\17\10\00\12\00\00\00j\17\10\00\22\00\00\00range end index \9c\17\10\00\10\00\00\00j\17\10\00\22\00\00\00slice index starts at  but ends at \00\bc\17\10\00\16\00\00\00\d2\17\10\00\0d\00\00\00source slice length () does not match destination slice length (\f0\17\10\00\15\00\00\00\05\18\10\00+\00\00\00\ab\14\10\00\01\00\00\00[...]byte index  is out of bounds of `\00\00M\18\10\00\0b\00\00\00X\18\10\00\16\00\00\00t\15\10\00\01\00\00\00begin <= end ( <= ) when slicing `\00\00\88\18\10\00\0e\00\00\00\96\18\10\00\04\00\00\00\9a\18\10\00\10\00\00\00t\15\10\00\01\00\00\00 is not a char boundary; it is inside  (bytes ) of `M\18\10\00\0b\00\00\00\cc\18\10\00&\00\00\00\f2\18\10\00\08\00\00\00\fa\18\10\00\06\00\00\00t\15\10\00\01\00\00\00library/core/src/str/mod.rs\00(\19\10\00\1b\00\00\00\07\01\00\00\1d\00\00\00library/core/src/unicode/printable.rs\00\00\00T\19\10\00%\00\00\00\0a\00\00\00\1c\00\00\00T\19\10\00%\00\00\00\1a\00\00\00(\00\00\00\00\01\03\05\05\06\06\02\07\06\08\07\09\11\0a\1c\0b\19\0c\1a\0d\10\0e\0c\0f\04\10\03\12\12\13\09\16\01\17\04\18\01\19\03\1a\07\1b\01\1c\02\1f\16 \03+\03-\0b.\010\031\022\01\a7\02\a9\02\aa\04\ab\08\fa\02\fb\05\fd\02\fe\03\ff\09\adxy\8b\8d\a20WX\8b\8c\90\1c\dd\0e\0fKL\fb\fc./?\5c]_\e2\84\8d\8e\91\92\a9\b1\ba\bb\c5\c6\c9\ca\de\e4\e5\ff\00\04\11\12)147:;=IJ]\84\8e\92\a9\b1\b4\ba\bb\c6\ca\ce\cf\e4\e5\00\04\0d\0e\11\12)14:;EFIJ^de\84\91\9b\9d\c9\ce\cf\0d\11):;EIW[\5c^_de\8d\91\a9\b4\ba\bb\c5\c9\df\e4\e5\f0\0d\11EIde\80\84\b2\bc\be\bf\d5\d7\f0\f1\83\85\8b\a4\a6\be\bf\c5\c7\cf\da\dbH\98\bd\cd\c6\ce\cfINOWY^_\89\8e\8f\b1\b6\b7\bf\c1\c6\c7\d7\11\16\17[\5c\f6\f7\fe\ff\80mq\de\df\0e\1fno\1c\1d_}~\ae\af\7f\bb\bc\16\17\1e\1fFGNOXZ\5c^~\7f\b5\c5\d4\d5\dc\f0\f1\f5rs\8ftu\96&./\a7\af\b7\bf\c7\cf\d7\df\9a@\97\980\8f\1f\d2\d4\ce\ffNOZ[\07\08\0f\10'/\ee\efno7=?BE\90\91Sgu\c8\c9\d0\d1\d8\d9\e7\fe\ff\00 _\22\82\df\04\82D\08\1b\04\06\11\81\ac\0e\80\ab\05\1f\09\81\1b\03\19\08\01\04/\044\04\07\03\01\07\06\07\11\0aP\0f\12\07U\07\03\04\1c\0a\09\03\08\03\07\03\02\03\03\03\0c\04\05\03\0b\06\01\0e\15\05N\07\1b\07W\07\02\06\17\0cP\04C\03-\03\01\04\11\06\0f\0c:\04\1d%_ m\04j%\80\c8\05\82\b0\03\1a\06\82\fd\03Y\07\16\09\18\09\14\0c\14\0cj\06\0a\06\1a\06Y\07+\05F\0a,\04\0c\04\01\031\0b,\04\1a\06\0b\03\80\ac\06\0a\06/1M\03\80\a4\08<\03\0f\03<\078\08+\05\82\ff\11\18\08/\11-\03!\0f!\0f\80\8c\04\82\97\19\0b\15\88\94\05/\05;\07\02\0e\18\09\80\be\22t\0c\80\d6\1a\0c\05\80\ff\05\80\df\0c\f2\9d\037\09\81\5c\14\80\b8\08\80\cb\05\0a\18;\03\0a\068\08F\08\0c\06t\0b\1e\03Z\04Y\09\80\83\18\1c\0a\16\09L\04\80\8a\06\ab\a4\0c\17\041\a1\04\81\da&\07\0c\05\05\80\a6\10\81\f5\07\01 *\06L\04\80\8d\04\80\be\03\1b\03\0f\0d\00\06\01\01\03\01\04\02\05\07\07\02\08\08\09\02\0a\05\0b\02\0e\04\10\01\11\02\12\05\13\11\14\01\15\02\17\02\19\0d\1c\05\1d\08\1f\01$\01j\04k\02\af\03\b1\02\bc\02\cf\02\d1\02\d4\0c\d5\09\d6\02\d7\02\da\01\e0\05\e1\02\e7\04\e8\02\ee \f0\04\f8\02\fa\03\fb\01\0c';>NO\8f\9e\9e\9f{\8b\93\96\a2\b2\ba\86\b1\06\07\096=>V\f3\d0\d1\04\14\1867VW\7f\aa\ae\af\bd5\e0\12\87\89\8e\9e\04\0d\0e\11\12)14:EFIJNOde\5c\b6\b7\1b\1c\07\08\0a\0b\14\1769:\a8\a9\d8\d9\097\90\91\a8\07\0a;>fi\8f\92\11o_\bf\ee\efZb\f4\fc\ffST\9a\9b./'(U\9d\a0\a1\a3\a4\a7\a8\ad\ba\bc\c4\06\0b\0c\15\1d:?EQ\a6\a7\cc\cd\a0\07\19\1a\22%>?\e7\ec\ef\ff\c5\c6\04 #%&(38:HJLPSUVXZ\5c^`cefksx}\7f\8a\a4\aa\af\b0\c0\d0\ae\afno\be\93^\22{\05\03\04-\03f\03\01/.\80\82\1d\031\0f\1c\04$\09\1e\05+\05D\04\0e*\80\aa\06$\04$\04(\084\0bNC\817\09\16\0a\08\18;E9\03c\08\090\16\05!\03\1b\05\01@8\04K\05/\04\0a\07\09\07@ '\04\0c\096\03:\05\1a\07\04\0c\07PI73\0d3\07.\08\0a\81&RK+\08*\16\1a&\1c\14\17\09N\04$\09D\0d\19\07\0a\06H\08'\09u\0bB>*\06;\05\0a\06Q\06\01\05\10\03\05\80\8bb\1eH\08\0a\80\a6^\22E\0b\0a\06\0d\13:\06\0a6,\04\17\80\b9<dS\0cH\09\0aFE\1bH\08S\0dI\07\0a\80\f6F\0a\1d\03GI7\03\0e\08\0a\069\07\0a\816\19\07;\03\1cV\01\0f2\0d\83\9bfu\0b\80\c4\8aLc\0d\840\10\16\8f\aa\82G\a1\b9\829\07*\04\5c\06&\0aF\0a(\05\13\82\b0[eK\049\07\11@\05\0b\02\0e\97\f8\08\84\d6*\09\a2\e7\813\0f\01\1d\06\0e\04\08\81\8c\89\04k\05\0d\03\09\07\10\92`G\09t<\80\f6\0as\08p\15Fz\14\0c\14\0cW\09\19\80\87\81G\03\85B\0f\15\84P\1f\06\06\80\d5+\05>!\01p-\03\1a\04\02\81@\1f\11:\05\01\81\d0*\82\e6\80\f7)L\04\0a\04\02\83\11DL=\80\c2<\06\01\04U\05\1b4\02\81\0e,\04d\0cV\0a\80\ae8\1d\0d,\04\09\07\02\0e\06\80\9a\83\d8\04\11\03\0d\03w\04_\06\0c\04\01\0f\0c\048\08\0a\06(\08\22N\81T\0c\1d\03\09\076\08\0e\04\09\07\09\07\80\cb%\0a\84\06library/core/src/unicode/unicode_data.rsError\00\00\00\00\03\00\00\83\04 \00\91\05`\00]\13\a0\00\12\17 \1f\0c `\1f\ef,\a0+*0 ,o\a6\e0,\02\a8`-\1e\fb`.\00\fe 6\9e\ff`6\fd\01\e16\01\0a!7$\0d\e17\ab\0ea9/\18\a190\1caH\f3\1e\a1L@4aP\f0j\a1QOo!R\9d\bc\a1R\00\cfaSe\d1\a1S\00\da!T\00\e0\e1U\ae\e2aW\ec\e4!Y\d0\e8\a1Y \00\eeY\f0\01\7fZ\00p\00\07\00-\01\01\01\02\01\02\01\01H\0b0\15\10\01e\07\02\06\02\02\01\04#\01\1e\1b[\0b:\09\09\01\18\04\01\09\01\03\01\05+\03<\08*\18\01 7\01\01\01\04\08\04\01\03\07\0a\02\1d\01:\01\01\01\02\04\08\01\09\01\0a\02\1a\01\02\029\01\04\02\04\02\02\03\03\01\1e\02\03\01\0b\029\01\04\05\01\02\04\01\14\02\16\06\01\01:\01\01\02\01\04\08\01\07\03\0a\02\1e\01;\01\01\01\0c\01\09\01(\01\03\017\01\01\03\05\03\01\04\07\02\0b\02\1d\01:\01\02\01\02\01\03\01\05\02\07\02\0b\02\1c\029\02\01\01\02\04\08\01\09\01\0a\02\1d\01H\01\04\01\02\03\01\01\08\01Q\01\02\07\0c\08b\01\02\09\0b\07I\02\1b\01\01\01\01\017\0e\01\05\01\02\05\0b\01$\09\01f\04\01\06\01\02\02\02\19\02\04\03\10\04\0d\01\02\02\06\01\0f\01\00\03\00\03\1d\02\1e\02\1e\02@\02\01\07\08\01\02\0b\09\01-\03\01\01u\02\22\01v\03\04\02\09\01\06\03\db\02\02\01:\01\01\07\01\01\01\01\02\08\06\0a\02\010\1f1\040\07\01\01\05\01(\09\0c\02 \04\02\02\01\038\01\01\02\03\01\01\03:\08\02\02\98\03\01\0d\01\07\04\01\06\01\03\02\c6@\00\01\c3!\00\03\8d\01` \00\06i\02\00\04\01\0a \02P\02\00\01\03\01\04\01\19\02\05\01\97\02\1a\12\0d\01&\08\19\0b.\030\01\02\04\02\02'\01C\06\02\02\02\02\0c\01\08\01/\013\01\01\03\02\02\05\02\01\01*\02\08\01\ee\01\02\01\04\01\00\01\00\10\10\10\00\02\00\01\e2\01\95\05\00\03\01\02\05\04(\03\04\01\a5\02\00\04\00\02P\03F\0b1\04{\016\0f)\01\02\02\0a\031\04\02\02\07\01=\03$\05\01\08>\01\0c\024\09\0a\04\02\01_\03\02\01\01\02\06\01\02\01\9d\01\03\08\15\029\02\01\01\01\01\16\01\0e\07\03\05\c3\08\02\03\01\01\17\01Q\01\02\06\01\01\02\01\01\02\01\02\eb\01\02\04\06\02\01\02\1b\02U\08\02\01\01\02j\01\01\01\02\06\01\01e\03\02\04\01\05\00\09\01\02\f5\01\0a\02\01\01\04\01\90\04\02\02\04\01 \0a(\06\02\04\08\01\09\06\02\03.\0d\01\02\00\07\01\06\01\01R\16\02\07\01\02\01\02z\06\03\01\01\02\01\07\01\01H\02\03\01\01\01\00\02\0b\024\05\05\01\01\01\00\01\06\0f\00\05;\07\00\01?\04Q\01\00\02\00.\02\17\00\01\01\03\04\05\08\08\02\07\1e\04\94\03\007\042\08\01\0e\01\16\05\01\0f\00\07\01\11\02\07\01\02\01\05d\01\a0\07\00\01=\04\00\04\00\07m\07\00`\80\f0\00\00\18\1f\10\00(\00\00\00?\01\00\00\09\00\00\00"))
