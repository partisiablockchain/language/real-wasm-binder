package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.TestRealBinary;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.PlainTextMultiplier;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import com.partisiablockchain.zk.real.protocol2.binary.BinaryProtocolFactory;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test execution of circuits. */
public final class ZkCircuitExecuteTest {

  static List<ExampleCircuits.TestCase> testCases() {
    return ExampleCircuits.testCases();
  }

  static List<ExampleCircuits.TestCaseForBad> testCasesForBad() {
    return ExampleCircuits.testCasesForBad();
  }

  static RealProtocol<
          SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
      testRealBinary(IntFunction<List<List<Boolean>>> variableLookup) {
    return BinaryProtocolFactory.from(
        new TestRealBinary(new PlainTextMultiplier<BinaryExtensionFieldElement>(), variableLookup));
  }

  @ParameterizedTest
  @MethodSource("testCases")
  void executeSharedNumber(ExampleCircuits.TestCase testCase) {
    // Create engine
    final RealProtocol<
            SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        protocol = testRealBinary(sharedEnv(testCase.inputType(), testCase.inputs()));

    // Check success
    final SecretSharedNumberAsBits result =
        TestUtil.executeSecret(protocol, testCase.circuit()).get(0);
    Assertions.assertThat(TestUtil.reveal(result))
        .as(testCase.testname())
        .isEqualTo(testCase.expectedOutput());
  }

  @ParameterizedTest
  @MethodSource("testCasesForBad")
  void failInExecuteSharedNumber(ExampleCircuits.TestCaseForBad testCase) {
    // Create engine
    final RealProtocol<
            SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        protocol = testRealBinary(sharedEnv(testCase.inputType(), testCase.inputs()));

    // Check fails
    Assertions.assertThatCode(() -> TestUtil.executeSecret(protocol, testCase.circuit()).get(0))
        .as(testCase.testname())
        .isInstanceOf(RuntimeException.class)
        .hasMessage(testCase.expectedErrorMessage());
  }

  private static IntFunction<List<List<Boolean>>> sharedEnv(
      ZkType inputType, List<Integer> inputs) {
    final Map<Integer, List<List<Boolean>>> env = new HashMap<>();

    final int bitwidth = ((ZkType.Sbi) inputType).bitwidth();

    for (int idx = 0; idx < inputs.size(); idx++) {
      final int input = inputs.get(idx);
      final List<Boolean> bits =
          IntStream.range(0, bitwidth)
              .mapToObj(bitIdx -> ((input >> bitIdx) & 0x1) == 0x1)
              .toList();
      env.put(idx + 1, List.of(bits));
    }

    return env::get;
  }

  @Test
  void testMultiOutputCircuit() {
    final RealProtocol<
            SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        protocol = testRealBinary(value -> Assertions.fail("Computation must never lookup"));

    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress constValue =
        zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 101791));
    final ZkAddress addValue =
        zkCircuit.add(
            ZkType.SBI32,
            new ZkOperation.Binary(
                ZkOperation.BinaryOp.ARITH_ADD_WRAPPING, constValue, constValue));
    final ZkAddress negatedValue =
        zkCircuit.add(
            ZkType.SBI32,
            new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, constValue));
    zkCircuit.setRoots(List.of(addValue, negatedValue));
    final List<SecretSharedNumberAsBits> result = TestUtil.executeSecret(protocol, zkCircuit);
    Assertions.assertThat(TestUtil.reveal(result.get(0))).isEqualTo(101791 + 101791);
    Assertions.assertThat(TestUtil.reveal(result.get(1))).isEqualTo(-101791);
  }

  @Test
  void testMultiOutputTypeCircuit() {
    final RealProtocol<
            SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        protocol = testRealBinary(value -> Assertions.fail("Computation must never lookup"));

    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress constValue =
        zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 101791));
    final ZkAddress addValue =
        zkCircuit.add(
            ZkType.SBI32,
            new ZkOperation.Binary(
                ZkOperation.BinaryOp.ARITH_ADD_WRAPPING, constValue, constValue));
    final ZkAddress ltValue =
        zkCircuit.add(
            ZkType.SBI32,
            new ZkOperation.Binary(
                ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED, constValue, addValue));
    zkCircuit.setRoots(List.of(addValue, ltValue));
    final List<SecretSharedNumberAsBits> result = TestUtil.executeSecret(protocol, zkCircuit);
    Assertions.assertThat(TestUtil.reveal(result.get(0))).isEqualTo(101791 + 101791);
    Assertions.assertThat(TestUtil.reveal(result.get(1))).isEqualTo(1);
  }

  @Test
  void loadedSharedNumberMustMatchTypeLength() {
    RealProtocol<SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        protocol = testRealBinary(badVariableLookup(40, 0x01020304));

    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    ZkAddress root =
        zkCircuit.add(
            ZkType.SBI32, new ZkOperation.Load(ZkType.SBI32, new ZkOperation.ZkVariableId(1)));
    zkCircuit.setRoots(List.of(root));
    Assertions.assertThatCode(() -> TestUtil.executeSecret(protocol, zkCircuit))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Loaded secret variable 1 had incorrect bit length 40; expected 32 bits");
  }

  private static IntFunction<List<List<Boolean>>> badVariableLookup(int bitwidth, int input) {
    final Map<Integer, List<List<Boolean>>> env = new HashMap<>();
    final List<Boolean> bits =
        IntStream.range(0, bitwidth).mapToObj(bitIdx -> ((input >> bitIdx) & 0x1) == 0x1).toList();
    env.put(1, List.of(bits));
    return env::get;
  }

  @Test
  void subtractLargeSecretSharedNumbers() {
    final RealProtocol<
            SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        protocol = testRealBinary(value -> Assertions.fail("Computation must never lookup"));
    final ZkType.Sbi sbi128 = new ZkType.Sbi(128);

    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    ZkAddress const1 = zkCircuit.add(sbi128, ZkOperation.Constant.fromInt(sbi128, 1));
    ZkAddress const2 = zkCircuit.add(sbi128, ZkOperation.Constant.fromInt(sbi128, 2));
    ZkAddress root =
        zkCircuit.add(
            sbi128,
            new ZkOperation.Binary(ZkOperation.BinaryOp.ARITH_SUBTRACT_WRAPPING, const2, const1));

    zkCircuit.setRoots(List.of(root));
    final SecretSharedNumberAsBits result = TestUtil.executeSecret(protocol, zkCircuit).get(0);
    Assertions.assertThat(toBigInt(result)).isEqualTo(BigInteger.ONE);
  }

  private static BigInteger toBigInt(final SecretSharedNumberAsBits value) {
    BigInteger bitVal = BigInteger.ONE;
    BigInteger val = BigInteger.ZERO;
    for (BinaryExtensionFieldElement element : value.stream().toList()) {
      if (element.isOne()) {
        val = val.add(bitVal);
      }
      bitVal = bitVal.multiply(BigInteger.TWO);
    }
    return val;
  }
}
