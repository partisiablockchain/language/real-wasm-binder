package com.partisiablockchain.language.zkcircuit.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.zk.ZkComputationState;
import com.partisiablockchain.language.realwasmbinder.MetaCircuitComputation;
import com.partisiablockchain.language.zkcircuit.TestUtil;
import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitValidator;
import com.partisiablockchain.language.zkcircuit.ZkType;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.protocol.RealEnvironment;
import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import java.util.List;
import java.util.function.Function;

/** A REAL created from the zkstate and outputs Sbi32. */
record ZkCircuitSbi32FromState(
    Function<ZkComputationState<ZkOpenState, StateVoid, RealClosed<StateVoid>>, ZkCircuit>
        circuitFromState)
    implements RealComputationEnv<ZkOpenState, StateVoid> {

  @Override
  public <
          NumberT extends SharedNumber<NumberT>,
          BitT extends SharedBit<BitT>,
          ElementT extends FiniteFieldElement<ElementT>>
      void compute(
          final ZkComputationState<ZkOpenState, StateVoid, RealClosed<StateVoid>> zkState,
          final RealEnvironment<NumberT, BitT> environment) {

    final RealProtocol<NumberT, BitT, ElementT> protocol =
        MetaCircuitComputation.realProtocol(environment);

    final ZkCircuit circuit = circuitFromState.apply(zkState);
    ZkCircuitValidator.validateZkCircuit(circuit, List.of(ZkType.SBI32));
    final NumberT result = TestUtil.executeSecret(protocol, circuit).get(0);
    protocol.storage().saveNumber(result);
  }
}
