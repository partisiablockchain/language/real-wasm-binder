package com.partisiablockchain.language.zkcircuit.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.zk.SecretInputBuilder;
import com.partisiablockchain.contract.zk.ShareReader;
import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.contract.zk.ZkContractContext;
import com.partisiablockchain.contract.zk.ZkState;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.contract.RealContract;
import com.secata.stream.SafeDataInputStream;
import java.util.Collections;
import java.util.List;

/**
 * A zero-knowledge contract running a ZK computation based on ZK circuits.
 *
 * <p>The contract has a hard-coded interface for interactions: It accepts secret 32bit signed
 * integer inputs from any sender. Sending an open input starts the computation of the circuit. The
 * computation produces a number of secret 32bit signed integer output. When the computation
 * completes the result is opened and copied to the open state. Finally, the contract is prepared
 * for receiving more secret input and repeating the computation.
 */
public final class ZkCircuitContract extends RealContract<ZkOpenState, StateVoid> {

  /** The number outputs from the zk computation. */
  private int numComputationOutputs;

  private int inputBitlength;

  /**
   * Create a smart contract that runs a ZK computation based on ZK circuits.
   *
   * @param computation The computation to run
   * @param numComputationOutputs The number outputs from the zk computation
   */
  public ZkCircuitContract(
      RealComputationEnv<ZkOpenState, StateVoid> computation,
      int numComputationOutputs,
      int inputBitlength) {
    super(computation);
    this.numComputationOutputs = numComputationOutputs;
    this.inputBitlength = inputBitlength;
  }

  @Override
  public ZkOpenState onCreate(
      ZkContractContext context,
      ZkState<StateVoid, RealClosed<StateVoid>> zkState,
      SafeDataInputStream invocation) {
    return new ZkOpenState();
  }

  /** Secret inputs are only bit-length validated. */
  @Override
  public ZkOpenState onSecretInput(
      ZkContractContext context,
      ZkState<StateVoid, RealClosed<StateVoid>> zkState,
      ZkOpenState state,
      SecretInputBuilder<StateVoid> input,
      SafeDataInputStream invocation) {
    // Validate bit-length
    input.expectBitLength(inputBitlength);
    return state;
  }

  /** Open Input starts a computation. */
  @Override
  public ZkOpenState onOpenInput(
      ZkContractContext context,
      ZkState<StateVoid, RealClosed<StateVoid>> zkState,
      ZkOpenState state,
      SafeDataInputStream invocation) {
    if (zkState.getVariables().size() < 2) {
      throw new IllegalArgumentException("Need at least two inputs to compute");
    }
    // Start computation with expected number of outputs
    zkState.startComputation(Collections.nCopies(numComputationOutputs, null));
    return state;
  }

  /** The (single) result of the computation is revealed. */
  @Override
  public ZkOpenState onComputeComplete(
      ZkContractContext context,
      ZkState<StateVoid, RealClosed<StateVoid>> zkState,
      ZkOpenState state,
      List<Integer> createdVariables) {
    // Instruct the gates to reveal the value of the result
    zkState.openVariables(createdVariables);
    return state;
  }

  @Override
  public ZkOpenState onVariablesOpened(
      ZkContractContext context,
      ZkState<StateVoid, RealClosed<StateVoid>> zkState,
      ZkOpenState state,
      List<Integer> openedVariables) {
    // Copy the opened variable values to the open state
    for (int i = 0; i < numComputationOutputs; i++) {
      state = state.setValue(i, readOpenedI32(zkState.getVariable(openedVariables.get(i))));
    }
    // Delete the result variable - and prepare for more input / a new computation
    openedVariables.stream().mapToInt(value -> value).forEach(zkState::deleteVariable);
    return state;
  }

  private int readOpenedI32(RealClosed<StateVoid> resultVariable) {
    // Fetch the opened result value
    byte[] openResultValue = packBitArrayIntoByteArray(resultVariable.getOpenValue());
    ShareReader shareReader =
        ShareReader.create(openResultValue, resultVariable.getShareBitLengths().get(0));
    return (int) shareReader.readLong(32);
  }

  // Pack a little endian bit array into a little endian byte array
  private byte[] packBitArrayIntoByteArray(byte[] bits) {
    int byteLength = ZkClosed.getByteLength(bits.length);
    byte[] result = new byte[byteLength];
    int byteIndex = 0;
    int bitIndex = 0;

    for (byte bit : bits) {
      result[byteIndex] = (byte) (result[byteIndex] | ((bit & 1) << bitIndex));
      bitIndex++;
      if (bitIndex == Byte.SIZE) {
        bitIndex = 0;
        byteIndex++;
      }
    }
    return result;
  }
}
