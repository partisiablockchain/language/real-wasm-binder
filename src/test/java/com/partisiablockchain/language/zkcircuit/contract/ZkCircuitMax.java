package com.partisiablockchain.language.zkcircuit.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.zk.ZkComputationState;
import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitAppendable;
import com.partisiablockchain.language.zkcircuit.ZkCircuitImpl;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import java.util.List;

/**
 * A REAL computation based on {@link ZkCircuits} that finds the maximum value of all the secret
 * inputs.
 */
final class ZkCircuitMax {

  static final RealComputationEnv<ZkOpenState, StateVoid> COMPUTATION =
      new ZkCircuitSbi32FromState(ZkCircuitMax::circuitFromState);

  private static <
          NumberT extends SharedNumber<NumberT>,
          BitT extends SharedBit<BitT>,
          ElementT extends FiniteFieldElement<ElementT>>
      ZkCircuit circuitFromState(
          ZkComputationState<ZkOpenState, StateVoid, RealClosed<StateVoid>> zkState) {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    ZkAddress max = circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 0));
    for (RealClosed<StateVoid> variable : zkState.getVariables()) {
      final ZkAddress value =
          circuit.add(
              ZkType.SBI32,
              new ZkOperation.Load(ZkType.SBI32, new ZkOperation.ZkVariableId(variable.getId())));
      final ZkAddress cond =
          circuit.add(
              ZkType.SBOOL,
              new ZkOperation.Binary(ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED, max, value));
      max = circuit.add(ZkType.SBI32, new ZkOperation.Select(cond, value, max));
    }
    circuit.setRoots(List.of(max));
    return circuit;
  }
}
