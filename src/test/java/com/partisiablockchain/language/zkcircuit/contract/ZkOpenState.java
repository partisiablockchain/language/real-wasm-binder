package com.partisiablockchain.language.zkcircuit.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.Collections;

/** The open state of the ZK contract. The open state consists of 10 variables with type i32. */
@Immutable
public final class ZkOpenState implements StateSerializable {

  /** The i32 values of the open variables. The ID of the open variable is the index in the list */
  private final FixedList<Integer> openVariables;

  private ZkOpenState(FixedList<Integer> openVariables) {
    this.openVariables = openVariables;
  }

  /** Create an initial state with all 10 variables set to zero. */
  public ZkOpenState() {
    this(FixedList.create(Collections.nCopies(10, 0)));
  }

  /**
   * Set the value of an open variable.
   *
   * @param variableId The variable ID (0-9)
   * @param value The value to assign to the variable
   * @return The updated state.
   */
  public ZkOpenState setValue(int variableId, int value) {
    return new ZkOpenState(openVariables.setElement(variableId, value));
  }

  /**
   * Get the value of an open variable.
   *
   * @param variableId The variable ID (0-9)
   * @return The i32 value of the open variable
   */
  public Integer getValue(int variableId) {
    return openVariables.get(variableId);
  }
}
