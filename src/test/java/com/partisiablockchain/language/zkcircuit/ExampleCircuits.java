package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/** This have been duplicated directly from zk-circuit. */
final class ExampleCircuits {

  private ExampleCircuits() {}

  record TestCase(
      String testname,
      ZkCircuit circuit,
      ZkType inputType,
      List<Integer> inputs,
      int expectedOutput) {

    TestCaseForBad asBad(String expectedErrorMessage) {
      return new TestCaseForBad(testname, circuit, inputType, inputs, expectedErrorMessage);
    }

    TestCaseForBad asBad(String expectedErrorMessage, List<Integer> inputs) {
      return new TestCaseForBad(testname, circuit, inputType, inputs, expectedErrorMessage);
    }
  }

  record TestCaseForBad(
      String testname,
      ZkCircuit circuit,
      ZkType inputType,
      List<Integer> inputs,
      String expectedErrorMessage) {}

  @CheckReturnValue
  static List<TestCase> testCases() {
    final List<TestCase> cases = new ArrayList<>();

    for (int c = 1; c < 100; c += 7) {
      final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
      final ZkAddress constValue =
          zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, c));
      zkCircuit.setRoots(List.of(constValue));
      cases.add(new TestCase("constant_sbi32_" + c, zkCircuit, ZkType.SBOOL, List.of(), c));
    }

    for (int c = 3; c < 100; c += 7) {
      cases.add(unopCase(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, ZkType.SBI32, c, -c));
    }

    for (int c = 0; c <= 1; c++) {
      final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
      final ZkAddress constValue =
          zkCircuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, c));
      zkCircuit.setRoots(List.of(constValue));

      cases.add(new TestCase("constant_sbi1_" + c, zkCircuit, ZkType.SBOOL, List.of(), c));
    }

    for (int c = 0; c <= 1; c++) {
      cases.add(unopCase(ZkOperation.UnaryOp.BITWISE_NOT, ZkType.SBOOL, c, 1 - c));
    }

    for (int c = 0; c < 1; c++) {
      cases.add(unopCase(ZkOperation.UnaryOp.BITWISE_NOT, ZkType.SBI32, 0x01020304, 0xfefdfcfb));
    }

    for (int c1 = 0; c1 < 16; c1++) {
      for (int c2 = 0; c2 < 16; c2++) {
        cases.add(binopCase(ZkOperation.BinaryOp.BITWISE_OR, new ZkType.Sbi(4), c1, c2, c1 | c2));
        cases.add(binopCase(ZkOperation.BinaryOp.BITWISE_AND, new ZkType.Sbi(4), c1, c2, c1 & c2));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.ARITH_MULTIPLY_SIGNED, new ZkType.Sbi(8), c1, c2, c1 * c2));
        cases.add(
            binopCase(ZkOperation.BinaryOp.ARITH_ADD_WRAPPING, new ZkType.Sbi(8), c1, c2, c1 + c2));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.ARITH_SUBTRACT_WRAPPING,
                new ZkType.Sbi(8),
                c1,
                c2,
                (c1 - c2 + 0x100) & 0xFF));
        cases.add(
            binopCase(ZkOperation.BinaryOp.BIT_CONCAT, new ZkType.Sbi(4), c1, c2, (c1 << 4) | c2));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_EQUALS, new ZkType.Sbi(4), c1, c2, c1 == c2 ? 1 : 0));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED,
                new ZkType.Sbi(5),
                c1 - 8,
                c2 - 8,
                c1 < c2 ? 1 : 0));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_SIGNED,
                new ZkType.Sbi(5),
                c1 - 8,
                c2 - 8,
                c1 <= c2 ? 1 : 0));
      }
    }

    for (int c2 = 0; c2 <= 0xf; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr =
          zkCircuit.add(new ZkType.Sbi(4), ZkOperation.Constant.fromInt(new ZkType.Sbi(4), c2));
      final ZkAddress v1 = addConcat(zkCircuit, new ZkType.Sbi(8), input1Addr, input1Addr);
      final ZkAddress v2 = addConcat(zkCircuit, new ZkType.Sbi(16), v1, v1);
      zkCircuit.setRoots(List.of(v2));

      final int expected = c2 | (c2 << 4) | (c2 << 8) | (c2 << 12);
      cases.add(new TestCase("concat_x4", zkCircuit, ZkType.SBOOL, List.of(), expected));
    }

    for (int c2 = 0; c2 <= 0xf; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr =
          zkCircuit.add(new ZkType.Sbi(4), ZkOperation.Constant.fromInt(new ZkType.Sbi(4), c2));
      final ZkAddress v1 = addConcat(zkCircuit, new ZkType.Sbi(8), input1Addr, input1Addr);
      final ZkAddress v2 = addConcat(zkCircuit, new ZkType.Sbi(12), v1, input1Addr);
      zkCircuit.setRoots(List.of(v2));

      final int expected = c2 | (c2 << 4) | (c2 << 8);
      cases.add(new TestCase("concat_x3", zkCircuit, ZkType.SBOOL, List.of(), expected));
    }

    for (int c2 = 0; c2 <= 9; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr =
          zkCircuit.add(
              new ZkType.Sbi(8), ZkOperation.Constant.fromInt(new ZkType.Sbi(8), 0x40 | c2));
      final ZkAddress computedValueAddr =
          zkCircuit.add(new ZkType.Sbi(4), new ZkOperation.Extract(input1Addr, 4, 4));
      zkCircuit.setRoots(List.of(computedValueAddr));
      cases.add(new TestCase("extract_4_" + c2, zkCircuit, ZkType.SBOOL, List.of(), 4));
    }

    for (int c2 = 0; c2 <= 9; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr =
          zkCircuit.add(
              new ZkType.Sbi(8), ZkOperation.Constant.fromInt(new ZkType.Sbi(8), 0x40 | c2));
      final ZkAddress computedValueAddr =
          zkCircuit.add(new ZkType.Sbi(4), new ZkOperation.Extract(input1Addr, 4, 0));
      zkCircuit.setRoots(List.of(computedValueAddr));
      cases.add(new TestCase("extract_0_" + c2, zkCircuit, ZkType.SBOOL, List.of(), c2));
    }

    for (int c = 0; c <= 1; c++) {
      final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
      final ZkAddress constValue =
          zkCircuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, c));
      final ZkAddress v1 =
          zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 231));
      final ZkAddress v2 =
          zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 666));
      final ZkAddress negatedValue =
          zkCircuit.add(ZkType.SBI32, new ZkOperation.Select(constValue, v1, v2));
      zkCircuit.setRoots(List.of(negatedValue));
      cases.add(
          new TestCase("select_" + c, zkCircuit, ZkType.SBOOL, List.of(), c == 0 ? 666 : 231));
    }

    return cases;
  }

  @CheckReturnValue
  private static ZkAddress addConcat(
      ZkCircuitAppendable zkCircuit, ZkType type, ZkAddress v1, ZkAddress v2) {
    return zkCircuit.add(type, new ZkOperation.Binary(ZkOperation.BinaryOp.BIT_CONCAT, v1, v2));
  }

  @CheckReturnValue
  static List<TestCaseForBad> testCasesForBad() {
    final List<TestCaseForBad> cases = new ArrayList<>();

    cases.add(
        binopCase(ZkOperation.BinaryOp.UNKNOWN, new ZkType.Sbi(8), 0, 0, 0)
            .asBad("Unknown binop: UNKNOWN"));

    cases.add(
        unopCase(ZkOperation.UnaryOp.UNKNOWN, new ZkType.Sbi(8), 0, 0)
            .asBad("Unknown unop: UNKNOWN"));

    return cases;
  }

  @CheckReturnValue
  private static TestCase binopCase(
      final ZkOperation.BinaryOp operation,
      final ZkType.Sbi inputType,
      final int input1,
      final int input2,
      final int output) {
    // Produce circuit
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress input1Addr =
        zkCircuit.add(inputType, new ZkOperation.Load(inputType, new ZkOperation.ZkVariableId(1)));
    final ZkAddress input2Addr =
        zkCircuit.add(inputType, ZkOperation.Constant.fromInt(inputType, input2));
    final ZkAddress computedValueAddr =
        zkCircuit.add(inputType, new ZkOperation.Binary(operation, input1Addr, input2Addr));
    zkCircuit.setRoots(List.of(computedValueAddr));

    // Produce name
    final String name =
        "%s_sbi%s_%d_%d"
            .formatted(
                operation.toString().toLowerCase(Locale.ENGLISH),
                inputType.bitwidth(),
                input1,
                input2);
    return new TestCase(name, zkCircuit, inputType, List.of(input1), output);
  }

  @CheckReturnValue
  private static TestCase unopCase(
      final ZkOperation.UnaryOp operation,
      final ZkType.Sbi inputType,
      final int input1,
      final int output) {
    // Produce circuit
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress input1Addr =
        zkCircuit.add(inputType, new ZkOperation.Load(inputType, new ZkOperation.ZkVariableId(1)));
    final ZkAddress computedValueAddr =
        zkCircuit.add(inputType, new ZkOperation.Unary(operation, input1Addr));
    zkCircuit.setRoots(List.of(computedValueAddr));

    // Produce name
    final String name =
        "%s_sbi%s_%d"
            .formatted(
                operation.toString().toLowerCase(Locale.ENGLISH), inputType.bitwidth(), input1);
    return new TestCase(name, zkCircuit, inputType, List.of(input1), output);
  }
}
