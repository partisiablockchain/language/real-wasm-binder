package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.testutil.ZkCircuitExecutionComplianceTesting;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test execution of circuits. */
public final class ZkCircuitSharedNumberExecutionTest {

  private static ZkCircuitSharedNumberExecution<
          SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
      zkCircuitSharedNumberExecution() {
    final RealProtocol<
            SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        testRealProtocol =
            ZkCircuitExecuteTest.testRealBinary(
                value -> Assertions.fail("Computation must never lookup"));

    return new ZkCircuitSharedNumberExecution<
        SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>(
        testRealProtocol);
  }

  @Test
  void checkCompliance() {
    final var zkCircuitSharedNumberExecution = zkCircuitSharedNumberExecution();
    Assertions.assertThat(
            ZkCircuitExecutionComplianceTesting.testCompliance(
                zkCircuitSharedNumberExecution, SecretSharedNumberAsBits::isEqual))
        .isEmpty();
  }

  private static final BinaryExtensionFieldElement V0 = BinaryExtensionFieldElement.ZERO;
  private static final BinaryExtensionFieldElement V1 = BinaryExtensionFieldElement.ONE;

  private static final Map<SecretSharedNumberAsBits, SecretSharedNumberAsBits>
      SIGN_EXTEND_MAPPING_TO_6_BITS =
          Map.ofEntries(
              Map.entry(
                  SecretSharedNumberAsBits.create(List.of(V0)),
                  SecretSharedNumberAsBits.create(List.of(V0, V0, V0, V0, V0, V0))),
              Map.entry(
                  SecretSharedNumberAsBits.create(List.of(V1)),
                  SecretSharedNumberAsBits.create(List.of(V1, V1, V1, V1, V1, V1))),
              Map.entry(
                  SecretSharedNumberAsBits.create(List.of(V1, V0, V1)),
                  SecretSharedNumberAsBits.create(List.of(V1, V0, V1, V1, V1, V1))),
              Map.entry(
                  SecretSharedNumberAsBits.create(List.of(V0, V0, V1)),
                  SecretSharedNumberAsBits.create(List.of(V0, V0, V1, V1, V1, V1))),
              Map.entry(
                  SecretSharedNumberAsBits.create(List.of(V1, V1, V1)),
                  SecretSharedNumberAsBits.create(List.of(V1, V1, V1, V1, V1, V1))),
              Map.entry(
                  SecretSharedNumberAsBits.create(List.of(V0, V1, V0, V1, V0, V1)),
                  SecretSharedNumberAsBits.create(List.of(V0, V1, V0, V1, V0, V1))),
              Map.entry(
                  SecretSharedNumberAsBits.create(List.of(V1, V1, V1, V1, V1, V1)),
                  SecretSharedNumberAsBits.create(List.of(V1, V1, V1, V1, V1, V1))));

  @Test
  void executeGateSignExtendTestFailing() {
    final var operation = new ZkOperation.SignExtend(new ZkType.Sbi(6), new ZkAddress(0));
    final var zkCircuitSharedNumberExecution = zkCircuitSharedNumberExecution();
    final SecretSharedNumberAsBits zeroBitInput = SecretSharedNumberAsBits.create(List.of());
    Assertions.assertThatCode(
            () -> zkCircuitSharedNumberExecution.executeGateSignExtend(operation, zeroBitInput))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Cannot sign extend from zero-width operand!");
  }

  @Test
  void executeGateSignExtendTest() {
    final var zkCircuitSharedNumberExecution = zkCircuitSharedNumberExecution();
    final var operation = new ZkOperation.SignExtend(new ZkType.Sbi(6), new ZkAddress(0));

    for (final SecretSharedNumberAsBits input : SIGN_EXTEND_MAPPING_TO_6_BITS.keySet()) {
      final SecretSharedNumberAsBits output = SIGN_EXTEND_MAPPING_TO_6_BITS.get(input);
      final var result = zkCircuitSharedNumberExecution.executeGateSignExtend(operation, input);
      final var errstr =
          "sign-extend(%s) produced %s".formatted(numberToString(input), numberToString(result));

      Assertions.assertThat(result).as(errstr).hasSize(6);
      Assertions.assertThat(result.isEqual(output)).as(errstr).isTrue();
    }
  }

  private static String numberToString(SecretSharedNumberAsBits number) {
    return number.stream()
        .map(BinaryExtensionFieldElement::intValue)
        .map(Object::toString)
        .collect(Collectors.joining(",", "[", "]"));
  }

  @Test
  void executeUnaryGateNegate() {
    final var zkCircuitSharedNumberExecution = zkCircuitSharedNumberExecution();
    var real = zkCircuitSharedNumberExecution.protocol();

    SecretSharedNumberAsBits zero = real.shareFactory().constant(0, 32);
    Assertions.assertThat(executeNegate(zero).isEqual(zero)).isTrue();

    SecretSharedNumberAsBits seven = real.shareFactory().constant(7, 128);
    SecretSharedNumberAsBits negSeven = real.shareFactory().constant(-7, 128);
    Assertions.assertThat(executeNegate(seven).isEqual(negSeven)).isTrue();
    Assertions.assertThat(executeNegate(negSeven).isEqual(seven)).isTrue();

    SecretSharedNumberAsBits max = real.shareFactory().constant(Integer.MAX_VALUE, 32);
    SecretSharedNumberAsBits minPlusOne = real.shareFactory().constant(Integer.MIN_VALUE + 1, 32);
    Assertions.assertThat(executeNegate(max).isEqual(minPlusOne)).isTrue();
    Assertions.assertThat(executeNegate(minPlusOne).isEqual(max)).isTrue();

    SecretSharedNumberAsBits min = real.shareFactory().constant(Integer.MIN_VALUE, 32);
    Assertions.assertThat(executeNegate(min).isEqual(min)).isTrue();
  }

  private static SecretSharedNumberAsBits executeNegate(SecretSharedNumberAsBits zero) {
    final var zkCircuitSharedNumberExecution = zkCircuitSharedNumberExecution();
    return zkCircuitSharedNumberExecution.executeGateUnary(
        new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, new ZkAddress(0)), zero);
  }
}
