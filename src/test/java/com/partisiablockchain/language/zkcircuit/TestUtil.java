package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import java.util.List;

/** Utility methods for testing ZK circuits. */
public final class TestUtil {

  /**
   * Executes the given circuit with the given protocol.
   *
   * @param protocol Protocol to run with.
   * @param circuit Circuit to run.
   * @return Produced output results
   */
  public static <
          NumberT extends SharedNumber<NumberT>,
          BitT extends SharedBit<BitT>,
          ElementT extends FiniteFieldElement<ElementT>>
      List<NumberT> executeSecret(
          final RealProtocol<NumberT, BitT, ElementT> protocol, final ZkCircuit circuit) {

    return new ZkCircuitEngine<>(new ZkCircuitSharedNumberExecution<>(protocol), circuit)
        .executeZkCircuit();
  }

  /**
   * Reveal the secret value from a test-version of a secret-shared boolean.
   *
   * @param value The sbool value
   * @return The revealed secret value
   */
  static boolean reveal(final SharedBit<?> value) {
    return ((BinaryExtensionFieldElement) value).isOne();
  }

  /**
   * Reveal the secret value from a test-version of a secret-shared i32.
   *
   * @param value The Sbi32 value
   * @return The revealed secret value
   */
  static int reveal(final SharedNumber<?> value) {
    int bitVal = 1;
    int val = 0;
    SecretSharedNumberAsBits numberAsBits = (SecretSharedNumberAsBits) value;
    for (BinaryExtensionFieldElement element : numberAsBits.stream().toList()) {
      if (element.isOne()) {
        val += bitVal;
      }
      bitVal *= 2;
    }
    return val;
  }
}
