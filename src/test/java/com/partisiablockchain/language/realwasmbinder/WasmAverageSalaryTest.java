package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.contract.RealTest;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;

/** Tests for {@link WasmRealContract}. */
final class WasmAverageSalaryTest extends AbstractWasmAverageSalaryTest {

  /**
   * Filename of the compiled WASM for the tests.
   *
   * <p>Source code for the testing contract can be found in the rust-example-average-salary
   * example: {@code https://gitlab.com/partisiablockchain/language/contracts/zk-average-salary}.
   *
   * <p>Requires no features enabled.
   */
  private static final String FILE_CONTRACT = "contracts/average_salary.zkwa";

  /** Initialize test. */
  public WasmAverageSalaryTest() {
    super(FILE_CONTRACT, null);
  }

  /** Tests creation of the average salary contract. */
  @RealTest
  public void createContract() {
    createContractInternal();
  }

  /** Tests lifecycle of the average salary contract. */
  @RealTest(previous = "createContract")
  public void simpleHappyPath() {
    simpleHappyPathInternal();

    // Check result
    Assertions.assertThat(getContractState().getData())
        .isEqualTo(
            Hex.decode(
                "000000000000000000000000000000000000123456" // Admin addr
                    + "01c8af0000" // Average salary (with 1 being an option discrimiant)
                    + "0103000000" // Number employees (with 1 being an option discrimiant)
                ));
  }

  /** Tests lifecycle of the average salary contract. */
  @RealTest(previous = "createContract")
  public void hugeHappyPath() {
    hugeHappyPathInternal();

    // Check result
    final byte[] decode =
        Hex.decode(
            "000000000000000000000000000000000000123456" // Admin addr
                + "0156130000" // Average salary (with 1 being an option discrimiant)
                + "0164000000" // Number employees (with 1 being an option discrimiant)
            );
    Assertions.assertThat(getContractState().getData()).isEqualTo(decode);
  }

  /** Tests lifecycle of the average salary contract. */
  @RealTest(previous = "createContract")
  public void complexLifecycle() {
    complexLifecycleInternal();

    // Check result
    byte[] decode =
        Hex.decode(
            "000000000000000000000000000000000000123456" // Admin addr
                + "01c8af0000" // Average salary (with 1 being an option discrimiant)
                + "0103000000" // Number employees (with 1 being an option discrimiant)
            );
    Assertions.assertThat(getContractState().getData()).isEqualTo(decode);
  }
}
