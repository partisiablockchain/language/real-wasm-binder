package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.zk.real.contract.RealTest;
import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Tests for {@link WasmRealContract}. */
public final class ZkManyVariablesTest extends BinaryRealContractTest {

  private static final Logger logger = LoggerFactory.getLogger(ZkManyVariablesTest.class);

  /**
   * Source: <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/zk-secret-counters">zk-secret-counters</a>.
   */
  private static final String fileContract = "contracts/zk_many_variables_test.zkwa";

  /** Initialize test. */
  public ZkManyVariablesTest() {
    super(AbstractWasmAverageSalaryTest.loadInvoker(fileContract));
  }

  @Override
  protected List<byte[]> asShares(List<Integer> values) {
    return super.asShares(values).stream()
        .map(WasmAverageSalaryTest::expandByteArrayToBitArray)
        .toList();
  }

  //// Action constants

  static final byte[] ACTION_START_COMPUTE = new byte[] {0x01};

  //// Tests

  /** Tests whether contract can be initialized. */
  @RealTest
  public void createContract() {
    // Initialize
    this.create(output -> output.write(WasmRealContractInvoker.INIT_HEADER));
    Assertions.assertThat(getContractState().getData()).isNotNull();
  }

  /** Stress-testing involving a whole lot of variables. */
  @RealTest(previous = "createContract")
  public void hugeHappyPath() {
    final int numVariablesPerRound = 1000;
    final int numRounds = 5;
    runManyVariablesTest(numVariablesPerRound, numRounds);
  }

  private void runManyVariablesTest(final int numVariablesPerRound, final int numRounds) {
    for (int round = 0; round < numRounds; round++) {
      logger.info("Starting round %d".formatted(round));
      sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE));
      Assertions.assertThat(getCalculationStatus())
          .as("Calculation Status")
          .isEqualTo(CalculationStatus.CALCULATING);
      executeComputation(new Random(132));
      Assertions.assertThat(getStateLoader().zkContractState().getPendingInputs())
          .as("Zk Pending inputs")
          .isEmpty();
      Assertions.assertThat(getVariables())
          .as("Zk Variables")
          .hasSize((round + 1) * numVariablesPerRound);
      Assertions.assertThat(getCalculationStatus())
          .as("Calculation Status")
          .isEqualTo(CalculationStatus.WAITING);
    }
  }
}
