package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import com.secata.util.BitHelper;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;

/** Tests for {@link WasmRealContract}. */
public abstract class AbstractWasmAverageSalaryTest extends BinaryRealContractTest {

  private final List<BlockchainAddress> users;
  private final List<Hash> transactions;

  private static final List<Integer> BITLENGTHS = List.of(32);

  /**
   * Loads contract file and creates an invoker for it.
   *
   * @param fileContract File to load. For this variant, it must be an {@code .zkwa} file.
   * @return Newly created invoker.
   */
  public static WasmRealContractInvoker loadInvoker(final String fileContract) {
    assert fileContract.endsWith(".zkwa");
    return loadInvoker(fileContract, null);
  }

  /**
   * Loads contract files and creates an invoker for it.
   *
   * @param fileContract Contract file to load.
   * @param fileComputation Computation/MC file to load. Might be null if {@code fileContract} is
   *     {@code .zkwa}.
   * @return Newly created invoker.
   */
  public static WasmRealContractInvoker loadInvoker(
      final String fileContract, final String fileComputation) {
    WasmRealContractInvoker.WasmContract wasmContractInfo;
    if (fileContract.endsWith(".zkwa")) {
      wasmContractInfo =
          WasmRealContractInvokerFactory.parseSectionedContractBytesToContract(
              fileContract, WasmExampleComputations.loadBytes(fileContract));
      if (fileComputation != null) {
        wasmContractInfo =
            wasmContractInfo.withAlternativeComputation(
                WasmExampleComputations.loadZkContractComputation(fileComputation));
      }
    } else {
      wasmContractInfo =
          new WasmRealContractInvoker.WasmContract(
              fileContract,
              WasmExampleComputations.loadZkContractWasm(fileContract),
              WasmExampleComputations.loadZkContractComputation(fileComputation));
    }

    return WasmRealContractInvoker.fromWasmBytes(wasmContractInfo);
  }

  /** Initialize test. */
  protected AbstractWasmAverageSalaryTest(final String fileContract, final String fileComputation) {
    super(loadInvoker(fileContract, fileComputation));

    users = IntStream.range(0, 100).mapToObj(this::getUser).toList();
    transactions = IntStream.range(0, 100).mapToObj(i -> Hash.create(s -> s.writeInt(i))).toList();
  }

  static final byte[] expandByteArrayToBitArray(final byte[] bytes) {
    if (bytes.length == 1) {
      return bytes;
    }
    final byte[] result = new byte[bytes.length * Byte.SIZE];
    int bitIndex = 0;
    for (final byte b : bytes) {
      for (int i = 0; i < Byte.SIZE; i++) {
        result[bitIndex + i] = (byte) (BitHelper.isLongBitSet(b, i) ? 1 : 0);
      }
      bitIndex += Byte.SIZE;
    }
    return result;
  }

  @Override
  protected final List<byte[]> asShares(List<Integer> values) {
    return super.asShares(values).stream()
        .map(WasmAverageSalaryTest::expandByteArrayToBitArray)
        .collect(Collectors.toList());
  }

  //// Action constants

  static final byte[] ACTION_START_COMPUTE = new byte[] {0x01};
  static final byte[] ACTION_ADD_SECRET = new byte[] {0x40};

  //// Tests

  private void addSalary(
      final BlockchainAddress sender, final Hash transaction, final int secretData) {
    sendSecretInput(
        transaction,
        asShares(List.of(makeNumber(secretData))),
        BITLENGTHS,
        sender,
        true,
        output -> output.write(ACTION_ADD_SECRET));
  }

  /** Tests creation of the average salary contract. */
  protected final void createContractInternal() {
    // Initialize
    this.create(output -> output.write(WasmRealContractInvoker.INIT_HEADER));
    Assertions.assertThat(getContractState().getData())
        .isEqualTo(
            Hex.decode(
                "000000000000000000000000000000000000123456" // Admin addr
                    + "00" // Average salary (not set yet)
                    + "00" // Number employees (not set yet)
                ));
  }

  /** A simple lifecycle of computing the average salary. */
  protected final void simpleHappyPathInternal() {
    // Add variables
    addSalary(users.get(0), transactions.get(0), 40000);
    addSalary(users.get(1), transactions.get(1), 45000);
    addSalary(users.get(2), transactions.get(2), 50000);

    // Confirm
    confirmSecretInput(transactions.get(0), users.get(0));
    confirmSecretInput(transactions.get(1), users.get(1));
    confirmSecretInput(transactions.get(2), users.get(2));

    // Administrator starts computation, which discards pending
    // Sum should give 135000
    sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE));

    // Perform computation
    executeComputation(new Random(132));

    // Open computation result
    resolveOpens();
  }

  /** A lifecycle of computing the average salary with many inputs. */
  protected final void hugeHappyPathInternal() {
    // Add variables
    for (int idx = 0; idx < users.size(); idx++) {
      final int salary = idx * 100;
      addSalary(users.get(idx), transactions.get(idx), salary);
      confirmSecretInput(transactions.get(idx), users.get(idx));
    }

    // Administrator starts computation, which discards pending
    // Sum should give users.size() * users.size() / 2
    sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE));

    // Perform computation
    executeComputation(new Random(132));

    // Open computation result
    resolveOpens();
  }

  /** A lifecycle of computing the average salary with multiple errors in between. */
  protected final void complexLifecycleInternal() {

    // Add first variable
    addSalary(users.get(4), transactions.get(1), 40000);

    // Try to start computation without enough variables; goes bad
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining("At least 3 employees")
        .hasMessageContaining("had only 0");

    // Add next three variables
    addSalary(users.get(2), transactions.get(2), 45000);
    addSalary(users.get(3), transactions.get(3), 50000);

    // Try to start computation before confirmations; goes bad
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining("At least 3 employees")
        .hasMessageContaining("had only 0");

    // Cannot add another variables
    Assertions.assertThatThrownBy(() -> addSalary(users.get(3), transactions.get(4), 52000))
        .hasMessageContaining("Each address is only allowed to send one salary variable");

    // Confirm
    confirmSecretInput(transactions.get(1), users.get(4));
    confirmSecretInput(transactions.get(2), users.get(2));
    confirmSecretInput(transactions.get(3), users.get(3));

    // Non-administrator tries to start computation; goes bad
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(users.get(4), out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining("Only administrator can start computation");

    // Add last; this will be discarded in next step
    addSalary(OWNER, transactions.get(4), 74000);

    // Administrator starts computation, which discards pending
    // Sum should give 135000
    sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE));

    // Administrator tries starts computation AGAIN, but fails as it's already
    // onging
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining("Computation must start from Waiting state")
        .hasMessageContaining("was Calculating");

    // Perform computation
    executeComputation(new Random(132));

    // Open computation result
    resolveOpens();
  }
}
