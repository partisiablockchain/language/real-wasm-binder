package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.contract.RealTest;
import org.assertj.core.api.Assertions;

/** Tests for {@link WasmRealContract}. */
public final class WasmAverageSalaryMalformedMcTest extends AbstractWasmAverageSalaryTest {

  /**
   * Filename of the compiled WASM for the tests.
   *
   * <p>Source code for the testing contract can be found in the rust-example-average-salary
   * example: {@code https://gitlab.com/partisiablockchain/language/contracts/zk-average-salary}.
   *
   * <p>Requires enabling the {@code plus_metadata} feature.
   */
  private static final String FILE_CONTRACT = "contracts/average_salary.zkwa";

  /** Hand-coded meta-circuit that instantly fails. */
  private static final String FILE_COMPUTATION =
      "contracts/average_salary_computation_malformed.mc";

  /** Initialize test. */
  public WasmAverageSalaryMalformedMcTest() {
    super(FILE_CONTRACT, FILE_COMPUTATION);
  }

  /** Tests creation of the average salary contract. */
  @RealTest
  public void createContract() {
    createContractInternal();
  }

  /** Tests lifecycle of the average salary contract. */
  @RealTest(previous = "createContract")
  public void simpleHappyPath() {
    Assertions.assertThatThrownBy(() -> simpleHappyPathInternal())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Could not load metadata for secret variable 103");
  }
}
