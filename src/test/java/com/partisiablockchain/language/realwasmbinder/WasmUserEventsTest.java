package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.zk.real.contract.RealTest;
import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** A ZK WASM contract that creates events. */
public final class WasmUserEventsTest extends BinaryRealContractTest {

  /**
   * Filename of the compiled WASM.
   *
   * <p>Source code for the testing contract can be found <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-zk-events">here</a>
   */
  private static final String ZK_CONTRACT_FILE = "contracts/testing_zk_events.wasm";

  static final byte[] ACTION_RETURN_INTERACTION = new byte[] {0x01};
  static final byte[] ACTION_RETURN_CALLBACK = new byte[] {0x02};
  static final byte[] ACTION_SECRET_INPUT = new byte[] {0x03};
  static final byte[] CALLBACK = new byte[] {0x05};
  static final byte[] ACTION_RETURN_VALUE = new byte[] {0x06};

  Hash transactionHash = Hash.create(s -> s.writeInt(1));

  /** Initialize test. */
  public WasmUserEventsTest() {
    super(
        WasmRealContractInvoker.fromWasmBytes(
            new WasmRealContractInvoker.WasmContract(
                ZK_CONTRACT_FILE,
                WasmExampleComputations.loadZkContractWasm(ZK_CONTRACT_FILE),
                WasmExampleComputations.COMPUTATION_CONSTANT_TRUE)));
  }

  /** Tests creation of the contract. */
  @RealTest
  public void createContract() {
    // Initialize
    this.create(output -> output.write(WasmRealContractInvoker.INIT_HEADER));
    assertThat(getResult()).isNull();
    assertThat(getRemoteCalls()).isNull();

    assertThat(getInteractions()).hasSize(1);
    BinderInteraction event = getInteractions().get(0);
    assertThat(event.contract).isEqualTo(createReceiverAddress("00"));
  }

  /** Testing event creation. */
  @RealTest(previous = "createContract")
  public void returnEvent() {
    sendOpenInput(OWNER, out -> out.write(ACTION_RETURN_INTERACTION));
    assertThat(getResult()).isNull();
    assertThat(getRemoteCalls()).isNull();

    assertThat(getInteractions()).hasSize(1);
    BinderInteraction event = getInteractions().get(0);
    assertThat(event.contract).isEqualTo(createReceiverAddress("01"));
  }

  /** Testing callbacks. */
  @RealTest(previous = "createContract")
  public void returnCallback() {
    sendOpenInput(OWNER, out -> out.write(ACTION_RETURN_CALLBACK));
    assertThat(getResult()).isNull();
    assertThat(getInteractions()).isEmpty();

    assertThat(getRemoteCalls().getEvents()).hasSize(1);
    BinderInteraction event = getRemoteCalls().getEvents().get(0);
    assertThat(event.contract).isEqualTo(createReceiverAddress("02"));

    assertThat(getRemoteCalls().getCallbackRpc()).isEqualTo(new byte[] {0});
  }

  /** Testing event creation during secret input. */
  @RealTest(previous = "createContract")
  public void onSecretInputEvent() {

    List<Integer> bitLengths = List.of(1);

    sendSecretInput(
        transactionHash,
        List.of(new byte[] {1}),
        bitLengths,
        OWNER,
        true,
        output -> output.write(ACTION_SECRET_INPUT));

    assertThat(getPendingInput(OWNER)).hasSize(1);
    assertThat(getResult()).isNull();
    assertThat(getRemoteCalls()).isNull();

    assertThat(getInteractions()).hasSize(1);
    BinderInteraction event = getInteractions().get(0);
    assertThat(event.contract).isEqualTo(createReceiverAddress("03"));
  }

  /**
   * Some automatic zk functions call executeWasm in the {@link WasmRealContractInvoker}. This test
   * checks that events are properly handled during this call.
   */
  @RealTest(previous = "onSecretInputEvent")
  public void executeWasmEvent() {
    confirmSecretInput(transactionHash, OWNER);

    assertThat(getPendingInput(OWNER)).hasSize(0);
    assertThat(getResult()).isNull();
    assertThat(getRemoteCalls()).isNull();

    assertThat(getInteractions()).hasSize(1);
    BinderInteraction event = getInteractions().get(0);
    assertThat(event.contract).isEqualTo(createReceiverAddress("04"));
  }

  /** Testing event creation during a callback. */
  @RealTest(previous = "createContract")
  public void callbackEvent() {
    // callback with no events in eventgroup
    callback(out -> out.write(CALLBACK), FixedList.create());

    assertThat(getResult()).isNull();
    assertThat(getRemoteCalls()).isNull();

    assertThat(getInteractions()).hasSize(1);
    BinderInteraction event = getInteractions().get(0);
    assertThat(event.contract).isEqualTo(createReceiverAddress("05"));
  }

  /** Testing action that returns a value. */
  @RealTest(previous = "createContract")
  public void returnValue() {
    sendOpenInput(OWNER, out -> out.write(ACTION_RETURN_VALUE));
    assertThat(getInteractions()).isEmpty();
    assertThat(getRemoteCalls()).isNull();

    assertThat(getResult()).isNotNull();
    int result =
        SafeDataInputStream.deserialize(SafeDataInputStream::readInt, getResult().getResult());
    assertThat(result).isEqualTo(6);
  }

  private BlockchainAddress createReceiverAddress(String id) {
    String addressString = "0200000000000000000000000000000000000000" + id;
    return BlockchainAddress.fromString(addressString);
  }
}
