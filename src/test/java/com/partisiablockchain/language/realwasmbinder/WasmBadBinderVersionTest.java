package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link WasmRealContractInvoker}. */
public final class WasmBadBinderVersionTest {

  private static final String ZK_CONTRACT_FILE =
      "contracts/secret_voting_contract_bad_version.wasm";

  @Test
  public void badVersion() {
    final var contract =
        new WasmRealContractInvoker.WasmContract(
            ZK_CONTRACT_FILE,
            WasmExampleComputations.loadZkContractWasm(ZK_CONTRACT_FILE),
            WasmExampleComputations.COMPUTATION_COUNT_VOTES);
    Assertions.assertThatCode(() -> WasmRealContractInvoker.fromWasmBytes(contract))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Unsupported binder ABI version 4.0.0 in contract"
                + " \"contracts/secret_voting_contract_bad_version.wasm\". Supported version is"
                + " 11.5.0");
  }
}
