package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.realwasmbinder.WasmRealContractInvoker.SECTION_ZK_STATE_UPDATE;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.evm.oracle.contract.EventLog;
import com.partisiablockchain.evm.oracle.contract.EventSubscription;
import com.partisiablockchain.language.wasminvoker.WasmInvoker;
import com.partisiablockchain.language.wasminvoker.WasmState;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.oracle.ConfirmedEventLog;
import com.partisiablockchain.oracle.CreateEvmEventSubscription;
import com.partisiablockchain.oracle.EvmEventLog;
import com.partisiablockchain.oracle.PendingHeartbeat;
import com.partisiablockchain.oracle.Topic;
import com.partisiablockchain.oracle.TopicList;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.binder.BinderTest;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.secata.stream.SafeDataOutputStream;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/** Tests for subscribing and unsubscribing to, and receiving, EVM event logs. */
public final class EvmEventsTest extends BinderTest {
  private WasmInvoker invoker;

  private static final EvmEventsTestData testData = EvmEventsTestData.generate();

  EvmEventsTest() {
    super();
  }

  @BeforeEach
  void setUp() {
    this.invoker = Mockito.mock(WasmInvoker.class);
    WasmRealContractInvoker.WasmContract contract =
        Mockito.mock(WasmRealContractInvoker.WasmContract.class);
    Mockito.when(contract.getComputation())
        .thenReturn(WasmRealContractBinderTest.COMPUTATION_DEFAULT);
    WasmRealContractInvoker contractInvoker = new WasmRealContractInvoker(invoker, contract);
    this.binder = new WasmRealContractBinder(contractInvoker);
  }

  @Test
  void addFirstEvmEventSubscription() {
    readState("real-binder-initial");

    CreateEvmEventSubscription firstSubscription = getSubscription(0);

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.subscribeToEvents(firstSubscription).build(invoker);

    invokeZkStateUpdates();

    Assertions.assertThat(currentState().getEventSubscriptions()).hasSize(1);
    EventSubscription actualSubscription = currentState().getEventSubscriptions().get(0);
    assertSubscription(actualSubscription, firstSubscription, 1);

    verifySerialization("real-binder-with-one-evm-event-subscription");
  }

  private static CreateEvmEventSubscription getSubscription(int index) {
    return testData.getTestData().get(index).subscription();
  }

  private void assertSubscription(
      EventSubscription actualSubscription,
      CreateEvmEventSubscription expectedData,
      int expectedId) {
    Assertions.assertThat(actualSubscription.getId()).isEqualTo(expectedId);
    Assertions.assertThat(actualSubscription.isCancelled()).isFalse();
    Assertions.assertThat(actualSubscription.getChainId())
        .isEqualToIgnoringCase(expectedData.chainId());
    Assertions.assertThat(actualSubscription.contractAddress())
        .isEqualTo(expectedData.contractAddress().asBytes());
    Assertions.assertThat(actualSubscription.fromBlock()).isEqualTo(expectedData.fromBlock());

    List<TopicList> expectedTopicFilters = expectedData.topics();
    List<List<byte[]>> actualTopicFilters = actualSubscription.topics();
    Assertions.assertThat(actualTopicFilters).hasSize(expectedTopicFilters.size());
    for (int i = 0; i < actualTopicFilters.size(); i++) {
      List<Topic> expectedFilter = expectedTopicFilters.get(i).topics();
      List<byte[]> expectedDecodedFilter = expectedFilter.stream().map(Topic::asBytes).toList();
      List<byte[]> actualFilter = actualTopicFilters.get(i);
      Assertions.assertThat(actualFilter).containsExactlyElementsOf(expectedDecodedFilter);
    }
  }

  @Test
  void addSecondEvmEventSubscription() {
    readState("real-binder-with-one-evm-event-subscription");

    CreateEvmEventSubscription secondSubscription = getSubscription(1);

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.subscribeToEvents(secondSubscription).build(invoker);

    invokeZkStateUpdates();

    Assertions.assertThat(currentState().getEventSubscriptions()).hasSize(2);
    EventSubscription actualSubscription = currentState().getEventSubscriptions().get(1);
    assertSubscription(actualSubscription, secondSubscription, 2);

    verifySerialization("real-binder-with-two-evm-event-subscriptions");
  }

  @Test
  void addAllEventSubscriptions() {
    readState("real-binder-initial");

    for (EvmEventsTestData.TestData testDatum : testData.getTestData()) {
      CreateEvmEventSubscription subscription = testDatum.subscription();
      ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
      builder.subscribeToEvents(subscription).build(invoker);
      invokeZkStateUpdates();
    }

    Assertions.assertThat(currentState().getEventSubscriptions())
        .hasSize(testData.getTestData().size());
    verifySerialization("real-binder-with-many-evm-event-subscriptions");
  }

  @Test
  void cancelFirstEvmEventSubscription() {
    readState("real-binder-with-two-evm-event-subscriptions");

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.unsubscribeFromEvents(1).build(invoker);

    invokeZkStateUpdatesWithoutFees();

    Assertions.assertThat(currentState().getEventSubscriptions()).hasSize(2);
    EventSubscription first = currentState().getEventSubscriptions().get(0);
    Assertions.assertThat(first.isCancelled()).isTrue();

    EventSubscription second = currentState().getEventSubscriptions().get(1);
    Assertions.assertThat(second.isCancelled()).isFalse();

    verifySerialization("real-binder-with-one-cancelled-event-subscription");
  }

  @Test
  void cancelSecondEvmEventSubscription() {
    readState("real-binder-with-two-evm-event-subscriptions");

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.unsubscribeFromEvents(2).build(invoker);

    invokeZkStateUpdatesWithoutFees();

    Assertions.assertThat(currentState().getEventSubscriptions()).hasSize(2);
    EventSubscription first = currentState().getEventSubscriptions().get(0);
    Assertions.assertThat(first.isCancelled()).isFalse();

    EventSubscription second = currentState().getEventSubscriptions().get(1);
    Assertions.assertThat(second.isCancelled()).isTrue();
  }

  @Test
  void cancelNonExistingEvmEventSubscription() {
    readState("real-binder-with-two-evm-event-subscriptions");

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.unsubscribeFromEvents(3).build(invoker);

    invokeZkStateUpdatesWithoutFees();

    Assertions.assertThat(currentState().getEventSubscriptions()).hasSize(2);
    EventSubscription first = currentState().getEventSubscriptions().get(0);
    Assertions.assertThat(first.isCancelled()).isFalse();

    EventSubscription second = currentState().getEventSubscriptions().get(1);
    Assertions.assertThat(second.isCancelled()).isFalse();
  }

  /// Testing positive cases for adding new external events

  @Test
  void addSinglePendingExternalEvent() {
    readState("real-binder-with-one-evm-event-subscription");

    List<EvmEventLog> events = getEvents(0);
    EvmEventLog observedLog = events.get(0);

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }

    invoke("invoke?", ADDRESSES.get(0), 34L, serializeAddExternalEvent(1, observedLog))
        .verifyNoInteractionsOrFees();

    EvmEventLog actualCandidate =
        currentState().getExternalEvents().getPendingEventForSubscription(1).candidateFor(0);
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();
    Assertions.assertThat(actualCandidate).isEqualTo(observedLog);

    verifySerialization("real-binder-with-pending-event-with-one-confirmation");
  }

  private List<EvmEventLog> getEvents(int index) {
    return testData.getTestData().get(index).events();
  }

  @Test
  void confirmSinglePendingExternalEvent() {
    readState("real-binder-with-pending-event-with-one-confirmation");

    List<EvmEventLog> events = getEvents(0);
    EvmEventLog observedLog = events.get(0);
    invoke("invoke?", ADDRESSES.get(1), 34L, serializeAddExternalEvent(1, observedLog))
        .verifyNoInteractionsOrFees();
    InteractionsAndResult selfInvocation =
        invoke("invoke?", ADDRESSES.get(2), 34L, serializeAddExternalEvent(1, observedLog))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);

    ArgumentCaptor<byte[]> rpcCaptor = ArgumentCaptor.forClass(byte[].class);
    byte[] wasmState = {42};
    mockOnExternalEvent(rpcCaptor, wasmState);

    invokeSelf(selfInvocation.singleInteraction(), 35L)
        .verifyShortName(ON_EXTERNAL_EVENT)
        .verifyNoInteractions();

    byte[] expectedHookInput =
        WasmRealContractInvoker.serializeInput(
            currentState(),
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeInt(1); // subscriptionId
                  s.writeInt(1); // eventId
                }));
    Assertions.assertThat(rpcCaptor.getValue()).isEqualTo(expectedHookInput);
    Assertions.assertThat(currentState().getOpenState().getData()).isEqualTo(wasmState);

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    List<ConfirmedEventLog> confirmedEventLogs = currentState().getConfirmedEventLogsRaw(1);
    Assertions.assertThat(confirmedEventLogs).hasSize(1);
    ConfirmedEventLog actualEvent = confirmedEventLogs.get(0);
    Assertions.assertThat(actualEvent.eventId()).isEqualTo(1);
    Assertions.assertThat(actualEvent.subscriptionId()).isEqualTo(1);
    assertEventLogs(actualEvent, observedLog);

    verifySerialization("real-binder-with-one-confirmed-event");
  }

  private void mockOnExternalEvent(ArgumentCaptor<byte[]> bytesCaptor, byte[] wasmDataData) {
    String hookName = "zk_on_external_event";
    Mockito.when(invoker.doesFunctionExist(hookName)).thenReturn(true);
    WasmState wasmState = new WasmState().withState(wasmDataData);
    Mockito.when(
            invoker.executeWasm(
                Mockito.anyLong(),
                Mockito.anyString(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                bytesCaptor.capture()))
        .thenReturn(new WasmInvoker.WasmResult(wasmState, EventResult.create(), 0L, List.of()));
  }

  @Test
  void addingPendingEventsForBothSubscriptions() {
    readState("real-binder-with-two-evm-event-subscriptions");

    EvmEventLog firstEvent = getEvents(0).get(0);
    EvmEventLog secondEvent = getEvents(1).get(0);

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
      Assertions.assertThat(currentState().hasPushedNextEventLog(2, i)).isFalse();
    }

    invoke("invoke?", ADDRESSES.get(0), 34L, serializeAddExternalEvent(1, firstEvent))
        .verifyNoInteractionsOrFees();
    invoke("invoke?", ADDRESSES.get(3), 34L, serializeAddExternalEvent(2, secondEvent))
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();

    verifySerialization("real-binder-with-two-pending-events");
  }

  @Test
  void confirmingMultipleEventsForBothSubscriptions() {
    readState("real-binder-with-two-pending-events");
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(1)).isNull();
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(2)).isNull();

    EvmEventLog firstEvent = getEvents(0).get(0);
    EvmEventLog secondEvent = getEvents(1).get(0);

    // Confirm already existing pending events
    invoke("invoke?", ADDRESSES.get(1), 34L, serializeAddExternalEvent(1, firstEvent))
        .verifyNoInteractionsOrFees();
    InteractionsAndResult selfInvocation =
        invoke("invoke?", ADDRESSES.get(3), 34L, serializeAddExternalEvent(1, firstEvent))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);
    invokeSelf(selfInvocation.singleInteraction(), 35L)
        .verifyShortName(ON_EXTERNAL_EVENT)
        .verifyNoInteractions();
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(1))
        .isEqualTo(firstEvent.getMetadata());

    invoke("invoke?", ADDRESSES.get(2), 34L, serializeAddExternalEvent(2, secondEvent))
        .verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().hasPushedNextEventLog(2, 0)).isFalse();
    selfInvocation =
        invoke("invoke?", ADDRESSES.get(0), 34L, serializeAddExternalEvent(2, secondEvent))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);
    invokeSelf(selfInvocation.singleInteraction(), 35L)
        .verifyShortName(ON_EXTERNAL_EVENT)
        .verifyNoInteractions();
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(2))
        .isEqualTo(secondEvent.getMetadata());

    // Add and confirm more events
    EvmEventLog thirdEvent = getEvents(0).get(1);
    EvmEventLog fourthEvent = getEvents(1).get(1);

    invoke("invoke?", ADDRESSES.get(1), 34L, serializeAddExternalEvent(1, thirdEvent))
        .verifyNoInteractionsOrFees();
    invoke("invoke?", ADDRESSES.get(0), 34L, serializeAddExternalEvent(1, thirdEvent))
        .verifyNoInteractionsOrFees();
    invoke("invoke?", ADDRESSES.get(0), 34L, serializeAddExternalEvent(2, fourthEvent))
        .verifyNoInteractionsOrFees();
    selfInvocation =
        invoke("invoke?", ADDRESSES.get(2), 34L, serializeAddExternalEvent(1, thirdEvent))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);
    invokeSelf(selfInvocation.singleInteraction(), 35L)
        .verifyShortName(ON_EXTERNAL_EVENT)
        .verifyNoInteractions();

    invoke("invoke?", ADDRESSES.get(1), 34L, serializeAddExternalEvent(2, fourthEvent))
        .verifyNoInteractionsOrFees();
    selfInvocation =
        invoke("invoke?", ADDRESSES.get(3), 34L, serializeAddExternalEvent(2, fourthEvent))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);
    invokeSelf(selfInvocation.singleInteraction(), 35L)
        .verifyShortName(ON_EXTERNAL_EVENT)
        .verifyNoInteractions();
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(1))
        .isEqualTo(thirdEvent.getMetadata());
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(2))
        .isEqualTo(fourthEvent.getMetadata());

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
      Assertions.assertThat(currentState().hasPushedNextEventLog(2, i)).isFalse();
    }

    Assertions.assertThat(getEventLogs(1)).hasSize(2);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);

    verifySerialization("real-binder-with-two-subscriptions-and-four-events");
  }

  @Test
  void deletingEmptyEventsListDoesNothing() {
    readState("real-binder-with-two-subscriptions-and-four-events");

    Assertions.assertThat(getEventLogs(1)).hasSize(2);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.deleteEvents(List.of()).build(invoker);
    invokeZkStateUpdatesWithoutFees();

    Assertions.assertThat(getEventLogs(1)).hasSize(2);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);
  }

  @Test
  void deletingNonExistingEventDoesNothing() {
    readState("real-binder-with-two-subscriptions-and-four-events");

    Assertions.assertThat(getEventLogs(1)).hasSize(2);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.deleteEvents(List.of(5)).build(invoker);
    invokeZkStateUpdatesWithoutFees();

    Assertions.assertThat(getEventLogs(1)).hasSize(2);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);
  }

  @Test
  void deletingSingleExternalEvent() {
    readState("real-binder-with-two-subscriptions-and-four-events");

    Assertions.assertThat(getEventLogs(1)).hasSize(2);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.deleteEvents(List.of(1)).build(invoker);
    invokeZkStateUpdatesWithoutFees();

    Assertions.assertThat(getEventLogs(1)).hasSize(1);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);
  }

  @Test
  void deletingMultipleExternalEvent() {
    readState("real-binder-with-two-subscriptions-and-four-events");

    Assertions.assertThat(getEventLogs(1)).hasSize(2);
    Assertions.assertThat(getEventLogs(2)).hasSize(2);
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(1))
        .isEqualTo(getEvents(0).get(1).getMetadata());
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(2))
        .isEqualTo(getEvents(1).get(1).getMetadata());

    ZkStateChangeBuilder builder = new ZkStateChangeBuilder();
    builder.deleteEvents(List.of(1, 3, 2, 4)).build(invoker);
    invokeZkStateUpdatesWithoutFees();

    Assertions.assertThat(getEventLogs(1)).hasSize(0);
    Assertions.assertThat(getEventLogs(2)).hasSize(0);

    // Assert that latest confirmed is not deleted
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(1))
        .isEqualTo(getEvents(0).get(1).getMetadata());
    Assertions.assertThat(currentState().getLatestConfirmedEventLog(2))
        .isEqualTo(getEvents(1).get(1).getMetadata());
  }

  @Test
  @EnabledIfEnvironmentVariable(named = "CI", matches = "true", disabledReason = "Slow test")
  void confirmManyEvents() {
    readState("real-binder-with-many-evm-event-subscriptions");

    for (int subId = 1; subId <= testData.getTestData().size(); subId += 4) {
      List<EvmEventLog> events = testData.getTestData().get(0).events();
      for (EvmEventLog event : events) {
        invoke("invoke?", ADDRESSES.get(0), 34L + subId, serializeAddExternalEvent(subId, event))
            .verifyNoInteractionsOrFees();
        invoke("invoke?", ADDRESSES.get(1), 34L + 34L, serializeAddExternalEvent(subId, event))
            .verifyNoInteractionsOrFees();
        InteractionsAndResult selfInvocation =
            invoke("invoke?", ADDRESSES.get(2), 34L + 34L, serializeAddExternalEvent(subId, event))
                .verifyInteractWithSelf()
                .verifyZkFees(25_000L);
        invokeSelf(selfInvocation.singleInteraction(), 34L + subId)
            .verifyShortName(ON_EXTERNAL_EVENT)
            .verifyNoInteractions();
      }
    }
    verifySerialization("real-binder-with-many-evm-events");
  }

  @Test
  void duplicatedEventIsNotAddedAgain() {
    readState("real-binder-with-one-confirmed-event");

    EvmEventLog event = getEvents(0).get(0);

    Assertions.assertThatThrownBy(
            () -> invoke("invoke?", ADDRESSES.get(0), 34L, serializeAddExternalEvent(1, event)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Cannot add event [blockNumber=3475142, transactionIndex=0, logIndex=0] that is"
                + " chronologically the same or older that latest confirmed [blockNumber=3475142,"
                + " transactionIndex=0, logIndex=0] for subscription 1");

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);
  }

  /// Testing negative cases for adding new external events

  @Test
  void oracleMembersCannotCallOnExternalEventDirectly() {
    readState("real-binder-with-pending-event-with-one-confirmation");
    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(0),
                    55L,
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeByte(ON_EXTERNAL_EVENT);
                          s.writeInt(1);
                          s.writeInt(1);
                        })))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Invocation can only be executed by the contract itself. Caller was"
                + " BlockchainAddress[identifier=000000000000000000000000000000000000000022],"
                + " contract address is"
                + " BlockchainAddress[identifier=01a000000000000000000000000000000000000001]");
  }

  @Test
  void badEvmOracleMemberCannotRejectPendingEventWithModifiedData() {
    readState("real-binder-with-pending-event-with-one-confirmation");

    // Bad actor tries to "reject" and fails, i.e. event is still pending
    EvmEventLog observedLog = getEvents(0).get(0);
    EvmEventLog modifiedDataEvent = modifyData(observedLog);
    Assertions.assertThat(observedLog).isNotEqualTo(modifiedDataEvent);

    invoke("invoke?", ADDRESSES.get(1), 35L, serializeAddExternalEvent(1, modifiedDataEvent))
        .verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 1)).isTrue();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 2)).isFalse();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 3)).isFalse();

    // The last two oracles confirm the original event
    Assertions.assertThat(getEventLogs(1)).hasSize(0);
    invoke("invoke?", ADDRESSES.get(2), 34L, serializeAddExternalEvent(1, observedLog))
        .verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 2)).isTrue();
    InteractionsAndResult selfInvocation =
        invoke("invoke?", ADDRESSES.get(3), 34L, serializeAddExternalEvent(1, observedLog))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);
    invokeSelf(selfInvocation.singleInteraction(), 35L).verifyShortName(ON_EXTERNAL_EVENT);

    // Has pushed is reset
    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);

    ConfirmedEventLog actualEvent = currentState().getConfirmedEventLogsRaw(1).get(0);
    assertEventLogs(actualEvent, observedLog);
  }

  private EvmEventLog modifyData(EvmEventLog base) {
    byte[] data = Arrays.copyOf(base.data(), base.data().length);
    ByteBuffer bytes = ByteBuffer.wrap(data);
    if (data.length == 0) {
      bytes = ByteBuffer.wrap(new byte[] {1});
    } else {
      int flipped = data[data.length - 1] ^ 1;
      bytes.put(data.length - 1, (byte) flipped);
    }
    return new EvmEventLog(base.getMetadata(), new LargeByteArray(bytes.array()), base.topics());
  }

  private EvmEventLog modifyTopic(EvmEventLog base) {
    List<Topic> topics = new ArrayList<>(base.topics().topics());
    byte[] topicBytes = Arrays.copyOf(topics.get(0).asBytes(), topics.get(0).asBytes().length);
    ByteBuffer modifiedBytes = ByteBuffer.wrap(topicBytes);
    int flipped = topicBytes[topicBytes.length - 1] ^ 1;
    modifiedBytes.put(topicBytes.length - 1, (byte) flipped);
    Topic modifiedTopic = Topic.fromBytes(modifiedBytes.array().clone());
    topics.set(0, modifiedTopic);
    return new EvmEventLog(
        base.getMetadata(), new LargeByteArray(base.data()), TopicList.fromTopics(topics));
  }

  @Test
  void badEvmOracleMemberCannotRejectPendingEventWithModifiedTopic() {
    readState("real-binder-with-pending-event-with-one-confirmation");
    EvmEventLog observedLog = getEvents(0).get(0);
    invoke("invoke?", ADDRESSES.get(2), 34L, serializeAddExternalEvent(1, observedLog))
        .verifyNoInteractionsOrFees();

    EvmEventLog modifiedTopicEvent = modifyTopic(observedLog);

    // Bad actor tries to "reject" and fails, i.e. event is still pending
    invoke("invoke?", ADDRESSES.get(1), 35L, serializeAddExternalEvent(1, modifiedTopicEvent))
        .verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 1)).isTrue();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 2)).isTrue();

    // The last oracle confirm the original event
    InteractionsAndResult selfInvocation =
        invoke("invoke?", ADDRESSES.get(3), 55L, serializeAddExternalEvent(1, observedLog))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);
    invokeSelf(selfInvocation.singleInteraction(), 65L).verifyShortName(ON_EXTERNAL_EVENT);

    // Has pushed is reset
    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);

    ConfirmedEventLog actualEvent = currentState().getConfirmedEventLogsRaw(1).get(0);
    assertEventLogs(actualEvent, observedLog);
  }

  @Test
  void badEvmOracleMemberCannotAlterContentOfIncomingEvent() {

    readState("real-binder-with-one-evm-event-subscription");

    EvmEventLog observedEvent = getEvents(0).get(0);
    EvmEventLog maliciousEvent = modifyData(observedEvent);
    invoke("invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(1, maliciousEvent))
        .verifyNoInteractionsOrFees();

    confirmAndAssertEvent(observedEvent);
  }

  private void confirmAndAssertEvent(EvmEventLog observedEvent) {
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();
    Assertions.assertThat(getEventLogs(1)).isEmpty();

    invoke("invoke?", ADDRESSES.get(1), 36L, serializeAddExternalEvent(1, observedEvent))
        .verifyNoInteractionsOrFees();
    invoke("invoke?", ADDRESSES.get(2), 37L, serializeAddExternalEvent(1, observedEvent))
        .verifyNoInteractionsOrFees();
    InteractionsAndResult selfInvocation =
        invoke("invoke?", ADDRESSES.get(3), 38L, serializeAddExternalEvent(1, observedEvent))
            .verifyInteractWithSelf()
            .verifyZkFees(25_000L);
    invokeSelf(selfInvocation.singleInteraction(), 35L)
        .verifyShortName(ON_EXTERNAL_EVENT)
        .verifyNoInteractions();

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);

    ConfirmedEventLog confirmedEvent = currentState().getConfirmedEventLogsRaw(1).get(0);
    assertEventLogs(confirmedEvent, observedEvent);
  }

  @Test
  void badEvmOracleMemberCannotSkipEventsInTheEventQueue() {
    readState("real-binder-with-one-evm-event-subscription");

    EvmEventLog observedEvent = getEvents(0).get(0);
    EvmEventLog skippedEvent = getEvents(0).get(4);
    invoke("invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(1, skippedEvent))
        .verifyNoInteractionsOrFees();

    confirmAndAssertEvent(observedEvent);
  }

  @Test
  void badEvmOracleMemberCannotReplayOldEvents() {
    readState("real-binder-with-one-confirmed-event");

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);

    List<EvmEventLog> events = getEvents(0);
    EvmEventLog eventToReplay = events.get(0);
    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(1, eventToReplay)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Cannot add event [blockNumber=3475142, transactionIndex=0, logIndex=0] that is"
                + " chronologically the same or older that latest confirmed [blockNumber=3475142,"
                + " transactionIndex=0, logIndex=0] for subscription 1");

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);
  }

  @Test
  void badEvmOracleCannotReplayOldEventWithAlteredData() {
    readState("real-binder-with-one-confirmed-event");

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);

    List<EvmEventLog> events = getEvents(0);
    EvmEventLog eventToReplay = modifyData(events.get(0));
    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(1, eventToReplay)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Cannot add event [blockNumber=3475142, transactionIndex=0, logIndex=0] that is"
                + " chronologically the same or older that latest confirmed [blockNumber=3475142,"
                + " transactionIndex=0, logIndex=0] for subscription 1");

    for (int i = 0; i < 4; i++) {
      Assertions.assertThat(currentState().hasPushedNextEventLog(1, i)).isFalse();
    }
    Assertions.assertThat(getEventLogs(1)).hasSize(1);
  }

  @Test
  void onlyEvmOracleMembersCanAddPendingExternalEvent() {
    readState("real-binder-with-one-evm-event-subscription");
    EvmEventLog observedEvent = getEvents(0).get(0);
    EvmEventLog event = modifyData(observedEvent);
    Assertions.assertThatThrownBy(
            () -> invoke("invoke?", ACCOUNT_FOUR, 35L, serializeAddExternalEvent(1, event)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Only engines allowed to call");
  }

  @Test
  void singleOracleCannotAddTwoDifferentCandidatesToPendingEvent() {
    readState("real-binder-with-one-evm-event-subscription");

    EvmEventLog firstCandidate = getEvents(0).get(0);
    EvmEventLog secondCandidate = getEvents(0).get(1);
    invoke("invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(1, firstCandidate))
        .verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();
    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(0),
                    35L,
                    serializeAddExternalEvent(1, secondCandidate)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Event [blockNumber=3475142, transactionIndex=0, logIndex=4] already added to"
                + " subscription 1 by oracle 0");

    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 1)).isFalse();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 2)).isFalse();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 3)).isFalse();
    Assertions.assertThat(getEventLogs(1)).isEmpty();
  }

  @Test
  void cannotSendEventToCancelledSubscription() {
    readState("real-binder-with-one-cancelled-event-subscription");

    EvmEventLog event = getEvents(0).get(0);
    Assertions.assertThatThrownBy(
            () -> invoke("invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(1, event)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot add events to cancelled or non-existing subscription 1");
  }

  @Test
  void cannotSendEventNonExistingSubscriptions() {
    readState("real-binder-with-two-evm-event-subscriptions");

    EvmEventLog event = getEvents(0).get(0);
    Assertions.assertThatThrownBy(
            () -> invoke("invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(-1, event)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot add events to cancelled or non-existing subscription -1");
    Assertions.assertThatThrownBy(
            () -> invoke("invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(0, event)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot add events to cancelled or non-existing subscription 0");
    Assertions.assertThatThrownBy(
            () -> invoke("invoke?", ADDRESSES.get(0), 35L, serializeAddExternalEvent(3, event)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot add events to cancelled or non-existing subscription 3");
  }

  /**
   * An EVM oracle node can register a heartbeat block to indicate that there have been no event for
   * some amount of blocks since the last event or heartbeat.
   *
   * <p><em>Note:</em> The binder does not make any assumptions about many blocks have passed since
   * the last finalized event (either event log or heartbeat block). Assuming that a majority of the
   * oracles are honest, they will be able to agree on the same heartbeat block when running the
   * same real protocol.
   */
  @Test
  void registerHeartbeatBlock() {
    readState("real-binder-with-one-evm-event-subscription");
    int subscriptionIndex = 0;
    int subscriptionId = subscriptionIndex + 1;

    CreateEvmEventSubscription subscription = getSubscription(subscriptionIndex);
    Unsigned256 heartbeat = subscription.fromBlock().add(Unsigned256.create(1));

    PendingHeartbeat initialPending =
        currentState().getExternalEvents().getPendingHeartbeat(subscriptionId);
    Assertions.assertThat(initialPending).isNull();
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isFalse();

    registerHeartbeat(ADDRESSES.get(0), subscriptionId, heartbeat);
    assertPendingHeartbeat(1, heartbeat, null, null, null);
    Assertions.assertThat(currentState().hasPushedNextEventLog(1, 0)).isTrue();

    verifySerialization("real-binder-with-one-pending-heartbeat-block");
  }

  private void registerHeartbeat(
      BlockchainAddress evmOracle, int subscriptionId, Unsigned256 heartbeatBlock) {
    InteractionsAndResult invoke =
        invoke(
            "invoke?",
            evmOracle,
            35L,
            serializeRegisterExternalHeartbeatBlock(subscriptionId, heartbeatBlock));
    invoke.verifyNoInteractionsOrFees();
  }

  private void confirmHeartbeat(
      BlockchainAddress evmOracle, int subscriptionId, Unsigned256 heartbeatBlock) {
    InteractionsAndResult invoke =
        invoke(
            "invoke?",
            evmOracle,
            35L,
            serializeRegisterExternalHeartbeatBlock(subscriptionId, heartbeatBlock));
    invoke.verifyNoInteractions().verifyZkFees(25_000L);
  }

  private void assertPendingHeartbeat(
      int subscriptionId,
      Unsigned256 firstCandidate,
      Unsigned256 secondCandidate,
      Unsigned256 thirdCandidate,
      Unsigned256 fourthCandidate) {
    PendingHeartbeat pending =
        currentState().getExternalEvents().getPendingHeartbeat(subscriptionId);
    Assertions.assertThat(pending.candidateFor(0)).isEqualTo(firstCandidate);
    Assertions.assertThat(pending.candidateFor(1)).isEqualTo(secondCandidate);
    Assertions.assertThat(pending.candidateFor(2)).isEqualTo(thirdCandidate);
    Assertions.assertThat(pending.candidateFor(3)).isEqualTo(fourthCandidate);

    Assertions.assertThat(currentState().hasPushedNextEventLog(subscriptionId, 0))
        .isEqualTo(firstCandidate != null);
    Assertions.assertThat(currentState().hasPushedNextEventLog(subscriptionId, 1))
        .isEqualTo(secondCandidate != null);
    Assertions.assertThat(currentState().hasPushedNextEventLog(subscriptionId, 2))
        .isEqualTo(thirdCandidate != null);
    Assertions.assertThat(currentState().hasPushedNextEventLog(subscriptionId, 3))
        .isEqualTo(fourthCandidate != null);
  }

  /**
   * When three of the EVM oracles have confirmed the same heartbeat block it is registered as the
   * latest confirmed heartbeat and all nodes are compensated for their work.
   */
  @Test
  void confirmHeartbeatBlock() {
    readState("real-binder-with-one-pending-heartbeat-block");
    int subscriptionIndex = 0;
    int subscriptionId = subscriptionIndex + 1;

    Unsigned256 heartbeatToConfirm =
        currentState().getExternalEvents().getPendingHeartbeat(subscriptionId).candidateFor(0);

    EventLog initialEvent =
        currentState().getExternalEvents().getLatestConfirmedForSubscription(subscriptionId);
    Assertions.assertThat(initialEvent).isNull();

    registerHeartbeat(ADDRESSES.get(1), subscriptionId, heartbeatToConfirm);
    assertPendingHeartbeat(1, heartbeatToConfirm, heartbeatToConfirm, null, null);
    confirmHeartbeat(ADDRESSES.get(2), subscriptionId, heartbeatToConfirm);

    PendingHeartbeat pending =
        currentState().getExternalEvents().getPendingHeartbeat(subscriptionId);
    Assertions.assertThat(pending).isNull();

    Unsigned256 confirmed =
        currentState()
            .getExternalEvents()
            .getLatestConfirmedForSubscription(subscriptionId)
            .blockNumber();
    Assertions.assertThat(confirmed).isEqualTo(heartbeatToConfirm);
    verifySerialization("real-binder-with-one-confirmed-heartbeat-block");
  }

  /**
   * An EVM oracle can register a second heartbeat if it occurs after the last confirmed heartbeat.
   */
  @Test
  void registerSecondHeartbeatAfterFirstConfirmedHeartBeat() {
    readState("real-binder-with-one-confirmed-heartbeat-block");

    int subscriptionId = 1;
    Unsigned256 heartbeat =
        currentState()
            .getExternalEvents()
            .getLatestConfirmedForSubscription(subscriptionId)
            .blockNumber()
            .add(Unsigned256.create(1));

    registerHeartbeat(ADDRESSES.get(3), subscriptionId, heartbeat);
    assertPendingHeartbeat(1, null, null, null, heartbeat);

    registerHeartbeat(ADDRESSES.get(2), subscriptionId, heartbeat);
    assertPendingHeartbeat(1, null, null, heartbeat, heartbeat);

    confirmHeartbeat(ADDRESSES.get(0), subscriptionId, heartbeat);
    Unsigned256 confirmed =
        currentState()
            .getExternalEvents()
            .getLatestConfirmedForSubscription(subscriptionId)
            .blockNumber();
    Assertions.assertThat(confirmed).isEqualTo(heartbeat);
  }

  /** An EVM oracle can register a heartbeat if it occurs after the latest confirmed event. */
  @Test
  void registerHeartbeatAfterConfirmedEvent() {
    readState("real-binder-with-one-confirmed-event");

    int subscriptionId = 1;
    Unsigned256 heartbeat =
        currentState()
            .getExternalEvents()
            .getLatestConfirmedForSubscription(subscriptionId)
            .blockNumber()
            .add(Unsigned256.create(1));

    registerHeartbeat(ADDRESSES.get(1), subscriptionId, heartbeat);
    assertPendingHeartbeat(1, null, heartbeat, null, null);

    registerHeartbeat(ADDRESSES.get(3), subscriptionId, heartbeat);
    assertPendingHeartbeat(1, null, heartbeat, null, heartbeat);

    confirmHeartbeat(ADDRESSES.get(2), subscriptionId, heartbeat);
    Unsigned256 confirmed =
        currentState()
            .getExternalEvents()
            .getLatestConfirmedForSubscription(subscriptionId)
            .blockNumber();
    Assertions.assertThat(confirmed).isEqualTo(heartbeat);
  }

  /**
   * An EVM oracle cannot register a new heartbeat on a subscription until the last one it
   * registered has been confirmed or rejected.
   */
  @Test
  void cannotRegisterMultipleHeartbeats() {
    readState("real-binder-with-one-pending-heartbeat-block");
    int subscriptionId = 1;

    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(0),
                    35L,
                    serializeRegisterExternalHeartbeatBlock(subscriptionId, Unsigned256.ONE)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Pending heartbeat block already registered for subscription 1 by oracle 0");
  }

  /**
   * It is not possible for an oracle node to register a heartbeat block that occurs in the same
   * block or before the last confirmed heartbeat.
   *
   * <p><em>Note:</em> Heartbeat registrations for past blocks can be assumed to be sent by an
   * oracle if it is slightly behind the latest block. They can safely be ignored.
   */
  @Test
  void heartbeatBlockRegisteredBeforeOrSameAsLastHeartbeatAreIgnored() {
    readState("real-binder-with-one-confirmed-heartbeat-block");

    int subscriptionId = 1;
    Assertions.assertThat(currentState().getExternalEvents().getPendingHeartbeat(subscriptionId))
        .isNull();

    Unsigned256 sameBlock =
        currentState()
            .getExternalEvents()
            .getLatestConfirmedForSubscription(subscriptionId)
            .blockNumber();
    Assertions.assertThatNoException()
        .isThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(1),
                    35L,
                    serializeRegisterExternalHeartbeatBlock(subscriptionId, sameBlock)));

    Assertions.assertThat(currentState().getExternalEvents().getPendingHeartbeat(subscriptionId))
        .isNull();
    verifySerialization("real-binder-with-one-confirmed-heartbeat-block");

    Unsigned256 previousBlock = sameBlock.subtract(Unsigned256.ONE);
    Assertions.assertThatNoException()
        .isThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(1),
                    35L,
                    serializeRegisterExternalHeartbeatBlock(subscriptionId, previousBlock)));

    Assertions.assertThat(currentState().getExternalEvents().getPendingHeartbeat(subscriptionId))
        .isNull();
    verifySerialization("real-binder-with-one-confirmed-heartbeat-block");
  }

  /**
   * It is not possible for an oracle node to register a heartbeat block that occurs in the same
   * block or before the last confirmed event.
   *
   * <p><em>Note:</em> Heartbeat registrations for past blocks can be assumed to be sent by an
   * oracle if it is slightly behind the latest block. They can safely be ignored.
   */
  @Test
  void heartbeatBlockRegisteredBeforeOrSameAsLastEventAreIgnored() {
    readState("real-binder-with-one-confirmed-event");

    int subscriptionId = 1;
    Unsigned256 sameBlock =
        currentState()
            .getExternalEvents()
            .getLatestConfirmedForSubscription(subscriptionId)
            .blockNumber();

    Assertions.assertThatNoException()
        .isThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(2),
                    35L,
                    serializeRegisterExternalHeartbeatBlock(subscriptionId, sameBlock)));
    Assertions.assertThat(currentState().getExternalEvents().getPendingHeartbeat(subscriptionId))
        .isNull();
    verifySerialization("real-binder-with-one-confirmed-event");

    Unsigned256 previousBlock = sameBlock.subtract(Unsigned256.ONE);
    Assertions.assertThatNoException()
        .isThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(1),
                    35L,
                    serializeRegisterExternalHeartbeatBlock(subscriptionId, previousBlock)));
    Assertions.assertThat(currentState().getExternalEvents().getPendingHeartbeat(subscriptionId))
        .isNull();
    verifySerialization("real-binder-with-one-confirmed-event");
  }

  /** It is only the EVM oracle nodes that are allowed to register heartbeat blocks. */
  @Test
  void nonEvmOracleNodesCannotRegisterHeartbeats() {
    readState("real-binder-with-one-evm-event-subscription");
    int subscriptionIndex = 0;
    int subscriptionId = subscriptionIndex + 1;

    CreateEvmEventSubscription subscription = getSubscription(subscriptionIndex);
    Unsigned256 heartbeat = subscription.fromBlock().add(Unsigned256.create(1));

    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ACCOUNT_FOUR,
                    35L,
                    serializeRegisterExternalHeartbeatBlock(subscriptionId, heartbeat)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Only engines allowed to call");
  }

  /**
   * It is not possible for an EVM oracle node to register heartbeat blocks for non-existing
   * subscriptions.
   */
  @Test
  void cannotRegisterHeartbeatForNonExistingSubscriptions() {
    readState("real-binder-with-one-evm-event-subscription");
    CreateEvmEventSubscription subscription = getSubscription(0);
    Unsigned256 heartbeat = subscription.fromBlock().add(Unsigned256.create(1));

    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(0),
                    35L,
                    serializeRegisterExternalHeartbeatBlock(-1, heartbeat)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot add events to cancelled or non-existing subscription -1");
  }

  /**
   * It is not possible for an EVM oracle node to register a heartbeat block for a subscription that
   * has been cancelled.
   */
  @Test
  void cannotRegisterHeartbeatForCancelledSubscription() {
    readState("real-binder-with-one-cancelled-event-subscription");

    CreateEvmEventSubscription subscription = getSubscription(0);
    Unsigned256 heartbeat = subscription.fromBlock().add(Unsigned256.create(1));

    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    "invoke?",
                    ADDRESSES.get(0),
                    35L,
                    serializeRegisterExternalHeartbeatBlock(1, heartbeat)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot add events to cancelled or non-existing subscription 1");
  }

  /** The EVM oracles can register heartbeat blocks for multiple subscriptions simultaneously. */
  @Test
  void registerHeartbeatsForMultipleSubscriptions() {
    readState("real-binder-with-two-evm-event-subscriptions");

    CreateEvmEventSubscription firstSub = getSubscription(0);
    Unsigned256 firstHeartbeat = firstSub.fromBlock().add(Unsigned256.create(1));

    CreateEvmEventSubscription secondSub = getSubscription(1);
    Unsigned256 secondHeartbeat = secondSub.fromBlock().add(Unsigned256.create(100));

    registerHeartbeat(ADDRESSES.get(1), 1, firstHeartbeat);
    assertPendingHeartbeat(1, null, firstHeartbeat, null, null);

    registerHeartbeat(ADDRESSES.get(3), 2, secondHeartbeat);
    assertPendingHeartbeat(2, null, null, null, secondHeartbeat);

    verifySerialization("real-binder-with-two-pending-heartbeats");
  }

  /** The EVM oracles can confirm heartbeat blocks for multiple subscriptions simultaneously. */
  @Test
  void confirmHeartbeatsForMultipleSubscriptions() {
    readState("real-binder-with-two-pending-heartbeats");

    CreateEvmEventSubscription firstSub = getSubscription(0);
    Unsigned256 firstHeartbeat = firstSub.fromBlock().add(Unsigned256.create(1));

    CreateEvmEventSubscription secondSub = getSubscription(1);
    Unsigned256 secondHeartbeat = secondSub.fromBlock().add(Unsigned256.create(100));

    registerHeartbeat(ADDRESSES.get(3), 1, firstHeartbeat);
    confirmHeartbeat(ADDRESSES.get(2), 1, firstHeartbeat);

    registerHeartbeat(ADDRESSES.get(0), 2, secondHeartbeat);
    confirmHeartbeat(ADDRESSES.get(1), 2, secondHeartbeat);

    verifySerialization("real-binder-with-two-confirmed-heartbeats");
  }

  private List<EvmEventLog> getEventLogs(int subscriptionId) {
    List<EvmEventLog> eventData =
        currentState().getConfirmedEventLogsRaw(subscriptionId).stream()
            .map(ConfirmedEventLog::getEventLog)
            .toList();
    return List.copyOf(eventData);
  }

  void assertEventLogs(ConfirmedEventLog actualLog, EvmEventLog observedLog) {
    Assertions.assertThat(actualLog.getEventLog()).isEqualTo(observedLog);
  }

  private byte[] serializeAddExternalEvent(int subscriptionId, EvmEventLog observedEventLog) {
    return SafeDataOutputStream.serialize(
        rpc -> {
          rpc.writeByte(ADD_EXTERNAL_EVENT);
          rpc.writeInt(subscriptionId);
          observedEventLog.getMetadata().write(rpc);
          observedEventLog.write(rpc);
        });
  }

  private byte[] serializeRegisterExternalHeartbeatBlock(
      int subscriptionId, Unsigned256 blockNumber) {
    return SafeDataOutputStream.serialize(
        rpc -> {
          rpc.writeByte(REGISTER_EXTERNAL_BLOCK_HEARTBEAT);
          rpc.writeInt(subscriptionId);
          blockNumber.write(rpc);
        });
  }

  private void invokeZkStateUpdates() {
    long registeredEventSubscriptionFees =
        invoke(
                "openInvocation",
                ADDRESSES.get(0),
                34L,
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeByte(OPEN_INVOCATION);
                      stream.writeByte(0x00); // Dummy action id
                    }))
            .registeredZkFees();

    Assertions.assertThat(registeredEventSubscriptionFees).isEqualTo(25_000L);
  }

  private void invokeZkStateUpdatesWithoutFees() {
    long registeredEventSubscriptionFees =
        invoke(
                "openInvocation",
                ADDRESSES.get(0),
                34L,
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeByte(OPEN_INVOCATION);
                      stream.writeByte(0x00); // Dummy action id
                    }))
            .registeredZkFees();

    Assertions.assertThat(registeredEventSubscriptionFees).isEqualTo(0);
  }

  private static final class ZkStateChangeBuilder {

    private final List<byte[]> updates = new ArrayList<>();

    private ZkStateChangeBuilder subscribeToEvents(CreateEvmEventSubscription subscriptionRpc) {
      List<TopicList> topics = subscriptionRpc.topics();
      updates.add(
          SafeDataOutputStream.serialize(
              s -> {
                s.writeByte(0x0A); // SUBSCRIBE TO EVENTS
                s.writeString(subscriptionRpc.chainId());
                subscriptionRpc.contractAddress().write(s);
                subscriptionRpc.fromBlock().write(s);
                s.writeInt(topics.size());
                for (TopicList inner : topics) {
                  inner.write(s);
                }
              }));
      return this;
    }

    private ZkStateChangeBuilder unsubscribeFromEvents(int subId) {
      updates.add(
          SafeDataOutputStream.serialize(
              s -> {
                s.writeByte(0x0B); // UNSUBSCRIBE FROM EVENTS
                s.writeInt(subId);
              }));
      return this;
    }

    private ZkStateChangeBuilder deleteEvents(List<Integer> eventIds) {
      updates.add(
          SafeDataOutputStream.serialize(
              s -> {
                s.writeByte(0x0C); // Delete external events
                s.writeInt(eventIds.size()); // delete one event
                for (Integer eventId : eventIds) {
                  s.writeInt(eventId);
                }
              }));
      return this;
    }

    private void build(WasmInvoker invoker) {
      WasmInvoker.WasmResultSection zkStateUpdate =
          new WasmInvoker.WasmResultSection(
              SECTION_ZK_STATE_UPDATE,
              SafeDataOutputStream.serialize(
                  s -> {
                    s.writeInt(updates.size()); // Number of updates
                    for (byte[] update : updates) {
                      s.write(update);
                    }
                  }));

      WasmState wasmState = new WasmState().withState(new byte[0]);
      Mockito.when(
              invoker.executeWasm(
                  Mockito.anyLong(),
                  Mockito.anyString(),
                  Mockito.any(),
                  Mockito.any(),
                  Mockito.any(),
                  Mockito.any(),
                  Mockito.any()))
          .thenReturn(
              new WasmInvoker.WasmResult(
                  wasmState, EventResult.create(), 25L, List.of(zkStateUpdate)));
    }
  }

  @Override
  protected byte[] defaultLinearElement() {
    return BinaryExtensionFieldElement.create((byte) 1).serialize();
  }

  @Override
  protected Path pathForStateJson(String name) {
    return pathForResource(getClass(), "ref/WasmRealContractBinderTest/%s.json".formatted(name));
  }
}
