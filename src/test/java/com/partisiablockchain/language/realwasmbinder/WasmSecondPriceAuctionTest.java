package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.realwasmbinder.ConversionToSolidityTest.convertSignatureToSolidityFormat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.zk.real.contract.RealNodeState;
import com.partisiablockchain.zk.real.contract.RealTest;
import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;

/** Tests for {@link WasmRealContract}. */
public final class WasmSecondPriceAuctionTest extends BinaryRealContractTest {

  // Lots of type boilerplate: CPD-OFF

  private final List<BlockchainAddress> bidders;
  private final List<BlockchainAddress> nonBidders;
  private final List<Hash> transactions;

  /**
   * Filename of the compiled WASM for the {@link WasmSecondPriceAuctionTests}.
   *
   * <p>Source code for the testing contract can be found in the second-price-auction example: <a
   * href="https://gitlab.com/partisiablockchain/language/rust-example-zk-second-price-auction">second-price-auction</a>
   */
  private static final String ZK_CONTRACT_FILE = "contracts/zk_second_price.zkwa";

  /** Initialize test. */
  public WasmSecondPriceAuctionTest() {
    super(AbstractWasmAverageSalaryTest.loadInvoker(ZK_CONTRACT_FILE));

    bidders =
        List.of(getUser(0), getUser(1), getUser(2), getUser(3), getUser(4), getUser(5), getUser(6));
    nonBidders = List.of(getUser(1001), getUser(1002));
    transactions = new ArrayList<>();
    for (int idx = 0; idx < 10; idx++) {
      final int idxFinal = idx;
      transactions.add(Hash.create(s -> s.writeInt(idxFinal)));
    }
  }

  @Override
  protected List<byte[]> asShares(List<Integer> values) {
    return super.asShares(values).stream()
        .map(WasmAverageSalaryTest::expandByteArrayToBitArray)
        .collect(Collectors.toList());
  }

  //// Action constants

  static final byte[] ACTION_START_COMPUTE = new byte[] {0x01};
  static final byte[] ACTION_REGISTER_BIDDER = new byte[] {0x30};
  static final byte[] ACTION_BID = new byte[] {0x40};

  //// Tests

  private void addBid(final BlockchainAddress sender, final Hash transaction, final int bidAmount) {
    sendSecretInput(
        transaction,
        asShares(List.of(makeNumber(bidAmount))),
        List.of(32),
        sender,
        true,
        output -> output.write(ACTION_BID));
  }

  private void registerBidder(
      final BlockchainAddress sender, final int bidderId, final BlockchainAddress bidder) {
    sendOpenInput(
        sender,
        out -> {
          out.write(ACTION_REGISTER_BIDDER);
          out.writeInt(bidderId);
          bidder.write(out);
        });
  }

  /** Tests creation of the second-price auction contract. */
  @RealTest
  public void createContract() {

    // Initialize
    this.create(output -> output.write(WasmRealContractInvoker.INIT_HEADER));

    // Register bidders
    int bidderIdCounter = 0x4310;
    for (final BlockchainAddress bidder : bidders) {
      registerBidder(OWNER, bidderIdCounter++, bidder);
    }
  }

  /** Test that contract creation must include header bytes. */
  @RealTest
  public void createContractMissingInitHeaderBytes() {
    Assertions.assertThatThrownBy(
            () -> this.create(output -> output.write(new byte[] {0, 1, 2, 3, 4})))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Contract initialization bytes should start with 0xFFFFFFFF0F");
  }

  /** Tests lifecycle of the second-price auction contract. */
  @RealTest(previous = "createContract")
  public void lifecycle() {
    setTime(444, 444444);

    // Add two bid
    addBid(bidders.get(0), transactions.get(0), 100);
    addBid(bidders.get(1), transactions.get(1), 90);

    // Try to start computation without enough bids; goes bad
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining(
            "At least 3 bidders must have submitted bids for the auction to start");

    // Non-bidder tries to add themselves as bidder.
    Assertions.assertThatThrownBy(() -> registerBidder(nonBidders.get(0), 1337, nonBidders.get(0)))
        .hasMessageContaining("Only the owner can register bidders");

    // Add next three bids
    addBid(bidders.get(2), transactions.get(2), 150);
    addBid(bidders.get(3), transactions.get(3), 120);

    Assertions.assertThatThrownBy(() -> addBid(bidders.get(3), transactions.get(4), 130))
        .hasMessageContaining("Each bidder is only allowed to send one bid");

    Assertions.assertThatThrownBy(() -> addBid(nonBidders.get(0), transactions.get(4), 99999))
        .hasMessageContaining("is not a registered bidder");

    // Confirm
    confirmSecretInput(transactions.get(0), bidders.get(0));
    confirmSecretInput(transactions.get(1), bidders.get(1));
    confirmSecretInput(transactions.get(2), bidders.get(2));
    confirmSecretInput(transactions.get(3), bidders.get(3));

    // Add last; this will be discarded
    setTime(4444, 4444445);
    addBid(bidders.get(5), transactions.get(4), 200);

    // Non-owner tries to start computation, goes bad
    setTime(44444, 90000001);
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(bidders.get(5), out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining("Only contract owner can start the auction");

    // Now owner finally starts
    sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE));
  }

  /** Tests lifecycle of the second-price auction contract. */
  @RealTest(previous = "lifecycle")
  public void lifecycle2() {

    // Someone tries starts computation AGAIN, but fails as it's already
    // ongoing
    setTime(44446, 90005001);
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(bidders.get(0), out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining("Computation must start from Waiting state")
        .hasMessageContaining("was Calculating");

    // Perform computation
    executeComputation(new Random(132));

    // Open computation result
    resolveOpens();

    // Check that result is not available yet
    Assertions.assertThat(Hex.toHexString(getContractState().getData()))
        .isEqualTo(
            ("000000000000000000000000000000000000123456" // Admin addr
                    + "0700 0000" // Allowed bidders
                    + "1043 0000 000000000000000000000000000000000000000000" // bidder 0
                    + "1143 0000 000000000000000000000000000000000000000001" // bidder 1
                    + "1243 0000 000000000000000000000000000000000000000002" // bidder 2
                    + "1343 0000 000000000000000000000000000000000000000003" // bidder 3
                    + "1443 0000 000000000000000000000000000000000000000004" // bidder 4
                    + "1543 0000 000000000000000000000000000000000000000005" // bidder 5
                    + "1643 0000 000000000000000000000000000000000000000006" // bidder 6
                    + "00" // Bid result
                )
                .replace(" ", ""));

    Assertions.assertThat(getStateLoader().zkContractState().getAttestations()).isNotEmpty();
  }

  /** Exists to trigger snapshot. */
  @RealTest(previous = "lifecycle2")
  public void attest() {
    completeAttestation(getAttestations().get(0).getId());

    Assertions.assertThat(Hex.toHexString(getContractState().getData()))
        .isEqualTo(
            ("000000000000000000000000000000000000123456" // Admin addr
                    + "0700 0000" // Allowed bidders
                    + "1043 0000 000000000000000000000000000000000000000000" // bidder 0
                    + "1143 0000 000000000000000000000000000000000000000001" // bidder 1
                    + "1243 0000 000000000000000000000000000000000000000002" // bidder 2
                    + "1343 0000 000000000000000000000000000000000000000003" // bidder 3
                    + "1443 0000 000000000000000000000000000000000000000004" // bidder 4
                    + "1543 0000 000000000000000000000000000000000000000005" // bidder 5
                    + "1643 0000 000000000000000000000000000000000000000006" // bidder 6
                    + "01" // Bid result
                    + "1243 0000" // Winner
                    + "7800 0000" // 2nd highest bid
                )
                .replace(" ", ""));
  }

  /** Exists to trigger snapshot. */
  @RealTest(previous = "attest")
  public void postAttest() {
    Assertions.assertThat(getStateLoader().zkContractState().getAttestations()).isNotEmpty();
  }

  /** Check that call halfway through attestation does not include attestation. */
  @RealTest(previous = "lifecycle2")
  public void attestHalfway() {
    Assertions.assertThatThrownBy(
            () -> completeAttestation(getAttestations().get(0).getId(), 1, 2, 3))
        .withFailMessage("Attestation must be complete")
        .isInstanceOf(TrapException.class);
    registerBidder(OWNER, 1337, nonBidders.get(0));
  }

  /** Exists to trigger snapshot. */
  @RealTest(previous = "attestHalfway")
  public void postAttestHalfway() {}

  /**
   * Check that the conversion to solidity gives the correct values. The values are used for testing
   * the solidity contract. <a
   * href="https://gitlab.com/partisiablockchain/real/examples/second-price-auction-eth">second-price-auction-eth</a>
   */
  @RealTest(previous = "attest")
  public void conversionToSolidity() {
    DataAttestation dataAttestation = getAttestations().get(0);
    List<BlockchainPublicKey> enginePublicKeys =
        getStateLoader().zkContractState().getParticipatingEngines().stream()
            .map(RealNodeState.RealEngine::getPublicKey)
            .toList();
    List<String> solidityPublicKeys =
        enginePublicKeys.stream()
            .map(ConversionToSolidityTest::convertPublicKeyToSolidityFormat)
            .toList();

    Assertions.assertThat(solidityPublicKeys.size()).isEqualTo(4);
    Assertions.assertThat(solidityPublicKeys.get(0))
        .isEqualTo(
            "79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798483ada7726a3c4655da4fb"
                + "fc0e1108a8fd17b448a68554199c47d08ffb10d4b8");
    Assertions.assertThat(solidityPublicKeys.get(1))
        .isEqualTo(
            "c6047f9441ed7d6d3045406e95c07cd85c778e4b8cef3ca7abac09b95c709ee51ae168fea63dc339a3c584"
                + "19466ceaeef7f632653266d0e1236431a950cfe52a");
    Assertions.assertThat(solidityPublicKeys.get(2))
        .isEqualTo(
            "f9308a019258c31049344f85f89d5229b531c845836f99b08601f113bce036f9388f7b0f632de8140fe337"
                + "e62a37f3566500a99934c2231b6cb9fd7584b8e672");
    Assertions.assertThat(solidityPublicKeys.get(3))
        .isEqualTo(
            "e493dbf1c10d80f3581e4904930b1404cc6c13900ee0758474fa94abe8c4cd1351ed993ea0d455b75642e2"
                + "098ea51448d967ae33bfbdfe40cfe97bdc47739922");
    byte[] data = dataAttestation.getData();
    Assertions.assertThat(Hex.toHexString(data)).isEqualTo("0000431200000078");
    List<String> signatures = new ArrayList<>();
    for (int i = 0; i < 4; i++) {
      signatures.add(
          Hex.toHexString(convertSignatureToSolidityFormat(dataAttestation.getSignature(i))));
    }
    Assertions.assertThat(signatures.get(0))
        .isEqualTo(
            "57fb7e4a8073faec3081640d566163dbc3eff75743549dc28021f4dd15a7d7f55405c3cd5aa83f21f64fdf"
                + "3e32e033cc36264cce1078351d81c606b111ba9ce01b");
    Assertions.assertThat(signatures.get(1))
        .isEqualTo(
            "d206e0bd7e8270baca03b1e951942f52cc0bb7fab4f607c4659573d49e85f0a07b7f3280bfc9f4c2d381d4"
                + "b649f4f28546773889bc667cd3359f4c66c9d223711c");
    Assertions.assertThat(signatures.get(2))
        .isEqualTo(
            "22d05f23917bff91f63ea16b86c894faaea4264a9f5245acc92d5eb55562ec1305042bca531a32c5d0b89b"
                + "63b895e189908598483af2830dd6745198c635089a1b");
    Assertions.assertThat(signatures.get(3))
        .isEqualTo(
            "514844a127c08d47148269688663506d713f9b79207a119a6a5f1dcfb3f67902069f72a42d93ab974202b8"
                + "ef2b394c9ab68efaff4ed588458a10befdf534d2391b");
  }
  // Lots of type boilerplate: CPD-ON
}
