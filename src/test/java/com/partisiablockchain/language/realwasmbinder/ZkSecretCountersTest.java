package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.zk.real.contract.RealTest;
import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;

/** Testing of ZK Secret Counters contract. */
public final class ZkSecretCountersTest extends BinaryRealContractTest {

  /**
   * Source: <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/zk-secret-counters">zk-secret-counters</a>.
   */
  private static final String fileContract = "contracts/zk_secret_counters.zkwa";

  //// Action constants

  static final byte[] ACTION_COUNTER_NEW = new byte[] {0x40};
  static final byte[] ACTION_COUNTER_INC = new byte[] {0x41};

  /** Initialize test. */
  public ZkSecretCountersTest() {
    super(AbstractWasmAverageSalaryTest.loadInvoker(fileContract));
  }

  private List<byte[]> makeIntegerTupleVariable(int... fields) {
    final byte[] byteArray =
        SafeDataOutputStream.serialize(
            stream -> {
              for (final int field : fields) {
                stream.writeInt(Integer.reverseBytes(field));
              }
            });
    final byte[] bitArray = WasmAverageSalaryTest.expandByteArrayToBitArray(byteArray);
    return List.of(bitArray);
  }

  private static Hash transactionHash(int transactionIdx) {
    return Hash.create(s -> s.writeInt(transactionIdx));
  }

  private void createNewCounter(
      final BlockchainAddress sender, final Hash transaction, final int key, final int amount) {
    sendSecretInput(
        transaction,
        makeIntegerTupleVariable(key, amount),
        List.of(32 + 32),
        sender,
        true,
        output -> {
          output.write(ACTION_COUNTER_NEW);
        });
  }

  private void incrementCounter(
      final BlockchainAddress sender, final Hash transaction, final int key, final int amount) {
    sendSecretInput(
        transaction,
        makeIntegerTupleVariable(key, amount),
        List.of(32 + 32),
        sender,
        true,
        output -> {
          output.write(ACTION_COUNTER_INC);
        });
  }

  //// Tests

  /** Tests whether contract can be initialized. */
  @RealTest
  public void createContract() {
    // Initialize
    this.create(output -> output.write(WasmRealContractInvoker.INIT_HEADER));
    Assertions.assertThat(getContractState().getData()).isNotNull();
  }

  static final int NUM_VARIABLES = 10;

  /** Create counters test. */
  @RealTest(previous = "createContract")
  public void createCounters() {
    for (int idx = 0; idx < NUM_VARIABLES; idx++) {
      createNewCounter(getUser(idx), transactionHash(idx), idx, 1000 + idx);
      confirmSecretInput(transactionHash(idx), getUser(idx));
      Assertions.assertThat(getVariables()).as("Zk Variables").hasSize(idx + 1);
    }
  }

  /** Update counters test. */
  @RealTest(previous = "createCounters")
  public void updateCounters() {
    for (int idx = 0; idx < NUM_VARIABLES; idx++) {
      incrementCounter(
          getUser(idx), transactionHash(idx + NUM_VARIABLES), idx, NUM_VARIABLES + idx);
      confirmSecretInput(transactionHash(idx + NUM_VARIABLES), getUser(idx));
      Assertions.assertThat(getVariables()).as("Zk Variables").hasSize(NUM_VARIABLES + 1);
      executeComputation(new Random(132));
      Assertions.assertThat(getVariables()).as("Zk Variables").hasSize(NUM_VARIABLES);
    }
  }

  @RealTest(previous = "updateCounters")
  void updateCountersCheckpoint() {}
}
