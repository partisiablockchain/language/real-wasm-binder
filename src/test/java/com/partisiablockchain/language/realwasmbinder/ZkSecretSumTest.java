package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.zk.real.contract.RealTest;
import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Tests for {@link WasmRealContract}. */
public final class ZkSecretSumTest extends BinaryRealContractTest {

  private static final Logger logger = LoggerFactory.getLogger(ZkSecretSumTest.class);

  private static final List<Integer> BITLENGTHS = List.of(32);

  private static final String fileContract = "contracts/zk_secret_sum.zkwa";

  //// Action constants

  static final byte[] ACTION_ADD_SECRET = new byte[] {0x40};

  /** Initialize test. */
  public ZkSecretSumTest() {
    super(AbstractWasmAverageSalaryTest.loadInvoker(fileContract));
  }

  @Override
  protected List<byte[]> asShares(List<Integer> values) {
    return super.asShares(values).stream()
        .map(WasmAverageSalaryTest::expandByteArrayToBitArray)
        .toList();
  }

  private static Hash transactionHash(int transactionIdx) {
    return Hash.create(s -> s.writeInt(transactionIdx));
  }

  private void addVariable(
      final BlockchainAddress sender,
      final Hash transaction,
      final int secretData,
      final boolean openAfterSum) {
    sendSecretInput(
        transaction,
        asShares(List.of(makeNumber(secretData))),
        BITLENGTHS,
        sender,
        true,
        output -> {
          output.write(ACTION_ADD_SECRET);
          output.writeBoolean(openAfterSum);
        });
  }

  //// Tests

  /** Tests whether contract can be initialized. */
  @RealTest
  public void createContract() {
    // Initialize
    this.create(output -> output.write(WasmRealContractInvoker.INIT_HEADER));
    Assertions.assertThat(getContractState().getData()).isNotNull();
  }

  /** Test summation. */
  @RealTest(previous = "createContract")
  public void bigNumberNow() {
    runSumTest(List.of(0x12_00_00_34, 0), idx -> idx != 0);
    Assertions.assertThat(getSumVariableData())
        .containsExactly(
            0, 0, 1, 0, // 4
            1, 1, 0, 0, // 3
            0, 0, 0, 0, //
            0, 0, 0, 0, //
            0, 0, 0, 0, //
            0, 0, 0, 0, //
            0, 1, 0, 0, // 2
            1, 0, 0, 0); // 1
  }

  /** Test summation. */
  @RealTest(previous = "createContract")
  public void happyPathSmallNoOpen() {
    runSumTest(10, idx -> false);
    Assertions.assertThat(getSumVariableData())
        .containsExactly(
            1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0);
  }

  /** Stress-testing involving a whole lot of computations. */
  @RealTest(previous = "createContract")
  public void happyPathSmallAlwaysOpen() {
    runSumTest(10, idx -> true);
    Assertions.assertThat(getSumVariableData())
        .containsExactly(
            1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0);
  }

  /** Stress-testing involving a whole lot of computations. */
  @RealTest(previous = "createContract")
  public void hugeHappyPath() {
    final int numVariables = 100;
    runSumTest(numVariables, idx -> idx + 1 == numVariables);
    Assertions.assertThat(getSumVariableData())
        .containsExactly(
            0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0);
  }

  private byte[] getSumVariableData() {
    final var variableId = getVariables().get(0).getId();
    return getStateLoader().stateController().getDataForVariable(variableId).get(0);
  }

  private void runSumTest(final int numVariables, final Predicate<Integer> shouldOpenPredicate) {
    runSumTest(IntStream.range(0, numVariables).mapToObj(x -> x).toList(), shouldOpenPredicate);
  }

  private void runSumTest(
      final Iterable<Integer> inputs, final Predicate<Integer> shouldOpenPredicate) {
    int inputIdx = 0;
    for (final int input : inputs) {
      if (inputIdx % 1000 == 0) {
        logger.info("Starting round %d".formatted(inputIdx));
      }
      final boolean shouldOpen = shouldOpenPredicate.test(inputIdx);
      addVariable(getUser(inputIdx), transactionHash(inputIdx), input, shouldOpen);
      confirmSecretInput(transactionHash(inputIdx), getUser(inputIdx));
      if (inputIdx > 0) {
        executeComputation(new Random(132));
      }
      if (getStateLoader().zkContractState().hasPendingOnChainOpen()) {
        resolveOpens();
      }
      Assertions.assertThat(getStateLoader().zkContractState().getPendingInputs())
          .as("Zk Pending inputs")
          .isEmpty();
      Assertions.assertThat(getVariables()).as("Zk Variables").hasSize(1);

      inputIdx++;
    }
  }
}
