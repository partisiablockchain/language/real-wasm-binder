package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.zk.real.contract.RealTest;
import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;

/** Tests for {@link WasmSecretVotingTest}. */
public final class WasmSecretVotingTest extends BinaryRealContractTest {

  // Lots of type boilerplate: CPD-OFF

  private final List<BlockchainAddress> voters;
  private final List<BlockchainAddress> nonVoters;
  private final List<Hash> transactions;

  private static final List<Integer> BITLENGTHS = List.of(32);

  /**
   * Filename of the compiled WASM for {@link WasmSecretVotingTest}.
   *
   * <p>Source code for the testing contract can be found in the <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/zk-voting">rust-example-secret-voting</a>
   * example:
   */
  private static final String ZK_CONTRACT_FILE = "contracts/zk_voting.zkwa";

  /** Initialize test. */
  public WasmSecretVotingTest() {
    super(AbstractWasmAverageSalaryTest.loadInvoker(ZK_CONTRACT_FILE, null));

    voters =
        List.of(getUser(0), getUser(1), getUser(2), getUser(3), getUser(4), getUser(5), getUser(6));
    nonVoters = List.of(getUser(1001), getUser(1002));
    transactions = new java.util.ArrayList<Hash>();
    for (int idx = 0; idx < 10; idx++) {
      final int idxFinal = idx;
      transactions.add(Hash.create(s -> s.writeInt(idxFinal)));
    }
  }

  @Override
  protected List<byte[]> asShares(List<Integer> values) {
    return super.asShares(values).stream()
        .map(WasmAverageSalaryTest::expandByteArrayToBitArray)
        .collect(Collectors.toList());
  }

  //// Action constants

  static final byte[] ACTION_START_COMPUTE = new byte[] {0x01};
  static final byte[] ACTION_ADD_VOTE = new byte[] {0x40};

  //// Tests

  private void addVote(
      final BlockchainAddress sender, final Hash transaction, final boolean isForVote) {
    sendSecretInput(
        transaction,
        asShares(List.of(makeBoolean(isForVote))),
        BITLENGTHS,
        sender,
        true,
        output -> output.write(ACTION_ADD_VOTE));
  }

  /** Tests creation of the secret voting contract. */
  @RealTest
  public void createContract() {

    // Initialize
    this.create(
        output -> {
          output.write(WasmRealContractInvoker.INIT_HEADER);
          // Duration: 24 hours
          output.writeInt(24 * 60 * 60 * 1000);
          // Allowed voters
          output.writeInt(voters.size());
          for (final BlockchainAddress addr : voters) {
            addr.write(output);
          }

          // Vote basis: 1/2 strict majority
          output.writeInt(1);
          output.writeInt(2);
          output.writeBoolean(true);
        });
  }

  /** Test that contract creation must include header bytes. */
  @RealTest
  public void createContractMissingInitHeaderBytes() {
    Assertions.assertThatThrownBy(
            () -> this.create(output -> output.write(new byte[] {0, 1, 2, 3, 4})))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Contract initialization bytes should start with 0xFFFFFFFF0F");
  }

  /**
   * Tests lifecycle of the secret voting contract from right after creation until right after the
   * computation is started.
   */
  @RealTest(previous = "createContract")
  public void lifecycle() {
    setTime(444, 444444);

    // Add two vote
    addVote(voters.get(0), transactions.get(0), true);
    addVote(voters.get(1), transactions.get(1), false);

    // Try to start computation without enough votes; goes bad
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(OWNER, out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining(
            "Vote counting cannot start before specified starting time 90000000 ms UTC")
        .hasMessageContaining("current time is 444444 ms UTC");

    // Add next three votes
    addVote(voters.get(2), transactions.get(2), true);
    addVote(voters.get(3), transactions.get(3), false);

    Assertions.assertThatThrownBy(() -> addVote(voters.get(3), transactions.get(4), true))
        .hasMessageContaining("Each voter is only allowed to send one vote variable");

    Assertions.assertThatThrownBy(() -> addVote(nonVoters.get(0), transactions.get(4), false))
        .hasMessageContaining("Only voters can send votes");

    // Confirm
    confirmSecretInput(transactions.get(0), voters.get(0));
    confirmSecretInput(transactions.get(1), voters.get(1));
    confirmSecretInput(transactions.get(2), voters.get(2));
    confirmSecretInput(transactions.get(3), voters.get(3));

    // Add last; this will be discarded
    setTime(4444, 4444445);
    addVote(voters.get(5), transactions.get(4), false);

    // Someone starts computation, which discards pending
    setTime(44444, 90000001);
    sendOpenInput(voters.get(5), out -> out.write(ACTION_START_COMPUTE));
  }

  /**
   * Tests lifecycle of the secret voting contract from right after the computation is started until
   * after the result has been opened and attested.
   */
  @RealTest(previous = "lifecycle")
  public void lifecycle2() {

    // Someone tries starts computation AGAIN, but fails as it's already
    // ongoing
    setTime(44446, 90005001);
    Assertions.assertThatThrownBy(
            () -> sendOpenInput(voters.get(0), out -> out.write(ACTION_START_COMPUTE)))
        .hasMessageContaining("Vote counting must start from Waiting state")
        .hasMessageContaining("was Calculating");

    // Perform computation
    executeComputation(new Random(132));

    // Open computation result
    resolveOpens();

    // Check result
    Assertions.assertThat(getContractState().getData())
        .isEqualTo(
            Hex.decode(
                "000000000000000000000000000000000000123456" // Admin addr
                    + "005c 2605 0000 0000" // Voting deadline
                    + "804a 5d05 0000 0000" // Counting deadline
                    + "0700 0000" // Allowed voters
                    + "000000000000000000000000000000000000000000" // Voter 0
                    + "000000000000000000000000000000000000000001" // Voter 1
                    + "000000000000000000000000000000000000000002" // Voter 2
                    + "000000000000000000000000000000000000000003" // Voter 3
                    + "000000000000000000000000000000000000000004" // Voter 4
                    + "000000000000000000000000000000000000000005" // Voter 5
                    + "000000000000000000000000000000000000000006" // Voter 6
                    + "0100 0000 0200 0000 01" // Vote basis
                    + "01 0200 0000 0200 0000 00" // Result
                ));
    Assertions.assertThat(getAttestations()).isNotEmpty();
  }

  /** Tests happy lifecycle of the secret voting contract. */
  @RealTest(previous = "createContract")
  public void lifecycleYes() {
    setTime(444, 444444);

    // Add and confirm votes
    for (int idx = 1; idx <= 6; idx++) {
      setTime(1000 + idx * 10, 1000000 + idx * 1000);
      addVote(voters.get(idx), transactions.get(idx), true);
      confirmSecretInput(transactions.get(idx), voters.get(idx));
    }

    // Computation is started
    setTime(44444, 90000001);
    sendOpenInput(voters.get(5), out -> out.write(ACTION_START_COMPUTE));

    // Perform computation
    executeComputation(new Random(12321));

    // Open computation result
    resolveOpens();

    // Check result
    Assertions.assertThat(getContractState().getData())
        .isEqualTo(
            Hex.decode(
                "000000000000000000000000000000000000123456" // Admin addr
                    + "005c 2605 0000 0000" // Voting deadline
                    + "804a 5d05 0000 0000" // Counting deadline
                    + "0700 0000" // Allowed voters
                    + "000000000000000000000000000000000000000000" // Voter 0
                    + "000000000000000000000000000000000000000001" // Voter 1
                    + "000000000000000000000000000000000000000002" // Voter 2
                    + "000000000000000000000000000000000000000003" // Voter 3
                    + "000000000000000000000000000000000000000004" // Voter 4
                    + "000000000000000000000000000000000000000005" // Voter 5
                    + "000000000000000000000000000000000000000006" // Voter 6
                    + "0100 0000 0200 0000 01" // Vote basis
                    + "01 0600 0000 0000 0000 01" // Result
                ));

    Assertions.assertThat(getAttestations()).isNotEmpty();
  }

  /** Return attestation calls. */
  @RealTest(previous = "lifecycle2")
  public void attest() {
    completeAttestation(getAttestations().get(0).getId());
  }

  /** Exists to trigger snapshot. */
  @RealTest(previous = "attest")
  public void postAttest() {}

  // Lots of type boilerplate: CPD-ON
}
