package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

final class ConversionToSolidityTest {

  /**
   * Converts a {@link BlockchainPublicKey} to the byte format expected in solidity and returns the
   * corresponding hex string.
   *
   * @param blockchainPublicKey the {@link BlockchainPublicKey} to be converted.
   * @return a hex string containing to the formatted public key.
   */
  static String convertPublicKeyToSolidityFormat(BlockchainPublicKey blockchainPublicKey) {
    byte[] encodedWithoutHeader = getEncodedWithoutHeader(blockchainPublicKey);
    return Hex.toHexString(encodedWithoutHeader);
  }

  /**
   * Converts a {@link BlockchainPublicKey} to the byte format expected in solidity.
   *
   * @param publicKey the {@link BlockchainPublicKey} to be converted.
   * @return a bytearray holding the public key formatted for solidity.
   */
  private static byte[] getEncodedWithoutHeader(BlockchainPublicKey publicKey) {
    byte[] encoded = publicKey.getEcPointBytes();
    byte[] headless = new byte[encoded.length - 1];
    System.arraycopy(encoded, 1, headless, 0, headless.length);
    return headless;
  }

  /**
   * Converts a bigInteger into byte[32] and zero pads it.
   *
   * @param value {@link BigInteger} to be converted.
   * @return a zero-padded byte[32] corresponding to the value.
   */
  private static byte[] zeroPad(BigInteger value) {
    byte[] bytes = value.toByteArray();
    int offset;
    if (bytes[0] == 0) {
      // The byte is only included to contain sign
      offset = 1;
    } else {
      offset = 0;
    }

    int bytesToWrite = bytes.length - offset;
    int destination = 32 - bytesToWrite;
    byte[] result = new byte[32];
    System.arraycopy(bytes, offset, result, destination, bytesToWrite);
    return result;
  }

  /**
   * Converts a signature to the byte format expected in solidity.
   *
   * @param signature the signature to be converted.
   * @return a bytearray formatted for solidity.
   */
  static byte[] convertSignatureToSolidityFormat(Signature signature) {
    BigInteger r = signature.getR();
    BigInteger s = signature.getS();
    int v = signature.getRecoveryId() + 27;

    return SafeDataOutputStream.serialize(
        stream -> {
          stream.write(zeroPad(r));
          stream.write(zeroPad(s));
          stream.writeByte(v);
        });
  }

  /**
   * Checks that the conversion to solidity gives correct values when working on strings retrieved
   * from a real PBC interaction.
   */
  @Test
  void conversionToSolidityFromStringValues() {
    Signature sig0 =
        Signature.fromString(
            "006a22c25d521e757c9b74a0bfdb3bafbd57db1f8b2c985debb4393b95507a97c83bd6234ee1d5d62c3cb6"
                + "dd91c998a87ec235744252a8c8767b78a3bcb41478ba");
    Signature sig1 =
        Signature.fromString(
            "00eb7b2ae29aa590fb92bfb1d5c9ce4255e722e459eff302afef32d237d3b8f5bf056f6524681d4c6fdacd"
                + "822de1d891cacc92624db360061ebbbd7ecd05ca9024");
    Signature sig2 =
        Signature.fromString(
            "0095cdacf5394bceea7fd0f8c9f7579d9c29dee5546086416100dc60034acc46cb60414e1d3fc7a08e165f"
                + "2435c88cff5a1fe41a5e0f323c3793bd04bf8d3e062c");
    Signature sig3 =
        Signature.fromString(
            "00c3e0ed23a3200b1d9cb5bc52a284c0fd6b6e56872ce09644e5f98e862acb5ab37cd093f3c2e3df526a1c"
                + "616266a3d4e265c80654251d4fe47f9e5314a5e07da1");
    String convertedSig0 = Hex.toHexString(convertSignatureToSolidityFormat(sig0));
    String convertedSig1 = Hex.toHexString(convertSignatureToSolidityFormat(sig1));
    String convertedSig2 = Hex.toHexString(convertSignatureToSolidityFormat(sig2));
    String convertedSig3 = Hex.toHexString(convertSignatureToSolidityFormat(sig3));

    String pk0 =
        convertPublicKeyToSolidityFormat(
            BlockchainPublicKey.read(
                SafeDataInputStream.createFromBytes(
                    Base64.decode("AmrULWTt/Et92kgwwj7MlS/AePhsfCw9dSVVR9rGyOfL"))));
    String pk1 =
        convertPublicKeyToSolidityFormat(
            BlockchainPublicKey.read(
                SafeDataInputStream.createFromBytes(
                    Base64.decode("A8D5q4Sei5i16B23l9tt/fQWDTxOJsjr8hrvJsIfe18Q"))));
    String pk2 =
        convertPublicKeyToSolidityFormat(
            BlockchainPublicKey.read(
                SafeDataInputStream.createFromBytes(
                    Base64.decode("A/DInYJYvH+4tpYLrheUEROxv1x3r7irWNbjsp2AU4fG"))));
    String pk3 =
        convertPublicKeyToSolidityFormat(
            BlockchainPublicKey.read(
                SafeDataInputStream.createFromBytes(
                    Base64.decode("A5HFujCoDsEh8Q8Celvp189Q2YAjuUqCY521wmZLAnyP"))));

    Assertions.assertThat(convertedSig0)
        .isEqualTo(
            "6a22c25d521e757c9b74a0bfdb3bafbd57db1f8b2c985debb4393b95507a97c83bd6234ee1d5d62c3cb6dd"
                + "91c998a87ec235744252a8c8767b78a3bcb41478ba1b");
    Assertions.assertThat(convertedSig1)
        .isEqualTo(
            "eb7b2ae29aa590fb92bfb1d5c9ce4255e722e459eff302afef32d237d3b8f5bf056f6524681d4c6fdacd82"
                + "2de1d891cacc92624db360061ebbbd7ecd05ca90241b");
    Assertions.assertThat(convertedSig2)
        .isEqualTo(
            "95cdacf5394bceea7fd0f8c9f7579d9c29dee5546086416100dc60034acc46cb60414e1d3fc7a08e165f24"
                + "35c88cff5a1fe41a5e0f323c3793bd04bf8d3e062c1b");
    Assertions.assertThat(convertedSig3)
        .isEqualTo(
            "c3e0ed23a3200b1d9cb5bc52a284c0fd6b6e56872ce09644e5f98e862acb5ab37cd093f3c2e3df526a1c61"
                + "6266a3d4e265c80654251d4fe47f9e5314a5e07da11b");

    Assertions.assertThat(pk0)
        .isEqualTo(
            "6ad42d64edfc4b7dda4830c23ecc952fc078f86c7c2c3d75255547dac6c8e7cb5ffe23fe1163b55c3b405e"
                + "a39ea8b4cbc5d860c1123d0458412421d4434e464c");
    Assertions.assertThat(pk1)
        .isEqualTo(
            "c0f9ab849e8b98b5e81db797db6dfdf4160d3c4e26c8ebf21aef26c21f7b5f10088dc3f48838f1697713ac"
                + "45b5049a7ef3c0c9c5cb49e893cde377f4d2bc839f");
    Assertions.assertThat(pk2)
        .isEqualTo(
            "f0c89d8258bc7fb8b6960bae17941113b1bf5c77afb8ab58d6e3b29d805387c667ed5961191e85ef14fabe"
                + "41266c7aa7961f9b8b620c3066b9b5bd669b5de4bb");
    Assertions.assertThat(pk3)
        .isEqualTo(
            "91c5ba30a80ec121f10f027a5be9d7cf50d98023b94a82639db5c2664b027c8f98da501c2447b4b63c4c61"
                + "655b3bcba48868475d0440cc75ff4652d77959d3bd");
  }
}
