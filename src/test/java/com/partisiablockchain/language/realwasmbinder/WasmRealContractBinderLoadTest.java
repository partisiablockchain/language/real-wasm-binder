package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/** Tests for {@link WasmRealContract}. */
public final class WasmRealContractBinderLoadTest {

  // Disable CPD for WasmRealContractBinderTest, as it will eventually be moved
  // to a different repository. CPD-OFF

  /**
   * Filename of the compiled WASM for the tests.
   *
   * <p>Source code for the testing contract can be found in the rust-example-average-salary
   * example: {@code https://gitlab.com/partisiablockchain/language/contracts/zk-average-salary}.
   */
  private static final String FILE_CONTRACT = "contracts/average_salary.zkwa";

  static final byte[] WASM_BYTES = WasmExampleComputations.loadBytes(FILE_CONTRACT);

  @Test
  public void parseSectionedContractBytesToContract() {
    final WasmRealContractInvoker.WasmContract contractInfo =
        WasmRealContractInvokerFactory.parseSectionedContractBytesToContract(
            FILE_CONTRACT, WASM_BYTES);
    assertThat(contractInfo).isNotNull();
    assertThat(contractInfo.wasmFilename).isNotNull();
    assertThat(contractInfo.wasmBytes).isNotNull();
  }

  @Test
  public void invokerFromBytes() {
    final WasmRealContractInvoker invoker =
        WasmRealContractInvokerFactory.invokerFromBytes(WASM_BYTES);
    assertThat(invoker).isNotNull();
  }
}
