package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.zk.real.binder.TestBinderContext.DEFAULT_AVAILABLE_GAS;

import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.wasminvoker.WasmInvoker;
import com.partisiablockchain.language.wasminvoker.WasmState;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.binder.BinderTest;
import com.partisiablockchain.zk.real.binder.ComputationStateImpl;
import com.partisiablockchain.zk.real.binder.EnginesStatus;
import com.partisiablockchain.zk.real.binder.PendingInputImpl;
import com.partisiablockchain.zk.real.binder.PendingOutputVariable;
import com.partisiablockchain.zk.real.binder.PreProcessMaterialType;
import com.partisiablockchain.zk.real.binder.RealContractBinder;
import com.partisiablockchain.zk.real.binder.TestBinderContext;
import com.partisiablockchain.zk.real.binder.ZkClosedImpl;
import com.partisiablockchain.zk.real.binder.ZkStateImmutable;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.contract.RealNodeState.ComputationState;
import com.partisiablockchain.zk.real.contract.RealNodeState.FinishedComputation;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElementFactory;
import com.partisiablockchain.zk.real.protocol.field.Polynomial;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import com.secata.util.BitHelper;
import java.lang.reflect.Constructor;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Tests for {@link WasmRealContract}. */
public final class WasmRealContractBinderTest extends BinderTest {

  // Disable CPD for WasmRealContractBinderTest, as it will eventually be moved
  // to a different repository. CPD-OFF

  WasmRealContractBinderTest() {
    super();
  }

  private static final byte[] computationOutput = new byte[] {2, 5, 3, 99};
  private final List<byte[]> computationOutputShares = share(computationOutput);

  private static final long BASE_SERVICE_FEES = 25_000L;

  /**
   * Filename of the compiled WASM for the ZK testing contract.
   *
   * <p>Source code for the testing contract can be found in the <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-types">rust-example-zk-testing-contract</a>:
   */
  private static final String ZK_CONTRACT_FILE = "contracts/zk_actions.wasm";

  /**
   * Filename of the compiled WASM for the ZK testing contract.
   *
   * <p>Source code for the testing contract can be found in the <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-types">rust-example-zk-testing-contract</a>:
   *
   * <p>This variant is with the default {@code passive_hooks} feature disabled.
   */
  private static final String ZK_CONTRACT_FILE_NO_AUTO = "contracts/zk_actions_no_automatic.wasm";

  /**
   * Filename of the compiled WASM for the ZK testing contract.
   *
   * <p>Source code for the testing contract can be found in the <a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-types">rust-example-zk-testing-contract</a>:
   *
   * <p>This variant is with the default {@code throw_on_everything} feature enabled.
   */
  private static final String ZK_CONTRACT_FILE_THROW_ON_EVERYTHING =
      "contracts/zk_actions_throw_on_everything.wasm";

  @Override
  protected byte[] defaultLinearElement() {
    return BinaryExtensionFieldElement.create((byte) 1).serialize();
  }

  private void setWasmContract(final String wasmFilename) {
    setWasmContract(wasmFilename, COMPUTATION_DEFAULT);
  }

  private void setWasmContract(
      final String wasmFilename,
      final RealComputationEnv<WasmRealContractState, LargeByteArray> computation) {
    this.binder =
        ExceptionConverter.call(
            () -> {
              final Path wasmPath = pathForResource(getClass(), wasmFilename);
              final byte[] wasmBytes = readFileBytes(wasmPath);
              final var contract =
                  new WasmRealContractInvoker.WasmContract(wasmFilename, wasmBytes, computation);
              return new WasmRealContractBinder(WasmRealContractInvoker.fromWasmBytes(contract));
            },
            "Could not load: " + wasmFilename);
  }

  @CheckReturnValue
  private static Long asLong(final LargeByteArray byteArray) {
    return asLong(byteArray.getData());
  }

  @CheckReturnValue
  private static Long asLong(final byte[] byteArray) {
    final var buffer = java.nio.ByteBuffer.wrap(byteArray);
    buffer.order(java.nio.ByteOrder.LITTLE_ENDIAN);
    return buffer.getLong();
  }

  @CheckReturnValue
  private Long getOpenStateAsLong() {
    return asLong(currentState().getOpenState().getData());
  }

  @CheckReturnValue
  private InteractionsAndResult openInvocation(
      final byte actionId,
      final BlockchainAddress sender,
      final Consumer<SafeDataOutputStream> invocation,
      final long blockTime) {
    return openInvocation(actionId, sender, invocation, blockTime, DEFAULT_BLOCK_PRODUCTION_TIME);
  }

  @CheckReturnValue
  private InteractionsAndResult openInvocation(
      final byte actionId,
      final BlockchainAddress sender,
      final Consumer<SafeDataOutputStream> invocation,
      final long blockTime,
      final long blockProductionTime) {
    Assertions.assertThat(actionId).isLessThan((byte) 100);
    return invoke(
        "openInvocation",
        sender,
        blockTime,
        blockProductionTime,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(OPEN_INVOCATION);
              stream.writeByte(actionId);
              invocation.accept(stream);
            }));
  }

  @CheckReturnValue
  private InteractionsAndResult emptyOpenInvocation(
      final byte actionId, final BlockchainAddress sender) {
    return emptyOpenInvocation(actionId, sender, 10L);
  }

  @CheckReturnValue
  private InteractionsAndResult emptyOpenInvocation(
      final byte actionId, final BlockchainAddress sender, final long blockTime) {
    return openInvocation(actionId, sender, FunctionUtility.noOpConsumer(), blockTime);
  }

  @CheckReturnValue
  private InteractionsAndResult emptyOpenInvocation(
      final byte actionId,
      final BlockchainAddress sender,
      final long blockTime,
      final long blockProductionTime) {
    return openInvocation(
        actionId, sender, FunctionUtility.noOpConsumer(), blockTime, blockProductionTime);
  }

  private void setWasmContractWithSecretInputs(final String contractFile, final int bitlength) {
    setWasmContract(contractFile, COMPUTATION_USELESS);
    bitlengthShortname(bitlength); // Check that this bitlength pattern is known
  }

  @CheckReturnValue
  private static byte[] concat(final byte[] a, final byte[] b) {
    final byte[] out = java.util.Arrays.copyOf(a, a.length + b.length);
    System.arraycopy(b, 0, out, a.length, b.length);
    return out;
  }

  @CheckReturnValue
  private InteractionsAndResult addUnconfirmedOnChainInputWithExplicitShortname(
      final BlockchainAddress owner,
      final byte[] shortname,
      final int shareBitLength,
      final long blockTime,
      final byte[] additionalInformation,
      final byte[] inputData) {
    return addUnconfirmedInput(
            ZERO_KNOWLEDGE_INPUT_ON_CHAIN,
            owner,
            shareBitLength,
            blockTime,
            concat(shortname, additionalInformation),
            inputData)
        .verifyZkFees(expectedServiceFees(shareBitLength));
  }

  private static long expectedServiceFees(int shareBitLength) {
    int rpcLength = shareBitLength + 5;
    long singleNodeCost = 700 + (10L * rpcLength) + 500;
    return singleNodeCost * 4 + 5000;
  }

  @CheckReturnValue
  private InteractionsAndResult addUnconfirmedOnChainInput(
      final BlockchainAddress owner,
      final int shareBitLength,
      final long blockTime,
      final byte[] additionalInformation,
      final byte[] inputData) {
    return addUnconfirmedOnChainInputWithExplicitShortname(
        owner,
        bitlengthShortname(shareBitLength),
        shareBitLength,
        blockTime,
        additionalInformation,
        inputData);
  }

  @CheckReturnValue
  private InteractionsAndResult addUnconfirmedOffChainInput(
      final BlockchainAddress owner,
      final int shareBitLength,
      final long blockTime,
      final byte[] additionalInformation,
      final byte[] inputData) {
    return addUnconfirmedInput(
            ZERO_KNOWLEDGE_INPUT_OFF_CHAIN,
            owner,
            shareBitLength,
            blockTime,
            concat(bitlengthShortname(shareBitLength), additionalInformation),
            inputData)
        .verifyZkFees(expectedServiceFees(shareBitLength));
  }

  //// Exclusive MPC computation

  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_DEFAULT =
      WasmExampleComputations.loadZkContractComputation("contracts/mult_and_extract.mc");

  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_USELESS =
      WasmExampleComputations.loadZkContractComputation("contracts/useless.mc");

  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_USELESS_5 =
      WasmExampleComputations.loadZkContractComputation("contracts/useless_with_id_5.mc");

  //// Action constants
  static final byte ACTION_0_SOME_ACTION = 0;
  static final byte ACTION_1_TO_CONTRACT_DONE = 1;
  static final byte ACTION_2_START_COMPUTATION_ONE_INPUT = 2;
  static final byte ACTION_3_NEW_COMPUTATION = 3;
  static final byte ACTION_4_OUTPUT_COMPLETE_DELETE_VARIABLE = 4;
  static final byte ACTION_5_CONTRACT_COMPLETE = 5;
  static final byte ACTION_6_TRANSFER_VARIABLE_ONE = 6;
  static final byte ACTION_7_DELETING_VARIABLE_ONE = 7;
  static final byte ACTION_8_OPEN_VARIABLE_ONE = 8;
  static final byte ACTION_9_DELETING_VARIABLE_THREE = 9;
  static final byte ACTION_10_VERIFY_ONE_PENDING_INPUT = 10;
  static final byte ACTION_11_START_COMPUTATION_NO_OUTPUTS = 11;
  static final byte ACTION_12_START_COMPUTATION_TWO_OUTPUTS = 12;
  static final byte ACTION_13_UPDATE_STATE = 13;
  static final byte ACTION_14_CALL_EVENTS = 14;
  static final byte ACTION_15_DELETING_PENDING_INPUT_ONE = 15;
  static final byte ACTION_16_VERIFY_ONE_VARIABLE = 16;
  static final byte ACTION_17_ATTEST_DATA = 17;
  static final byte ACTION_18_START_COMPUTATION_DELETE_VARIABLE = 18;
  static final byte ACTION_19_OPEN_VARIABLE_TWO = 19;
  static final byte ACTION_20_OPEN_VARIABLE_ONE_AND_TWO = 20;
  static final byte ACTION_21_DELETE_VARIABLE_ONE_AND_TWO = 21;

  static final byte[] SECRET_INPUT_LENGTHS_16 = new byte[] {0x60};
  static final byte[] SECRET_INPUT_LENGTHS_1 = new byte[] {0x62};
  static final byte[] SECRET_INPUT_LENGTHS_1024 = new byte[] {0x63};
  static final byte[] SECRET_INPUT_LENGTHS_32_32 = new byte[] {0x65};
  static final byte[] SECRET_INPUT_LENGTHS_1_SEALED_77 = new byte[] {0x69};

  private static byte[] bitlengthShortname(final int bitlength) {
    if (bitlength == 16) {
      return SECRET_INPUT_LENGTHS_16;
    } else if (bitlength == 1) {
      return SECRET_INPUT_LENGTHS_1;
    } else if (bitlength == 1024) {
      return SECRET_INPUT_LENGTHS_1024;
    } else if (bitlength == 64) {
      return SECRET_INPUT_LENGTHS_32_32;
    } else {
      throw new UnsupportedOperationException("Unsupported bitlength: " + bitlength);
    }
  }

  @Test
  public void getStateClass() {
    setWasmContract(ZK_CONTRACT_FILE);
    Assertions.assertThat(binder.getStateClass()).isEqualTo(ZkStateImmutable.class);
    Assertions.assertThat(binder.getStateClassTypeParameters())
        .containsExactly(WasmRealContractState.class, LargeByteArray.class);
  }

  @Test
  void registeredServiceFeesCannotExceedAvailableGas() {
    setWasmContract(ZK_CONTRACT_FILE);
    TestBinderContext context = createZkContext(1, DEFAULT_BLOCK_PRODUCTION_TIME, OWNER);
    binder.create(
        context,
        SafeDataOutputStream.serialize(
            stream -> {
              for (int i = 0; i < 4; i++) {
                ADDRESSES.get(i).write(stream);
                PUBLIC_KEYS.get(i).write(stream);
                stream.writeString("https://zk.com");
              }
              PREPROCESS_ADDRESS.write(stream);
              NODE_REGISTRY_ADDRESS.write(stream);
              stream.writeLong(DEFAULT_ZK_COMPUTATION_DEADLINE);
              stream.writeLong(
                  DEFAULT_AVAILABLE_GAS
                      - 3); // Need a tiny bit of gas for running contract init code
              stream.writeInt(PREPROCESSED_MATERIAL_TO_PREFETCH);
              stream.writeInt(PREPROCESSED_MATERIAL_TO_PREFETCH);
              stream.writeBoolean(DEFAULT_OPTIMISTIC_MODE);
              stream.write(WasmRealContractInvoker.INIT_HEADER);
              stream.write(new byte[1]);
            }));
    Assertions.assertThatThrownBy(
            () ->
                binder.create(
                    context,
                    SafeDataOutputStream.serialize(
                        stream -> {
                          for (int i = 0; i < 4; i++) {
                            ADDRESSES.get(i).write(stream);
                            PUBLIC_KEYS.get(i).write(stream);
                            stream.writeString("https://zk.com");
                          }
                          PREPROCESS_ADDRESS.write(stream);
                          NODE_REGISTRY_ADDRESS.write(stream);
                          stream.writeLong(DEFAULT_ZK_COMPUTATION_DEADLINE);
                          stream.writeLong(DEFAULT_AVAILABLE_GAS + 1);
                          stream.writeInt(PREPROCESSED_MATERIAL_TO_PREFETCH);
                          stream.writeInt(PREPROCESSED_MATERIAL_TO_PREFETCH);
                          stream.writeBoolean(DEFAULT_OPTIMISTIC_MODE);
                          stream.write(WasmRealContractInvoker.INIT_HEADER);
                          stream.write(new byte[1]);
                        })))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Registered service fees cannot exceed available gas (%d > %d)"
                .formatted(DEFAULT_AVAILABLE_GAS + 1, DEFAULT_AVAILABLE_GAS));
  }

  //// Tests for missing functionality

  @Test
  public void cannotReadZkDefSectionWithoutSection() {
    setWasmContract(ZK_CONTRACT_FILE);
    Assertions.assertThatThrownBy(() -> WasmRealContractInvoker.readZkVarDefSection(null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Expected variable definition section, but got null");
  }

  @Test
  public void checkCanParseSealed() {
    setWasmContract(ZK_CONTRACT_FILE);
    var bytes =
        new byte[] {
          0, 0, 0, 1, // Number of bitlength
          0, 0, 0, 9, // Bitlength 0
          1, // Is sealed
          0, // No shortname
          1, 2, 3, 4, // Associated info
        };
    var section =
        new WasmInvoker.WasmResultSection(
            WasmRealContractInvoker.SECTION_ZK_VARIABLE_DEFINITION, bytes);
    var declaration = WasmRealContractInvoker.readZkVarDefSection(section);
    Assertions.assertThat(declaration.bitLengths()).isEqualTo(List.of(9));
    Assertions.assertThat(declaration.seal()).isTrue();
    Assertions.assertThat(declaration.metadata()).containsExactly(1, 2, 3, 4);
  }

  //// Tests

  @Test
  public void initializeDirectly() {
    final Path wasmPath = pathForResource(getClass(), "contracts/average_salary.zkwa");
    final byte[] contractBytes = readFileBytes(wasmPath);
    final WasmRealContractBinder binder = new WasmRealContractBinder(contractBytes);
    Assertions.assertThat(binder).isNotNull().isInstanceOf(WasmRealContractBinder.class);
    Assertions.assertThat(binder.getContract()).isNotNull();
  }

  /** Construct binder in a way expected by the blockchain. */
  @Test
  public void initializeLikeOnchain() throws ReflectiveOperationException {
    final Path wasmPath = pathForResource(getClass(), "contracts/average_salary.zkwa");
    final byte[] contractBytes = readFileBytes(wasmPath);

    final Class<?> binderClass = WasmRealContractBinder.class;
    final Constructor<?> declaredConstructor = binderClass.getDeclaredConstructor(byte[].class);
    final Object binder = declaredConstructor.newInstance(contractBytes);

    Assertions.assertThat(binder).isNotNull().isInstanceOf(WasmRealContractBinder.class);
    Assertions.assertThat(((WasmRealContractBinder) binder).getContract()).isNotNull();
  }

  @Test
  public void serializeInput() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    var rpcBytes = new byte[] {1, 2, 3};
    var serializedBytes = WasmRealContractInvoker.serializeInput(currentState(), rpcBytes);

    final byte[] expectedBytes =
        new byte[] {
          0, // Computation status
          -1, -1, -1, -1, // Pending inputs tree id
          -2, -1, -1, -1, // Variables tree id
          -3, -1, -1, -1, // Attestations tree id
          -4, -1, -1, -1, // Subscriptions tree id
          -5, -1, -1, -1, // External events tree id
          1, 2, 3, // RPC data
        };

    Assertions.assertThat(serializedBytes).isEqualTo(expectedBytes);
  }

  @Test
  public void createAndCompute() {
    setWasmContract(ZK_CONTRACT_FILE);
    long multiplications = 100;
    long computationFees = 2 * BASE_SERVICE_FEES + multiplications * 5;
    long expectedServiceFees = DEFAULT_STAKING_FEE + computationFees;
    byte[] immediateComputeMode = new byte[] {1};
    this.create(immediateComputeMode)
        .verifyZkFees(expectedServiceFees)
        .verifyRequestTripleBatches(1);
  }

  @Test
  public void getUnconfirmedVariables() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-unconfirmed");

    Assertions.assertThat(currentState().getPendingInput(ACCOUNT_ONE)).isPresent();

    Assertions.assertThat(currentState().getPendingInput(ACCOUNT_TWO)).isEmpty();
  }

  @Test
  public void invalidOnChainInputSize() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    Assertions.assertThatThrownBy(
            () -> addUnconfirmedOnChainInput(ACCOUNT_ONE, 1, 6L, new byte[0], new byte[2]))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid number of bytes received for input");
  }

  @Test
  public void encryptedShareParsing() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    byte[] inputs = new byte[4];
    inputs[0] = 42;
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 1, 6L, new byte[0], inputs)
        .verifyRequestInputMaskBatches(1);
    final var pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getEncryptedShares()).hasSize(4);
    Assertions.assertThat(pending.getEncryptedShares().get(0)).isEqualTo(new byte[] {42});
  }

  @Test
  public void oneInputMaskBatch() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    sendInputMaskBatch(3, 33).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getInputMasks()).containsExactly(33);
    verifySerialization("real-binder-with-one-input-mask");
  }

  @Test
  public void testCallback() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    final SafeDataInputStream stream = SafeDataInputStream.createFromBytes(new byte[0]);
    final FixedList<CallbackContext.ExecutionResult> executionResult =
        FixedList.create(List.of(CallbackContext.createResult(TRANSACTION_HASH_2, true, stream)));

    final byte[] rpcBytes =
        new byte[] {
          0x40, // Callback Shortname
        };

    callback(PREPROCESS_ADDRESS, 3, rpcBytes, executionResult).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-after-callback");
  }

  @Test
  public void failOnStartingComputation() {
    // createContract
    setWasmContract(ZK_CONTRACT_FILE, COMPUTATION_USELESS_5);
    this.create(new byte[] {0}).verifyZkFees(DEFAULT_STAKING_FEE);
    Assertions.assertThat(currentState().getOpenState().getData())
        .isEqualTo(new byte[] {0, 0, 0, 0, 0, 0, 0, 0});

    // startCompute
    Assertions.assertThatCode(
            () -> emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No ZK functions with id 0; found [5]");
  }

  /**
   * Test going through the full lifecycle for a contract from creation to starting and finishing a
   * computation and finally being done. This test creates many of the json states used in the
   * following tests.
   */
  @Test
  public void fullLifeCycle() {
    // createContract
    setWasmContract(ZK_CONTRACT_FILE);
    this.create(new byte[] {0}).verifyZkFees(DEFAULT_STAKING_FEE);
    Assertions.assertThat(currentState().getOpenState().getData())
        .isEqualTo(new byte[] {0, 0, 0, 0, 0, 0, 0, 0});
    verifySerialization("real-binder-initial");

    // First triple
    sendTripleBatch(3, 1).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1);
    verifySerialization("real-binder-with-one-triple");

    // Second triple
    sendTripleBatch(3, 3).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 3);
    verifySerialization("real-binder-with-two-triple");

    // startCompute
    emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L)
        .verifyNoInteractions()
        .verifyZkFees(50_500L);
    ComputationStateImpl computationState = currentState().getComputationState();
    Assertions.assertThat(
            computationState.getComputationOutput().getVariables().stream()
                .map(PendingOutputVariable::getVariableId)
                .toList())
        .containsExactly(1);
    Assertions.assertThat(computationState.getOnComputeCompleteShortname().getData())
        .isEqualTo(new byte[] {0x70});
    verifySerialization("real-binder-computing");

    // computationDoneOptimistic
    sendOptimisticOutput(9L, 0, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimisticVerification())
        .isFalse();

    verifySerialization("real-binder-computing-one-optimistic-output");

    sendOptimisticOutput(9L, 1, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    computationState = currentState().getComputationState();
    verifyComputationState(computationState);
    verifySerialization("real-binder-await-optimistic-verification");

    // sendVerificationsOptimistic
    BlockchainAddress sender = ADDRESSES.get(2);
    long calculateFor = currentState().getComputationState().getCalculateFor();
    InteractionsAndResult beforeSelfInvocation =
        sendOptimisticVerification(calculateFor, sender, new byte[] {1})
            .verifyZkFees(0L)
            .verifyInteractWithSelf();

    verifySerialization("real-binder-with-finished-optimistic-verification-before-self-invoke");
    invokeSelf(beforeSelfInvocation.interactions().get(0), 10L)
        .verifyShortName(ON_COMPUTE_COMPLETE)
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getVariables()).hasSize(1);
    Assertions.assertThat(currentState().getVariables().get(0).getId()).isEqualTo(1);
    verifySerialization("real-binder-with-finished-optimistic-verification");

    // Send Verification
    BlockchainAddress lastEngine = ADDRESSES.get(3);
    sendOptimisticVerification(calculateFor, lastEngine, new byte[] {1})
        .verifyNoInteractionsOrFees();
    verifySerialization("real-binder-in-waiting-with-variable");

    // transferContractVariable
    emptyOpenInvocation(ACTION_6_TRANSFER_VARIABLE_ONE, ACCOUNT_FOUR).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getVariable(1).getOwner()).isEqualTo(ACCOUNT_FOUR);
    Assertions.assertThat(asLong(currentState().getVariable(1).getInformation())).isEqualTo(0);
    Assertions.assertThat(currentState().getVariable(1).isSealed()).isFalse();

    verifySerialization("real-binder-output-owned-by-four");

    // Finish computation
    emptyOpenInvocation(ACTION_5_CONTRACT_COMPLETE, ACCOUNT_ONE)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyZkGovernanceGasCosts();

    verifySerialization("real-binder-done");
  }

  @Test
  public void addUnconfirmedVariable() {
    setWasmContract(ZK_CONTRACT_FILE);

    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[] {}, createInput(16))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getEncryptedShares())
        .isNotNull();
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getShareCommitments())
        .isNull();
    Assertions.assertThat(currentState().getPendingInput(ACCOUNT_ONE).isPresent()).isTrue();
    Assertions.assertThat(currentState().getPendingInput(ACCOUNT_ONE).get().getTransaction())
        .isEqualTo(TRANSACTION_HASH);

    verifySerialization("real-binder-with-unconfirmed");
  }

  @Test
  public void confirmInputForEmptyData() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, bitlength);
    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_TWO, bitlength, 5L, new byte[0], new byte[0])
        .verifyRequestInputMaskBatches(1);
  }

  /**
   * A pending input is confirmed when three nodes sends masked shares that can successfully
   * reconstruct a consistent blinded version of the input. The fourth input is then ignored.
   */
  @Test
  public void confirmVariable() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, bitlength);
    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_TWO, bitlength, 5L, new byte[0], createInput(bitlength))
        .verifyRequestInputMaskBatches(1);

    PendingInputImpl pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getEncryptedShares()).hasSize(4);
    Assertions.assertThat(pending.getPublicKey()).isEqualTo(INPUT_KEY);

    // confirm input
    confirmInput(ADDRESSES.get(0), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(1);
    Assertions.assertThat(pending.getNodeOpenings()).hasSize(1);

    confirmInput(ADDRESSES.get(1), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(2);
    Assertions.assertThat(pending.getNodeOpenings()).hasSize(2);

    confirmInput(ADDRESSES.get(2), 5L, 1, new byte[bitlength]).verifyInteractWithSelf();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending).isNull();

    confirmInput(ADDRESSES.get(3), 5L, 1, new byte[bitlength]).verifyNoInteractionsOrFees();

    verifySerialization("real-binder-with-confirmed");
  }

  /**
   * Even if the first node never sends their confirm-input, the pending input is confirmed once the
   * remaining three nodes sends correct masked shares to reconstruct the blinded input.
   */
  @Test
  public void confirmVariableOtherOrder() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, bitlength);
    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_TWO, bitlength, 5L, new byte[0], createInput(bitlength))
        .verifyRequestInputMaskBatches(1);

    // confirm input
    confirmInput(ADDRESSES.get(3), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    PendingInputImpl pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(1);

    confirmInput(ADDRESSES.get(1), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(2);

    confirmInput(ADDRESSES.get(2), 5L, 1, new byte[bitlength]).verifyInteractWithSelf();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending).isNull();

    verifySerialization("real-binder-with-confirmed");
  }

  /**
   * Multiple inputs can be confirmed in parallel, by sending shares for different variable ids.
   * Each variable is confirmed once three correct shares have been inputted to its variable id.
   */
  @Test
  public void confirmedInterleavedInputs() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    // Preprocessing material is requested as part of the invocation
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 64, 5L, new byte[0], createInput(64))
        .verifyRequestInputMaskBatches(1);
    // Additional preprocessing material not needed
    addUnconfirmedOnChainInput(ACCOUNT_TWO, 1, 6L, new byte[0], createInput(1))
        .verifyNoInteractions();

    confirmInput(ADDRESSES.get(0), 7L, 2, new byte[1]).verifyNoInteractions();
    confirmInput(ADDRESSES.get(0), 7L, 1, new byte[64]).verifyNoInteractions();

    confirmInput(ADDRESSES.get(1), 7L, 2, new byte[1]).verifyNoInteractions();
    confirmInput(ADDRESSES.get(1), 7L, 1, new byte[64]).verifyNoInteractions();

    confirmInput(ADDRESSES.get(2), 7L, 2, new byte[1]).verifyInteractWithSelf();
    confirmInput(ADDRESSES.get(2), 7L, 1, new byte[64]).verifyInteractWithSelf();

    // No need for fourth confirmed input

    ZkClosedImpl variableOne = currentState().getVariable(1);
    ZkClosedImpl variableTwo = currentState().getVariable(2);
    Assertions.assertThat(variableOne.getBlindedInputs()).isEqualTo(new byte[64]);
    Assertions.assertThat(variableTwo.getBlindedInputs()).isEqualTo(new byte[1]);
    // Offset depends on when variables where defined, not when they were confirmed.
    Assertions.assertThat(variableOne.getInputMaskOffset()).isEqualTo(0);
    Assertions.assertThat(variableTwo.getInputMaskOffset()).isEqualTo(64);

    verifySerialization("real-binder-with-two-confirmed");
  }

  /**
   * If a single node sends a wrong share, then the pending input is not confirmed at the third
   * input, but instead when the fourth input is received.
   */
  @Test
  public void nodeSendsWrongConfirm() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, bitlength);
    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_TWO, bitlength, 5L, new byte[0], createInput(bitlength))
        .verifyRequestInputMaskBatches(1);

    confirmInput(ADDRESSES.get(0), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    PendingInputImpl pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(1);

    // Send wrong share
    byte[] wrongShare = new byte[bitlength];
    wrongShare[1] = 1;
    confirmInput(ADDRESSES.get(1), 5L, 1, wrongShare).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(2);

    confirmInput(ADDRESSES.get(2), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(3);

    confirmInput(ADDRESSES.get(3), 5L, 1, new byte[bitlength]).verifyInteractWithSelf();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending).isNull();

    ZkClosedImpl variableOne = currentState().getVariable(1);
    Assertions.assertThat(variableOne.getBlindedInputs()).isEqualTo(new byte[bitlength]);

    verifySerialization("real-binder-with-confirmed");
  }

  /** A node can only send their masked share to a pending input once. */
  @Test
  public void sendInputAgain() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_ONE, 1, 6L, new byte[0], createInput(1))
        .verifyRequestInputMaskBatches(1);
    confirmInput(ADDRESSES.get(0), 7L, 1, new byte[1]).verifyNoInteractions();

    Assertions.assertThatThrownBy(() -> confirmInput(ADDRESSES.get(0), 7L, 1, new byte[1]))
        .hasMessage("Engine is only allowed to push once");
  }

  /**
   * If a node sends reject input for a pending input a self invocation to on variable rejected is
   * made, the pending input is deleted, and further rejects and confirms for that variable id is
   * ignored.
   */
  @Test
  public void nodeRejectsPending() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(16))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1))
        .matches(not(PendingInputImpl::isRejected), "not isRejected");
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);

    InteractionsAndResult selfInvocation =
        rejectInput(ADDRESSES.get(0), 5L, 1).verifyInteractWithSelf();
    // invoke pending self invocation
    invokeSelf(selfInvocation.interactions().get(0), 5L)
        .verifyShortName(ON_VARIABLE_REJECTED)
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getPendingInputs()).isEmpty();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(2);

    confirmInput(ADDRESSES.get(1), 5L, 1, new byte[0]).verifyNoInteractionsOrFees();

    rejectInput(ADDRESSES.get(1), 6L, 1).verifyNoInteractionsOrFees();
  }

  @Test
  public void invalidInput() {
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, 16);
    readState("real-binder-initial");
    Assertions.assertThatThrownBy(
            () ->
                addUnconfirmedInput(
                    ZERO_KNOWLEDGE_INPUT_ON_CHAIN,
                    ACCOUNT_ONE,
                    32,
                    5L,
                    SECRET_INPUT_LENGTHS_16,
                    new byte[32]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected an input of sizes [16], but got sizes [32]");
  }

  /** Sending a confirm input for a variable that does not exist is ignored. */
  @Test
  public void confirmUnknownInput() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    confirmInput(ADDRESSES.get(0), 5L, 99, new byte[0]).verifyNoInteractionsOrFees();
  }

  /**
   * If all nodes send inconsistent masked input shares, due to wrong client input, then once the
   * fourth inputs is received the variable is rejected, the pending input is deleted, and a self
   * invocation to onVariableRejected is made.
   */
  @Test
  public void clientProvidesBadInput() {
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, 1);
    readState("real-binder-initial");
    BinaryExtensionFieldElementFactory factory = new BinaryExtensionFieldElementFactory();
    Polynomial<BinaryExtensionFieldElement> sharePolynomial =
        new Polynomial<>(
            List.of(factory.createElement(5), factory.createElement(2), factory.createElement(3)),
            BinaryExtensionFieldElement.ZERO);
    final List<BinaryExtensionFieldElement> badShares =
        factory.alphas().stream().map(sharePolynomial::evaluate).toList();
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 1, 5L, new byte[0], createInput(1))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1))
        .matches(not(PendingInputImpl::isRejected), "not isRejected");
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);
    confirmInput(ADDRESSES.get(0), 5L, 1, badShares.get(0).serialize()).verifyNoInteractions();

    Assertions.assertThat(currentState().getPendingInput().getValue(1))
        .matches(not(PendingInputImpl::isRejected), "not isRejected");
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);

    confirmInput(ADDRESSES.get(1), 5L, 1, badShares.get(1).serialize()).verifyNoInteractions();
    Assertions.assertThat(currentState().getPendingInput().getValue(1))
        .matches(not(PendingInputImpl::isRejected), "not isRejected");
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);

    confirmInput(ADDRESSES.get(2), 5L, 1, badShares.get(2).serialize()).verifyNoInteractions();
    Assertions.assertThat(currentState().getPendingInput().getValue(1))
        .matches(not(PendingInputImpl::isRejected), "not isRejected");
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);

    InteractionsAndResult beforeSelfInvocation =
        confirmInput(ADDRESSES.get(3), 5L, 1, badShares.get(3).serialize())
            .verifyInteractWithSelf();
    // invoke pending self invocation
    invokeSelf(beforeSelfInvocation.singleInteraction(), 6L)
        .verifyShortName(ON_VARIABLE_REJECTED)
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getPendingInputs()).isEmpty();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(2);
  }

  /** Only the zk nodes are allowed to invoke OpenMaskedInput invocation. */
  @Test
  public void nonNodeCannotConfirmOpening() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(16))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThatThrownBy(() -> confirmInput(ACCOUNT_ONE, 5L, 1, new byte[16]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Only engines allowed to call");
  }

  @Test
  public void encryptedSharesCanHaveArbitrarySize() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(123))
        .verifyRequestInputMaskBatches(1);
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(1))
        .verifyNoInteractions();
  }

  @Test
  public void offChainInput() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    addUnconfirmedOffChainInput(
            ACCOUNT_ONE,
            16,
            5L,
            new byte[0],
            SafeDataOutputStream.serialize(
                stream -> {
                  for (int i = 0; i < 4; i++) {
                    int finalI = i;
                    stream.write(Hash.create(s -> s.writeInt(finalI)).getBytes());
                  }
                }))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getShareCommitments())
        .hasSize(4);
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getEncryptedShares())
        .isNull();
    for (int i = 0; i < 4; i++) {
      int finalI = i;
      Hash hash = currentState().getPendingInput().getValue(1).getShareCommitments().get(i);
      Assertions.assertThat(hash).isEqualTo(Hash.create(stream -> stream.writeInt(finalI)));
    }
  }

  @Test
  public void contractState() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-confirmed");

    stateInvocation(1L, 23L).verifyNoInteractionsOrFees();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(23L);
    stateInvocation(23L, 37L).verifyNoInteractionsOrFees();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(37L);
    stateInvocation(37L, 74L).verifyNoInteractionsOrFees();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(74L);
  }

  @Test
  public void startComputeWaitForTriplesButWaitWhatsThatAnAdditionalInputGaspIdNotExpectThis() {
    long noOfMultiplications = 100_520;
    setWasmContract(ZK_CONTRACT_FILE, WasmExampleComputations.COMPUTATION_ADD_SUM);
    readState("real-binder-with-one-triple");

    long expectedComputationFees = 5 * noOfMultiplications + 2 * BASE_SERVICE_FEES;
    triggerComputation(ACTION_2_START_COMPUTATION_ONE_INPUT, 5L)
        .verifyZkFees(expectedComputationFees)
        .verifyRequestTripleBatches(1);
    Assertions.assertThat(currentState().getComputationState())
        .matches(ComputationState::isWaitingForTriples, "isWaitingForTriples");

    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(16))
        .verifyRequestInputMaskBatches(1);

    Assertions.assertThat(currentState().getComputationState())
        .matches(ComputationState::isWaitingForTriples, "isWaitingForTriples");

    verifySerialization("real-binder-with-one-triple-waiting-and-an-input");
  }

  @Test
  public void startComputeWaitForTriples() {
    long noOfMultiplications = 100_520;
    setWasmContract(ZK_CONTRACT_FILE, WasmExampleComputations.COMPUTATION_ADD_SUM);
    readState("real-binder-with-one-triple");

    long expectedComputationFees = 5 * noOfMultiplications + 2 * BASE_SERVICE_FEES;
    triggerComputation(ACTION_2_START_COMPUTATION_ONE_INPUT, 5L)
        .verifyZkFees(expectedComputationFees)
        .verifyRequestTripleBatches(1);
    Assertions.assertThat(currentState().getComputationState())
        .matches(ComputationState::isWaitingForTriples, "isWaitingForTriples");

    sendTripleBatch(9L, 77).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(9L);
    Assertions.assertThat(currentState().getComputationState().getTripleIds())
        .containsExactly(1, 77);
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(0);
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 77);

    sendTripleBatch(10L, 88).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 77, 88);
  }

  @Test
  public void startComputeNoMultiplications() {
    setWasmContract(ZK_CONTRACT_FILE, WasmExampleComputations.COMPUTATION_CONSTANT_TRUE);
    readState("real-binder-initial");

    triggerComputation(ACTION_2_START_COMPUTATION_ONE_INPUT, 5L)
        .verifyZkFees(2 * BASE_SERVICE_FEES)
        .verifyNoInteractions();

    ComputationStateImpl computationState = currentState().getComputationState();
    Assertions.assertThat(computationState.isWaitingForTriples()).isFalse();
    Assertions.assertThat(computationState.getTripleIds()).isEmpty();
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(0);
    Assertions.assertThat(computationState.getCalculateFor()).isEqualTo(5L);
    Assertions.assertThat(computationState.getMultiplicationCount()).isEqualTo(0);
    Assertions.assertThat(computationState.getConfirmedInputs()).isEqualTo(0);
  }

  @Test
  public void startComputeOneInputWaitingForMasksBasic() {
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, 16);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(16))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getInputMaskState().numUsedElements()).isEqualTo(16);
  }

  @Test
  public void startComputeOneInputWaitingForMasks() {
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, 16);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(16))
        .verifyRequestInputMaskBatches(1);
    confirmInput(5L, 1, new byte[16]);
    triggerComputation(ACTION_11_START_COMPUTATION_NO_OUTPUTS, 6L)
        .verifyZkFees(2 * BASE_SERVICE_FEES)
        .verifyNoInteractions();

    Assertions.assertThat(currentState().getInputMaskState().numUsedElements()).isEqualTo(16);
    Assertions.assertThat(currentState().getComputationState().getConfirmedInputs()).isEqualTo(16);
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(0);

    sendInputMaskBatch(7, 44).verifyNoInteractionsOrFees();
  }

  @Test
  public void startComputeOneMaskWaitingForMore() {
    int bitlength1 = 1;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, bitlength1);
    readState("real-binder-initial");

    // Add first unconfirmed input
    addUnconfirmedOnChainInput(ACCOUNT_ONE, bitlength1, 4L, new byte[0], createInput(bitlength1))
        .verifyRequestInputMaskBatches(1);

    // Send mask batch
    sendInputMaskBatch(4L, 1).verifyNoInteractionsOrFees();

    // Add second unconfirmed input
    int bitlength2 = 1024;
    addUnconfirmedOnChainInput(ACCOUNT_ONE, bitlength2, 5L, new byte[0], createInput(bitlength2))
        .verifyRequestInputMaskBatches(1);

    // Confirm the inputs and execute the computation
    confirmInput(5L, 1, new byte[1]);
    confirmInput(5L, 2, new byte[bitlength2]);
    emptyOpenInvocation(ACTION_11_START_COMPUTATION_NO_OUTPUTS, ACCOUNT_ONE)
        .verifyNoInteractions()
        .verifyZkFees(50_000L);
  }

  @Test
  public void startComputeWithManyInputs() {
    readState("real-binder-initial");
    int numberOfInputs = 1024;
    Assertions.assertThat(PreProcessMaterialType.BINARY_INPUT_MASK.getBatchSize())
        .isLessThan(numberOfInputs);
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, numberOfInputs);

    addUnconfirmedOnChainInput(ACCOUNT_ONE, numberOfInputs, 6L, new byte[0], createInput(1))
        .verifyRequestInputMaskBatches(2);

    confirmInput(7L, 1, new byte[numberOfInputs]);
    triggerComputation(ACTION_11_START_COMPUTATION_NO_OUTPUTS, 8L)
        .verifyZkFees(2 * BASE_SERVICE_FEES)
        .verifyNoInteractions();

    sendInputMaskBatch(9, 55).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getInputMasks()).containsExactly(55);

    sendInputMaskBatch(16, 66).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getInputMasks()).containsExactly(55, 66);
  }

  @Test
  public void startComputeWrongOutputCount() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_12_START_COMPUTATION_TWO_OUTPUTS, ACCOUNT_ONE, 5L))
        .isInstanceOf(IllegalStateException.class)
        .hasMessageContaining(
            "Variable count mismatch. Contract declared metadata for 2 variables, but computation"
                + " produced 1 variables");
  }

  @Test
  public void variableWithInformation() {
    setWasmContract(ZK_CONTRACT_FILE);
    final int bitlength = 1;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE, bitlength);
    readState("real-binder-initial");

    addUnconfirmedOnChainInputWithExplicitShortname(
            ACCOUNT_TWO,
            SECRET_INPUT_LENGTHS_1_SEALED_77,
            bitlength,
            27L,
            new byte[0],
            createInput(bitlength))
        .verifyRequestInputMaskBatches(1);

    final List<BinderInteraction> beforeSelfInvocations =
        confirmInput(27L, 1, new byte[bitlength]).stream()
            .map(InteractionsAndResult::interactions)
            .flatMap(Collection::stream)
            .toList();

    Assertions.assertThat(beforeSelfInvocations).hasSize(1);

    invokeSelf(beforeSelfInvocations.get(0), 28L)
        .verifyShortName(ON_VARIABLE_INPUTTED)
        .verifyNoInteractionsOrFees();
    verifySerialization("real-binder-with-sealed-confirmed-with-information-before-self-invoke");

    RealClosed<LargeByteArray> variable = currentState().getVariable(1);
    Assertions.assertThat(asLong(variable.getInformation())).isEqualTo(77);
    Assertions.assertThat(variable.isSealed()).isFalse();
    verifySerialization("real-binder-with-sealed-confirmed-with-information");
  }

  @Test
  public void computationDoneOptimisticNotPartOfCalculate() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");
    Assertions.assertThatThrownBy(
            () -> sendOptimisticOutput(9L, 2, defaultPartialOpens(), defaultVerificationSeed()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Node is not a part of the computation");
  }

  @Test
  public void computationDoneOptimisticWrongPartialOpens() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-one-optimistic-output");

    sendOptimisticOutput(
            9L, 1, Hash.create(stream -> stream.writeString("Wrong")), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(9L);
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(100);
  }

  @Test
  public void computationDoneOptimisticWrongVerificationSeed() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-one-optimistic-output");

    sendOptimisticOutput(
            10L,
            1,
            defaultPartialOpens(),
            Hash.create(stream -> stream.writeString("Wrong")).getBytes())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(10L);
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(100);
  }

  @Test
  public void wrongCalculateForInFinished() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-finished-optimistic-verification");

    Assertions.assertThatThrownBy(
            () ->
                sendOptimisticVerification(789L, ADDRESSES.get(3), new byte[] {0}).verifyZkFees(0L))
        .isInstanceOf(NoSuchElementException.class);
  }

  @Test
  public void sendVerificationsOptimisticWrongFirst() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-await-optimistic-verification");

    sendOptimisticVerification(9L, ADDRESSES.get(3), new byte[] {2}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimisticVerification()).isTrue();
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(0);
    verifySerialization("real-binder-await-optimistic-verification-one-illegal");
  }

  @Test
  public void sendVerificationsOptimisticWrongSecond() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-await-optimistic-verification-one-illegal");

    sendOptimisticVerification(9L, ADDRESSES.get(2), new byte[] {2}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
  }

  @Test
  public void sendVerificationsOptimisticGoodSecond() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-await-optimistic-verification-one-illegal");

    sendOptimisticVerification(9L, ADDRESSES.get(2), new byte[] {1})
        .verifyZkFees(0L)
        .verifyInteractWithSelf();

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);
  }

  @Test
  public void newComputation() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-variable");

    emptyOpenInvocation(ACTION_3_NEW_COMPUTATION, ACCOUNT_ONE, 5L)
        .verifyNoInteractions()
        .verifyZkFees(50_500L);

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
  }

  @Test
  public void contractDone() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-variable");

    emptyOpenInvocation(ACTION_5_CONTRACT_COMPLETE, ACCOUNT_ONE, 5L)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyZkGovernanceGasCosts();

    Assertions.assertThat(currentState().getCalculationStatus()).isEqualTo(CalculationStatus.DONE);
  }

  @Test
  void contractDoneWhileCalculating() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");

    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_5_CONTRACT_COMPLETE, ACCOUNT_ONE, 5L))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to start computation or move to done when not in state WAITING. Current state:"
                + " CALCULATING");

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
  }

  @Test
  void contractDoneWhileDone() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-done");

    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_5_CONTRACT_COMPLETE, ACCOUNT_ONE, 5L))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to start computation or move to done when not in state WAITING. Current state:"
                + " DONE");

    Assertions.assertThat(currentState().getCalculationStatus()).isEqualTo(CalculationStatus.DONE);
  }

  @Test
  void contractDoneWhileMalicious() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-malicious");

    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_5_CONTRACT_COMPLETE, ACCOUNT_ONE, 5L))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to start computation or move to done when not in state WAITING. Current state:"
                + " MALICIOUS_BEHAVIOUR");

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.MALICIOUS_BEHAVIOUR);
  }

  @Test
  public void unableToCalculateNotCalculating() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 7L, 2L).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    verifySerialization("real-binder-initial");
  }

  @Test
  public void unableToCalculateOtherCalculateFor() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 8L, 4L).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);
    verifySerialization("real-binder-computing");
  }

  /**
   * If a node sends unable to calculate during an optimistic computation, the computation state
   * gets reset, which turns it to a non-optimistic calculation, gives new calculateFor and assigns
   * new computation triples, but preserves the output variables and onComputeCompleteShortname.
   */
  @Test
  public void unableToCalculateTriggersFull() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);

    FixedList<PendingOutputVariable> pendingBefore =
        currentState().getComputationState().getComputationOutput().getVariables();
    Assertions.assertThat(pendingBefore).hasSize(1);
    PendingOutputVariable before = pendingBefore.get(0);
    byte[] onComputeCompleteShortnameBefore =
        currentState().getComputationState().getOnComputeCompleteShortname().getData();
    int offsetIntoTriplesBefore =
        currentState().getComputationState().getOffsetIntoFirstTripleBatch();

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 8L, 5L).verifyNoInteractions();

    FixedList<PendingOutputVariable> pendingAfter =
        currentState().getComputationState().getComputationOutput().getVariables();
    Assertions.assertThat(pendingAfter).hasSize(1);
    PendingOutputVariable after = pendingAfter.get(0);

    Assertions.assertThat(before.getVariableId()).isEqualTo(after.getVariableId());

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(8L);
    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    int multiplicationCount = currentState().getComputationState().getMultiplicationCount();
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(offsetIntoTriplesBefore + multiplicationCount);
    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(
            currentState().getComputationState().getOnComputeCompleteShortname().getData())
        .isEqualTo(onComputeCompleteShortnameBefore);

    verifySerialization("real-binder-computing-full");
  }

  @Test
  public void unableToCalculateTriggerFullAndRequestForMoreMaterial() {
    long noOfMultiplications = 100_520;
    setWasmContract(ZK_CONTRACT_FILE, WasmExampleComputations.COMPUTATION_ADD_SUM);
    readState("real-binder-initial");

    long expectedComputationFees = 5 * noOfMultiplications + 2 * BASE_SERVICE_FEES;
    long calculateFor = 9L;

    // Start compute
    triggerComputation(ACTION_2_START_COMPUTATION_ONE_INPUT, 5L)
        .verifyZkFees(expectedComputationFees)
        .verifyRequestTripleBatches(2);
    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isTrue();

    // Send missing triple batch
    sendTripleBatch(3, 1).verifyNoInteractionsOrFees();
    sendTripleBatch(9L, 77).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor())
        .isEqualTo(calculateFor);
    Assertions.assertThat(currentState().getComputationState().getTripleIds())
        .containsExactly(1, 77);
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(0);
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 77);

    // Woops, something failed
    sendUnableToCalculateInteraction(ADDRESSES.get(0), 8L, calculateFor)
        .verifyRequestTripleBatches(1);

    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isTrue();

    verifySerialization("real-binder-after-reset");
  }

  @Test
  public void unableToCalculateNonNode() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");

    Assertions.assertThatThrownBy(() -> sendUnableToCalculateInteraction(ACCOUNT_ONE, 8L, 5L))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Only engines allowed to call");
  }

  @Test
  public void startComputeWhenAlreadyComputing() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");

    Assertions.assertThatThrownBy(
            () ->
                openInvocation(
                    ACTION_2_START_COMPUTATION_ONE_INPUT,
                    ACCOUNT_ONE,
                    FunctionUtility.noOpConsumer(),
                    5L))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to start computation or move to done when not in state WAITING. Current state:"
                + " CALCULATING");
  }

  @Test
  public void transferUserVariable() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-confirmed");

    Assertions.assertThat(currentState().getVariable(1).getOwner()).isEqualTo(ACCOUNT_TWO);

    emptyOpenInvocation(ACTION_6_TRANSFER_VARIABLE_ONE, ACCOUNT_ONE).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getVariable(1).getOwner()).isEqualTo(ACCOUNT_FOUR);
  }

  @Test
  public void commitResultVariableNonEngine() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");

    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    ACCOUNT_TWO,
                    10L,
                    SafeDataOutputStream.serialize(
                        stream -> {
                          stream.writeByte(COMMIT_RESULT_VARIABLE);
                          stream.write(new byte[100]);
                        })))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Only engines allowed to call");
  }

  @Test
  public void unableToCalculateFullTriggersRetry() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-full");

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 19L, 8L).verifyNoInteractions();

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(19L);
    Assertions.assertThat(currentState().getComputationState())
        .matches(not(ComputationState::isWaitingForTriples), "isWaitingForTriples");
  }

  @Test
  public void invokeCommitsForFirstEngine() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-full");

    sendCommit(0).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-computing-full-one-commit");

    // invokeCommitsForTwoEngines
    sendCommit(3).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-computing-full-two-commits");
  }

  @Test
  public void illegalCommitsForLastEngines() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-full-two-commits");

    Hash partialOpenings = Hash.create(stream -> stream.writeString("My openings"));
    sendCommit(1, partialOpenings).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);

    sendCommit(2, partialOpenings)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyZkGovernanceGasCosts();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.MALICIOUS_BEHAVIOUR);

    verifySerialization("real-binder-malicious");
  }

  @Test
  public void oneIllegalAndOneGoodCommitForLastEngines() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-full-two-commits");

    Hash partialOpenings = Hash.create(stream -> stream.writeString("My openings"));
    sendCommit(1, partialOpenings).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);

    sendCommit(2).verifyInteractWithSelf();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);
  }

  @Test
  public void invokeCommitsForLastEngines() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-full-two-commits");

    sendCommit(1).verifyInteractWithSelf();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    List<FinishedComputation> finishedComputations = currentState().getFinishedComputations();
    Assertions.assertThat(finishedComputations).hasSize(1);
    Assertions.assertThat(finishedComputations.get(0).getCalculateFor()).isEqualTo(8L);
    Assertions.assertThat(finishedComputations.get(0).getPartialOpensHash())
        .isEqualTo(defaultPartialOpens());
    Assertions.assertThat(finishedComputations.get(0).isOptimistic()).isFalse();
    Assertions.assertThat(finishedComputations.get(0).isHandledByEngine(1)).isTrue();
    Assertions.assertThat(finishedComputations.get(0).isHandledByEngine(2)).isFalse();

    sendCommit(2).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getFinishedComputations()).isEmpty();
  }

  @Test
  public void commitResultWrongCalculation() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-full-two-commits");

    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    ADDRESSES.get(2), 15L, createCommitResultInvocation(3L, defaultPartialOpens())))
        .isInstanceOf(NoSuchElementException.class);
  }

  @Test
  public void outputContractVariableOnChain() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-variable");

    Assertions.assertThat(currentState().hasPendingOnChainOpen()).isFalse();

    emptyOpenInvocation(ACTION_8_OPEN_VARIABLE_ONE, ACCOUNT_ONE).verifyZkFees(11280L);

    Assertions.assertThat(currentState().hasPendingOnChainOpen()).isTrue();
    Assertions.assertThat(currentState().getPendingOnChainOpens()).hasSize(1);
    Assertions.assertThat(currentState().getPendingOnChainOpens().get(0).getVariableIds())
        .containsExactly(1);
    Assertions.assertThat(currentState().getPendingOnChainOpens().get(0).getOutputId())
        .isEqualTo(2);

    verifySerialization("real-binder-in-waiting-with-pending-open");
  }

  @Test
  public void multipleSimultaneousOpenings() {
    setWasmContract(ZK_CONTRACT_FILE);

    readState("real-binder-with-two-confirmed");

    Assertions.assertThat(currentState().hasPendingOnChainOpen()).isFalse();

    emptyOpenInvocation(ACTION_8_OPEN_VARIABLE_ONE, ACCOUNT_ONE).verifyZkFees(12560L);

    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().keySet()).containsExactly(3);
    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().getValue(3).getVariableIds())
        .containsExactly(1);
    emptyOpenInvocation(ACTION_19_OPEN_VARIABLE_TWO, ACCOUNT_ONE).verifyZkFees(10040L);
    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().keySet())
        .containsExactly(3, 4);
    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().getValue(3).getVariableIds())
        .containsExactly(1);
    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().getValue(4).getVariableIds())
        .containsExactly(2);
  }

  /**
   * A pending open is confirmed when three nodes sends masked shares that can successfully
   * reconstruct a consistent version of the variable. The forth input is ignored.
   */
  @Test
  public void openingFromFirst() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-pending-open");

    sendOpenVariablesSharesForEngine(0, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    verifySerialization("real-binder-pending-open-one-share");

    // openingFromRest
    sendOpenVariablesSharesForEngine(2, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    InteractionsAndResult resultWithSelfInvocation =
        sendOpenVariablesSharesForEngine(1, 10L, 2, computationOutputShares)
            .verifyInteractWithSelf();

    invokeSelf(resultWithSelfInvocation.singleInteraction(), 10L).verifyNoInteractionsOrFees();

    sendOpenVariablesSharesForEngine(3, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    byte[] openValue = currentState().getVariable(1).getOpenValue();
    Assertions.assertThat(bitsToBytes(openValue)).isEqualTo(computationOutput);

    verifySerialization("real-binder-in-waiting-with-output-opened");
  }

  /** Multiple pending opens, each opening multiple variables, can be confirmed at once. */
  @Test
  public void overlappingSimultaneousOpenings() {
    setWasmContract(ZK_CONTRACT_FILE);

    readState("real-binder-with-two-confirmed");
    emptyOpenInvocation(ACTION_20_OPEN_VARIABLE_ONE_AND_TWO, ACCOUNT_ONE).verifyZkFees(12600L);
    emptyOpenInvocation(ACTION_8_OPEN_VARIABLE_ONE, ACCOUNT_ONE).verifyZkFees(12560L);

    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().keySet())
        .containsExactly(3, 4);
    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().getValue(3).getVariableIds())
        .containsExactly(1, 2);
    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().getValue(4).getVariableIds())
        .containsExactly(1);

    Assertions.assertThat(currentState().getVariable(1).getOpenValue()).isNull();

    int opening1bitlength = 32 + 32 + 1;
    List<byte[]> opening1Shares = Collections.nCopies(4, new byte[opening1bitlength]);

    // Execute first opening
    sendOpenVariablesSharesForEngine(0, 10L, 3, opening1Shares).verifyNoInteractionsOrFees();
    sendOpenVariablesSharesForEngine(1, 10L, 3, opening1Shares).verifyNoInteractionsOrFees();
    InteractionsAndResult resultWithSelfInvocation1 =
        sendOpenVariablesSharesForEngine(3, 10L, 3, opening1Shares).verifyInteractWithSelf();
    invokeSelf(resultWithSelfInvocation1.singleInteraction(), 10L).verifyNoInteractionsOrFees();

    byte[] openedValueVar1 = currentState().getVariable(1).getOpenValue();
    Assertions.assertThat(openedValueVar1).isNotNull();
    Assertions.assertThat(currentState().getVariable(2).getOpenValue()).isNotNull();

    Assertions.assertThat(currentState().hasPendingOnChainOpen()).isTrue();
    Assertions.assertThat(currentState().getPendingOnChainOpensRaw().keySet()).containsExactly(4);

    int opening2bitlength = 32 + 32;
    List<byte[]> opening2Shares = Collections.nCopies(4, new byte[opening2bitlength]);

    // Execute second opening
    sendOpenVariablesSharesForEngine(0, 11L, 4, opening2Shares).verifyNoInteractionsOrFees();
    sendOpenVariablesSharesForEngine(1, 11L, 4, opening2Shares).verifyNoInteractionsOrFees();
    InteractionsAndResult resultWithSelfInvocation2 =
        sendOpenVariablesSharesForEngine(2, 11L, 4, opening2Shares).verifyInteractWithSelf();
    invokeSelf(resultWithSelfInvocation2.singleInteraction(), 11L).verifyNoInteractionsOrFees();
    sendOpenVariablesSharesForEngine(3, 11L, 4, opening2Shares).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getVariable(1).getOpenValue()).isEqualTo(openedValueVar1);
    Assertions.assertThat(currentState().hasPendingOnChainOpen()).isFalse();
  }

  /**
   * If a node sends an illegal share, then the pending open will not be confirmed after the third
   * node reaction, but instead after the fourth.
   */
  @Test
  public void nodesSendsIllegalOpen() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-pending-open");

    sendOpenVariablesSharesForEngine(0, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    List<byte[]> illegalShares = Collections.nCopies(4, new byte[32]);
    sendOpenVariablesSharesForEngine(2, 10L, 2, illegalShares).verifyNoInteractionsOrFees();

    sendOpenVariablesSharesForEngine(3, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    InteractionsAndResult resultWithSelfInvocation =
        sendOpenVariablesSharesForEngine(1, 10L, 2, computationOutputShares)
            .verifyInteractWithSelf();

    invokeSelf(resultWithSelfInvocation.singleInteraction(), 10L).verifyNoInteractionsOrFees();

    byte[] openValue = currentState().getVariable(1).getOpenValue();
    Assertions.assertThat(bitsToBytes(openValue)).isEqualTo(computationOutput);

    verifySerialization("real-binder-in-waiting-with-output-opened");
  }

  /** The invocation fails if trying to open a variable that has been deleted. */
  @Test
  public void openDeletedVariable() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-variable-deleted");
    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_8_OPEN_VARIABLE_ONE, ACCOUNT_ONE))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Unable to open a variable that does not exist.");
  }

  /** The binder ignores opens to wrong ids. */
  @Test
  public void sendOutputInOutputWithNoPending() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-variable");

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    sendOpenVariablesSharesForEngine(0, 10L, 7, computationOutputShares)
        .verifyNoInteractionsOrFees();
  }

  /** A node can only send their masked share to a pending open once. */
  @Test
  public void sendOutputAgain() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-pending-open-one-share");
    Assertions.assertThatThrownBy(
            () -> sendOpenVariablesSharesForEngine(0, 10L, 2, computationOutputShares))
        .hasMessage("Engine is only allowed to push once");
  }

  /** Cannot open a variable if the calculation status is DONE. */
  @Test
  public void openingFromDone() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-done");
    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_8_OPEN_VARIABLE_ONE, ACCOUNT_ONE))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to open variables when contract is in a terminal state. Current state: DONE");
  }

  /** Cannot open a variable if the calculation status is MALICIOUS. */
  @Test
  public void openingFromMalicious() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-malicious");
    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_8_OPEN_VARIABLE_ONE, ACCOUNT_ONE))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to open variables when contract is in a terminal state. Current state:"
                + " MALICIOUS_BEHAVIOUR");
  }

  @Test
  public void verifyPendingUserVariables() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-unconfirmed");

    Optional<ZkClosedImpl> unconfirmedVariable = currentState().getPendingInput(ACCOUNT_ONE);
    Assertions.assertThat(unconfirmedVariable).isPresent();
    Assertions.assertThat(unconfirmedVariable.orElseThrow().getId()).isEqualTo(1);

    emptyOpenInvocation(ACTION_10_VERIFY_ONE_PENDING_INPUT, ACCOUNT_ONE)
        .verifyNoInteractionsOrFees();
  }

  @Test
  public void verifyConfirmedUserVariables() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-confirmed");

    emptyOpenInvocation(ACTION_16_VERIFY_ONE_VARIABLE, ACCOUNT_TWO).verifyNoInteractionsOrFees();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(2);
  }

  @Test
  public void inputFailsWithoutSecretInputListener() {
    setWasmContract(ZK_CONTRACT_FILE);

    readState("real-binder-initial");
    Assertions.assertThatThrownBy(
            () ->
                addUnconfirmedOnChainInputWithExplicitShortname(
                    ACCOUNT_TWO, SECRET_INPUT_LENGTHS_16, 1, 1, new byte[0], createInput(1)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected an input of sizes [16], but got sizes [1]");
  }

  @Test
  public void callRemovedInvocations() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    for (byte invocation : REMOVED_INVOCATIONS) {
      Assertions.assertThatThrownBy(() -> invoke(ACCOUNT_ONE, 1L, new byte[] {invocation}))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Invocation with shortname 0x%02X does not exist".formatted(invocation));
    }
  }

  //// Tests for Contract without the passive hooks

  /**
   * Test going through the full lifecycle for a contract without passive hooks from creation to
   * starting and finishing a computation and finally being done. This test creates many of the json
   * states used in the following 'no passive hooks' tests.
   */
  @Test
  public void fullLifeCycleNoPassiveHooks() {
    // createContract
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    this.create(new byte[] {0}).verifyZkFees(DEFAULT_STAKING_FEE);
    Assertions.assertThat(currentState().getOpenState().getData())
        .isEqualTo(new byte[] {0, 0, 0, 0, 0, 0, 0, 0});
    verifySerialization("real-binder-initial");

    // First triple
    sendTripleBatch(3, 1).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1);
    verifySerialization("real-binder-with-one-triple");

    // Second triple
    sendTripleBatch(3, 3).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 3);
    verifySerialization("real-binder-with-two-triple");

    // startCompute
    emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L)
        .verifyNoInteractions()
        .verifyZkFees(50_500L);
    ComputationStateImpl computationState = currentState().getComputationState();
    Assertions.assertThat(
            computationState.getComputationOutput().getVariables().stream()
                .map(PendingOutputVariable::getVariableId)
                .toList())
        .containsExactly(1);
    Assertions.assertThat(computationState.getOnComputeCompleteShortname()).isNull();
    verifySerialization("real-binder-computing-no-passive-hooks");

    // computationDoneOptimistic
    sendOptimisticOutput(9L, 0, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimisticVerification())
        .isFalse();

    verifySerialization("real-binder-computing-one-optimistic-output-no-passive-hooks");

    sendOptimisticOutput(9L, 1, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    computationState = currentState().getComputationState();
    verifyComputationState(computationState);
    verifySerialization("real-binder-await-optimistic-verification-no-passive-hooks");

    // sendVerificationsOptimistic
    BlockchainAddress sender = ADDRESSES.get(2);
    long calculateFor = currentState().getComputationState().getCalculateFor();
    sendOptimisticVerification(calculateFor, sender, new byte[] {1}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getVariables()).hasSize(1);
    Assertions.assertThat(currentState().getVariables().get(0).getId()).isEqualTo(1);
    verifySerialization("real-binder-with-finished-optimistic-verification-no-passive-hooks");

    // Send Verification
    BlockchainAddress lastEngine = ADDRESSES.get(3);
    sendOptimisticVerification(calculateFor, lastEngine, new byte[] {1})
        .verifyNoInteractionsOrFees();
    verifySerialization("real-binder-in-waiting-with-variable-no-passive-hooks");

    // transferContractVariable
    emptyOpenInvocation(ACTION_6_TRANSFER_VARIABLE_ONE, ACCOUNT_FOUR).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getVariable(1).getOwner()).isEqualTo(ACCOUNT_FOUR);
    Assertions.assertThat(asLong(currentState().getVariable(1).getInformation())).isEqualTo(0);
    Assertions.assertThat(currentState().getVariable(1).isSealed()).isFalse();

    verifySerialization("real-binder-output-owned-by-four-no-passive-hooks");

    // Finish computation
    emptyOpenInvocation(ACTION_5_CONTRACT_COMPLETE, ACCOUNT_ONE)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyZkGovernanceGasCosts();

    verifySerialization("real-binder-done-no-passive-hooks");
  }

  @Test
  public void oneInputMaskBatchNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-initial");
    sendInputMaskBatch(3L, 33).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getInputMasks()).containsExactly(33);
    verifySerialization("real-binder-with-one-input-mask");
  }

  @Test
  public void testCallbackNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-initial");

    final SafeDataInputStream stream = SafeDataInputStream.createFromBytes(new byte[0]);
    final FixedList<CallbackContext.ExecutionResult> executionResult =
        FixedList.create(List.of(CallbackContext.createResult(TRANSACTION_HASH_2, true, stream)));

    final byte[] rpcBytes =
        new byte[] {
          0x40, // Callback Shortname
        };

    callback(PREPROCESS_ADDRESS, 3, rpcBytes, executionResult).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-after-callback");
  }

  @Test
  public void oneAndTwoTripleBatchNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-initial");

    // First triple
    sendTripleBatch(3, 1).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1);
    verifySerialization("real-binder-with-one-triple");

    // Second triple
    sendTripleBatch(3, 3).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 3);
    verifySerialization("real-binder-with-two-triple");
  }

  @Test
  public void confirmUnknownInputNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-initial");

    confirmInput(ADDRESSES.get(0), 5L, 99, new byte[0]).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-initial");
  }

  @Test
  public void nodeRejectsPendingNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(16))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1))
        .matches(not(PendingInputImpl::isRejected), "not isRejected");
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);
    rejectInput(ADDRESSES.get(0), 5L, 1).verifyInteractWithSelf();
    Assertions.assertThat(currentState().getPendingInputs()).isEmpty();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1);

    confirmInput(ADDRESSES.get(1), 5L, 1, new byte[0]).verifyNoInteractionsOrFees();

    rejectInput(ADDRESSES.get(1), 6L, 1).verifyNoInteractionsOrFees();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void offChainInputNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-initial");
    addUnconfirmedOffChainInput(
            ACCOUNT_ONE,
            16,
            5L,
            new byte[0],
            SafeDataOutputStream.serialize(
                stream -> {
                  for (int i = 0; i < 4; i++) {
                    int finalI = i;
                    stream.write(Hash.create(s -> s.writeInt(finalI)).getBytes());
                  }
                }))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getShareCommitments())
        .hasSize(4);
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getEncryptedShares())
        .isNull();
    for (int i = 0; i < 4; i++) {
      int finalI = i;
      Hash hash = currentState().getPendingInput().getValue(1).getShareCommitments().get(i);
      Assertions.assertThat(hash).isEqualTo(Hash.create(stream -> stream.writeInt(finalI)));
    }
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void startComputeWaitForTriplesNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO, WasmExampleComputations.COMPUTATION_ADD_SUM);
    readState("real-binder-with-one-triple");

    emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L)
        .verifyRequestTripleBatches(1);
    Assertions.assertThat(currentState().getComputationState())
        .matches(ComputationState::isWaitingForTriples, "isWaitingForTriples");

    sendTripleBatch(9L, 77).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(9L);
    Assertions.assertThat(currentState().getComputationState().getTripleIds())
        .containsExactly(1, 77);
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(0);
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 77);

    sendTripleBatch(10L, 88).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 77, 88);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void computationDoneOptimisticNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-no-passive-hooks");

    sendOptimisticOutput(9L, 0, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimisticVerification())
        .isFalse();

    verifySerialization("real-binder-computing-one-optimistic-output-no-passive-hooks");

    sendOptimisticOutput(9L, 1, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    ComputationStateImpl computationState = currentState().getComputationState();
    verifyComputationState(computationState);
    verifySerialization("real-binder-await-optimistic-verification-no-passive-hooks");
  }

  @Test
  public void computationDoneOptimisticWrongPartialOpensNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-one-optimistic-output-no-passive-hooks");

    sendOptimisticOutput(
            9L, 1, Hash.create(stream -> stream.writeString("Wrong")), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(9L);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void computationDoneOptimisticWrongVerificationSeedNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-one-optimistic-output-no-passive-hooks");

    sendOptimisticOutput(
            10L,
            1,
            defaultPartialOpens(),
            Hash.create(stream -> stream.writeString("Wrong")).getBytes())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(10L);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void sendVerificationsOptimisticWrongFirstNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-await-optimistic-verification-no-passive-hooks");

    sendOptimisticVerification(9L, ADDRESSES.get(3), new byte[] {2}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimisticVerification()).isTrue();
    verifySerialization("real-binder-await-optimistic-verification-one-illegal-no-passive-hooks");
  }

  @Test
  public void sendVerificationsOptimisticWrongSecondNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-await-optimistic-verification-one-illegal-no-passive-hooks");

    sendOptimisticVerification(9L, ADDRESSES.get(2), new byte[] {2}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void sendVerificationsOptimisticGoodSecondNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-await-optimistic-verification-one-illegal-no-passive-hooks");

    sendOptimisticVerification(9L, ADDRESSES.get(2), new byte[] {1}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void unableToCalculateNotCalculatingNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-initial");

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 7L, 2L).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    verifySerialization("real-binder-initial");
  }

  @Test
  public void unableToCalculateOtherCalculateForNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-no-passive-hooks");

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 8L, 4L).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);
    verifySerialization("real-binder-computing-no-passive-hooks");
  }

  @Test
  public void unableToCalculateTriggersFullNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-no-passive-hooks");

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);

    FixedList<PendingOutputVariable> pendingBefore =
        currentState().getComputationState().getComputationOutput().getVariables();
    Assertions.assertThat(pendingBefore).hasSize(1);
    PendingOutputVariable before = pendingBefore.get(0);

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 8L, 5L).verifyNoInteractions();

    FixedList<PendingOutputVariable> pendingAfter =
        currentState().getComputationState().getComputationOutput().getVariables();
    Assertions.assertThat(pendingAfter).hasSize(1);
    PendingOutputVariable after = pendingAfter.get(0);

    Assertions.assertThat(before.getVariableId()).isEqualTo(after.getVariableId());

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(8L);
    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getOnComputeCompleteShortname())
        .isNull();

    verifySerialization("real-binder-computing-full-no-passive-hooks");
  }

  @Test
  public void unableToCalculateFullTriggersRetryNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-full-no-passive-hooks");

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 19L, 8L).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(19L);
    Assertions.assertThat(currentState().getComputationState())
        .matches(not(ComputationState::isWaitingForTriples), "not isWaitingForTriples");
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void invokeCommitsForFirstEngineNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-full-no-passive-hooks");

    sendCommit(0).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-computing-full-one-commit-no-passive-hooks");

    // invokeCommitsForTwoEnginesNoPassiveHooks
    sendCommit(3).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-computing-full-two-commits-no-passive-hooks");
  }

  @Test
  public void illegalCommitsForLastEnginesNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-full-two-commits-no-passive-hooks");

    Hash partialOpenings = Hash.create(stream -> stream.writeString("My openings"));
    sendCommit(1, partialOpenings).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);

    sendCommit(2, partialOpenings)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyZkGovernanceGasCosts();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.MALICIOUS_BEHAVIOUR);

    verifySerialization("real-binder-malicious-no-passive-hooks");
  }

  @Test
  public void oneIllegalAndOneGoodCommitForLastEnginesNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-full-two-commits-no-passive-hooks");

    Hash partialOpenings = Hash.create(stream -> stream.writeString("My openings"));
    sendCommit(1, partialOpenings).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);

    sendCommit(2).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void invokeCommitsForLastEnginesNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-computing-full-two-commits-no-passive-hooks");

    sendCommit(1).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    List<FinishedComputation> finishedComputations = currentState().getFinishedComputations();
    Assertions.assertThat(finishedComputations).hasSize(1);
    Assertions.assertThat(finishedComputations.get(0).getCalculateFor()).isEqualTo(8L);
    Assertions.assertThat(finishedComputations.get(0).getPartialOpensHash())
        .isEqualTo(defaultPartialOpens());
    Assertions.assertThat(finishedComputations.get(0).isOptimistic()).isFalse();
    Assertions.assertThat(finishedComputations.get(0).isHandledByEngine(1)).isTrue();
    Assertions.assertThat(finishedComputations.get(0).isHandledByEngine(2)).isFalse();

    sendCommit(2).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getFinishedComputations()).isEmpty();
  }

  @Test
  public void openingFromRestNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-pending-open-one-share");

    sendOpenVariablesSharesForEngine(2, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    sendOpenVariablesSharesForEngine(1, 10L, 2, computationOutputShares).verifyInteractWithSelf();

    sendOpenVariablesSharesForEngine(3, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    byte[] openValue = currentState().getVariable(1).getOpenValue();
    Assertions.assertThat(bitsToBytes(openValue)).isEqualTo(computationOutput);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(3L);
  }

  @Test
  public void openingFromFirstNoPassiveHooks() {
    setWasmContract(ZK_CONTRACT_FILE_NO_AUTO);
    readState("real-binder-in-waiting-with-pending-open");

    sendOpenVariablesSharesForEngine(0, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    verifySerialization("real-binder-pending-open-one-share");
  }

  @Test
  public void confirmVariableNoPassiveHooks() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE_NO_AUTO, bitlength);
    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_TWO, bitlength, 5L, new byte[0], createInput(bitlength))
        .verifyRequestInputMaskBatches(1);

    PendingInputImpl pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getEncryptedShares()).hasSize(4);

    // confirm input
    confirmInput(ADDRESSES.get(0), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(1);
    Assertions.assertThat(pending.getNodeOpenings()).hasSize(1);

    confirmInput(ADDRESSES.get(1), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(2);
    Assertions.assertThat(pending.getNodeOpenings()).hasSize(2);

    confirmInput(ADDRESSES.get(2), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending).isNull();

    confirmInput(ADDRESSES.get(3), 5L, 1, new byte[bitlength]).verifyNoInteractionsOrFees();
  }

  //// Tests for Contract where every interesting call throws

  @Test
  public void oneInputMaskBatchThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-initial");
    sendInputMaskBatch(3L, 33).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getInputMasks()).containsExactly(33);
    verifySerialization("real-binder-with-one-input-mask");
  }

  @Test
  public void testCallbackThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-initial");

    final SafeDataInputStream stream = SafeDataInputStream.createFromBytes(new byte[0]);
    final FixedList<CallbackContext.ExecutionResult> executionResult =
        FixedList.create(List.of(CallbackContext.createResult(TRANSACTION_HASH_2, true, stream)));

    final byte[] rpcBytes =
        new byte[] {
          0x40 // Callback Shortname
        };

    callback(PREPROCESS_ADDRESS, 3, rpcBytes, executionResult).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-after-callback");
  }

  @Test
  public void oneAndTwoTripleBatchThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-initial");

    // First triple
    sendTripleBatch(3, 1).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1);
    verifySerialization("real-binder-with-one-triple");

    // Second triple
    sendTripleBatch(3, 3).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 3);
    verifySerialization("real-binder-with-two-triple");
  }

  @Test
  public void confirmUnknownInputThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-initial");

    confirmInput(ADDRESSES.get(0), 5L, 99, new byte[0]).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-initial");
  }

  @Test
  public void nodeRejectsPendingThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[0], createInput(16))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1))
        .matches(not(PendingInputImpl::isRejected), "not isRejected");
    Assertions.assertThat(currentState().getPendingInputs()).hasSize(1);
    rejectInput(ADDRESSES.get(0), 5L, 1).verifyInteractWithSelf();
    Assertions.assertThat(currentState().getPendingInputs()).isEmpty();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1);

    confirmInput(ADDRESSES.get(1), 5L, 1, new byte[0]).verifyNoInteractionsOrFees();

    rejectInput(ADDRESSES.get(1), 6L, 1).verifyNoInteractionsOrFees();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void offChainInputThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-initial");
    addUnconfirmedOffChainInput(
            ACCOUNT_ONE,
            16,
            5L,
            new byte[0],
            SafeDataOutputStream.serialize(
                stream -> {
                  for (int i = 0; i < 4; i++) {
                    int finalI = i;
                    stream.write(Hash.create(s -> s.writeInt(finalI)).getBytes());
                  }
                }))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getShareCommitments())
        .hasSize(4);
    Assertions.assertThat(currentState().getPendingInput().getValue(1).getEncryptedShares())
        .isNull();
    for (int i = 0; i < 4; i++) {
      int finalI = i;
      Hash hash = currentState().getPendingInput().getValue(1).getShareCommitments().get(i);
      Assertions.assertThat(hash).isEqualTo(Hash.create(stream -> stream.writeInt(finalI)));
    }
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void startComputeWaitForTriplesThrowOnEverything() {
    setWasmContract(
        ZK_CONTRACT_FILE_THROW_ON_EVERYTHING, WasmExampleComputations.COMPUTATION_ADD_SUM);
    readState("real-binder-with-one-triple");

    emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L)
        .verifyRequestTripleBatches(1);
    Assertions.assertThat(currentState().getComputationState())
        .matches(ComputationState::isWaitingForTriples, "isWaitingForTriples");

    sendTripleBatch(9L, 77).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(9L);
    Assertions.assertThat(currentState().getComputationState().getTripleIds())
        .containsExactly(1, 77);
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(0);
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 77);

    sendTripleBatch(10L, 88).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getComputationTriples()).containsExactly(1, 77, 88);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void computationDoneOptimisticThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing");

    sendOptimisticOutput(9L, 0, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimisticVerification())
        .isFalse();

    verifySerialization("real-binder-computing-one-optimistic-output");

    sendOptimisticOutput(9L, 1, defaultPartialOpens(), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    ComputationStateImpl computationState = currentState().getComputationState();
    verifyComputationState(computationState);
    verifySerialization("real-binder-await-optimistic-verification");
  }

  @Test
  public void computationDoneOptimisticWrongPartialOpensThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing-one-optimistic-output");

    sendOptimisticOutput(
            9L, 1, Hash.create(stream -> stream.writeString("Wrong")), defaultVerificationSeed())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(9L);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void computationDoneOptimisticWrongVerificationSeedThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing-one-optimistic-output");

    sendOptimisticOutput(
            10L,
            1,
            defaultPartialOpens(),
            Hash.create(stream -> stream.writeString("Wrong")).getBytes())
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(10L);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void sendVerificationsOptimisticWrongFirstThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-await-optimistic-verification");

    sendOptimisticVerification(9L, ADDRESSES.get(3), new byte[] {2}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState())
        .matches(ComputationStateImpl::isOptimisticVerification, "isOptimisticVerification");
    verifySerialization("real-binder-await-optimistic-verification-one-illegal");
  }

  @Test
  public void sendVerificationsOptimisticWrongSecondThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-await-optimistic-verification-one-illegal");

    sendOptimisticVerification(9L, ADDRESSES.get(2), new byte[] {2}).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void sendVerificationsOptimisticGoodSecondThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-await-optimistic-verification-one-illegal");

    sendOptimisticVerification(9L, ADDRESSES.get(2), new byte[] {1})
        .verifyZkFees(0L)
        .verifyInteractWithSelf();

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void unableToCalculateNotCalculatingThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-initial");

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 7L, 2L).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    verifySerialization("real-binder-initial");
  }

  @Test
  public void unableToCalculateOtherCalculateForThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing");

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 8L, 4L).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);
    verifySerialization("real-binder-computing");
  }

  @Test
  public void unableToCalculateTriggersFullThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing");

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(5L);

    FixedList<PendingOutputVariable> pendingBefore =
        currentState().getComputationState().getComputationOutput().getVariables();
    Assertions.assertThat(pendingBefore).hasSize(1);
    PendingOutputVariable before = pendingBefore.get(0);
    byte[] onComputeCompleteShortnameBefore =
        currentState().getComputationState().getOnComputeCompleteShortname().getData();

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 8L, 5L).verifyNoInteractions();

    FixedList<PendingOutputVariable> pendingAfter =
        currentState().getComputationState().getComputationOutput().getVariables();
    Assertions.assertThat(pendingAfter).hasSize(1);
    PendingOutputVariable after = pendingAfter.get(0);

    Assertions.assertThat(before.getVariableId()).isEqualTo(after.getVariableId());

    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);
    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(8L);
    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isFalse();
    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(currentState().getComputationState().getOffsetIntoFirstTripleBatch())
        .isEqualTo(100);
    Assertions.assertThat(
            currentState().getComputationState().getOnComputeCompleteShortname().getData())
        .isEqualTo(onComputeCompleteShortnameBefore);
    verifySerialization("real-binder-computing-full");

    // invokeCommitsForFirstEngineThrowOnEverything
    sendCommit(0).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-computing-full-one-commit");

    // invokeCommitsForTwoEnginesThrowOnEverything
    sendCommit(3).verifyNoInteractionsOrFees();
    verifySerialization("real-binder-computing-full-two-commits");
  }

  @Test
  public void unableToCalculateFullTriggersRetryThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing-full");

    sendUnableToCalculateInteraction(ADDRESSES.get(0), 19L, 8L).verifyNoInteractions();

    Assertions.assertThat(currentState().getComputationState().getCalculateFor()).isEqualTo(19L);
    Assertions.assertThat(currentState().getComputationState())
        .matches(not(ComputationState::isWaitingForTriples), "not isWaitingForTriples");
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void illegalCommitsForLastEnginesThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing-full-two-commits");

    Hash partialOpenings = Hash.create(stream -> stream.writeString("My openings"));
    sendCommit(1, partialOpenings).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);

    sendCommit(2, partialOpenings)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyZkGovernanceGasCosts();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.MALICIOUS_BEHAVIOUR);

    verifySerialization("real-binder-malicious");
  }

  @Test
  public void oneIllegalAndOneGoodCommitForLastEnginesThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing-full-two-commits");

    Hash partialOpenings = Hash.create(stream -> stream.writeString("My openings"));
    sendCommit(1, partialOpenings).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);

    sendCommit(2).verifyInteractWithSelf();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(1L);
  }

  @Test
  public void invokeCommitsForLastEnginesThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-computing-full-two-commits");

    sendCommit(1).verifyInteractWithSelf();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    List<FinishedComputation> finishedComputations = currentState().getFinishedComputations();
    Assertions.assertThat(finishedComputations).hasSize(1);
    Assertions.assertThat(finishedComputations.get(0).getCalculateFor()).isEqualTo(8L);
    Assertions.assertThat(finishedComputations.get(0).getPartialOpensHash())
        .isEqualTo(defaultPartialOpens());
    Assertions.assertThat(finishedComputations.get(0).isOptimistic()).isFalse();
    Assertions.assertThat(finishedComputations.get(0).isHandledByEngine(1)).isTrue();
    Assertions.assertThat(finishedComputations.get(0).isHandledByEngine(2)).isFalse();

    sendCommit(2).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getFinishedComputations()).isEmpty();
  }

  @Test
  public void openingFromRestThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-pending-open-one-share");

    sendOpenVariablesSharesForEngine(2, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    sendOpenVariablesSharesForEngine(1, 10L, 2, computationOutputShares).verifyInteractWithSelf();

    sendOpenVariablesSharesForEngine(3, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    byte[] openValue = currentState().getVariable(1).getOpenValue();
    Assertions.assertThat(bitsToBytes(openValue)).isEqualTo(computationOutput);
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(3L);
  }

  @Test
  public void openingFromFirstThrowOnEverything() {
    setWasmContract(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING);
    readState("real-binder-in-waiting-with-pending-open");

    sendOpenVariablesSharesForEngine(0, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    verifySerialization("real-binder-pending-open-one-share");
  }

  @Test
  public void confirmVariableThrowOnEverything() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE_THROW_ON_EVERYTHING, bitlength);
    readState("real-binder-initial");

    addUnconfirmedOnChainInput(ACCOUNT_TWO, bitlength, 5L, new byte[0], createInput(bitlength))
        .verifyRequestInputMaskBatches(1);

    PendingInputImpl pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getEncryptedShares()).hasSize(4);

    // confirm input
    confirmInput(ADDRESSES.get(0), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(1);
    Assertions.assertThat(pending.getNodeOpenings()).hasSize(1);

    confirmInput(ADDRESSES.get(1), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getNodeReactions()).isEqualTo(2);
    Assertions.assertThat(pending.getNodeOpenings()).hasSize(2);

    confirmInput(ADDRESSES.get(2), 5L, 1, new byte[bitlength]).verifyInteractWithSelf();
    pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending).isNull();

    confirmInput(ADDRESSES.get(3), 5L, 1, new byte[bitlength]).verifyNoInteractionsOrFees();
  }

  //// Tests for gas top up

  @Test
  public void invokeEmptyTopsUpGas() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    ZkStateImmutable previousState = currentState();

    InteractionsAndResult result = invoke(ACCOUNT_TWO, 1, new byte[0]).verifyNoInteractionsOrFees();
    Assertions.assertThat(result.interactions()).isEmpty();
    Assertions.assertThat(result.callResult()).isNull();
    Assertions.assertThat(result.registeredZkFees()).isEqualTo(0);
    Assertions.assertThat(currentState().getOpenState().getData())
        .isEqualTo(previousState.getOpenState().getData());
  }

  //// Tests for computation deadline logic and calculating scores

  @Test
  void nodeRegistryAsksForComputationDeadline() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    Assertions.assertThatThrownBy(
            () -> invoke(NODE_REGISTRY_ADDRESS, 4, new byte[] {GET_COMPUTATION_DEADLINE}))
        .hasMessage("Deprecated");
  }

  @Test
  void nodeRegistryAsksForComputationDeadlineWrongInvocationByte() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    Assertions.assertThatThrownBy(
            () -> invoke(NODE_REGISTRY_ADDRESS, 4, new byte[] {(byte) 0xFF}).callResult())
        .withFailMessage("Invocation with shortname 0x04 does not exist")
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void computationCannotStartAfterDeadlineReached() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-one-triple");

    Assertions.assertThatThrownBy(
            () ->
                emptyOpenInvocation(
                    ACTION_11_START_COMPUTATION_NO_OUTPUTS,
                    ACCOUNT_ONE,
                    5L,
                    DEFAULT_ZK_COMPUTATION_DEADLINE + 1))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to create inputs and start computations when the ZK computation deadline for"
                + " the contract has been exceeded");
  }

  /** Active calculation on deadline results in failure status. */
  @Test
  public void activeCalculationWhenDeadlineReachedSendFailure() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing");

    emptyOpenInvocation(ACTION_0_SOME_ACTION, ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE));
    Assertions.assertThat(currentState().getCalculationStatus()).isEqualTo(CalculationStatus.DONE);
  }

  @Test
  void computationIsSetToDoneAndNoticeSendAfterDeadlineReached() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-variable");
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    emptyOpenInvocation(ACTION_0_SOME_ACTION, ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS))
        .verifyZkGovernanceGasCosts();
    Assertions.assertThat(currentState().getCalculationStatus()).isEqualTo(CalculationStatus.DONE);
  }

  /** Missing zk node interaction for on chain pending open results in failure status. */
  @Test
  void computationIsSetToDoneAndNoticeSentEventWithPendingOpen() {
    setWasmContract(ZK_CONTRACT_FILE);

    readState("real-binder-pending-open-one-share");
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.WAITING);

    emptyOpenInvocation(ACTION_0_SOME_ACTION, ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE));
    Assertions.assertThat(currentState().getCalculationStatus()).isEqualTo(CalculationStatus.DONE);
  }

  /** Missing zk node interaction for on chain pending input results in failure status. */
  @Test
  void zkNodeScoresMissingOnChainPendingInput() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE_NO_AUTO, bitlength);
    readState("real-binder-initial");
    addUnconfirmedOnChainInput(ACCOUNT_TWO, bitlength, 5L, new byte[0], createInput(bitlength))
        .verifyRequestInputMaskBatches(1);

    PendingInputImpl pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getEncryptedShares()).hasSize(4);

    // confirm input
    confirmInput(ADDRESSES.get(0), 5L, 1, new byte[bitlength]).verifyNoInteractions();
    confirmInput(ADDRESSES.get(2), 5L, 1, new byte[bitlength]).verifyNoInteractions();

    emptyInvocation(ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.FAILURE));
  }

  /** Missing zk node interaction for off chain pending input does not result in failure status. */
  @Test
  void zkNodeScoresMissingOffChainPendingInputStillSuccess() {
    final int bitlength = 64;
    setWasmContractWithSecretInputs(ZK_CONTRACT_FILE_NO_AUTO, bitlength);
    readState("real-binder-initial");
    addUnconfirmedOffChainInput(
            ACCOUNT_TWO,
            bitlength,
            5L,
            new byte[0],
            SafeDataOutputStream.serialize(
                stream -> {
                  for (int i = 0; i < 4; i++) {
                    int finalI = i;
                    stream.write(Hash.create(s -> s.writeInt(finalI)).getBytes());
                  }
                }))
        .verifyRequestInputMaskBatches(1);

    PendingInputImpl pending = currentState().getPendingInput().getValue(1);
    Assertions.assertThat(pending.getShareCommitments()).hasSize(4);

    emptyInvocation(ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS));
  }

  /** Missing zk node interaction for attestations results in failure status. */
  @Test
  void zkNodeScoresMissingAttestations() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-two-attestation-requests");

    byte[] blob = new byte[] {1, 2, 3, 4, 5};
    byte[] anotherBlob = new byte[] {1, 2, 3, 4, 5, 6, 7};
    invoke(ADDRESSES.get(3), 36L, addAttestation(blob, 1, 3)).verifyNoInteractionsOrFees();
    invoke(ADDRESSES.get(3), 37L, addAttestation(anotherBlob, 2, 3)).verifyNoInteractionsOrFees();
    invoke(ADDRESSES.get(2), 38L, addAttestation(blob, 1, 2)).verifyNoInteractionsOrFees();

    emptyInvocation(ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS));
  }

  /** Missing zk node interaction for finished computation results in failure status. */
  @Test
  void zkNodeScoresFinishedComputation() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-finished-optimistic-verification");

    emptyInvocation(ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.FAILURE));
  }

  /** If no required inputs results in success status. */
  @Test
  void zkNodeScoresNoInputsSuccess() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    emptyInvocation(ACCOUNT_ONE, 5L, DEFAULT_ZK_COMPUTATION_DEADLINE + 1L)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS,
                EnginesStatus.ZkNodeScoreStatus.SUCCESS));
  }

  //// Tests for variable deletion

  @Test
  public void deleteContractVariable() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-variable");

    emptyOpenInvocation(ACTION_7_DELETING_VARIABLE_ONE, ACCOUNT_FOUR).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getVariable(1)).isNull();
  }

  @Test
  public void deleteUserVariable() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-confirmed");

    Assertions.assertThat(currentState().getVariable(1).getOwner()).isEqualTo(ACCOUNT_TWO);
    emptyOpenInvocation(ACTION_7_DELETING_VARIABLE_ONE, ACCOUNT_ONE).verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getVariable(1)).isNull();
  }

  @Test
  public void deletePendingUserVariable() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-unconfirmed");
    final int unconfirmedVariableId = 1;
    final byte[] maskedInput = new byte[16];

    Optional<ZkClosedImpl> unconfirmedVariable = currentState().getPendingInput(ACCOUNT_ONE);
    Assertions.assertThat(unconfirmedVariable).isPresent();
    Assertions.assertThat(unconfirmedVariable.orElseThrow().getId()).isEqualTo(1);

    emptyOpenInvocation(ACTION_15_DELETING_PENDING_INPUT_ONE, ACCOUNT_ONE)
        .verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getPendingInput(ACCOUNT_ONE)).isEmpty();

    // Ensure input confirmations from nodes are ignored
    verifySerialization("real-binder-with-variable-deleted");
    confirmInput(25L, unconfirmedVariableId, maskedInput);
    verifySerialization("real-binder-with-variable-deleted");
  }

  @Test
  void deleteTwoVariables() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-two-confirmed");

    Assertions.assertThat(currentState().getVariables()).hasSize(2);

    emptyOpenInvocation(ACTION_21_DELETE_VARIABLE_ONE_AND_TWO, ACCOUNT_ONE)
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getVariables()).hasSize(0);
  }

  @Test
  void runComputationDeleteNeededVariable() {
    setWasmContract(ZK_CONTRACT_FILE, WasmExampleComputations.COMPUTATION_VARIABLE_ONE);
    readState("real-binder-with-confirmed");

    Assertions.assertThat(currentState().getVariable(1)).isNotNull();
    Assertions.assertThatThrownBy(
            () -> emptyOpenInvocation(ACTION_18_START_COMPUTATION_DELETE_VARIABLE, ACCOUNT_ONE))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Could not load SBI32 value with id 1");
    Assertions.assertThat(currentState().getVariable(1)).isNotNull();
  }

  @Test
  public void deleteVariablePendingOutput() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-pending-open-one-share");

    Assertions.assertThat(currentState().getPendingOnChainOpens()).hasSize(1);
    Assertions.assertThat(currentState().getVariables()).hasSize(1);
    emptyOpenInvocation(ACTION_7_DELETING_VARIABLE_ONE, ACCOUNT_ONE).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getPendingOnChainOpens()).hasSize(1);
    Assertions.assertThat(currentState().getVariables()).isEmpty();

    sendOpenVariablesSharesForEngine(2, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();
    sendOpenVariablesSharesForEngine(1, 10L, 2, computationOutputShares).verifyInteractWithSelf();
    sendOpenVariablesSharesForEngine(3, 10L, 2, computationOutputShares)
        .verifyNoInteractionsOrFees();

    Assertions.assertThat(currentState().getPendingOnChainOpens()).isEmpty();
    Assertions.assertThat(currentState().getVariables()).isEmpty();
  }

  //// Tests for attestation data

  @Test
  void attestationMatchesExpectedOnEthereum() {
    BlockchainAddress pbcContract =
        BlockchainAddress.fromString("00339896577caf2fc1cdcb1b144683e1312b791c24");
    Hash hash =
        RealContractBinder.createAttestationHash(
            pbcContract,
            SafeDataOutputStream.serialize(
                ss -> {
                  ss.writeInt(42);
                  ss.writeInt(123);
                }));
    Assertions.assertThat(hash)
        .isEqualTo(
            Hash.fromString("c351b01a0d79cebf0c4207a18ab8f8f2d2f256faa83c496438ff5c307a19f5eb"));
  }

  @Test
  void requestAttestation() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    byte[] blob = new byte[] {1, 2, 3, 4, 5};
    long registeredAttestationFees =
        openInvocation(
                ACTION_17_ATTEST_DATA,
                ADDRESSES.get(0),
                rpc -> rpc.writeDynamicBytes(blob),
                34L,
                123)
            .registeredZkFees();
    Assertions.assertThat(registeredAttestationFees).isEqualTo(25_000L);
    Assertions.assertThat(currentState().getAttestations()).hasSize(1);
    Assertions.assertThat(currentState().getAttestations().get(0).getData()).isEqualTo(blob);
    Assertions.assertThat(currentState().getAttestation(1).getData()).isEqualTo(blob);
    byte[] anotherBlob = new byte[] {1, 2, 3, 4, 5, 6, 7};
    registeredAttestationFees =
        openInvocation(
                ACTION_17_ATTEST_DATA,
                ADDRESSES.get(0),
                rpc -> rpc.writeDynamicBytes(anotherBlob),
                35L,
                124)
            .registeredZkFees();
    Assertions.assertThat(registeredAttestationFees).isEqualTo(25_000L);
    Assertions.assertThat(currentState().getAttestations()).hasSize(2);

    Assertions.assertThat(currentState().getAttestations().get(1).getData()).isEqualTo(anotherBlob);
    Assertions.assertThat(currentState().getAttestation(2).getData()).isEqualTo(anotherBlob);
    verifySerialization("real-binder-with-two-attestation-requests");
  }

  @Test
  void updateAndFinalizeAttestations() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-two-attestation-requests");

    byte[] blob = new byte[] {1, 2, 3, 4, 5};
    invoke(ADDRESSES.get(0), 36L, addAttestation(blob, 1, 3)).verifyNoInteractionsOrFees();
    DataAttestation attestation = currentState().getAttestations().get(0);
    Assertions.assertThat(attestation.getSignature(3)).isNotNull();

    invoke(ADDRESSES.get(0), 37L, addAttestation(blob, 1, 1)).verifyNoInteractionsOrFees();
    attestation = currentState().getAttestations().get(0);
    Assertions.assertThat(attestation.getSignature(1)).isNotNull();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(2);
    final InteractionsAndResult noSelfInvocation =
        invoke(ADDRESSES.get(0), 38L, addAttestation(blob, 1, 2)).verifyNoInteractionsOrFees();
    attestation = currentState().getAttestations().get(0);
    Assertions.assertThat(attestation.getSignature(2)).isNotNull();
    Assertions.assertThat(noSelfInvocation.interactions()).isEmpty();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(2);

    final InteractionsAndResult beforeSelfInvocation =
        invoke(ADDRESSES.get(0), 39L, addAttestation(blob, 1, 0)).verifyInteractWithSelf();
    invokeSelf(beforeSelfInvocation.interactions().get(0), 40L)
        .verifyShortName(ON_ATTESTATION_COMPLETE)
        .verifyNoInteractionsOrFees();
    attestation = currentState().getAttestations().get(0);
    Assertions.assertThat(attestation.getSignature(0)).isNotNull();
    Assertions.assertThat(getOpenStateAsLong()).isEqualTo(3);
  }

  @Test
  void invalidAttestations() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-two-attestation-requests");

    Assertions.assertThatThrownBy(
            () -> invoke(ADDRESSES.get(0), 36L, addAttestation(new byte[] {1, 2, 3}, 3, 0)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Engine trying to attest invalid attestation");

    Assertions.assertThatThrownBy(
            () -> invoke(ADDRESSES.get(0), 36L, addAttestation(new byte[] {1, 2, 3}, 1, 0)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Attestation from unknown engine");
  }

  @Test
  void duplicateAttestations() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-two-attestation-requests");

    byte[] blob = new byte[] {1, 2, 3, 4, 5};
    invoke(ADDRESSES.get(0), 39L, addAttestation(blob, 1, 0)).verifyNoInteractionsOrFees();
    invoke(ADDRESSES.get(0), 39L, addAttestation(blob, 1, 1)).verifyNoInteractionsOrFees();
    invoke(ADDRESSES.get(0), 39L, addAttestation(blob, 1, 2)).verifyNoInteractionsOrFees();
    InteractionsAndResult beforeSelfInvocation =
        invoke(ADDRESSES.get(0), 40L, addAttestation(blob, 1, 3)).verifyInteractWithSelf();
    invokeSelf(beforeSelfInvocation.singleInteraction(), 41L)
        .verifyShortName(ON_ATTESTATION_COMPLETE)
        .verifyNoInteractionsOrFees();

    InteractionsAndResult noSelfInvocation =
        invoke(ADDRESSES.get(0), 42L, addAttestation(blob, 1, 3)).verifyNoInteractionsOrFees();
    Assertions.assertThat(noSelfInvocation.interactions()).isEmpty();
  }

  private byte[] addAttestation(byte[] data, int id, int signer) {
    KeyPair keyPair = new KeyPair(PRIVATE_KEYS.get(signer));
    Hash toSign =
        Hash.create(
            s -> {
              s.write(RealContractBinder.ATTESTATION_DOMAIN_SEPARATOR);
              CONTRACT_ONE.write(s);
              s.write(data);
            });
    Signature sign = keyPair.sign(toSign);
    return SafeDataOutputStream.serialize(
        rpc -> {
          rpc.writeByte(ADD_ATTESTATION_SIGNATURE);
          rpc.writeInt(id);
          sign.write(rpc);
        });
  }

  //// Test that user cannot invoke

  @Test
  void userInvokesSelfInvocations() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    List<Byte> selfInvocationIds =
        List.of(
            ON_COMPUTE_COMPLETE,
            ON_VARIABLES_OPENED,
            ON_ATTESTATION_COMPLETE,
            ON_VARIABLE_INPUTTED,
            ON_VARIABLE_REJECTED);
    for (byte b : selfInvocationIds) {
      byte[] interaction = SafeDataOutputStream.serialize(stream -> stream.writeByte(b));
      Assertions.assertThatThrownBy(() -> invoke(ACCOUNT_ONE, 5L, interaction))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage(
              String.format(
                  "Invocation can only be executed by the contract itself. Caller was %s, contract"
                      + " address is %s",
                  ACCOUNT_ONE, CONTRACT_ONE));
    }
  }

  @Test
  void userInvokesAddBatch() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    byte[] interaction = SafeDataOutputStream.serialize(stream -> stream.writeByte(ADD_BATCHES));
    Assertions.assertThatThrownBy(() -> invoke(ACCOUNT_ONE, 5L, interaction))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Only the preprocessing contract is allowed to add batches");
  }

  //// Staking fees tests

  @Test
  public void payingStakingFeesConsumeAllGasProvided() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long oldDeadline = currentState().getComputationDeadline();

    long blockProductionTime = oldDeadline - 1;
    InteractionsAndResult results =
        invokeExtendZkComputationDeadline(1, blockProductionTime, 50_000L);
    Assertions.assertThat(results.interactions()).isEmpty();
    Assertions.assertThat(results.callResult()).isNull();
    Assertions.assertThat(results.registeredZkFees()).isEqualTo(50_000L);
  }

  @Test
  public void deadlineIsNotExtendedIfLessThanMinimumRequiredIsSupplied() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long oldDeadline = currentState().getComputationDeadline();

    long blockProductionTime = oldDeadline - 1;
    Assertions.assertThatCode(
            () -> invokeExtendZkComputationDeadline(1, blockProductionTime, 28571))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Not possible to extend deadline with minimum required amount");
    Assertions.assertThat(currentState().getComputationDeadline()).isEqualTo(oldDeadline);
  }

  @Test
  public void canPayServiceFeesIfCurrentDeadlineIsSlightlyLessThan182DaysInTheFuture() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long deadline = currentState().getComputationDeadline();
    long blockProductionTime =
        Instant.ofEpochMilli(deadline).minus(182, ChronoUnit.DAYS).toEpochMilli() + 1;
    Assertions.assertThatCode(
            () -> invokeExtendZkComputationDeadline(1, blockProductionTime, 50_000L))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Not possible to extend deadline with minimum required amount");
  }

  @Test
  public void cannotPayServiceFeesIfCurrentDeadlineIs182DaysInTheFuture() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long deadline = currentState().getComputationDeadline();
    long blockProductionTime =
        Instant.ofEpochMilli(deadline).minus(182, ChronoUnit.DAYS).toEpochMilli();
    Assertions.assertThatCode(
            () -> invokeExtendZkComputationDeadline(1, blockProductionTime, 28572))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to pay staking fees - current deadline is more than 182 days in the future");
  }

  @Test
  public void cannotPayServiceFeesIfCurrentDeadlineIsMoreThan182DaysInTheFuture() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long deadline = currentState().getComputationDeadline();
    long blockProductionTime =
        Instant.ofEpochMilli(deadline).minus(182, ChronoUnit.DAYS).toEpochMilli();
    Assertions.assertThatCode(
            () -> invokeExtendZkComputationDeadline(1, blockProductionTime - 1, 28572))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to pay staking fees - current deadline is more than 182 days in the future");
  }

  @Test
  public void payingMinimumAmountOfGasExtendsDeadlineByMinimumAmount() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long oldDeadline = currentState().getComputationDeadline();

    long blockProductionTime = oldDeadline - 1;
    int gas = 28572; // 800_000 / 28
    long feesPaid =
        invokeExtendZkComputationDeadline(1, blockProductionTime, gas).registeredZkFees();

    long msPer28Days = Duration.ofDays(28).toMillis();
    long msPerGas = msPer28Days / DEFAULT_STAKING_FEE;
    long msToExtend = msPerGas * gas;

    Assertions.assertThat(currentState().getComputationDeadline())
        .isEqualTo(oldDeadline + msToExtend);
    Assertions.assertThat(feesPaid).isEqualTo(gas);
  }

  @Test
  public void payingForMaximumExtensionExtendByMaxAmount() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long oldDeadline = currentState().getComputationDeadline();

    long blockProductionTime = oldDeadline - Duration.ofDays(1).toMillis();
    long msPerGas = 3024;

    long msToPayFor = Duration.ofDays(181).toMillis(); // 182 - 1 days;
    long gas = 1 + (msToPayFor - 1) / msPerGas;

    long feesPaid =
        invokeExtendZkComputationDeadline(1, blockProductionTime, gas).registeredZkFees();

    long expectedDeadlineLimit =
        Instant.ofEpochMilli(blockProductionTime).plus(182, ChronoUnit.DAYS).toEpochMilli();
    long expectedDeadlineMin = expectedDeadlineLimit - msPerGas;
    Assertions.assertThat(currentState().getComputationDeadline())
        .isBetween(expectedDeadlineMin, expectedDeadlineLimit);
    Assertions.assertThat(feesPaid).isEqualTo(gas);
  }

  @Test
  public void payingTooMuchMayResultInAnOverflow() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long oldDeadline = currentState().getComputationDeadline();

    long blockProductionTime = oldDeadline - Duration.ofDays(1).toMillis();

    Assertions.assertThatCode(
            () -> invokeExtendZkComputationDeadline(1, blockProductionTime, Long.MAX_VALUE))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("long overflow");
  }

  @Test
  public void canPayStakingFeesIfCurrentTimeIsExactlyTheDeadline() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long oldDeadline = currentState().getComputationDeadline();
    long blockProductionTime = oldDeadline;
    long paidFees =
        invokeExtendZkComputationDeadline(1, blockProductionTime, 50_000L).registeredZkFees();

    Assertions.assertThat(paidFees).isEqualTo(50_000L);
  }

  @Test
  public void cannotPayStakingFeesIfDeadlineHasBeenReached() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");

    long oldDeadline = currentState().getComputationDeadline();
    long blockProductionTime = oldDeadline + 1;
    Assertions.assertThatCode(() -> invokeExtendZkComputationDeadline(1, blockProductionTime, 0))
        .isInstanceOf(IllegalStateException.class)
        .hasMessageContaining("Unable to pay staking fees")
        .hasMessageContaining("computation deadline has been reached");
  }

  @Test
  public void cannotPayStakingFeesIfCalculationStateIsDone() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-in-waiting-with-variable");

    emptyOpenInvocation(ACTION_5_CONTRACT_COMPLETE, ACCOUNT_ONE, 5L)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyZkGovernanceGasCosts();

    long oldDeadline = currentState().getComputationDeadline();
    long blockProductionTime = oldDeadline - 1;
    Assertions.assertThat(currentState().getCalculationStatus()).isEqualTo(CalculationStatus.DONE);
    Assertions.assertThatCode(() -> invokeExtendZkComputationDeadline(1, blockProductionTime, 0))
        .isInstanceOf(IllegalStateException.class)
        .hasMessageContaining("Unable to pay staking fees")
        .hasMessageContaining("calculation status in final state");
  }

  @Test
  public void cannotPayStakingFeesIfCalculationStateIsMaliciousBehaviour() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-computing-full-two-commits");

    Hash partialOpenings = Hash.create(stream -> stream.writeString("My openings"));
    sendCommit(1, partialOpenings).verifyNoInteractionsOrFees();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.CALCULATING);

    sendCommit(2, partialOpenings)
        .verifyInteract(NODE_REGISTRY_ADDRESS)
        .verifyNotifyCalculationDoneEvent(
            List.of(
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE,
                EnginesStatus.ZkNodeScoreStatus.FAILURE))
        .verifyZkGovernanceGasCosts();
    Assertions.assertThat(currentState().getCalculationStatus())
        .isEqualTo(CalculationStatus.MALICIOUS_BEHAVIOUR);

    long oldDeadline = currentState().getComputationDeadline();
    long blockProductionTime = oldDeadline - 1;
    Assertions.assertThatCode(() -> invokeExtendZkComputationDeadline(1, blockProductionTime, 0))
        .isInstanceOf(IllegalStateException.class)
        .hasMessageContaining("Unable to pay staking fees")
        .hasMessageContaining("calculation status in final state");
  }

  @Test
  public void onlyZkNodeRegistryCanCallExtendingZkDeadline() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-initial");
    TestBinderContext zkBinderContext =
        TestBinderContext.withGas(1, DEFAULT_BLOCK_PRODUCTION_TIME, ACCOUNT_ONE, 50_000_000L);
    Assertions.assertThatCode(
            () ->
                binder.invoke(
                    zkBinderContext,
                    currentState(),
                    SafeDataOutputStream.serialize(
                        stream -> {
                          stream.writeByte(EXTEND_ZK_COMPUTATION_DEADLINE);
                          // ms / gas (3024 ms/gas is default for staking fee of 800,000 gas)
                          stream.writeLong(3024L);
                          stream.writeLong(1L);
                          // Minimum extension amount
                          stream.writeLong(Duration.ofHours(24).toMillis());
                          // Extension limit
                          stream.writeLong(Duration.ofDays(182).toMillis());
                        })))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Only node registry allowed to pay staking fees - Expected caller address "
                + NODE_REGISTRY_ADDRESS.writeAsString()
                + ", but was "
                + ACCOUNT_ONE.writeAsString());
  }

  //// Test illegal events from the invoker

  /**
   * Throws error if the contract itself tries to call notify computation done v1 on the node
   * registry.
   */
  @Test
  void illegalNotifyDoneV1() {
    EventResult eventResult = EventResult.create();
    eventResult.addEventWithCostFromContract(NODE_REGISTRY_ADDRESS, new byte[] {0x05}, 5000L);
    this.binder = mockedBinderThatCreatesEvents(eventResult);
    readState("real-binder-initial");
    Assertions.assertThatThrownBy(() -> emptyOpenInvocation((byte) 1, ACCOUNT_ONE))
        .hasMessage("Contract cannot itself notify that it is done.");
  }

  /**
   * Throws error if the contract itself tries to call notify computation done v2 on the node
   * registry.
   */
  @Test
  void illegalNotifyDoneV2() {
    EventResult eventResult = EventResult.create();
    eventResult.addEventWithCostFromContract(NODE_REGISTRY_ADDRESS, new byte[] {0x12}, 5000L);
    this.binder = mockedBinderThatCreatesEvents(eventResult);
    readState("real-binder-initial");
    Assertions.assertThatThrownBy(() -> emptyOpenInvocation((byte) 1, ACCOUNT_ONE))
        .hasMessage("Contract cannot itself notify that it is done.");
  }

  /** Other calls to the node registry succeeds. */
  @Test
  void otherCallsToNodeRegistryAreFine() {
    EventResult eventResult = EventResult.create();
    eventResult.addEventWithCostFromContract(NODE_REGISTRY_ADDRESS, new byte[] {0x06}, 5000L);
    this.binder = mockedBinderThatCreatesEvents(eventResult);
    readState("real-binder-initial");
    emptyOpenInvocation((byte) 1, ACCOUNT_ONE).verifyInteract(NODE_REGISTRY_ADDRESS);
  }

  /** Pinging a contract succeeds. */
  @Test
  void pingContract() {
    EventResult eventResult = EventResult.create();
    eventResult.addEventWithCostFromContract(NODE_REGISTRY_ADDRESS, new byte[] {}, 5000L);
    this.binder = mockedBinderThatCreatesEvents(eventResult);
    readState("real-binder-initial");
    emptyOpenInvocation((byte) 1, ACCOUNT_ONE).verifyInteract(NODE_REGISTRY_ADDRESS);
  }

  //// Testing prefetching of preprocessed material

  /**
   * If created with number of preprocessed material to prefetch, the binder will request material
   * immediately after create.
   */
  @Test
  void willPrefetchMaterialsOnCreate() {
    setWasmContract(ZK_CONTRACT_FILE);
    int inputMaskElements = 1000; // One batch
    int triplesElements = 200000; // Two batches
    byte[] initRpc =
        SafeDataOutputStream.serialize(
            stream -> {
              for (int i = 0; i < 4; i++) {
                ADDRESSES.get(i).write(stream);
                PUBLIC_KEYS.get(i).write(stream);
                stream.writeString("https://zk.com");
              }
              PREPROCESS_ADDRESS.write(stream);
              NODE_REGISTRY_ADDRESS.write(stream);
              stream.writeLong(DEFAULT_ZK_COMPUTATION_DEADLINE);
              stream.writeLong(
                  DEFAULT_AVAILABLE_GAS
                      - 3); // Need a tiny bit of gas for running contract init code
              stream.writeInt(inputMaskElements);
              stream.writeInt(triplesElements);
              stream.writeBoolean(DEFAULT_OPTIMISTIC_MODE);
              stream.write(WasmRealContractInvoker.INIT_HEADER);
              stream.write(new byte[1]);
            });
    InteractionsAndResult binderInteractions = createWithInitRpc(initRpc);
    Assertions.assertThat(binderInteractions.interactions()).hasSize(2);
    binderInteractions.verifyRequestInputMaskBatches(1, 0).verifyRequestTripleBatches(2, 1);

    Assertions.assertThat(currentState().getInputMaskState().numElementsToPrefetch())
        .isEqualTo(inputMaskElements);
    Assertions.assertThat(currentState().getInputMaskState().numUsedElements()).isEqualTo(0);
    Assertions.assertThat(currentState().getInputMaskState().numRequestedBatches()).isEqualTo(1);

    Assertions.assertThat(currentState().getTriplesState().numElementsToPrefetch())
        .isEqualTo(triplesElements);
    Assertions.assertThat(currentState().getTriplesState().numUsedElements()).isEqualTo(0);
    Assertions.assertThat(currentState().getTriplesState().numRequestedBatches()).isEqualTo(2);

    sendInputMaskBatch(9, 1).verifyNoInteractionsOrFees();
    sendTripleBatch(10, 2).verifyNoInteractionsOrFees();
    sendTripleBatch(11, 3).verifyNoInteractionsOrFees();

    verifySerialization("real-binder-prefetched-preprocessed");
  }

  /**
   * If the binder has less available input masks than specified to prefetch, the binder will
   * request more input masks.
   */
  @Test
  void requestMorePrefetchedInputMasks() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-prefetched-preprocessed");
    addUnconfirmedOnChainInput(ACCOUNT_ONE, 16, 5L, new byte[] {}, createInput(16))
        .verifyRequestInputMaskBatches(1);
    Assertions.assertThat(currentState().getInputMaskState().numUsedElements()).isEqualTo(16);
    Assertions.assertThat(currentState().getInputMaskState().numRequestedBatches()).isEqualTo(2);
  }

  /**
   * If a computation is started such that the binder has less computation triples than specified to
   * prefetch, the binder will request more computation triples.
   */
  @Test
  void requestMorePrefetchedComputationTriples() {
    setWasmContract(ZK_CONTRACT_FILE, WasmExampleComputations.COMPUTATION_ADD_SUM);
    readState("real-binder-prefetched-preprocessed");
    // startCompute
    emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L)
        .verifyRequestTripleBatches(2);

    Assertions.assertThat(currentState().getComputationState().isWaitingForTriples()).isFalse();
    Assertions.assertThat(currentState().getTriplesState().numUsedElements()).isEqualTo(100520);
    Assertions.assertThat(currentState().getTriplesState().numRequestedBatches()).isEqualTo(4);
  }

  /**
   * A binder configured to start a computation as optimistic starts the computation optimistically.
   */
  @Test
  void startComputationInOptimisticMode() {
    setWasmContract(ZK_CONTRACT_FILE);
    readState("real-binder-with-two-triple");
    Assertions.assertThat(currentState().getDefaultOptimisticMode()).isTrue();

    // startCompute
    emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L)
        .verifyNoInteractions();

    Assertions.assertThat(currentState().getComputationState().isOptimistic()).isTrue();
    verifySerialization("real-binder-computing");
  }

  /**
   * A binder configured to start a computation as non-optimistic starts the computation
   * non-optimistically.
   */
  @Test
  void startComputationInNonOptimisticMode() {
    setWasmContract(ZK_CONTRACT_FILE);
    boolean defaultOptimisticMode = false;
    byte[] initRpc =
        SafeDataOutputStream.serialize(
            stream -> {
              for (int i = 0; i < 4; i++) {
                ADDRESSES.get(i).write(stream);
                PUBLIC_KEYS.get(i).write(stream);
                stream.writeString("https://zk.com");
              }
              PREPROCESS_ADDRESS.write(stream);
              NODE_REGISTRY_ADDRESS.write(stream);
              stream.writeLong(DEFAULT_ZK_COMPUTATION_DEADLINE);
              stream.writeLong(DEFAULT_STAKING_FEE);
              stream.writeInt(0);
              stream.writeInt(0);
              stream.writeBoolean(defaultOptimisticMode);
              stream.write(WasmRealContractInvoker.INIT_HEADER);
              stream.write(new byte[1]);
            });
    createWithInitRpc(initRpc).verifyNoInteractions().verifyZkFees(DEFAULT_STAKING_FEE);

    Assertions.assertThat(currentState().getDefaultOptimisticMode()).isFalse();

    // startCompute
    sendTripleBatch(3, 1).verifyNoInteractionsOrFees();
    emptyOpenInvocation(ACTION_2_START_COMPUTATION_ONE_INPUT, ACCOUNT_ONE, 5L)
        .verifyNoInteractions()
        .verifyZkFees(50_500L);
    ComputationStateImpl computationState = currentState().getComputationState();
    Assertions.assertThat(computationState.isOptimistic()).isFalse();
    verifySerialization("default-non-optimistic-computation");
  }

  //// Utility

  private WasmRealContractBinder mockedBinderThatCreatesEvents(EventResult eventResult) {
    WasmInvoker invoker = Mockito.mock(WasmInvoker.class);
    WasmRealContractInvoker.WasmContract contract =
        Mockito.mock(WasmRealContractInvoker.WasmContract.class);
    Mockito.when(
            invoker.executeWasm(
                Mockito.anyLong(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any()))
        .thenReturn(
            new WasmInvoker.WasmResult(
                new WasmState(new LargeByteArray(new byte[0])), eventResult, 5, List.of()));
    return new WasmRealContractBinder(new WasmRealContractInvoker(invoker, contract));
  }

  @CheckReturnValue
  private InteractionsAndResult sendUnableToCalculateInteraction(
      final BlockchainAddress addr, final long blockTime, long calculateForTime) {
    return invoke(
        "unableToCalculate",
        addr,
        blockTime,
        DEFAULT_BLOCK_PRODUCTION_TIME,
        createUnableToCalculateInteraction(calculateForTime));
  }

  @CheckReturnValue
  private static byte[] createUnableToCalculateInteraction(long calculateForTime) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(UNABLE_TO_CALCULATE);
          stream.writeLong(calculateForTime);
        });
  }

  @CheckReturnValue
  private InteractionsAndResult rejectInput(
      final BlockchainAddress addr, final long blockTime, final int inputId) {
    return invoke(
        "rejectInput",
        addr,
        blockTime,
        DEFAULT_BLOCK_PRODUCTION_TIME,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(REJECT_INPUT);
              stream.writeInt(inputId);
            }));
  }

  @CheckReturnValue
  private InteractionsAndResult stateInvocation(final long current, final long next) {
    return openInvocation(
        ACTION_13_UPDATE_STATE,
        ACCOUNT_ONE,
        stream -> {
          stream.writeLong(current);
          stream.writeLong(next);
        },
        10L);
  }

  @CheckReturnValue
  private InteractionsAndResult sendCommit(int index) {
    return sendCommit(index, defaultPartialOpens());
  }

  @CheckReturnValue
  private InteractionsAndResult sendCommit(int index, Hash partialOpenings) {
    return invoke(
        "sendCommit",
        ADDRESSES.get(index),
        15L,
        DEFAULT_BLOCK_PRODUCTION_TIME,
        createCommitResultInvocation(8L, partialOpenings));
  }

  @CheckReturnValue
  private InteractionsAndResult invoke(BlockchainAddress sender, long blockTime, byte[] input) {
    return invoke("invoke?", sender, blockTime, DEFAULT_BLOCK_PRODUCTION_TIME, input);
  }

  @CheckReturnValue
  private InteractionsAndResult emptyInvocation(
      BlockchainAddress sender, long blockTime, long blockProductionTime) {
    return invoke("invoke?", sender, blockTime, blockProductionTime, new byte[0]);
  }

  @CheckReturnValue
  private byte[] bitsToBytes(byte[] bits) {
    int byteLength = bits.length / 8;
    byte[] bytes = new byte[byteLength];
    for (int i = 0; i < byteLength; i++) {
      for (int j = 0; j < 8; j++) {
        bytes[i] |= (byte) (bits[i * 8 + j] << j);
      }
    }
    return bytes;
  }

  /**
   * Used for triggering a pending computation. Creates an empty open invocation.
   *
   * @param actionId the id of the action to call
   * @param blockTime the block time of the invocation
   * @return the eventgroup resulting from the invocation
   */
  @CheckReturnValue
  private InteractionsAndResult triggerComputation(byte actionId, long blockTime) {
    return emptyOpenInvocation(actionId, ACCOUNT_ONE, blockTime);
  }

  /** Shares the given value using fixed randomness such that it is reproducible. */
  private static List<byte[]> share(byte[] value) {
    int counter = 0; // serves as the PRG.
    BinaryExtensionFieldElementFactory factory = new BinaryExtensionFieldElementFactory();
    List<BinaryExtensionFieldElement> elements = factory.alphas();

    int size = value.length * factory.elementBitSize();
    List<byte[]> result = List.of(new byte[size], new byte[size], new byte[size], new byte[size]);
    for (int i = 0; i < value.length; i++) {
      byte b = value[i];
      for (int j = 0; j < factory.elementBitSize(); j++) {
        List<BinaryExtensionFieldElement> coefficients =
            List.of(
                BitHelper.isLongBitSet(b, j) ? factory.one() : factory.zero(),
                factory.createElement(counter++));
        Polynomial<BinaryExtensionFieldElement> polynomial =
            new Polynomial<>(coefficients, factory.zero());
        for (int k = 0; k < 4; k++) {
          int offset = i * factory.elementBitSize() + j;
          result.get(k)[offset] = polynomial.evaluate(elements.get(k)).serialize()[0];
        }
      }
    }
    return result;
  }

  // CPD-ON

  @Override
  protected Path pathForStateJson(final String name) {
    return pathForResource(getClass(), "ref/WasmRealContractBinderTest/%s.json".formatted(name));
  }
}
