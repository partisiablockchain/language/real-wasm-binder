package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitAppendable;
import com.partisiablockchain.language.zkcircuit.ZkCircuitImpl;
import com.partisiablockchain.language.zkcircuit.ZkCircuitValidator;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import com.partisiablockchain.serialization.LargeByteArray;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Testing {@link MetaCircuitComputation}. */
public final class MetaCircuitComputationTest {

  /**
   * Pi-Test coverage for instances where an ill-formed circuit is produced by the Meta-Circuit
   * lowering process.
   */
  @Test
  public void validatePitestCoverage() {
    Assertions.assertThatThrownBy(() -> MetaCircuitComputation.runCircuit(invalidZkCircuit(), null))
        .isInstanceOf(ZkCircuitValidator.ZkCircuitInvalid.class)
        .hasMessageContaining("Reference to unknown gate 123");
  }

  private static ZkCircuit invalidZkCircuit() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // Sub-gate does not exist!
    final ZkAddress root =
        circuit.add(
            ZkType.SBI32,
            new ZkOperation.Unary(ZkOperation.UnaryOp.BITWISE_NOT, new ZkAddress(123)));
    circuit.setRoots(List.of(root));
    return circuit;
  }

  @Test
  public void publicInputValuesFromCoverage() {
    final FixedList<LargeByteArray> inputs =
        FixedList.create(
            List.of(
                new LargeByteArray(new byte[] {0x55, 0x44, 0x33, 0x22}),
                new LargeByteArray(new byte[] {0x77, 0x44, 0x33, 0x11})));

    Assertions.assertThat(MetaCircuitComputation.publicInputValuesFrom(inputs))
        .containsExactly(BigInteger.valueOf(0x22334455), BigInteger.valueOf(0x11334477));
  }

  @Test
  public void bigPublicInputValuesFromCoverage() {
    final FixedList<LargeByteArray> inputs =
        FixedList.create(
            List.of(
                new LargeByteArray(
                    new byte[] {(byte) 0x99, (byte) 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22}),
                new LargeByteArray(new byte[] {0x77, 0x44, 0x33, 0x11})));

    Assertions.assertThat(MetaCircuitComputation.publicInputValuesFrom(inputs))
        .containsExactly(new BigInteger("2233445566778899", 16), new BigInteger("11334477", 16));
  }
}
