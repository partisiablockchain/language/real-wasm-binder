package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasminvoker.SemVer;
import com.partisiablockchain.zk.real.binder.ZkStateMutable;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Testing {@link WasmRealContractInvoker}. */
public final class WasmRealContractInvokerTest {

  private static ZkStateMutable mockPreviousState() {
    return Mockito.mock(ZkStateMutable.class);
  }

  @Test
  public void unknownActionId() {
    final ZkStateMutable previousState = mockPreviousState();
    final byte[] bytes = new byte[] {0, 0, 0, 1, 99};
    Assertions.assertThatThrownBy(
            () -> WasmRealContractInvoker.applyZkStateUpdatesSection(previousState, bytes))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Unknown action with id: 99");
  }

  @Test
  public void outputCompleteNotSupported() {
    final ZkStateMutable previousState = mockPreviousState();
    final byte[] bytes = new byte[] {0, 0, 0, 1, 5};
    Assertions.assertThatThrownBy(
            () -> WasmRealContractInvoker.applyZkStateUpdatesSection(previousState, bytes))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining("OUTPUT_COMPLETE is no longer supported");
  }

  @Test
  public void startComputationWithPublicInputs() {
    // Setup
    final byte[] bytes =
        Hex.decode(
            ""
                + "00000001" // Num actions
                + "09" // Event: START_COMPUTATION_3
                + "00000000" // Initial function
                + "00000000" // Num output variables
                + "00000002" // Num input variables
                + "0000000401020304" // Input variable 1
                + "0000000145" // Input variable 2
                + "00" // No shortname
            );
    final ZkStateMutable previousState = mockPreviousState();

    // Test
    final WasmRealContractState.ComputationInput computationInput =
        WasmRealContractInvoker.applyZkStateUpdatesSection(previousState, bytes);

    // Assert
    Mockito.verify(previousState, Mockito.times(1)).startComputation(List.of(), null);
    Assertions.assertThat(computationInput.calledFunctionShortname()).isEqualTo(0);
    Assertions.assertThat(computationInput.calledFunctionArguments().get(0).getData())
        .containsExactly(1, 2, 3, 4);
    Assertions.assertThat(computationInput.calledFunctionArguments().get(1).getData())
        .containsExactly(0x45);
  }

  @Test
  public void isBinderSupported() {
    Assertions.assertThat(WasmRealContractInvoker.isSupported(new SemVer(11, 5, 0))).isTrue();
    Assertions.assertThat(WasmRealContractInvoker.isSupported(new SemVer(11, 5, 3))).isTrue();
    Assertions.assertThat(WasmRealContractInvoker.isSupported(new SemVer(9, 0, 0))).isFalse();
    Assertions.assertThat(WasmRealContractInvoker.isSupported(new SemVer(11, 6, 0))).isFalse();
  }
}
