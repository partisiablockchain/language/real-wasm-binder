package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.oracle.CreateEvmEventSubscription;
import com.partisiablockchain.oracle.EventMetadata;
import com.partisiablockchain.oracle.EvmAddress;
import com.partisiablockchain.oracle.EvmEventLog;
import com.partisiablockchain.oracle.Topic;
import com.partisiablockchain.oracle.TopicList;
import com.partisiablockchain.serialization.LargeByteArray;
import com.secata.stream.SafeDataOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.bouncycastle.util.encoders.Hex;

/** Test eventLog. */
public final class EvmEventsTestData {
  private static final long SEED = 7032307166244393944L;
  private static final Random RNG = new Random(SEED);

  private static final List<String> CHAIN_IDS = List.of("ETHEREUM", "POLYGON", "BNB_SMART_CHAIN");
  private final List<TestData> testData;

  private EvmEventsTestData(List<TestData> testData) {
    this.testData = testData;
  }

  public List<TestData> getTestData() {
    return testData;
  }

  /**
   * Deterministically generate test eventLog to be used in EVM events testing.
   *
   * @return generated test eventLog
   */
  public static EvmEventsTestData generate() {
    List<EvmAddress> testAddresses = generateTestAddresses();

    List<EventDef> testEventDefs =
        List.of(
            new EventDef(
                "Transfer",
                new EventArgDef("from", true, false, EvmEventsTestData::evmAddress),
                new EventArgDef("to", true, false, EvmEventsTestData::evmAddress),
                new EventArgDef("amount", false, false, () -> randomBytes(8)),
                new EventArgDef("dataTag", false, true, EvmEventsTestData::unused)),
            new EventDef(
                "Mint",
                new EventArgDef("amount", false, false, () -> randomBytes(8)),
                new EventArgDef("symbol", true, false, () -> randomBytes(1)),
                new EventArgDef("owner", true, false, EvmEventsTestData::evmAddress),
                new EventArgDef("dataTag", false, true, EvmEventsTestData::unused)),
            new EventDef(
                "Register",
                new EventArgDef("pbcAccount", false, false, () -> randomBytes(21)),
                new EventArgDef("ethAccount", false, false, EvmEventsTestData::evmAddress),
                new EventArgDef("dataTag", false, true, EvmEventsTestData::unused)),
            new EventDef(
                "BidReceived",
                new EventArgDef("bidderId", true, false, () -> randomBytes(4)),
                new EventArgDef("dataTag", false, true, EvmEventsTestData::unused)),
            new EventDef(
                "Vote",
                new EventArgDef("voterId", true, false, () -> randomBytes(21)),
                new EventArgDef("choice", false, false, () -> randomBytes(2)),
                new EventArgDef("dataTag", false, true, EvmEventsTestData::unused)));

    int numberOfSubscriptions = 20;
    List<SubscriptionAndEventDef> testSubscriptions = new ArrayList<>();
    for (int i = 0; i < numberOfSubscriptions; i++) {
      EventDef eventDef = testEventDefs.get(RNG.nextInt(testEventDefs.size()));
      CreateEvmEventSubscription subscription = generateSubscription(eventDef, testAddresses);
      testSubscriptions.add(new SubscriptionAndEventDef(subscription, eventDef));
    }

    List<TestData> data = new ArrayList<>();
    for (SubscriptionAndEventDef subscription : testSubscriptions) {
      List<EvmEventLog> testEvents = createEvents(subscription);
      data.add(new TestData(subscription.subscription, testEvents));
    }

    return new EvmEventsTestData(data);
  }

  private static List<EvmEventLog> createEvents(SubscriptionAndEventDef subAndEventDef) {
    // Generate events for the subscription
    // Generate for x amount of blocks, each block having events in y transactions, and each
    // transaction holding z events.
    int numberOfBlocks = 25;
    int maxTransactionsPerBlock = 25;
    int maxEventsPerTransaction = 5;

    List<EvmEventLog> testEvents = new ArrayList<>();
    long nextBlock = subAndEventDef.subscription().fromBlock().longValueExact();
    for (int i = 0; i < numberOfBlocks; i++) {
      Unsigned256 blockNumber = Unsigned256.create(nextBlock);
      int j = 0;
      while (j < maxTransactionsPerBlock) {
        Unsigned256 transactionIndex = Unsigned256.create(j);
        int k = 0;
        while (k < maxEventsPerTransaction) {
          Unsigned256 logIndex = Unsigned256.create(k);
          EvmEventLog testEvent =
              createEvent(subAndEventDef, blockNumber, transactionIndex, logIndex);
          testEvents.add(testEvent);
          k = RNG.nextInt(k, maxEventsPerTransaction) + 1; // Add one to ensure increase
        }
        j = RNG.nextInt(j, maxTransactionsPerBlock) + 1; // Add one to ensure increase
      }

      nextBlock = RNG.nextLong(nextBlock + 1, nextBlock + 100);
    }

    return testEvents;
  }

  private static EvmEventLog createEvent(
      SubscriptionAndEventDef subAndEventDef,
      Unsigned256 blockNumber,
      Unsigned256 transactionIndex,
      Unsigned256 logIndex) {

    byte[] data =
        SafeDataOutputStream.serialize(generateEventData(subAndEventDef.eventDef().args()));
    List<Topic> topics = new ArrayList<>();
    for (TopicList topicList : subAndEventDef.subscription().topics()) {
      List<Topic> choices = topicList.topics();
      if (!choices.isEmpty()) {
        Topic topic = choices.get(RNG.nextInt(choices.size()));
        topics.add(topic);
      } else {
        byte[] randomTopic = new byte[32];
        RNG.nextBytes(randomTopic);
        topics.add(Topic.fromBytes(randomTopic));
      }
    }

    EventMetadata metadata = new EventMetadata(logIndex, transactionIndex, blockNumber);
    return new EvmEventLog(metadata, new LargeByteArray(data), TopicList.fromTopics(topics));
  }

  private static Consumer<SafeDataOutputStream> generateEventData(
      EvmEventsTestData.EventArgDef... args) {
    return dataStream -> {
      for (EventArgDef arg : args) {
        if (!arg.indexed) {
          if (arg.isDynamic) {
            // NOTE: this is really not how dynamic eventLog is encoded in the eventLog field, but
            // this is
            // added to test events that may not necessarily all have the same eventLog size.
            int dataFrames = RNG.nextInt(1, 13);
            int dataFrameSize = 32;
            byte[] dynamicBytes = new byte[dataFrames * dataFrameSize];
            RNG.nextBytes(dynamicBytes);
            dataStream.write(dynamicBytes);
          } else {
            byte[] bytes = arg.value.get();
            byte[] paddedBytes = leftPad(bytes);
            dataStream.write(paddedBytes);
          }
        }
      }
    };
  }

  private static CreateEvmEventSubscription generateSubscription(
      EventDef eventDef, List<EvmAddress> addresses) {
    String chainId = CHAIN_IDS.get(RNG.nextInt(CHAIN_IDS.size()));
    EvmAddress contractAddress = addresses.get(RNG.nextInt(addresses.size()));
    Unsigned256 fromBlock = Unsigned256.create(RNG.nextLong(500_000L, 5_000_000L));

    List<TopicList> filters = new ArrayList<>();
    String eventIdentifier =
        eventDef.name
            + Arrays.stream(eventDef.args())
                .map(def -> def.name)
                .collect(Collectors.joining(",", "(", ")"));
    byte[] eventSignature = keccak256(eventIdentifier);
    Topic eventSignatureTopic = Topic.fromBytes(eventSignature);
    filters.add(TopicList.fromTopics(List.of(eventSignatureTopic)));

    for (EventArgDef arg : eventDef.args()) {
      if (arg.indexed()) {
        List<Topic> topics = new ArrayList<>();
        int numberOfTopics = RNG.nextInt(4);
        for (int i = 0; i < numberOfTopics; i++) {
          byte[] topicValue = arg.value().get();
          topics.add(Topic.fromBytes(leftPad(topicValue)));
        }
        filters.add(TopicList.fromTopics(topics));
      }
    }

    return new CreateEvmEventSubscription(chainId, contractAddress, fromBlock, filters);
  }

  private static byte[] evmAddress() {
    byte[] buffer = new byte[20];
    RNG.nextBytes(buffer);
    return buffer;
  }

  private static byte[] randomBytes(int amount) {
    byte[] buffer = new byte[amount];
    RNG.nextBytes(buffer);
    return buffer;
  }

  private static byte[] unused() {
    throw new RuntimeException("This supplier should not be used!");
  }

  private static List<EvmAddress> generateTestAddresses() {
    int testAddressesAmount = 20;
    List<EvmAddress> testAddresses = new ArrayList<>();
    for (int i = 0; i < testAddressesAmount; i++) {
      byte[] buffer = new byte[20];
      RNG.nextBytes(buffer);
      EvmAddress address = EvmAddress.fromBytes(buffer);
      testAddresses.add(address);
    }
    return testAddresses;
  }

  private static byte[] keccak256(String input) {
    Keccak.Digest256 keccak = new Keccak.Digest256();
    keccak.update(input.getBytes(StandardCharsets.UTF_8));
    return keccak.digest();
  }

  private static String leftPad(String input) {
    return String.format("%64s", input).replace(" ", "0");
  }

  private static byte[] leftPad(byte[] input) {
    String hex = Hex.toHexString(input);
    String padded = leftPad(hex);
    return Hex.decode(padded);
  }

  @SuppressWarnings("ArrayRecordComponent")
  record EventDef(String name, EventArgDef... args) {}

  record EventArgDef(String name, boolean indexed, boolean isDynamic, Supplier<byte[]> value) {}

  record SubscriptionAndEventDef(CreateEvmEventSubscription subscription, EventDef eventDef) {}

  /** Record for test eventLog to be used when testing EVM events. */
  public record TestData(CreateEvmEventSubscription subscription, List<EvmEventLog> events) {}
}
