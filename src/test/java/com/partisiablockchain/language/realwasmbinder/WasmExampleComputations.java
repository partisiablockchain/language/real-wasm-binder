package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitNormalizer;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitPrettyParser;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitSerializer;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.WithResource;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/** Example metacircuits for Zk contract computations. */
public final class WasmExampleComputations {

  private WasmExampleComputations() {}

  /** Hand-made computation that counts boolean values together. */
  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_COUNT_VOTES =
      loadZkContractComputation("contracts/example_count_votes.mc");

  /** Hand-made computation that uselessly `and`s a lot of bits together. */
  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_AND_BITS =
      loadZkContractComputation("contracts/binop_and_bit.mc");

  /** Hand-made computation that adds a lot of secret-shared constants together. */
  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_ADD_SUM =
      loadZkContractComputation("contracts/binop_add_sum.mc");

  /** Hand-made comptuation that saves a constant to output. */
  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_CONSTANT_TRUE =
      loadZkContractComputation("contracts/return_true.mc");

  /** Computation that loads and saves all inputs given. */
  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_IDENTITY_ALL =
      loadZkContractComputation("contracts/function_load_save_all.rs.mc");

  /** Saves variable 1 to output. */
  static final RealComputationEnv<WasmRealContractState, LargeByteArray> COMPUTATION_VARIABLE_ONE =
      loadZkContractComputation("contracts/load_variable_one.mc");

  static RealComputationEnv<WasmRealContractState, LargeByteArray> loadZkContractComputation(
      final String path) {
    return WasmRealContractInvokerFactory.computationFromBytes(loadMetaCircuitAsBytes(path));
  }

  /**
   * Loads a pretty-formatted {@link MetaCircuit} and converts it to its bytecode form.
   *
   * @param path Path to the meta-circuit file.
   * @return Meta-circuit bytecode
   */
  public static byte[] loadMetaCircuitAsBytes(final String path) {
    final String metaCircuitPretty =
        new String(readResourceBytes(WasmExampleComputations.class, path), StandardCharsets.UTF_8);

    final MetaCircuit metaCircuit = MetaCircuitPrettyParser.parse(metaCircuitPretty);

    final MetaCircuit.Normalized metaCircuitNormalized =
        MetaCircuitNormalizer.normalizeMetaCircuit(metaCircuit);

    return SafeDataOutputStream.serialize(
        stream ->
            MetaCircuitSerializer.serializeMetaCircuitToStream(stream, metaCircuitNormalized));
  }

  static byte[] loadZkContractWasm(final String path) {
    return loadBytes(path);
  }

  static byte[] loadBytes(final String path) {
    return readResourceBytes(WasmExampleComputations.class, path);
  }

  static byte[] readResourceBytes(Class<?> context, String file) {
    return WithResource.apply(
        () -> context.getResourceAsStream(file),
        InputStream::readAllBytes,
        "Unable to read resource " + file + " in context " + context);
  }
}
