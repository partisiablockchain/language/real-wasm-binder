package com.partisiablockchain.zk.real.contract.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.binder.RealContractInvoker;
import com.partisiablockchain.zk.real.contract.AbstractRealContractTest;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.contract.RealContractTest;
import com.partisiablockchain.zk.real.contract.StateHandler;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.TestRealBinary;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.OrderMonitorMultiplier;
import com.partisiablockchain.zk.real.protocol2.PlainTextMultiplier;
import com.secata.util.ShareConversion;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Use this contract if you wish to execute the contract with REAL binary.
 *
 * <p>{@link AbstractRealContractTest} and {@link RealContractTest}.
 */
public abstract class BinaryRealContractTest extends RealContractTest {

  private static final Logger logger = LoggerFactory.getLogger(BinaryRealContractTest.class);

  /** Monitoring multiplier, used for computation ordering reference tests. */
  private final OrderMonitorMultiplier<BinaryExtensionFieldElement> multiplier =
      new OrderMonitorMultiplier<>(new PlainTextMultiplier<BinaryExtensionFieldElement>());

  /**
   * Initializes the test framework.
   *
   * @param contractInvoker the contract to test
   */
  protected BinaryRealContractTest(final RealContractInvoker contractInvoker) {
    super(
        contractInvoker,
        RealContractTest::nextToTestSources,
        BinaryZkStateController::create,
        (computation, stateFactory) ->
            new StateHandler(
                BinaryZkStateController.class, computation, BinaryZkStateController::new));
  }

  @Override
  protected void validateProvidedInput(List<byte[]> variableValue, List<Integer> bitLengths) {
    int totalExpectedBytes = bitLengths.stream().reduce(0, Integer::sum);
    int totalBytes = variableValue.stream().map(v -> v.length).reduce(0, Integer::sum);
    if (totalBytes != totalExpectedBytes) {
      throw new IllegalArgumentException(
          "Length of variableValue does not match bit length of variable. "
              + "Expected="
              + totalExpectedBytes
              + ", received="
              + totalBytes);
    }
  }

  /**
   * Executes the Zk computation - using a test channel working on clear text data. In the test this
   * needs to be done explicitly for flexibility.
   *
   * @param random the randomness used for selecting random bits in the channel
   */
  @Override
  protected void executeComputation(Random random) {
    final LargeByteArray onComputeCompleteShortname =
        prepareState(
            zkStateController ->
                zkStateController.zkState().getComputationState().getOnComputeCompleteShortname());

    List<Integer> variableIds =
        prepareState(
            zkStateController -> {
              TestRealBinary testChannel =
                  new TestRealBinary(
                      multiplier,
                      varId -> dataToShares(zkStateController.getDataForVariable(varId)));
              final int previousRounds = multiplier.numComputations();
              final int previousMults = multiplier.numMultiplications();

              Assertions.assertThat(getCalculationStatus())
                  .describedAs(
                      "The smart contract was not computing, "
                          + "did you remember to initiate compute through the smart contract?")
                  .isEqualTo(CalculationStatus.CALCULATING);

              zkStateController.executeComputation(
                  (RealComputationEnv<WasmRealContractState, LargeByteArray>)
                      contractInvoker.getContract().getComputation(),
                  testChannel);

              List<SecretSharedNumberAsBits> resultVariables = testChannel.getResults();
              Assertions.assertThat(resultVariables)
                  .describedAs(
                      "Size of result variables should be identical to the "
                          + "number of open variable values  the computation was initiated with")
                  .hasSize(zkStateController.zkState().getComputationState().getOutputIds().size());

              logger.info(
                  "Executed computation with {} rounds and {} total AND gates",
                  multiplier.numComputations() - previousRounds,
                  multiplier.numMultiplications() - previousMults);

              return zkStateController.updateWithResult(
                  resultVariables, result -> new LargeByteArray(toBytes(result)));
            });

    getStateLoader()
        .stateController()
        .onComputeComplete(contractInvoker, onComputeCompleteShortname.getData(), variableIds);
  }

  /**
   * Helper function that creates the share version of an integer.
   *
   * @param values the values to secret share
   * @return the byte array.
   */
  @Override
  protected List<byte[]> asShares(List<Integer> values) {
    List<byte[]> shares = new ArrayList<>();
    for (Integer value : values) {
      shares.add(ShareConversion.intToBytes(value));
    }
    return shares;
  }

  private List<List<Boolean>> dataToShares(List<byte[]> data) {
    List<List<Boolean>> shares = new ArrayList<>();
    for (byte[] bytes : data) {
      List<Boolean> bits = new ArrayList<>();
      for (byte b : bytes) {
        bits.add(b == 1);
      }
      shares.add(bits);
    }
    return shares;
  }

  private static byte[] toBytes(SecretSharedNumberAsBits variable) {
    int byteLength = variable.size();
    byte[] bytes = new byte[byteLength];
    int bitIndex = 0;
    for (BinaryExtensionFieldElement currentBitValue : variable) {
      bytes[bitIndex] = currentBitValue.serialize()[0];
      bitIndex++;
    }
    return bytes;
  }

  /**
   * Monitoring multiplier, used for computation ordering reference tests.
   *
   * @return Multiplier
   */
  public OrderMonitorMultiplier<BinaryExtensionFieldElement> multiplier() {
    return multiplier;
  }
}
