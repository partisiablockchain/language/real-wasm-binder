package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.partisiablockchain.contract.MemoryStateStorage;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.zk.real.binder.ZkStateImmutable;
import com.secata.tools.coverage.ExceptionConverter;
import org.assertj.core.api.Assertions;

/** Reads/writes a state object {@link ZkStateController} object to/from the disk. */
public final class StateHandler {

  /** JSON object mapper with indentation. */
  public static final ObjectMapper JSON_MAPPER =
      StateObjectMapper.createObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);

  private final StateSerializer serializer = new StateSerializer(new MemoryStateStorage(), true);
  private final RealComputationEnv<WasmRealContractState, LargeByteArray> computation;
  private final Class<? extends ZkStateController> jsonType;
  private final ZkStateController.ControllerFactory controllerSupplier;

  /**
   * Construct a new {@link StateHandler}.
   *
   * @param jsonType describes the type of {@link ZkController} object we wish to read/write.
   * @param computation the computation.
   * @param controllerSupplier a factory object which can supply {@link ZkController} object.
   */
  public StateHandler(
      Class<? extends ZkStateController> jsonType,
      RealComputationEnv<WasmRealContractState, LargeByteArray> computation,
      ZkStateController.ControllerFactory controllerSupplier) {
    this.computation = computation;
    this.jsonType = jsonType;
    this.controllerSupplier = controllerSupplier;
  }

  ZkStateController read(String rawState) {
    ZkStateController state =
        ExceptionConverter.call(
            () -> JSON_MAPPER.readValue(rawState, jsonType), "Unable to read json state");
    return serializeState(state);
  }

  String write(ZkStateController state) {
    String beforeSerialize = writeToJson(state);
    ZkStateController read = serializeState(state);
    String afterSerialize = writeToJson(read);

    Assertions.assertThat(afterSerialize)
        .describedAs("Before and after serialization to bytes should give same json")
        .isEqualTo(beforeSerialize);

    return afterSerialize;
  }

  private ZkStateController serializeState(ZkStateController state) {
    SerializationResult firstWrite =
        serializer.write(state.zkState(), WasmRealContractState.class, LargeByteArray.class);
    ZkStateImmutable read =
        serializer.read(
            firstWrite.hash(),
            ZkStateImmutable.class,
            WasmRealContractState.class,
            LargeByteArray.class);
    // Write again to ensure all hashes in trees are cached
    SerializationResult secondWrite =
        serializer.write(read, WasmRealContractState.class, LargeByteArray.class);
    Assertions.assertThat(firstWrite.hash()).isEqualTo(secondWrite.hash());
    return controllerSupplier.create(read, state, computation);
  }

  private String writeToJson(ZkStateController state) {
    return ExceptionConverter.call(
        () -> JSON_MAPPER.writeValueAsString(state), "Unable to write value to json");
  }
}
