package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.zk.real.binder.EngineState;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import org.assertj.core.api.Assertions;

/** Singleton with a collection of information about 4 computation engines. */
public final class ComputationEngines {

  /** The engines. */
  public static final EngineState ENGINES = initEngines();

  /**
   * Initialize the default engines.
   *
   * @return default engines.
   */
  public static EngineState initEngines() {
    return EngineState.read(
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                s -> {
                  writeEngine(s, 0);
                  writeEngine(s, 1);
                  writeEngine(s, 2);
                  writeEngine(s, 3);
                })));
  }

  /**
   * Get the PBC address of a particular computation engines.
   *
   * @param engineIndex the engine's index.
   * @return the address of this engine. A string of the form 0011223300 ... 00engineIndex.
   */
  public static BlockchainAddress addressOf(int engineIndex) {
    check(engineIndex);
    return BlockchainAddress.fromString(String.format("00112233%034d", engineIndex));
  }

  /**
   * Get the private signing key of a computation node. This is the signing key used to sign
   * attestations.
   *
   * @param engineIndex the index of the node
   * @return the signing key of the node
   */
  public static KeyPair signingKeyOf(int engineIndex) {
    check(engineIndex);
    return new KeyPair(BigInteger.valueOf(engineIndex + 1));
  }

  /**
   * Get the rest endpoint of a computation node.
   *
   * @param engineIndex the index of the engine
   * @return the engine's rest endpoint.
   */
  public static String restEndpointOf(int engineIndex) {
    check(engineIndex);
    return "https://computation_node_" + engineIndex;
  }

  private static void writeEngine(SafeDataOutputStream stream, int engineIndex) {
    addressOf(engineIndex).write(stream);
    signingKeyOf(engineIndex).getPublic().write(stream);
    stream.writeString(restEndpointOf(engineIndex));
  }

  private static void check(int engineIndex) {
    Assertions.assertThat(engineIndex).isGreaterThanOrEqualTo(0);
    Assertions.assertThat(engineIndex).isLessThan(4);
  }

  private ComputationEngines() {}
}
