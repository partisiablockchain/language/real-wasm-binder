package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zk.real.binder.ComputationDoneNotifier;
import com.partisiablockchain.zk.real.binder.ComputationStateImpl;
import com.partisiablockchain.zk.real.binder.PendingInputImpl;
import com.partisiablockchain.zk.real.binder.PreprocessedMaterialRequester;
import com.partisiablockchain.zk.real.binder.RealContractInvoker;
import com.partisiablockchain.zk.real.binder.ZkBinderContextWithGas;
import com.partisiablockchain.zk.real.binder.ZkClosedImpl;
import com.partisiablockchain.zk.real.binder.ZkStateImmutable;
import com.partisiablockchain.zk.real.binder.ZkStateImmutableBuilder;
import com.partisiablockchain.zk.real.binder.ZkStateMutable;
import com.partisiablockchain.zk.real.protocol.RealEnvironment;
import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElementFactory;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import org.assertj.core.api.Assertions;

/** Controller that wraps {@link ZkStateImmutable} from {@link RealContractBinder}. */
public abstract class ZkStateController {

  /** Creates finite field elements. */
  protected transient FiniteFieldElementFactory<BinaryExtensionFieldElement> factory;

  private transient RealComputationEnv<WasmRealContractState, LargeByteArray> computation;
  private transient long blockTime;
  private transient long blockProductionTime;
  private transient List<BinderInteraction> invocations;
  private transient CallResult callResult;
  private ZkStateImmutable zkState;

  /** AVL tree from variable id to variable data. */
  protected AvlTree<Integer, LargeByteArray> actualValues;

  private static final BlockchainAddress RANDOM_COMPUTATION_ENGINE_ADDR =
      BlockchainAddress.fromString("0000000000000000000000000000000000000FFFFF");
  private static final long AVAILABLE_GAS = 10_000_000L;

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  public ZkStateController() {}

  /**
   * Create state controller with computation.
   *
   * @param zkState immutable state
   * @param original inner state controller
   * @param computation actual REAL computation
   * @param factory creates finite field elements
   */
  protected ZkStateController(
      ZkStateImmutable zkState,
      ZkStateController original,
      RealComputationEnv<WasmRealContractState, LargeByteArray> computation,
      FiniteFieldElementFactory<BinaryExtensionFieldElement> factory) {
    requireNonNull(computation);
    requireNonNull(zkState);
    requireNonNull(original);
    requireNonNull(factory);
    this.computation = computation;
    this.zkState = zkState;
    this.actualValues = original.actualValues;
    this.invocations = new ArrayList<>();
    this.factory = factory;
  }

  /**
   * Create state controller without computation.
   *
   * @param zkState immutable state
   * @param actualValues variable data
   * @param factory creates finite field elements
   */
  protected ZkStateController(
      ZkStateImmutable zkState,
      AvlTree<Integer, LargeByteArray> actualValues,
      FiniteFieldElementFactory<BinaryExtensionFieldElement> factory) {
    this.zkState = zkState;
    this.actualValues = actualValues;
    this.invocations = new ArrayList<>();
    this.factory = factory;
  }

  record ConfirmedSecretVariableInfo(ZkClosedImpl var, LargeByteArray onInputtedShortname) {}

  ConfirmedSecretVariableInfo confirmSecretInput(Hash transaction, BlockchainAddress owner) {
    LargeByteArray onInputtedShortname = getPendingInputtedShortname(owner, transaction);
    ZkClosedImpl pending = getPendingInput(owner, transaction);
    Assertions.assertThat(pending)
        .overridingErrorMessage(
            "Owner does not have a previously inputted variable with client id=%s", transaction)
        .isNotNull();
    updateState(
        wrapBuilder(
            builder -> {
              builder.removePendingInput(pending.getId());
              builder.setVariable(pending);
            }));
    return new ConfirmedSecretVariableInfo(pending, onInputtedShortname);
  }

  /**
   * Gets the result of the execution.
   *
   * @return the result
   */
  public CallReturnValue getResult() {
    return callResult instanceof CallReturnValue ? (CallReturnValue) callResult : null;
  }

  /**
   * Gets the event group from the call back structure.
   *
   * @return the event group
   */
  @SuppressWarnings("unchecked")
  public BinderEventGroup<BinderInteraction> getRemoteCalls() {
    return callResult instanceof BinderEventGroup<?>
        ? (BinderEventGroup<BinderInteraction>) callResult
        : null;
  }

  /**
   * Gets the list of events.
   *
   * @return the events
   */
  public List<BinderInteraction> getInteractions() {
    return List.copyOf(invocations);
  }

  void addPendingVariable(
      TestZkBinderContext ctx,
      Hash transaction,
      byte[] variableValue,
      List<Integer> bitLengths,
      BlockchainAddress sender,
      boolean sealed,
      LargeByteArray information,
      LargeByteArray onInputtedShortname) {
    FixedList<LargeByteArray> shares = FixedList.create(List.of(new LargeByteArray(variableValue)));
    PreprocessedMaterialRequester inputMaskBatchRequester = (t, i) -> {};

    actualValues = actualValues.set(zkState.getNextVariableId(), new LargeByteArray(variableValue));
    updateState(
        ctx,
        wrapMutable(
            zkState ->
                zkState.createPendingInput(
                    transaction,
                    sender,
                    sealed,
                    information,
                    bitLengths,
                    null,
                    shares,
                    null,
                    inputMaskBatchRequester,
                    onInputtedShortname)));
  }

  TestZkBinderContext binderContext(final BlockchainAddress from) {
    requireNonNull(from);
    return new TestZkBinderContext(from);
  }

  private WasmRealContractState getOpenState() {
    return zkState.getOpenState();
  }

  private Function<ZkStateMutable, Void> wrapMutable(
      Consumer<ZkStateMutable> zkStateMutableConsumer) {
    return zkStateMutable -> {
      zkStateMutableConsumer.accept(zkStateMutable);
      return null;
    };
  }

  private Function<ZkStateMutable, Void> wrapBuilder(
      Consumer<ZkStateImmutableBuilder> builderConsumer) {
    return wrapMutable(zkStateMutable -> zkStateMutable.mutate(builderConsumer));
  }

  /**
   * Update state and retain result.
   *
   * @param update function applying update to state
   * @param <T> type of result from updating state
   * @return result from updating state
   */
  private <T> T updateState(Function<ZkStateMutable, T> update) {
    return updateState(binderContext(RANDOM_COMPUTATION_ENGINE_ADDR), update);
  }

  private <T> T updateState(
      final TestZkBinderContext binderContext, Function<ZkStateMutable, T> update) {
    UpdateStateRetainResult<T> updateRetainingResult = new UpdateStateRetainResult<>(update);
    updateState(binderContext, updateRetainingResult);
    return updateRetainingResult.result;
  }

  private void updateState(
      final TestZkBinderContext binderContext,
      final BiFunction<TestZkBinderContext, ZkStateMutable, WasmRealContractState> update) {

    ZkStateMutable stateMutable = makeMutable(zkState, binderContext, factory);

    WasmRealContractState open = update.apply(binderContext, stateMutable);
    stateMutable.setOpen(open);
    PreprocessedMaterialRequester nullRequester = (t, i) -> {};
    ComputationDoneNotifier nullNotifier = (status) -> {};
    zkState = stateMutable.finalizeInteraction(computation, nullRequester, nullNotifier);
    Set<Integer> variables = actualValues.keySet();
    for (Integer variableId : variables) {
      if (missing(variableId)) {
        actualValues = actualValues.remove(variableId);
      }
    }
  }

  /**
   * Create mutable state from immutable.
   *
   * @param zkState immutable state
   * @param context contract context
   * @param factory creates finite field elements
   * @return mutable state
   */
  protected abstract ZkStateMutable makeMutable(
      ZkStateImmutable zkState,
      ZkBinderContextWithGas context,
      FiniteFieldElementFactory<BinaryExtensionFieldElement> factory);

  private boolean missing(Integer variableId) {
    return zkState.getVariable(variableId) == null
        && zkState.getPendingInput().getValue(variableId) == null;
  }

  /**
   * Get bit-lengths of shares of a variable.
   *
   * @param id variable id
   * @return list of bit-lengths
   */
  public List<Integer> getVariableBitLengths(int id) {
    return zkState.getVariable(id).getShareBitLengths();
  }

  /**
   * Get data for a variable.
   *
   * @param id id of variable
   * @return data for variable
   */
  public abstract List<byte[]> getDataForVariable(int id);

  private ZkClosedImpl getPendingInput(BlockchainAddress owner, Hash transaction) {
    return zkState.getPendingInput().values().stream()
        .map(PendingInputImpl::getVariable)
        .filter(closed -> closed.getTransaction().equals(transaction))
        .filter(closed -> closed.getOwner().equals(owner))
        .findFirst()
        .orElse(null);
  }

  private LargeByteArray getPendingInputtedShortname(BlockchainAddress owner, Hash transaction) {
    return zkState.getPendingInput().values().stream()
        .filter(pending -> pending.getVariable().getTransaction().equals(transaction))
        .filter(closed -> closed.getVariable().getOwner().equals(owner))
        .map(PendingInputImpl::getOnInputtedShortname)
        .filter(Objects::nonNull)
        .findFirst()
        .orElse(null);
  }

  List<Integer> getAndClearPendingOutput() {
    Assertions.assertThat(zkState)
        .matches(p -> p.hasPendingOnChainOpen(), "has pending on chain open");

    List<Integer> variableIds =
        zkState.getPendingOnChainOpens().stream()
            .flatMap(open -> open.getVariableIds().stream())
            .toList();
    variableIds.forEach(this::openVariable);

    List<Integer> outputIds =
        zkState.getPendingOnChainOpens().stream()
            .map(RealNodeState.PendingOnChainOpen::getOutputId)
            .toList();
    outputIds.forEach(
        id -> updateState(wrapBuilder(builder -> builder.removePendingOnChainOpen(id))));
    return variableIds;
  }

  void openVariable(Integer id) {
    ZkClosedImpl confirmedVariable = zkState.getVariable(id);
    byte[] data = concatBytes(getDataForVariable(id));
    ZkClosedImpl withOpen = confirmedVariable.withOpen(new LargeByteArray(data));
    updateState(wrapBuilder(builder -> builder.setVariable(withOpen)));
  }

  private byte[] concatBytes(List<byte[]> data) {
    byte[] result = new byte[data.stream().map(b -> b.length).reduce(0, Integer::sum)];
    int offset = 0;
    for (byte[] bytes : data) {
      System.arraycopy(bytes, 0, result, offset, bytes.length);
    }
    return result;
  }

  /**
   * Update the state with a list of variables which hold the result of the execution.
   *
   * @param resultVariables the result variables.
   * @param resultParser function which parses a result into a byte array.
   * @param <ResultT> Type of the result.
   * @return a list of variable identifiers.
   */
  public <ResultT> List<Integer> updateWithResult(
      List<ResultT> resultVariables, Function<ResultT, LargeByteArray> resultParser) {
    List<Integer> variableIdCollector =
        updateState(
            mutable -> {
              ComputationStateImpl computationState = mutable.getState().getComputationState();
              Hash partialOpens = computationState.computeStatus(factory).getPartialOpens();
              return mutable.completeComputation(partialOpens);
            });
    for (int i = 0; i < variableIdCollector.size(); i++) {
      LargeByteArray apply = resultParser.apply(resultVariables.get(i));
      Integer variableId = variableIdCollector.get(i);
      actualValues = actualValues.set(variableId, apply);
    }
    return variableIdCollector;
  }

  List<byte[]> getVariableValue(int id) {
    ZkClosedImpl variable = zkState.getVariable(id);
    Assertions.assertThat(variable).as("Variable " + id).isNotNull();
    return getDataForVariable(id);
  }

  void setTime(long blockTime, long blockProductionTime) {
    this.blockTime = blockTime;
    this.blockProductionTime = blockProductionTime;
  }

  /**
   * Get the immutable zk state.
   *
   * @return immutable zk state
   */
  public ZkStateImmutable zkState() {
    return zkState;
  }

  /**
   * Execute a computation.
   *
   * @param computation the computation to execute
   * @param channel the protocol used in the execution
   * @param <NumberT> the type of numbers
   * @param <BitT> the type of bits
   */
  public <NumberT extends SharedNumber<NumberT>, BitT extends SharedBit<BitT>>
      void executeComputation(
          RealComputationEnv<WasmRealContractState, LargeByteArray> computation,
          RealEnvironment<NumberT, BitT> channel) {
    requireNonNull(computation);
    updateState(
        wrapMutable(
            zkState -> computation.compute(zkState.getState().getComputationState(), channel)));
  }

  void onCreate(
      final TestZkBinderContext ctx,
      final RealContractInvoker contractInvoker,
      Consumer<SafeDataOutputStream> input) {
    updateState(
        ctx,
        (binderContext, zkState) ->
            with(
                input,
                stream -> {
                  var wasmStateAndEvents = contractInvoker.onCreate(binderContext, zkState, stream);
                  saveEvents(wasmStateAndEvents.eventResult());
                  return wasmStateAndEvents.wasmState();
                }));
  }

  void onOpenInput(
      final TestZkBinderContext ctx,
      final RealContractInvoker contractInvoker,
      Consumer<SafeDataOutputStream> input) {
    updateState(
        ctx,
        (binderContext, zkState) ->
            with(
                input,
                stream -> {
                  var wasmStateAndEvents =
                      contractInvoker.onOpenInput(binderContext, zkState, getOpenState(), stream);
                  saveEvents(wasmStateAndEvents.eventResult());
                  return wasmStateAndEvents.wasmState();
                }));
  }

  void onCallback(
      final TestZkBinderContext ctx,
      final RealContractInvoker contractInvoker,
      Consumer<SafeDataOutputStream> input,
      FixedList<CallbackContext.ExecutionResult> executionResult) {
    CallbackContext callbackCtx = CallbackContext.create(executionResult);
    updateState(
        ctx,
        (binderContext, zkState) ->
            with(
                input,
                stream -> {
                  var wasmStateAndEvents =
                      contractInvoker.onCallback(
                          binderContext, zkState, getOpenState(), callbackCtx, stream);
                  saveEvents(wasmStateAndEvents.eventResult());
                  return wasmStateAndEvents.wasmState();
                }));
  }

  RealContractInvoker.VariableDefinition onSecretInput(
      final TestZkBinderContext ctx,
      final RealContractInvoker contractInvoker,
      Consumer<SafeDataOutputStream> input) {
    List<RealContractInvoker.VariableDefinition> variableDefinitions = new ArrayList<>();
    updateState(
        ctx,
        (binderContext, zkState) ->
            with(
                input,
                stream -> {
                  final var wasmStateAndEvents =
                      contractInvoker.onSecretInput(binderContext, zkState, getOpenState(), stream);
                  saveEvents(wasmStateAndEvents.eventResult());
                  variableDefinitions.add(wasmStateAndEvents.variableDefinition());
                  return wasmStateAndEvents.wasmState();
                }));
    Assertions.assertThat(variableDefinitions).hasSize(1);
    return variableDefinitions.get(0);
  }

  void onVariableInputted(
      final TestZkBinderContext ctx,
      final RealContractInvoker contractInvoker,
      final int variableId,
      final byte[] inputtedShortnameBytes) {
    updateState(
        ctx,
        (binderContext, zkState) -> {
          var wasmStateAndEvents =
              contractInvoker.onVariableInputted(
                  binderContext, zkState, getOpenState(), variableId, inputtedShortnameBytes);
          saveEvents(wasmStateAndEvents.eventResult());
          return wasmStateAndEvents.wasmState();
        });
  }

  /**
   * Mark the contract as complemented and update the state with the result.
   *
   * @param contractInvoker the contract.
   * @param variableIds a list of output variable identifiers.
   */
  public void onComputeComplete(
      final RealContractInvoker contractInvoker,
      final byte[] onComputeCompleteShortname,
      final List<Integer> variableIds) {
    updateState(
        binderContext(RANDOM_COMPUTATION_ENGINE_ADDR),
        (binderContext, zkState) -> {
          var wasmStateAndEvents =
              contractInvoker.onComputeComplete(
                  binderContext, zkState, getOpenState(), variableIds, onComputeCompleteShortname);
          saveEvents(wasmStateAndEvents.eventResult());
          return wasmStateAndEvents.wasmState();
        });
  }

  void onVariablesOpened(final RealContractInvoker contractInvoker, final List<Integer> output) {
    updateState(
        binderContext(RANDOM_COMPUTATION_ENGINE_ADDR),
        (binderContext, zkState) -> {
          var wasmStateAndEvents =
              contractInvoker.onVariablesOpened(binderContext, zkState, getOpenState(), output);
          saveEvents(wasmStateAndEvents.eventResult());
          return wasmStateAndEvents.wasmState();
        });
  }

  private DataAttestation getAttestation(int attestationId) {
    DataAttestation attestation =
        zkState.getAttestations().stream()
            .filter(a -> a.getId() == attestationId)
            .findFirst()
            .orElse(null);
    Assertions.assertThat(attestation)
        .withFailMessage("No attestation with ID=%s", attestationId)
        .isNotNull();
    return attestation;
  }

  void updateAttestation(int attestationId, int nodeIndex, KeyPair signingNode) {
    DataAttestation attestation = getAttestation(attestationId);
    byte[] data = attestation.getData();
    updateState(
        binderContext(RANDOM_COMPUTATION_ENGINE_ADDR),
        (binderContext, zkState) -> {
          BlockchainAddress contractAddress = binderContext.getContractAddress();
          Hash hash =
              Hash.create(
                  s -> {
                    // see RealContractBinder
                    s.write("ZK_REAL_ATTESTATION".getBytes(StandardCharsets.UTF_8));
                    contractAddress.write(s);
                    s.write(data);
                  });
          Signature sign = signingNode.sign(hash);
          zkState.updateAttestation(nodeIndex, attestationId, sign);
          return getOpenState();
        });
  }

  /**
   * Check if an attestation has received enough signatures to be considered completed.
   *
   * @param attestation the attestation
   * @return true if the attestation has received at least 3 signatures and false otherwise.
   */
  static boolean attestationHasEnoughSignatures(DataAttestation attestation) {
    int count = 0;
    for (int i = 0; i < 4; i++) {
      if (attestation.getSignature(i) != null) {
        count++;
      }
    }
    return count > 2;
  }

  void onAttestationComplete(final RealContractInvoker contractInvoker, int attestationId) {
    DataAttestation attestation = getAttestation(attestationId);
    Assertions.assertThat(attestationHasEnoughSignatures(attestation))
        .withFailMessage(
            "Attestation with ID=%s does not have enough signatures to be considered complete",
            attestationId)
        .isTrue();
    updateState(
        binderContext(RANDOM_COMPUTATION_ENGINE_ADDR),
        (binderContext, zkState) -> {
          var wasmStateAndEvents =
              contractInvoker.onAttestationComplete(
                  binderContext, zkState, getOpenState(), attestationId);
          saveEvents(wasmStateAndEvents.eventResult());
          return wasmStateAndEvents.wasmState();
        });
  }

  private WasmRealContractState with(
      Consumer<SafeDataOutputStream> input,
      Function<SafeDataInputStream, WasmRealContractState> consumer) {
    byte[] serialize = SafeDataOutputStream.serialize(input);
    return SafeDataInputStream.readFully(serialize, consumer);
  }

  /**
   * Convert the events in the event result in order to save them.
   *
   * @param eventResult the event result of the REAL invocation
   */
  private void saveEvents(EventResult eventResult) {
    var binderResult = eventResult.toBinderResult(null, AVAILABLE_GAS);
    invocations = binderResult.getInvocations();
    callResult = binderResult.getCallResult();
  }

  final class TestZkBinderContext implements ZkBinderContextWithGas {

    private final BlockchainAddress from;

    TestZkBinderContext(BlockchainAddress from) {
      requireNonNull(from);
      this.from = from;
    }

    @Override
    public void payServiceFees(final long amount, final BlockchainAddress from) {
      // Currently does nothing
    }

    @Override
    public void registerCpuFee(final long amount) {
      // Currently does nothing
    }

    @Override
    public long availableGas() {
      return AVAILABLE_GAS;
    }

    @Override
    public long getBlockTime() {
      return blockTime;
    }

    @Override
    public long getBlockProductionTime() {
      return blockProductionTime;
    }

    @Override
    public BlockchainAddress getFrom() {
      return from;
    }

    @Override
    public Hash getCurrentTransactionHash() {
      return AbstractRealContractTest.EMPTY_HASH;
    }

    @Override
    public Hash getOriginalTransactionHash() {
      return AbstractRealContractTest.EMPTY_HASH;
    }

    @Override
    public BlockchainAddress getContractAddress() {
      return AbstractRealContractTest.CONTRACT_ADDRESS;
    }

    @Override
    public void payFromCaller(long fees, List<BlockchainAddress> receivers) {
      // Currently does nothing
    }

    @Override
    public void payFromContract(long fees, List<BlockchainAddress> receivers) {
      // Currently does nothing
    }
  }

  private static final class UpdateStateRetainResult<T>
      implements BiFunction<TestZkBinderContext, ZkStateMutable, WasmRealContractState> {

    private final Function<ZkStateMutable, T> update;
    private T result;

    public UpdateStateRetainResult(Function<ZkStateMutable, T> update) {
      this.update = update;
    }

    @Override
    public WasmRealContractState apply(TestZkBinderContext c, ZkStateMutable zkState) {
      this.result = update.apply(zkState);
      return zkState.getState().getOpenState();
    }
  }

  /** Factory for constructing a {@link ZkStateController}. */
  public interface ControllerFactory {
    /**
     * Create a state controller.
     *
     * @param read immutable state
     * @param state inner state controller
     * @param computation the REAL computation
     * @return state controller
     */
    ZkStateController create(
        ZkStateImmutable read,
        ZkStateController state,
        RealComputationEnv<WasmRealContractState, LargeByteArray> computation);
  }
}
