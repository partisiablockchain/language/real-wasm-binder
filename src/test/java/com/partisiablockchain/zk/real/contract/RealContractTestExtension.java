package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.contract.binary.BinaryRealContractTest;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/** Extension to be used by {@link RealContractTest}. */
final class RealContractTestExtension implements BeforeEachCallback, AfterEachCallback {

  @Override
  public void beforeEach(ExtensionContext context) {
    RealTest annotation = context.getRequiredTestMethod().getAnnotation(RealTest.class);
    if (annotation != null) {
      String previous = annotation.previous();
      String fromFile = annotation.fromFile();
      var test = (RealContractTest) context.getRequiredTestInstance();
      var stateLoader = (RealContractStateLoaderRule) test.getStateLoader();
      if (!previous.isEmpty()) {
        stateLoader.setStateFromReference(previous);
      } else if (!fromFile.isEmpty()) {
        stateLoader.setStateFromReference(fromFile);
      }
    }
  }

  @Override
  public void afterEach(ExtensionContext context) {
    final String testName = context.getRequiredTestMethod().getName();
    final RealTest annotation = context.getRequiredTestMethod().getAnnotation(RealTest.class);
    if (annotation != null) {
      // Reference test state
      var test = (RealContractTest) context.getRequiredTestInstance();
      var stateLoader = (RealContractStateLoaderRule) test.getStateLoader();
      stateLoader.saveFile(testName);

      // Reference computation report
      if (test instanceof BinaryRealContractTest binaryRealContractTest) {
        final String report = binaryRealContractTest.multiplier().report();
        if (report != null) {
          stateLoader.assertOrUpdateComputationReference(
              report, stateLoader.computationReferencePath(testName));
        }
      }
    }
  }
}
