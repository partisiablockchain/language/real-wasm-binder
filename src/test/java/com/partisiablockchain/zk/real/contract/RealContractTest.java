package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.binder.RealContractInvoker;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * Allows programmers of {@link RealContract} to write complete tests of the contracts without
 * booting the blockchain.
 *
 * <p>{@link AbstractRealContractTest} for how to use the available methods for tests.
 *
 * <p>{@link RealContractTest} adds the capability to load state from a file or a previous test.
 *
 * <p>Each test should be annotated by {@link RealTest}.
 */
@TestMethodOrder(RealTestMethodOrderer.class)
@ExtendWith(RealContractTestExtension.class)
public abstract class RealContractTest extends AbstractRealContractTest {

  private static final String DISABLE_REFERENCE_FILE_CHECK = "DISABLE_REFERENCE_FILE_CHECK";

  /** Rule for loading REAL contract states. */
  public final RealContractStateLoaderRule stateLoader;

  @Override
  protected StateLoader getStateLoader() {
    return stateLoader;
  }

  /**
   * Initializes the test framework.
   *
   * @param contractInvoker the contract to test
   * @param referenceFileRoot where to put the generated reference files
   * @param stateFactory can create state controller
   * @param stateCreator creates states
   */
  @SuppressWarnings({"WeakerAccess"})
  protected RealContractTest(
      final RealContractInvoker contractInvoker,
      Function<Class<?>, Path> referenceFileRoot,
      Supplier<ZkStateController> stateFactory,
      StateHandlerFactory stateCreator) {
    super(contractInvoker);

    StateHandler handler =
        stateCreator.create(
            (RealComputationEnv<WasmRealContractState, LargeByteArray>)
                contractInvoker.getContract().getComputation(),
            stateFactory);
    String environmentVariable = System.getenv(DISABLE_REFERENCE_FILE_CHECK);

    Class<? extends RealContractTest> clazz = getClass();
    Predicate<String> hasDependency = RealRunner.dependencyResolver(clazz);

    Path referenceFilePath = referenceFileRoot.apply(clazz);
    this.stateLoader =
        createLoader(handler, referenceFilePath, hasDependency, environmentVariable, stateFactory);
  }

  /** Helper interface for constructing a {@link StateHandler} object. */
  protected interface StateHandlerFactory {

    /**
     * Create a state handler.
     *
     * @param computation REAL computation
     * @param stateFactory supplies state controller
     * @return created state handler
     */
    StateHandler create(
        RealComputationEnv<WasmRealContractState, LargeByteArray> computation,
        Supplier<ZkStateController> stateFactory);
  }

  static RealContractStateLoaderRule createLoader(
      StateHandler handler,
      Path path,
      Predicate<String> hasDependency,
      String getenv,
      Supplier<ZkStateController> stateFactory) {
    boolean shouldTestReferenceFile = !Boolean.parseBoolean(getenv);
    return new RealContractStateLoaderRule(
        handler, path, hasDependency, shouldTestReferenceFile, stateFactory);
  }

  /**
   * Find path to directory where a test class is located.
   *
   * @param clazz test class to look for
   * @return path to test class directory
   */
  protected static Path nextToTestSources(Class<?> clazz) {
    String[] split = clazz.getPackageName().split("\\.");
    Path referencePathDir = Paths.get("./src/test/resources/", split);
    return referencePathDir.resolve("ref").resolve(clazz.getSimpleName());
  }
}
