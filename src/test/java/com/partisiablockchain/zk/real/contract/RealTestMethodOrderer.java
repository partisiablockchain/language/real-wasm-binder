package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;
import org.junit.jupiter.api.MethodDescriptor;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.MethodOrdererContext;

/** Method orderer responsible for ordering Real Tests. */
public final class RealTestMethodOrderer implements MethodOrderer {

  @Override
  public void orderMethods(MethodOrdererContext methodOrdererContext) {
    List<? extends MethodDescriptor> methodDescriptors =
        methodOrdererContext.getMethodDescriptors();
    List<Method> sortedMethods =
        RealRunner.computeTestMethods(
            methodDescriptors.stream().map(MethodDescriptor::getMethod).toList());
    methodDescriptors.sort(Comparator.comparingInt(m -> sortedMethods.indexOf(m.getMethod())));
  }
}
