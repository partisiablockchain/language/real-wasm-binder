package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.binder.PendingOnChainOpenImpl;
import com.partisiablockchain.zk.real.binder.RealContractInvoker;
import com.partisiablockchain.zk.real.binder.ZkStateImmutable;
import com.partisiablockchain.zk.real.contract.RealNodeState.PendingInput;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;

/**
 * Allows programmers of {@link RealContract} to write complete tests of the contracts without
 * booting the blockchain. Inherit from this class and use the protected methods to invoke the smart
 * contract. All methods starting with "send" is send from the users of the contract.
 *
 * <p>Use the constructor to initialize, then call {@link #create(Consumer) create} to create for
 * contract and state.
 *
 * <p>The first step does not need access to the previous state - it initializes a new state. The
 * following tests can use the state resulting from the first step by referencing the method name in
 * previous.
 *
 * <ul>
 *   <li>Use {@link #create} to create the smart contract
 *   <li>Use {@link #sendOpenInput} and {@link #sendSecretInput} to interact with the smart contract
 *       as a user
 *   <li>Use {@link #confirmSecretInput}, {@link #executeComputation}, {@link #resolveOpens} and
 *       {@link #completeAttestation} to trigger the events of the computing nodes
 *   <li>Use {@link #callback} to trigger a callback
 * </ul>
 *
 * <p>Inspect state via {@link #getCalculationStatus()}, {@link #getContractState()}, {@link
 * #getPendingInput}, {@link #getVariables(BlockchainAddress)}, {@link
 * #hasPendingOnChainOpen()},{@link #getPendingOnChainOpen()}, {@link #getVariableValue(int)},
 * {@link #getInteractions()}, {@link #getResult()}, {@link #getRemoteCalls()} and {@link
 * #getAttestations()} to inspect the state after each interaction.
 */
public abstract class AbstractRealContractTest {

  /** Utility for creating an empty hash. */
  public static final Hash EMPTY_HASH = Hash.create(FunctionUtility.noOpConsumer());

  /** Empty input - useful if the interaction does not need input. */
  protected static final Consumer<SafeDataOutputStream> EMPTY = FunctionUtility.noOpConsumer();

  /** Contract address. */
  protected static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromString("010000000000000000000000000000000000000001");

  /** Owner of the contract. */
  protected static final BlockchainAddress OWNER =
      BlockchainAddress.fromString("000000000000000000000000000000000000123456");

  /** The REAL contract. */
  protected final RealContractInvoker contractInvoker;

  /** State load responsible, can load state from disc or previous sessions. */
  public interface StateLoader {

    /**
     * Get contract state controller.
     *
     * @return contract state controller
     */
    ZkStateController stateController();

    /**
     * Get contract state.
     *
     * @return contract state
     */
    ZkStateImmutable zkContractState();

    /** Refresh the state with a refresh, clean instance. */
    void newState();
  }

  /**
   * Get state loader.
   *
   * @return state loader
   */
  protected abstract StateLoader getStateLoader();

  /**
   * Initializes the test framework.
   *
   * @param contractInvoker the contract to test
   */
  @SuppressWarnings({"WeakerAccess"})
  protected AbstractRealContractTest(final RealContractInvoker contractInvoker) {
    this.contractInvoker = contractInvoker;
  }

  /**
   * Utility function for creating a {@link BlockchainAddress} for a user.
   *
   * @param i integer value for this address
   * @return blockchain address for this user
   */
  protected BlockchainAddress getUser(int i) {
    StringBuilder name = new StringBuilder(String.valueOf(i));
    while (name.length() < 42) {
      name.insert(0, "0");
    }
    return BlockchainAddress.fromString(name.toString());
  }

  private ZkStateController.TestZkBinderContext getBinderContext(final BlockchainAddress sender) {
    return getStateLoader().stateController().binderContext(sender);
  }

  /**
   * Prepare state and apply function.
   *
   * @param consumer function to apply to state
   * @param <T> return type of function
   * @return result of applying function to state
   */
  protected <T> T prepareState(Function<ZkStateController, T> consumer) {
    ZkStateController state = getStateLoader().stateController();
    return consumer.apply(state);
  }

  /**
   * Gets the pending variables for an owner.
   *
   * @param owner the owner
   * @return the corresponding pending variables
   */
  protected List<RealClosed<LargeByteArray>> getPendingInput(BlockchainAddress owner) {
    return getStateLoader().zkContractState().getPendingInputs().stream()
        .map(PendingInput::getVariable)
        .filter(variable -> variable.getOwner().equals(owner))
        .collect(Collectors.toList());
  }

  /**
   * Gets the confirmed variables for an owner.
   *
   * @param owner the owner
   * @return the corresponding confirmed variables
   */
  protected List<RealClosed<LargeByteArray>> getVariables(BlockchainAddress owner) {
    return getStateLoader().zkContractState().getVariables().stream()
        .filter(var -> var.getOwner().equals(owner))
        .collect(Collectors.toList());
  }

  /**
   * Gets every variable in the smart contract.
   *
   * @return the list of variables
   */
  protected List<RealClosed<LargeByteArray>> getVariables() {
    return getStateLoader().zkContractState().getVariables();
  }

  /**
   * Gets a variable with a specific id in the smart contract.
   *
   * @param id the isd to lookup
   * @return the variable
   */
  protected RealClosed<LargeByteArray> getVariable(int id) {
    return getStateLoader().zkContractState().getVariable(id);
  }

  /**
   * Gets the current calculation status for the zk contract.
   *
   * @return the current calculation status
   */
  protected CalculationStatus getCalculationStatus() {
    return getStateLoader().zkContractState().getCalculationStatus();
  }

  /**
   * Gets the current list of variable openings ready to be output by the nodes. Returns null if the
   * current state is not outputting.
   *
   * @return the list of pending output openings
   */
  protected List<PendingOnChainOpenImpl> getPendingOnChainOpen() {
    return getStateLoader().zkContractState().getPendingOnChainOpensRaw().values();
  }

  /**
   * Checks if there is pending on chain output.
   *
   * @return true if {@link #getPendingOnChainOpen()} is valid
   */
  protected boolean hasPendingOnChainOpen() {
    return getStateLoader().zkContractState().hasPendingOnChainOpen();
  }

  /**
   * Gets the current global contract state.
   *
   * @return the contract state
   */
  protected WasmRealContractState getContractState() {
    return getStateLoader().zkContractState().getOpenState();
  }

  /**
   * Gets the value of a variable.
   *
   * @param id the variable id
   * @return the byte array of this variable
   */
  protected List<byte[]> getVariableValue(int id) {
    return getStateLoader().stateController().getVariableValue(id);
  }

  /**
   * Gets the result of the execution.
   *
   * @return the result
   */
  protected CallReturnValue getResult() {
    return getStateLoader().stateController().getResult();
  }

  /**
   * Gets the event group from the call back structure.
   *
   * @return the event group
   */
  protected BinderEventGroup<BinderInteraction> getRemoteCalls() {
    return getStateLoader().stateController().getRemoteCalls();
  }

  /**
   * Gets the list of events.
   *
   * @return the events
   */
  protected List<BinderInteraction> getInteractions() {
    return getStateLoader().stateController().getInteractions();
  }

  /**
   * Sets the block time and block production time for the following execution.
   *
   * @param blockTime new block time
   * @param blockProductionTime new block production time
   */
  protected void setTime(long blockTime, long blockProductionTime) {
    getStateLoader().stateController().setTime(blockTime, blockProductionTime);
  }

  /**
   * Creates the smart contract using OWNER as a sender.
   *
   * @param input the input if any
   */
  protected void create(Consumer<SafeDataOutputStream> input) {
    Assertions.assertThat(getStateLoader().stateController()).as("State controller").isNull();
    getStateLoader().newState();
    getStateLoader().stateController().onCreate(getBinderContext(OWNER), contractInvoker, input);
  }

  /**
   * Sends a callback event to the contract. On the blockchain this would be sent automatically as
   * part of handling the event group of an interaction. In the tests this is done explicitly for
   * flexibility.
   *
   * @param input the invocation input
   * @param results the results of the event group in the call result
   */
  protected void callback(
      Consumer<SafeDataOutputStream> input, FixedList<CallbackContext.ExecutionResult> results) {
    requireNonNull(input);
    requireNonNull(results);
    getStateLoader()
        .stateController()
        .onCallback(getBinderContext(OWNER), contractInvoker, input, results);
  }

  /**
   * Sends input from a client to the contract.
   *
   * @param transaction the hash of the transaction that creates the variable
   * @param variableValue the value of the variable, can be created using {@link #asShares(List)}
   * @param bitLengths the bit lengths of the variables
   * @param sender the sender of transaction
   * @param onChain true if this input should be simulated as onChain input
   * @param input the invocation input
   */
  protected void sendSecretInput(
      Hash transaction,
      List<byte[]> variableValue,
      List<Integer> bitLengths,
      BlockchainAddress sender,
      boolean onChain,
      Consumer<SafeDataOutputStream> input) {
    requireNonNull(transaction);
    requireNonNull(variableValue);
    requireNonNull(bitLengths);
    requireNonNull(sender);
    requireNonNull(input);
    validateProvidedInput(variableValue, bitLengths);
    ZkStateController.TestZkBinderContext binderContext = getBinderContext(sender);
    final RealContractInvoker.VariableDefinition variableDefinition =
        getStateLoader().stateController().onSecretInput(binderContext, contractInvoker, input);
    byte[] variableBytes =
        new byte[variableValue.stream().map(v -> v.length).reduce(0, Integer::sum)];
    int offset = 0;
    for (byte[] bytes : variableValue) {
      System.arraycopy(bytes, 0, variableBytes, offset, bytes.length);
      offset += bytes.length;
    }
    Assertions.assertThat(variableDefinition.bitLengths())
        .as("Variable bitlengths")
        .isEqualTo(bitLengths);
    getStateLoader()
        .stateController()
        .addPendingVariable(
            binderContext,
            transaction,
            variableBytes,
            bitLengths,
            sender,
            variableDefinition.seal(),
            new LargeByteArray(variableDefinition.metadata()),
            variableDefinition.onInputtedShortname());
  }

  /**
   * Validate input that was provided in sendSecretInput. Validation is protocol dependent. In
   * binary REAL, the sum of the bit lengths must match the size of <code>variableValue</code>,
   * whereas in arithmetic REAL <code>variableValue.length</code> must divide 16 and <code>
   * variableValue.length / 16</code> must be the same as <code>bitLengths.size()</code>.
   *
   * @param variableValue the secret input
   * @param bitLengths the bit lengths of the inputs. Determines the number of inputs as well.
   */
  protected abstract void validateProvidedInput(
      List<byte[]> variableValue, List<Integer> bitLengths);

  /**
   * Confirms the input - on the blockchain this would be done by the nodes. In the test this needs
   * to be done explicitly for flexibility.
   *
   * @param transaction the hash of the transaction that create this variable.
   * @param owner the variable owner.
   */
  protected void confirmSecretInput(Hash transaction, BlockchainAddress owner) {
    ZkStateController.ConfirmedSecretVariableInfo secretVarInfo =
        prepareState(zkStateController -> zkStateController.confirmSecretInput(transaction, owner));
    final LargeByteArray onInputtedShortname = secretVarInfo.onInputtedShortname();
    if (onInputtedShortname != null) {
      final int variableId = secretVarInfo.var().getId();
      getStateLoader()
          .stateController()
          .onVariableInputted(
              getBinderContext(CONTRACT_ADDRESS),
              contractInvoker,
              variableId,
              onInputtedShortname.getData());
    }
  }

  /**
   * Sends open input to the contract.
   *
   * @param sender the sender of transaction
   * @param input the invocation input
   */
  protected void sendOpenInput(BlockchainAddress sender, Consumer<SafeDataOutputStream> input) {
    getStateLoader()
        .stateController()
        .onOpenInput(getBinderContext(sender), contractInvoker, input);
  }

  /**
   * Executes the Zk computation - using a test channel working on clear text data. In the test this
   * needs to be done explicitly for flexibility.
   *
   * @param random the randomness used for selecting random bits in the channel
   */
  protected abstract void executeComputation(Random random);

  /**
   * Mimics that the nodes are ready for opening the variables. In the test this needs to be done
   * explicitly for flexibility.
   */
  protected void resolveOpens() {
    List<Integer> output = prepareState(ZkStateController::getAndClearPendingOutput);
    getStateLoader().stateController().onVariablesOpened(contractInvoker, output);
  }

  /**
   * Get all attestations currently in the contract state.
   *
   * @return all attestations in the contract's state.
   */
  protected List<DataAttestation> getAttestations() {
    return getStateLoader().zkContractState().getAttestations();
  }

  /**
   * Complete an attestation by explicitly specifying which nodes should have signed the
   * attestation.
   *
   * @param attestationId the ID of the attestation
   * @param signingNodes the indices of the nodes signing the attestation request
   */
  protected void completeAttestation(int attestationId, int... signingNodes) {
    Assertions.assertThat(signingNodes)
        .withFailMessage("At least 3 nodes must sign an attestation")
        .hasSizeGreaterThan(2);
    for (int signingNode : signingNodes) {
      getStateLoader()
          .stateController()
          .updateAttestation(attestationId, signingNode, node(signingNode));
    }
    getStateLoader().stateController().onAttestationComplete(contractInvoker, attestationId);
  }

  /**
   * Complete an attestation request.
   *
   * @param attestationId the ID of the attestation
   */
  protected void completeAttestation(int attestationId) {
    completeAttestation(attestationId, 0, 1, 2, 3);
  }

  /**
   * Create exclusive pair with only integral number value.
   *
   * @param number integral number value
   * @return exclusive pair
   */
  protected Integer makeNumber(int number) {
    return number;
  }

  /**
   * Create exclusive pair with only boolean value.
   *
   * @param value boolean value
   * @return exclusive pair
   */
  protected Integer makeBoolean(boolean value) {
    return value ? 1 : 0;
  }

  /**
   * Helper function that creates the share version of an integer.
   *
   * @param values the value to secret share
   * @return the byte array.
   */
  protected abstract List<byte[]> asShares(List<Integer> values);

  private KeyPair node(int nodeIndex) {
    return ComputationEngines.signingKeyOf(nodeIndex);
  }

  /**
   * Test if an attestation has been signed by a particular node.
   *
   * @param nodeIndex the node
   * @param attestation the attestation
   * @return true if the node signed this attestation and false otherwise.
   */
  protected boolean isSignedBy(int nodeIndex, DataAttestation attestation) {
    Hash hash =
        Hash.create(
            s -> {
              s.write("ZK_REAL_ATTESTATION".getBytes(StandardCharsets.UTF_8));
              CONTRACT_ADDRESS.write(s);
              s.write(attestation.getData());
            });
    BlockchainPublicKey publicKey = node(nodeIndex).getPublic();
    Signature signature = attestation.getSignature(nodeIndex);
    return signature != null && signature.recoverPublicKey(hash).equals(publicKey);
  }
}
