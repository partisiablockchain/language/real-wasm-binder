package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.junit.jupiter.api.Test;

/**
 * Annotation to mark a method as a test method. Use by annotating methods with
 * {@code @RealTest(previous="otherMethod")}.
 *
 * @see #previous()
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Test
public @interface RealTest {

  /**
   * Gets the previous test which supplies the initial state for this test. The name must match
   * exactly a method name on the same class - also with the same annotation, i.e. a (test) method
   * refers another test method.
   *
   * @return the name of the previous test
   */
  String previous() default "";

  /**
   * Gets the name of the state file that supplies the initial state for this test. It is
   * recommended to always use {@link #previous()} to supply the step that is responsible for
   * creating the initial state of this test. This should only be used when one need to test states
   * that are not reachable by interactions on the contract.
   *
   * @return the name of the state file
   */
  String fromFile() default "";
}
