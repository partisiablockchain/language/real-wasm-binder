package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import org.junit.platform.commons.support.AnnotationSupport;
import org.junit.platform.commons.support.HierarchyTraversalMode;

/** Utility class for running Real tests. */
public final class RealRunner {

  private final Class<? extends RealContractTest> clazz;

  /**
   * Create a runner for REAL protocol.
   *
   * @param clazz type of contract to run
   */
  @SuppressWarnings("WeakerAccess")
  public RealRunner(Class<? extends RealContractTest> clazz) {
    this.clazz = clazz;
  }

  /**
   * Calculate order of Real test methods execution.
   *
   * @param realTestMethods list of methods to be sorted
   * @return ordered test methods
   */
  public static List<Method> computeTestMethods(List<Method> realTestMethods) {
    Map<String, List<Method>> dependencies = new HashMap<>();
    Deque<Method> resolvedDependencies = new ArrayDeque<>();

    forEachAnnotatedTest(
        (method, previous, fromFile) -> {
          if (!previous.isEmpty() && !fromFile.isEmpty()) {
            throw new RuntimeException(
                "Test declared with both previous and fromFile: " + method.getName());
          }
          if (!previous.isEmpty()) {
            List<Method> methods = dependencies.computeIfAbsent(previous, s -> new ArrayList<>());
            methods.add(method);
          } else {
            resolvedDependencies.add(method);
          }
        },
        realTestMethods);

    List<Method> ordered = new ArrayList<>();
    while (!resolvedDependencies.isEmpty()) {
      Method method = resolvedDependencies.removeFirst();
      ordered.add(method);
      List<Method> remove = dependencies.remove(method.getName());
      if (remove != null) {
        resolvedDependencies.addAll(remove);
      }
    }
    if (!dependencies.isEmpty()) {
      throw new RuntimeException("Cannot resolve dependencies: " + dependencies.keySet());
    }
    return ordered;
  }

  private static void forEachAnnotatedTest(TestCallback consumer, List<Method> methods) {
    for (Method method : methods) {
      RealTest annotation = method.getAnnotation(RealTest.class);
      if (annotation != null) {
        String previous = annotation.previous();
        String fromFile = annotation.fromFile();
        consumer.accept(method, previous, fromFile);
      }
    }
  }

  private void forEachAnnotatedTest(TestCallback consumer) {
    List<Method> testMethods =
        AnnotationSupport.findAnnotatedMethods(
            clazz, RealTest.class, HierarchyTraversalMode.TOP_DOWN);
    forEachAnnotatedTest(consumer, testMethods);
  }

  static Predicate<String> dependencyResolver(Class<? extends RealContractTest> clazz) {
    return testName ->
        ExceptionConverter.call(() -> new RealRunner(clazz).hasDependencies(testName), "Error");
  }

  private boolean hasDependencies(String testName) {
    AtomicBoolean result = new AtomicBoolean();
    forEachAnnotatedTest(
        (method, previous, fromFile) -> {
          if (previous.equals(testName)) {
            result.set(true);
          }
        });
    return result.get();
  }

  @FunctionalInterface
  private interface TestCallback {

    void accept(Method method, String previous, String fromFile);
  }
}
