package com.partisiablockchain.zk.real.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.binder.BinderTest;
import com.partisiablockchain.zk.real.binder.ZkStateImmutable;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.assertj.core.api.Assertions;

final class RealContractStateLoaderRule implements AbstractRealContractTest.StateLoader {

  private final StateHandler handler;
  private final Path referenceFileDir;
  private final Predicate<String> shouldGenerateReferenceFile;
  private final boolean shouldTestReferenceFile;
  private final Supplier<ZkStateController> stateFactory;
  private ZkStateController state;

  RealContractStateLoaderRule(
      StateHandler handler,
      Path referenceFileRoot,
      Predicate<String> shouldGenerateReferenceFile,
      boolean shouldTestReferenceFile,
      Supplier<ZkStateController> stateFactory) {
    this.handler = handler;
    this.referenceFileDir = referenceFileRoot;
    this.shouldGenerateReferenceFile = shouldGenerateReferenceFile;
    this.shouldTestReferenceFile = shouldTestReferenceFile;
    this.stateFactory = stateFactory;
  }

  @Override
  public ZkStateController stateController() {
    return state;
  }

  @Override
  public ZkStateImmutable zkContractState() {
    return state.zkState();
  }

  void setStateFromReference(String fileName) {
    final Path initialStatePath = testReferencePath(fileName);
    Assertions.assertThat(initialStatePath)
        .describedAs("Expected reference file (%s) does not exist", fileName)
        .existsNoFollowLinks();
    final String stateAsString = readRawState(initialStatePath);
    state = handler.read(stateAsString);
  }

  void saveFile(String testName) {
    // Make sure we save the state can be saved
    // I.e. is well formed according to the serializer framework
    String resultingTestState = handler.write(state);

    if (!shouldGenerateReferenceFile.test(testName)) {
      return;
    }

    assertOrUpdateReference(
        resultingTestState, testReferencePath(testName), w -> handler.write(handler.read(w)));
  }

  private void assertOrUpdateReference(
      final String testResult, final Path path, final Function<String, String> referenceMapper) {

    if (BinderTest.shouldRegenerateReferences()) {
      writeToReference(testResult, path);
      // Assertions.fail("Written reference file '%s'".formatted(path));
      System.out.println("Written reference file '%s'".formatted(path));
    }
    if (shouldTestReferenceFile) {
      String referenceState = readRawState(path);
      String expectedStateHandlerFormat = referenceMapper.apply(referenceState);
      Assertions.assertThat(testResult)
          .describedAs(
              path
                  + ": Version problem, stored reference does not match test result.\n"
                  + "Note that the data on disc is unchanged.\n"
                  + BinderTest.differenceReport(testResult, expectedStateHandlerFormat))
          .isEqualToIgnoringNewLines(expectedStateHandlerFormat);
    }
  }

  void assertOrUpdateComputationReference(final String gottenReport, final Path path) {
    assertOrUpdateReference(gottenReport, path, Function.identity());
  }

  private static void writeToReference(String serialized, Path referencePath) {
    WithResource.accept(
        () -> new PrintWriter(referencePath.toFile(), StandardCharsets.UTF_8),
        writer -> writer.write(serialized),
        "Cannot write file during test");
  }

  Path testReferencePath(String testName) {
    return testReferencePathInternal(testName + ".json");
  }

  Path computationReferencePath(String testName) {
    return testReferencePathInternal(testName + ".computation.txt");
  }

  private Path testReferencePathInternal(String testName) {
    final Path parentFolder =
        ExceptionConverter.call(
            () -> Files.createDirectories(referenceFileDir),
            "Could not create required parent directories");
    return parentFolder.resolve(testName);
  }

  private String readRawState(Path path) {
    return WithResource.apply(
        () -> new FileInputStream(path.toFile()),
        stream ->
            new String(
                ExceptionConverter.call(stream::readAllBytes, "Unable"), StandardCharsets.UTF_8),
        "Cannot read stream from file: " + path);
  }

  @Override
  public void newState() {
    state = stateFactory.get();
  }
}
