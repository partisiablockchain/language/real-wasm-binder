package com.partisiablockchain.zk.real.contract.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zk.real.binder.ZkBinderContextWithGas;
import com.partisiablockchain.zk.real.binder.ZkClosedImpl;
import com.partisiablockchain.zk.real.binder.ZkStateImmutable;
import com.partisiablockchain.zk.real.binder.ZkStateMutable;
import com.partisiablockchain.zk.real.contract.ComputationEngines;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.contract.ZkStateController;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElementFactory;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElementFactory;
import java.util.ArrayList;
import java.util.List;

/** {@link ZkStateController} to be used with {@link BinaryRealContractTest}. */
public final class BinaryZkStateController extends ZkStateController {

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  public BinaryZkStateController() {}

  /**
   * Create binary state controller.
   *
   * @param zkState immutable state
   * @param original inner state controller
   * @param computation the REAL computation
   */
  public BinaryZkStateController(
      ZkStateImmutable zkState,
      ZkStateController original,
      RealComputationEnv<WasmRealContractState, LargeByteArray> computation) {
    super(zkState, original, computation, new BinaryExtensionFieldElementFactory());
  }

  /**
   * Create binary state controller.
   *
   * @param zkState immutable state
   * @param actualValues variable data
   */
  public BinaryZkStateController(
      ZkStateImmutable zkState, AvlTree<Integer, LargeByteArray> actualValues) {
    super(zkState, actualValues, new BinaryExtensionFieldElementFactory());
  }

  @Override
  protected ZkStateMutable makeMutable(
      ZkStateImmutable zkState,
      ZkBinderContextWithGas context,
      FiniteFieldElementFactory<BinaryExtensionFieldElement> factory) {
    return ZkStateMutable.createMutableStateBinary(zkState, context);
  }

  @Override
  public List<byte[]> getDataForVariable(int id) {
    byte[] data = actualValues.getValue(id).getData();
    ZkClosedImpl variable = zkState().getVariable(id);
    List<byte[]> values = new ArrayList<>();
    int offset = 0;
    for (Integer size : variable.getShareBitLengths()) {
      int byteLength = size;
      byte[] variableData = new byte[byteLength];
      System.arraycopy(data, offset, variableData, 0, byteLength);
      offset += byteLength;
      values.add(variableData);
    }
    return values;
  }

  /**
   * Create binary state controller.
   *
   * @return created binary state controller
   */
  public static ZkStateController create() {
    return new BinaryZkStateController(
        new ZkStateImmutable(ComputationEngines.ENGINES, null, null, 1234567890L, true),
        AvlTree.create());
  }
}
