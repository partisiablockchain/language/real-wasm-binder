package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import java.math.BigInteger;
import java.util.HexFormat;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AttestationImplTest {

  private static Signature signature =
      new KeyPair(BigInteger.TEN).sign(Hash.create(s -> s.writeString("1")));

  @Test
  void oobSafeGet() {
    AttestationImpl attestation = AttestationImpl.create(42, new byte[2]);
    Assertions.assertThat(attestation.getId()).isEqualTo(42);
    Assertions.assertThat(attestation.getSignature(3)).isNull();
    attestation = attestation.addAttestation(3, signature);
    Assertions.assertThat(attestation.getSignature(3)).isNotNull();
    Assertions.assertThat(attestation.getSignature(2)).isNull();
    Assertions.assertThat(attestation.getSignature(4)).isNull();
  }

  @Test
  void testSerialization() {
    AttestationImpl attestation =
        AttestationImpl.create(1, HexFormat.of().parseHex("01020304"))
            .addAttestation(3, Signature.fromString("00".repeat(65)));

    Assertions.assertThat(attestation.serialize())
        .hasSize(attestation.serializedByteLength())
        .isEqualTo(
            HexFormat.of()
                .parseHex(
                    "01000000" // Attestation id
                        + "04000000" // number of signatures
                        + "000000" // First three signatures are none
                        + "01" // Fourth signature is some
                        + "00".repeat(65) // Signature value
                        + "04000000" // length of data
                        + "01020304")); // data
  }
}
