package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.LargeByteArray;
import java.util.HexFormat;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PendingInputImplTest {
  @Test
  void isHandledByEngine() {

    PendingInputImpl pendingInput = new PendingInputImpl(null, null, null, null, null);

    pendingInput = pendingInput.addNodeReaction(0, null);
    pendingInput = pendingInput.addNodeReaction(2, null);

    assertThat(pendingInput.isHandledByEngine(0)).isTrue();
    assertThat(pendingInput.isHandledByEngine(1)).isFalse();
    assertThat(pendingInput.isHandledByEngine(2)).isTrue();
    assertThat(pendingInput.isHandledByEngine(3)).isFalse();
  }

  @Test
  public void testSerialization() {
    PendingInputImpl pending =
        new PendingInputImpl(
            new ZkClosedImpl(
                BlockchainAddress.fromString("000000000000000000000000000000000000000042"),
                1,
                true,
                new LargeByteArray(HexFormat.of().parseHex("21")),
                List.of(),
                null,
                0),
            null,
            null,
            null,
            null);
    assertThat(pending.serialize())
        .hasSize(pending.serializedByteLength())
        .isEqualTo(
            HexFormat.of()
                .parseHex(
                    "01000000" // Variable id
                        + "000000000000000000000000000000000000000042" // owner
                        + "01" // isSealed
                        + "21" // metadata
                        + "00")); // no open data
  }
}
