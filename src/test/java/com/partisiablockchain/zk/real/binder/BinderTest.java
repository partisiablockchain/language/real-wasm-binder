package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.zk.real.binder.RealContractBinder.PREPROCESS_GENERATE_BATCH_V4;
import static com.partisiablockchain.zk.real.binder.RealContractBinder.calculatePreprocessingFees;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractInvoker;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.zk.real.contract.RealNodeState.ComputationState;
import com.partisiablockchain.zk.real.protocol2.ReferenceTestUtility;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import com.secata.tools.immutable.FixedList;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

/** Test. */
@DisplayNameGeneration(DisplayNameGenerator.Simple.class)
public abstract class BinderTest {

  /** Invocation identifiers. */
  public static final byte COMMIT_RESULT_VARIABLE = 0x00;

  public static final byte VARIABLE_OPENING_SHARES = 0x01;
  public static final byte UNABLE_TO_CALCULATE = 0x02;
  public static final byte OPEN_MASKED_INPUT = 0x03;
  public static final byte ZERO_KNOWLEDGE_INPUT_OFF_CHAIN = 0x04;
  public static final byte ZERO_KNOWLEDGE_INPUT_ON_CHAIN = 0x05;
  public static final byte REJECT_INPUT = 0x06;
  public static final byte OPEN_INVOCATION = 0x09;
  public static final byte ADD_ATTESTATION_SIGNATURE = 0x0a;
  public static final byte GET_COMPUTATION_DEADLINE = 0x0b;
  public static final byte ON_COMPUTE_COMPLETE = 0x0c;
  public static final byte ON_VARIABLES_OPENED = 0x0d;
  public static final byte ON_ATTESTATION_COMPLETE = 0x0e;
  public static final byte ON_VARIABLE_INPUTTED = 0x0f;
  public static final byte ON_VARIABLE_REJECTED = 0x10;
  public static final byte ADD_BATCHES = 0x12;
  public static final byte EXTEND_ZK_COMPUTATION_DEADLINE = 0x13;
  public static final byte ADD_EXTERNAL_EVENT = 0x14;
  public static final byte ON_EXTERNAL_EVENT = 0x15;
  public static final byte REGISTER_EXTERNAL_BLOCK_HEARTBEAT = 0x16;
  public static final byte ON_EXTERNAL_BLOCK_HEARTBEAT = 0x17;

  public static final List<Byte> REMOVED_INVOCATIONS =
      List.of((byte) 0x07, (byte) 0x08, (byte) 0x11);
  protected static final BlockchainAddress ACCOUNT_ONE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  protected static final BlockchainAddress ACCOUNT_TWO =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  protected static final BlockchainAddress ACCOUNT_FOUR =
      BlockchainAddress.fromString("000000000000000000000000000000000000000004");

  protected static final Hash TRANSACTION_HASH = Hash.create(stream -> stream.writeInt(42));
  protected static final Hash TRANSACTION_HASH_2 = Hash.create(stream -> stream.writeInt(12345));
  protected static final Hash ORIGINAL_TRANSACTION_HASH =
      Hash.create(stream -> stream.writeInt(1337));
  protected static final BlockchainAddress OWNER = ACCOUNT_ONE;
  protected static final BlockchainAddress CONTRACT_ONE =
      BlockchainAddress.fromString("01A000000000000000000000000000000000000001");

  public static Instant MAINNET_LAUNCH_DATE =
      LocalDateTime.of(2022, Month.MAY, 31, 9, 0).toInstant(ZoneOffset.UTC);
  public static Instant VERSION_3_LAUNCH_DATE =
      LocalDateTime.of(2022, Month.JUNE, 30, 0, 0).toInstant(ZoneOffset.UTC);
  public static long DEFAULT_ZK_COMPUTATION_DEADLINE =
      VERSION_3_LAUNCH_DATE.plus(28, ChronoUnit.DAYS).toEpochMilli();

  // Minimum staking fees is 1% of 2,000.0000 MPC converted to gas. Assuming 0.4 USD per MPC the
  // minimum staking fee is 800,000 gas.
  public static final long DEFAULT_STAKING_FEE = 800_000L;
  protected static final List<BigInteger> PRIVATE_KEYS =
      List.of(
          BigInteger.valueOf(22),
          BigInteger.valueOf(23),
          BigInteger.valueOf(24),
          BigInteger.valueOf(25));
  protected static final List<BlockchainPublicKey> PUBLIC_KEYS =
      PRIVATE_KEYS.stream().map(pk -> new KeyPair(pk).getPublic()).collect(Collectors.toList());

  /** REAL Node Addresses. */
  protected static final List<BlockchainAddress> ADDRESSES =
      List.of(
          BlockchainAddress.fromString("000000000000000000000000000000000000000022"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000023"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000024"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000025"));

  protected static final BlockchainAddress PREPROCESS_ADDRESS =
      BlockchainAddress.fromString("01A000000000000000000000000000000000000888");
  protected static final BlockchainAddress NODE_REGISTRY_ADDRESS =
      BlockchainAddress.fromString("01A000000000000000000000000000000000000999");

  protected static final BlockchainPublicKey INPUT_KEY = new KeyPair(BigInteger.TEN).getPublic();

  /**
   * Configures whether {@link InteractionsAndResult} should perform verification. Will ignore
   * verification if {@code false}.
   */
  private static final boolean PERFORM_VERIFICATION = true;

  /** Configures whether {@link #checkInteractionReference} will check test with its reference. */
  private static final boolean CHECK_INTERACTION_REFERENCE = true;

  /**
   * Cost is CPU fees of 2500 gas and network fees. Bytes send over the network is 1 for transaction
   * type, 21 for address 4 for the length of the payload and finally the payload itself. The
   * payload for generate preprocess batch is 3 bytes for contract invocation, batch type and batch
   * count + 4 * 54 bytes for engine information. The transaction byte total is then 245 bytes and
   * with a cost of 5 gas per byte this is 1225. Total cost is 2500 + 1225 = 3725 gas.
   */
  static final long EXPECTED_PREPROCESSING_NETWORK_AND_WASM_FEES = 3730L;

  static final long EXPECTED_PREPROCESS_INPUT_MASK_BATCH_FEES =
      EXPECTED_PREPROCESSING_NETWORK_AND_WASM_FEES
          + calculatePreprocessingFees(1, PreProcessMaterialType.BINARY_INPUT_MASK);

  static final long EXPECTED_PREPROCESS_TRIPLE_BATCH_FEES =
      EXPECTED_PREPROCESSING_NETWORK_AND_WASM_FEES
          + calculatePreprocessingFees(1, PreProcessMaterialType.BINARY_TRIPLE);

  /**
   * Cost is CPU fees of 2500 gas and network fees. Bytes send over the network is 1 for transaction
   * type, 21 for address 4 for the length of the payload and finally the payload itself. The
   * payload for notify computation done is 93 bytes, giving a network gas cost of 595. Total cost
   * is 2500 + 595 = 3095 gas.
   */
  static final long EXPECTED_NOTIFY_CONTRACT_DONE_COST = 3095;

  private static final ObjectMapper OBJECT_MAPPER =
      StateObjectMapper.createObjectMapper()
          .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
          .configure(SerializationFeature.INDENT_OUTPUT, true);

  protected static final long DEFAULT_BLOCK_PRODUCTION_TIME = 1112233;

  protected static final int PREPROCESSED_MATERIAL_TO_PREFETCH = 0;
  protected static final boolean DEFAULT_OPTIMISTIC_MODE = true;

  protected RealContractBinder binder;
  private ZkStateImmutable state;

  private final StateSerializer serializer = new StateSerializer(new TestStorage(), true);

  /** Constructor. */
  protected BinderTest() {}

  /**
   * Get current state.
   *
   * @return current state
   */
  protected final ZkStateImmutable currentState() {
    return this.state;
  }

  /** Keeps track of all interactions and resulting events for the current test. */
  private List<InteractionsAndResult> seenInteractionsForThisTest;

  @Test
  public void ensureTestWeakeningOptionsAreDisabled() {
    Assertions.assertThat(PERFORM_VERIFICATION).as("PERFORM_VERIFICATION").isTrue();
    Assertions.assertThat(CHECK_INTERACTION_REFERENCE).as("CHECK_INTERACTION_REFERENCE").isTrue();
  }

  @Test
  public void identifierCheck() {
    for (final var invocation : RealContractBinder.INVOCATIONS.entrySet()) {
      Assertions.assertThat(invocation.getKey()).isEqualTo(invocation.getValue().identifier());
    }
  }

  /** Initializes {@link #seenInteractionsForThisTest}. */
  @BeforeEach
  public void setupInteractions() {
    seenInteractionsForThisTest = new ArrayList<>();
  }

  /**
   * Compares {@link #seenInteractionsForThisTest} with the saved report.
   *
   * @param testInfo Current test info. Used to get name for reference file path.
   */
  @AfterEach
  public void checkInteractionReference(TestInfo testInfo) {
    if (!CHECK_INTERACTION_REFERENCE) {
      return;
    }
    if (seenInteractionsForThisTest.isEmpty()) {
      return;
    }

    final String report =
        interactionReferenceReport(testInfo.getDisplayName(), seenInteractionsForThisTest);
    final Path path =
        ReferenceTestUtility.getReferenceDirectoryPath(this.getClass())
            .resolve("%s.interactions.txt".formatted(testInfo.getDisplayName()));

    if (shouldRegenerateReferences()) {
      ReferenceTestUtility.writeText(path, report);
      System.out.println("Written reference file '%s'".formatted(path));
    } else {
      Assertions.assertThat(report)
          .as(path.toString())
          .isEqualToNormalizingNewlines(ReferenceTestUtility.loadText(path));
    }
  }

  /**
   * Whether to regenerate tests based on an environment variable.
   *
   * @return whether to regenerate tests
   */
  public static boolean shouldRegenerateReferences() {
    final var ref = System.getenv("REGENERATE_REFERENCES");
    return ref != null && !ref.isEmpty();
  }

  private static String interactionReferenceReport(
      String testName, List<InteractionsAndResult> seenInteractionsForThisTest) {
    final StringBuilder l = new StringBuilder();
    l.append("Interaction reference for ")
        .append(testName)
        .append("\n")
        .append("\nInteractions (")
        .append(seenInteractionsForThisTest.size())
        .append("):");

    var idx = 1;
    for (final InteractionsAndResult result : seenInteractionsForThisTest) {
      final Byte shortName = shortNameFromRpc(result.originalRpc());
      l.append("\n")
          .append("% 4d".formatted(idx++))
          .append(": ")
          .append(result.invocationHumanName());
      if (shortName != null) {
        l.append(" 0x%02X".formatted(shortName));
      }
      l.append(" (")
          .append("Fees: ")
          .append(result.registeredZkFees())
          .append(" Zk + ")
          .append(result.cpuFee())
          .append(" Cpu")
          .append(")");
      for (final BinderInteraction event : result.interactions()) {
        l.append("\n")
            .append("        -> ")
            .append(event.contract.writeAsString())
            .append(" (Cost ")
            .append(event.effectiveCost);
        if (event.costFromContract) {
          l.append(" paid by contract");
        }
        l.append(")");
      }
    }
    return l.toString();
  }

  /**
   * Create new binder.
   *
   * @param additional additional rpc sent to the contract
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult create(byte[] additional) {
    return createWithInitRpc(makeRpcForCreateInvocation(additional));
  }

  /**
   * Create new binder.
   *
   * @param initRpc init rpc of the binder
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult createWithInitRpc(byte[] initRpc) {
    final TestBinderContext context = createZkContext(1, DEFAULT_BLOCK_PRODUCTION_TIME, OWNER);
    final BinderResult<ZkStateImmutable, BinderInteraction> invokeResult =
        binder.create(context, initRpc);
    this.state = invokeResult.getState();
    ensureSerializationPossible();
    Assertions.assertThat(invokeResult.getCallResult()).isNull();
    final InteractionsAndResult interactionsAndResult =
        InteractionsAndResult.of("create", invokeResult, context, initRpc);
    seenInteractionsForThisTest.add(interactionsAndResult);
    return interactionsAndResult.verifyCpuFee(true);
  }

  @CheckReturnValue
  private byte[] makeRpcForCreateInvocation(byte[] additional) {
    return SafeDataOutputStream.serialize(
        stream -> {
          for (int i = 0; i < 4; i++) {
            ADDRESSES.get(i).write(stream);
            PUBLIC_KEYS.get(i).write(stream);
            stream.writeString("https://zk.com");
          }
          PREPROCESS_ADDRESS.write(stream);
          NODE_REGISTRY_ADDRESS.write(stream);
          stream.writeLong(DEFAULT_ZK_COMPUTATION_DEADLINE);
          stream.writeLong(DEFAULT_STAKING_FEE);
          stream.writeInt(PREPROCESSED_MATERIAL_TO_PREFETCH);
          stream.writeInt(PREPROCESSED_MATERIAL_TO_PREFETCH);
          stream.writeBoolean(DEFAULT_OPTIMISTIC_MODE);
          stream.write(WasmRealContractInvoker.INIT_HEADER);
          stream.write(additional);
        });
  }

  /**
   * Add an unconfirmed input to the binder.
   *
   * @param invocation invocation byte. On-chain or off-chain
   * @param owner owner of the variable
   * @param shareBitLength bit length of the input
   * @param blockTime block time of the input
   * @param additionalInformation additional rpc sent to the contract onZkInput
   * @param inputData data of the input. Encrypted shares if on-chain. Hashes if off-chain
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult addUnconfirmedInput(
      byte invocation,
      BlockchainAddress owner,
      int shareBitLength,
      long blockTime,
      byte[] additionalInformation,
      byte[] inputData) {
    final TestBinderContext context =
        createZkContext(blockTime, DEFAULT_BLOCK_PRODUCTION_TIME, owner);
    final byte[] rpc =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(invocation);
              stream.writeInt(1);
              stream.writeInt(shareBitLength);
              if (invocation == ZERO_KNOWLEDGE_INPUT_ON_CHAIN) {
                INPUT_KEY.write(stream);
                stream.writeDynamicBytes(inputData);
              } else {
                // size of the hash is known in advance.
                stream.write(inputData);
              }
              stream.write(additionalInformation);
            });
    BinderResult<ZkStateImmutable, BinderInteraction> invokeResult =
        binder.invoke(context, state, rpc);
    this.state = invokeResult.getState();
    ensureSerializationPossible();
    final InteractionsAndResult interactionsAndResult =
        InteractionsAndResult.of("addUnconfirmedInput", invokeResult, context, rpc);
    seenInteractionsForThisTest.add(interactionsAndResult);
    return interactionsAndResult.verifyCpuFee();
  }

  /**
   * Returns a predicate that is the negation of the supplied predicate.
   *
   * @param <T> the type of arguments to the specified predicate
   * @param pred predicate to negate
   * @return a predicate that negates the results of the supplied predicate
   */
  protected static <T> Predicate<T> not(Predicate<? super T> pred) {
    return Predicate.not(pred);
  }

  /**
   * Verify computation state.
   *
   * @param computationState computation state to verify
   */
  protected void verifyComputationState(ComputationStateImpl computationState) {
    Assertions.assertThat(computationState)
        .as("Computation state")
        .isNotNull()
        .matches(not(ComputationState::isWaitingForTriples))
        .matches(ComputationState::isOptimistic)
        .matches(ComputationState::isOptimisticVerification);

    Assertions.assertThat(computationState.getTripleIds())
        .as("Computation state triple ids")
        .containsExactly(1);
    Assertions.assertThat(computationState.getOptimisticPartialOpeningsHash())
        .as("computation state, partial openings")
        .isEqualTo(defaultPartialOpens());
    Assertions.assertThat(computationState.getOptimisticVerificationSeed())
        .as("computation state, optimistic verification seed")
        .isEqualTo(defaultVerificationSeed());
  }

  /**
   * Confirm an input on behalf of all nodes.
   *
   * @param blockTime block time
   * @param variableId id of the variable to confirm
   * @param maskedInput the resulting masked input of variable
   * @return interactions spawned for each of the invocations
   */
  protected final List<InteractionsAndResult> confirmInput(
      long blockTime, int variableId, byte[] maskedInput) {
    List<InteractionsAndResult> results = new ArrayList<>();
    for (BlockchainAddress node : ADDRESSES) {
      results.add(confirmInput(node, blockTime, variableId, maskedInput));
    }
    return results;
  }

  /**
   * Confirm an input on behalf of a node.
   *
   * @param node zk node which sends the confirmation
   * @param blockTime block time
   * @param variableId id of the variable to confirm
   * @param maskedInput the resulting masked input of variable
   * @return interactions spawned for each of the invocations
   */
  @CheckReturnValue
  protected final InteractionsAndResult confirmInput(
      BlockchainAddress node, long blockTime, int variableId, byte[] maskedInput) {
    return invoke(
        "confirmInput",
        node,
        blockTime,
        DEFAULT_BLOCK_PRODUCTION_TIME,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(OPEN_MASKED_INPUT);
              stream.writeInt(variableId);
              stream.write(maskedInput);
            }));
  }

  /**
   * Send the result of an optimistic computation to the binder.
   *
   * @param blockTime block time
   * @param engine zk node which sends the result
   * @param partialOpenings the partial openings of the computation
   * @param verificationSeed the verification seed
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult sendOptimisticOutput(
      long blockTime, int engine, Hash partialOpenings, byte[] verificationSeed) {
    final BlockchainAddress sender = ADDRESSES.get(engine);
    final long calculateFor = state.getComputationState().getCalculateFor();
    final TestBinderContext zkContext =
        createZkContext(blockTime, DEFAULT_BLOCK_PRODUCTION_TIME, sender);
    return invoke(
        "sendOptimisticOutput",
        zkContext,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(COMMIT_RESULT_VARIABLE);
              stream.writeLong(calculateFor);
              partialOpenings.write(stream);
              stream.write(verificationSeed);
              stream.write(defaultLinearElement());
            }));
  }

  /**
   * The default linear element for this contract.
   *
   * @return the default linear element
   */
  protected abstract byte[] defaultLinearElement();

  /**
   * Send a triple batch to this binder on behalf of the preprocess contract.
   *
   * @param blockTime block time
   * @param batchId batch id
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult sendTripleBatch(long blockTime, int batchId) {
    return invoke(
        "sendTripleBatch",
        PREPROCESS_ADDRESS,
        blockTime,
        DEFAULT_BLOCK_PRODUCTION_TIME,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(ADD_BATCHES);
              stream.writeEnum(PreProcessMaterialType.BINARY_TRIPLE);
              stream.writeInt(batchId);
            }));
  }

  /**
   * Send an input mask batch to this binder on behalf of the preprocess contract.
   *
   * @param blockTime block time
   * @param batchId batch id
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult sendInputMaskBatch(long blockTime, int batchId) {
    return invoke(
        "sendInputMask",
        PREPROCESS_ADDRESS,
        blockTime,
        DEFAULT_BLOCK_PRODUCTION_TIME,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(ADD_BATCHES);
              stream.writeEnum(PreProcessMaterialType.BINARY_INPUT_MASK);
              stream.writeInt(batchId);
            }));
  }

  /**
   * Send open variable shares on behalf of an engine.
   *
   * @param engineIndex engine to send for
   * @param blockTime block time
   * @param idOfPendingOpen id of the pending open
   * @param shares the shares of the open
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult sendOpenVariablesSharesForEngine(
      int engineIndex, long blockTime, int idOfPendingOpen, List<byte[]> shares) {
    BlockchainAddress sender = ADDRESSES.get(engineIndex);
    byte[] share = shares.get(engineIndex);
    return invoke(
        "sendOpenVariableShares",
        sender,
        blockTime,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(VARIABLE_OPENING_SHARES);
              stream.writeInt(idOfPendingOpen);
              stream.write(share);
            }));
  }

  /**
   * Default verification seed.
   *
   * @return default verification seed
   */
  protected static byte[] defaultVerificationSeed() {
    return Hash.create(stream -> stream.writeString("VerificationSeed")).getBytes();
  }

  /**
   * Default partial opens.
   *
   * @return default partial opens
   */
  protected static Hash defaultPartialOpens() {
    return Hash.create(s -> s.writeInt(117));
  }

  /**
   * Create a commit result invocation rpc.
   *
   * @param calculateFor the calculation specifier
   * @param hash the hash
   * @return rpc of the commit result
   */
  protected static byte[] createCommitResultInvocation(long calculateFor, Hash hash) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(COMMIT_RESULT_VARIABLE);
          stream.writeLong(calculateFor);
          hash.write(stream);
        });
  }

  /**
   * Read the state of a json file.
   *
   * @param file file name of the json file
   */
  protected final void readState(final String file) {
    final Path stateJsonPath = pathForStateJson(file);
    this.state =
        ExceptionConverter.call(
            () -> readState(stateJsonPath),
            "Unable to read state for '%s'".formatted(stateJsonPath));
  }

  private ZkStateImmutable readState(final Path stateJsonPath) {
    final String serialized = readFileAsString(stateJsonPath);
    ZkStateImmutable state = deserialize(serialized);
    return ensureStateSerialization(state, serializer);
  }

  /**
   * Deserialize json state into {@link ZkStateImmutable}.
   *
   * @param serialized json state
   * @return binder state
   */
  protected static ZkStateImmutable deserialize(String serialized) {
    return ExceptionConverter.call(
        () -> OBJECT_MAPPER.readValue(serialized, ZkStateImmutable.class),
        "Unable to read contract");
  }

  /**
   * Verify the current binder state is equal to the given json state.
   *
   * @param expectedJsonName file name of the expected json state
   */
  protected final void verifySerialization(final String expectedJsonName) {
    final Path expectedJsonPath = pathForStateJson(expectedJsonName);

    ensureSerializationPossible();
    final String serializedState = serialize(state);
    final String expectedSerialization = getOrUpdateExpected(expectedJsonPath, serializedState);

    Assertions.assertThat(serializedState)
        .as(expectedJsonName + "\n" + differenceReport(serializedState, expectedSerialization))
        .isEqualTo(expectedSerialization);
  }

  /**
   * Produces a report of the first differing line between the given strings.
   *
   * @param str1 First string to compare.
   * @param str2 Second string to compare.
   * @return Produced report
   */
  public static String differenceReport(String str1, String str2) {
    final List<String> lines1 = str1.lines().toList();
    final List<String> lines2 = str2.lines().toList();
    final int differIdx = firstDifferingIndex(lines1, lines2);

    return "First difference at line %d:\n    > %s\nvs. > %s"
        .formatted(differIdx + 1, lineOrEof(lines1, differIdx), lineOrEof(lines2, differIdx));
  }

  private static String lineOrEof(List<String> lines, int idx) {
    return idx < lines.size() ? lines.get(idx) : "EOF";
  }

  private static <T> int firstDifferingIndex(List<T> str1, List<T> str2) {
    final int len = Integer.min(str1.size(), str2.size());
    for (int idx = 0; idx < len; idx++) {
      if (!str1.get(idx).equals(str2.get(idx))) {
        return idx;
      }
    }
    return len;
  }

  /** Get the path where json states are saved. */
  protected abstract Path pathForStateJson(final String name);

  /**
   * Produces source path for the given resource relative to the given class.
   *
   * @param contextClass Class to determine resource relative to.
   * @param filename Resource filename.
   * @return Source path for resource. Never null.
   */
  public static Path pathForResource(final Class<?> contextClass, final String filename) {
    final String[] split = contextClass.getPackageName().split("\\.", -1);
    final Path dir = Paths.get("src/test/resources", split);
    return dir.resolve(filename);
  }

  private static String serialize(ZkStateImmutable state) {
    return ExceptionConverter.call(
        () -> OBJECT_MAPPER.writeValueAsString(state), "Unable to save contract to json");
  }

  private void ensureSerializationPossible() {
    state = ensureStateSerialization(state, serializer);
  }

  private static ZkStateImmutable ensureStateSerialization(
      final ZkStateImmutable state, final StateSerializer serializer) {
    Hash firstHash = serializer.write(state).hash();
    ZkStateImmutable read = serializer.read(firstHash, ZkStateImmutable.class);
    Hash secondHash = serializer.write(read).hash();
    Assertions.assertThat(secondHash).isEqualTo(firstHash);
    return read;
  }

  /**
   * Send an optimistic verification for a calculation on behalf of a zk node.
   *
   * @param calculateFor the specifier of the calculation
   * @param engine the zk node to send for
   * @param verificationElement the verification element sent
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult sendOptimisticVerification(
      long calculateFor, BlockchainAddress engine, byte[] verificationElement) {
    return invoke(
        "sendOptimisticVerification",
        engine,
        10L,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(COMMIT_RESULT_VARIABLE);
              stream.writeLong(calculateFor);
              stream.write(verificationElement);
            }));
  }

  /**
   * Reads file pointed to by given path as bytes.
   *
   * @param path Path of file.
   * @return File contents as byte array. Never null.
   */
  public static byte[] readFileBytes(final Path path) {
    return WithResource.apply(
        () -> new FileInputStream(path.toFile()),
        InputStream::readAllBytes,
        "Unable to read file path: " + path);
  }

  /**
   * Reads file pointed to by given path as string.
   *
   * @param path Path of file.
   * @return File contents as string. Never null.
   */
  public static String readFileAsString(final Path path) {
    return new String(readFileBytes(path), StandardCharsets.UTF_8);
  }

  private String getOrUpdateExpected(final Path expectedJsonPath, final String serialization) {
    if (shouldRegenerateReferences()) {
      WithResource.accept(
          () -> {
            Files.createDirectories(expectedJsonPath.getParent());
            return new PrintStream(expectedJsonPath.toFile(), StandardCharsets.UTF_8);
          },
          stream -> stream.print(serialization),
          "Unable to write file path: " + expectedJsonPath);
    }

    try {
      final String expectedJsonStr = readFileAsString(expectedJsonPath);
      // Convert to json to ensure it is pretty printed as the object mapper would have done
      final JsonNode expectedJsonTree = OBJECT_MAPPER.readTree(expectedJsonStr);
      return OBJECT_MAPPER.writeValueAsString(expectedJsonTree);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void verifyForwardEvent(final BinderInteraction selfInvocation) {
    Assertions.assertThat(selfInvocation.contract).isEqualTo(CONTRACT_ONE);
    Assertions.assertThat(selfInvocation.costFromContract).isTrue();
    Assertions.assertThat(selfInvocation.effectiveCost)
        .isEqualTo(TransactionCostCalculator.costOfSelfInvoke(selfInvocation.rpc));
  }

  /**
   * Make a self invocation.
   *
   * @param selfInvocation the invocation to self invoke
   * @param blockTime the block time of the self invoke
   * @return interactions spawned
   */
  protected InteractionsAndResult invokeSelf(BinderInteraction selfInvocation, long blockTime) {
    verifyForwardEvent(selfInvocation);

    return invoke(
        "invokeSelf",
        selfInvocation.contract,
        blockTime,
        DEFAULT_BLOCK_PRODUCTION_TIME,
        selfInvocation.rpc);
  }

  /**
   * Make an invocation to the binder.
   *
   * @param invocationHumanName human-readable name of the invocation
   * @param sender sender of the invocation
   * @param blockTime block time
   * @param input the rpc of the invocation
   * @return interactions spawned
   */
  @CheckReturnValue
  protected InteractionsAndResult invoke(
      String invocationHumanName, BlockchainAddress sender, long blockTime, byte[] input) {
    return invoke(invocationHumanName, sender, blockTime, DEFAULT_BLOCK_PRODUCTION_TIME, input);
  }

  /**
   * Make an invocation to the binder.
   *
   * @param invocationHumanName human-readable name of the invocation
   * @param sender sender of the invocation
   * @param blockTime block time
   * @param productionTime production time
   * @param input the rpc of the invocation
   * @return interactions spawned
   */
  @CheckReturnValue
  protected InteractionsAndResult invoke(
      String invocationHumanName,
      BlockchainAddress sender,
      long blockTime,
      long productionTime,
      byte[] input) {
    TestBinderContext zkContext = createZkContext(blockTime, productionTime, sender);
    return invoke(invocationHumanName, zkContext, input);
  }

  @CheckReturnValue
  private InteractionsAndResult invoke(
      String invocationHumanName, TestBinderContext zkContext, byte[] rpc) {
    BinderResult<ZkStateImmutable, BinderInteraction> invokeResult =
        binder.invoke(zkContext, state, rpc);

    state = invokeResult.getState();

    ensureSerializationPossible();

    final InteractionsAndResult interactionsAndResult =
        InteractionsAndResult.of(invocationHumanName, invokeResult, zkContext, rpc);
    seenInteractionsForThisTest.add(interactionsAndResult);
    return interactionsAndResult.verifyCpuFee();
  }

  /**
   * Send an invocation for extending zk computation deadline.
   *
   * @param blockTime block time
   * @param blockProductionTime block production time
   * @param gas gas
   * @return interactions spawned
   */
  protected InteractionsAndResult invokeExtendZkComputationDeadline(
      long blockTime, long blockProductionTime, long gas) {
    TestBinderContext zkBinderContext =
        createZkContext(blockTime, blockProductionTime, NODE_REGISTRY_ADDRESS, gas);
    return invoke(
        "invokeExtendZkComputationDeadline",
        zkBinderContext,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(EXTEND_ZK_COMPUTATION_DEADLINE);
              // ms / gas (3024 ms/gas is default for staking fee of 800,000 gas)
              stream.writeLong(3024L);
              stream.writeLong(1L);
              // Minimum extension amount
              stream.writeLong(Duration.ofHours(24).toMillis());
              // Extension limit
              stream.writeLong(Duration.ofDays(182).toMillis());
            }));
  }

  /**
   * Invoke callback of the binder.
   *
   * @param sender sender of the callback
   * @param blockTime block time
   * @param rpc tpc
   * @param executionResult execution results of the callback
   * @return interactions spawned
   */
  @CheckReturnValue
  protected final InteractionsAndResult callback(
      BlockchainAddress sender,
      long blockTime,
      byte[] rpc,
      FixedList<CallbackContext.ExecutionResult> executionResult) {
    final TestBinderContext zkContext =
        createZkContext(blockTime, DEFAULT_BLOCK_PRODUCTION_TIME, sender);
    BinderResult<ZkStateImmutable, BinderInteraction> invokeResult =
        binder.callback(zkContext, state, CallbackContext.create(executionResult), rpc);
    this.state = invokeResult.getState();
    Assertions.assertThat(zkContext.registerCpuFee).isGreaterThan(0);
    ensureSerializationPossible();
    final InteractionsAndResult interactionsAndResult =
        InteractionsAndResult.of("callback", invokeResult, zkContext, rpc);
    seenInteractionsForThisTest.add(interactionsAndResult);
    return interactionsAndResult;
  }

  /**
   * Create a zk context.
   *
   * @param blockTime block time
   * @param blockProductionTime block production time
   * @param sender sender
   * @return the binder context
   */
  @CheckReturnValue
  protected TestBinderContext createZkContext(
      long blockTime, long blockProductionTime, BlockchainAddress sender) {
    return TestBinderContext.withDefaultGas(
        blockTime, blockProductionTime, sender, TRANSACTION_HASH, TRANSACTION_HASH);
  }

  /**
   * Create a zk context.
   *
   * @param blockTime block time
   * @param blockProductionTime block production time
   * @param sender sender
   * @param availableGas available gas
   * @return the binder context
   */
  @CheckReturnValue
  protected TestBinderContext createZkContext(
      long blockTime, long blockProductionTime, BlockchainAddress sender, long availableGas) {
    return TestBinderContext.withGas(blockTime, blockProductionTime, sender, availableGas);
  }

  /**
   * Create secret input from bit-lengths.
   *
   * @param length bit length
   * @return secret input of zero
   */
  protected static byte[] createInput(final Integer length) {
    final int nodes = 4;
    return new byte[nodes * length];
  }

  @CheckReturnValue
  static Byte shortNameFromRpc(byte[] rpc) {
    if (rpc.length == 0) {
      return null;
    }
    return (byte) SafeDataInputStream.createFromBytes(rpc).readUnsignedByte();
  }

  @CheckReturnValue
  static boolean invokesWasmContract(Byte shortName) {
    if (shortName == null) {
      return false;
    }
    List<Byte> invocationsInvokingWasmContract =
        List.of(
            ZERO_KNOWLEDGE_INPUT_OFF_CHAIN,
            ZERO_KNOWLEDGE_INPUT_ON_CHAIN,
            OPEN_INVOCATION,
            ON_COMPUTE_COMPLETE,
            ON_VARIABLES_OPENED,
            ON_ATTESTATION_COMPLETE,
            ON_VARIABLE_INPUTTED,
            ON_VARIABLE_REJECTED);
    return invocationsInvokingWasmContract.contains(shortName);
  }

  /**
   * Result record to return after invoking the binder, containing any interactions spawned by the
   * call, the call result, and any ZK fees registered to the call.
   */
  @SuppressWarnings("ArrayRecordComponent")
  protected record InteractionsAndResult(
      List<BinderInteraction> interactions,
      CallResult callResult,
      long registeredZkFees,
      String invocationHumanName,
      byte[] originalRpc,
      long cpuFee) {

    @CheckReturnValue
    static InteractionsAndResult of(
        String invocationHumanName,
        BinderResult<ZkStateImmutable, BinderInteraction> binderResult,
        TestBinderContext zkContext,
        byte[] originalRpc) {
      return new InteractionsAndResult(
          binderResult.getInvocations(),
          binderResult.getCallResult(),
          zkContext.zkFees,
          invocationHumanName,
          originalRpc,
          zkContext.registerCpuFee);
    }

    @CheckReturnValue
    public BinderInteraction singleInteraction() {
      Assertions.assertThat(interactions).as("Interactions").hasSize(1);
      return interactions.get(0);
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyNoInteractions() {
      if (PERFORM_VERIFICATION) {
        Assertions.assertThat(interactions)
            .as("Interactions to: " + interactions.stream().map(x -> x.contract).toList())
            .isEmpty();
      }
      return this;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyNoInteractionsOrFees() {
      return this.verifyNoInteractions().verifyZkFees(0L);
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyRequestTripleBatches(int numBatches) {
      return this.verifyRequestPreProcessMaterial(
          this.singleInteraction(), PreProcessMaterialType.BINARY_TRIPLE, numBatches);
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyRequestTripleBatches(
        int numBatches, int binderInteractionIndex) {
      return this.verifyRequestPreProcessMaterial(
          this.interactions.get(binderInteractionIndex),
          PreProcessMaterialType.BINARY_TRIPLE,
          numBatches);
    }

    @CanIgnoreReturnValue
    public byte[] verifyEventGroup(
        BinderInteraction binderInteraction, BlockchainAddress toContract, long expectedCost) {
      if (PERFORM_VERIFICATION) {
        Assertions.assertThat(binderInteraction.originalSender)
            .as("Is from original sender? (This feature have been permanently disabled.)")
            .isFalse();
        Assertions.assertThat(binderInteraction.effectiveCost)
            .as("Expected cost")
            .isEqualTo(expectedCost);
        Assertions.assertThat(binderInteraction.contract)
            .as("Contract address")
            .isEqualTo(toContract);
      }
      return binderInteraction.rpc;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyRequestInputMaskBatches(int numBatches) {
      return this.verifyRequestPreProcessMaterial(
          this.singleInteraction(), PreProcessMaterialType.BINARY_INPUT_MASK, numBatches);
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyRequestInputMaskBatches(
        int numBatches, int binderInteractionIndex) {
      return this.verifyRequestPreProcessMaterial(
          this.interactions.get(binderInteractionIndex),
          PreProcessMaterialType.BINARY_INPUT_MASK,
          numBatches);
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyRequestPreProcessMaterial(
        BinderInteraction binderInteraction, PreProcessMaterialType batchType, int numBatches) {
      Assertions.assertThat(numBatches).isBetween(1, (int) Byte.MAX_VALUE);
      if (PERFORM_VERIFICATION) {
        final var expectedFees =
            EXPECTED_PREPROCESSING_NETWORK_AND_WASM_FEES
                + calculatePreprocessingFees(numBatches, batchType);
        byte[] rpc = verifyEventGroup(binderInteraction, PREPROCESS_ADDRESS, expectedFees);
        Assertions.assertThat(rpc)
            .as("Pre-process material request RPC")
            .isEqualTo(
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeByte(PREPROCESS_GENERATE_BATCH_V4);
                      stream.writeEnum(batchType);
                      stream.writeByte(numBatches);
                      for (int i = 0; i < 4; i++) {
                        ADDRESSES.get(i).write(stream);
                        PUBLIC_KEYS.get(i).write(stream);
                      }
                      stream.writeByte(ADD_BATCHES);
                    }));
      }
      return this;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyNotifyCalculationDoneEvent(
        List<EnginesStatus.ZkNodeScoreStatus> statuses) {
      if (PERFORM_VERIFICATION) {
        byte[] rpc =
            verifyEventGroup(
                this.singleInteraction(),
                NODE_REGISTRY_ADDRESS,
                EXPECTED_NOTIFY_CONTRACT_DONE_COST);
        Assertions.assertThat(rpc)
            .isEqualTo(
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeByte(0x12);
                      stream.writeInt(4);
                      for (int i = 0; i < 4; i++) {
                        ADDRESSES.get(i).write(stream);
                        stream.writeEnum(statuses.get(i));
                      }
                    }));
      }
      return this;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyZkFees(final long expectedServiceFees) {
      if (PERFORM_VERIFICATION) {
        Assertions.assertThat(this.registeredZkFees)
            .as("Registered ZK Service Fees")
            .isEqualTo(expectedServiceFees);
      }
      return this;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyInteract(BlockchainAddress targetAddr) {
      if (PERFORM_VERIFICATION) {
        Assertions.assertThat(this.singleInteraction().contract)
            .as("Interaction target")
            .isEqualTo(targetAddr);
      }
      return this;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyInteractWithSelf() {
      return this.verifyInteract(CONTRACT_ONE);
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyZkGovernanceGasCosts() {
      if (PERFORM_VERIFICATION) {
        for (BinderInteraction interaction : interactions) {
          boolean isSelfInvocation = interaction.contract.equals(CONTRACT_ONE);
          // Currently not testing expected cost for self invocations as they vary in size
          if (!isSelfInvocation) {
            int interactionLength = interaction.rpc.length;
            long expectedCost =
                interactionLength == 93
                    ? EXPECTED_NOTIFY_CONTRACT_DONE_COST
                    : EXPECTED_PREPROCESS_TRIPLE_BATCH_FEES;

            Assertions.assertThat(interaction.effectiveCost).isEqualTo(expectedCost);
          }
        }
      }
      return this;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyShortName(Byte expectedShortName) {
      final Byte shortName = shortNameFromRpc(originalRpc);
      Assertions.assertThat(shortName).as("Interaction shortname").isEqualTo(expectedShortName);
      return this;
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyCpuFee() {
      final Byte shortName = shortNameFromRpc(originalRpc);
      return this.verifyCpuFee(invokesWasmContract(shortName));
    }

    @CanIgnoreReturnValue
    public InteractionsAndResult verifyCpuFee(final boolean expectCpuFee) {
      if (expectCpuFee) {
        Assertions.assertThat(cpuFee).as("CPU Gas Fee").isGreaterThan(0L);
      } else {
        Assertions.assertThat(cpuFee).as("CPU Gas Fee").isEqualTo(0L);
      }
      return this;
    }
  }

  private static final class TestStorage implements StateStorage {

    private final Map<Hash, byte[]> serialized = new HashMap<>();

    @Override
    public synchronized boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
      if (serialized.containsKey(hash)) {
        return false;
      }
      serialized.put(
          hash,
          SafeDataOutputStream.serialize(t -> ExceptionConverter.run(() -> writer.accept(t), "")));
      return true;
    }

    @Override
    public synchronized <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
      if (serialized.containsKey(hash)) {
        SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(serialized.get(hash));
        return ExceptionConverter.call(() -> reader.apply(bytes), "");
      } else {
        return null;
      }
    }
  }
}
