package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ZkStateImmutableBuilderTest {

  public static final Hash TRANSACTION = Hash.create(stream -> stream.writeInt(12345));

  @Test
  public void assertReturnsBuilder() {
    ZkStateImmutable state = new ZkStateImmutable();
    ZkStateImmutableBuilder builder = new ZkStateImmutableBuilder(state);

    ZkClosedImpl variable = new ZkClosedImpl(null, 1, false, null, List.of(1), TRANSACTION, 0);
    Assertions.assertThat(builder.setVariable(variable)).isSameAs(builder);

    Assertions.assertThat(builder.removeVariable(1)).isSameAs(builder);

    PendingInputImpl input = new PendingInputImpl(variable, null, null, null, null);
    Assertions.assertThat(builder.setPendingInput(input)).isSameAs(builder);

    Assertions.assertThat(builder.removePendingInput(1)).isSameAs(builder);

    Assertions.assertThat(builder.setOpenState(null)).isSameAs(builder);

    Assertions.assertThat(builder.setOpenState(null)).isSameAs(builder);

    Assertions.assertThat(
            builder.setComputationState(
                new ComputationStateImpl(
                    false,
                    0,
                    0,
                    1,
                    TestStateConversion.openStateFromLong(1L),
                    AvlTree.create(),
                    new ComputationOutput(FixedList.create()),
                    null)))
        .isSameAs(builder);

    Assertions.assertThat(builder.updateComputationState(prev -> prev)).isSameAs(builder);

    Assertions.assertThat(builder.setPendingOnChainOpenFromIds(FixedList.create(List.of())))
        .isSameAs(builder);

    Assertions.assertThat(
            builder.setPendingOnChainOpen(
                new PendingOnChainOpenImpl(1, FixedList.create(List.of(1)), AvlTree.create())))
        .isSameAs(builder);

    Assertions.assertThat(builder.removePendingOnChainOpen(1)).isSameAs(builder);

    Assertions.assertThat(
            builder.addPreProcessMaterialBatch(PreProcessMaterialType.BINARY_TRIPLE, 1))
        .isSameAs(builder);
  }

  @Test
  public void addInputMaskBatch() {
    final ZkStateImmutable state = new ZkStateImmutable();
    final ZkStateImmutableBuilder builder = new ZkStateImmutableBuilder(state);
    builder.addPreProcessMaterialBatch(PreProcessMaterialType.BINARY_INPUT_MASK, 99);

    Assertions.assertThat(builder.build().getInputMaskState().batchIds()).containsExactly(99);
    Assertions.assertThat(builder.build().getInputMaskState().numRequestedBatches()).isEqualTo(1);
    Assertions.assertThat(builder.build().getTriplesState().batchIds()).isEmpty();
    Assertions.assertThat(builder.build().getTriplesState().numRequestedBatches()).isEqualTo(0);
  }

  @Test
  public void addTripleBatch() {
    final ZkStateImmutable state = new ZkStateImmutable();
    final ZkStateImmutableBuilder builder = new ZkStateImmutableBuilder(state);
    builder.addPreProcessMaterialBatch(PreProcessMaterialType.BINARY_TRIPLE, 99);

    Assertions.assertThat(builder.build().getTriplesState().batchIds()).containsExactly(99);
    Assertions.assertThat(builder.build().getTriplesState().numRequestedBatches()).isEqualTo(1);
    Assertions.assertThat(builder.build().getInputMaskState().batchIds()).isEmpty();
    Assertions.assertThat(builder.build().getInputMaskState().numRequestedBatches()).isEqualTo(0);
  }

  @Test
  public void replaceFinishedComputation() {
    ZkStateImmutable state = new ZkStateImmutable();
    ZkStateImmutableBuilder builder = new ZkStateImmutableBuilder(state);
    FinishedComputationImpl computation =
        new FinishedComputationImpl(
                10L, (byte) 0, false, Hash.create(FunctionUtility.noOpConsumer()))
            .withInput(0)
            .withInput(2);
    FinishedComputationImpl updated = computation.withInput(1);
    ZkStateImmutable after =
        builder
            .setFinishedComputations(FixedList.create(List.of(computation)))
            .replaceComputation(computation, updated)
            .build();
    Assertions.assertThat(after.getFinishedComputations()).hasSize(1);
    Assertions.assertThat(after.getFinishedComputations().get(0)).isEqualTo(updated);

    ZkStateImmutable withoutFinished =
        after.builder().replaceComputation(updated, updated.withInput(3)).build();
    Assertions.assertThat(withoutFinished.getFinishedComputations()).isEmpty();
    Assertions.assertThat(withoutFinished.getComputationDeadline()).isEqualTo(0L);
    Assertions.assertThat(withoutFinished.getNodeRegistryContract()).isNull();
  }
}
