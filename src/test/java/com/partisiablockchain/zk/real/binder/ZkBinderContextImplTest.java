package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Tests {@link ZkBinderContextImpl}. */
public final class ZkBinderContextImplTest {
  private final ZkBinderContextImpl context;
  private static final BlockchainAddress ADDRESS = BinderTest.CONTRACT_ONE;

  /** Test constructor. */
  public ZkBinderContextImplTest() {
    TestBinderContext binderContext =
        TestBinderContext.withGas(12, 123456789, BinderTest.ACCOUNT_TWO, 1_000L);
    context = new ZkBinderContextImpl(binderContext);
  }

  @Test
  public void propagate() {
    assertThat(context.getBlockTime()).isEqualTo(12);
    assertThat(context.getBlockProductionTime()).isEqualTo(123456789);
    assertThat(context.getFrom()).isEqualTo(BinderTest.ACCOUNT_TWO);
    assertThat(context.getCurrentTransactionHash()).isEqualTo(BinderTest.TRANSACTION_HASH);
    assertThat(context.getOriginalTransactionHash())
        .isEqualTo(BinderTest.ORIGINAL_TRANSACTION_HASH);
    assertThat(context.availableGas()).isEqualTo(1_000L);
  }

  @Test
  public void feePayment() {
    assertThat(context.availableGas()).isEqualTo(1_000L);
    context.payFromContract(1, List.of(ADDRESS));
    assertThat(context.availableGas()).isEqualTo(1_000L);
    context.payFromCaller(1, List.of(ADDRESS));
    assertThat(context.availableGas()).isEqualTo(999L);
    context.registerCpuFee(1);
    assertThat(context.availableGas()).isEqualTo(998L);

    assertThatThrownBy(() -> context.payServiceFees(1, ADDRESS)).hasMessageStartingWith("Unused");
  }

  @Test
  void callerFeesCannotExceedAvailableGas() {
    assertThatThrownBy(() -> context.payFromCaller(1_001L, List.of(ADDRESS)))
        .hasMessage(
            "Registered service fees cannot exceed available gas (%d > %d)"
                .formatted(1_001L, 1_000L));
  }

  @Test
  void cpuFeeCannotExceedAvailableGas() {
    assertThatThrownBy(() -> context.registerCpuFee(1_001L))
        .hasMessage(
            "Registered CPU fees cannot exceed available gas (%d > %d)".formatted(1_001L, 1_000L));
  }
}
