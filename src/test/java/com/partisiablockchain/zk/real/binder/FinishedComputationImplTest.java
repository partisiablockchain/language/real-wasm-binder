package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Finished computation contains the information used on contract after the computation. */
@Immutable
public final class FinishedComputationImplTest {

  @Test
  public void isOptimisticTest() {
    final var comp1 = new FinishedComputationImpl(0, (byte) 0, false, null);
    final var comp2 = new FinishedComputationImpl(0, (byte) 0, true, null);
    Assertions.assertThat(comp1.isOptimistic()).isFalse();
    Assertions.assertThat(comp2.isOptimistic()).isTrue();
  }
}
