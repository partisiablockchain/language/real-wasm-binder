package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.crypto.Hash;

/** Test. */
public final class TestBinderContext implements ZkBinderContext {

  public static final long DEFAULT_AVAILABLE_GAS = 2_000_000L;
  private final long blockTime;
  private final long blockProductionTime;
  private final BlockchainAddress sender;
  private final Hash transactionHash;
  private final Hash originalTransactionHash;

  private final long availableGas;
  long zkFees;
  long registerCpuFee;

  private TestBinderContext(
      long blockTime,
      long blockProductionTime,
      BlockchainAddress sender,
      Hash transactionHash,
      Hash originalTransactionHash,
      long availableGas) {
    this.blockTime = blockTime;
    this.blockProductionTime = blockProductionTime;
    this.sender = sender;
    this.transactionHash = transactionHash;
    this.originalTransactionHash = originalTransactionHash;
    this.availableGas = availableGas;
  }

  /**
   * Construct {@link TestBinderContext} with the default amount of gas.
   *
   * @param blockTime current block
   * @param blockProductionTime current production time
   * @param sender of the transaction
   * @param transactionHash transaction identifier
   * @param originalTransactionHash identifier of original transaction
   * @return new test context
   */
  public static TestBinderContext withDefaultGas(
      long blockTime,
      long blockProductionTime,
      BlockchainAddress sender,
      Hash transactionHash,
      Hash originalTransactionHash) {
    return new TestBinderContext(
        blockTime,
        blockProductionTime,
        sender,
        transactionHash,
        originalTransactionHash,
        DEFAULT_AVAILABLE_GAS);
  }

  /**
   * Construct {@link TestBinderContext} with the default amount of gas, and default hashes.
   *
   * @param blockTime current block
   * @param blockProductionTime current production time
   * @param sender of the transaction
   * @return new test context
   */
  public static TestBinderContext withDefaultGas(
      long blockTime, long blockProductionTime, BlockchainAddress sender) {
    return new TestBinderContext(
        blockTime,
        blockProductionTime,
        sender,
        BinderTest.TRANSACTION_HASH,
        BinderTest.ORIGINAL_TRANSACTION_HASH,
        DEFAULT_AVAILABLE_GAS);
  }

  /**
   * Construct {@link TestBinderContext} with specified gas.
   *
   * @param sender of the transaction
   * @param blockTime current block
   * @param blockProductionTime current production time
   * @param gas provided to transaction
   * @return new test context
   */
  public static TestBinderContext withGas(
      long blockTime, long blockProductionTime, BlockchainAddress sender, long gas) {
    return new TestBinderContext(
        blockTime,
        blockProductionTime,
        sender,
        BinderTest.TRANSACTION_HASH,
        BinderTest.ORIGINAL_TRANSACTION_HASH,
        gas);
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return BinderTest.CONTRACT_ONE;
  }

  @Override
  public long getBlockTime() {
    return blockTime;
  }

  @Override
  public long getBlockProductionTime() {
    return blockProductionTime;
  }

  @Override
  public BlockchainAddress getFrom() {
    return sender;
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return transactionHash;
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return originalTransactionHash;
  }

  @Override
  public void payServiceFees(long gas, BlockchainAddress target) {
    zkFees += gas;
  }

  @Override
  public long availableGas() {
    return availableGas;
  }

  @Override
  public void registerCpuFee(long l) {
    this.registerCpuFee = l;
  }

  /**
   * Get zk fees.
   *
   * @return zk feeds
   */
  public long zkFees() {
    return zkFees;
  }
}
