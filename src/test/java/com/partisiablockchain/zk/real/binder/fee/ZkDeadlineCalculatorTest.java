package com.partisiablockchain.zk.real.binder.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import com.partisiablockchain.zk.real.binder.BinderTest;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/** Test invocation to pay staking fees to contract. */
public final class ZkDeadlineCalculatorTest {

  private static final long DEFAULT_MS_PER_GAS_NUMERATOR = 3024;
  private static final long DEFAULT_MS_PER_GAS_DENOMINATOR = 1;

  private static final long MIN_EXTENSION = Duration.ofHours(24).toMillis();
  private static final Duration MAX_EXTENSION = Duration.ofDays(182);

  @DisplayName("Valid scenarios for paying staking fees on a ZK contract")
  @ParameterizedTest(name = "{index}: {0}")
  @MethodSource("testData")
  void payValidStakingFees(TestCase testCase) {

    ZkDeadlineCalculator calculator =
        ZkDeadlineCalculator.create(
            testCase.msPerGasNumerator(),
            testCase.msPerGasDenominator(),
            testCase.currentDeadline(),
            MIN_EXTENSION,
            MAX_EXTENSION);

    ZkDeadlineCalculator.Result result =
        calculator.calculateFor(testCase.blockProductionTime(), testCase.gas());

    Assertions.assertThat(result.deadlineExtension())
        .isEqualTo(testCase.expectedDeadlineExtension());
    Assertions.assertThat(result.gasToPay()).isEqualTo(testCase.expectedGasToPay());
  }

  @Test
  public void cannotPayForOneMsLessThan24Hours() {
    ZkDeadlineCalculator calculator =
        ZkDeadlineCalculator.create(
            7,
            1,
            Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE),
            MIN_EXTENSION,
            MAX_EXTENSION);
    Assertions.assertThatCode(
            () -> calculator.calculateFor(BinderTest.VERSION_3_LAUNCH_DATE, 12_342_857L))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Not possible to extend deadline with minimum required amount");
  }

  @Test
  public void cannotPayIfThereIsLessThan24HoursToCurrentDeadline() {
    Instant now =
        Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE)
            .minus(24, ChronoUnit.HOURS)
            .plusMillis(1);
    ZkDeadlineCalculator calculator =
        ZkDeadlineCalculator.create(
            7,
            1,
            Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE),
            MIN_EXTENSION,
            MAX_EXTENSION);
    Assertions.assertThatCode(() -> calculator.calculateFor(now, 12_342_857L))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Not possible to extend deadline with minimum required amount");
  }

  @Test
  public void cannotPayIfCurrentDeadlineIsMoreThanMaxLimitIntoTheFuture() {
    Instant now =
        Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE)
            .minus(24, ChronoUnit.HOURS);
    ZkDeadlineCalculator calculator =
        ZkDeadlineCalculator.create(
            3024, 1, now.plus(182, ChronoUnit.DAYS), MIN_EXTENSION, MAX_EXTENSION);
    Assertions.assertThatCode(() -> calculator.calculateFor(now, 12_342_857L))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Not possible to extend deadline with minimum required amount");
  }

  @Test
  public void stakingFeeMustBeNonZero() {
    Assertions.assertThatCode(
            () ->
                ZkDeadlineCalculator.create(
                    0, 0, BinderTest.VERSION_3_LAUNCH_DATE, MIN_EXTENSION, MAX_EXTENSION))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Denominator cannot be zero");
  }

  @Test
  public void mustPayPositiveAmountOfGas() {
    Instant now =
        Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE)
            .minus(24, ChronoUnit.HOURS);
    ZkDeadlineCalculator calculator =
        ZkDeadlineCalculator.create(
            3024, 1, now.plus(182, ChronoUnit.DAYS), MIN_EXTENSION, MAX_EXTENSION);
    Assertions.assertThatCode(() -> calculator.calculateFor(now, 0L))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Not possible to extend deadline with minimum required amount");
  }

  private static long ceilingDivision(long dividend, long divisor) {
    if (dividend == 0) {
      return 0;
    } else {
      long x = dividend + divisor - 1;
      return x / divisor;
    }
  }

  static List<Arguments> testData() {
    return List.of(
        arguments(
            named(
                "Paying the initial staking fee extends currentDeadline with 28 days",
                TestCase.defaultTestCase())),
        arguments(
            named(
                "Paying 1/4th of initial staking fee extends currentDeadline with 1 week",
                TestCase.defaultTestCase()
                    // 800.000 / 28 is 28.571,4286 so round up to ensure we are able to pay for a
                    // full day. Expect new deadline to be more than one day, but not more than one
                    // day plus one gas' with of ms
                    .withGas(ceilingDivision(BinderTest.DEFAULT_STAKING_FEE, 4))
                    .expectGasToPay(ceilingDivision(BinderTest.DEFAULT_STAKING_FEE, 4))
                    .expectDeadlineExtendedBy(Duration.ofDays(7).toMillis()))),
        arguments(
            named(
                "Paying slightly less than 1/4th of initial staking fee extends currentDeadline"
                    + " with at most 1 week",
                TestCase.defaultTestCase()
                    .withGas(BinderTest.DEFAULT_STAKING_FEE / 4)
                    .expectGasToPay(BinderTest.DEFAULT_STAKING_FEE / 4)
                    .expectDeadlineExtendedBy(Duration.ofDays(7).toMillis()))),
        arguments(
            named(
                "Paying for exactly 182 days",
                TestCase.defaultTestCase()
                    .withGas(BinderTest.DEFAULT_STAKING_FEE * 182 / 28)
                    .expectGasToPay(BinderTest.DEFAULT_STAKING_FEE * 182 / 28)
                    .withBlockProductionTime(
                        Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE))
                    .expectDeadlineExtendedBy(Duration.ofDays(182).toMillis()))),
        arguments(
            named(
                "Paying for 182 days + 1 gas",
                TestCase.defaultTestCase()
                    .withGas(BinderTest.DEFAULT_STAKING_FEE * 182 / 28 + 1)
                    .expectGasToPay(BinderTest.DEFAULT_STAKING_FEE * 182 / 28)
                    .withBlockProductionTime(
                        Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE))
                    .expectDeadlineExtendedBy(Duration.ofDays(182).toMillis()))),
        arguments(
            named(
                "Paying for 182 days - 1 gas",
                TestCase.defaultTestCase()
                    .withGas(BinderTest.DEFAULT_STAKING_FEE * 182 / 28 - 1)
                    .expectGasToPay(BinderTest.DEFAULT_STAKING_FEE * 182 / 28 - 1)
                    .withBlockProductionTime(
                        Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE))
                    .expectDeadlineExtendedBy(
                        Duration.ofDays(182).minus(Duration.ofMillis(3024)).toMillis()))),
        arguments(
            named(
                "Paying minimum amount of gas extends deadline exactly 24 hours",
                TestCase.defaultTestCase()
                    .withMsPerGas(86_400_000L, 28_751L) // corresponds to 805,028 gas fees
                    .withGas(28_751L)
                    .expectGasToPay(28_751L)
                    .expectDeadlineExtendedBy(Duration.ofHours(24).toMillis()))),
        arguments(
            named(
                "Paying minimum for some staking fee extends to 24 hours and 1 ms",
                TestCase.defaultTestCase()
                    .withMsPerGas(1, 1) // corresponds to 2,419,200,000 gas fees
                    .withGas(86_400_001L)
                    .expectGasToPay(86_400_001L)
                    .expectDeadlineExtendedBy(
                        Duration.ofHours(24).plus(Duration.ofMillis(1)).toMillis()))),
        arguments(
            named(
                "Paying staking fees for some amount of stakes that is not divisible by 28 days",
                TestCase.defaultTestCase()
                    .withMsPerGas(268800000, 88889) // corresponds to 800,001 gas fees
                    .withGas(50_000L)
                    .expectGasToPay(50_000L)
                    .expectDeadlineExtendedBy(151_199_811L))),
        arguments(
            named(
                "Paying excess staking fees for some amount of stakes that is not divisible by 28"
                    + " days",
                TestCase.defaultTestCase()
                    .withMsPerGas(268800000, 88889) // corresponds to 800,001 gas fees
                    .withGas(7_050_000L)
                    .expectGasToPay(5_200_007L)
                    // 181 days, 23 hours, 59 minutes, 59 seconds and 999 milliseconds
                    .expectDeadlineExtendedBy(15_724_799_999L))));
  }

  @SuppressWarnings("unused")
  private record TestCase(
      Instant currentDeadline,
      long msPerGasNumerator,
      long msPerGasDenominator,
      Instant blockProductionTime,
      long gas,
      long expectedDeadlineExtension,
      long expectedGasToPay) {

    static TestCase defaultTestCase() {
      long twentyEightDays = Duration.ofDays(28).toMillis();
      return new TestCase(
          Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE),
          DEFAULT_MS_PER_GAS_NUMERATOR,
          DEFAULT_MS_PER_GAS_DENOMINATOR,
          Instant.ofEpochMilli(BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE - 1),
          twentyEightDays / (DEFAULT_MS_PER_GAS_NUMERATOR / DEFAULT_MS_PER_GAS_DENOMINATOR),
          twentyEightDays,
          twentyEightDays / (DEFAULT_MS_PER_GAS_NUMERATOR / DEFAULT_MS_PER_GAS_DENOMINATOR));
    }

    TestCase withMsPerGas(long numerator, long denominator) {
      return new TestCase(
          this.currentDeadline(),
          numerator,
          denominator,
          this.blockProductionTime(),
          this.gas(),
          this.expectedDeadlineExtension(),
          this.expectedGasToPay());
    }

    TestCase withBlockProductionTime(Instant blockProductionTime) {
      return new TestCase(
          this.currentDeadline(),
          this.msPerGasNumerator(),
          this.msPerGasDenominator(),
          blockProductionTime,
          this.gas(),
          this.expectedDeadlineExtension(),
          this.expectedGasToPay());
    }

    TestCase withGas(long gas) {
      return new TestCase(
          this.currentDeadline(),
          this.msPerGasNumerator(),
          this.msPerGasDenominator(),
          this.blockProductionTime(),
          gas,
          this.expectedDeadlineExtension(),
          this.expectedGasToPay());
    }

    TestCase expectDeadlineExtendedBy(long ms) {
      return new TestCase(
          this.currentDeadline(),
          this.msPerGasNumerator(),
          this.msPerGasDenominator(),
          this.blockProductionTime(),
          this.gas(),
          ms,
          this.expectedGasToPay());
    }

    TestCase expectGasToPay(long gasToPay) {
      return new TestCase(
          this.currentDeadline(),
          this.msPerGasNumerator(),
          this.msPerGasDenominator(),
          this.blockProductionTime(),
          this.gas(),
          this.expectedDeadlineExtension(),
          gasToPay);
    }
  }
}
