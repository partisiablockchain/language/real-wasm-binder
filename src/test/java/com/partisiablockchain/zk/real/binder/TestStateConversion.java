package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import com.secata.util.NumericConversion;

/** Helper class with utility functions for converting to and from open and zk state. */
public final class TestStateConversion {

  /**
   * Create {@link ZkState} (as LargeByteArray) from a long value.
   *
   * @param value state value
   * @return value wrapped in a LargeByteArray
   */
  public static LargeByteArray zkStateFromLong(long value) {
    return new LargeByteArray(NumericConversion.longToBytes(value));
  }

  /**
   * Convert from ZkState / {@link LargeByteArray} to a long.
   *
   * @param state ZkState
   * @return state as a long
   */
  public static long zkStateToLong(LargeByteArray state) {
    return NumericConversion.longFromBytes(state.getData(), 0);
  }

  /**
   * Create open state (WasmRealContractState) from a long value.
   *
   * @param value of the open state
   * @return new WasmRealContractState
   */
  public static WasmRealContractState openStateFromLong(long value) {
    final WasmRealContractState.ComputationInput computationInput =
        new WasmRealContractState.ComputationInput(0, FixedList.create());
    return new WasmRealContractState(zkStateFromLong(value), computationInput, AvlTree.create());
  }

  /**
   * Convert open state data to a long value.
   *
   * @param state open wasm state
   * @return state data as a long
   */
  public static long openStateToLong(WasmRealContractState state) {
    return NumericConversion.longFromBytes(state.getData(), 0);
  }
}
