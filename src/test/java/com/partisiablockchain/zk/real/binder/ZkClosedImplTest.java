package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.LargeByteArray;
import java.util.HexFormat;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link ZkClosedImpl}. */
public final class ZkClosedImplTest {

  @Test
  public void testIsSealedNope() {
    ZkClosedImpl closed = new ZkClosedImpl(null, 0, false, null, List.of(), null, 0);
    Assertions.assertThat(closed.isSealed()).isFalse();
    Assertions.assertThat(closed.getShareBitLengths()).isEmpty();
  }

  @Test
  public void testIsSealedYes() {
    ZkClosedImpl closed = new ZkClosedImpl(null, 0, true, null, List.of(), null, 0);
    Assertions.assertThat(closed.isSealed()).isTrue();
    Assertions.assertThat(closed.getShareBitLengths()).isEmpty();
  }

  @Test
  public void unsupportedField() {
    ZkClosedImpl closed = new ZkClosedImpl(null, 0, false, null, List.of(), null, 0);
    Assertions.assertThatThrownBy(() -> closed.getShareBitLength())
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("getShareBitLength() not supported on Real ZkClosed");
  }

  @Test
  public void testMiscSizes() {
    ZkClosedImpl closed =
        new ZkClosedImpl(null, 0, false, null, List.of(0, 1, 32, 64, 127), null, 0);
    Assertions.assertThat(closed.getShareBitLengths()).containsExactly(0, 1, 32, 64, 127);
  }

  @Test
  public void testLargeSizes() {
    ZkClosedImpl closed = new ZkClosedImpl(null, 0, false, null, List.of(128, 256), null, 0);
    Assertions.assertThat(closed.getShareBitLengths()).containsExactly(128, 256);
  }

  @Test
  public void testNegativeBitLengths() {
    Assertions.assertThatThrownBy(
            () ->
                new ZkClosedImpl(
                    null, 0, false, null, List.of(1, 2, -1, -2, 10000, -1000), null, 0))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "shareBitLengths must all be non-negative, but was [1, 2, -1, -2, 10000, -1000]");
  }

  @Test
  public void testSerialization() {
    ZkClosedImpl closed =
        new ZkClosedImpl(
            BlockchainAddress.fromString("000000000000000000000000000000000000000042"),
            1,
            true,
            new LargeByteArray(HexFormat.of().parseHex("21")),
            List.of(),
            null,
            0);
    Assertions.assertThat(closed.serialize())
        .hasSize(closed.serializedByteLength())
        .isEqualTo(
            HexFormat.of()
                .parseHex(
                    "01000000" // Variable id
                        + "000000000000000000000000000000000000000042" // owner
                        + "01" // isSealed
                        + "21" // metadata
                        + "00")); // no open data
  }

  @Test
  public void testSerializationWithOpen() {
    ZkClosedImpl closed =
        new ZkClosedImpl(
                BlockchainAddress.fromString("000000000000000000000000000000000000000042"),
                1,
                true,
                new LargeByteArray(HexFormat.of().parseHex("21")),
                List.of(),
                null,
                0)
            .withOpen(new LargeByteArray(HexFormat.of().parseHex("010001")));
    Assertions.assertThat(closed.serialize())
        .hasSize(closed.serializedByteLength())
        .isEqualTo(
            HexFormat.of()
                .parseHex(
                    "01000000" // Variable id
                        + "000000000000000000000000000000000000000042" // owner
                        + "01" // isSealed
                        + "21" // metadata
                        + "01" // some open data
                        + "01000000" // length of open data (packed)
                        + "05")); // open data (packed)
  }

  @Test
  void testPackSize() {
    for (int size = 0; size < 100; size++) {
      byte[] bits = new byte[size];
      Assertions.assertThat(ZkClosedImpl.packLittleEndianBitArray(bits).length)
          .isEqualTo(ZkClosedImpl.packedBitArraySize(bits));
    }
  }
}
