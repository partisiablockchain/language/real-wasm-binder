package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractBinder;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractInvoker;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractInvokerFactory;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.WithResource;
import java.io.InputStream;
import java.time.Instant;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test the PAR-12274 issue in which a contract is unable to extend ZK node allocation, due to the
 * contract not having enough gas to pay for new triple batches.
 */
final class TripleBatchRequestBugfixTest {

  private static final String contractIdentifier = "03054fd4ab40819a846e777b16d1aa863dff86633f";
  private static final long blockProductionTime =
      Instant.parse("2024-02-19T11:26:18Z").toEpochMilli();
  private static final BlockchainAddress nodeRegistry =
      BlockchainAddress.fromString("01a2020bb33ef9e0323c7a3210d5cb7fd492aa0d65");
  private static final long gas = 28_572;

  @Test
  void triplesAreOnlyRequestedWhenNotAssignedToComputation() throws Exception {
    byte[] contractBytes = readResourceBytes(contractIdentifier + ".zkwa");
    WasmRealContractInvoker contractInvoker =
        WasmRealContractInvokerFactory.invokerFromBytes(contractBytes);
    WasmRealContractBinder wasmRealContractBinder = new WasmRealContractBinder(contractInvoker);

    ObjectMapper objectMapper = StateObjectMapper.createObjectMapper();
    ZkStateImmutable stateImmutable =
        objectMapper.readValue(
            readResourceBytes(contractIdentifier + ".json"), ZkStateImmutable.class);

    assertBinaryTripleBatches(stateImmutable);

    BinderResult<ZkStateImmutable, BinderInteraction> result =
        wasmRealContractBinder.invoke(
            TestBinderContext.withGas(123, blockProductionTime, nodeRegistry, gas),
            stateImmutable,
            SafeDataOutputStream.serialize(
                s -> {
                  // InvokeExtendZkComputationDeadline#IDENTIFIER
                  s.writeByte(0x13);
                  s.writeLong(3024);
                  s.writeLong(1);
                  s.writeLong(86400000);
                  s.writeLong(15724800000L);
                }));

    ZkStateImmutable updatedState = result.getState();
    Assertions.assertThat(result.getInvocations()).isEmpty();
    assertBinaryTripleBatches(updatedState);
  }

  private static void assertBinaryTripleBatches(ZkStateImmutable stateImmutable) {
    // Assert that we are not waiting for triples to be assigned the computation
    Assertions.assertThat(stateImmutable.getComputationState().isWaitingForTriples()).isFalse();

    PreProcessMaterialState preProcessMaterials =
        stateImmutable.getPreProcessMaterials(PreProcessMaterialType.BINARY_TRIPLE);
    // Assert that the batches used are the ones we have
    Assertions.assertThat(preProcessMaterials.batchIds()).hasSize(3);
    Assertions.assertThat(preProcessMaterials.batchIds())
        .containsExactlyInAnyOrderElementsOf(stateImmutable.getComputationState().getTripleIds());
    Assertions.assertThat(preProcessMaterials.numUsedElements()).isEqualTo(202712);
    Assertions.assertThat(preProcessMaterials.numRequestedBatches()).isEqualTo(3);
  }

  private byte[] readResourceBytes(String file) {
    return WithResource.apply(
        () -> this.getClass().getResourceAsStream(file),
        InputStream::readAllBytes,
        "Unable to read resource " + file);
  }
}
