package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.zk.real.binder.RealContractBinder.calculatePreprocessingFees;
import static com.partisiablockchain.zk.real.binder.ZkStateMutable.calculateComputationFees;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.binder.ZkStateMutable.ComputationStateFromMutable;
import com.partisiablockchain.zk.real.binder.counter.CountingShareStorage;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link ZkStateMutable}. */
public final class ZkStateMutableTest {

  private final Hash hash = ZkStateImmutableBuilderTest.TRANSACTION;
  private final List<Integer> inputRequests = new ArrayList<>();
  private final PreprocessedMaterialRequester inputMaskBatchRequester =
      (t, i) -> inputRequests.add(i);

  private static final EngineState engines =
      EngineState.read(
          SafeDataInputStream.createFromBytes(
              SafeDataOutputStream.serialize(
                  stream -> {
                    for (int i = 0; i < 4; i++) {
                      BinderTest.ADDRESSES.get(i).write(stream);
                      BinderTest.PUBLIC_KEYS.get(i).write(stream);
                      stream.writeString("https://");
                    }
                  })));

  @Test
  public void createPendingInput() {
    long availableGas = 100_000L;
    ZkBinderContext binderContext =
        TestBinderContext.withGas(
            1, BinderTest.DEFAULT_BLOCK_PRODUCTION_TIME, BinderTest.ACCOUNT_ONE, availableGas);
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    ZkStateImmutable state =
        new ZkStateImmutable(engines, null, null, BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE, true);
    ZkStateMutable mutable = ZkStateMutable.createMutableStateBinary(state, context);

    Assertions.assertThat(mutable.getCalculationStatus()).isEqualTo(CalculationStatus.WAITING);
    Assertions.assertThat(mutable.getPendingInputs(BinderTest.ACCOUNT_ONE)).hasSize(0);

    createPending(BinderTest.ACCOUNT_ONE, mutable);
    Assertions.assertThat(mutable.getPendingInput(1)).isNotNull();
    Assertions.assertThat(inputRequests).containsExactly(1);

    createPending(BinderTest.ACCOUNT_ONE, mutable);
    Assertions.assertThat(mutable.getPendingInput(2)).isNotNull();
    Assertions.assertThat(mutable.getPendingInputs(BinderTest.ACCOUNT_ONE)).hasSize(2);
    Assertions.assertThat(inputRequests).containsExactly(1);

    createPending(BinderTest.ACCOUNT_TWO, mutable);
    Assertions.assertThat(mutable.getPendingInput(3)).isNotNull();
    Assertions.assertThat(inputRequests).containsExactly(1);

    createPending(BinderTest.ACCOUNT_TWO, mutable);
    Assertions.assertThat(mutable.getPendingInput(4)).isNotNull();
    Assertions.assertThat(inputRequests).containsExactly(1);

    // ACCOUNT_TWO has two pendingInputs, each with two values of size 1 and 32.
    List<RealClosed<LargeByteArray>> pendingInputs =
        mutable.getPendingInputs(BinderTest.ACCOUNT_TWO);
    Assertions.assertThat(pendingInputs).hasSize(2);
    Assertions.assertThat(pendingInputs.get(0).getShareBitLengths()).containsExactly(1, 32);
    Assertions.assertThat(pendingInputs.get(1).getShareBitLengths()).containsExactly(1, 32);
  }

  @Test
  public void manyPendingInputs1() {
    manyPendingInputs(
        List.of(PreProcessMaterialType.BINARY_INPUT_MASK.getBatchSize() + 1), List.of(2));
  }

  @Test
  public void manyPendingInputs2() {
    manyPendingInputs(
        List.of(PreProcessMaterialType.BINARY_INPUT_MASK.getBatchSize() - 1, 2), List.of(1, 1));
  }

  @Test
  public void manyPendingInputs3() {
    manyPendingInputs(
        List.of(PreProcessMaterialType.BINARY_INPUT_MASK.getBatchSize() - 1, 1), List.of(1, 0));
  }

  @Test
  public void manyPendingInputs4() {
    manyPendingInputs(
        Collections.nCopies(11, PreProcessMaterialType.BINARY_INPUT_MASK.getBatchSize() / 10),
        List.of(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1));
  }

  @Test
  public void manyPendingInputs5() {
    manyPendingInputs(
        Collections.nCopies(32, 32),
        List.of(
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 1));
  }

  private void manyPendingInputs(
      List<Integer> numBitsPerPending, List<Integer> numBatchesRequested) {
    Assertions.assertThat(numBitsPerPending)
        .as("Precondition for using manyPendingInputs")
        .hasSameSizeAs(numBatchesRequested);

    long availableGas = 100_000_000L;
    ZkBinderContext binderContext =
        TestBinderContext.withGas(
            1, BinderTest.DEFAULT_BLOCK_PRODUCTION_TIME, BinderTest.ACCOUNT_ONE, availableGas);
    ZkStateImmutable state =
        new ZkStateImmutable(engines, null, null, BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE, true);

    for (int idx = 0; idx < numBitsPerPending.size(); idx++) {
      state =
          createPendingInputForManyPendingInputsTest(
              state, binderContext, numBitsPerPending.get(idx));

      final List<Integer> numBatchesRequestSoFar =
          numBatchesRequested.stream().limit(idx + 1).filter(x -> x != 0).toList();
      final int expectedNumBatchesRequested =
          numBatchesRequestSoFar.stream().mapToInt(x -> x).sum();

      Assertions.assertThat(state.getInputMaskState().numRequestedBatches())
          .as("Iteration " + idx)
          .isEqualTo(expectedNumBatchesRequested);
      Assertions.assertThat(inputRequests)
          .as("Iteration " + idx)
          .containsExactlyElementsOf(numBatchesRequestSoFar);
    }
  }

  private ZkStateImmutable createPendingInputForManyPendingInputsTest(
      final ZkStateImmutable statePre, final ZkBinderContext binderContext, final int numBits) {

    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    final ZkStateMutable mutableState = ZkStateMutable.createMutableStateBinary(statePre, context);

    mutableState.createPendingInput(
        ZkStateImmutableBuilderTest.TRANSACTION,
        BinderTest.ACCOUNT_ONE,
        false,
        TestStateConversion.zkStateFromLong(1),
        Collections.nCopies(numBits, 1),
        null,
        null,
        null,
        inputMaskBatchRequester,
        null);

    return mutableState.getState();
  }

  private void createPending(BlockchainAddress account, ZkStateMutable mutable) {
    mutable.createPendingInput(
        ZkStateImmutableBuilderTest.TRANSACTION,
        account,
        false,
        TestStateConversion.zkStateFromLong(1),
        List.of(1, 32),
        null,
        null,
        null,
        inputMaskBatchRequester,
        null);
  }

  @Test
  public void computationStateFromMutable() {
    ZkStateImmutable state =
        new ZkStateImmutable()
            .builder()
            .setOpenState(TestStateConversion.openStateFromLong(12))
            .setVariable(
                new ZkClosedImpl(BinderTest.ACCOUNT_ONE, 1, false, null, List.of(2), hash, 0))
            .build();

    ComputationStateFromMutable computationState = new ComputationStateFromMutable(state);

    Assertions.assertThat(TestStateConversion.openStateToLong(computationState.getOpenState()))
        .isEqualTo(12L);
    Assertions.assertThat(computationState.getVariable(1)).isNotNull();
    Assertions.assertThat(computationState.getVariable(2)).isNull();
    Assertions.assertThat(computationState.getVariables()).hasSize(1);
  }

  @Test
  public void countingShareStorage() {
    ZkStateImmutable state =
        new ZkStateImmutable()
            .builder()
            .setOpenState(TestStateConversion.openStateFromLong(12))
            .setVariable(
                new ZkClosedImpl(BinderTest.ACCOUNT_ONE, 1, false, null, List.of(1, 2, 3), hash, 0))
            .build();

    final CountingShareStorage<SecretSharedNumberAsBits, WasmRealContractState, LargeByteArray>
        storage = CountingShareStorage.forBinary(new ComputationStateFromMutable(state));

    Assertions.assertThat(storage.loadNumber(0)).isNull();
    List<SecretSharedNumberAsBits> variable = storage.loadNumber(1);
    Assertions.assertThat(variable).isNotNull();
    Assertions.assertThat(variable.get(0).get(0))
        .isNotNull()
        .isEqualTo(BinaryExtensionFieldElement.ZERO);
    Assertions.assertThat(variable.get(1))
        .isNotNull()
        .usingRecursiveComparison()
        .isEqualTo(
            SecretSharedNumberAsBits.create(
                Collections.nCopies(2, BinaryExtensionFieldElement.ZERO)));
    Assertions.assertThat(variable.get(2))
        .isNotNull()
        .usingRecursiveComparison()
        .isEqualTo(
            SecretSharedNumberAsBits.create(
                Collections.nCopies(3, BinaryExtensionFieldElement.ZERO)));

    storage.saveNumber(
        SecretSharedNumberAsBits.create(Collections.nCopies(7, BinaryExtensionFieldElement.ZERO)));
    storage.saveNumber(
        SecretSharedNumberAsBits.create(Collections.nCopies(1, BinaryExtensionFieldElement.ZERO)));

    Assertions.assertThat(storage.bitLengths()).containsExactly(7, 1);
  }

  @Test
  public void createStateWithConfirmedVariables() {
    ZkBinderContext binderContext =
        TestBinderContext.withDefaultGas(
            1, BinderTest.DEFAULT_BLOCK_PRODUCTION_TIME, BinderTest.ACCOUNT_ONE);
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    ZkStateImmutable state = new ZkStateImmutable();
    ZkStateImmutableBuilder builder = state.builder();
    ZkClosedImpl variable =
        new ZkClosedImpl(
            BinderTest.ACCOUNT_ONE,
            1,
            false,
            TestStateConversion.zkStateFromLong(1),
            List.of(1),
            hash,
            0);
    builder.setVariable(variable.withBlindedInputs(new LargeByteArray(new byte[1])));
    ZkStateMutable mutable = ZkStateMutable.createMutableStateBinary(builder.build(), context);
    Assertions.assertThat(mutable.getConfirmedInputs()).isEqualTo(1);
    Assertions.assertThat(mutable.getVariables()).hasSize(1);
    Assertions.assertThat(mutable.getVariable(1).getBlindedInputs()).hasSize(1);
  }

  @Test
  public void ensureConfirmedInputsAreCounted() {
    long availableGas = 25_000L;
    ZkBinderContext binderContext =
        TestBinderContext.withGas(
            1, BinderTest.DEFAULT_BLOCK_PRODUCTION_TIME, BinderTest.ACCOUNT_ONE, availableGas);
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);

    EngineState engines =
        EngineState.read(
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(
                    stream -> {
                      for (int i = 0; i < 4; i++) {
                        BinderTest.ADDRESSES.get(i).write(stream);
                        BinderTest.PUBLIC_KEYS.get(i).write(stream);
                        stream.writeString("https://");
                      }
                    })));

    ZkStateImmutable state =
        new ZkStateImmutable(engines, null, null, BinderTest.DEFAULT_ZK_COMPUTATION_DEADLINE, true);
    ZkStateMutable mutable = ZkStateMutable.createMutableStateBinary(state, context);
    mutable.createPendingInput(
        ZkStateImmutableBuilderTest.TRANSACTION,
        BinderTest.ACCOUNT_FOUR,
        false,
        TestStateConversion.zkStateFromLong(42),
        List.of(1),
        null,
        null,
        null,
        (t, size) -> Assertions.assertThat(size).isEqualTo(1),
        null);
    PendingInputImpl pending = mutable.getState().getPendingInput().getValue(1);
    Assertions.assertThat(mutable.getConfirmedInputs()).isEqualTo(0);
    pending =
        pending
            .addNodeReaction(0, new LargeByteArray(new byte[16]))
            .addNodeReaction(1, new LargeByteArray(new byte[16]))
            .addNodeReaction(2, new LargeByteArray(new byte[16]));
    ZkStateImmutableBuilder builder = mutable.getState().builder();
    builder.setPendingInput(pending);
    mutable = ZkStateMutable.createMutableStateBinary(builder.build(), context);
    mutable.handleOpenOfMaskedInput(
        new LargeByteArray(new byte[16]), pending, BinderTest.ADDRESSES.get(3));
    Assertions.assertThat(mutable.getConfirmedInputs()).isEqualTo(1);
  }

  @Test
  public void cannotPendingInputAfterDeadlineReached() {
    long blockProductionTime = 1L;
    long zkDeadline = 0L;
    ZkBinderContext binderContext =
        TestBinderContext.withDefaultGas(1L, blockProductionTime, BinderTest.ACCOUNT_ONE);
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    ZkStateImmutable state = new ZkStateImmutable(engines, null, null, zkDeadline, true);

    ZkStateImmutableBuilder builder = state.builder();
    builder
        .addPreProcessMaterialBatch(PreProcessMaterialType.BINARY_INPUT_MASK, 1)
        .addPreProcessMaterialBatch(PreProcessMaterialType.BINARY_INPUT_MASK, 2);
    ZkStateMutable mutable = ZkStateMutable.createMutableStateBinary(builder.build(), context);

    PreprocessedMaterialRequester inputRequestAsserter =
        (t, n) -> Assertions.fail("should not be called");
    Assertions.assertThatThrownBy(
            () ->
                mutable.createPendingInput(
                    ZkStateImmutableBuilderTest.TRANSACTION,
                    BinderTest.ACCOUNT_ONE,
                    false,
                    TestStateConversion.zkStateFromLong(1),
                    Collections.nCopies(1001, 1),
                    null,
                    null,
                    null,
                    inputRequestAsserter,
                    null))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(
            "Unable to create inputs and start computations when the ZK computation deadline for"
                + " the contract has been exceeded");
  }

  @Test
  void pendingInputAllowedWhenExactlyOnDeadline() {
    long blockProductionTime = 1L;
    long zkDeadline = 1L;
    ZkBinderContext binderContext =
        TestBinderContext.withDefaultGas(1L, blockProductionTime, BinderTest.ACCOUNT_ONE);
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    ZkStateImmutable state = new ZkStateImmutable(engines, null, null, zkDeadline, true);

    ZkStateImmutableBuilder builder = state.builder();
    builder
        .addPreProcessMaterialBatch(PreProcessMaterialType.BINARY_INPUT_MASK, 1)
        .addPreProcessMaterialBatch(PreProcessMaterialType.BINARY_INPUT_MASK, 2);
    ZkStateMutable mutable = ZkStateMutable.createMutableStateBinary(builder.build(), context);

    Assertions.assertThatNoException()
        .isThrownBy(
            () ->
                mutable.createPendingInput(
                    ZkStateImmutableBuilderTest.TRANSACTION,
                    BinderTest.ACCOUNT_ONE,
                    false,
                    TestStateConversion.zkStateFromLong(1),
                    Collections.nCopies(1001, 1),
                    null,
                    null,
                    null,
                    (t, i) -> {},
                    null));
  }

  @Test
  public void getAttestationIds() {
    ZkBinderContext binderContext =
        TestBinderContext.withDefaultGas(
            1L, BinderTest.DEFAULT_BLOCK_PRODUCTION_TIME, BinderTest.ACCOUNT_ONE);
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    ZkStateImmutable state = new ZkStateImmutable(engines, null, null, 0L, true);

    ZkStateImmutableBuilder builder = state.builder();
    ZkStateMutable mutable = ZkStateMutable.createMutableStateBinary(builder.build(), context);

    byte[] blob = new byte[] {1, 2, 3, 4, 5};
    mutable.attestData(blob);
    Assertions.assertThat(mutable.getAttestationIds()).isNotEmpty();
    Assertions.assertThat(mutable.getAttestation(1)).isNotNull();
  }

  @Test
  public void feeCalculation() {
    long multiplications = 0;
    long expectedComputationFees = 25000L * 2;
    Assertions.assertThat(calculateComputationFees(multiplications))
        .isEqualTo(expectedComputationFees);

    multiplications = 12345;
    expectedComputationFees = multiplications * 5 + 25000L * 2;
    Assertions.assertThat(calculateComputationFees(multiplications))
        .isEqualTo(expectedComputationFees);

    int batches = 0;
    long expectedPreprocessingFees = 0;
    Assertions.assertThat(calculatePreprocessingFees(batches, PreProcessMaterialType.BINARY_TRIPLE))
        .isEqualTo(expectedPreprocessingFees);

    batches = 4;
    expectedPreprocessingFees = batches * (500000L + 25000L * 2);
    Assertions.assertThat(calculatePreprocessingFees(batches, PreProcessMaterialType.BINARY_TRIPLE))
        .isEqualTo(expectedPreprocessingFees);
  }

  private static PreProcessMaterialState materialState(
      long numUsedElements, int numRequestedBatches) {
    return new PreProcessMaterialState(FixedList.create(), numUsedElements, numRequestedBatches, 0);
  }

  @Test
  public void batchesNeededForMultiplicationsTest() {
    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                0L, PreProcessMaterialType.BINARY_TRIPLE, materialState(0L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(0, 0, 0));
    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                100L, PreProcessMaterialType.BINARY_TRIPLE, materialState(0L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(0, 1, 0));
    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                100L, PreProcessMaterialType.BINARY_TRIPLE, materialState(100L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(0, 1, 100));
    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                100_000L, PreProcessMaterialType.BINARY_TRIPLE, materialState(100L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(0, 2, 100));
    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                99_999L, PreProcessMaterialType.BINARY_TRIPLE, materialState(1L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(0, 1, 1));
    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                100_000L, PreProcessMaterialType.BINARY_TRIPLE, materialState(0L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(0, 1, 0));
    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                100_000L, PreProcessMaterialType.BINARY_TRIPLE, materialState(100_400L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(1, 3, 400));

    Assertions.assertThat(
            ZkStateMutable.batchInfoForComputation(
                100L, PreProcessMaterialType.BINARY_TRIPLE, materialState(1_000_000_000_400L, 0)))
        .isEqualTo(new ZkStateMutable.BatchInfoForComputation(10_000_000, 10_000_001, 400));
  }
}
