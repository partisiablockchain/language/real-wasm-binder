package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link ComputationStateImpl}. */
public final class ComputationStateImplTest {

  @Test
  public void getters() {
    ComputationStateImpl state =
        new ComputationStateImpl(
            false,
            0,
            0,
            1,
            TestStateConversion.openStateFromLong(11L),
            AvlTree.create(
                Map.of(
                    1,
                    new ZkClosedImpl(
                        BinderTest.ACCOUNT_ONE,
                        1,
                        false,
                        TestStateConversion.zkStateFromLong(12L),
                        List.of(1),
                        ZkStateImmutableBuilderTest.TRANSACTION,
                        0))),
            new ComputationOutput(
                (byte) 1,
                FixedList.create(
                    List.of(
                        new PendingOutputVariable(2, TestStateConversion.zkStateFromLong(13L), 7))),
                FixedList.create()),
            null);

    Assertions.assertThat(TestStateConversion.openStateToLong(state.getOpenState())).isEqualTo(11L);
    Assertions.assertThat(state.getVariables()).hasSize(1);
    Assertions.assertThat(state.getVariable(1)).isNotNull();
    Assertions.assertThat(state.getVariable(1).getId()).isEqualTo(1);
    Assertions.assertThat(state.getVariables().get(0)).isSameAs(state.getVariable(1));

    Assertions.assertThat(state.getOutputIds()).hasSize(1);
    Assertions.assertThat(state.getOutputIds().get(0)).isEqualTo(2);
    Assertions.assertThat(state.getOffsetIntoFirstTripleBatch()).isEqualTo(0);
    Assertions.assertThat(state.getOnComputeCompleteShortname()).isNull();
  }
}
