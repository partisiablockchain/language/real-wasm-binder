package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.zk.real.binder.EngineState.EngineInformation;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link EngineInformation}. */
public final class EngineStateTest {

  @Test
  public void engineInformation() {
    String expected = "https://zk.privacyblockchain.io/";
    BlockchainAddress identity =
        BlockchainAddress.fromString("000000000000000000000000000000000000000011");
    BlockchainPublicKey publicKey = new KeyPair().getPublic();
    EngineInformation engineInformation = new EngineInformation(identity, publicKey, expected);

    Assertions.assertThat(engineInformation.getRestInterface()).isEqualTo(expected);
    Assertions.assertThat(engineInformation.getIdentity()).isEqualTo(identity);
    Assertions.assertThat(engineInformation.getPublicKey()).isEqualTo(publicKey);
  }
}
