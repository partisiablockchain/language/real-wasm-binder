package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.wasminvoker.avltrees.ReadOnlyAvlTreeMapper;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.LittleEndianByteOutput;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SerializationAvlTreeTest {

  private static final ZkClosedImpl zkVariable =
      new ZkClosedImpl(
          BlockchainAddress.fromString("000000000000000000000000000000000000000042"),
          1,
          true,
          new LargeByteArray(HexFormat.of().parseHex("21")),
          List.of(),
          null,
          0);

  @Test
  void getValue() {
    AvlTree<Integer, ZkClosedImpl> variableTree = AvlTree.create(Map.of(1, zkVariable));

    ReadOnlyAvlTreeMapper specialTree =
        new SerializationAvlTree<>(
            variableTree, ZkClosedImpl::serialize, ZkClosedImpl::serializedByteLength);
    byte[] value = specialTree.getValue(intToBytes(1));
    Assertions.assertThat(specialTree.getValueSize(intToBytes(1))).isEqualTo(value.length);
    Assertions.assertThat(HexFormat.of().formatHex(value))
        .isEqualTo(
            "01000000" // Variable id
                + "000000000000000000000000000000000000000042" // owner
                + "01" // isSealed
                + "21" // metadata
                + "00"); // some open data

    Assertions.assertThat(specialTree.getValue(intToBytes(4))).isNull();
    Assertions.assertThat(specialTree.getValueSize(intToBytes(4))).isEqualTo(-1);
  }

  @Test
  void size() {
    AvlTree<Integer, ZkClosedImpl> variableTree = AvlTree.create(Map.of(1, zkVariable));

    ReadOnlyAvlTreeMapper specialTree =
        new SerializationAvlTree<>(
            variableTree, ZkClosedImpl::serialize, ZkClosedImpl::serializedByteLength);
    Assertions.assertThat(specialTree.size()).isEqualTo(1);
  }

  @Test
  void getNext() {
    AvlTree<Integer, ZkClosedImpl> variableTree = AvlTree.create(Map.of(2, zkVariable));

    ReadOnlyAvlTreeMapper specialTree =
        new SerializationAvlTree<>(
            variableTree, ZkClosedImpl::serialize, ZkClosedImpl::serializedByteLength);
    Map.Entry<byte[], byte[]> entry = specialTree.getNextEntry(null);
    Assertions.assertThat(specialTree.getNextEntrySize(null))
        .isEqualTo(entry.getKey().length + entry.getValue().length);
    Assertions.assertThat(HexFormat.of().formatHex(entry.getKey()))
        .isEqualTo("02000000"); // Variable id

    Assertions.assertThat(HexFormat.of().formatHex(entry.getValue()))
        .isEqualTo(
            "01000000" // Variable id
                + "000000000000000000000000000000000000000042" // owner
                + "01" // isSealed
                + "21" // metadata
                + "00"); // no open data

    Map.Entry<byte[], byte[]> entry2 = specialTree.getNextEntry(intToBytes(2));
    Assertions.assertThat(entry2).isNull();
    Assertions.assertThat(specialTree.getNextEntrySize(intToBytes(2))).isEqualTo(-1);
  }

  @Test
  void wrongSize() {
    AvlTree<Integer, ZkClosedImpl> variableTree = AvlTree.create(Map.of(2, zkVariable));

    ReadOnlyAvlTreeMapper specialTree =
        new SerializationAvlTree<>(variableTree, ZkClosedImpl::serialize, key -> 0);
    Assertions.assertThatThrownBy(() -> specialTree.getValue(intToBytes(2)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Length of serialized value (28) is not equal to expected size (0)");

    Assertions.assertThatThrownBy(() -> specialTree.getNextEntry(null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Length of serialized entry (32) is not equal to expected size (4)");
  }

  private byte[] intToBytes(int i) {
    return LittleEndianByteOutput.serialize(out -> out.writeI32(i));
  }
}
