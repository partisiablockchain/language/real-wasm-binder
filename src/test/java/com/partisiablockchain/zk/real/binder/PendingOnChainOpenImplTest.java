package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PendingOnChainOpenImplTest {
  @Test
  void isHandledByEngine() {

    PendingOnChainOpenImpl onChainOpen =
        new PendingOnChainOpenImpl(1, FixedList.create(List.of()), null);

    onChainOpen = onChainOpen.withEngineShares(0, null, dummyReader());
    onChainOpen = onChainOpen.withEngineShares(2, null, dummyReader());

    assertThat(onChainOpen.isHandledByEngine(0)).isTrue();
    assertThat(onChainOpen.isHandledByEngine(1)).isFalse();
    assertThat(onChainOpen.isHandledByEngine(2)).isTrue();
    assertThat(onChainOpen.isHandledByEngine(3)).isFalse();
  }

  private Shares.ShareReader dummyReader() {
    return (stream, bitLengths) -> null;
  }
}
