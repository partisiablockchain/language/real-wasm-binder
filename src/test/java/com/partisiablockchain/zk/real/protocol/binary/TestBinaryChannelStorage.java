package com.partisiablockchain.zk.real.protocol.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.binary.BinaryShareFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

/** Storage implementation for binary real. */
public final class TestBinaryChannelStorage
    implements SharedNumberStorage<SecretSharedNumberAsBits> {

  private final IntFunction<List<List<Boolean>>> variableLookup;
  private final List<SecretSharedNumberAsBits> results = new ArrayList<>();
  private final BinaryShareFactory shareFactory;

  /**
   * Create a storage for a binary REAL protocol.
   *
   * @param variableLookup function for looking up a variable
   */
  public TestBinaryChannelStorage(IntFunction<List<List<Boolean>>> variableLookup) {
    this.variableLookup = variableLookup;
    this.shareFactory = new BinaryShareFactory();
  }

  /**
   * Returns the collection of values associated with a variable. The returned collection stores
   * bits and numbers in the same order as they were initially provided.
   *
   * @param variableId the identifier of the bit variable
   * @return the values associated with a variable
   */
  @Override
  public List<SecretSharedNumberAsBits> loadNumber(int variableId) {
    List<SecretSharedNumberAsBits> values = new ArrayList<>();
    List<List<Boolean>> apply = variableLookup.apply(variableId);
    for (List<Boolean> number : apply) {
      List<BinaryExtensionFieldElement> binaryNumber =
          number.stream().map(shareFactory::constant).collect(Collectors.toList());
      values.add(SecretSharedNumberAsBits.create(binaryNumber));
    }
    return values;
  }

  @Override
  public void saveNumber(SecretSharedNumberAsBits value) {
    results.add(value);
  }

  List<SecretSharedNumberAsBits> getResults() {
    return Collections.unmodifiableList(results);
  }
}
