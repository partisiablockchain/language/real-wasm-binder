package com.partisiablockchain.zk.real.protocol.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.contract.TestChannel;
import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import java.util.List;
import java.util.function.IntFunction;

/** {@link TestChannel} for computing the circuits without MPC. */
public final class TestRealBinary
    implements TestChannel<SecretSharedNumberAsBits, BinaryExtensionFieldElement> {

  private final TestBinaryChannelStorage shareStorage;
  private final ElementMultiplier<BinaryExtensionFieldElement> multiplier;

  /**
   * Create {@link TestChannel}.
   *
   * @param multiplier {@link ElementMultiplier} for real binary.
   * @param variableLookup how to find variables in the storage
   */
  public TestRealBinary(
      ElementMultiplier<BinaryExtensionFieldElement> multiplier,
      IntFunction<List<List<Boolean>>> variableLookup) {
    this.shareStorage = new TestBinaryChannelStorage(variableLookup);
    this.multiplier = multiplier;
  }

  @Override
  public SharedNumberStorage<SecretSharedNumberAsBits> storage() {
    return this.shareStorage;
  }

  @Override
  public ElementMultiplier<BinaryExtensionFieldElement> multiplier() {
    return this.multiplier;
  }

  /**
   * Get the list of resulting numbers.
   *
   * @return list of resulting numbers
   */
  public List<SecretSharedNumberAsBits> getResults() {
    return this.shareStorage.getResults();
  }
}
