package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.SecretShared;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import java.util.List;

/**
 * {@link ElementMultiplier} for testing, performing computations in plain text, without secret
 * sharing.
 */
public record PlainTextMultiplier<
        ElementT extends SecretShared<ElementT> & FiniteFieldElement<ElementT>>()
    implements ElementMultiplier<ElementT> {

  @Override
  public ElementT multiply(ElementT left, ElementT right) {
    throw new UnsupportedOperationException("use multiplyPairwise instead");
  }

  @Override
  public List<ElementT> multiplyPairwise(Iterable<ElementT> left, Iterable<ElementT> right) {
    return BatchUtility.pairwiseMap(ElementT::multiply, left, right);
  }
}
