package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.binder.counter.CountingMultiplier;
import com.partisiablockchain.zk.real.binder.counter.CountingShareStorage;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.binary.BinaryProtocolFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link BinaryMultiplicationCounter}. */
public final class RealProtocolImplTest {

  @Test
  public void unusedGetter() {
    final var shareStorage = CountingShareStorage.forBinary(null);
    final RealProtocol<
            SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
        realProtocol =
            BinaryProtocolFactory.create(
                shareStorage, CountingMultiplier.<BinaryExtensionFieldElement>newFromZero());

    Assertions.assertThat(realProtocol).isNotNull();
    Assertions.assertThat(realProtocol.elementFactory()).isNotNull();
  }
}
