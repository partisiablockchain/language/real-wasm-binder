package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.SecretShared;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.bouncycastle.util.encoders.Hex;

/** Decorating multiplier for tracking multiplications. */
public final class OrderMonitorMultiplier<
        ElementT extends SecretShared<ElementT> & DataStreamSerializable>
    implements ElementMultiplier<ElementT> {

  private final ElementMultiplier<ElementT> wrappedMultiplier;
  private final ArrayList<AndComputation<ElementT>> computations;

  private record AndComputation<ElementT extends SecretShared<ElementT> & DataStreamSerializable>(
      List<ElementT> operandLeft, List<ElementT> operandRight) {
    public AndComputation {
      requireNonNull(operandLeft);
      requireNonNull(operandRight);
    }
  }

  /**
   * Constructor for {@link OrderMonitorMultiplier}.
   *
   * @param wrappedMultiplier the decorated multiplier
   */
  public OrderMonitorMultiplier(ElementMultiplier<ElementT> wrappedMultiplier) {
    this.wrappedMultiplier = wrappedMultiplier;
    this.computations = new ArrayList<>();
  }

  @Override
  public ElementT multiply(ElementT left, ElementT right) {
    throw new UnsupportedOperationException("use multiplyPairwise instead");
  }

  @Override
  public List<ElementT> multiplyPairwise(Iterable<ElementT> left, Iterable<ElementT> right) {
    computations.add(new AndComputation<>(toList(left), toList(right)));
    return wrappedMultiplier.multiplyPairwise(left, right);
  }

  /**
   * Amount of multiplications that multiplier have performed so far.
   *
   * @return Number of multiplications performed.
   */
  public int numMultiplications() {
    return numComputations();
  }

  /**
   * Amount of computations/rounds that multiplier have performed so far.
   *
   * @return Number of computations performed.
   */
  public int numComputations() {
    return computations.size();
  }

  /**
   * Amount of elements that have been processed so far.
   *
   * @return Number of elements processed.
   */
  public long numElementsSent() {
    return elementsStream().count();
  }

  private long numBytesSent() {
    return elementsStream()
        .mapToInt(elem -> SafeDataOutputStream.serialize(elem::write).length)
        .sum();
  }

  private Stream<ElementT> elementsStream() {
    return Stream.concat(
        computations.stream().map(AndComputation::operandLeft).flatMap(List::stream),
        computations.stream().map(AndComputation::operandRight).flatMap(List::stream));
  }

  /**
   * Generate report for the ands that have been performed so far.
   *
   * @return Formatted report
   */
  public String report() {
    if (computations.isEmpty()) {
      return null;
    }
    final StringBuilder l = new StringBuilder();

    l.append("")
        .append("AND COMP : %5d\n".formatted(numComputations()))
        .append("ELEMENT  : %5d\n".formatted(numElementsSent()))
        .append("BYTE     : %5d\n".formatted(numBytesSent()))
        .append("========================\n\n");

    for (final AndComputation<ElementT> computation : computations) {
      final String prefix = "AND %2d:".formatted(computation.operandLeft().size());
      l.append(prefix);
      for (ElementT elem : computation.operandLeft()) {
        final byte[] bytes = SafeDataOutputStream.serialize(elem::write);
        l.append(" ").append(Hex.toHexString(bytes));
      }
      l.append("\n").append(" ".repeat(prefix.length()));
      for (ElementT elem : computation.operandRight()) {
        final byte[] bytes = SafeDataOutputStream.serialize(elem::write);
        l.append(" ").append(Hex.toHexString(bytes));
      }
      l.append("\n");
    }

    return l.toString();
  }

  static <T> List<T> toList(Iterable<T> iterable) {
    final ArrayList<T> list = new ArrayList<>();
    for (T e : iterable) {
      list.add(e);
    }
    return List.copyOf(list);
  }
}
