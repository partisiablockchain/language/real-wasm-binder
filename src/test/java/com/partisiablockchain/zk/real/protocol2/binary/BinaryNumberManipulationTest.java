package com.partisiablockchain.zk.real.protocol2.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.zk.real.binder.counter.CountingShareStorage;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.OrderMonitorMultiplier;
import com.partisiablockchain.zk.real.protocol2.PlainTextMultiplier;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import com.partisiablockchain.zk.real.protocol2.ReferenceTestUtility;
import java.nio.file.Path;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

/** Tests for methods defined in {@link BinaryNumberManipulation}. */
@DisplayNameGeneration(DisplayNameGenerator.Simple.class)
public final class BinaryNumberManipulationTest {

  OrderMonitorMultiplier<BinaryExtensionFieldElement> orderMonitorMultiplier;
  RealProtocol<SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
      real;

  private static final BinaryExtensionFieldElement ZERO = BinaryExtensionFieldElement.ZERO;
  private static final BinaryExtensionFieldElement ONE = BinaryExtensionFieldElement.ONE;

  private static final SharedNumberStorage<SecretSharedNumberAsBits> USELESS_STORAGE =
      new CountingShareStorage<>(null, null);

  private SecretSharedNumberAsBits makeSbi32(long value) {
    return real.shareFactory().constant(value, 32);
  }

  /** Initializes each units-under-test. */
  @BeforeEach
  public void createComponents(TestInfo testInfo) {
    this.orderMonitorMultiplier =
        new OrderMonitorMultiplier<>(new PlainTextMultiplier<BinaryExtensionFieldElement>());
    this.real = BinaryProtocolFactory.create(USELESS_STORAGE, orderMonitorMultiplier);
  }

  /**
   * Ensures that the computations attempted, if any, have proceeded in the manner proscribed by the
   * reference. This is very important, in order to document and test for underlying protocol
   * changes.
   */
  @AfterEach
  public void checkMultiplierReference(TestInfo testInfo) {
    final String report = orderMonitorMultiplier.report();
    final Path path =
        ReferenceTestUtility.getReferenceDirectoryPath(BinaryNumberManipulationTest.class)
            .resolve("%s.txt".formatted(testInfo.getDisplayName()));

    final boolean hasFile = path.toFile().exists();

    if (!hasFile && report == null) {
      return; // Test did not perform any sends.
    }
    if (!hasFile) {
      ReferenceTestUtility.writeText(path, report);
      Assertions.fail("Reference file newly written. Run tests again.");
    }

    assertThat(report)
        .as("Computation protocol bytes: " + path.toString())
        .as(path.toString())
        .isNotNull()
        .isEqualToNormalizingNewlines(ReferenceTestUtility.loadText(path));
  }

  @Test
  public void leSigned() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    assertThatThrownBy(() -> real.numeric().leSigned(wrongLeft, wrongRight))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");

    assertThat(real.numeric().leSigned(makeSbi32(27), makeSbi32(27)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leSigned(makeSbi32(27), makeSbi32(66)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leSigned(makeSbi32(66), makeSbi32(27)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().leSigned(makeSbi32(-12), makeSbi32(27)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leSigned(makeSbi32(27), makeSbi32(-12)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().leSigned(makeSbi32(-33), makeSbi32(-12)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leSigned(makeSbi32(-12), makeSbi32(-33)).isEqual(ZERO)).isTrue();
    assertThat(
            real.numeric()
                .leSigned(makeSbi32(Integer.MIN_VALUE), makeSbi32(Integer.MAX_VALUE))
                .isEqual(ONE))
        .isTrue();
    assertThat(
            real.numeric()
                .leSigned(makeSbi32(Integer.MAX_VALUE), makeSbi32(Integer.MIN_VALUE))
                .isEqual(ZERO))
        .isTrue();

    assertThat(real.numeric().gtSigned(makeSbi32(27), makeSbi32(27)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtSigned(makeSbi32(27), makeSbi32(66)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtSigned(makeSbi32(66), makeSbi32(27)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().gtSigned(makeSbi32(-12), makeSbi32(27)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtSigned(makeSbi32(27), makeSbi32(-12)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().gtSigned(makeSbi32(-33), makeSbi32(-12)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtSigned(makeSbi32(-12), makeSbi32(-33)).isEqual(ONE)).isTrue();
    assertThat(
            real.numeric()
                .gtSigned(makeSbi32(Integer.MIN_VALUE), makeSbi32(Integer.MAX_VALUE))
                .isEqual(ZERO))
        .isTrue();
    assertThat(
            real.numeric()
                .gtSigned(makeSbi32(Integer.MAX_VALUE), makeSbi32(Integer.MIN_VALUE))
                .isEqual(ONE))
        .isTrue();
  }

  @Test
  public void leUnsigned() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    assertThatThrownBy(() -> real.numeric().leUnsigned(wrongLeft, wrongRight))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");

    assertThat(real.numeric().leUnsigned(makeSbi32(27), makeSbi32(27)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leUnsigned(makeSbi32(27), makeSbi32(66)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leUnsigned(makeSbi32(66), makeSbi32(27)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().leUnsigned(makeSbi32(-12), makeSbi32(27)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().leUnsigned(makeSbi32(27), makeSbi32(-12)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leUnsigned(makeSbi32(-33), makeSbi32(-12)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().leUnsigned(makeSbi32(-12), makeSbi32(-33)).isEqual(ZERO)).isTrue();
    assertThat(
            real.numeric()
                .leUnsigned(makeSbi32(Integer.MIN_VALUE), makeSbi32(Integer.MAX_VALUE))
                .isEqual(ZERO))
        .isTrue();
    assertThat(
            real.numeric()
                .leUnsigned(makeSbi32(Integer.MAX_VALUE), makeSbi32(Integer.MIN_VALUE))
                .isEqual(ONE))
        .isTrue();

    assertThat(real.numeric().gtUnsigned(makeSbi32(27), makeSbi32(27)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtUnsigned(makeSbi32(27), makeSbi32(66)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtUnsigned(makeSbi32(66), makeSbi32(27)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().gtUnsigned(makeSbi32(-12), makeSbi32(27)).isEqual(ONE)).isTrue();
    assertThat(real.numeric().gtUnsigned(makeSbi32(27), makeSbi32(-12)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtUnsigned(makeSbi32(-33), makeSbi32(-12)).isEqual(ZERO)).isTrue();
    assertThat(real.numeric().gtUnsigned(makeSbi32(-12), makeSbi32(-33)).isEqual(ONE)).isTrue();
    assertThat(
            real.numeric()
                .gtUnsigned(makeSbi32(Integer.MIN_VALUE), makeSbi32(Integer.MAX_VALUE))
                .isEqual(ONE))
        .isTrue();
    assertThat(
            real.numeric()
                .gtUnsigned(makeSbi32(Integer.MAX_VALUE), makeSbi32(Integer.MIN_VALUE))
                .isEqual(ZERO))
        .isTrue();
  }

  @Test
  public void parallelEq() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    assertThatThrownBy(() -> real.numeric().parallelEq(List.of(wrongLeft), List.of(wrongRight)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");

    assertThat(
            real.numeric()
                .parallelEq(List.of(makeSbi32(42)), List.of(makeSbi32(42)))
                .get(0)
                .isEqual(ONE))
        .isTrue();
    assertThat(
            real.numeric()
                .parallelEq(List.of(makeSbi32(42)), List.of(makeSbi32(43)))
                .get(0)
                .isEqual(ZERO))
        .isTrue();
    assertThat(
            real.numeric()
                .parallelEq(List.of(makeSbi32(43)), List.of(makeSbi32(42)))
                .get(0)
                .isEqual(ZERO))
        .isTrue();
  }

  @Test
  public void addOperandsNotSameLength() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    assertThatThrownBy(() -> real.numeric().parallelAdd(List.of(wrongLeft), List.of()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operand lists must be of same length: 1 vs 0");
    assertThatThrownBy(() -> real.numeric().parallelAdd(List.of(wrongLeft), List.of(wrongRight)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");
  }

  @Test
  public void add() {
    SecretSharedNumberAsBits left = makeSbi32(31);
    SecretSharedNumberAsBits right = makeSbi32(11);
    SecretSharedNumberAsBits expected = makeSbi32(42);
    assertThat(real.numeric().parallelAdd(List.of(left), List.of(right)).get(0).isEqual(expected))
        .isTrue();
  }

  @Test
  public void addSmallNumbers() {
    SecretSharedNumberAsBits left = real.shareFactory().constant(31, 7);
    SecretSharedNumberAsBits right = real.shareFactory().constant(11, 7);
    SecretSharedNumberAsBits expected = real.shareFactory().constant(42, 7);
    assertThat(real.numeric().parallelAdd(List.of(left), List.of(right)).get(0).isEqual(expected))
        .isTrue();
  }

  @Test
  public void multOperandsNotSameLength() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    assertThatThrownBy(() -> real.numeric().mult(wrongLeft, wrongRight))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");
  }

  @Test
  public void mult() {
    SecretSharedNumberAsBits left = makeSbi32(42);
    SecretSharedNumberAsBits right = makeSbi32(10);
    SecretSharedNumberAsBits expected = makeSbi32(420);
    assertThat(real.numeric().mult(left, right).isEqual(expected)).isTrue();
  }

  @Test
  public void div() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    assertThatThrownBy(() -> real.numeric().div(wrongLeft, wrongRight))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");

    SecretSharedNumberAsBits left = makeSbi32(1);
    SecretSharedNumberAsBits right = makeSbi32(1);
    assertThatThrownBy(() -> real.numeric().div(left, right))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Division has not been implemented yet");
  }

  @Test
  public void subtractOperandsNotSameLength() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    assertThatThrownBy(() -> real.numeric().subtract(wrongLeft, wrongRight))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");
  }

  @Test
  public void subtractIsWip() {
    SecretSharedNumberAsBits left = makeSbi32(1);
    SecretSharedNumberAsBits right = makeSbi32(1);
    assertThatThrownBy(() -> real.numeric().subtract(left, right))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Subtraction has not been implemented yet");
  }

  @Test
  public void selectNumberOperandsNotSameLength() {
    SecretSharedNumberAsBits wrongLeft = real.shareFactory().constant(27, 11);
    SecretSharedNumberAsBits wrongRight = real.shareFactory().constant(27, 10);
    BinaryExtensionFieldElement wrongSelector = ONE;
    assertThatThrownBy(() -> real.numeric().selectNumber(wrongSelector, wrongLeft, wrongRight))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operands must be of same length: 11 vs 10");
  }

  @Test
  public void selectNumber() {
    SecretSharedNumberAsBits trueValue = makeSbi32(27);
    SecretSharedNumberAsBits falseValue = makeSbi32(42);

    assertThat(real.numeric().selectNumber(ONE, trueValue, falseValue).isEqual(makeSbi32(27)))
        .isTrue();

    assertThat(real.numeric().selectNumber(ZERO, trueValue, falseValue).isEqual(makeSbi32(42)))
        .isTrue();
  }

  // the next couple of tests are related to SecretSharedNumberAsBits

  @Test
  public void xorConstant() {
    List<BinaryExtensionFieldElement> bits = List.of(ONE, ZERO, ONE);
    List<BinaryExtensionFieldElement> negated = List.of(ZERO, ONE, ZERO);

    SecretSharedNumberAsBits xorFalse = real.shareFactory().number(bits);
    SecretSharedNumberAsBits xorFalseExpected = real.shareFactory().number(bits);
    assertThat(xorFalse.xor(false).isEqual(xorFalseExpected)).isTrue();

    SecretSharedNumberAsBits xorTrue = real.shareFactory().number(bits);
    SecretSharedNumberAsBits xorTrueExpected = real.shareFactory().number(negated);
    assertThat(xorTrue.xor(true).isEqual(xorTrueExpected)).isTrue();
  }

  @Test
  public void andConstant() {
    List<BinaryExtensionFieldElement> bits = List.of(ONE, ZERO, ONE);

    List<BinaryExtensionFieldElement> allFalse = List.of(ZERO, ZERO, ZERO);
    SecretSharedNumberAsBits andFalse = real.shareFactory().number(bits);
    SecretSharedNumberAsBits andFalseExpected = real.shareFactory().number(allFalse);
    assertThat(andFalse.and(false).isEqual(andFalseExpected)).isTrue();

    List<BinaryExtensionFieldElement> someFalse = List.of(ONE, ZERO, ONE);
    SecretSharedNumberAsBits andTrue = real.shareFactory().number(bits);
    SecretSharedNumberAsBits andTrueExpected = real.shareFactory().number(someFalse);
    assertThat(andTrue.and(true).isEqual(andTrueExpected)).isTrue();
  }

  @Test
  public void append() {
    List<BinaryExtensionFieldElement> firstBits = List.of(ONE, ZERO);
    List<BinaryExtensionFieldElement> lastBits = List.of(ZERO, ONE);
    List<BinaryExtensionFieldElement> allBits = List.of(ONE, ZERO, ZERO, ONE);

    SecretSharedNumberAsBits first = real.shareFactory().number(firstBits);
    SecretSharedNumberAsBits last = real.shareFactory().number(lastBits);
    SecretSharedNumberAsBits all = real.shareFactory().number(allBits);
    assertThat(first.append(last).isEqual(all)).isTrue();
  }

  @Test
  public void isZero() {
    SecretSharedNumberAsBits zero = makeSbi32(0);
    SecretSharedNumberAsBits twelve = makeSbi32(12);
    SecretSharedNumberAsBits one = makeSbi32(1);

    assertThat(real.numeric().isZero(zero).isEqual(BinaryExtensionFieldElement.ONE)).isTrue();
    assertThat(real.numeric().isZero(twelve).isEqual(BinaryExtensionFieldElement.ZERO)).isTrue();
    assertThat(real.numeric().isZero(one).isEqual(BinaryExtensionFieldElement.ZERO)).isTrue();
  }

  @Test
  public void ifNegative() {
    SecretSharedNumberAsBits zero = makeSbi32(0);
    SecretSharedNumberAsBits twelve = makeSbi32(-12);
    SecretSharedNumberAsBits one = makeSbi32(1);

    assertThat(real.numeric().isNegative(zero).isEqual(BinaryExtensionFieldElement.ZERO)).isTrue();
    assertThat(real.numeric().isNegative(twelve).isEqual(BinaryExtensionFieldElement.ONE)).isTrue();
    assertThat(real.numeric().isNegative(one).isEqual(BinaryExtensionFieldElement.ZERO)).isTrue();
  }
}
