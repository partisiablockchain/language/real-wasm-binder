package com.partisiablockchain.zk.real.protocol2.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.ShareFactory;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link BinaryShareFactory}. */
public final class BinaryShareFactoryTest {

  private static final ShareFactory<SecretSharedNumberAsBits, BinaryExtensionFieldElement> FACTORY =
      new BinaryShareFactory();

  private static final List<BinaryExtensionFieldElement> SOME_BITS =
      List.of(
          BinaryExtensionFieldElement.ONE,
          BinaryExtensionFieldElement.ZERO,
          BinaryExtensionFieldElement.ONE,
          BinaryExtensionFieldElement.ONE);

  @Test
  public void randomBits() {
    Assertions.assertThatCode(() -> FACTORY.randomBits(5))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("ShareFactory#randomBits is not supported");
  }

  @Test
  public void constant() {
    SecretSharedNumberAsBits first = FACTORY.constant(42, Long.SIZE);
    SecretSharedNumberAsBits second = FACTORY.constant(42);
    Assertions.assertThat(first).containsExactlyElementsOf(second);
  }

  @Test
  public void bits() {
    SecretSharedNumberAsBits n = FACTORY.constant(5, 4);
    Assertions.assertThat(FACTORY.bit(n, 0)).isEqualTo(BinaryExtensionFieldElement.ONE);
    Assertions.assertThat(FACTORY.bit(n, 1)).isEqualTo(BinaryExtensionFieldElement.ZERO);
    Assertions.assertThat(FACTORY.bit(n, 2)).isEqualTo(BinaryExtensionFieldElement.ONE);
    Assertions.assertThat(FACTORY.bit(n, 3)).isEqualTo(BinaryExtensionFieldElement.ZERO);
  }

  @Test
  public void fromBitsAndSize() {
    SecretSharedNumberAsBits justRight = FACTORY.number(4, SOME_BITS);
    SecretSharedNumberAsBits expected = FACTORY.constant(13, 4);
    Assertions.assertThat(justRight).containsExactlyElementsOf(expected);

    SecretSharedNumberAsBits sizeLargerThanBits = FACTORY.number(5, SOME_BITS);
    SecretSharedNumberAsBits expectedWithMoreBits = FACTORY.constant(13, 5);
    Assertions.assertThat(sizeLargerThanBits).containsExactlyElementsOf(expectedWithMoreBits);
  }

  @Test
  public void numberConstructors() {
    final SecretSharedNumberAsBits left = FACTORY.number(4, SOME_BITS);
    final SecretSharedNumberAsBits right = FACTORY.number(4, SOME_BITS::get);
    Assertions.assertThat(left).containsExactlyElementsOf(right);
  }
}
