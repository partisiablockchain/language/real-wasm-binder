package com.partisiablockchain.zk.real.protocol2.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.zk.real.binder.counter.CountingMultiplier;
import com.partisiablockchain.zk.real.binder.counter.CountingShareStorage;
import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.OrderMonitorMultiplier;
import com.partisiablockchain.zk.real.protocol2.PlainTextMultiplier;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import com.partisiablockchain.zk.real.protocol2.ReferenceTestUtility;
import java.nio.file.Path;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

/** Tests for methods defined in {@link BinaryBitManipulation}. */
@DisplayNameGeneration(DisplayNameGenerator.Simple.class)
public final class BinaryBitManipulationTest {

  OrderMonitorMultiplier<BinaryExtensionFieldElement> orderMonitorMultiplier;
  RealProtocol<SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
      real;

  private static final SharedNumberStorage<SecretSharedNumberAsBits> USELESS_STORAGE =
      new CountingShareStorage<>(null, null);

  private static final BinaryExtensionFieldElement ZERO = BinaryExtensionFieldElement.ZERO;
  private static final BinaryExtensionFieldElement ONE = BinaryExtensionFieldElement.ONE;

  /** Initializes each units-under-test. */
  @BeforeEach
  public void createComponents(TestInfo testInfo) {
    this.orderMonitorMultiplier =
        new OrderMonitorMultiplier<>(new PlainTextMultiplier<BinaryExtensionFieldElement>());
    this.real = BinaryProtocolFactory.create(USELESS_STORAGE, orderMonitorMultiplier);
  }

  /**
   * Ensures that the computations attempted, if any, have proceeded in the manner proscribed by the
   * reference. This is very important, in order to document and test for underlying protocol
   * changes.
   */
  @AfterEach
  public void checkMultiplierReference(TestInfo testInfo) {
    final String report = orderMonitorMultiplier.report();
    final Path path =
        ReferenceTestUtility.getReferenceDirectoryPath(BinaryBitManipulationTest.class)
            .resolve("%s.txt".formatted(testInfo.getDisplayName()));

    final boolean hasFile = path.toFile().exists();

    if (!hasFile && report == null) {
      return; // Test did not perform any sends.
    }
    if (!hasFile) {
      ReferenceTestUtility.writeText(path, report);
      Assertions.fail("Reference file newly written. Run tests again.");
    }

    assertThat(report)
        .as(path.toString())
        .isEqualToNormalizingNewlines(ReferenceTestUtility.loadText(path));
  }

  @Test
  public void xor() {
    assertThat(real.bits().xor(ZERO, ZERO).isEqual(ZERO)).isTrue();
    assertThat(real.bits().xor(ZERO, ONE).isEqual(ONE)).isTrue();
    assertThat(real.bits().xor(ONE, ZERO).isEqual(ONE)).isTrue();
    assertThat(real.bits().xor(ONE, ONE).isEqual(ZERO)).isTrue();
  }

  @Test
  public void and() {
    assertThat(real.bits().and(ZERO, ZERO).isEqual(ZERO)).isTrue();
    assertThat(real.bits().and(ZERO, ONE).isEqual(ZERO)).isTrue();
    assertThat(real.bits().and(ONE, ZERO).isEqual(ZERO)).isTrue();
    assertThat(real.bits().and(ONE, ONE).isEqual(ONE)).isTrue();
  }

  @Test
  public void or() {
    assertThat(real.bits().or(ZERO, ZERO).isEqual(ZERO)).isTrue();
    assertThat(real.bits().or(ZERO, ONE).isEqual(ONE)).isTrue();
    assertThat(real.bits().or(ONE, ZERO).isEqual(ONE)).isTrue();
    assertThat(real.bits().or(ONE, ONE).isEqual(ONE)).isTrue();
  }

  @Test
  public void orReducer() {
    final List<BinaryExtensionFieldElement> allFalse = List.of(ZERO, ZERO, ZERO, ZERO, ZERO, ZERO);
    assertThat(real.bits().orReducer(allFalse).isEqual(ZERO)).isTrue();
    final List<BinaryExtensionFieldElement> oneTrue = List.of(ZERO, ZERO, ONE, ZERO, ZERO, ZERO);
    assertThat(real.bits().orReducer(oneTrue).isEqual(ONE)).isTrue();
  }

  @Test
  public void andReducer() {
    final List<BinaryExtensionFieldElement> allTrue = List.of(ONE, ONE, ONE, ONE, ONE, ONE);
    assertThat(real.bits().andReducer(allTrue).isEqual(ONE)).isTrue();
    final List<BinaryExtensionFieldElement> oneFalse = List.of(ONE, ONE, ONE, ZERO, ONE, ONE);
    assertThat(real.bits().andReducer(oneFalse).isEqual(ZERO)).isTrue();
  }

  @Test
  public void selectBit() {
    assertThat(real.bits().selectBit(ZERO, ZERO, ZERO).isEqual(ZERO)).isTrue();
    assertThat(real.bits().selectBit(ZERO, ZERO, ONE).isEqual(ONE)).isTrue();
    assertThat(real.bits().selectBit(ZERO, ONE, ZERO).isEqual(ZERO)).isTrue();
    assertThat(real.bits().selectBit(ZERO, ONE, ONE).isEqual(ONE)).isTrue();
    assertThat(real.bits().selectBit(ONE, ZERO, ZERO).isEqual(ZERO)).isTrue();
    assertThat(real.bits().selectBit(ONE, ZERO, ONE).isEqual(ZERO)).isTrue();
    assertThat(real.bits().selectBit(ONE, ONE, ZERO).isEqual(ONE)).isTrue();
    assertThat(real.bits().selectBit(ONE, ONE, ONE).isEqual(ONE)).isTrue();
  }

  @Test
  public void unsupportedSingleMultiply() {
    final ElementMultiplier<BinaryExtensionFieldElement> countingMultiplier =
        CountingMultiplier.newFromZero();
    Assertions.assertThatThrownBy(() -> countingMultiplier.multiply(null, null))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("use multiplyPairwise instead");
  }
}
