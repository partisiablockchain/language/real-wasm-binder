package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.LargeByteArray;
import java.util.HexFormat;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ConfirmedEventLogTest {
  @Test
  void serialize() {
    ConfirmedEventLog externalEvent =
        new ConfirmedEventLog(
            42,
            84,
            new EvmEventLog(
                null,
                new LargeByteArray(new byte[] {1, 2, 3}),
                TopicList.fromTopics(
                    List.of(Topic.fromBytes(new byte[32]), Topic.fromBytes(new byte[32])))));

    byte[] expected =
        HexFormat.of()
            .parseHex(
                "2a000000" // subscription id
                    + "54000000" // event id
                    + "03000000" // data length
                    + "010203" // data
                    + "02000000" // topics length
                    + "0000000000000000000000000000000000000000000000000000000000000000" // topic1
                    + "0000000000000000000000000000000000000000000000000000000000000000"); // topic2

    Assertions.assertThat(externalEvent.serialize()).isEqualTo(expected);
    Assertions.assertThat(externalEvent.serializedByteLength()).isEqualTo(expected.length);
  }
}
