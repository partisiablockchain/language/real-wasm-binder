package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TopicTest {

  @Test
  void asBytes() {
    byte[] bytes = new byte[32];
    Topic topic = Topic.fromBytes(bytes);
    Assertions.assertThat(topic.asBytes()).isEqualTo(bytes);
  }

  @Test
  void equals() {
    EqualsVerifier.forClass(Topic.class).withNonnullFields("topic").verify();
  }

  @Test
  void tooShortValue() {
    Assertions.assertThatThrownBy(() -> Topic.fromBytes(new byte[31]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected 32 bytes but got 31");
  }

  @Test
  void tooLongValue() {
    Assertions.assertThatThrownBy(() -> Topic.fromBytes(new byte[33]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected 32 bytes but got 33");
  }
}
