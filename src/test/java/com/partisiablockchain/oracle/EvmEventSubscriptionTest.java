package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import com.secata.tools.immutable.FixedList;
import java.util.HexFormat;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EvmEventSubscriptionTest {

  @Test
  void getters() {
    int subId = 2112;
    boolean isCancelled = false;
    String chainId = "BNB_SMART_CHAIN";
    EvmAddress address = EvmAddress.fromBytes(new byte[20]);
    Unsigned256 fromBlock = Unsigned256.create(1989);
    List<TopicList> topics =
        List.of(
            TopicList.fromTopics(List.of(Topic.fromBytes(new byte[32]))),
            TopicList.fromTopics(
                List.of(Topic.fromBytes(new byte[32]), Topic.fromBytes(new byte[32]))),
            TopicList.fromTopics(
                List.of(
                    Topic.fromBytes(new byte[32]),
                    Topic.fromBytes(new byte[32]),
                    Topic.fromBytes(new byte[32]))));
    EvmEventSubscription subscription =
        new EvmEventSubscription(
            subId,
            isCancelled,
            chainId,
            address,
            fromBlock,
            FixedList.create(topics),
            null,
            null,
            null);
    Assertions.assertThat(subscription.getId()).isEqualTo(subId);
    Assertions.assertThat(subscription.isCancelled()).isEqualTo(isCancelled);
    Assertions.assertThat(subscription.getChainId()).isEqualTo(chainId);
    Assertions.assertThat(subscription.contractAddress()).isEqualTo(address.asBytes());
    Assertions.assertThat(subscription.fromBlock()).isEqualTo(fromBlock);
    Assertions.assertThat(subscription.rawTopics()).containsExactlyElementsOf(topics);
  }

  @Test
  void serialize() {
    int subId = 2112;
    boolean isCancelled = false;
    String chainId = "BNB_SMART_CHAIN";
    EvmAddress address = EvmAddress.fromBytes(new byte[20]);
    Unsigned256 fromBlock = Unsigned256.create(1989);
    List<TopicList> topics =
        List.of(
            TopicList.fromTopics(
                List.of(Topic.fromBytes(new byte[32]), Topic.fromBytes(new byte[32]))));
    EvmEventSubscription subscription =
        new EvmEventSubscription(
            subId,
            isCancelled,
            chainId,
            address,
            fromBlock,
            FixedList.create(topics),
            null,
            null,
            null);

    byte[] expected =
        HexFormat.of()
            .parseHex(
                "40080000" // subscription id
                    + "00" // is cancelled
                    + "0f000000" // chain id length
                    + "424e425f534d4152545f434841494e" // chain id: BNB_SMART_CHAIN
                    + "0000000000000000000000000000000000000000" // evm address
                    + "c507000000000000000000000000000000000000000000000000000000000000" // block
                    + "01000000" // number of topic lists
                    + "02000000" // number of topics
                    + "0000000000000000000000000000000000000000000000000000000000000000" // topic1
                    + "0000000000000000000000000000000000000000000000000000000000000000"); // topic2

    Assertions.assertThat(subscription.serialize()).isEqualTo(expected);
    Assertions.assertThat(subscription.serializedByteLength()).isEqualTo(expected.length);
  }
}
