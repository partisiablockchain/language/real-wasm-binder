package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;
import org.bouncycastle.util.encoders.Hex;

/** An EVM address. Represented as a 40 character hexadecimal string. */
@Immutable
public final class EvmAddress implements DataStreamSerializable, StateSerializableInline {
  private static final int ADDRESS_BYTES_LENGTH = 20;
  private final String address;

  @SuppressWarnings("unused")
  EvmAddress() {
    this.address = null;
  }

  private EvmAddress(String address) {
    this.address = address;
  }

  /**
   * Read the EVM address from an input stream.
   *
   * @param rpc to read from
   * @return new EVM address
   */
  public static EvmAddress read(SafeDataInputStream rpc) {
    byte[] bytes = rpc.readBytes(ADDRESS_BYTES_LENGTH);
    return fromBytes(bytes);
  }

  /**
   * Create a new EVM address from a byte value.
   *
   * @param address to create address from
   * @return new EVM address
   */
  public static EvmAddress fromBytes(byte[] address) {
    if (address.length != ADDRESS_BYTES_LENGTH) {
      String errorMessage =
          String.format("Expected %d bytes but got %d", ADDRESS_BYTES_LENGTH, address.length);
      throw new IllegalArgumentException(errorMessage);
    } else {
      return new EvmAddress(Hex.toHexString(address));
    }
  }

  /**
   * Get the byte encoding of the EVM address.
   *
   * @return address as bytes
   */
  public byte[] asBytes() {
    return Hex.decode(address);
  }

  @Override
  public void write(SafeDataOutputStream out) {
    out.write(asBytes());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof EvmAddress that)) {
      return false;
    }
    return Objects.equals(address, that.address);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(address);
  }
}
