package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Arrays;
import java.util.Objects;

/**
 * An event log emitted by a contract on an external EVM chain.
 *
 * <p>For more information on what the different fields in an EVM event log, see the <a
 * href="https://ethereum.org/en/developers/docs/apis/json-rpc/">Ethereum JSON-RPC API</a> for more
 * information.
 */
@Immutable
public final class EvmEventLog implements DataStreamSerializable, StateSerializable {

  private final EventMetadata metadata;
  private final LargeByteArray data;
  private final TopicList topics;

  @SuppressWarnings("unused")
  EvmEventLog() {
    this.metadata = null;
    this.data = null;
    this.topics = null;
  }

  /**
   * Constructor for the EVM event log.
   *
   * @param metadata event log metadata
   * @param data event log data
   * @param topics event log topics
   */
  public EvmEventLog(EventMetadata metadata, LargeByteArray data, TopicList topics) {
    this.metadata = metadata;
    this.data = data;
    this.topics = topics;
  }

  /**
   * Read an EVM event log from an input stream.
   *
   * @param rpc to read from
   * @return a new EVM event log
   */
  public static EvmEventLog read(SafeDataInputStream rpc) {
    EventMetadata metadata = EventMetadata.read(rpc);
    byte[] data = rpc.readDynamicBytes();
    TopicList topics = TopicList.read(rpc);
    return new EvmEventLog(metadata, new LargeByteArray(data), topics);
  }

  /**
   * Get metadata for the event log.
   *
   * @return event log metadata
   */
  public EventMetadata getMetadata() {
    return metadata;
  }

  /**
   * Get the log data.
   *
   * @return event log data
   */
  public byte[] data() {
    return data.getData();
  }

  /**
   * Get the log topics.
   *
   * @return event log topics
   */
  public TopicList topics() {
    return topics;
  }

  @Override
  public void write(SafeDataOutputStream out) {
    out.writeDynamicBytes(data.getData());
    topics.write(out);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof EvmEventLog that)) {
      return false;
    }
    return Objects.equals(metadata, that.metadata)
        && Arrays.equals(data.getData(), that.data.getData())
        && Objects.equals(topics, that.topics);
  }

  @Override
  public int hashCode() {
    return Objects.hash(metadata, Arrays.hashCode(data.getData()), topics);
  }
}
