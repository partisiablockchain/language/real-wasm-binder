package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.LittleEndianByteOutput;
import java.util.List;

/** An external EVM event log that has been confirmed by a majority of the oracle nodes. */
@Immutable
public final class ConfirmedEventLog implements StateSerializable {

  private final int eventId;
  private final int subscriptionId;
  private final EvmEventLog eventLog;

  @SuppressWarnings("unused")
  ConfirmedEventLog() {
    this.eventId = -1;
    this.subscriptionId = -1;
    this.eventLog = null;
  }

  ConfirmedEventLog(int subscriptionId, int eventId, EvmEventLog eventData) {
    this.subscriptionId = subscriptionId;
    this.eventId = eventId;
    this.eventLog = eventData;
  }

  /**
   * Get the id of the event.
   *
   * @return event id
   */
  public int eventId() {
    return eventId;
  }

  /**
   * Get the id of the subscription this event was created for.
   *
   * @return subscription id
   */
  public int subscriptionId() {
    return subscriptionId;
  }

  /**
   * Get the event log.
   *
   * @return event log
   */
  public EvmEventLog getEventLog() {
    return eventLog;
  }

  /**
   * Serialize into wasm byte representation of an event.
   *
   * @return wasm byte representation.
   */
  public byte[] serialize() {
    return LittleEndianByteOutput.serialize(
        out -> {
          out.writeI32(subscriptionId);
          out.writeI32(eventId);
          byte[] data = eventLog.data();
          out.writeI32(data.length);
          out.writeBytes(data);
          List<byte[]> topics = eventLog.topics().asBytes();
          out.writeI32(topics.size());
          for (byte[] topic : topics) {
            out.writeBytes(topic);
          }
        });
  }

  /**
   * Computes the size of the event when serialized to bytes.
   *
   * @return size of serialized bytes.
   */
  public int serializedByteLength() {
    int size = 0;
    // Subscription id
    size += 4;
    // Event id
    size += 4;
    // data
    size += 4;
    size += eventLog.data().length;

    // Topics
    size += 4;
    size += eventLog.topics().topics().size() * 32;

    return size;
  }
}
