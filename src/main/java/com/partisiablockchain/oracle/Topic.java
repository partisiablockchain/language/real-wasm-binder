package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;
import org.bouncycastle.util.encoders.Hex;

/** An EVM event topic. Ensures that the topic value is exactly 32 bytes. */
@Immutable
public final class Topic implements StateSerializableInline, DataStreamSerializable {

  private static final int TOPIC_BYTES_LENGTH = 32;

  private final String topic;

  @SuppressWarnings("unused")
  Topic() {
    this.topic = null;
  }

  private Topic(String topic) {
    this.topic = topic;
  }

  /**
   * Read a topic from input stream.
   *
   * @param rpc to read from
   * @return a new topic
   */
  public static Topic read(SafeDataInputStream rpc) {
    byte[] bytes = rpc.readBytes(TOPIC_BYTES_LENGTH);
    return fromBytes(bytes);
  }

  /**
   * Create a new topic from bytes.
   *
   * @param topic bytes to create topic from
   * @return a new topic
   */
  public static Topic fromBytes(byte[] topic) {
    if (topic.length != TOPIC_BYTES_LENGTH) {
      String errorMessage =
          String.format("Expected %d bytes but got %d", TOPIC_BYTES_LENGTH, topic.length);
      throw new IllegalArgumentException(errorMessage);
    } else {
      return new Topic(Hex.toHexString(topic));
    }
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.write(asBytes());
  }

  /**
   * Get the topic value as raw bytes.
   *
   * @return topic as bytes
   */
  public byte[] asBytes() {
    return Hex.decode(topic);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Topic that)) {
      return false;
    }
    return Objects.equals(topic, that.topic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(topic);
  }
}
