package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** A list of EVM event topics. */
@Immutable
public final class TopicList implements DataStreamSerializable, StateSerializableInline {

  private final FixedList<Topic> topics;

  @SuppressWarnings("unused")
  TopicList() {
    this.topics = null;
  }

  /**
   * Default constructor for a topic filter.
   *
   * @param topics list of topics to match on
   */
  private TopicList(FixedList<Topic> topics) {
    this.topics = topics;
  }

  /**
   * Read a TopicList from an input stream.
   *
   * @param rpc to read from
   * @return a new TopicList
   */
  public static TopicList read(SafeDataInputStream rpc) {
    List<Topic> result = new ArrayList<>();
    int count = rpc.readInt();
    for (int i = 0; i < count; i++) {
      Topic topic = Topic.read(rpc);
      result.add(topic);
    }
    return new TopicList(FixedList.create(result));
  }

  /**
   * Create TopicList from a list of topics.
   *
   * @param topics to create topic list from
   * @return new TopicList
   */
  public static TopicList fromTopics(List<Topic> topics) {
    return new TopicList(FixedList.create(topics));
  }

  /**
   * Get the topic list as a list of bytes.
   *
   * @return list of raw topics bytes
   */
  public List<byte[]> asBytes() {
    return topics.stream().map(Topic::asBytes).toList();
  }

  /**
   * Get the list of topics.
   *
   * @return topics list
   */
  public List<Topic> topics() {
    return List.copyOf(topics);
  }

  @Override
  public void write(SafeDataOutputStream out) {
    out.writeInt(topics.size());
    for (Topic topic : topics) {
      topic.write(out);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TopicList that)) {
      return false;
    }
    return Objects.equals(new ArrayList<>(topics), new ArrayList<>(that.topics));
  }

  @Override
  public int hashCode() {
    return Objects.hash(new ArrayList<>(topics));
  }
}
