package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.ListUtil;
import com.secata.tools.immutable.FixedList;
import java.util.Collections;

/**
 * Heartbeat pending verification by EVM oracle.
 *
 * <p>A heartbeat indicates that the oracle nodes have not read any events for a given event
 * subscription for a certain amount of blocks since either the last confirmed event or the last
 * heartbeat. The heartbeat is used to compensate the nodes at regular intervals for their continued
 * work for the event subscription.
 */
@Immutable
public final class PendingHeartbeat implements StateSerializable {
  private static final ListUtil<Unsigned256> listHelper = ListUtil.createNull();

  private final FixedList<Unsigned256> candidates;

  @SuppressWarnings("unused")
  PendingHeartbeat() {
    this.candidates = null;
  }

  private PendingHeartbeat(FixedList<Unsigned256> candidates) {
    this.candidates = candidates;
  }

  static PendingHeartbeat create() {
    return new PendingHeartbeat(FixedList.create());
  }

  /**
   * Get the confirmed block number of the pending heartbeat. May return null if the heartbeat has
   * not been confirmed yet.
   *
   * @return the confirmed heartbeat block number or null
   */
  public Unsigned256 confirmHeartbeat() {
    for (Unsigned256 current : candidates) {
      if (current == null) {
        continue;
      }
      int count = Collections.frequency(candidates, current);
      if (count >= 3) {
        return current;
      }
    }
    return null;
  }

  /**
   * Register heartbeat as reported by an oracle.
   *
   * @param subscriptionId id of the subscription to report to
   * @param oracleIndex index of the reporting oracle node
   * @param heartbeatBlock reported heartbeat block number
   * @return updated pending heartbeat
   */
  public PendingHeartbeat registerHeartbeat(
      int subscriptionId, int oracleIndex, Unsigned256 heartbeatBlock) {
    if (candidates.size() > oracleIndex && candidates.get(oracleIndex) != null) {
      String errorMessage =
          "Pending heartbeat block already registered for subscription %d by oracle %d"
              .formatted(subscriptionId, oracleIndex);
      throw new IllegalArgumentException(errorMessage);
    } else {
      FixedList<Unsigned256> updated =
          listHelper.withEntry(candidates, oracleIndex, heartbeatBlock);
      return new PendingHeartbeat(updated);
    }
  }

  /**
   * Get the candidate heartbeat for the oracle.
   *
   * @param oracleIndex to get candidate for
   * @return candidate or null
   */
  public Unsigned256 candidateFor(int oracleIndex) {
    if (candidates.size() > oracleIndex) {
      return candidates.get(oracleIndex);
    } else {
      return null;
    }
  }
}
