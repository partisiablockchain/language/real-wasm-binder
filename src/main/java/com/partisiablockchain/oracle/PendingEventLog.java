package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.ListUtil;
import com.secata.tools.immutable.FixedList;
import java.util.Collections;

/** Event log that is pending final verification by an EVM oracle. */
@Immutable
public final class PendingEventLog implements StateSerializable {
  private static final ListUtil<EvmEventLog> listHelper = ListUtil.createNull();

  private final int subscriptionId;
  private final int eventId;
  private final FixedList<EvmEventLog> candidates;

  @SuppressWarnings("unused")
  PendingEventLog() {
    this.subscriptionId = -1;
    this.eventId = -1;
    this.candidates = null;
  }

  private PendingEventLog(int subscriptionId, int eventId, FixedList<EvmEventLog> candidates) {
    this.subscriptionId = subscriptionId;
    this.eventId = eventId;
    this.candidates = candidates;
  }

  /**
   * Create a new pending event log, with an empty list of candidates.
   *
   * @param subscriptionId of the subscription the event is pending for
   * @param eventId that the event will be assigned when confirmed
   * @return new pending event log
   */
  public static PendingEventLog create(int subscriptionId, int eventId) {
    return new PendingEventLog(subscriptionId, eventId, FixedList.create());
  }

  /**
   * Add an event candidate for the oracle. If a candidate already exists throw an illegal argument
   * exception.
   *
   * @param observedEvent candidate to add
   * @param oracleIndex index of the observing oracle
   * @return pending event log updated with the new candidate
   */
  public PendingEventLog addCandidateForOracle(EvmEventLog observedEvent, int oracleIndex) {
    if (candidates.size() > oracleIndex && candidates.get(oracleIndex) != null) {
      String errorMessage =
          "Event %s already added to subscription %d by oracle %d"
              .formatted(observedEvent.getMetadata(), subscriptionId, oracleIndex);
      throw new IllegalArgumentException(errorMessage);
    } else {
      return new PendingEventLog(
          subscriptionId, eventId, listHelper.withEntry(candidates, oracleIndex, observedEvent));
    }
  }

  /**
   * Find a candidate that enough oracles agree on and return it as the newly confirmed event. If no
   * such candidate exists yet, return null.
   *
   * @return a confirmed candidate, or null
   */
  public ConfirmedEventLog confirmEvent() {
    for (EvmEventLog currentCandidate : candidates) {
      if (currentCandidate == null) {
        continue;
      }
      int count = Collections.frequency(candidates, currentCandidate);
      if (count >= 3) {
        return new ConfirmedEventLog(subscriptionId, eventId, currentCandidate);
      }
    }
    return null;
  }

  /**
   * Get the candidate event log for the oracle.
   *
   * @param oracleIndex to get candidate for
   * @return candidate or null
   */
  public EvmEventLog candidateFor(int oracleIndex) {
    if (candidates.size() > oracleIndex) {
      return candidates.get(oracleIndex);
    } else {
      return null;
    }
  }

  /**
   * Get the id of the subscription this event is pending for.
   *
   * @return subscription id
   */
  public int getSubscriptionId() {
    return subscriptionId;
  }
}
