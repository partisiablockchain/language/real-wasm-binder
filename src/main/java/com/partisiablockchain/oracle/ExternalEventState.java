package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.evm.oracle.contract.EventLog;
import com.partisiablockchain.evm.oracle.contract.EventSubscription;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;

/** State for external events and subscriptions. */
@Immutable
public final class ExternalEventState implements StateSerializable {
  private final int nextSubscriptionId;
  private final AvlTree<Integer, EvmEventSubscription> subscriptions;

  private final AvlTree<Integer, ConfirmedEventLog> confirmedEvents;

  private final int nextEventId;

  @SuppressWarnings("unused")
  ExternalEventState() {
    this.nextSubscriptionId = 0;
    this.subscriptions = null;
    this.confirmedEvents = null;
    this.nextEventId = 0;
  }

  private ExternalEventState(
      int nextSubscriptionId,
      AvlTree<Integer, EvmEventSubscription> subscriptions,
      int nextEventId,
      AvlTree<Integer, ConfirmedEventLog> confirmedEvents) {
    this.nextSubscriptionId = nextSubscriptionId;
    this.subscriptions = subscriptions;
    this.confirmedEvents = confirmedEvents;
    this.nextEventId = nextEventId;
  }

  /**
   * Create new state for external events.
   *
   * @return new external event state
   */
  public static ExternalEventState create() {
    return new ExternalEventState(1, AvlTree.create(), 1, AvlTree.create());
  }

  /**
   * Get list of subscriptions.
   *
   * @return list of subscriptions
   */
  public List<EvmEventSubscription> getSubscriptions() {
    return new ArrayList<>(subscriptions.values());
  }

  /**
   * Get subscriptions raw.
   *
   * @return avl tree of subscriptions
   */
  public AvlTree<Integer, EvmEventSubscription> getSubscriptionsRaw() {
    return subscriptions;
  }

  /**
   * Get confirmed events raw.
   *
   * @return avl tree of confirmed events
   */
  public AvlTree<Integer, ConfirmedEventLog> getConfirmedEventsRaw() {
    return confirmedEvents;
  }

  /**
   * Get subscription with the provided id.
   *
   * @param subscriptionId id for subscription
   * @return subscription
   */
  public EventSubscription getSubscription(int subscriptionId) {
    return subscriptions.getValue(subscriptionId);
  }

  /**
   * Get the currently pending event for the subscription. May be null.
   *
   * @param subscriptionId to get pending event for
   * @return pending event
   */
  public PendingEventLog getPendingEventForSubscription(int subscriptionId) {
    return subscriptions.getValue(subscriptionId).getPendingEvent();
  }

  /**
   * Get all confirmed events for subscription.
   *
   * @param subscriptionId to confirmed events for
   * @return map of confirmed events
   */
  public List<ConfirmedEventLog> getConfirmedEventsForSubscription(int subscriptionId) {
    return confirmedEvents.values().stream()
        .filter(event -> event.subscriptionId() == subscriptionId)
        .toList();
  }

  /**
   * Get the latest confirmed event log for the given subscription. Returns null if no event log has
   * been confirmed yet.
   *
   * @param subscriptionId to get latest confirmed event log for
   * @return latest confirmed event log
   */
  public EventLog getLatestConfirmedForSubscription(int subscriptionId) {
    return subscriptions.getValue(subscriptionId).getLatestConfirmedEvent();
  }

  /**
   * Add a new event subscription.
   *
   * @param chainId to listen on
   * @param address of the contract to listen on
   * @param fromBlock earliest block to receive events from
   * @param topics filters for event topics
   * @return updated state with a new event subscription
   */
  public ExternalEventState addEventSubscription(
      String chainId, EvmAddress address, Unsigned256 fromBlock, FixedList<TopicList> topics) {
    EvmEventSubscription newSubscription =
        EvmEventSubscription.create(nextSubscriptionId, chainId, address, fromBlock, topics);
    AvlTree<Integer, EvmEventSubscription> updatedSubscriptions =
        subscriptions.set(nextSubscriptionId, newSubscription);
    return new ExternalEventState(
        nextSubscriptionId + 1, updatedSubscriptions, nextEventId, confirmedEvents);
  }

  /**
   * Cancel an event subscription.
   *
   * @param subscriptionId to cancel
   * @return updated state with the subscription cancelled
   */
  public ExternalEventState cancelSubscription(int subscriptionId) {
    return new ExternalEventState(
        nextSubscriptionId,
        nullSafeSubscriptionUpdate(subscriptionId, EvmEventSubscription::cancel),
        nextEventId,
        confirmedEvents);
  }

  /**
   * Remove the current pending event for the subscription.
   *
   * @param subscriptionId to remove pending event from
   * @return updated state with a pending event removed
   */
  public ExternalEventState removePendingEvent(int subscriptionId) {
    return new ExternalEventState(
        nextSubscriptionId,
        nullSafeSubscriptionUpdate(subscriptionId, EvmEventSubscription::removePendingEvent),
        nextEventId,
        confirmedEvents);
  }

  /**
   * Add a new confirmed event to the state.
   *
   * @param confirmedEvent raw eventLog of the event to add
   * @return updated state with a new confirmed event
   */
  public ExternalEventState addConfirmedEvent(ConfirmedEventLog confirmedEvent) {
    int subscriptionId = confirmedEvent.subscriptionId();
    AvlTree<Integer, EvmEventSubscription> updatedSubscriptions =
        nullSafeSubscriptionUpdate(
            subscriptionId, subscription -> subscription.addConfirmedEvent(confirmedEvent));
    AvlTree<Integer, ConfirmedEventLog> updatedConfirmedEvent =
        confirmedEvents.set(confirmedEvent.eventId(), confirmedEvent);
    return new ExternalEventState(
        nextSubscriptionId, updatedSubscriptions, nextEventId, updatedConfirmedEvent);
  }

  /**
   * Create a new pending event for the subscription.
   *
   * @param subscriptionId to create pending event for
   * @return updated state with new pending event
   */
  public ExternalEventState createPendingEvent(int subscriptionId) {
    PendingEventLog pendingEvent = PendingEventLog.create(subscriptionId, nextEventId);
    AvlTree<Integer, EvmEventSubscription> updatedSubscriptions =
        nullSafeSubscriptionUpdate(
            subscriptionId, subscription -> subscription.setPendingEvent(pendingEvent));
    return new ExternalEventState(
        nextSubscriptionId, updatedSubscriptions, nextEventId + 1, confirmedEvents);
  }

  /**
   * Update the pending event.
   *
   * @param pending event to update
   * @return updated state with new pending event
   */
  public ExternalEventState updatePendingEvent(PendingEventLog pending) {
    int subscriptionId = pending.getSubscriptionId();
    AvlTree<Integer, EvmEventSubscription> updatedSubscriptions =
        nullSafeSubscriptionUpdate(
            subscriptionId, subscription -> subscription.setPendingEvent(pending));
    return new ExternalEventState(
        nextSubscriptionId, updatedSubscriptions, nextEventId, confirmedEvents);
  }

  /**
   * Remove an external event.
   *
   * @param eventId identifier of the event to remove
   * @return updated state with removed event
   */
  public ExternalEventState removeExternalEvent(int eventId) {
    AvlTree<Integer, ConfirmedEventLog> updatedConfirmedEvents = confirmedEvents.remove(eventId);
    return new ExternalEventState(
        nextSubscriptionId, subscriptions, nextEventId, updatedConfirmedEvents);
  }

  private AvlTree<Integer, EvmEventSubscription> nullSafeSubscriptionUpdate(
      int subscriptionId, SubscriptionUpdater updater) {
    EvmEventSubscription subscription = subscriptions.getValue(subscriptionId);
    if (subscription == null) {
      return subscriptions;
    } else {
      EvmEventSubscription updatedSubscription = updater.apply(subscription);
      return subscriptions.set(subscriptionId, updatedSubscription);
    }
  }

  /**
   * Get the current pending heartbeat for a subscription. May be null.
   *
   * @param subscriptionId id of the subscription to get pending heartbeat for
   * @return pending heartbeat or null
   */
  public PendingHeartbeat getPendingHeartbeat(int subscriptionId) {
    return subscriptions.getValue(subscriptionId).getPendingHeartbeat();
  }

  /**
   * Update the pending heartbeat for a subscription.
   *
   * @param subscriptionId id of the subscription to update
   * @param pending the updated pending heartbeat
   * @return updated state with new pending heartbeat
   */
  public ExternalEventState putPendingHeartbeat(int subscriptionId, PendingHeartbeat pending) {
    AvlTree<Integer, EvmEventSubscription> updatedSubscriptions =
        nullSafeSubscriptionUpdate(
            subscriptionId, subscription -> subscription.setPendingHeartbeat(pending));
    return new ExternalEventState(
        nextSubscriptionId, updatedSubscriptions, nextEventId, confirmedEvents);
  }

  /**
   * Create a new pending heartbeat for the subscription.
   *
   * @param subscriptionId id of the subscription to create heartbeat for
   * @return updated state with new pending heartbeat
   */
  public ExternalEventState createPendingHeartbeat(int subscriptionId) {
    PendingHeartbeat pendingHeartbeat = PendingHeartbeat.create();
    AvlTree<Integer, EvmEventSubscription> updatedSubscriptions =
        nullSafeSubscriptionUpdate(
            subscriptionId, subscription -> subscription.setPendingHeartbeat(pendingHeartbeat));
    return new ExternalEventState(
        nextSubscriptionId, updatedSubscriptions, nextEventId, confirmedEvents);
  }

  /**
   * Remove the current heartbeat block, pending confirmation, for the specified subscription.
   *
   * @param subscriptionId id of the subscription to remove pending heartbeat from
   * @return updated state with heartbeat removed
   */
  public ExternalEventState removePendingHeartbeat(int subscriptionId) {
    return new ExternalEventState(
        nextSubscriptionId,
        nullSafeSubscriptionUpdate(subscriptionId, EvmEventSubscription::removePendingHeartbeat),
        nextEventId,
        confirmedEvents);
  }

  /**
   * Set a heartbeat block as the latest confirmed event for the specified subscription.
   *
   * @param subscriptionId id of the subscription to update
   * @param confirmed heartbeat block to set as confirmed
   * @return updated state with a new confirmed heartbeat block
   */
  public ExternalEventState setConfirmedHeartbeat(int subscriptionId, Unsigned256 confirmed) {
    AvlTree<Integer, EvmEventSubscription> updatedSubscriptions =
        nullSafeSubscriptionUpdate(
            subscriptionId, subscriptions -> subscriptions.setConfirmedHeartbeat(confirmed));
    return new ExternalEventState(
        nextSubscriptionId, updatedSubscriptions, nextEventId, confirmedEvents);
  }

  @FunctionalInterface
  private interface SubscriptionUpdater {
    EvmEventSubscription apply(EvmEventSubscription current);
  }
}
