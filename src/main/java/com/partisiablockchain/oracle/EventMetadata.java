package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.evm.oracle.contract.EventLog;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/** Serializable metadata for an event. Events may either be an event log or a heartbeat block */
@Immutable
public final class EventMetadata implements DataStreamSerializable, StateSerializable, EventLog {
  private final Unsigned256 logIndex;
  private final Unsigned256 transactionIndex;
  private final Unsigned256 blockNumber;

  @SuppressWarnings("unused")
  EventMetadata() {
    this.logIndex = null;
    this.transactionIndex = null;
    this.blockNumber = null;
  }

  /**
   * Default constructor for EventLogMetadata.
   *
   * @param logIndex index of the log in the transaction
   * @param transactionIndex index of the transaction in the block
   * @param blockNumber the block number
   */
  public EventMetadata(
      Unsigned256 logIndex, Unsigned256 transactionIndex, Unsigned256 blockNumber) {
    this.logIndex = logIndex;
    this.transactionIndex = transactionIndex;
    this.blockNumber = blockNumber;
  }

  static EventMetadata read(SafeDataInputStream rpc) {
    Unsigned256 logIndex = Unsigned256.read(rpc);
    Unsigned256 transactionIndex = Unsigned256.read(rpc);
    Unsigned256 blockNumber = Unsigned256.read(rpc);
    return new EventMetadata(logIndex, transactionIndex, blockNumber);
  }

  @Override
  public Unsigned256 logIndex() {
    return logIndex;
  }

  @Override
  public Unsigned256 transactionIndex() {
    return transactionIndex;
  }

  @Override
  public Unsigned256 blockNumber() {
    return blockNumber;
  }

  @Override
  public void write(SafeDataOutputStream rpc) {
    logIndex.write(rpc);
    transactionIndex.write(rpc);
    blockNumber.write(rpc);
  }

  @Override
  public String toString() {
    return "[blockNumber=%s, transactionIndex=%s, logIndex=%s]"
        .formatted(blockNumber, transactionIndex, logIndex);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof EventMetadata that)) {
      return false;
    }
    return Objects.equals(logIndex, that.logIndex)
        && Objects.equals(transactionIndex, that.transactionIndex)
        && Objects.equals(blockNumber, that.blockNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(logIndex, transactionIndex, blockNumber);
  }
}
