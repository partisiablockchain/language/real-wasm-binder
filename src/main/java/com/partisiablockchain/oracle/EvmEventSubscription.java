package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.evm.oracle.contract.EventSubscription;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.LittleEndianByteOutput;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/** A subscription for external events emitted by an external EVM chain. */
@Immutable
public final class EvmEventSubscription implements EventSubscription, StateSerializable {

  private final int id;
  private final boolean isCancelled;
  private final String chainId;
  private final EvmAddress contractAddress;
  private final Unsigned256 fromBlock;
  private final FixedList<TopicList> topics;

  /** Current event pending confirmation for this subscription. May be null. */
  private final PendingEventLog pendingEvent;

  /** A heartbeat waiting for confirmation. May be null. */
  private final PendingHeartbeat pendingHeartbeatBlock;

  /**
   * Metadata for the latest event that was confirmed for this subscription. The metadata may
   * contain only the block number if last event was a heartbeat. Latest confirmed may be null if
   * not events have been confirmed yet.
   */
  private final EventMetadata latestConfirmedEvent;

  @SuppressWarnings("unused")
  EvmEventSubscription() {
    this.id = 0;
    this.isCancelled = false;
    this.chainId = null;
    this.contractAddress = null;
    this.fromBlock = null;
    this.topics = null;
    this.pendingEvent = null;
    this.latestConfirmedEvent = null;
    this.pendingHeartbeatBlock = null;
  }

  EvmEventSubscription(
      int id,
      boolean isCancelled,
      String chainId,
      EvmAddress contractAddress,
      Unsigned256 fromBlock,
      FixedList<TopicList> topics,
      PendingEventLog pendingEvent,
      EventMetadata latestConfirmedEvent,
      PendingHeartbeat pendingHeartbeatBlock) {
    this.id = id;
    this.isCancelled = isCancelled;
    this.chainId = chainId;
    this.contractAddress = contractAddress;
    this.fromBlock = fromBlock;
    this.topics = topics;
    this.pendingEvent = pendingEvent;
    this.latestConfirmedEvent = latestConfirmedEvent;
    this.pendingHeartbeatBlock = pendingHeartbeatBlock;
  }

  /**
   * Create a new EvmEventSubscription.
   *
   * @param id for this subscription
   * @param chainId identifier for the external chain the subscription is for
   * @param contractAddress of the contract to listen for events on
   * @param fromBlock block number of the earliest block the subscription is for
   * @param topics the subscription is listening on
   * @return newly create event subscription
   */
  public static EvmEventSubscription create(
      int id,
      String chainId,
      EvmAddress contractAddress,
      Unsigned256 fromBlock,
      FixedList<TopicList> topics) {
    return new EvmEventSubscription(
        id, false, chainId, contractAddress, fromBlock, topics, null, null, null);
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public boolean isCancelled() {
    return isCancelled;
  }

  /**
   * Cancel this subscription.
   *
   * @return updated cancelled subscription
   */
  EvmEventSubscription cancel() {
    return new EvmEventSubscription(
        this.id,
        true,
        this.chainId,
        this.contractAddress,
        this.fromBlock,
        this.topics,
        this.pendingEvent,
        this.latestConfirmedEvent,
        this.pendingHeartbeatBlock);
  }

  /**
   * Remove the current pending event log from the state.
   *
   * @return updated subscription state
   */
  EvmEventSubscription removePendingEvent() {
    return new EvmEventSubscription(
        this.id,
        this.isCancelled,
        this.chainId,
        this.contractAddress,
        this.fromBlock,
        this.topics,
        null,
        this.latestConfirmedEvent,
        this.pendingHeartbeatBlock);
  }

  /**
   * Update the pending event log in the state.
   *
   * @param pendingEvent event to update
   * @return updated subscription state
   */
  EvmEventSubscription setPendingEvent(PendingEventLog pendingEvent) {
    return new EvmEventSubscription(
        this.id,
        this.isCancelled,
        this.chainId,
        this.contractAddress,
        this.fromBlock,
        this.topics,
        pendingEvent,
        this.latestConfirmedEvent,
        this.pendingHeartbeatBlock);
  }

  /**
   * SSet the latest confirmed event to be an event log confirmed by a majority of the EVM oracle
   * nodes.
   *
   * @param confirmedEvent event to set as latest confirmed
   * @return updated subscription state
   */
  EvmEventSubscription addConfirmedEvent(ConfirmedEventLog confirmedEvent) {
    return new EvmEventSubscription(
        this.id,
        this.isCancelled,
        this.chainId,
        this.contractAddress,
        this.fromBlock,
        this.topics,
        this.pendingEvent,
        confirmedEvent.getEventLog().getMetadata(),
        this.pendingHeartbeatBlock);
  }

  /**
   * Update the pending heartbeat in the state.
   *
   * @param heartbeat to update
   * @return updated subscription state
   */
  EvmEventSubscription setPendingHeartbeat(PendingHeartbeat heartbeat) {
    return new EvmEventSubscription(
        this.id,
        this.isCancelled,
        this.chainId,
        this.contractAddress,
        this.fromBlock,
        this.topics,
        this.pendingEvent,
        this.latestConfirmedEvent,
        heartbeat);
  }

  /**
   * Remove the current heartbeat block that is pending confirmation from a majority of the EVM
   * oracle nodes.
   *
   * @return updated subscription state
   */
  public EvmEventSubscription removePendingHeartbeat() {
    return new EvmEventSubscription(
        this.id,
        this.isCancelled,
        this.chainId,
        this.contractAddress,
        this.fromBlock,
        this.topics,
        this.pendingEvent,
        this.latestConfirmedEvent,
        null);
  }

  /**
   * Set the latest confirmed event to be a heartbeat block.
   *
   * @param confirmed confirmed heartbeat block
   * @return updated subscription state
   */
  public EvmEventSubscription setConfirmedHeartbeat(Unsigned256 confirmed) {
    return new EvmEventSubscription(
        this.id,
        this.isCancelled,
        this.chainId,
        this.contractAddress,
        this.fromBlock,
        this.topics,
        this.pendingEvent,
        new EventMetadata(null, null, confirmed),
        this.pendingHeartbeatBlock);
  }

  @Override
  public String getChainId() {
    return chainId;
  }

  @Override
  public byte[] contractAddress() {
    return contractAddress.asBytes();
  }

  @Override
  public Unsigned256 fromBlock() {
    return fromBlock;
  }

  @Override
  public List<List<byte[]>> topics() {
    List<List<byte[]>> result = new ArrayList<>();
    for (TopicList topic : topics) {
      result.add(topic.asBytes());
    }
    return result;
  }

  List<TopicList> rawTopics() {
    return new ArrayList<>(topics);
  }

  PendingEventLog getPendingEvent() {
    return pendingEvent;
  }

  EventMetadata getLatestConfirmedEvent() {
    return latestConfirmedEvent;
  }

  PendingHeartbeat getPendingHeartbeat() {
    return pendingHeartbeatBlock;
  }

  /**
   * Serialize into wasm byte representation of a subscription.
   *
   * @return wasm byte representation.
   */
  public byte[] serialize() {
    return LittleEndianByteOutput.serialize(
        out -> {
          out.writeI32(id);
          out.writeBoolean(isCancelled);
          out.writeString(chainId);
          out.writeBytes(contractAddress.asBytes());
          out.writeUnsignedBigInteger(new BigInteger(1, fromBlock.serialize()), 32);
          out.writeI32(topics.size());
          for (TopicList topicList : topics) {
            List<byte[]> topics = topicList.asBytes();
            out.writeI32(topics.size());
            for (byte[] bytes : topics) {
              out.writeBytes(bytes);
            }
          }
        });
  }

  /**
   * Computes the size of the subscription when serialized to bytes.
   *
   * @return size of serialized bytes.
   */
  public int serializedByteLength() {
    int size = 0;
    // Variable id
    size += 4;
    // isCancelled
    size += 1;
    // chainId
    size += 4;
    size += chainId.length();
    // EvmAddress
    size += 20;
    // From block
    size += 32;

    // Topics
    size += 4;
    for (TopicList topicList : topics) {
      size += 4;
      size += topicList.topics().size() * 32;
    }

    return size;
  }
}
