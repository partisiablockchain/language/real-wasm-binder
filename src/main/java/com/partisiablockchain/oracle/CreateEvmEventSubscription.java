package com.partisiablockchain.oracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Action for creating a new event subscription for the contract. Contains information on which
 * external chain to read from, and fields needed to build the event log filter.
 */
public record CreateEvmEventSubscription(
    String chainId, EvmAddress contractAddress, Unsigned256 fromBlock, List<TopicList> topics) {

  /**
   * Read EVM event subscription info needed for creating a new subscription in state.
   *
   * @param stream to read from
   * @return new EVM event subscription
   */
  public static CreateEvmEventSubscription read(SafeDataInputStream stream) {
    String chainId = stream.readString();
    EvmAddress contractAddress = EvmAddress.read(stream);
    Unsigned256 fromBlock = Unsigned256.read(stream);
    List<TopicList> topics = new ArrayList<>();
    int filtersCount = stream.readInt();
    for (int i = 0; i < filtersCount; i++) {
      TopicList filter = TopicList.read(stream);
      topics.add(filter);
    }
    return new CreateEvmEventSubscription(chainId, contractAddress, fromBlock, topics);
  }
}
