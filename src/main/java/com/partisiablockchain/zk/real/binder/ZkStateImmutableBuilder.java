package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.oracle.ConfirmedEventLog;
import com.partisiablockchain.oracle.CreateEvmEventSubscription;
import com.partisiablockchain.oracle.ExternalEventState;
import com.partisiablockchain.oracle.PendingEventLog;
import com.partisiablockchain.oracle.PendingHeartbeat;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/** Builder for {@link ZkStateImmutable}. */
public final class ZkStateImmutableBuilder {

  private final EngineState engines;
  private WasmRealContractState openState;
  private int nextVariableId;
  private CalculationStatus calculationStatus;
  private ComputationStateImpl computationState;
  private AvlTree<Integer, PendingInputImpl> pendingInput;
  private AvlTree<Integer, ZkClosedImpl> variables;
  private AvlTree<Integer, PendingOnChainOpenImpl> pendingOnChainOpens;

  private final BlockchainAddress preprocessContract;
  private final BlockchainAddress nodeRegistryContract;
  private FixedList<FinishedComputationImpl> finishedComputations;

  private AvlTree<PreProcessMaterialType, PreProcessMaterialState> preProcessMaterials;

  private long zkComputationDeadline;
  private int nextAttestationId;
  private AvlTree<Integer, AttestationImpl> attestations;

  private ExternalEventState externalEvents;

  private final boolean defaultOptimisticMode;

  ZkStateImmutableBuilder(ZkStateImmutable impl) {
    engines = impl.getEngines();
    openState = impl.getOpenState();
    nextVariableId = impl.getNextVariableId();
    calculationStatus = impl.getCalculationStatus();
    computationState = impl.getComputationState();
    pendingInput = impl.getPendingInput();
    variables = impl.getVariablesTree();
    pendingOnChainOpens = impl.getPendingOnChainOpensRaw();

    preProcessMaterials = impl.getAllPreProcessMaterials();

    preprocessContract = impl.getPreprocessContract();
    nodeRegistryContract = impl.getNodeRegistryContract();
    finishedComputations = impl.getFinishedComputationsRaw();

    zkComputationDeadline = impl.getComputationDeadline();
    nextAttestationId = impl.getNextAttestationId();
    attestations = impl.getAttestationsRaw();

    externalEvents = impl.getExternalEvents();

    defaultOptimisticMode = impl.getDefaultOptimisticMode();
  }

  ZkStateImmutable build() {
    return new ZkStateImmutable(
        engines,
        openState,
        nextVariableId,
        calculationStatus,
        computationState,
        pendingInput,
        variables,
        pendingOnChainOpens,
        preprocessContract,
        nodeRegistryContract,
        finishedComputations,
        zkComputationDeadline,
        nextAttestationId,
        attestations,
        preProcessMaterials,
        externalEvents,
        defaultOptimisticMode);
  }

  /**
   * Set the open state.
   *
   * @param state open state
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setOpenState(WasmRealContractState state) {
    this.openState = state;
    return this;
  }

  /**
   * Set calculation status.
   *
   * @param status calculation status
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setCalculationStatus(CalculationStatus status) {
    this.calculationStatus = status;
    return this;
  }

  /**
   * Set computation state.
   *
   * @param state computation state
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setComputationState(ComputationStateImpl state) {
    computationState = state;
    return this;
  }

  /**
   * Update computation state.
   *
   * @param updater applies update to the computation state
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder updateComputationState(
      Function<ComputationStateImpl, ComputationStateImpl> updater) {
    return setComputationState(updater.apply(computationState));
  }

  /**
   * Set pending input.
   *
   * @param input pending input
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setPendingInput(PendingInputImpl input) {
    this.pendingInput = this.pendingInput.set(input.getVariable().getId(), input);
    return this;
  }

  /**
   * Remove pending input.
   *
   * @param variableId id of pending input
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder removePendingInput(int variableId) {
    this.pendingInput = this.pendingInput.remove(variableId);
    return this;
  }

  /**
   * Set closed variable.
   *
   * @param variable closed variable
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setVariable(ZkClosedImpl variable) {
    this.variables = this.variables.set(variable.getId(), variable);
    return this;
  }

  /**
   * Remove closed variable.
   *
   * @param variableId id of closed variable
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder removeVariable(int variableId) {
    this.variables = this.variables.remove(variableId);
    return this;
  }

  /**
   * Set pending on-chain open event information from ids.
   *
   * @param ids list of variable ids
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setPendingOnChainOpenFromIds(FixedList<Integer> ids) {
    final int variableId = getAndIncrementNextVariableId();
    AvlTree<Integer, PendingOnChainOpenImpl.BitLengths> bitLengthsOfVariables = AvlTree.create();
    for (int id : ids) {
      List<Integer> bitLengths = this.variables.getValue(id).getShareBitLengths();
      bitLengthsOfVariables =
          bitLengthsOfVariables.set(
              id, new PendingOnChainOpenImpl.BitLengths(FixedList.create(bitLengths)));
    }
    this.pendingOnChainOpens =
        this.pendingOnChainOpens.set(
            variableId, new PendingOnChainOpenImpl(variableId, ids, bitLengthsOfVariables));
    return this;
  }

  /**
   * Set pending on-chain open event information.
   *
   * @param pending holds on-chain open event information
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setPendingOnChainOpen(PendingOnChainOpenImpl pending) {
    Objects.requireNonNull(pending);
    this.pendingOnChainOpens = this.pendingOnChainOpens.set(pending.getOutputId(), pending);
    return this;
  }

  /**
   * Remove pending open on-chain.
   *
   * @param outputId id of output to remove
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder removePendingOnChainOpen(int outputId) {
    this.pendingOnChainOpens = this.pendingOnChainOpens.remove(outputId);
    return this;
  }

  int getAndIncrementNextVariableId() {
    return nextVariableId++;
  }

  /**
   * Request attestation of a piece of data.
   *
   * @param dataToAttest the data to attest
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder addAttestation(byte[] dataToAttest) {
    attestations =
        attestations.set(
            nextAttestationId, AttestationImpl.create(nextAttestationId, dataToAttest));
    nextAttestationId++;
    return this;
  }

  /**
   * Update an existing attestation.
   *
   * @param id id of the attestation
   * @param attestation the attestation
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder updateAttestation(int id, AttestationImpl attestation) {
    this.attestations = attestations.set(id, attestation);
    return this;
  }

  /**
   * Add batch of preprocess material.
   *
   * @param batchType Type of preprocess material.
   * @param batchId id of batch
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder addPreProcessMaterialBatch(
      PreProcessMaterialType batchType, int batchId) {
    updatePreProcessMaterial(batchType, materialInfo -> materialInfo.addBatch(batchId));
    return this;
  }

  private PreProcessMaterialState updatePreProcessMaterial(
      PreProcessMaterialType batchType,
      Function<PreProcessMaterialState, PreProcessMaterialState> updater) {
    final PreProcessMaterialState materialInfoOrNull = preProcessMaterials.getValue(batchType);
    final PreProcessMaterialState materialInfo =
        materialInfoOrNull != null ? materialInfoOrNull : PreProcessMaterialState.EMPTY;
    final PreProcessMaterialState updatedMaterialInfo = updater.apply(materialInfo);
    if (!materialInfo.equals(updatedMaterialInfo)) {
      preProcessMaterials = preProcessMaterials.set(batchType, updatedMaterialInfo);
    }
    return materialInfo;
  }

  @CanIgnoreReturnValue
  ZkStateImmutableBuilder addFinishedComputation(
      long calculateFor, byte engineState, boolean optimistic, Hash partialOpens) {
    FinishedComputationImpl element =
        new FinishedComputationImpl(calculateFor, engineState, optimistic, partialOpens);
    if (element.isFinished()) {
      return this;
    } else {
      return setFinishedComputations(finishedComputations.addElement(element));
    }
  }

  @CanIgnoreReturnValue
  ZkStateImmutableBuilder replaceComputation(
      FinishedComputationImpl current, FinishedComputationImpl next) {
    int index = finishedComputations.indexOf(current);
    if (next.isFinished()) {
      return setFinishedComputations(finishedComputations.removeElementAtIndex(index));
    } else {
      return setFinishedComputations(finishedComputations.setElement(index, next));
    }
  }

  /**
   * Set finished computations.
   *
   * @param nextComputations next set of finished computations
   * @return builder
   */
  @CanIgnoreReturnValue
  public ZkStateImmutableBuilder setFinishedComputations(
      FixedList<FinishedComputationImpl> nextComputations) {
    finishedComputations = nextComputations;
    return this;
  }

  /**
   * Increments number of ordered batches for the given type of pre-process material.
   *
   * @param batchType Type of pre-process material
   * @param numBatchesRequested Number of batches requested.
   * @return this
   */
  @CanIgnoreReturnValue
  ZkStateImmutableBuilder incrementPreProcessBatchesOrderedInTotal(
      PreProcessMaterialType batchType, int numBatchesRequested) {
    updatePreProcessMaterial(
        batchType, materialInfo -> materialInfo.incrementOrdered(numBatchesRequested));
    return this;
  }

  /**
   * Increments number of used pre-process materials.
   *
   * @param batchType Type of pre-process material
   * @param numUsed Number of recently used materials.
   * @return the original offset.
   */
  @CanIgnoreReturnValue
  long incrementPreProcessMaterialOffset(PreProcessMaterialType batchType, long numUsed) {
    final PreProcessMaterialState prevMaterialState =
        updatePreProcessMaterial(
            batchType, materialInfo -> materialInfo.incrementNumUsedElements(numUsed));
    return prevMaterialState.numUsedElements();
  }

  /**
   * Set the number of elements the contract should attempt to prefetch of the given batch type.
   *
   * @param batchType type of preprocessed material
   * @param numElementsToPrefetch number of elements to prefetch
   */
  ZkStateImmutableBuilder setPreprocessMaterialToPrefetch(
      PreProcessMaterialType batchType, int numElementsToPrefetch) {
    updatePreProcessMaterial(
        batchType, materialInfo -> materialInfo.setNumElementsToPrefetch(numElementsToPrefetch));
    return this;
  }

  void extendComputationDeadline(long extension) {
    this.zkComputationDeadline += extension;
  }

  ZkStateImmutableBuilder subscribeToEvents(CreateEvmEventSubscription subscription) {
    externalEvents =
        externalEvents.addEventSubscription(
            subscription.chainId(),
            subscription.contractAddress(),
            subscription.fromBlock(),
            FixedList.create(subscription.topics()));
    return this;
  }

  ZkStateImmutableBuilder cancelSubscription(int subscriptionId) {
    externalEvents = externalEvents.cancelSubscription(subscriptionId);
    return this;
  }

  ZkStateImmutableBuilder removePendingEvent(int subscriptionId) {
    externalEvents = externalEvents.removePendingEvent(subscriptionId);
    return this;
  }

  ZkStateImmutableBuilder confirmEvent(ConfirmedEventLog confirmedEvent) {
    externalEvents = externalEvents.addConfirmedEvent(confirmedEvent);
    return this;
  }

  ZkStateImmutableBuilder createPendingEvent(int subscriptionId) {
    externalEvents = externalEvents.createPendingEvent(subscriptionId);
    return this;
  }

  ZkStateImmutableBuilder setPendingEvent(PendingEventLog pendingEvent) {
    externalEvents = externalEvents.updatePendingEvent(pendingEvent);
    return this;
  }

  ZkStateImmutableBuilder removeExternalEvent(int eventId) {
    externalEvents = externalEvents.removeExternalEvent(eventId);
    return this;
  }

  /**
   * Set a pending heartbeat on the subscription.
   *
   * @param subscriptionId id of the subscription to update
   * @param pending heartbeat to put on the subscription
   * @return builder
   */
  public ZkStateImmutableBuilder setPendingHeartbeatBlock(
      int subscriptionId, PendingHeartbeat pending) {
    externalEvents = externalEvents.putPendingHeartbeat(subscriptionId, pending);
    return this;
  }

  /**
   * Create a new pending heartbeat on the subscription.
   *
   * @param subscriptionId id of the subscription to update
   * @return builder
   */
  public ZkStateImmutableBuilder createPendingHeartbeat(int subscriptionId) {
    externalEvents = externalEvents.createPendingHeartbeat(subscriptionId);
    return this;
  }

  /**
   * Remove the pending heartbeat from a subscription.
   *
   * @param subscriptionId id of the subscription to update
   * @return builder
   */
  public ZkStateImmutableBuilder removePendingHeartbeat(int subscriptionId) {
    externalEvents = externalEvents.removePendingHeartbeat(subscriptionId);
    return this;
  }

  /**
   * Set a heartbeat as the last confirmed event on a subscription.
   *
   * @param subscriptionId id of the subscription to update
   * @param confirmed heartbeat block number
   * @return builder
   */
  public ZkStateImmutableBuilder confirmHeartbeat(int subscriptionId, Unsigned256 confirmed) {
    externalEvents = externalEvents.setConfirmedHeartbeat(subscriptionId, confirmed);
    return this;
  }
}
