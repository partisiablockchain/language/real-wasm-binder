package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.crypto.Hash;
import com.secata.tools.immutable.FixedList;
import java.util.Collections;
import java.util.List;

/**
 * Extended {@link ZkBinderContext} with the methods in {@link ServicePayerWithGas}, allowing it to
 * keep track of fees paid by contract caller.
 *
 * <p>Most calls are delegated directly to the inner {@link ZkBinderContext} given by the
 * blockchain.
 */
public final class ZkBinderContextImpl implements ZkBinderContextWithGas {
  private final ZkBinderContext zkBinderContext;
  private long registeredCallerServiceFees;
  private long registeredCpuFees;

  /**
   * Creates a new binder context to be used in place of the supplied {@link ZkBinderContext}.
   *
   * @param zkBinderContext the inner context to delegate method calls to
   */
  public ZkBinderContextImpl(ZkBinderContext zkBinderContext) {
    this.zkBinderContext = zkBinderContext;
    this.registeredCallerServiceFees = 0;
    this.registeredCpuFees = 0;
  }

  @Override
  public long availableGas() {
    return zkBinderContext.availableGas() - registeredCallerServiceFees - registeredCpuFees;
  }

  @Override
  public void registerCpuFee(long fees) {
    if (availableGas() < fees) {
      String errorMessage =
          String.format(
              "Registered CPU fees cannot exceed available gas (%d > %d)", fees, availableGas());
      throw new IllegalArgumentException(errorMessage);
    }
    zkBinderContext.registerCpuFee(fees);
    registeredCpuFees = fees;
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return zkBinderContext.getContractAddress();
  }

  @Override
  public long getBlockTime() {
    return zkBinderContext.getBlockTime();
  }

  @Override
  public long getBlockProductionTime() {
    return zkBinderContext.getBlockProductionTime();
  }

  @Override
  public BlockchainAddress getFrom() {
    return zkBinderContext.getFrom();
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return zkBinderContext.getCurrentTransactionHash();
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return zkBinderContext.getOriginalTransactionHash();
  }

  @Override
  public void payFromCaller(long fees, List<BlockchainAddress> receivers) {
    if (availableGas() < fees) {
      String errorMessage =
          String.format(
              "Registered service fees cannot exceed available gas (%d > %d)",
              fees, availableGas());
      throw new IllegalArgumentException(errorMessage);
    }
    zkBinderContext.payServiceFees(
        fees,
        FixedList.create(receivers),
        FixedList.create(Collections.nCopies(receivers.size(), 1)));
    registeredCallerServiceFees += fees;
  }

  @Override
  public void payFromContract(long fees, List<BlockchainAddress> receivers) {
    zkBinderContext.payServiceFees(
        fees,
        FixedList.create(receivers),
        FixedList.create(Collections.nCopies(receivers.size(), 1)));
  }

  @Override
  public void payServiceFees(long gas, BlockchainAddress target) {
    throw new RuntimeException("Unused. Use payFromCaller or payFromContract instead.");
  }
}
