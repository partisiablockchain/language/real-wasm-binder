package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

@Immutable
final class OpenId implements StateSerializable {

  private final byte receivedForEngine;
  private final int id;

  @SuppressWarnings("unused")
  OpenId() {
    receivedForEngine = -1;
    id = -1;
  }

  OpenId(int id) {
    receivedForEngine = 0;
    this.id = id;
  }

  private OpenId(int id, byte receivedForEngine) {
    this.id = id;
    this.receivedForEngine = receivedForEngine;
  }

  public int getReceivedForEngine() {
    return receivedForEngine;
  }

  public int getId() {
    return id;
  }

  OpenId withReceivedForEngine(int engineIndex) {
    return new OpenId(id, EnginesStatus.setEngineBit(engineIndex, receivedForEngine));
  }
}
