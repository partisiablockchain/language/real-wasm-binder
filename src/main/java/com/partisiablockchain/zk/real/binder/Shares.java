package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;

/** Shares in the contract when sending input. */
@Immutable
public final class Shares implements StateSerializable {

  private final FixedList<LargeByteArray> shares;

  Shares() {
    shares = FixedList.create();
  }

  Shares(FixedList<LargeByteArray> shares) {
    this.shares = shares;
  }

  /**
   * Get raw shares.
   *
   * @return list of raw shares
   */
  public FixedList<LargeByteArray> getShares() {
    return shares;
  }

  interface ShareReader {
    byte[] read(SafeDataInputStream stream, PendingOnChainOpenImpl.BitLengths bitLengths);
  }

  Shares withShares(
      PendingOnChainOpenImpl.BitLengths bitLengths,
      int engine,
      SafeDataInputStream stream,
      ShareReader reader) {
    LargeByteArray shares = new LargeByteArray(reader.read(stream, bitLengths));
    return new Shares(RealContractBinder.BYTE_LIST.withEntry(this.shares, engine, shares));
  }
}
