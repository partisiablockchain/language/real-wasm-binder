package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.RealEnvironment;
import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;

/**
 * Primary interface of a REAL protocol instantiation.
 *
 * @param <NumberT> the representation of a secret-shared number.
 * @param <BitT> the representation of a secret-shared bit.
 */
public record RealEnvironmentImpl<
        NumberT extends SharedNumber<NumberT>, BitT extends SharedBit<BitT>>(
    SharedNumberStorage<NumberT> storage, ElementMultiplier<BitT> multiplier)
    implements RealEnvironment<NumberT, BitT> {}
