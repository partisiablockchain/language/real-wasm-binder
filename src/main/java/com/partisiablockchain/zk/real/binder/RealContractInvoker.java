package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.contract.RealContract;
import com.secata.stream.SafeDataInputStream;
import java.util.List;

/** Bridge between {@link RealContractBinder} itself, and the underlying REAL contract. */
public interface RealContractInvoker {

  /**
   * Called when the REAL contract is created.
   *
   * @param context the context for this execution
   * @param zkState the zero knowledge state
   * @param rpc the input data in this transaction
   * @return initial state for the contract
   */
  WasmStateAndEvents onCreate(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final SafeDataInputStream rpc);

  /**
   * Called when the REAL contract is called with an RPC call.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param rpc the input data in this transaction
   * @return updated state for the contract
   */
  WasmStateAndEvents onOpenInput(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final SafeDataInputStream rpc);

  /**
   * Called when the REAL contract is called with an RPC callback.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state state for the contract
   * @param callbackContext the spawned event transactions and their execution result
   * @param rpc the invocation data registered with the callback
   * @return updated state for the contract
   */
  WasmStateAndEvents onCallback(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final CallbackContext callbackContext,
      final SafeDataInputStream rpc);

  /**
   * Called when the REAL contract is called with a secret input.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param rpc the input data in this transaction
   * @return updated state for the contract
   */
  WasmStateAndEventsAndVariableDefinition onSecretInput(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final SafeDataInputStream rpc);

  /**
   * Automatically called when one of the REAL contract's secret inputs is confirmed.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param variableId the variable id now inputted
   * @param onVariableInputtedShortname the shortname of the {@code zk_on_variable_inputted}
   *     function to call.
   * @return updated state for the contract
   */
  WasmStateAndEvents onVariableInputted(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final int variableId,
      final byte[] onVariableInputtedShortname);

  /**
   * Automatically called when one of the REAL contract's secret inputs is rejected.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param variableId the variable id now inputted
   * @return updated state for the contract
   */
  WasmStateAndEvents onVariableRejected(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final int variableId);

  /**
   * Automatically called when the REAL contract's computation is finished.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param createdVariables the variable id now inputted
   * @param onComputeCompleteShortname the shortname of the {@code zk_on_compute_complete} function
   *     to invoke. Should be Null if no such function was specified.
   * @return updated state for the contract
   */
  WasmStateAndEvents onComputeComplete(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final List<Integer> createdVariables,
      final byte[] onComputeCompleteShortname);

  /**
   * Automatically called one or more of the REAL contract's secret variables are opened.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param openedVariables the variable ids which have been opened
   * @return updated state for the contract
   */
  WasmStateAndEvents onVariablesOpened(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final List<Integer> openedVariables);

  /**
   * Called when an attestation on a previously requested piece of data becomes valid.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param attestationId the ID of the attestation that is now valid
   * @return updated state for the contract
   */
  WasmStateAndEvents onAttestationComplete(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      Integer attestationId);

  /**
   * Called when external events have been confirmed by a majority of the EVM oracles.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param subscriptionId the ID of the subscription the event is for
   * @param eventId the ID of the event that has been confirmed for the subscription
   * @return updated state for the contract
   */
  WasmStateAndEvents onExternalEvents(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final int subscriptionId,
      final int eventId);

  /**
   * Returns the wrapped contract.
   *
   * @return wrapped contract
   */
  RealContract<WasmRealContractState, LargeByteArray> getContract();

  /**
   * The new wasm state resulting from a wasm invocation, as well as an {@link EventResult}
   * containing spawned events.
   *
   * @param wasmState the wasm state resulting from an invocation
   * @param eventResult contains any return value, callback and/or spawned events
   */
  record WasmStateAndEvents(WasmRealContractState wasmState, EventResult eventResult) {}

  /**
   * The new wasm state resulting from a wasm invocation, as well as an {@link EventResult}
   * containing spawned events.
   *
   * @param wasmState the wasm state resulting from an invocation
   * @param eventResult contains any return value, callback and/or spawned events
   * @param variableDefinition Input variable definition
   */
  record WasmStateAndEventsAndVariableDefinition(
      WasmRealContractState wasmState,
      EventResult eventResult,
      VariableDefinition variableDefinition) {}

  /**
   * Internal definition information for a variable, parsed from a Zk VarDef {@code
   * WasmInvoker.WasmResultSection}.
   *
   * @param bitLengths Bit-lengths expected by the contract.
   * @param seal Whether the variable should be sealed or not.
   * @param onInputtedShortname Shortname of the {@code zk_on_variable_inputted} function to call
   *     when variable is input.
   * @param metadata Contract-defined data associated with the variable.
   */
  @SuppressWarnings("ArrayRecordComponent")
  record VariableDefinition(
      List<Integer> bitLengths,
      boolean seal,
      LargeByteArray onInputtedShortname,
      byte[] metadata) {}
}
