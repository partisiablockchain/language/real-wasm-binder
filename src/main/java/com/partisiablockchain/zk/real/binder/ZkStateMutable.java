package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.contract.zk.ZkComputationState;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.evm.oracle.contract.EventLog;
import com.partisiablockchain.evm.oracle.contract.EventSubscription;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.oracle.ConfirmedEventLog;
import com.partisiablockchain.oracle.CreateEvmEventSubscription;
import com.partisiablockchain.oracle.EventMetadata;
import com.partisiablockchain.oracle.EvmEventLog;
import com.partisiablockchain.oracle.ExternalEventState;
import com.partisiablockchain.oracle.PendingEventLog;
import com.partisiablockchain.oracle.PendingHeartbeat;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.binder.ComputationStateImpl.ComputationResult;
import com.partisiablockchain.zk.real.binder.counter.CountingMultiplier;
import com.partisiablockchain.zk.real.binder.counter.CountingShareStorage;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.contract.RealNodeState.PendingInput;
import com.partisiablockchain.zk.real.protocol.RealEnvironment;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElementFactory;
import com.partisiablockchain.zk.real.protocol.field.Lagrange;
import com.partisiablockchain.zk.real.protocol.field.Polynomial;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Mutable version of {@link ZkStateImmutable}, used to modify {@link RealContractBinder} state.
 *
 * <p>Intermediate between instances of {@link ZkStateImmutable}.
 */
public final class ZkStateMutable {

  /**
   * A constant amount of gas that should be distributed to the computation nodes on inputs,
   * attestations and computation starts, to cover their transactions fees. Each node will pay 5,000
   * gas per transaction (currently hardcoded in the "main" project). The fee constant should cover
   * the cost for 4 nodes with some extra to spare.
   */
  public static final long BASE_SERVICE_FEES = 25_000L;

  /** Gas cost of sending a single byte over the network. */
  public static final long NETWORK_FEE_PER_BYTE = 5L;

  /**
   * In addition to the gas cost required for the zk nodes to cover their transaction, they are
   * given some extra payment for their work.
   */
  public static final long EXTRA_SERVICE_FEES = 5000L;

  /**
   * When calculating the gas cost of a transaction add some extra to account for small future
   * changes. The nodes also send this extra, which means it ends back in the contract.
   */
  public static final long EXTRA_TRANSACTION_COST = 500L;

  /** Number of zk nodes. */
  public static final long NUMBER_OF_NODES = 4L;

  /** Max number of malicious nodes. */
  public static final int MALICIOUS_COUNT = 1;

  /**
   * Set the number of elements the contract should attempt to prefetch of the given batch type.
   *
   * @param batchType type of preprocessed material
   * @param numElementsToPrefetch number of elements to prefetch
   */
  public void setPreprocessMaterialToPrefetch(
      PreProcessMaterialType batchType, int numElementsToPrefetch) {
    state =
        state.builder().setPreprocessMaterialToPrefetch(batchType, numElementsToPrefetch).build();
  }

  /**
   * Information relating the current pending ZK computation.
   *
   * @param outputVariableMetadata Public metadata for the output variable.
   * @param onCompleteShortname Shortname of the {@code zk_on_compute_complete} function to invoke,
   *     when the computation finishes. Null if no such function was specified.
   */
  private record InfoAboutPendingComputation(
      List<LargeByteArray> outputVariableMetadata, LargeByteArray onCompleteShortname) {}

  private final BinaryExtensionFieldElementFactory factory;
  private final ZkBinderContextWithGas context;
  private InfoAboutPendingComputation infoAboutPendingComputation;
  private int confirmedInputs;
  private ZkStateImmutable state;
  private boolean calculationIsTerminating;

  static int shareCountFromLengths(Collection<Integer> bitLengths) {
    return bitLengths.stream().reduce(0, Integer::sum);
  }

  private ZkStateMutable(
      final ZkStateImmutable state,
      ZkBinderContextWithGas context,
      BinaryExtensionFieldElementFactory factory,
      int confirmedInputs) {
    this.state = state;
    this.context = context;
    this.confirmedInputs = confirmedInputs;
    this.factory = factory;
    this.calculationIsTerminating = false;
  }

  /**
   * Constructor method of creating a new {@link ZkStateMutable} with binary defaults.
   *
   * @param state inner immutable state
   * @param context contract context
   * @return new ZkStateMutable
   */
  public static ZkStateMutable createMutableStateBinary(
      ZkStateImmutable state, ZkBinderContextWithGas context) {
    int confirmedInputs = getNumberOfConfirmedInputs(state);
    BinaryExtensionFieldElementFactory factory = new BinaryExtensionFieldElementFactory();
    return new ZkStateMutable(state, context, factory, confirmedInputs);
  }

  private static int getNumberOfConfirmedInputs(ZkStateImmutable state) {
    // for binary real, each input encodes 1 bit, so the number of confirmed inputs is the sum of
    // all bit-lengths of variables which have a blindedInput (this field is only set on inputs
    // which have been confirmed).
    int confirmed = 0;
    for (RealClosed<LargeByteArray> variable : state.getVariables()) {
      if (variable.getBlindedInputs() != null) {
        Integer count = shareCountFromLengths(variable.getShareBitLengths());
        confirmed += count;
      }
    }
    return confirmed;
  }

  int getConfirmedInputs() {
    return confirmedInputs;
  }

  /**
   * Mutate state via builder.
   *
   * @param mutator accepts builder
   */
  public void mutate(final Consumer<ZkStateImmutableBuilder> mutator) {
    ZkStateImmutableBuilder builder = state.builder();
    mutator.accept(builder);
    state = builder.build();
  }

  /**
   * Get immutable state.
   *
   * @return immutable state
   */
  public ZkStateImmutable getState() {
    return state;
  }

  /**
   * Finalize the current {@link ZkStateMutable}. Will apply any pending computation states and try
   * to prefetch preprocess material.
   *
   * @param computation the computation of the contract
   * @param preprocessMaterialRequester used to request preprocessing material, if needed
   * @param notifier used to notify that the computation is done, if needed
   * @return the updated state
   */
  public ZkStateImmutable finalizeInteraction(
      RealComputationEnv<WasmRealContractState, LargeByteArray> computation,
      PreprocessedMaterialRequester preprocessMaterialRequester,
      ComputationDoneNotifier notifier) {
    if (computationIsAboutToStart()) {
      initializeComputationState(computation);
    }

    boolean computationIsWaitingForTriples =
        this.state.getComputationState() != null
            && this.state.getComputationState().isWaitingForTriples();
    if (computationIsWaitingForTriples) {
      requestPreProcessMaterialIfNeeded(
          preprocessMaterialRequester,
          PreProcessMaterialType.BINARY_TRIPLE,
          this.state.getComputationState().getMultiplicationCount());
      assignTriplesToComputationIfReceivedEnoughTriples();
    }

    requestPreProcessMaterialPrefetch(
        preprocessMaterialRequester, PreProcessMaterialType.BINARY_INPUT_MASK);
    requestPreProcessMaterialPrefetch(
        preprocessMaterialRequester, PreProcessMaterialType.BINARY_TRIPLE);

    computationDoneNotifierIfNeeded(notifier);
    return getState();
  }

  private void computationDoneNotifierIfNeeded(ComputationDoneNotifier notifier) {
    if (calculationIsTerminating) {
      if (state.getCalculationStatus() == CalculationStatus.MALICIOUS_BEHAVIOUR) {
        notifier.notifyComputationDone(EnginesStatus.ALL_FAILURE);
      } else {
        notifier.notifyComputationDone(EnginesStatus.ALL_SUCCESS);
      }
    } else if (computationDeadlineReached()) {
      EnginesStatus enginesStatus = calculateComputationStatuses();
      notifier.notifyComputationDone(enginesStatus);
      state = state.builder().setCalculationStatus(CalculationStatus.DONE).build();
    }
  }

  /**
   * Calculate the total engine status based on the state.
   *
   * <p>The following fields are taken into account, if an engine has a failure in any of them the
   * result is a failure for that node.
   *
   * <ul>
   *   <li>Pending on-chain inputs.
   *   <li>Pending on-chain opens.
   *   <li>Attestations.
   *   <li>Finished computations.
   *   <li>Whether there is an ongoing computation. If so everyone gets a failure
   * </ul>
   *
   * @return the total status for each node
   */
  private EnginesStatus calculateComputationStatuses() {
    if (state.getComputationState() != null) {
      return EnginesStatus.ALL_FAILURE;
    }

    List<EnginesStatus> pendingInputStatuses =
        state.getPendingInput().values().stream()
            .filter(PendingInputImpl::isOnChainInput)
            .map(PendingInputImpl::getEnginesStatus)
            .toList();
    List<EnginesStatus> pendingOnChainOpenStatuses =
        state.getPendingOnChainOpensRaw().values().stream()
            .map(PendingOnChainOpenImpl::getEnginesStatus)
            .toList();
    List<EnginesStatus> attestationStatuses =
        state.getAttestationsRaw().values().stream()
            .map(AttestationImpl::getEnginesStatus)
            .toList();
    List<EnginesStatus> finishedComputationsStatuses =
        state.getFinishedComputationsRaw().stream()
            .map(FinishedComputationImpl::getEnginesStatus)
            .toList();

    return EnginesStatus.ALL_SUCCESS
        .andAll(pendingInputStatuses)
        .andAll(pendingOnChainOpenStatuses)
        .andAll(attestationStatuses)
        .andAll(finishedComputationsStatuses);
  }

  private boolean computationIsAboutToStart() {
    return this.infoAboutPendingComputation != null;
  }

  private void initializeComputationState(
      RealComputationEnv<WasmRealContractState, LargeByteArray> computation) {
    final List<LargeByteArray> variableMetadata =
        infoAboutPendingComputation.outputVariableMetadata();

    OutputDefinition outputDefinition =
        determineComputationOutput(new ComputationStateFromMutable(state), computation);
    if (variableMetadata.size() != outputDefinition.outputSizes.size()) {
      throw new IllegalStateException(
          "Variable count mismatch. Contract declared metadata for "
              + "%d variables, but computation produced %d variables"
                  .formatted(variableMetadata.size(), outputDefinition.outputSizes.size()));
    }
    final ZkStateImmutableBuilder builder = state.builder();
    List<PendingOutputVariable> pending = new ArrayList<>();
    for (int i = 0; i < variableMetadata.size(); i++) {
      LargeByteArray information = variableMetadata.get(i);
      int bitLength = outputDefinition.outputSizes.get(i);
      pending.add(
          new PendingOutputVariable(
              builder.getAndIncrementNextVariableId(), information, bitLength));
    }
    int multiplications = outputDefinition.multiplications;
    ComputationStateImpl computationState =
        new ComputationStateImpl(
            state.getDefaultOptimisticMode(),
            multiplications,
            confirmedInputs,
            context.getBlockTime(),
            this.state.getOpenState(),
            this.state.getVariablesTree(),
            new ComputationOutput(FixedList.create(pending)),
            infoAboutPendingComputation.onCompleteShortname());

    long multiplicationCount = computationState.getMultiplicationCount();
    payComputationFees(multiplicationCount);

    builder.setComputationState(computationState);
    this.state = builder.build();
  }

  /**
   * Pays computation and service fees from the contract to the zk computation nodes.
   *
   * @param multiplicationCount the number of multiplications in the computation
   */
  private void payComputationFees(long multiplicationCount) {
    List<BlockchainAddress> engineIdentities = state.getEngineIdentities();

    long computationFees = calculateComputationFees(multiplicationCount);

    context.payFromContract(computationFees, engineIdentities);
  }

  /**
   * Calculates the fees associated with a zk computation. The fees consist of fees for the number
   * of multiplications in the computation and service fees for the transactions that the zk
   * computation nodes must make.
   *
   * <p>The computation fees are 5 USD cent per 1000 multiplications.
   *
   * <p>The service fees should cover the following for the real nodes:
   *
   * <ul>
   *   <li>1 transaction to notify that the computation is complete.
   *   <li>1 possible repeat of this transaction if the optimistic computation fails.
   * </ul>
   *
   * @param multiplicationCount the number of multiplications in the computation
   * @return the fees for the zk computation
   */
  public static long calculateComputationFees(long multiplicationCount) {
    // 1 USD cent = 1000 gas
    // Multiplication fees = (multiplicationCount / 1000) * 5 * 1000 = multiplicationCount * 5
    long multiplicationFees = multiplicationCount * 5;

    long computationTransactionFees = BASE_SERVICE_FEES * 2;

    return multiplicationFees + computationTransactionFees;
  }

  /**
   * Checks whether the contract have received enough triples to be able to assign triples to the
   * computation and start it.
   *
   * <p>Invariant: Assumes that computation state is non-null.
   */
  private void assignTriplesToComputationIfReceivedEnoughTriples() {
    final ComputationStateImpl computationState = state.getComputationState();
    final PreProcessMaterialType batchType = PreProcessMaterialType.BINARY_TRIPLE;
    final PreProcessMaterialState tripleState = state.getPreProcessMaterials(batchType);
    final BatchInfoForComputation tripleBatches =
        batchInfoForComputation(computationState.getMultiplicationCount(), batchType, tripleState);

    if (tripleState.batchIds().size() >= tripleBatches.batchIdxAfterLast()) {

      final FixedList<Integer> batchesForComputation =
          tripleState
              .batchIds()
              .subList(tripleBatches.batchIdxStart(), tripleBatches.batchIdxAfterLast());

      final var builder =
          state
              .builder()
              .setComputationState(
                  computationState.withTriples(
                      context.getBlockTime(),
                      batchesForComputation,
                      tripleBatches.offsetIntoStartBatch()));
      builder.incrementPreProcessMaterialOffset(
          PreProcessMaterialType.BINARY_TRIPLE, computationState.getMultiplicationCount());

      this.state = builder.build();
    }
  }

  /**
   * Retrieve shortname of the {@code zk_on_compute_complete} function the current computation state
   * is linked to.
   *
   * @return byte representation of compute complete function shortname of the current computation.
   *     Null if no function is linked, or there is no computation ongoing.
   */
  public LargeByteArray getComputeCompleteShortname() {
    return this.state.getComputationState().getOnComputeCompleteShortname();
  }

  /**
   * Contains information on how triple batches should be split for use in computations.
   *
   * @param batchIdxStart The index of the first batch id for the computation, index inclusive.
   * @param batchIdxAfterLast The index of the last batch id for the computation, index exclusive.
   * @param offsetIntoStartBatch The index of the first triple to be used.
   */
  record BatchInfoForComputation(
      int batchIdxStart, int batchIdxAfterLast, int offsetIntoStartBatch) {}

  /**
   * Determines what batches should be used by a computation.
   *
   * @param neededElements Number of triples needed for the computation.
   * @param materialState The triple material state.
   * @return The batch split info.
   */
  static BatchInfoForComputation batchInfoForComputation(
      final long neededElements,
      final PreProcessMaterialType batchType,
      final PreProcessMaterialState materialState) {
    final long batchSize = batchType.getBatchSize();

    final long numElementsAlreadyUsed = materialState.numUsedElements();
    final long batchIdxStart = numElementsAlreadyUsed / batchSize;
    final long batchIdxAfterLast =
        divisionRoundUp(neededElements + numElementsAlreadyUsed, batchSize);
    final long offsetIntoStartBatch = numElementsAlreadyUsed - batchIdxStart * batchSize;

    return new BatchInfoForComputation(
        (int) batchIdxStart, (int) batchIdxAfterLast, (int) offsetIntoStartBatch);
  }

  /**
   * Determines how many batches should be requested.
   *
   * @param neededElements Amount of material needed currently.
   * @param state Contract state.
   * @param batchType The pre-process material batch type.
   * @return How many batches to request.
   */
  private static int numBatchesToRequest(
      long neededElements, final ZkStateImmutable state, PreProcessMaterialType batchType) {
    final PreProcessMaterialState materialState = state.getPreProcessMaterials(batchType);
    final long batchSize = batchType.getBatchSize();
    final int numBatchesOrderedSoFar = materialState.numRequestedBatches();
    final long numMaterialOrderedSoFar = numBatchesOrderedSoFar * batchSize;
    final long numMaterialToRequest =
        materialState.numUsedElements() + neededElements - numMaterialOrderedSoFar;
    final long numBatchesToRequest = divisionRoundUp(numMaterialToRequest, batchSize);
    return numMaterialToRequest >= 1L ? (int) numBatchesToRequest : 0;
  }

  /**
   * Divide {@code dividend} with {@code divisor}, rounding up.
   *
   * @param dividend Dividend.
   * @param divisor Divisor.
   * @return Divided, rounding up.
   */
  private static long divisionRoundUp(long dividend, long divisor) {
    return dividend == 0L ? 0L : 1L + (dividend - 1L) / divisor;
  }

  /**
   * Set open state on contract.
   *
   * @param open next state
   */
  public void setOpen(WasmRealContractState open) {
    mutate(builder -> builder.setOpenState(open));
  }

  /**
   * Gets the current calculation status for this zero knowledge computation.
   *
   * @return current calculation status
   */
  public CalculationStatus getCalculationStatus() {
    return state.getCalculationStatus();
  }

  /**
   * Gets every variable in the smart contract.
   *
   * @return the list of variables
   */
  public List<RealClosed<LargeByteArray>> getVariables() {
    return state.getVariables();
  }

  /**
   * Gets a variable with a specific id in the smart contract.
   *
   * @param id the id to lookup
   * @return the variable
   */
  public ZkClosedImpl getVariable(int id) {
    return state.getVariable(id);
  }

  /**
   * Gets a pending input with a specific id in the smart contract.
   *
   * @param id the id to lookup
   * @return the pending input
   */
  public RealClosed<LargeByteArray> getPendingInput(int id) {
    return state.getPendingInput().getValue(id).getVariable();
  }

  /**
   * Gets every pending (and hence unconfirmed) closed variable. These variables must be confirmed
   * to be used in the computation, this requires interaction by the nodes.
   *
   * @return the list of variables
   */
  public List<RealClosed<LargeByteArray>> getPendingInputs() {
    return state.getPendingInputs().stream()
        .map(PendingInput::getVariable)
        .collect(Collectors.toList());
  }

  /**
   * Gets every pending (and hence unconfirmed) closed variable for a specific account. These
   * variables must be confirmed to be used in the computation, this requires interaction by the
   * nodes.
   *
   * @param account the account
   * @return the list of variables
   */
  public List<RealClosed<LargeByteArray>> getPendingInputs(BlockchainAddress account) {
    return getPendingInputs().stream()
        .filter((variable) -> account.equals(variable.getOwner()))
        .collect(Collectors.toUnmodifiableList());
  }

  /**
   * Starts the computation for this zero knowledge computation, updates calculation status and
   * calculate for. NOTE: The list of information has the same size as the number of created output
   * variables; if you need three variables as a result, then make sure you add three variables
   * here.
   *
   * @param informationList the information that will be attached to the output variables.
   * @param onComputeCompleteShortname the {@code zk_on_compute_complete} shortname to invoke when
   *     the computation finishes.
   */
  public void startComputation(
      List<LargeByteArray> informationList, LargeByteArray onComputeCompleteShortname) {
    assertInWaitingState();
    assertComputationDeadlineNotReached();
    this.infoAboutPendingComputation =
        new InfoAboutPendingComputation(informationList, onComputeCompleteShortname);
    state = state.builder().setCalculationStatus(CalculationStatus.CALCULATING).build();
  }

  /** Signals that this contract has ended and have no further interactions. */
  public void contractDone() {
    assertInWaitingState();
    state = state.builder().setCalculationStatus(CalculationStatus.DONE).build();
    this.calculationIsTerminating = true;
  }

  /**
   * Opens variables on chain, this allows conditional reveals of any output variable.
   *
   * @param ids the variable ids to output.
   */
  public void openVariables(List<Integer> ids) {
    assertNotInTerminalState();
    assertVariablesExist(ids);

    // Service fees covering the transaction a single node must make
    long singleNodeGasCost = computeGasCost(calculateRpcLength(ids)) + EXTRA_TRANSACTION_COST;
    // Service fees for four nodes plus some extra payment
    long serviceFees = NUMBER_OF_NODES * singleNodeGasCost + EXTRA_SERVICE_FEES;
    context.payFromContract(serviceFees, state.getEngineIdentities());
    state = state.builder().setPendingOnChainOpenFromIds(FixedList.create(ids)).build();
  }

  /**
   * Calculate the length of the rpc for an open on chain variables invocation.
   *
   * @param ids List of ids to open
   * @return rpc length
   */
  private int calculateRpcLength(List<Integer> ids) {
    int bitLengths =
        ids.stream()
            .flatMap(id -> state.getVariablesTree().getValue(id).getShareBitLengths().stream())
            .reduce(0, Integer::sum);
    return bitLengths + 5;
  }

  /**
   * Compute the gas cost of a zk node invocation towards the binder.
   *
   * <ul>
   *   <li>A signed transaction sends 114 + rpcLength bytes.
   *   <li>An interactContract event sends 26 + rpcLength bytes.
   *   <li>Gas cost is equal to 5 * bytes sent.
   * </ul>
   *
   * <p>Total gas cost for an invocation with no spawned events is therefore 700 + 10 * rpcLength.
   *
   * @param rpcLength length of the rpc
   * @return gas cost of the invocation
   */
  private long computeGasCost(int rpcLength) {
    int signedTransactionBytes = 114 + rpcLength;
    int interactContractEventBytes = 26 + rpcLength;
    int totalNetworkBytes = signedTransactionBytes + interactContractEventBytes;

    return NETWORK_FEE_PER_BYTE * totalNetworkBytes;
  }

  /**
   * Transfers ownership of a specific variable.
   *
   * @param id the variable id
   * @param newOwner the new owner for the variable.
   */
  public void transferVariable(int id, BlockchainAddress newOwner) {
    ZkClosedImpl closed = state.getVariablesTree().getValue(id);
    state = state.builder().setVariable(closed.withOwner(newOwner)).build();
  }

  /**
   * Deletes a variable owned by the current user.
   *
   * @param id the id to delete.
   */
  public void deleteVariable(int id) {
    state = state.builder().removeVariable(id).build();
  }

  /**
   * Deletes a pending input owned by the current user.
   *
   * @param id the id to delete.
   */
  public void deletePendingInput(int id) {
    state = state.builder().removePendingInput(id).build();
  }

  private void assertInWaitingState() {
    if (this.state.getCalculationStatus() != CalculationStatus.WAITING) {
      throw new IllegalStateException(
          "Unable to start computation or move to done when not in state WAITING. Current state: %s"
              .formatted(this.state.getCalculationStatus()));
    }
  }

  private void assertNotInTerminalState() {
    CalculationStatus status = this.state.getCalculationStatus();
    if (status == CalculationStatus.DONE || status == CalculationStatus.MALICIOUS_BEHAVIOUR) {
      throw new IllegalStateException(
          "Unable to open variables when contract is in a terminal state. Current state: %s"
              .formatted(status));
    }
  }

  private void assertComputationDeadlineNotReached() {
    if (computationDeadlineReached()) {
      throw new IllegalStateException(
          "Unable to create inputs and start computations when the ZK computation deadline for the"
              + " contract has been exceeded");
    }
  }

  private boolean computationDeadlineReached() {
    return state.getComputationDeadline() < context.getBlockProductionTime();
  }

  private void assertVariablesExist(List<Integer> variableIds) {
    if (openingNonExistentVariables(variableIds)) {
      throw new IllegalArgumentException("Unable to open a variable that does not exist.");
    }
  }

  private boolean openingNonExistentVariables(List<Integer> variableIds) {
    for (int variableId : variableIds) {
      if (state.getVariable(variableId) == null) {
        return true;
      }
    }
    return false;
  }

  /**
   * Create a new pending variable.
   *
   * @param transaction the transaction that created this variable
   * @param owner of pending
   * @param sealed sealing variable
   * @param additionalInformation of variable
   * @param shareLengths lengths of shares
   * @param publicKey encryption key for the encrypted shares
   * @param encryptedShares the encrypted shares
   * @param commitments a list of commitments of the shares
   * @param preProcessMaterialRequester callback for requesting more input masks from preprocessing
   *     nodes
   * @param onInputtedShortname byte representation of {@code zk_on_variable_inputted} function
   *     shortname this pending input is linked to.
   */
  public void createPendingInput(
      Hash transaction,
      BlockchainAddress owner,
      boolean sealed,
      LargeByteArray additionalInformation,
      List<Integer> shareLengths,
      BlockchainPublicKey publicKey,
      FixedList<LargeByteArray> encryptedShares,
      FixedList<Hash> commitments,
      PreprocessedMaterialRequester preProcessMaterialRequester,
      LargeByteArray onInputtedShortname) {
    assertComputationDeadlineNotReached();
    int noOfInputs = shareCountFromLengths(shareLengths);
    int rpcLength = noOfInputs + 5;
    // Service fees covering the transaction a single node must make
    long singleNodeCost = computeGasCost(rpcLength) + EXTRA_TRANSACTION_COST;
    context.payFromCaller(
        NUMBER_OF_NODES * singleNodeCost + EXTRA_SERVICE_FEES, state.getEngineIdentities());
    requestPreProcessMaterialIfNeeded(
        preProcessMaterialRequester, PreProcessMaterialType.BINARY_INPUT_MASK, noOfInputs);

    final ZkStateImmutableBuilder builder = state.builder();
    final long nextInputMaskOffset =
        builder.incrementPreProcessMaterialOffset(
            PreProcessMaterialType.BINARY_INPUT_MASK, noOfInputs);

    final ZkClosedImpl variable =
        new ZkClosedImpl(
            owner,
            builder.getAndIncrementNextVariableId(),
            sealed,
            additionalInformation,
            shareLengths,
            transaction,
            (int) nextInputMaskOffset);
    state =
        builder
            .setPendingInput(
                new PendingInputImpl(
                    variable, publicKey, encryptedShares, commitments, onInputtedShortname))
            .build();
  }

  /**
   * Determines whether new batches are needed, and requests them if they are.
   *
   * @param preProcessMaterialRequester Material requester.
   * @param batchType Type of material to request
   * @param numMaterialNeededRightNow Number of individual materials that are needed.
   */
  private void requestPreProcessMaterialIfNeeded(
      final PreprocessedMaterialRequester preProcessMaterialRequester,
      final PreProcessMaterialType batchType,
      final int numMaterialNeededRightNow) {
    int requestNow = numBatchesToRequest(numMaterialNeededRightNow, state, batchType);
    if (requestNow >= 1) {
      preProcessMaterialRequester.request(batchType, requestNow);
      state =
          state.builder().incrementPreProcessBatchesOrderedInTotal(batchType, requestNow).build();
    }
  }

  /**
   * Request new material based on the amount of preprocessed material to prefetch.
   *
   * @param preprocessMaterialRequester material requester
   * @param batchType type of material to prefetch
   */
  private void requestPreProcessMaterialPrefetch(
      PreprocessedMaterialRequester preprocessMaterialRequester, PreProcessMaterialType batchType) {
    int numElementsToPrefetch = state.getPreProcessMaterials(batchType).numElementsToPrefetch();
    requestPreProcessMaterialIfNeeded(
        preprocessMaterialRequester, batchType, numElementsToPrefetch);
  }

  /**
   * Performs deserialization and state updates for pending output.
   *
   * @param pendingOutput information about the output variable
   * @return null if reconstructed, otherwise a pending output with updated engine shares
   */
  public PendingOnChainOpenImpl handlePendingOutput(final PendingOnChainOpenImpl pendingOutput) {

    int nodeReactions = pendingOutput.getNodeReactions();
    if (nodeReactions > 2 * MALICIOUS_COUNT) {

      final ZkStateImmutableBuilder builder = state.builder();
      List<ZkClosedImpl> reconstructedZkCloseds =
          pendingOutput.reconstructValues(
              state.getVariablesTree()::getValue,
              (shares, bitLengths) -> reconstruct(shares, bitLengths, 1));
      if (reconstructedZkCloseds == null) {
        return pendingOutput;
      }

      reconstructedZkCloseds.forEach(builder::setVariable);
      state = builder.build();
      return null;
    } else {
      return pendingOutput;
    }
  }

  /**
   * Ask the computation nodes to add an attestation to a piece of data.
   *
   * @param data the data to attest
   */
  public void attestData(byte[] data) {
    context.payFromContract(BASE_SERVICE_FEES, state.getEngineIdentities());
    ZkStateImmutableBuilder builder = state.builder();
    state = builder.addAttestation(data).build();
  }

  /**
   * Get the list of attestation request identifiers.
   *
   * @return the list of attestation IDs.
   */
  public List<Integer> getAttestationIds() {
    return new ArrayList<>(state.getAttestationsRaw().keySet());
  }

  /**
   * Get the attestation using the ID.
   *
   * @param attestationId an attestation identifier
   * @return an attestation.
   */
  public DataAttestation getAttestation(int attestationId) {
    return state.getAttestation(attestationId);
  }

  /**
   * Update an attestation with a signature from a node.
   *
   * @param nodeIndex the index of the node
   * @param id the ID of the attestation
   * @param signature the node's signature on the attestation request
   * @return true if the attestation has become valid, and otherwise false.
   */
  public boolean updateAttestation(int nodeIndex, int id, Signature signature) {
    AttestationImpl specificAttestation = state.getAttestation(id);
    if (specificAttestation.getSignature(nodeIndex) != null) {
      // The node has already signed this attestation request
      return false;
    } else {
      AttestationImpl updated = specificAttestation.addAttestation(nodeIndex, signature);
      state = state.builder().updateAttestation(id, updated).build();
      return updated.hasEnoughAttestations();
    }
  }

  static Shares.ShareReader createReader() {
    return (stream, bitLengths) -> stream.readBytes(shareCountFromLengths(bitLengths.bitLengths()));
  }

  /**
   * Commits to a variable for a specific engine/node.
   *
   * @param engineIndex index of engine
   * @param engineOutputInformation Information about the engine output.
   * @param nextCalculateFor The next block time to perform an calculate for.
   * @return the result of the computation
   */
  public ComputationResult commitResultVariable(
      int engineIndex,
      final EngineOutputInformation engineOutputInformation,
      long nextCalculateFor) {
    final ComputationStateImpl computationState = state.getComputationState();
    final ComputationOutput computationOutput = computationState.getComputationOutput();
    final byte engines = EnginesStatus.setEngineBit(engineIndex, computationOutput.getEngines());

    FixedList<EngineOutputInformation> outputInformation = computationOutput.getOutputInformation();
    FixedList<EngineOutputInformation> updatedOutputInformation =
        RealContractBinder.COUNTER_LIST.withEntry(
            outputInformation, engineIndex, engineOutputInformation);

    FixedList<PendingOutputVariable> variables = computationOutput.getVariables();

    long finalNextCalculateFor = nextCalculateFor;
    state =
        state
            .builder()
            .updateComputationState(
                prev ->
                    prev.withOutput(
                        finalNextCalculateFor,
                        new ComputationOutput(engines, variables, updatedOutputInformation)))
            .build();

    return state.getComputationState().computeStatus(factory);
  }

  /**
   * Completes computation.
   *
   * @param partialOpens partial openings for the computation
   * @return returns ids of created variables
   */
  public List<Integer> completeComputation(Hash partialOpens) {
    FixedList<PendingOutputVariable> toCreate =
        state.getComputationState().getComputationOutput().getVariables();
    List<Integer> outputVariableIds = new ArrayList<>();
    for (int i = 0; i < toCreate.size(); i++) {
      PendingOutputVariable variable = toCreate.get(i);
      ZkClosedImpl outputVariable =
          new ZkClosedImpl(
              context.getContractAddress(),
              variable.getVariableId(),
              false,
              variable.getInformation(),
              List.of(variable.getBitLength()),
              null,
              0);
      state = state.builder().setVariable(outputVariable).build();
      outputVariableIds.add(outputVariable.getId());
    }
    byte engineState = state.getComputationState().getComputationOutput().getEngines();
    this.state =
        this.state
            .builder()
            .setCalculationStatus(CalculationStatus.WAITING)
            .setComputationState(null)
            .addFinishedComputation(
                state.getComputationState().getCalculateFor(),
                engineState,
                state.getComputationState().isOptimistic(),
                partialOpens)
            .build();
    return outputVariableIds;
  }

  /** Updates state to indicate that some malicious behaviour have occured. */
  public void malicious() {
    state = state.builder().setCalculationStatus(CalculationStatus.MALICIOUS_BEHAVIOUR).build();
    this.calculationIsTerminating = true;
  }

  /**
   * Add batch of pre-process material.
   *
   * @param batchType Type of pre-process batch to add.
   * @param batchId id of batch
   */
  public void addPreProcessMaterialBatch(PreProcessMaterialType batchType, int batchId) {
    this.state = state.builder().addPreProcessMaterialBatch(batchType, batchId).build();
  }

  /**
   * Determine definition of computation output.
   *
   * @param computationState state of computation
   * @param computation REAL computation
   * @return definition of computation output
   */
  private OutputDefinition determineComputationOutput(
      ZkComputationState<WasmRealContractState, LargeByteArray, RealClosed<LargeByteArray>>
          computationState,
      RealComputationEnv<WasmRealContractState, LargeByteArray> computation) {

    final var shareStorage = CountingShareStorage.forBinary(computationState);
    final CountingMultiplier<BinaryExtensionFieldElement> multiplier =
        CountingMultiplier.newFromZero();
    final RealEnvironment<SecretSharedNumberAsBits, BinaryExtensionFieldElement> realEnvironment =
        new RealEnvironmentImpl<SecretSharedNumberAsBits, BinaryExtensionFieldElement>(
            shareStorage, multiplier);

    computation.compute(computationState, realEnvironment);
    return new OutputDefinition(multiplier.multCounter().get(), shareStorage.bitLengths());
  }

  /**
   * Requests reset of the ZK computation into non-optimistic mode.
   *
   * @param tripleBatchRequester used to request preprocessing triples, if needed
   */
  public void runNonOptimisticComputation(PreprocessedMaterialRequester tripleBatchRequester) {
    ComputationStateImpl currentComputation = state.getComputationState();
    this.state =
        this.state
            .builder()
            .setComputationState(currentComputation.reset(false, context.getBlockTime()))
            .build();
  }

  /**
   * Performs deserialization and state updates for finished ZK computations.
   *
   * @param engineIndex Engine to handle for
   * @param calculationFor Block time calculation is happening for
   */
  public void handleInputForFinished(final int engineIndex, final long calculationFor) {
    FixedList<FinishedComputationImpl> finishedComputations = state.getFinishedComputationsRaw();
    FinishedComputationImpl currentElement =
        finishedComputations.stream()
            .filter(computation -> computation.getCalculateFor() == calculationFor)
            .findFirst()
            .orElseThrow();
    FinishedComputationImpl nextElement = currentElement.withInput(engineIndex);
    mutate(builder -> builder.replaceComputation(currentElement, nextElement));
  }

  /**
   * Removes pending input with the given variable id.
   *
   * @param variableId Pending input to delete
   */
  public void removePendingInput(final int variableId) {
    ZkStateImmutableBuilder builder = state.builder();
    builder.removePendingInput(variableId);
    state = builder.build();
  }

  /**
   * Performs deserialization and state updates for opening masked inputs. If enough nodes have sent
   * correct shares, the variable is confirmed, then pending input is deleted and null is returned.
   * Else, return the pending input with its information updated.
   *
   * @param maskedInput Masked input to handle.
   * @param pending The input's information
   * @param from Address of the zk engine where this message have come from
   * @return pending input updated with information from the RPC, or null if the variable has been
   *     confirmed
   */
  public PendingInputImpl handleOpenOfMaskedInput(
      final LargeByteArray maskedInput,
      final PendingInputImpl pending,
      final BlockchainAddress from) {
    final int engineIndex = getState().assertValidZkEngineNode(from);
    PendingInputImpl updated = pending.addNodeReaction(engineIndex, maskedInput);
    int nodeReactions = updated.getNodeReactions();
    if (nodeReactions > 2 * MALICIOUS_COUNT) {
      if (confirmPending(updated)) {
        confirmedInputs += 1;
        return null;
      } else if (nodeReactions > 3 * MALICIOUS_COUNT) {
        updated = updated.reject();
      }
    }
    final ZkStateImmutableBuilder builder = state.builder();
    builder.setPendingInput(updated);
    state = builder.build();
    return updated;
  }

  /**
   * Try to confirm pending input by reconstructing the variable from shares inputted by the zk
   * nodes. If reconstructing succeeds, the pending input is deleted and a variable is created with
   * the opened masked input.
   *
   * @param updated the pending input to confirm
   * @return true if reconstructing succeeds, false otherwise
   */
  private boolean confirmPending(PendingInputImpl updated) {
    ZkClosedImpl variable = updated.getVariable();
    List<LargeByteArray> nodeOpenings = updated.getNodeOpenings();
    List<Integer> shareBitLengths = variable.getShareBitLengths();

    LargeByteArray blindedInput = reconstruct(nodeOpenings, shareBitLengths, 1);
    if (blindedInput == null) {
      return false;
    }
    ZkStateImmutableBuilder builder = state.builder();
    builder.removePendingInput(variable.getId());
    builder.setVariable(variable.withBlindedInputs(blindedInput));
    state = builder.build();
    return true;
  }

  /**
   * Attempt to reconstruct secret from received shares from nodes. Does lagrange interpolation for
   * each bit of data. If any interpolation fails it returns null.
   *
   * @param nodeOpenings data opened on node
   * @param shareBitLengths bit lengths of shares
   * @param degree polynomial degree for Lagrange interpolation
   * @return share as a {@link LargeByteArray}, or null if any interpolation fails
   */
  private LargeByteArray reconstruct(
      List<LargeByteArray> nodeOpenings, Collection<Integer> shareBitLengths, int degree) {

    int noOfShares = shareCountFromLengths(shareBitLengths);
    List<List<BinaryExtensionFieldElement>> shareLists = new ArrayList<>();
    List<BinaryExtensionFieldElement> alphas = determineAlphas(nodeOpenings);

    for (int i = 0; i < noOfShares; i++) {
      List<BinaryExtensionFieldElement> shares = new ArrayList<>();
      for (LargeByteArray nodeOpening : nodeOpenings) {
        if (nodeOpening != null) {
          shares.add(BinaryExtensionFieldElement.create(nodeOpening.getData()[i]));
        }
      }
      shareLists.add(shares);
    }

    byte[] resultBytes = new byte[noOfShares];
    for (int i = 0; i < noOfShares; i++) {
      Polynomial<BinaryExtensionFieldElement> interpolate =
          Lagrange.interpolateIfPossible(
              alphas, shareLists.get(i), degree, factory.zero(), factory.one());
      if (interpolate == null) {
        return null;
      }
      resultBytes[i] = interpolate.getConstantTerm().serialize()[0];
    }

    return new LargeByteArray(resultBytes);
  }

  /**
   * Determine the alphas needed for lagrange interpolations based on the shares from the zk nodes.
   * Only includes the alphas for nodes which have inputted a share.
   *
   * @param nodeOpenings list of shares from the zk nodes. Node {@code i} will have its share in the
   *     {@code i}th entry of the list. If a node has not inputted a share, its entry in the list
   *     will be null.
   * @return the list of alphas needed for lagrange interpolation
   */
  private List<BinaryExtensionFieldElement> determineAlphas(List<LargeByteArray> nodeOpenings) {
    List<BinaryExtensionFieldElement> alphas = new ArrayList<>();
    for (int i = 0; i < nodeOpenings.size(); i++) {
      if (nodeOpenings.get(i) != null) {
        alphas.add(factory.alphas().get(i));
      }
    }
    return alphas;
  }

  /**
   * Create a new subscription for external events on an EVM chain.
   *
   * @param subscription info read from RPC for creating a new subscription
   */
  public void subscribeToExternalEvents(CreateEvmEventSubscription subscription) {
    // Pay in advance for the first event for this subscription.
    context.payFromContract(BASE_SERVICE_FEES, state.getEngineIdentities());
    ZkStateImmutableBuilder builder = state.builder();
    state = builder.subscribeToEvents(subscription).build();
  }

  /**
   * Cancel an existing subscription.
   *
   * @param subscriptionId id of the subscription to cancel
   */
  public void cancelSubscription(int subscriptionId) {
    ZkStateImmutableBuilder builder = state.builder();
    state = builder.cancelSubscription(subscriptionId).build();
  }

  /**
   * Delete an external event.
   *
   * @param eventId identifier of the event to remove
   */
  public void deleteExternalEvent(int eventId) {
    state = state.builder().removeExternalEvent(eventId).build();
  }

  /**
   * Handle an incoming event observed by one of the EVM oracle members.
   *
   * @param evmOracle that has observed the event
   * @param subscriptionId id of the subscription the event belongs to
   * @param observedEvent to be handled
   * @return event eventLog if the observed event could be confirmed, null otherwise
   */
  public ConfirmedEventLog handleObservedEvent(
      BlockchainAddress evmOracle, int subscriptionId, EvmEventLog observedEvent) {
    assertValidSubscription(subscriptionId);
    assertEventNotConfirmedForSubscription(subscriptionId, observedEvent.getMetadata());

    PendingEventLog pendingEvent = getOrCreatePendingEvent(subscriptionId);
    int oracleIndex = state.getEngines().engineIndex(evmOracle);
    pendingEvent = pendingEvent.addCandidateForOracle(observedEvent, oracleIndex);

    ConfirmedEventLog confirmed = pendingEvent.confirmEvent();
    if (confirmed != null) {
      state = state.builder().removePendingEvent(subscriptionId).confirmEvent(confirmed).build();
    } else {
      state = state.builder().setPendingEvent(pendingEvent).build();
    }

    return confirmed;
  }

  private PendingEventLog getOrCreatePendingEvent(int subscriptionId) {
    PendingEventLog pending =
        state.getExternalEvents().getPendingEventForSubscription(subscriptionId);
    if (pending == null) {
      state = state.builder().createPendingEvent(subscriptionId).build();
      pending = state.getExternalEvents().getPendingEventForSubscription(subscriptionId);
    }
    return pending;
  }

  private void assertValidSubscription(int subscriptionId) {
    ExternalEventState externalEvents = state.getExternalEvents();
    EventSubscription subscription = externalEvents.getSubscription(subscriptionId);
    if (subscription == null || subscription.isCancelled()) {
      throw new IllegalStateException(
          "Cannot add events to cancelled or non-existing subscription " + subscriptionId);
    }
  }

  private void assertEventNotConfirmedForSubscription(
      int subscriptionId, EventMetadata observedEvent) {
    EventLog latestConfirmed =
        state.getExternalEvents().getLatestConfirmedForSubscription(subscriptionId);
    if (!isStrictlyNewer(latestConfirmed, observedEvent)) {
      String errorMessage =
          "Cannot add event "
              + observedEvent
              + " that is chronologically the same or older that latest confirmed "
              + latestConfirmed
              + " for subscription "
              + subscriptionId;
      throw new IllegalStateException(errorMessage);
    }
  }

  /**
   * Register a heartbeat block as reported by one of the EVM oracle nodes. If this registration
   * triggers the heartbeat to be confirmed by a majority of the EVM oracles, the confirmed
   * heartbeat is returned. Otherwise, null is returned.
   *
   * @param evmOracle address of the reporting oracle node
   * @param subscriptionId id of the subscription to add heartbeat to
   * @param heartbeatBlock the number of the reported heartbeat block
   * @return the confirmed heartbeat or null
   */
  public Unsigned256 registerHeartbeat(
      BlockchainAddress evmOracle, int subscriptionId, Unsigned256 heartbeatBlock) {
    assertValidSubscription(subscriptionId);
    EventLog latestConfirmed =
        state.getExternalEvents().getLatestConfirmedForSubscription(subscriptionId);
    if (heartbeatIsStrictlyNewer(latestConfirmed, heartbeatBlock)) {
      return handleValidHeartbeat(evmOracle, subscriptionId, heartbeatBlock);
    } else {
      return null;
    }
  }

  private Unsigned256 handleValidHeartbeat(
      BlockchainAddress evmOracle, int subscriptionId, Unsigned256 heartbeatBlock) {
    PendingHeartbeat pending = getPendingHeartbeat(subscriptionId);
    int oracleIndex = state.getEngines().engineIndex(evmOracle);

    pending = pending.registerHeartbeat(subscriptionId, oracleIndex, heartbeatBlock);
    Unsigned256 confirmed = pending.confirmHeartbeat();
    if (confirmed == null) {
      state = state.builder().setPendingHeartbeatBlock(subscriptionId, pending).build();
    } else {
      state =
          state
              .builder()
              .removePendingHeartbeat(subscriptionId)
              .confirmHeartbeat(subscriptionId, confirmed)
              .build();
    }
    return confirmed;
  }

  private PendingHeartbeat getPendingHeartbeat(int subscriptionId) {
    PendingHeartbeat pending = state.getExternalEvents().getPendingHeartbeat(subscriptionId);
    if (pending == null) {
      state = state.builder().createPendingHeartbeat(subscriptionId).build();
      pending = state.getExternalEvents().getPendingHeartbeat(subscriptionId);
    }
    return pending;
  }

  private boolean isStrictlyNewer(EventLog latestConfirmed, EventLog observedLog) {
    if (latestConfirmed == null) {
      return true;
    } else {
      return Comparator.comparing(EventLog::blockNumber)
              .thenComparing(EventLog::transactionIndex)
              .thenComparing(EventLog::logIndex)
              .compare(observedLog, latestConfirmed)
          > 0;
    }
  }

  private boolean heartbeatIsStrictlyNewer(EventLog latestConfirmed, Unsigned256 heartbeat) {
    if (latestConfirmed == null) {
      return true;
    } else {
      return heartbeat.compareTo(latestConfirmed.blockNumber()) > 0;
    }
  }

  record OutputDefinition(int multiplications, List<Integer> outputSizes) {}

  static final class ComputationStateFromMutable
      implements ZkComputationState<
          WasmRealContractState, LargeByteArray, RealClosed<LargeByteArray>> {

    private final ZkStateImmutable state;

    ComputationStateFromMutable(ZkStateImmutable state) {
      this.state = state;
    }

    @Override
    public WasmRealContractState getOpenState() {
      return state.getOpenState();
    }

    @Override
    public RealClosed<LargeByteArray> getVariable(int i) {
      return state.getVariable(i);
    }

    @Override
    public Collection<RealClosed<LargeByteArray>> getVariables() {
      return state.getVariables();
    }
  }
}
