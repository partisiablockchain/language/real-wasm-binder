package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealNodeState.ComputationState;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElementFactory;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** {@link ComputationState} implementation for REAL contracts. */
@Immutable
public final class ComputationStateImpl
    implements ComputationState<WasmRealContractState, LargeByteArray>, StateSerializable {

  static final int MALICIOUS_COUNT = 1;

  private final boolean optimisticMode;
  private final long calculateFor;
  private final WasmRealContractState openState;
  private final AvlTree<Integer, ZkClosedImpl> variables;
  private final ComputationOutput computationOutput;
  private final int multiplicationCount;
  private final int confirmedInputs;
  private final FixedList<Integer> tripleBatches;
  private final int offsetIntoFirstTripleBatch;

  /**
   * Shortname of the {@code zk_on_compute_complete} function to call, when the computation
   * finishes. Null if no such function was specified.
   */
  private final LargeByteArray onComputeCompleteShortname;

  @SuppressWarnings("unused")
  private ComputationStateImpl() {
    optimisticMode = false;
    calculateFor = -1;
    openState = null;
    variables = null;
    computationOutput = null;
    multiplicationCount = 0;
    confirmedInputs = 0;
    tripleBatches = null;
    offsetIntoFirstTripleBatch = 0;
    onComputeCompleteShortname = null;
  }

  ComputationStateImpl(
      boolean optimisticMode,
      int multiplicationCount,
      int confirmedInputs,
      long calculateFor,
      WasmRealContractState openState,
      AvlTree<Integer, ZkClosedImpl> variables,
      ComputationOutput computationOutput,
      LargeByteArray onComputeCompleteShortname) {
    this(
        optimisticMode,
        multiplicationCount,
        confirmedInputs,
        calculateFor,
        openState,
        variables,
        computationOutput,
        null,
        0,
        onComputeCompleteShortname);
  }

  private ComputationStateImpl(
      boolean optimisticMode,
      int multiplicationCount,
      int confirmedInputs,
      long calculateFor,
      WasmRealContractState openState,
      AvlTree<Integer, ZkClosedImpl> variables,
      ComputationOutput computationOutput,
      FixedList<Integer> tripleBatches,
      int offsetIntoFirstTripleBatch,
      LargeByteArray onComputeCompleteShortname) {
    this.multiplicationCount = multiplicationCount;
    this.confirmedInputs = confirmedInputs;
    this.optimisticMode = optimisticMode;
    this.calculateFor = calculateFor;
    this.openState = openState;
    this.variables = variables;
    this.computationOutput = computationOutput;
    this.tripleBatches = tripleBatches;
    this.offsetIntoFirstTripleBatch = offsetIntoFirstTripleBatch;
    this.onComputeCompleteShortname = onComputeCompleteShortname;
  }

  @Override
  public long getCalculateFor() {
    return calculateFor;
  }

  @Override
  public WasmRealContractState getOpenState() {
    return openState;
  }

  @Override
  public List<RealClosed<LargeByteArray>> getVariables() {
    return List.copyOf(variables.values());
  }

  @Override
  public RealClosed<LargeByteArray> getVariable(int id) {
    return variables.getValue(id);
  }

  @Override
  public List<Integer> getOutputIds() {
    return computationOutput.getVariables().stream()
        .map(PendingOutputVariable::getVariableId)
        .collect(Collectors.toList());
  }

  @Override
  public List<Integer> getTripleIds() {
    return new ArrayList<>(tripleBatches);
  }

  @Override
  public int getOffsetIntoFirstTripleBatch() {
    return offsetIntoFirstTripleBatch;
  }

  @Override
  public boolean isWaitingForTriples() {
    return tripleBatches == null;
  }

  @Override
  public boolean isOptimistic() {
    return optimisticMode;
  }

  @Override
  public boolean isOptimisticVerification() {
    return computationOutput.isWaitingForVerification();
  }

  @Override
  public Hash getOptimisticPartialOpeningsHash() {
    return computationOutput.getPartialOpenings();
  }

  @Override
  public byte[] getOptimisticVerificationSeed() {
    return computationOutput.getVerificationSeed();
  }

  /**
   * Get the computation output.
   *
   * @return computation output
   */
  public ComputationOutput getComputationOutput() {
    return computationOutput;
  }

  ComputationStateImpl reset(boolean isOptimistic, long blockTime) {
    return new ComputationStateImpl(
        isOptimistic,
        multiplicationCount,
        confirmedInputs,
        blockTime,
        openState,
        variables,
        computationOutput.reset(),
        null,
        0,
        onComputeCompleteShortname);
  }

  /**
   * Get amount of multiplications.
   *
   * @return amount of multiplications
   */
  public int getMultiplicationCount() {
    return multiplicationCount;
  }

  /**
   * Get amount of confirmed inputs.
   *
   * @return amount of confirmed inputs
   */
  public int getConfirmedInputs() {
    return confirmedInputs;
  }

  /**
   * Get status of output computation.
   *
   * @param factory creates elements in a finite field
   * @param <ElementT> finite element type
   * @return status of output computation
   */
  public <ElementT extends FiniteFieldElement<ElementT>> ComputationResult computeStatus(
      FiniteFieldElementFactory<ElementT> factory) {
    return computationOutput.status(optimisticMode, factory);
  }

  ComputationStateImpl withTriples(
      long currentBlockTime, FixedList<Integer> tripleBatches, int offsetIntoFirstTripleBatch) {
    return new ComputationStateImpl(
        isOptimistic(),
        multiplicationCount,
        confirmedInputs,
        currentBlockTime,
        openState,
        variables,
        computationOutput,
        tripleBatches,
        offsetIntoFirstTripleBatch,
        onComputeCompleteShortname);
  }

  ComputationStateImpl withOutput(long nextCalculateFor, ComputationOutput nextComputationOutput) {
    return new ComputationStateImpl(
        isOptimistic(),
        multiplicationCount,
        confirmedInputs,
        nextCalculateFor,
        openState,
        variables,
        nextComputationOutput,
        tripleBatches,
        offsetIntoFirstTripleBatch,
        onComputeCompleteShortname);
  }

  /**
   * Retrieve shortname of the {@code zk_on_compute_complete} function this computation is linked
   * to.
   *
   * @return byte representation of compute complete function shortname. Null if no such function
   *     exists.
   */
  public LargeByteArray getOnComputeCompleteShortname() {
    return onComputeCompleteShortname;
  }

  /** Computation result for a real computation. */
  public static final class ComputationResult {

    private final Hash partialOpens;
    private final ComputationStateStatus status;

    private ComputationResult(Hash partialOpens, ComputationStateStatus status) {
      this.partialOpens = partialOpens;
      this.status = status;
    }

    static ComputationResult createDone(Hash partialOpens) {
      return new ComputationResult(partialOpens, ComputationStateStatus.DONE);
    }

    static ComputationResult createMalicious() {
      return new ComputationResult(null, ComputationStateStatus.MALICIOUS);
    }

    static ComputationResult createPending() {
      return new ComputationResult(null, ComputationStateStatus.PENDING);
    }

    /**
     * Get partial openings for the computation.
     *
     * @return partial openings for the computation
     */
    public Hash getPartialOpens() {
      return partialOpens;
    }

    /**
     * Get computation status.
     *
     * @return computation status
     */
    public ComputationStateStatus getStatus() {
      return status;
    }
  }

  /** The different computation status. */
  public enum ComputationStateStatus {
    /** Pending. */
    PENDING,
    /** Done. */
    DONE,
    /** Malicious. */
    MALICIOUS
  }
}
