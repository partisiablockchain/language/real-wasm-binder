package com.partisiablockchain.zk.real.binder.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.time.Duration;
import java.time.Instant;

/** Class to help calculate how time is being paid for by staking fees. */
public final class ZkDeadlineCalculator {

  private final Fraction msPerGas;
  private final long minExtension;
  private final Duration maxExtension;
  private final Instant currentDeadline;

  private ZkDeadlineCalculator(
      Fraction msPerGas, long minExtension, Duration maxExtension, Instant currentDeadline) {
    this.msPerGas = msPerGas;
    this.minExtension = minExtension;
    this.maxExtension = maxExtension;
    this.currentDeadline = currentDeadline;
  }

  /**
   * Create new calculator with given parameters.
   *
   * @param msPerGasNumerator numerator part of the ms per gas fraction
   * @param msPerGasDenominator denominator part of the ms per gas fraction
   * @param currentDeadline timestamp for the current deadline
   * @param minExtension minimum required extension in ms
   * @param maxExtension limit for how long from now deadline can be extended
   * @return new calculator for ZK deadlines
   */
  public static ZkDeadlineCalculator create(
      long msPerGasNumerator,
      long msPerGasDenominator,
      Instant currentDeadline,
      long minExtension,
      Duration maxExtension) {
    Fraction msPerGas = new Fraction(msPerGasNumerator, msPerGasDenominator);
    return new ZkDeadlineCalculator(msPerGas, minExtension, maxExtension, currentDeadline);
  }

  /**
   * Calculate how much the deadline can be extended with and how much gas should be paid to do so.
   *
   * @param now timestamp for current block production time
   * @param availableGas to extend deadline with
   * @return calculation result
   */
  public Result calculateFor(Instant now, long availableGas) {
    long extensionCandidate = msPerGas.multiplyByScalar(availableGas).reduce().evaluate(false);
    Instant deadlineCandidate = currentDeadline.plus(Duration.ofMillis(extensionCandidate));
    Instant deadlineLimit = now.plus(maxExtension);

    if (deadlineCandidate.isAfter(deadlineLimit)) {
      deadlineCandidate = deadlineLimit;
      extensionCandidate = Duration.between(currentDeadline, deadlineCandidate).toMillis();
    }

    if (extensionCandidate < minExtension) {
      throw new IllegalArgumentException(
          "Not possible to extend deadline with minimum required amount");
    }

    long gasToPay = new Fraction(extensionCandidate, 1).divideBy(msPerGas).evaluate(true);
    return new Result(gasToPay, extensionCandidate);
  }

  /**
   * Result of calculating how much the deadline can be extended with.
   *
   * @param gasToPay for the extension
   * @param deadlineExtension the amount to extend in milliseconds
   */
  public record Result(long gasToPay, long deadlineExtension) {}

  @SuppressWarnings("unused")
  private record Fraction(long numerator, long denominator) {

    private Fraction {
      if (denominator == 0) {
        throw new IllegalStateException("Denominator cannot be zero");
      }
    }

    private Fraction multiplyByScalar(long scalar) {
      long numerator = Math.multiplyExact(this.numerator, scalar);
      return new Fraction(numerator, denominator);
    }

    private Fraction multiply(Fraction other) {
      return new Fraction(
              Math.multiplyExact(this.numerator(), other.numerator()),
              Math.multiplyExact(this.denominator(), other.denominator()))
          .reduce();
    }

    private Fraction divideBy(Fraction dividend) {
      return multiply(
          new Fraction(
              /* numerator= */ dividend.denominator(), /* denominator= */ dividend.numerator()));
    }

    private long evaluate(boolean roundUp) {
      if (roundUp) {
        return 1 + (numerator() - 1) / denominator();
      } else {
        return numerator() / denominator();
      }
    }

    private Fraction reduce() {
      long gcd = gcd(numerator, denominator);
      return new Fraction(numerator / gcd, denominator / gcd);
    }

    private static long gcd(long first, long second) {
      if (second == 0) {
        return first;
      } else {
        return gcd(second, first % second);
      }
    }
  }
}
