package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.secata.stream.LittleEndianByteOutput;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.Collection;
import java.util.List;

/** Implementation of a REAL variable/row ({@link RealClosed}) for the REAL contract. */
@Immutable
public final class ZkClosedImpl implements RealClosed<LargeByteArray>, StateSerializable {

  private final int id;
  private final boolean sealed;
  private final BlockchainAddress owner;
  private final LargeByteArray information;
  private final FixedList<Integer> shareBitLengths;
  private final LargeByteArray maskedInputShare;
  private final LargeByteArray openValue;
  private final Hash transaction;
  private final int inputMaskOffset;

  @SuppressWarnings("unused")
  ZkClosedImpl() {
    id = -1;
    sealed = false;
    owner = null;
    information = null;
    shareBitLengths = null;
    maskedInputShare = null;
    openValue = null;
    transaction = null;
    inputMaskOffset = 0;
  }

  ZkClosedImpl(
      BlockchainAddress owner,
      int id,
      boolean sealed,
      LargeByteArray information,
      Collection<Integer> shareBitLengths,
      Hash transaction,
      int inputMaskOffset) {
    this(
        id,
        sealed,
        owner,
        information,
        FixedList.create(shareBitLengths),
        null,
        transaction,
        null,
        inputMaskOffset);
  }

  private ZkClosedImpl(
      int id,
      boolean sealed,
      BlockchainAddress owner,
      LargeByteArray information,
      FixedList<Integer> shareBitLengths,
      LargeByteArray maskedInputShare,
      Hash transaction,
      LargeByteArray openValue,
      int inputMaskOffset) {
    this.id = id;
    this.sealed = sealed;
    this.owner = owner;
    this.information = information;
    this.shareBitLengths = requireNonNull(shareBitLengths);
    this.maskedInputShare = maskedInputShare;
    this.openValue = openValue;
    this.transaction = transaction;
    this.inputMaskOffset = inputMaskOffset;

    // Validate share bit lengths
    if (shareBitLengths.stream().anyMatch(x -> x < 0)) {
      throw new IllegalArgumentException(
          "shareBitLengths must all be non-negative, but was %s".formatted(shareBitLengths));
    }
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public BlockchainAddress getOwner() {
    return owner;
  }

  @Override
  public LargeByteArray getInformation() {
    return information;
  }

  @Override
  public boolean isSealed() {
    return sealed;
  }

  @Override
  public byte[] getOpenValue() {
    return nullSafeClone(openValue);
  }

  @Override
  public byte[] getBlindedInputs() {
    return nullSafeClone(maskedInputShare);
  }

  @Override
  public int getInputMaskOffset() {
    return inputMaskOffset;
  }

  @Override
  public Hash getTransaction() {
    return transaction;
  }

  @Override
  public List<Integer> getShareBitLengths() {
    return List.copyOf(shareBitLengths);
  }

  private static byte[] nullSafeClone(LargeByteArray value) {
    if (value == null) {
      return null;
    } else {
      return value.getData();
    }
  }

  @Override
  public int getShareBitLength() {
    throw new UnsupportedOperationException("getShareBitLength() not supported on Real ZkClosed");
  }

  /**
   * returns this object mutated with an open value.
   *
   * @param openValue the open value as a wrapped byte array
   * @return the mutated copy
   */
  public ZkClosedImpl withOpen(LargeByteArray openValue) {
    return new ZkClosedImpl(
        id,
        sealed,
        owner,
        information,
        shareBitLengths,
        maskedInputShare,
        transaction,
        openValue,
        inputMaskOffset);
  }

  ZkClosedImpl withOwner(BlockchainAddress newOwner) {
    return new ZkClosedImpl(
        id,
        sealed,
        newOwner,
        information,
        shareBitLengths,
        maskedInputShare,
        transaction,
        openValue,
        inputMaskOffset);
  }

  ZkClosedImpl withBlindedInputs(LargeByteArray blindedInputs) {
    return new ZkClosedImpl(
        id,
        sealed,
        owner,
        information,
        shareBitLengths,
        blindedInputs,
        transaction,
        openValue,
        inputMaskOffset);
  }

  /**
   * Serialize into wasm byte representation of a closed variable.
   *
   * @return wasm byte representation.
   */
  public byte[] serialize() {
    return LittleEndianByteOutput.serialize(
        out -> {

          // Variable id
          out.writeI32(this.getId());
          // Variable owner
          out.writeBytes(SafeDataOutputStream.serialize(stream -> this.getOwner().write(stream)));
          // Is sealed?
          out.writeBoolean(this.isSealed());

          // Metadata
          out.writeBytes(this.getInformation().getData());

          // Opened variable data, if applicable
          final byte[] valueBits = this.getOpenValue();
          out.writeBoolean(valueBits != null);
          if (valueBits != null) {
            final byte[] packedBits = packLittleEndianBitArray(valueBits);
            out.writeI32(packedBits.length);
            out.writeBytes(packedBits);
          }
        });
  }

  /**
   * Computes the size of the variable when serialized to bytes.
   *
   * @return size of serialized bytes.
   */
  public int serializedByteLength() {
    int size = 0;
    // Variable id
    size += 4;
    // Variable owner
    size += 21;
    // Is sealed?
    size += 1;

    // Metadata
    size += this.getInformation().getLength();

    // Opened variable data, if applicable
    final byte[] valueBits = this.getOpenValue();
    size += 1;
    if (valueBits != null) {
      // Open length
      size += 4;
      size += packedBitArraySize(valueBits);
    }
    return size;
  }

  static int packedBitArraySize(byte[] bits) {
    int size = bits.length >> 3;
    if ((bits.length & 0x07) != 0) {
      size += 1;
    }
    return size;
  }

  static byte[] packLittleEndianBitArray(final byte[] bits) {
    final int byteLength = ZkClosed.getByteLength(bits.length);
    final byte[] result = new byte[byteLength];
    int bitIndex = 0;

    for (final byte bit : bits) {
      final int byteIndex = bitIndex >> 3;
      result[byteIndex] = (byte) (result[byteIndex] | ((bit & 1) << (bitIndex & 0x07)));
      bitIndex++;
    }
    return result;
  }
}
