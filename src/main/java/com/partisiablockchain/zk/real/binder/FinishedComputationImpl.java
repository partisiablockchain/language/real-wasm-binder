package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.zk.real.contract.RealNodeState.FinishedComputation;

/** Finished computation contains the information used on contract after the computation. */
@Immutable
public final class FinishedComputationImpl implements StateSerializable, FinishedComputation {

  private final long calculateFor;
  private final byte engineState;
  private final boolean optimistic;
  private final Hash partialOpens;

  @SuppressWarnings("unused")
  FinishedComputationImpl() {
    engineState = 0;
    optimistic = false;
    partialOpens = null;
    calculateFor = 0;
  }

  FinishedComputationImpl(
      long calculateFor, byte engineState, boolean optimistic, Hash partialOpens) {
    this.calculateFor = calculateFor;
    this.engineState = engineState;
    this.optimistic = optimistic;
    this.partialOpens = partialOpens;
  }

  @Override
  public long getCalculateFor() {
    return calculateFor;
  }

  @Override
  public boolean isOptimistic() {
    return optimistic;
  }

  @Override
  public Hash getPartialOpensHash() {
    return partialOpens;
  }

  @Override
  public boolean isHandledByEngine(int i) {
    return getEnginesStatus().isSuccess(i);
  }

  FinishedComputationImpl withInput(int index) {
    return new FinishedComputationImpl(
        calculateFor, EnginesStatus.setEngineBit(index, engineState), optimistic, partialOpens);
  }

  boolean isFinished() {
    return Integer.bitCount(engineState) == 4;
  }

  /**
   * Get which engines have handled this finished computation.
   *
   * @return a byte where if the i'th bit has been set, then node i has handled the finished
   *     computation
   */
  public EnginesStatus getEnginesStatus() {
    return new EnginesStatus(engineState);
  }
}
