package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.evm.oracle.contract.EventLog;
import com.partisiablockchain.evm.oracle.contract.EventSubscription;
import com.partisiablockchain.evm.oracle.contract.OracleNodeState;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.oracle.ConfirmedEventLog;
import com.partisiablockchain.oracle.EvmEventSubscription;
import com.partisiablockchain.oracle.ExternalEventState;
import com.partisiablockchain.oracle.PendingEventLog;
import com.partisiablockchain.oracle.PendingHeartbeat;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealNodeState;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Immutable state for {@link RealContractBinder}.
 *
 * <p>Can be converted to {@link ZkStateMutable} for modification.
 */
@Immutable
public final class ZkStateImmutable
    implements RealNodeState<WasmRealContractState, LargeByteArray>,
        OracleNodeState,
        StateSerializable {

  private final EngineState engines;
  private final WasmRealContractState openState;
  private final int nextVariableId;
  private final CalculationStatus calculationStatus;
  private final ComputationStateImpl computationState;
  private final AvlTree<Integer, PendingInputImpl> pendingInput;
  private final AvlTree<Integer, ZkClosedImpl> variables;
  private final AvlTree<Integer, PendingOnChainOpenImpl> pendingOnChainOpen;

  private final BlockchainAddress preprocessContract;
  private final BlockchainAddress nodeRegistryContract;
  private final FixedList<FinishedComputationImpl> finishedComputations;

  private final AvlTree<PreProcessMaterialType, PreProcessMaterialState> preProcessMaterials;

  private final long zkComputationDeadline;
  private final int nextAttestationId;
  private final AvlTree<Integer, AttestationImpl> attestations;

  private final ExternalEventState externalEvents;
  private final boolean defaultOptimisticMode;

  /** Serializable constructor. */
  public ZkStateImmutable() {
    this(null, null, null, 0L, true);
  }

  /**
   * Initial constructor with engines.
   *
   * @param engines non null engines
   * @param preprocess address for preprocess contract
   * @param nodeRegistry address for the zk node registry contract
   * @param zkComputationDeadline timestamp for when the ZK computation must be finished
   * @param defaultOptimisticMode whether a computation should start in optimistic mode, falling
   *     back to non-optimistic mode if it fails, or if the computation starts directly in
   *     non-optimistic mode
   */
  public ZkStateImmutable(
      EngineState engines,
      BlockchainAddress preprocess,
      BlockchainAddress nodeRegistry,
      long zkComputationDeadline,
      boolean defaultOptimisticMode) {
    this(
        engines,
        null,
        1,
        CalculationStatus.WAITING,
        null,
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        preprocess,
        nodeRegistry,
        FixedList.create(),
        zkComputationDeadline,
        1,
        AvlTree.create(),
        AvlTree.create(),
        ExternalEventState.create(),
        defaultOptimisticMode);
  }

  ZkStateImmutable(
      EngineState engines,
      WasmRealContractState openState,
      int nextVariableId,
      CalculationStatus calculationStatus,
      ComputationStateImpl computationState,
      AvlTree<Integer, PendingInputImpl> pendingInput,
      AvlTree<Integer, ZkClosedImpl> variables,
      AvlTree<Integer, PendingOnChainOpenImpl> pendingOnChainOpen,
      BlockchainAddress preprocessContract,
      BlockchainAddress nodeRegistryContract,
      FixedList<FinishedComputationImpl> finishedComputations,
      long zkComputationDeadline,
      int nextAttestationId,
      AvlTree<Integer, AttestationImpl> attestations,
      AvlTree<PreProcessMaterialType, PreProcessMaterialState> preProcessMaterials,
      ExternalEventState externalEvents,
      boolean defaultOptimisticMode) {
    this.engines = engines;
    this.openState = openState;
    this.nextVariableId = nextVariableId;
    this.calculationStatus = calculationStatus;
    this.computationState = computationState;
    this.pendingInput = pendingInput;
    this.variables = variables;
    this.pendingOnChainOpen = pendingOnChainOpen;
    this.preprocessContract = preprocessContract;
    this.finishedComputations = finishedComputations;
    this.zkComputationDeadline = zkComputationDeadline;
    this.nodeRegistryContract = nodeRegistryContract;
    this.nextAttestationId = nextAttestationId;
    this.attestations = attestations;
    this.preProcessMaterials = preProcessMaterials;
    this.externalEvents = externalEvents;
    this.defaultOptimisticMode = defaultOptimisticMode;
  }

  ZkStateImmutableBuilder builder() {
    return new ZkStateImmutableBuilder(this);
  }

  /**
   * Get engines state.
   *
   * @return state of engines
   */
  public EngineState getEngines() {
    return engines;
  }

  /**
   * Get the open state.
   *
   * @return open state
   */
  public WasmRealContractState getOpenState() {
    return this.openState;
  }

  @Override
  public List<RealEngine> getParticipatingEngines() {
    return engines.getEngines();
  }

  /**
   * Retrieves the list of engine addresses.
   *
   * @return List of engine identities
   */
  public List<BlockchainAddress> getEngineIdentities() {
    return getParticipatingEngines().stream()
        .map(RealEngine::getIdentity)
        .collect(Collectors.toList());
  }

  @Override
  public ComputationStateImpl getComputationState() {
    return computationState;
  }

  @Override
  public CalculationStatus getCalculationStatus() {
    return calculationStatus;
  }

  /**
   * Get next variable id.
   *
   * @return next variable id
   */
  public int getNextVariableId() {
    return nextVariableId;
  }

  @Override
  public List<PendingInput<LargeByteArray>> getPendingInputs() {
    return new ArrayList<>(pendingInput.values());
  }

  /**
   * Get pending inputs.
   *
   * @return avl tree from input id to pending input
   */
  public AvlTree<Integer, PendingInputImpl> getPendingInput() {
    return pendingInput;
  }

  /**
   * Get a input variable that has not yet been confirmed.
   *
   * @param owner the owner of the input
   * @return the optional variable
   */
  public Optional<ZkClosedImpl> getPendingInput(BlockchainAddress owner) {
    return pendingInput.values().stream()
        .filter(pending -> pending.getVariable().getOwner().equals(owner))
        .map(PendingInputImpl::getVariable)
        .findFirst();
  }

  @Override
  public List<RealClosed<LargeByteArray>> getVariables() {
    return new ArrayList<>(variables.values());
  }

  @Override
  public ZkClosedImpl getVariable(int variableId) {
    return variables.getValue(variableId);
  }

  @Override
  public List<DataAttestation> getAttestations() {
    return new ArrayList<>(attestations.values());
  }

  int getNextAttestationId() {
    return nextAttestationId;
  }

  /**
   * Get the map of attestations currently in the state.
   *
   * @return a map of attestations and their IDs.
   */
  public AvlTree<Integer, AttestationImpl> getAttestationsRaw() {
    return attestations;
  }

  /**
   * Get attestation with specific id.
   *
   * @param id of the attestation
   * @return attestation with id
   */
  public AttestationImpl getAttestation(int id) {
    return attestations.getValue(id);
  }

  /**
   * Get closed variables.
   *
   * @return avl tree from variable id to closed variable
   */
  public AvlTree<Integer, ZkClosedImpl> getVariablesTree() {
    return variables;
  }

  /**
   * Check if state has pending on-chain openings.
   *
   * @return true if state has pending on-chain openings, false otherwise
   */
  public boolean hasPendingOnChainOpen() {
    return !pendingOnChainOpen.keySet().isEmpty();
  }

  @Override
  public List<PendingOnChainOpen> getPendingOnChainOpens() {
    return new ArrayList<>(pendingOnChainOpen.values());
  }

  /**
   * Get raw access to pending on-chain openings.
   *
   * @return part of the state holding information about pending on-chain openings.
   */
  public AvlTree<Integer, PendingOnChainOpenImpl> getPendingOnChainOpensRaw() {
    return pendingOnChainOpen;
  }

  @Override
  public long getComputationDeadline() {
    return zkComputationDeadline;
  }

  /**
   * Get the external events state.
   *
   * @return external events
   */
  public ExternalEventState getExternalEvents() {
    return externalEvents;
  }

  @Override
  public List<EventSubscription> getEventSubscriptions() {
    return List.copyOf(getEventSubscriptionsRaw());
  }

  /**
   * Get a list of the raw event subscriptions.
   *
   * @return list of subscriptions
   */
  public List<EvmEventSubscription> getEventSubscriptionsRaw() {
    return externalEvents.getSubscriptions();
  }

  @Override
  public EventLog getLatestConfirmedEventLog(int subscriptionId) {
    return externalEvents.getLatestConfirmedForSubscription(subscriptionId);
  }

  @Override
  public boolean hasPushedNextEventLog(int subscriptionId, int oracleIndex) {
    return hasPushedEvent(subscriptionId, oracleIndex)
        || hasPushedHeartbeat(subscriptionId, oracleIndex);
  }

  private boolean hasPushedEvent(int subscriptionId, int oracleIndex) {
    PendingEventLog pending = externalEvents.getPendingEventForSubscription(subscriptionId);
    if (pending == null) {
      return false;
    } else {
      return pending.candidateFor(oracleIndex) != null;
    }
  }

  private boolean hasPushedHeartbeat(int subscriptionId, int oracleIndex) {
    PendingHeartbeat pending = externalEvents.getPendingHeartbeat(subscriptionId);
    if (pending == null) {
      return false;
    } else {
      return pending.candidateFor(oracleIndex) != null;
    }
  }

  /**
   * Get list of raw event log eventLog of confirmed events for the given subscription.
   *
   * @param subscriptionId to get confirmed events for
   * @return list of raw event log eventLog
   */
  public List<ConfirmedEventLog> getConfirmedEventLogsRaw(int subscriptionId) {
    return externalEvents.getConfirmedEventsForSubscription(subscriptionId);
  }

  /**
   * Asserts that given address is for an engine node, and returns the engine index.
   *
   * @param from Address to assert on
   * @return Engine index
   * @throws IllegalArgumentException When address doesn't match a known engine.
   */
  public int assertValidZkEngineNode(final BlockchainAddress from) {
    int index = engines.engineIndex(from);
    if (index == -1) {
      throw new IllegalArgumentException("Only engines allowed to call");
    }
    return index;
  }

  int assertValidZkEngineNode(final BlockchainPublicKey key) {
    int index = 0;
    for (RealEngine engine : engines.getEngines()) {
      if (engine.getPublicKey().equals(key)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  @Override
  public List<Integer> getComputationTriples() {
    return List.copyOf(getTriplesState().batchIds());
  }

  /**
   * Get information about input mask pre-process material.
   *
   * @return Information on pre-process material.
   */
  public PreProcessMaterialState getInputMaskState() {
    return getPreProcessMaterials(PreProcessMaterialType.BINARY_INPUT_MASK);
  }

  /**
   * Get information about triple pre-process material.
   *
   * @return Information on pre-process material.
   */
  public PreProcessMaterialState getTriplesState() {
    return getPreProcessMaterials(PreProcessMaterialType.BINARY_TRIPLE);
  }

  /**
   * Get the full {@link AvlTree} of pre-process materials.
   *
   * @return All contract information on pre-process material. Never null.
   */
  AvlTree<PreProcessMaterialType, PreProcessMaterialState> getAllPreProcessMaterials() {
    return preProcessMaterials;
  }

  /**
   * Get information about pre-process materials by the material type.
   *
   * @param batchType Batch type to get state for.
   * @return Information on pre-process material. Never null.
   */
  public PreProcessMaterialState getPreProcessMaterials(PreProcessMaterialType batchType) {
    final PreProcessMaterialState materialState = preProcessMaterials.getValue(batchType);
    return materialState != null ? materialState : PreProcessMaterialState.EMPTY;
  }

  @Override
  public List<Integer> getInputMasks() {
    return List.copyOf(getInputMaskState().batchIds());
  }

  @Override
  public BlockchainAddress getPreprocessContract() {
    return preprocessContract;
  }

  @Override
  public BlockchainAddress getNodeRegistryContract() {
    return nodeRegistryContract;
  }

  @Override
  public List<FinishedComputation> getFinishedComputations() {
    return List.copyOf(finishedComputations);
  }

  FixedList<FinishedComputationImpl> getFinishedComputationsRaw() {
    return finishedComputations;
  }

  /**
   * Get whether to start a computation in optimistic mode or not.
   *
   * @return true if optimistic false otherwise
   */
  public boolean getDefaultOptimisticMode() {
    return defaultOptimisticMode;
  }
}
