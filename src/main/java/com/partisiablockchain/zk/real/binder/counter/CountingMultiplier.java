package com.partisiablockchain.zk.real.binder.counter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.SecretShared;
import com.partisiablockchain.zk.real.protocol2.BatchUtility;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * {@link ElementMultiplier} that counts the number of times an conjunction is computed. Produced
 * values are entirely worthless.
 *
 * @param <T> Type of multiplied elements.
 * @param roundCounter Counter to count rounds with.
 * @param multCounter Counter to count multiplications with.
 */
public record CountingMultiplier<T extends SecretShared<T>>(
    AtomicInteger roundCounter, AtomicInteger multCounter) implements ElementMultiplier<T> {

  /**
   * Create new counting multiplier with all counters initialized to zero.
   *
   * @param <T> Type of multiplied elements.
   * @return Newly created counting multiplier.
   */
  public static <T extends SecretShared<T>> CountingMultiplier<T> newFromZero() {
    return new CountingMultiplier<T>(new AtomicInteger(), new AtomicInteger());
  }

  /**
   * Create new counting multiplier.
   *
   * @param roundCounter Counter to count rounds with.
   * @param multCounter Counter to count multiplications with.
   */
  public CountingMultiplier {
    requireNonNull(roundCounter);
    requireNonNull(multCounter);
  }

  @Override
  public T multiply(T left, T right) {
    throw new UnsupportedOperationException("use multiplyPairwise instead");
  }

  @Override
  public List<T> multiplyPairwise(Iterable<T> left, Iterable<T> right) {
    final var ls = BatchUtility.pairwiseMap((a, b) -> a, left, right);
    multCounter().addAndGet(ls.size());
    roundCounter().addAndGet(1);
    return ls;
  }
}
