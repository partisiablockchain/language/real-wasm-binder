package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * An implementation of this interface is passed to a class to make it able to notify a contract
 * that a ZK computation has finished.
 */
public interface ComputationDoneNotifier {
  /**
   * Notifies a contract about a ZK computation that has finished.
   *
   * @param enginesStatus status for each zk node of the computation
   */
  void notifyComputationDone(EnginesStatus enginesStatus);
}
