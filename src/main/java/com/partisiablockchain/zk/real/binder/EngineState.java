package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.zk.real.contract.RealNodeState.RealEngine;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** The engines in a computation. */
@Immutable
public final class EngineState implements StateSerializable {

  private final FixedList<EngineInformation> engines;

  @SuppressWarnings("unused")
  EngineState() {
    engines = null;
  }

  private EngineState(FixedList<EngineInformation> engines) {
    this.engines = engines;
  }

  /**
   * Deserializes {@link EngineState} from a stream.
   *
   * @param stream Stream to read from
   * @return new engine state instance
   */
  public static EngineState read(final SafeDataInputStream stream) {
    return new EngineState(
        FixedList.create(
            List.of(
                EngineInformation.read(stream),
                EngineInformation.read(stream),
                EngineInformation.read(stream),
                EngineInformation.read(stream))));
  }

  int engineIndex(BlockchainAddress left) {
    for (int i = 0; i < engines.size(); i++) {
      if (engines.get(i).getIdentity().equals(left)) {
        return i;
      }
    }
    return -1;
  }

  List<RealEngine> getEngines() {
    return List.copyOf(engines);
  }

  /** Identification of an engine that can run real. */
  @Immutable
  static final class EngineInformation implements StateSerializable, RealEngine {

    private final BlockchainAddress identity;
    private final BlockchainPublicKey publicKey;
    private final String restInterface;

    @SuppressWarnings("unused")
    EngineInformation() {
      identity = null;
      publicKey = null;
      restInterface = null;
    }

    EngineInformation(
        BlockchainAddress identity, BlockchainPublicKey publicKey, String restInterface) {
      this.identity = identity;
      this.publicKey = publicKey;
      this.restInterface = restInterface;
    }

    static EngineInformation read(SafeDataInputStream stream) {
      return new EngineInformation(
          BlockchainAddress.read(stream), BlockchainPublicKey.read(stream), stream.readString());
    }

    @Override
    public BlockchainAddress getIdentity() {
      return identity;
    }

    @Override
    public BlockchainPublicKey getPublicKey() {
      return publicKey;
    }

    @Override
    public String getRestInterface() {
      return restInterface;
    }
  }
}
