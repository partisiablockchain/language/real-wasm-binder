package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

/**
 * Contains information about pre-process material. Note that each batch corresponds to a number of
 * elements as specified in {@link PreProcessMaterialType#getBatchSize}.
 *
 * <p>Expected invariants:
 *
 * <ul>
 *   <li>{@code batchIds.size() <= numRequestedBatches}
 *   <li>{@code numUsedElements <= batchIds.size() * batchType.getBatchSize()} (for computation
 *       triples, but not input masks.)
 * </ul>
 *
 * @param batchIds Known batch ids of pre-process material.
 * @param numUsedElements The amount of elements that have been allocated to computations so far.
 * @param numRequestedBatches Number of batches that have been requested from the preprocess
 *     contract. Includes batches that are tracked in {@link PreProcessMaterialState#batchIds}.
 * @param numElementsToPrefetch Number of elements to prefetch. The binder will attempt to always
 *     have at least this amount of material available.
 */
@Immutable
public record PreProcessMaterialState(
    FixedList<Integer> batchIds,
    long numUsedElements,
    int numRequestedBatches,
    int numElementsToPrefetch)
    implements StateSerializable {

  /**
   * Empty state indicating that contract have not requested nor received any batches for the
   * batchType.
   */
  public static final PreProcessMaterialState EMPTY =
      new PreProcessMaterialState(FixedList.create(), 0L, 0, 0);

  /**
   * Increments number of used pre-process materials.
   *
   * @param numUsed Number of recently used materials.
   * @return Updated record.
   */
  public PreProcessMaterialState incrementNumUsedElements(long numUsed) {
    return new PreProcessMaterialState(
        batchIds, numUsedElements + numUsed, numRequestedBatches, numElementsToPrefetch);
  }

  /**
   * Increments number of ordered batches for the given type of pre-process material.
   *
   * @param newlyRequested Number of batches requested.
   * @return Updated record.
   */
  public PreProcessMaterialState incrementOrdered(int newlyRequested) {
    return new PreProcessMaterialState(
        batchIds, numUsedElements, numRequestedBatches + newlyRequested, numElementsToPrefetch);
  }

  /**
   * Add batch of preprocess material.
   *
   * @param batchId id of batch
   * @return Updated record.
   */
  public PreProcessMaterialState addBatch(int batchId) {
    final FixedList<Integer> newBatchIds = batchIds.addElement(batchId);
    final int newNumRequestedBatches = Integer.max(this.numRequestedBatches, newBatchIds.size());
    return new PreProcessMaterialState(
        newBatchIds, numUsedElements, newNumRequestedBatches, numElementsToPrefetch);
  }

  /**
   * Set the number of elements of this batch type to prefetch.
   *
   * @param newNumElementsToPrefetch new number of elements to prefetch
   * @return Updated record.
   */
  public PreProcessMaterialState setNumElementsToPrefetch(int newNumElementsToPrefetch) {
    return new PreProcessMaterialState(
        batchIds, numUsedElements, numRequestedBatches, newNumElementsToPrefetch);
  }
}
