package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.zk.real.contract.RealNodeState.PendingInput;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** {@link PendingInput} for the REAL contract, containing the variable data. */
@Immutable
public final class PendingInputImpl implements PendingInput<LargeByteArray>, StateSerializable {

  private final ZkClosedImpl variable;
  private final FixedList<LargeByteArray> encryptedShares;
  private final FixedList<LargeByteArray> nodeOpenings;
  private final FixedList<Hash> commitments;
  private final byte engines;
  private final boolean rejected;

  /**
   * Shortname of the {@code zk_variable_inputted} function to call, when the pending variable is
   * fully input. Null if no such function was specified.
   */
  private final LargeByteArray onInputtedShortname;

  @SuppressWarnings("Immutable")
  private final BlockchainPublicKey publicKey;

  @SuppressWarnings("unused")
  PendingInputImpl() {
    variable = null;
    publicKey = null;
    encryptedShares = null;
    nodeOpenings = null;
    commitments = null;
    engines = 0;
    rejected = false;
    onInputtedShortname = null;
  }

  PendingInputImpl(
      ZkClosedImpl variable,
      BlockchainPublicKey publicKey,
      FixedList<LargeByteArray> encryptedShares,
      FixedList<Hash> commitments,
      LargeByteArray onInputtedShortname) {
    this(
        variable,
        publicKey,
        encryptedShares,
        FixedList.create(),
        commitments,
        (byte) 0,
        false,
        onInputtedShortname);
  }

  private PendingInputImpl(
      ZkClosedImpl variable,
      BlockchainPublicKey publicKey,
      FixedList<LargeByteArray> encryptedShares,
      FixedList<LargeByteArray> nodeOpenings,
      FixedList<Hash> commitments,
      byte engines,
      boolean rejected,
      LargeByteArray onInputtedShortname) {
    this.variable = variable;
    this.publicKey = publicKey;
    this.encryptedShares = encryptedShares;
    this.nodeOpenings = nodeOpenings;
    this.commitments = commitments;
    this.engines = engines;
    this.rejected = rejected;
    this.onInputtedShortname = onInputtedShortname;
  }

  @Override
  public BlockchainPublicKey getPublicKey() {
    return publicKey;
  }

  @Override
  public ZkClosedImpl getVariable() {
    return variable;
  }

  /**
   * Get node openings.
   *
   * @return list of node openings
   */
  public List<LargeByteArray> getNodeOpenings() {
    return new ArrayList<>(nodeOpenings);
  }

  @Override
  public List<byte[]> getEncryptedShares() {
    if (encryptedShares != null) {
      return encryptedShares.stream().map(LargeByteArray::getData).collect(Collectors.toList());
    } else {
      return null;
    }
  }

  @Override
  public List<Hash> getShareCommitments() {
    if (commitments != null) {
      return new ArrayList<>(commitments);
    } else {
      return null;
    }
  }

  @Override
  public boolean isHandledByEngine(int engineIndex) {
    return getEnginesStatus().isSuccess(engineIndex);
  }

  PendingInputImpl addNodeReaction(int engineIndex, LargeByteArray nodeOpening) {
    FixedList<LargeByteArray> updatedOpenings =
        RealContractBinder.BYTE_LIST.withEntry(nodeOpenings, engineIndex, nodeOpening);
    byte updatedEngines = EnginesStatus.setEngineBit(engineIndex, engines);
    return new PendingInputImpl(
        variable,
        publicKey,
        encryptedShares,
        updatedOpenings,
        commitments,
        updatedEngines,
        rejected,
        onInputtedShortname);
  }

  /**
   * Check if pending input is rejected.
   *
   * @return true if rejected, false otherwise
   */
  public boolean isRejected() {
    return rejected;
  }

  PendingInputImpl reject() {
    return new PendingInputImpl(
        variable,
        publicKey,
        encryptedShares,
        nodeOpenings,
        commitments,
        engines,
        true,
        onInputtedShortname);
  }

  /**
   * Get the number of nodes that have sent their share to this input.
   *
   * @return the number of node reactions to this input
   */
  public int getNodeReactions() {
    return Integer.bitCount(engines);
  }

  /**
   * Serialize into wasm byte representation of a pending variable. This is just the serialization
   * of the variable.
   *
   * @return wasm byte representation.
   */
  public byte[] serialize() {
    return this.variable.serialize();
  }

  /**
   * Computes the size of the variable when serialized to bytes.
   *
   * @return size of serialized bytes.
   */
  public int serializedByteLength() {
    return this.variable.serializedByteLength();
  }

  /**
   * Retrieves the shortname of the {@code zk_on_variable_inputted} function to call when the secret
   * variable is fully input.
   *
   * @return Shortname of {@code zk_on_variable_inputted} function.
   */
  public LargeByteArray getOnInputtedShortname() {
    return onInputtedShortname;
  }

  /**
   * Get which engines have handled this pending input.
   *
   * @return a byte where if the i'th bit has been set, then node i has handled the input
   */
  public EnginesStatus getEnginesStatus() {
    return new EnginesStatus(engines);
  }

  /**
   * Get whether this pending input is on chain or not.
   *
   * @return true if the input is on chain
   */
  public boolean isOnChainInput() {
    return encryptedShares != null;
  }
}
