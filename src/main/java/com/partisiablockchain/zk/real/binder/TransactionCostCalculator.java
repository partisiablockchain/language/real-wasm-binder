package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Utility class to help with calculating exact cost of a {@code InteractWithContractTransaction}.
 */
final class TransactionCostCalculator {

  private static final long NETWORK_MULTIPLIER = 5L;
  private static final long SELF_INVOKE_CPU = 25_000L;
  private static final long CPU_COST = 2_500L;

  private TransactionCostCalculator() {}

  /**
   * Calculate the exact cost of sending a payload and interacting with a system contract.
   *
   * @param payload to calculate for
   * @return exact cost
   */
  static long costOfSystemInteraction(byte[] payload) {
    return costOfInteraction(payload, CPU_COST);
  }

  /**
   * Calculate the exact cost of sending a payload with {@link #SELF_INVOKE_CPU} allocated for CPU.
   *
   * @param payload to calculate for
   * @return exact cost
   */
  static long costOfSelfInvoke(byte[] payload) {
    return costOfInteraction(payload, SELF_INVOKE_CPU);
  }

  private static long costOfInteraction(byte[] payload, long allocatedCpuCost) {
    long transactionTypeLength = 1L;
    long addressLength = 21;
    long payloadLengthPrefix = 4L;
    long networkBytes =
        transactionTypeLength + addressLength + payloadLengthPrefix + payload.length;

    return allocatedCpuCost + NETWORK_MULTIPLIER * networkBytes;
  }
}
