package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.language.wasminvoker.avltrees.ReadOnlyAvlTreeMapper;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.LittleEndianByteInput;
import com.secata.stream.LittleEndianByteOutput;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/** A read only avl tree where values are serialized with a custom serialization function. */
@SuppressWarnings("Immutable")
public final class SerializationAvlTree<@ImmutableTypeParameter V>
    implements ReadOnlyAvlTreeMapper {
  private final AvlTree<Integer, V> avlTree;
  private final Function<V, byte[]> valueSerializer;
  private final Function<V, Integer> valueSize;

  /**
   * Constructor.
   *
   * @param avlTree backing avl tree.
   * @param valueSerializer serialization implementation.
   * @param valueSize size computation.
   */
  public SerializationAvlTree(
      AvlTree<Integer, V> avlTree,
      Function<V, byte[]> valueSerializer,
      Function<V, Integer> valueSize) {
    this.avlTree = avlTree;
    this.valueSerializer = valueSerializer;
    this.valueSize = valueSize;
  }

  @Override
  public byte[] getValue(byte[] key) {
    int variableId = new LittleEndianByteInput(new ByteArrayInputStream(key)).readU32();
    V value = avlTree.getValue(variableId);
    if (value == null) {
      return null;
    }
    byte[] bytes = valueSerializer.apply(value);
    int expectedSize = valueSize.apply(value);
    if (bytes.length != expectedSize) {
      throw new RuntimeException(
          "Length of serialized value (%d) is not equal to expected size (%d)"
              .formatted(bytes.length, expectedSize));
    }
    return bytes;
  }

  @Override
  public int getValueSize(byte[] key) {
    int variableId = new LittleEndianByteInput(new ByteArrayInputStream(key)).readU32();
    V value = avlTree.getValue(variableId);
    if (value == null) {
      return -1;
    } else {
      return valueSize.apply(value);
    }
  }

  @Override
  public int size() {
    return avlTree.size();
  }

  @Override
  public Map.Entry<byte[], byte[]> getNextEntry(byte[] key) {
    Integer variableId =
        key == null ? null : new LittleEndianByteInput(new ByteArrayInputStream(key)).readU32();
    List<Map.Entry<Integer, V>> entries = this.avlTree.getNextN(variableId, 1);
    if (entries.isEmpty()) {
      return null;
    }
    byte[] valueBytes = valueSerializer.apply(entries.get(0).getValue());
    int entrySize = 4 + valueBytes.length;
    int expectedSize = 4 + valueSize.apply(entries.get(0).getValue());
    if (entrySize != expectedSize) {
      throw new RuntimeException(
          "Length of serialized entry (%d) is not equal to expected size (%d)"
              .formatted(entrySize, expectedSize));
    }
    return Map.entry(
        LittleEndianByteOutput.serialize(out -> out.writeI32(entries.get(0).getKey())), valueBytes);
  }

  @Override
  public int getNextEntrySize(byte[] key) {
    Integer variableId =
        key == null ? null : new LittleEndianByteInput(new ByteArrayInputStream(key)).readU32();
    List<Map.Entry<Integer, V>> entries = this.avlTree.getNextN(variableId, 1);
    if (entries.isEmpty()) {
      return -1;
    }
    return 4 + valueSize.apply(entries.get(0).getValue());
  }
}
