package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.zk.real.binder.ZkStateMutable.BASE_SERVICE_FEES;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.zk.CalculationStatus;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.realwasmbinder.WasmRealContractState;
import com.partisiablockchain.language.wasminvoker.Leb128Header;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.oracle.ConfirmedEventLog;
import com.partisiablockchain.oracle.EvmEventLog;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.util.ListUtil;
import com.partisiablockchain.zk.real.binder.ComputationStateImpl.ComputationResult;
import com.partisiablockchain.zk.real.binder.ComputationStateImpl.ComputationStateStatus;
import com.partisiablockchain.zk.real.binder.fee.ZkDeadlineCalculator;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.contract.RealContract;
import com.partisiablockchain.zk.real.contract.RealNodeState;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElementFactory;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The REAL contract binder, used to run REAL contracts on chain.
 *
 * <p>Handles the orchestration of on-chain operations based on either user input or REAL Node
 * reactions.
 *
 * <p>State is {@link ZkStateImmutable}.
 */
public abstract class RealContractBinder implements ZkBinderContract<ZkStateImmutable> {

  private static final Logger logger = LoggerFactory.getLogger(RealContractBinder.class);

  static final ListUtil<LargeByteArray> BYTE_LIST = ListUtil.createNull();
  static final ListUtil<EngineOutputInformation> COUNTER_LIST = ListUtil.createNull();

  private final RealContractInvoker contractInvoker;

  /** Domain separator for attestation data hashes. */
  public static final byte[] ATTESTATION_DOMAIN_SEPARATOR =
      "ZK_REAL_ATTESTATION".getBytes(StandardCharsets.UTF_8);

  /** Preprocess contract's shortname for generate batch request. */
  static final int PREPROCESS_GENERATE_BATCH_V4 = 0x07;

  /** Zk-node-registry contract's shortname for notify computation done v1. */
  static final int REGISTRY_NOTIFY_COMPUTATION_DONE_V1 = 0x05;

  /** Zk-node-registry contract's shortname for notify computation done v2. */
  static final int REGISTRY_NOTIFY_COMPUTATION_DONE_V2 = 0x12;

  /**
   * Create binder for a REAL contract.
   *
   * @param contractInvoker REAL contract invoker
   */
  public RealContractBinder(final RealContractInvoker contractInvoker) {
    this.contractInvoker = contractInvoker;
  }

  /**
   * Create a mutable state from the immutable state.
   *
   * @param immutable the immutable state
   * @param context the binder context
   * @return a mutable state
   */
  private ZkStateMutable makeMutableState(
      final ZkStateImmutable immutable, final ZkBinderContextImpl context) {
    return ZkStateMutable.createMutableStateBinary(immutable, context);
  }

  @Override
  public Class<ZkStateImmutable> getStateClass() {
    return ZkStateImmutable.class;
  }

  @Override
  public List<Class<?>> getStateClassTypeParameters() {
    Type[] typeArguments =
        ((ParameterizedType) getContract().getClass().getGenericSuperclass())
            .getActualTypeArguments();
    return List.of((Class<?>) typeArguments[0], (Class<?>) typeArguments[1]);
  }

  /**
   * Distributes remaining gas, fetches events and creates a result.
   *
   * @param state State contained in the result
   * @param context the contract context
   * @param eventCreator event creator handling gas distribution and result creation
   * @return An instance of BinderResult
   */
  final BinderResult<ZkStateImmutable, BinderInteraction> createResult(
      ZkStateImmutable state, ZkBinderContextImpl context, EventResult eventCreator) {
    return eventCreator.toBinderResult(state, context.availableGas());
  }

  @Override
  public BinderResult<ZkStateImmutable, BinderInteraction> create(
      ZkBinderContext context, byte[] bytes) {
    return SafeDataInputStream.readFully(
        bytes, rpc -> initializeContract(new ZkBinderContextImpl(context), rpc));
  }

  /**
   * Initialize the REAL contract's state. The following input is read from RPC.
   *
   * <ul>
   *   <li>Information list for the four engines selected to perform the computation for this
   *       contract.
   *   <li>Address of the preprocessing contract orchestrating preprocessed material for this
   *       contract.
   *   <li>Address of the node registry contract responsible for managing nodes and staked tokens
   *       allocated to this contract.
   *   <li>A UTC timestamp denoting when the stakes for this contract have expired, after which new
   *       computation cannot be started.
   *   <li>The amount of initial fees (in gas) that should be paid to the selected computation
   *       engines. The amount is determined on how many tokens the nodes have locked to this
   *       contract, and are calculated by the deployment contract.
   *   <li>The amount of input masks the binder should attempt to prefetch.
   *   <li>The amount of computation triples the binder should attempt to prefetch.
   *   <li>Remaining inputs from RPC is passed to the contract invoker.
   * </ul>
   *
   * @param rpc to read input from
   * @return result containing initial state and events
   */
  private BinderResult<ZkStateImmutable, BinderInteraction> initializeContract(
      ZkBinderContextImpl context, SafeDataInputStream rpc) {
    EngineState engines = EngineState.read(rpc);
    BlockchainAddress preprocessContract = BlockchainAddress.read(rpc);
    BlockchainAddress nodeRegistryContract = BlockchainAddress.read(rpc);
    long zkComputationDeadline = rpc.readLong();
    long initialStakingFees = rpc.readLong(); // For 28 days in gas
    int numInputMasksToPrefetch = rpc.readInt();
    int numTriplesToPrefetch = rpc.readInt();
    boolean defaultOptimisticMode = rpc.readBoolean();

    List<BlockchainAddress> engineIdentities =
        engines.getEngines().stream().map(RealNodeState.RealEngine::getIdentity).toList();
    context.payFromCaller(initialStakingFees, engineIdentities);

    ZkStateMutable mutable =
        makeMutableState(
            new ZkStateImmutable(
                engines,
                preprocessContract,
                nodeRegistryContract,
                zkComputationDeadline,
                defaultOptimisticMode),
            context);
    mutable.setPreprocessMaterialToPrefetch(
        PreProcessMaterialType.BINARY_INPUT_MASK, numInputMasksToPrefetch);
    mutable.setPreprocessMaterialToPrefetch(
        PreProcessMaterialType.BINARY_TRIPLE, numTriplesToPrefetch);

    RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
        contractInvoker.onCreate(context, mutable, rpc);
    mutable.setOpen(wasmStateAndEvents.wasmState());
    EventResult eventCreator = wasmStateAndEvents.eventResult();

    final PreprocessedMaterialRequester preProcessMaterialRequester =
        createPreProcessMaterialRequester(eventCreator, mutable.getState());
    final ComputationDoneNotifier notifier =
        createComputationDoneNotifier(eventCreator, mutable.getState());

    ZkStateImmutable state =
        mutable.finalizeInteraction(getComputation(), preProcessMaterialRequester, notifier);
    return createResult(state, context, eventCreator);
  }

  /**
   * Creates a {@link PreprocessedMaterialRequester} for preprocessing triples. Uses the provided
   * event creator for requesting triples from the preprocess contract.
   *
   * @param eventCreator creator of request triples event
   * @param state to retrieve some information from
   * @return Wanted requester
   */
  private static PreprocessedMaterialRequester createPreProcessMaterialRequester(
      final EventResult eventCreator, final ZkStateImmutable state) {
    return (batchType, numberOfBatches) ->
        requestPreProcessMaterialBatches(eventCreator, state, batchType, numberOfBatches);
  }

  /**
   * Creates a Notifier to signal that the zk computation is done. Uses the provided event creator
   * to notify the zk node registry contract.
   *
   * @param eventCreator creator of notify computation done event
   * @param state to retrieve some information from
   * @return Wanted notifier
   */
  private static ComputationDoneNotifier createComputationDoneNotifier(
      final EventResult eventCreator, ZkStateImmutable state) {
    List<EventResult.Event> invocations = eventCreator.getInvocations();
    if (invocations.stream().anyMatch(event -> isNotifyComputationDone(state, event))) {
      throw new RuntimeException("Contract cannot itself notify that it is done.");
    }
    return (status) -> notifyComputationDone(eventCreator, state, status);
  }

  /**
   * Test whether this event is a notify computation done invocation to the zk node registry
   * contract.
   *
   * @param state state of the contract. Contains the node registry's address.
   * @param event the event to test
   * @return true if the event is a notify computation done invocation
   */
  private static boolean isNotifyComputationDone(ZkStateImmutable state, EventResult.Event event) {
    if (event.rpc().length == 0) {
      return false;
    }
    byte invocationShortname = event.rpc()[0];
    boolean isNotifyDone =
        invocationShortname == REGISTRY_NOTIFY_COMPUTATION_DONE_V1
            || invocationShortname == REGISTRY_NOTIFY_COMPUTATION_DONE_V2;
    return event.contract().equals(state.getNodeRegistryContract()) && isNotifyDone;
  }

  /**
   * Retrieves computation.
   *
   * @return MPC computation for current contract
   */
  private RealComputationEnv<WasmRealContractState, LargeByteArray> getComputation() {
    return (RealComputationEnv<WasmRealContractState, LargeByteArray>)
        this.getContract().getComputation();
  }

  @Override
  public BinderResult<ZkStateImmutable, BinderInteraction> invoke(
      ZkBinderContext binderContext, ZkStateImmutable incoming, byte[] bytes) {
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    boolean topUpGas = bytes.length == 0;
    if (topUpGas) {
      final EventResult eventCreator = EventResult.create();
      final ZkStateMutable mutable = makeMutableState(incoming, context);

      final PreprocessedMaterialRequester preProcessMaterialRequester =
          createPreProcessMaterialRequester(eventCreator, incoming);
      final ComputationDoneNotifier notifier =
          createComputationDoneNotifier(eventCreator, incoming);

      ZkStateImmutable newState =
          mutable.finalizeInteraction(getComputation(), preProcessMaterialRequester, notifier);
      return createResult(newState, context, eventCreator);
    } else {
      return SafeDataInputStream.readFully(bytes, rpc -> invoke(context, incoming, rpc));
    }
  }

  private BinderResult<ZkStateImmutable, BinderInteraction> invoke(
      ZkBinderContextImpl context, ZkStateImmutable state, SafeDataInputStream rpc) {
    final BlockchainAddress from = context.getFrom();
    final ZkStateMutable mutable = makeMutableState(state, context);

    final int invocationIdentifier = rpc.readUnsignedByte();
    final Invocation invocation = INVOCATIONS.getOrDefault(invocationIdentifier, null);
    if (invocation == null) {
      throw new IllegalArgumentException(
          "Invocation with shortname 0x%02X does not exist".formatted(invocationIdentifier));
    }

    invocation.assertValidCaller(state, context, from);
    EventResult eventCreator = invocation.execute(context, state, contractInvoker, rpc, mutable);

    final PreprocessedMaterialRequester preProcessMaterialRequester =
        createPreProcessMaterialRequester(eventCreator, state);
    final ComputationDoneNotifier notifier = createComputationDoneNotifier(eventCreator, state);

    ZkStateImmutable newState =
        mutable.finalizeInteraction(getComputation(), preProcessMaterialRequester, notifier);
    return createResult(newState, context, eventCreator);
  }

  /** List of all invocation in the binder. */
  static Map<Integer, Invocation> INVOCATIONS =
      Stream.of(
              new InvokeAddBatches(),
              new InvokeGetComputationDeadline(),
              new InvokeCommitResultVariable(),
              new InvokeOpenInvocation(),
              new InvokeUnableToCalculate(),
              new InvokeOnComputeComplete(),
              new InvokeVariableOpeningShares(),
              new InvokeOnVariablesOpened(),
              new InvokeAddAttestationSignature(),
              new InvokeOnAttestationComplete(),
              new InvokeZeroKnowledgeInput(
                  InvokeZeroKnowledgeInput.IDENTIFIER_OFF_CHAIN,
                  VariableDeclarationInformationFromChain::readOffChainFromStream),
              new InvokeZeroKnowledgeInput(
                  InvokeZeroKnowledgeInput.IDENTIFIER_ON_CHAIN,
                  VariableDeclarationInformationFromChain::readOnChainFromStream),
              new InvokeOpenMaskedInput(),
              new InvokeRejectInput(),
              new InvokeOnVariableRejected(),
              new InvokeOnVariableInputted(),
              new InvokeExtendZkComputationDeadline(),
              new InvokeAddExternalEvent(),
              new InvokeOnExternalEvent(),
              new InvokeRegisterExternalHeartbeatBlock())
          .collect(Collectors.toUnmodifiableMap(Invocation::identifier, Function.identity()));

  /**
   * {@link Invocation} called when a batch of preprocessing material has been created for this
   * contract.
   *
   * <p>This invocation can be called only by the preprocessing contract.
   *
   * <p>Reads the following from the rpc:
   *
   * <ul>
   *   <li><code>{@link PreProcessMaterialType} batchType</code> The batch type, either input masks
   *       or multiplication triples
   *   <li><code>int batchId</code> The id of the batch
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeAddBatches implements Invocation {
    public static final byte IDENTIFIER = 0x12;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      // Parse RPC
      PreProcessMaterialType batchType = rpc.readEnum(PreProcessMaterialType.values());
      int batchId = rpc.readInt();

      // Perform invocation
      mutable.addPreProcessMaterialBatch(batchType, batchId);
      return EventResult.create();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      if (!caller.equals(state.getPreprocessContract())) {
        throw new IllegalArgumentException(
            "Only the preprocessing contract is allowed to add batches");
      }
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * Was used by the zk node registry to check if the contract has reached its computations
   * deadline, if the contract was not able to send "NOTIFY_DONE" itself.
   *
   * @deprecated Now, when invoked with an empty invocation, if the contract has reached its
   *     deadline it will always send "NOTIFY_DONE" with the computation statuses of the
   *     participating ZK nodes. The invocation is kept to make sure no other invocation uses this
   *     invocation identifier.
   */
  @Deprecated
  private static final class InvokeGetComputationDeadline implements Invocation {
    public static final byte IDENTIFIER = 0x0b;

    @Override
    public EventResult execute(
        ZkBinderContextImpl binderContext,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      throw new RuntimeException("Deprecated");
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {}

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called to send an rpc directly to the contract. Corresponds to an invocation
   * in a non-ZK contract.
   *
   * <p>This invocation can be called by any user.
   *
   * <p>This invocation can be expressed using an ABI, with pass-through to the inner contract.
   */
  private static final class InvokeOpenInvocation implements Invocation {
    public static final byte IDENTIFIER = 0x09;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
          contractInvoker.onOpenInput(context, mutable, mutable.getState().getOpenState(), rpc);
      mutable.setOpen(wasmStateAndEvents.wasmState());
      return wasmStateAndEvents.eventResult();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {}

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called by a ZK node when the optimistic attempt at running the calculation
   * fails. Resets the computation state and re-runs the computation using all ZK nodes.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>long calculationFor</code> The block time of the calculation
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeUnableToCalculate implements Invocation {
    public static final byte IDENTIFIER = 0x02;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      long calculationFor = rpc.readLong();
      EventResult eventCreator = EventResult.create();
      PreprocessedMaterialRequester preProcessMaterialRequester =
          createPreProcessMaterialRequester(eventCreator, state);
      if (isActiveCalculation(mutable, calculationFor)) {
        mutable.runNonOptimisticComputation(preProcessMaterialRequester);
      }
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(context.getFrom());
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called by a ZK node when it has successfully finished computation and
   * committed to a result. When this is called by the final ZK node, the computation is finished
   * and a self invocation to {@link InvokeOnComputeComplete InvokeOnComputeComplete} is created.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>long calculationFor</code> The block time of the calculation
   *   <li>Internal information needed to create the computation result
   * </ul>
   *
   * <p>This invocation cannot be expressed using an ABI.
   */
  private static final class InvokeCommitResultVariable implements Invocation {
    public static final byte IDENTIFIER = 0x00;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      EventResult eventCreator = EventResult.create();
      int engineIndex = mutable.getState().assertValidZkEngineNode(context.getFrom());
      long calculationFor = rpc.readLong();

      if (isActiveCalculation(mutable, calculationFor)) {

        final ComputationStateImpl computationState = state.getComputationState();
        final ComputationOutput computationOutput = computationState.getComputationOutput();
        final byte engines =
            EnginesStatus.setEngineBit(engineIndex, computationOutput.getEngines());

        long nextCalculateFor = computationState.getCalculateFor();
        final EngineOutputInformation engineOutputInformation;
        if (computationState.isOptimistic()) {
          final BinaryExtensionFieldElementFactory factory =
              new BinaryExtensionFieldElementFactory();
          if (computationOutput.isWaitingForOutput()) {
            if (engineIndex > 1) {
              throw new IllegalArgumentException("Node is not a part of the computation");
            } else {
              final Hash partialOpenings = Hash.read(rpc);
              final byte[] verificationSeed = rpc.readBytes(32);
              final byte[] linearCombinationElement = rpc.readBytes(factory.elementByteSize());
              engineOutputInformation =
                  EngineOutputInformation.createOptimisticOutput(
                      partialOpenings,
                      verificationSeed,
                      new LargeByteArray(linearCombinationElement));
              final boolean isLast =
                  Integer.bitCount(engines) == ComputationStateImpl.MALICIOUS_COUNT + 1;
              if (isLast) {
                nextCalculateFor = context.getBlockTime();
              }
            }
          } else {
            final byte[] linearCombinationElement = rpc.readBytes(factory.elementByteSize());
            engineOutputInformation =
                EngineOutputInformation.createOptimisticVerification(
                    new LargeByteArray(linearCombinationElement));
          }
        } else {
          final Hash partialOpenings = Hash.read(rpc);
          engineOutputInformation = EngineOutputInformation.createOutput(partialOpenings);
        }

        ComputationResult computationStateStatus =
            mutable.commitResultVariable(engineIndex, engineOutputInformation, nextCalculateFor);
        if (computationStateStatus.getStatus() == ComputationStateStatus.DONE) {
          final LargeByteArray onComputeCompleteShortname = mutable.getComputeCompleteShortname();
          List<Integer> ids = mutable.completeComputation(computationStateStatus.getPartialOpens());
          if (onComputeCompleteShortname != null) {
            invokeSelf(
                eventCreator,
                context,
                SafeDataOutputStream.serialize(
                    w -> {
                      w.writeByte(InvokeOnComputeComplete.IDENTIFIER);
                      w.writeInt(ids.size());
                      for (int id : ids) {
                        w.writeInt(id);
                      }
                      w.write(onComputeCompleteShortname.getData());
                    }));
          }
        } else if (computationStateStatus.getStatus() == ComputationStateStatus.MALICIOUS) {
          if (state.getComputationState().isOptimistic()) {
            PreprocessedMaterialRequester preProcessMaterialRequester =
                createPreProcessMaterialRequester(eventCreator, state);
            mutable.runNonOptimisticComputation(preProcessMaterialRequester);
          } else {
            logger.warn("Malicious activity detected in contract");
            mutable.malicious();
          }
        }
      } else {
        rpc.readAllBytes(); // Read whatever remains, in order to prevent a possible crash.
        mutable.handleInputForFinished(engineIndex, calculationFor);
      }
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called when the ZK computation has finished and all ZK nodes have committed
   * to a result.
   *
   * <p>This invocation is called by the binder itself from {@link InvokeCommitResultVariable
   * InvokeCommitResultVariable}.
   *
   * <p>Reads the following from the rpc:
   *
   * <ul>
   *   <li><code>List{@literal <Integer>} ids</code> The ids of variables created during the
   *       computation
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeOnComputeComplete implements Invocation {
    public static final byte IDENTIFIER = 0x0c;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      final List<Integer> ids = readList(rpc, SafeDataInputStream::readInt);

      final byte[] shortnameBytes = Leb128Header.readHeaderBytes(rpc);

      RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
          contractInvoker.onComputeComplete(
              context, mutable, mutable.getState().getOpenState(), ids, shortnameBytes);
      mutable.setOpen(wasmStateAndEvents.wasmState());
      return wasmStateAndEvents.eventResult();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      assertSelfCall(context, caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called by a ZK node during an opening of ZK variables. When called by the
   * enough ZK nodes to reconstruct the variables, the variables are now open, and a self invocation
   * to {@link InvokeOnVariablesOpened InvokeOnVariablesOpened} is created.
   *
   * <p>Will attempt to reconstruct the variables after three nodes have sent their shares. If
   * successful, the fourth input from a zk node will be ignored. Otherwise, the binder will wait
   * for the fourth input to try reconstructing again.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>int outputId</code> The id of the outputted variable
   *   <li><code>{@link LargeByteArray} shares</code> The ZK node's shares of the output
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeVariableOpeningShares implements Invocation {
    public static final byte IDENTIFIER = 0x01;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      EventResult eventCreator = EventResult.create();
      BlockchainAddress from = context.getFrom();
      final int engineIndex = mutable.getState().assertValidZkEngineNode(from);

      final int outputId = rpc.readInt();

      PendingOnChainOpenImpl pendingOutput =
          mutable.getState().getPendingOnChainOpensRaw().getValue(outputId);
      if (pendingOutput == null) {
        // If pendingOutput doesn't exist we assume that it has already received inputs from three
        // nodes, and therefore already been confirmed.
        rpc.readAllBytes();
        return eventCreator;
      }

      final PendingOnChainOpenImpl pendingOutputWithEngineShares =
          pendingOutput.withEngineShares(engineIndex, rpc, ZkStateMutable.createReader());

      final PendingOnChainOpenImpl updated =
          mutable.handlePendingOutput(pendingOutputWithEngineShares);

      if (updated != null) {
        mutable.mutate(builder -> builder.setPendingOnChainOpen(updated));
      } else {
        mutable.mutate(builder -> builder.removePendingOnChainOpen(outputId));
        invokeSelf(
            eventCreator,
            context,
            SafeDataOutputStream.serialize(
                w -> {
                  w.writeByte(InvokeOnVariablesOpened.IDENTIFIER);
                  w.writeInt(pendingOutput.getVariableIds().size());
                  pendingOutput.getVariableIds().forEach(w::writeInt);
                }));
      }
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called when the value of the outputs of a computation has been opened and
   * confirmed by all the ZK nodes.
   *
   * <p>This invocation is called by the binder itself from {@link InvokeVariableOpeningShares
   * InvokeOnChainOutput}.
   *
   * <p>Reads the following from the rpc:
   *
   * <ul>
   *   <li><code>List{@literal <Integer>} ids</code> The ids of the opened variables
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeOnVariablesOpened implements Invocation {
    public static final byte IDENTIFIER = 0x0d;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      final List<Integer> ids = readList(rpc, SafeDataInputStream::readInt);
      RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
          contractInvoker.onVariablesOpened(
              context, mutable, mutable.getState().getOpenState(), ids);
      mutable.setOpen(wasmStateAndEvents.wasmState());
      return wasmStateAndEvents.eventResult();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      assertSelfCall(context, caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called by a ZK node to sign data for which attestation has been requested.
   * When called by the final ZK node, the data has been attested, and a self invocation to {@link
   * InvokeOnAttestationComplete InvokeOnAttestationComplete} is created.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>int id</code> The id of the data to be attested
   *   <li><code>Signature signature</code> The signature of the ZK node on the data
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeAddAttestationSignature implements Invocation {
    public static final byte IDENTIFIER = 0x0a;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      EventResult eventCreator = EventResult.create();
      int attestationId = rpc.readInt();
      Signature signature = Signature.read(rpc);
      DataAttestation attestation = state.getAttestation(attestationId);
      if (attestation == null) {
        throw new IllegalArgumentException("Engine trying to attest invalid attestation");
      }

      byte[] attestedData = attestation.getData();
      Hash hash = createAttestationHash(context.getContractAddress(), attestedData);
      BlockchainPublicKey publicKey = signature.recoverPublicKey(hash);
      int nodeIndex = state.assertValidZkEngineNode(publicKey);
      if (nodeIndex == -1) {
        throw new IllegalArgumentException("Attestation from unknown engine");
      }

      if (mutable.updateAttestation(nodeIndex, attestationId, signature)) {
        invokeSelf(
            eventCreator,
            context,
            SafeDataOutputStream.serialize(
                w -> {
                  w.writeByte(InvokeOnAttestationComplete.IDENTIFIER);
                  w.writeInt(attestationId);
                }));
      }
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called when all ZK nodes have delivered their signature to a piece of data
   * for which attestation was requested.
   *
   * <p>This invocation is called by the binder itself from {@link InvokeAddAttestationSignature
   * InvokeAddAttestationSignature}.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>int id</code> The id of the attested data
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeOnAttestationComplete implements Invocation {
    public static final byte IDENTIFIER = 0x0e;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      int id = rpc.readInt();
      RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
          contractInvoker.onAttestationComplete(
              context, mutable, mutable.getState().getOpenState(), id);
      mutable.setOpen(wasmStateAndEvents.wasmState());
      return wasmStateAndEvents.eventResult();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      assertSelfCall(context, caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called when a user adds a secret variable.
   *
   * <p>The variable is either added on chain or off chain. If the variable is added on chain, the
   * user provides shares of the variable for each of the ZK nodes. If the variable is added off
   * chain, the user provides share commitments, and then provides the shares directly to the ZK
   * nodes through their REST endpoint.
   *
   * <p>The invocation creates pending input for the ZK nodes to handle. If all ZK nodes receive
   * correct shares, the variable will be inputted in {@link InvokeOnVariableInputted
   * InvokeOnVariableInputted}. Otherwise, the variable will be rejected in {@link
   * InvokeOnVariableRejected InvokeOnVariableRejected}.
   *
   * <p>This invocation can be called by any user.
   *
   * <p>For off chain input, reads the following from rpc:
   *
   * <ul>
   *   <li><code>List{@literal <Integer>} bitlengths</code> The bit lengths of each inputted
   *       variable
   *   <li><code>FixedList{@literal <Hash>} hashes</code> hashes of shares to be sent to the ZK
   *       nodes
   *   <li><code>{@link SafeDataInputStream} rpc</code> the remaining rpc sent to the rust contract
   * </ul>
   *
   * <p>For on chain input, reads the following from rpc:
   *
   * <ul>
   *   <li><code>List{@literal <Integer>} bitlengths</code> The bit lengths of each input of the
   *       secret variable
   *   <li><code>{@link BlockchainPublicKey} publicKey</code> the public key of the secret variable
   *   <li><code>FixedList{@literal <LargeByteArray>} encryptedShares</code> the shares sent to the
   *       ZK nodes
   *   <li><code>{@link SafeDataInputStream} rpc</code> the remaining rpc sent to the rust contract,
   *       which depends on the type of the input
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI, conditional on being different formats due to
   * handlesOffChain.
   */
  private static final class InvokeZeroKnowledgeInput implements Invocation {
    /** Specifies whether this invocation handles on-chain or off-chain secret inputs. */
    private final BiFunction<SafeDataInputStream, Integer, VariableDeclarationInformationFromChain>
        readDefinitionFromStream;

    private final int identifier;

    private static final int IDENTIFIER_OFF_CHAIN = 0x04;
    private static final int IDENTIFIER_ON_CHAIN = 0x05;

    public InvokeZeroKnowledgeInput(
        int identifier,
        BiFunction<SafeDataInputStream, Integer, VariableDeclarationInformationFromChain>
            readDefinitionFromStream) {
      this.identifier = identifier;
      this.readDefinitionFromStream = readDefinitionFromStream;
    }

    @Override
    public int identifier() {
      return identifier;
    }

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      final VariableDeclarationInformationFromChain variableDeclarationInformationFromChain =
          readDefinitionFromStream.apply(rpc, state.getEngineIdentities().size());

      final RealContractInvoker.WasmStateAndEventsAndVariableDefinition wasmStateAndEvents =
          contractInvoker.onSecretInput(context, mutable, mutable.getState().getOpenState(), rpc);
      final RealContractInvoker.VariableDefinition variableDefinition =
          wasmStateAndEvents.variableDefinition();
      mutable.setOpen(wasmStateAndEvents.wasmState());
      EventResult eventCreator = wasmStateAndEvents.eventResult();
      validateInputBitLengths(
          variableDefinition.bitLengths(), variableDeclarationInformationFromChain.bitLengths());
      final PreprocessedMaterialRequester preProcessMaterialRequester =
          createPreProcessMaterialRequester(eventCreator, state);

      mutable.createPendingInput(
          context.getOriginalTransactionHash(),
          context.getFrom(),
          variableDefinition.seal(),
          new LargeByteArray(variableDefinition.metadata()),
          variableDefinition.bitLengths(),
          variableDeclarationInformationFromChain.publicKey(),
          variableDeclarationInformationFromChain.encryptedShares(),
          variableDeclarationInformationFromChain.commitments(),
          preProcessMaterialRequester,
          wasmStateAndEvents.variableDefinition().onInputtedShortname());
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {}
  }

  /**
   * {@link Invocation} called by ZK nodes during input of a secret variable.
   *
   * <p>After a user has inputted a secret variable using {@link InvokeZeroKnowledgeInput
   * InvokeZeroKnowledgeInput}, enough ZK nodes must confirm that they have received their shares of
   * the input, by calling this invocation.
   *
   * <p>When called by the third ZK node, the binder attempts to reconstruct the blinded input. If
   * this goes successfully, a self invocation to {@link InvokeOnVariableInputted
   * InvokeOnVariableInputted} is created and the fourth input is ignored. Otherwise, the binder
   * waits for the fourth input.
   *
   * <p>When the fourth input is received, the binder attempts to reconstruct the variable again. If
   * this is successful a self invocation to {@link InvokeOnVariableInputted
   * InvokeOnVariableInputted} is created, otherwise a self invocation to {@link
   * InvokeOnVariableRejected InvokeOnVariableRejected} is created.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>int variableId</code> The ID of the new secret variable
   *   <li><code>{@link LargeByteArray} maskedShares</code> The masked shares of the variable
   * </ul>
   *
   * <p>This invocation cannot be expressed using an ABI.
   */
  private static final class InvokeOpenMaskedInput implements Invocation {
    public static final byte IDENTIFIER = 0x03;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      final int variableId = rpc.readInt();
      EventResult eventCreator = EventResult.create();

      // Check that pending exists
      PendingInputImpl pending = state.getPendingInput().getValue(variableId);
      if (pending == null) {
        // If pending doesn't exist we assume that it has already received inputs from three nodes,
        // and therefore already been confirmed.
        rpc.readAllBytes();
        return eventCreator;
      }

      BlockchainAddress from = context.getFrom();

      // Perform update of the pending input
      final Integer numOfShares =
          ZkStateMutable.shareCountFromLengths(pending.getVariable().getShareBitLengths());
      final LargeByteArray maskedInput = new LargeByteArray(rpc.readBytes(numOfShares));
      final PendingInputImpl pendingInputResult =
          mutable.handleOpenOfMaskedInput(maskedInput, pending, from);

      if (pendingInputResult == null) {
        final LargeByteArray onInputtedShortname = pending.getOnInputtedShortname();
        final boolean shortnameExists = onInputtedShortname != null;
        if (shortnameExists) {
          invokeSelf(
              eventCreator,
              context,
              SafeDataOutputStream.serialize(
                  w -> {
                    w.writeByte(InvokeOnVariableInputted.IDENTIFIER);
                    w.writeInt(variableId);
                    w.write(onInputtedShortname.getData());
                  }));
        }
      } else if (pendingInputResult.isRejected()) {
        mutable.removePendingInput(variableId);
        invokeSelf(
            eventCreator,
            context,
            SafeDataOutputStream.serialize(
                w -> {
                  w.writeByte(InvokeOnVariableRejected.IDENTIFIER);
                  w.writeInt(variableId);
                }));
      }
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called by a ZK node in order to reject a pending secret variable, stopping
   * it from being inputted. This happens if the user that inputted the variable delivers incorrect
   * shares to a ZK node. The invocation rejects the variable and immediately creates a self
   * invocation to {@link InvokeOnVariableRejected InvokeOnVariableRejected}.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>int variableId</code> The ID of the rejected secret variable
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeRejectInput implements Invocation {
    public static final byte IDENTIFIER = 0x06;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      EventResult eventCreator = EventResult.create();
      int variableId = rpc.readInt();
      PendingInputImpl pending = state.getPendingInput().getValue(variableId);
      if (pending != null) {
        mutable.mutate(builder -> builder.removePendingInput(variableId));
        invokeSelf(
            eventCreator,
            context,
            SafeDataOutputStream.serialize(
                w -> {
                  w.writeByte(InvokeOnVariableRejected.IDENTIFIER);
                  w.writeInt(variableId);
                }));
      }
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called when a pending secret variable has been rejected.
   *
   * <p>This invocation is called by the binder itself either from {@link InvokeOpenMaskedInput
   * InvokeOpenMaskedInput} if the binder cannot reconstruct a masked input using the shares sent by
   * the ZK nodes, or from {@link InvokeRejectInput InvokeRejectInput} if a ZK node has received
   * incorrect shares from the user that inputted the variable.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>int variableId</code> The ID of the rejected secret variable
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeOnVariableRejected implements Invocation {
    public static final byte IDENTIFIER = 0x10;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      int variableId = rpc.readInt();
      RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
          contractInvoker.onVariableRejected(
              context, mutable, mutable.getState().getOpenState(), variableId);
      mutable.setOpen(wasmStateAndEvents.wasmState());
      return wasmStateAndEvents.eventResult();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      assertSelfCall(context, caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called when a secret variable has been inputted by a user and confirmed by
   * all the ZK nodes.
   *
   * <p>This invocation is called by the binder itself from {@link InvokeOpenMaskedInput
   * InvokeOpenMaskedInput}.
   *
   * <p>Reads the following from rpc:
   *
   * <ul>
   *   <li><code>int variableId</code> The ID of the new secret variable
   * </ul>
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeOnVariableInputted implements Invocation {
    public static final byte IDENTIFIER = 0x0f;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      int variableId = rpc.readInt();
      final byte[] shortnameBytes = Leb128Header.readHeaderBytes(rpc);
      RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
          contractInvoker.onVariableInputted(
              context, mutable, mutable.getState().getOpenState(), variableId, shortnameBytes);
      mutable.setOpen(wasmStateAndEvents.wasmState());
      return wasmStateAndEvents.eventResult();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      assertSelfCall(context, caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /**
   * {@link Invocation} called by anyone (user or contracts) to pay ZK computation nodes additional
   * staking fees, extending the computation deadline of the contract.
   *
   * <p>Nothing is read from RPC, the amount of gas determines how much to pay the nodes and how
   * much the deadline should be extended.
   *
   * <p>This invocation can be expressed using an ABI.
   */
  private static final class InvokeExtendZkComputationDeadline implements Invocation {
    private static final byte IDENTIFIER = 0x13;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      // Fraction of how much many ms one (1) can extend the deadline
      long msPerGasNumerator = rpc.readLong();
      long msPerGasDenominator = rpc.readLong();
      // Minimum amount of ms to extend from current deadline
      long minExtension = rpc.readLong();
      // Maximum amount of ms to extend to from current block production time
      Duration maxExtension = Duration.ofMillis(rpc.readLong());
      assertValidContractState(state, context, maxExtension);

      ZkDeadlineCalculator extensionCalculator =
          ZkDeadlineCalculator.create(
              msPerGasNumerator,
              msPerGasDenominator,
              Instant.ofEpochMilli(state.getComputationDeadline()),
              minExtension,
              maxExtension);
      ZkDeadlineCalculator.Result zkExtension =
          extensionCalculator.calculateFor(
              Instant.ofEpochMilli(context.getBlockProductionTime()), context.availableGas());

      List<BlockchainAddress> engineIdentities = state.getEngineIdentities();
      context.payFromCaller(zkExtension.gasToPay(), engineIdentities);

      mutable.mutate(builder -> builder.extendComputationDeadline(zkExtension.deadlineExtension()));
      return EventResult.create();
    }

    private void assertValidContractState(
        ZkStateImmutable state, ZkBinderContext context, Duration maxExtension) {
      String errorMessage = "Unable to pay staking fees - %s";
      if (context.getBlockProductionTime() > state.getComputationDeadline()) {
        throw new IllegalStateException(
            errorMessage.formatted("computation deadline has been reached"));
      }
      CalculationStatus status = state.getCalculationStatus();
      if (status == CalculationStatus.DONE || status == CalculationStatus.MALICIOUS_BEHAVIOUR) {
        throw new IllegalStateException(
            errorMessage.formatted("calculation status in final state"));
      }
      Instant currentDeadline = Instant.ofEpochMilli(state.getComputationDeadline());
      Instant deadlineLimit =
          Instant.ofEpochMilli(context.getBlockProductionTime()).plus(maxExtension);
      if (currentDeadline.toEpochMilli() >= deadlineLimit.toEpochMilli()) {
        String durationString = maxExtension.toDaysPart() + " days";
        throw new IllegalStateException(
            errorMessage.formatted(
                "current deadline is more than " + durationString + " in the future"));
      }
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      BlockchainAddress registryAddress = state.getNodeRegistryContract();
      if (!caller.equals(registryAddress)) {
        String errMsg =
            String.format(
                "Only node registry allowed to pay staking fees - Expected caller address %s, but"
                    + " was %s",
                registryAddress.writeAsString(), caller.writeAsString());
        throw new IllegalArgumentException(errMsg);
      }
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  private static final class InvokeAddExternalEvent implements Invocation {
    private static final byte IDENTIFIER = 0x14;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      EventResult eventCreator = EventResult.create();

      BlockchainAddress evmOracle = context.getFrom();
      int subscriptionId = rpc.readInt();
      EvmEventLog observedEvent = EvmEventLog.read(rpc);
      ConfirmedEventLog result =
          mutable.handleObservedEvent(evmOracle, subscriptionId, observedEvent);

      if (result != null) {
        invokeSelf(
            eventCreator,
            context,
            SafeDataOutputStream.serialize(
                w -> {
                  w.writeByte(InvokeOnExternalEvent.IDENTIFIER);
                  w.writeInt(result.subscriptionId());
                  w.writeInt(result.eventId());
                }));

        // Pay in advance for the next event.
        context.payFromContract(BASE_SERVICE_FEES, state.getEngineIdentities());
      }
      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  private static final class InvokeOnExternalEvent implements Invocation {

    private static final byte IDENTIFIER = 0x15;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      int subscriptionId = rpc.readInt();
      int eventId = rpc.readInt();

      RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
          contractInvoker.onExternalEvents(
              context, mutable, mutable.getState().getOpenState(), subscriptionId, eventId);
      mutable.setOpen(wasmStateAndEvents.wasmState());
      return wasmStateAndEvents.eventResult();
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      assertSelfCall(context, caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  private static final class InvokeRegisterExternalHeartbeatBlock implements Invocation {
    private static final byte IDENTIFIER = 0x16;

    @Override
    public EventResult execute(
        ZkBinderContextImpl context,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable) {
      EventResult eventCreator = EventResult.create();

      BlockchainAddress evmOracle = context.getFrom();
      int subscriptionId = rpc.readInt();
      Unsigned256 heartbeat = Unsigned256.read(rpc);

      Unsigned256 confirmedHeartbeat =
          mutable.registerHeartbeat(evmOracle, subscriptionId, heartbeat);
      if (confirmedHeartbeat != null) {
        // Pay the nodes for sending the next event.
        context.payFromContract(BASE_SERVICE_FEES, state.getEngineIdentities());
      }

      return eventCreator;
    }

    @Override
    public void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller) {
      state.assertValidZkEngineNode(caller);
    }

    @Override
    public int identifier() {
      return IDENTIFIER;
    }
  }

  /** Implemented by all invocations on the {@link RealContractBinder}. */
  interface Invocation {

    /**
     * Executes the invocation.
     *
     * @param binderContext the context of the binder
     * @param state the immutable contract state
     * @param rpc the rpc sent along with the invocation
     * @param mutable the mutable contract state
     */
    EventResult execute(
        ZkBinderContextImpl binderContext,
        ZkStateImmutable state,
        RealContractInvoker contractInvoker,
        SafeDataInputStream rpc,
        ZkStateMutable mutable);

    /**
     * Asserts that the caller is valid for this invocation. Throws an exception in case it is not.
     *
     * @param state the immutable contract state
     * @param context the contract context
     * @param caller the sender of the invocation
     */
    void assertValidCaller(
        ZkStateImmutable state, ZkBinderContext context, BlockchainAddress caller);

    /**
     * Gets the identifier byte of the invocation.
     *
     * @return the identifier byte
     */
    int identifier();
  }

  /**
   * Create a hash based on the contract address, the attested data and some predefined domain
   * bytes.
   *
   * @param contractAddress of the current ZK contract
   * @param attestedData bytes of the attested data
   * @return attestation hash
   */
  public static Hash createAttestationHash(BlockchainAddress contractAddress, byte[] attestedData) {
    return Hash.create(
        s -> {
          s.write(ATTESTATION_DOMAIN_SEPARATOR);
          contractAddress.write(s);
          s.write(attestedData);
        });
  }

  private static void assertSelfCall(ZkBinderContext context, BlockchainAddress caller) {
    if (!caller.equals(context.getContractAddress())) {
      String errorMsg =
          String.format(
              "Invocation can only be executed by the contract itself. Caller was %s, contract"
                  + " address is %s",
              caller, context.getContractAddress());
      throw new IllegalArgumentException(errorMsg);
    }
  }

  @Override
  public BinderResult<ZkStateImmutable, BinderInteraction> callback(
      final ZkBinderContext binderContext,
      ZkStateImmutable zkStateImmutable,
      CallbackContext callbackContext,
      byte[] rpc) {
    ZkBinderContextImpl context = new ZkBinderContextImpl(binderContext);
    ZkStateMutable mutable = makeMutableState(zkStateImmutable, context);
    RealContractInvoker.WasmStateAndEvents wasmStateAndEvents =
        SafeDataInputStream.readFully(
            rpc,
            safeDataInputStream ->
                contractInvoker.onCallback(
                    context,
                    mutable,
                    zkStateImmutable.getOpenState(),
                    callbackContext,
                    safeDataInputStream));
    EventResult eventCreator = wasmStateAndEvents.eventResult();
    PreprocessedMaterialRequester preProcessMaterialRequester =
        createPreProcessMaterialRequester(eventCreator, zkStateImmutable);
    ComputationDoneNotifier notifier =
        createComputationDoneNotifier(eventCreator, zkStateImmutable);
    mutable.setOpen(wasmStateAndEvents.wasmState());
    ZkStateImmutable newState =
        mutable.finalizeInteraction(getComputation(), preProcessMaterialRequester, notifier);
    return createResult(newState, context, eventCreator);
  }

  /**
   * Reads share commitments from a stream for the given amount of engines.
   *
   * @param rpc Stream to read from
   * @param engineCount Number of engines to read for
   * @return engineCount-length list of commitments
   */
  private static FixedList<Hash> readShareCommitments(SafeDataInputStream rpc, int engineCount) {
    List<Hash> hashes = new ArrayList<>();
    for (int i = 0; i < engineCount; i++) {
      hashes.add(Hash.read(rpc));
    }
    return FixedList.create(hashes);
  }

  /**
   * Validates that two lists of bit lengths are equal; will throw otherwise.
   *
   * @param expectedBitLengths Expected bit lengths
   * @param bitLengths Gotten bit lengths
   * @throws IllegalArgumentException If bitLengths is not equal to expectedBitLengths
   */
  private static void validateInputBitLengths(
      final List<Integer> expectedBitLengths, final List<Integer> bitLengths) {
    if (!expectedBitLengths.equals(bitLengths)) {
      throw new IllegalArgumentException(
          "Expected an input of sizes " + expectedBitLengths + ", but got sizes " + bitLengths);
    }
  }

  /**
   * Reads encrypted shares from a stream, for a given number of engines.
   *
   * @param rpc Stream to read shares from
   * @param engineCount Number of engines to read for
   * @return engineCount-length list of shares as byte arrays
   * @throws IllegalStateException If read shares doesn't have equal size.
   */
  private static FixedList<LargeByteArray> readEncryptedShares(
      SafeDataInputStream rpc, int engineCount) {
    // We assume that all encrypted shares have the same size.
    byte[] bytes = rpc.readDynamicBytes();
    if (bytes.length % engineCount != 0) {
      throw new IllegalStateException("Invalid number of bytes received for input");
    }
    List<LargeByteArray> shares = new ArrayList<>();
    int shareSize = bytes.length / engineCount;
    for (int i = 0; i < engineCount; i++) {
      byte[] share = new byte[shareSize];
      System.arraycopy(bytes, i * shareSize, share, 0, shareSize);
      shares.add(new LargeByteArray(share));
    }
    return FixedList.create(shares);
  }

  private static void notifyComputationDone(
      EventResult eventCreator, ZkStateImmutable state, EnginesStatus status) {
    List<BlockchainAddress> engines = state.getEngineIdentities();
    byte[] payload =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(REGISTRY_NOTIFY_COMPUTATION_DONE_V2);
              stream.writeInt(engines.size());
              for (int i = 0; i < engines.size(); i++) {
                engines.get(i).write(stream);
                if (status.isSuccess(i)) {
                  stream.writeEnum(EnginesStatus.ZkNodeScoreStatus.SUCCESS);
                } else {
                  stream.writeEnum(EnginesStatus.ZkNodeScoreStatus.FAILURE);
                }
              }
            });
    createEvent(eventCreator, state.getNodeRegistryContract(), payload);
  }

  /**
   * Uses the given event creator to request triple batches.
   *
   * @param eventCreator Event creator to request through
   * @param state State to retrieve a bit of required information from
   * @param batchType Batch type to request
   * @param numBatches Number of batches to request
   */
  private static void requestPreProcessMaterialBatches(
      EventResult eventCreator,
      ZkStateImmutable state,
      PreProcessMaterialType batchType,
      int numBatches) {
    byte[] payload = generateBatchPayload(batchType, numBatches, state.getParticipatingEngines());
    long cost =
        TransactionCostCalculator.costOfSystemInteraction(payload)
            + calculatePreprocessingFees(numBatches, batchType);
    createEvent(eventCreator, state.getPreprocessContract(), payload, cost);
  }

  private static byte[] generateBatchPayload(
      PreProcessMaterialType inputMask, int numBatches, List<RealNodeState.RealEngine> engines) {
    final byte addBatchesShortname = InvokeAddBatches.IDENTIFIER;
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(PREPROCESS_GENERATE_BATCH_V4); // GENERATE_BATCH
          stream.writeEnum(inputMask);
          stream.writeByte(numBatches);
          for (RealNodeState.RealEngine engine : engines) {
            engine.getIdentity().write(stream);
            engine.getPublicKey().write(stream);
          }

          // Specify the shortname to invoke once batch is fully generated
          stream.writeByte(addBatchesShortname);
        });
  }

  /**
   * Calculates the fees associated with ordering preprocessing material. The fees consist of fees
   * for the amount of preprocessing material requested and service fees for the transactions that
   * the zk computation nodes must make.
   *
   * <p>Preprocessing material costs 5 USD cent per 1000 triples requested.
   *
   * <p>The service fees should cover the following for the real nodes:
   *
   * <ul>
   *   <li>2 transactions to the preprocess contract.
   * </ul>
   *
   * @param numberOfBatches the number of batches of preprocessing material to order
   * @param batchType the type of the batches to order
   * @return the fees for ordering the preprocessing material
   */
  public static long calculatePreprocessingFees(
      int numberOfBatches, PreProcessMaterialType batchType) {
    long noOfTriplesPerBatch = batchType.getBatchSize();

    // 1 USD cent = 1000 gas
    // Material fees per batch = (noOfTriplesPerBatch / 1000) * 5 * 1000 = noOfTriplesPerBatch * 5
    long preprocessingMaterialFees = noOfTriplesPerBatch * 5;

    long preprocessTransactionFees = 2 * BASE_SERVICE_FEES;

    return (preprocessingMaterialFees + preprocessTransactionFees) * numberOfBatches;
  }

  /**
   * Whether the ZK computation for the given state is currently active.
   *
   * @param mutable State to determine active calculation for
   * @param calculationFor Block time calculation is happening for
   * @return true if the ZK computation is active.
   */
  private static boolean isActiveCalculation(
      final ZkStateMutable mutable, final long calculationFor) {
    return mutable.getState().getCalculationStatus() == CalculationStatus.CALCULATING
        && calculationFor == mutable.getState().getComputationState().getCalculateFor();
  }

  @Override
  public final RealContract<WasmRealContractState, LargeByteArray> getContract() {
    return contractInvoker.getContract();
  }

  /**
   * Internal definition information for a variable, given by the blockchain, and required along
   * with {@link com.partisiablockchain.language.realwasmbinder.WasmRealContractInvoker
   * .VariableDefinition} for storing variable into state.
   *
   * @param bitLengths Bit-lengths supplied by the sender.
   * @param encryptedShares Encrypted shares.
   * @param commitments Commitments for the variable.
   * @param publicKey Public key for the variable.
   */
  @SuppressWarnings("UnusedVariable")
  private record VariableDeclarationInformationFromChain(
      List<Integer> bitLengths,
      FixedList<LargeByteArray> encryptedShares,
      FixedList<Hash> commitments,
      BlockchainPublicKey publicKey) {

    static VariableDeclarationInformationFromChain readOffChainFromStream(
        final SafeDataInputStream stream, final int numEngines) {
      final List<Integer> bitLengths = readList(stream, SafeDataInputStream::readInt);
      final FixedList<Hash> commitments = readShareCommitments(stream, numEngines);
      return new VariableDeclarationInformationFromChain(bitLengths, null, commitments, null);
    }

    static VariableDeclarationInformationFromChain readOnChainFromStream(
        final SafeDataInputStream stream, final int numEngines) {
      final List<Integer> bitLengths = readList(stream, SafeDataInputStream::readInt);
      final BlockchainPublicKey publicKey = BlockchainPublicKey.read(stream);
      final FixedList<LargeByteArray> encryptedShares = readEncryptedShares(stream, numEngines);
      return new VariableDeclarationInformationFromChain(
          bitLengths, encryptedShares, null, publicKey);
    }
  }

  private static <E> List<E> readList(
      final SafeDataInputStream stream, final Function<SafeDataInputStream, E> elementReader) {
    final int listSize = stream.readInt();
    final List<E> ids = new ArrayList<E>();
    for (int i = 0; i < listSize; i++) {
      ids.add(elementReader.apply(stream));
    }
    return ids;
  }

  private static void invokeSelf(
      EventResult eventCreator, ZkBinderContext binderContext, byte[] payload) {
    long cost = TransactionCostCalculator.costOfSelfInvoke(payload);
    createEvent(eventCreator, binderContext.getContractAddress(), payload, cost);
  }

  private static void createEvent(
      EventResult eventCreator, BlockchainAddress address, byte[] payload) {
    long cost = TransactionCostCalculator.costOfSystemInteraction(payload);
    createEvent(eventCreator, address, payload, cost);
  }

  private static void createEvent(
      EventResult eventCreator, BlockchainAddress address, byte[] payload, long cost) {
    eventCreator.addEventWithCostFromContract(address, payload, cost);
  }
}
