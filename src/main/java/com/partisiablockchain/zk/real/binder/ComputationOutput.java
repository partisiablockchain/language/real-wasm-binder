package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.zk.real.binder.ComputationStateImpl.ComputationResult;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElementFactory;
import com.partisiablockchain.zk.real.protocol.field.Lagrange;
import com.partisiablockchain.zk.real.protocol.field.Polynomial;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Computation output. Contains a list of variables that are being create as well as their open
 * information.
 */
@Immutable
public final class ComputationOutput implements StateSerializable {

  private final byte engines;
  private final FixedList<PendingOutputVariable> variables;
  private final FixedList<EngineOutputInformation> outputInformation;

  @SuppressWarnings("unused")
  private ComputationOutput() {
    this((byte) 0, null, null);
  }

  ComputationOutput(FixedList<PendingOutputVariable> variables) {
    this((byte) 0, variables, FixedList.create());
  }

  ComputationOutput(
      byte engines,
      FixedList<PendingOutputVariable> variables,
      FixedList<EngineOutputInformation> outputInformation) {
    this.engines = engines;
    this.variables = variables;
    this.outputInformation = outputInformation;
  }

  /**
   * Get the engines.
   *
   * @return engines as byte
   */
  public byte getEngines() {
    return engines;
  }

  /**
   * Get pending output variables.
   *
   * @return list of pending output variables
   */
  public FixedList<PendingOutputVariable> getVariables() {
    return variables;
  }

  /**
   * Get information for outputs.
   *
   * @return list of engine output information
   */
  public FixedList<EngineOutputInformation> getOutputInformation() {
    return outputInformation;
  }

  /**
   * Check if computation is waiting for output.
   *
   * @return true if waiting, false otherwise
   */
  public boolean isWaitingForOutput() {
    return Integer.bitCount(engines) < 2;
  }

  /**
   * Check if computation is waiting for verification.
   *
   * @return true if waiting, false otherwise
   */
  public boolean isWaitingForVerification() {
    return !isWaitingForOutput();
  }

  ComputationOutput reset() {
    return new ComputationOutput(variables);
  }

  /**
   * Get number of engines.
   *
   * @return number of engines
   */
  public int getEngineCount() {
    return Integer.bitCount(engines);
  }

  boolean verifyOptimisticOutputs() {
    FixedList<EngineOutputInformation> computationNodes =
        outputInformation.subList(0, ComputationStateImpl.MALICIOUS_COUNT + 1);
    EngineOutputInformation first = computationNodes.get(0);
    for (EngineOutputInformation computationNode : computationNodes) {
      if (!computationNode.isConsistentWith(first)) {
        return false;
      }
    }
    return true;
  }

  <ElementT extends FiniteFieldElement<ElementT>> boolean reconstructVerification(
      FiniteFieldElementFactory<ElementT> factory) {
    List<ElementT> computationAlphas = factory.alphas();
    List<ElementT> alphas = new ArrayList<>();
    List<ElementT> values = new ArrayList<>();
    for (int i = 0; i < outputInformation.size(); i++) {
      EngineOutputInformation engineOutputInformation = outputInformation.get(i);
      if (engineOutputInformation != null) {
        alphas.add(computationAlphas.get(i));
        values.add(
            factory.createElement(engineOutputInformation.getLinearCombinationElement().getData()));
      }
    }
    Polynomial<ElementT> interpolate =
        Lagrange.interpolateIfPossible(
            alphas, values, ComputationStateImpl.MALICIOUS_COUNT, factory.zero(), factory.one());
    return interpolate != null;
  }

  ComputationResult verifyOutput() {
    Map<EngineOutputInformation, Integer> counts = new HashMap<>();
    for (EngineOutputInformation engineOutputInformation : outputInformation) {
      if (engineOutputInformation != null) {
        counts.compute(
            findKeyToUse(counts, engineOutputInformation),
            (key, current) -> current == null ? 1 : current + 1);
      }
    }
    int engineCount = ComputationStateImpl.MALICIOUS_COUNT + 1;
    for (Map.Entry<EngineOutputInformation, Integer> entry : counts.entrySet()) {
      if (entry.getValue() > engineCount) {
        EngineOutputInformation key = entry.getKey();
        return ComputationResult.createDone(key.getPartialOpenings());
      }
    }
    return getEngineCount() < 3 * ComputationStateImpl.MALICIOUS_COUNT + 1
        ? ComputationResult.createPending()
        : ComputationResult.createMalicious();
  }

  private EngineOutputInformation findKeyToUse(
      Map<EngineOutputInformation, Integer> counts,
      EngineOutputInformation engineOutputInformation) {
    for (EngineOutputInformation information : counts.keySet()) {
      if (engineOutputInformation.isConsistentWith(information)) {
        return information;
      }
    }
    return engineOutputInformation;
  }

  <ElementT extends FiniteFieldElement<ElementT>> ComputationResult status(
      boolean isOptimistic, FiniteFieldElementFactory<ElementT> factory) {
    int engineCount = getEngineCount();
    if (isOptimistic) {
      if (engineCount == ComputationStateImpl.MALICIOUS_COUNT + 1) {
        if (verifyOptimisticOutputs()) {
          return ComputationResult.createPending();
        } else {
          return ComputationResult.createMalicious();
        }
      } else if (engineCount >= 2 * ComputationStateImpl.MALICIOUS_COUNT + 1) {
        if (reconstructVerification(factory)) {
          EngineOutputInformation information = outputInformation.get(0);
          return ComputationResult.createDone(information.getPartialOpenings());
        } else if (engineCount < 3 * ComputationStateImpl.MALICIOUS_COUNT + 1) {
          return ComputationResult.createPending();
        } else {
          return ComputationResult.createMalicious();
        }
      } else {
        return ComputationResult.createPending();
      }
    } else {
      if (engineCount >= 2 * ComputationStateImpl.MALICIOUS_COUNT + 1) {
        return verifyOutput();
      } else {
        return ComputationResult.createPending();
      }
    }
  }

  Hash getPartialOpenings() {
    return outputInformation.get(0).getPartialOpenings();
  }

  byte[] getVerificationSeed() {
    return outputInformation.get(0).getVerificationSeed();
  }
}
