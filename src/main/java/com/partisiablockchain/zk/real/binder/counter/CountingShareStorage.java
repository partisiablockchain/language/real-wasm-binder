package com.partisiablockchain.zk.real.binder.counter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.zk.ZkComputationState;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.IntFunction;

/**
 * Share storage class used when counting the number of multiplications needed for a zk evaluation
 * using binary REAL.
 *
 * @param <OpenT> variable type.
 * @param <ZkOpenT> zero-knowledge variable type.
 */
public final class CountingShareStorage<
        NumberT extends SharedNumber<NumberT>,
        OpenT extends StateSerializable,
        ZkOpenT extends StateSerializable>
    implements SharedNumberStorage<NumberT> {

  private final ZkComputationState<OpenT, ZkOpenT, RealClosed<ZkOpenT>> computationState;
  private final List<Integer> outputLengths;
  private final IntFunction<NumberT> zeroNumber;

  /**
   * Default constructor.
   *
   * @param computationState state of computation
   * @param zeroNumber Function for generating new zero element of some size.
   */
  public CountingShareStorage(
      final ZkComputationState<OpenT, ZkOpenT, RealClosed<ZkOpenT>> computationState,
      final IntFunction<NumberT> zeroNumber) {
    this.computationState = computationState;
    this.outputLengths = new ArrayList<>();
    this.zeroNumber = zeroNumber;
  }

  /**
   * Create new counting share storage for {@link BinaryExtensionFieldElement}.
   *
   * @param <OpenT> variable type.
   * @param <ZkOpenT> zero-knowledge variable type.
   * @param computationState State to load data from.
   * @return Newly created counting share storage.
   */
  public static <OpenT extends StateSerializable, ZkOpenT extends StateSerializable>
      CountingShareStorage<SecretSharedNumberAsBits, OpenT, ZkOpenT> forBinary(
          final ZkComputationState<OpenT, ZkOpenT, RealClosed<ZkOpenT>> computationState) {

    return new CountingShareStorage<>(
        computationState,
        size ->
            SecretSharedNumberAsBits.create(
                Collections.nCopies(size, BinaryExtensionFieldElement.ZERO)));
  }

  /**
   * Returns the collection of values associated with a variable. The returned collection stores
   * bits and numbers in the same order as they were initially provided.
   *
   * @param variableId the identifier of the bit variable
   * @return the values associated with a variable
   */
  @Override
  public List<NumberT> loadNumber(int variableId) {
    final RealClosed<ZkOpenT> variable = computationState.getVariable(variableId);
    if (variable == null) {
      return null;
    } else {
      return variable.getShareBitLengths().stream().map(zeroNumber::apply).toList();
    }
  }

  @Override
  public void saveNumber(NumberT secretSharedNumber) {
    outputLengths.add(secretSharedNumber.bitLength());
  }

  /**
   * Get bit-lengths of outputs.
   *
   * @return list of bit-lengths
   */
  public List<Integer> bitLengths() {
    return outputLengths;
  }
}
