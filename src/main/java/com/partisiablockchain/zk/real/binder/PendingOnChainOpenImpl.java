package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zk.real.contract.RealNodeState.PendingOnChainOpen;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.IntFunction;

/** Holds information about a pending on-chain open event. */
@Immutable
public final class PendingOnChainOpenImpl implements StateSerializable, PendingOnChainOpen {

  private final OpenId outputId;
  private final FixedList<Integer> variables;
  private final AvlTree<Integer, BitLengths> bitLengths;
  private final AvlTree<Integer, Shares> shares;

  @SuppressWarnings("unused")
  PendingOnChainOpenImpl() {
    outputId = null;
    variables = null;
    bitLengths = null;
    shares = null;
  }

  PendingOnChainOpenImpl(
      int id, FixedList<Integer> variables, AvlTree<Integer, BitLengths> bitLengths) {
    this.outputId = new OpenId(id);
    this.variables = variables;
    this.bitLengths = bitLengths;
    this.shares = AvlTree.create();
  }

  private PendingOnChainOpenImpl(
      OpenId outputId,
      FixedList<Integer> variables,
      AvlTree<Integer, BitLengths> bitLengths,
      AvlTree<Integer, Shares> shares) {
    this.outputId = outputId;
    this.variables = variables;
    this.bitLengths = bitLengths;
    this.shares = shares;
  }

  @Override
  public int getOutputId() {
    return outputId.getId();
  }

  @Override
  public List<Integer> getVariableIds() {
    return new ArrayList<>(variables);
  }

  @Override
  public boolean isHandledByEngine(int engineIndex) {
    return getEnginesStatus().isSuccess(engineIndex);
  }

  /**
   * Get the number of nodes that have sent their share to this input.
   *
   * @return the number of node reactions to this input
   */
  public int getNodeReactions() {
    return Integer.bitCount(outputId.getReceivedForEngine());
  }

  PendingOnChainOpenImpl withEngineShares(
      int engineIndex, SafeDataInputStream stream, Shares.ShareReader reader) {
    AvlTree<Integer, Shares> newShares = shares;
    for (Integer variable : variables) {
      Shares currentShares = Objects.requireNonNullElseGet(shares.getValue(variable), Shares::new);
      newShares =
          newShares.set(
              variable,
              currentShares.withShares(bitLengths.getValue(variable), engineIndex, stream, reader));
    }
    OpenId updatedId = this.outputId.withReceivedForEngine(engineIndex);
    return new PendingOnChainOpenImpl(updatedId, variables, bitLengths, newShares);
  }

  /** Functional interface for reconstructing a single variable. */
  @FunctionalInterface
  interface Reconstructor {

    /**
     * Attempt to reconstruct a single variable given shares of it. If it fails return null
     *
     * @param shares list of shares for each node
     * @param bitLengths the bit lengths of the variable
     * @return the reconstructed variable, or null if reconstruction fails
     */
    LargeByteArray reconstruct(List<LargeByteArray> shares, FixedList<Integer> bitLengths);
  }

  /**
   * Attempts to reconstruct secrets for each variable of this {@link PendingOnChainOpenImpl}. If a
   * single variable fails to reconstruct none of the variables are opened and null is returned.
   *
   * @param lookup function from variable id to zk variable
   * @param reconstructor function for reconstructing a single variable from shares
   * @return the list of opened variable, or null if any reconstruct fails
   */
  List<ZkClosedImpl> reconstructValues(
      IntFunction<ZkClosedImpl> lookup, Reconstructor reconstructor) {
    List<ZkClosedImpl> list = new ArrayList<>();
    for (Integer variable : variables) {
      LargeByteArray reconstructedValue =
          reconstructor.reconstruct(
              new ArrayList<>(shares.getValue(variable).getShares()),
              bitLengths.getValue(variable).bitLengths);

      if (reconstructedValue == null) {
        return null;
      }

      ZkClosedImpl closed = lookup.apply(variable);
      // The variable is null if it has been deleted during the opening
      // In that case do nothing
      if (closed != null) {
        list.add(closed.withOpen(reconstructedValue));
      }
    }
    return list;
  }

  /**
   * Get which engines have handled this pending on chain open.
   *
   * @return a byte where if the i'th bit has been set, then node i has handled the on chain open
   */
  public EnginesStatus getEnginesStatus() {
    return new EnginesStatus((byte) outputId.getReceivedForEngine());
  }

  /**
   * The bit lengths of the elements of a secret variable.
   *
   * @param bitLengths List of bitlengths of variable.
   */
  @Immutable
  public record BitLengths(FixedList<Integer> bitLengths) implements StateSerializable {}
}
