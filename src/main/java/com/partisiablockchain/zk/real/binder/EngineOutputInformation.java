package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Arrays;
import java.util.Objects;

@Immutable
final class EngineOutputInformation implements StateSerializable {

  private final Hash partialOpenings;
  private final LargeByteArray verificationSeed;
  private final LargeByteArray linearCombinationElement;

  @SuppressWarnings("unused")
  EngineOutputInformation() {
    partialOpenings = null;
    verificationSeed = null;
    linearCombinationElement = new LargeByteArray(new byte[0]);
  }

  private EngineOutputInformation(
      Hash partialOpenings,
      LargeByteArray verificationSeed,
      LargeByteArray linearCombinationElement) {
    this.partialOpenings = partialOpenings;
    this.verificationSeed = verificationSeed;
    this.linearCombinationElement = linearCombinationElement;
  }

  static EngineOutputInformation createOptimisticOutput(
      Hash partialOpenings, byte[] verificationSeed, LargeByteArray linearCombinationElement) {
    return new EngineOutputInformation(
        partialOpenings, new LargeByteArray(verificationSeed), linearCombinationElement);
  }

  static EngineOutputInformation createOptimisticVerification(
      LargeByteArray linearCombinationElement) {
    return new EngineOutputInformation(null, null, linearCombinationElement);
  }

  static EngineOutputInformation createOutput(Hash partialOpenings) {
    return new EngineOutputInformation(partialOpenings, null, new LargeByteArray(new byte[0]));
  }

  Hash getPartialOpenings() {
    return partialOpenings;
  }

  byte[] getVerificationSeed() {
    if (verificationSeed == null) {
      return null;
    } else {
      return verificationSeed.getData();
    }
  }

  LargeByteArray getLinearCombinationElement() {
    return linearCombinationElement;
  }

  public boolean isConsistentWith(EngineOutputInformation information) {
    return Objects.equals(this.partialOpenings, information.partialOpenings)
        && Arrays.equals(this.getVerificationSeed(), information.getVerificationSeed());
  }
}
