package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.List;

/** Interface for paying service fees. */
public interface ServicePayerWithGas {
  /**
   * Pay service fees where gas should be extracted from gas payment sent by the caller.
   *
   * @param fees to pay
   * @param receivers to register payment to
   */
  void payFromCaller(long fees, List<BlockchainAddress> receivers);

  /**
   * Pay service fees where gas should be extracted from the contract's balance.
   *
   * @param fees to pay
   * @param receivers to register payment to
   */
  void payFromContract(long fees, List<BlockchainAddress> receivers);
}
