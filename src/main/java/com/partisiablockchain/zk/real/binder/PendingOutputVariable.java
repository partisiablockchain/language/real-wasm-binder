package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;

/** A variable in an output from a computation. Contains information about the variable. */
@Immutable
public final class PendingOutputVariable implements StateSerializable {

  private final int variableId;
  private final LargeByteArray information;
  private final int bitLength;

  @SuppressWarnings("unused")
  PendingOutputVariable() {
    variableId = -1;
    information = null;
    bitLength = 0;
  }

  PendingOutputVariable(int variableId, LargeByteArray information, int bitLength) {
    this.variableId = variableId;
    this.information = information;
    this.bitLength = bitLength;
  }

  /**
   * Retrieves id of the output variable.
   *
   * @return id of the variable
   */
  public int getVariableId() {
    return variableId;
  }

  LargeByteArray getInformation() {
    return information;
  }

  int getBitLength() {
    return bitLength;
  }
}
