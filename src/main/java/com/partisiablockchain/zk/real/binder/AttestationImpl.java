package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.contract.zk.DataAttestation;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.ListUtil;
import com.secata.stream.LittleEndianByteOutput;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.Objects;

/** {@link DataAttestation} implementation for REAL contracts. */
@Immutable
public final class AttestationImpl implements DataAttestation, StateSerializable {

  private final FixedList<Signature> signatures;
  private final LargeByteArray data;
  private final int id;

  private static final ListUtil<Signature> listHelper = ListUtil.createNull();

  @SuppressWarnings("unused")
  AttestationImpl() {
    this.signatures = null;
    this.data = null;
    this.id = 0;
  }

  private AttestationImpl(int id, FixedList<Signature> signatures, LargeByteArray data) {
    this.signatures = signatures;
    this.data = data;
    this.id = id;
  }

  static AttestationImpl create(int id, byte[] data) {
    return new AttestationImpl(id, FixedList.create(), new LargeByteArray(data));
  }

  AttestationImpl addAttestation(int nodeIndex, Signature signature) {
    return new AttestationImpl(id, listHelper.withEntry(signatures, nodeIndex, signature), data);
  }

  /**
   * Returns true if this attestation has been signed by all the computation nodes.
   *
   * @return true if all nodes have signed this attestation and false otherwise.
   */
  boolean hasEnoughAttestations() {
    return signatures.stream().filter(Objects::nonNull).count() == 4;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public byte[] getData() {
    return data.getData();
  }

  private Signature oobSafeGet(int index) {
    if (index >= signatures.size()) {
      return null;
    } else {
      return signatures.get(index);
    }
  }

  @Override
  public Signature getSignature(int nodeIndex) {
    return oobSafeGet(nodeIndex);
  }

  /**
   * Serialize into wasm byte representation of an attestation.
   *
   * @return wasm byte representation.
   */
  public byte[] serialize() {
    return LittleEndianByteOutput.serialize(
        out -> {
          // Attestation id first
          out.writeI32(this.getId());

          // Signatures
          out.writeI32(4);
          for (int nodeId = 0; nodeId < 4; nodeId++) {
            Signature signature = this.getSignature(nodeId);
            if (signature == null) {
              out.writeBoolean(false);
            } else {
              out.writeBoolean(true);
              out.writeBytes(SafeDataOutputStream.serialize(signature::write));
            }
          }

          // Attested data itself
          out.writeI32(this.getData().length);
          out.writeBytes(this.getData());
        });
  }

  /**
   * Computes the size of the attestation when serialized to bytes.
   *
   * @return size of serialized bytes.
   */
  public int serializedByteLength() {
    int size = 0;
    // Attestation id first
    size += 4;

    // Signatures
    size += 4;
    for (int nodeId = 0; nodeId < 4; nodeId++) {
      Signature signature = this.getSignature(nodeId);
      size += 1;
      if (signature != null) {
        size += 65;
      }
    }

    // Attested data itself
    size += 4;
    size += this.data.getLength();

    return size;
  }

  /**
   * Get which engines have handled this attestation.
   *
   * @return a byte where if the i'th bit has been set, then node i has handled the attestation
   */
  public EnginesStatus getEnginesStatus() {
    byte status = 0;
    for (int i = 0; i < signatures.size(); i++) {
      if (signatures.get(i) != null) {
        status = EnginesStatus.setEngineBit(i, status);
      }
    }
    return new EnginesStatus(status);
  }
}
