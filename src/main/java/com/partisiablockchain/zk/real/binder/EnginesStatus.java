package com.partisiablockchain.zk.real.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * Statuses for each engine. Is used to keep track of whether each node has performed some task.
 * Each node can have the status of either success or failure.
 *
 * @param engines compact representation of the engine statuses. If the i'th bit is set, then engine
 *     i has status success.
 */
public record EnginesStatus(byte engines) {

  /** Engine status where all nodes are a success. */
  public static EnginesStatus ALL_SUCCESS = new EnginesStatus((byte) 0b11111111);

  /** Engine status where all nodes are a failure. */
  public static EnginesStatus ALL_FAILURE = new EnginesStatus((byte) 0b00000000);

  /**
   * Take this engine status and 'and' it with all the other statuses. The result follows that for
   * each engine, it is a success iff it is a success for all inputs.
   *
   * @param others list of other engine statuses
   * @return the result of the 'and'
   */
  public EnginesStatus andAll(List<EnginesStatus> others) {
    return others.stream().reduce(this, EnginesStatus::and);
  }

  /**
   * Take two engine statuses and 'and' them together such that the result for each engine is a
   * success iff that engine is a success in both of the inputs.
   *
   * @param other the other status to 'and' with
   * @return the result of the 'and'
   */
  public EnginesStatus and(EnginesStatus other) {
    return new EnginesStatus((byte) (this.engines & other.engines));
  }

  /**
   * Check if engine with the given index is a success.
   *
   * @param engineIndex the engine index to test
   * @return true if the given engine status is success
   */
  public boolean isSuccess(int engineIndex) {
    return ((1 << engineIndex) & engines) != 0;
  }

  /**
   * Set the index of an engine in a bit mask. Ensures that the value has not previously been set.
   *
   * @param engineIndex the engine to set the bit for
   * @param currentValue the current bit mask
   * @return the new bit mask
   */
  static byte setEngineBit(int engineIndex, byte currentValue) {
    int mask = 1 << engineIndex;
    if ((currentValue & mask) != 0) {
      throw new RuntimeException("Engine is only allowed to push once");
    }
    return (byte) (currentValue | mask);
  }

  /** Score status of a computation. */
  public enum ZkNodeScoreStatus {
    /** The computation was not successful. */
    FAILURE,
    /** The computation was successful. */
    SUCCESS,
  }
}
