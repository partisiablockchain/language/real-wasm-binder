package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElementFactory;

/** Simple implementation of {@link RealProtocol}. */
public record RealProtocolImpl<
        NumberT extends SharedNumber<NumberT>,
        BitT extends SharedBit<BitT>,
        ElementT extends FiniteFieldElement<ElementT>>(
    SharedNumberStorage<NumberT> storage,
    BitManipulation<BitT> bits,
    ShareFactory<NumberT, BitT> shareFactory,
    NumberManipulation<NumberT, BitT> numeric,
    FiniteFieldElementFactory<ElementT> elementFactory)
    implements RealProtocol<NumberT, BitT, ElementT> {

  /**
   * Create new Real Protocol impl.
   *
   * @param storage Share storage.
   * @param bits Bit manipulation.
   * @param shareFactory Share factory.
   * @param numeric Number manipulation.
   * @param elementFactory Element factory.
   */
  public RealProtocolImpl {
    requireNonNull(storage);
    requireNonNull(bits);
    requireNonNull(shareFactory);
    requireNonNull(numeric);
    requireNonNull(elementFactory);
  }
}
