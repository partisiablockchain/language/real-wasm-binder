package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.SharedBit;
import java.util.List;

/**
 * Interface for operating on numbers. Some operations, such as comparisons, will return shares of
 * bits rather than numbers.
 *
 * @param <NumberT> the representation of a number.
 * @param <BitT> the representation of a bit.
 */
public interface NumberManipulation<NumberT, BitT extends SharedBit<BitT>> {

  /**
   * Computes equality of left and right.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left == right</code>
   */
  List<BitT> parallelEq(List<NumberT> left, List<NumberT> right);

  /**
   * Computes signed <code>&le;</code> of left and right.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &le; right</code>
   */
  BitT leSigned(NumberT left, NumberT right);

  /**
   * Computes signed <code>&gt;</code> of left and right.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &gt; right</code>
   */
  default BitT gtSigned(NumberT left, NumberT right) {
    return leSigned(left, right).not();
  }

  /**
   * Computes unsigned <code>&le;</code> of left and right.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &le; right</code>
   */
  BitT leUnsigned(NumberT left, NumberT right);

  /**
   * Computes unsigned <code>&gt;</code> of left and right.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &gt; right</code>
   */
  default BitT gtUnsigned(NumberT left, NumberT right) {
    return leUnsigned(left, right).not();
  }

  /**
   * Computes sum of left and right.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left + right</code>
   */
  List<NumberT> parallelAdd(List<NumberT> left, List<NumberT> right);

  /**
   * Computes the product of left and right.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left * right</code>
   */
  NumberT mult(NumberT left, NumberT right);

  /**
   * Subtract two secret-shard numbers.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left - right</code>
   */
  NumberT subtract(NumberT left, NumberT right);

  /**
   * Computes the dividend divided by divisor.
   *
   * @param dividend the dividend
   * @param divisor the divisor
   * @return <code>dividend / divisor</code>
   */
  NumberT div(NumberT dividend, NumberT divisor);

  /**
   * Select either trueValue or falseValue based on the selector bit.
   *
   * @param selector bit indicating the value to select
   * @param trueValue the value to return if selector is true
   * @param falseValue the value to return if selector is false
   * @return trueValue if selector is true, falseValue otherwise
   */
  NumberT selectNumber(BitT selector, NumberT trueValue, NumberT falseValue);

  /**
   * Checks if the number is zero.
   *
   * @param number the number checked.
   * @return Whether all bits are zero or not.
   */
  BitT isZero(NumberT number);

  /**
   * This checks if the number is negative.
   *
   * @param number the number checked.
   * @return If negative.
   */
  BitT isNegative(NumberT number);
}
