package com.partisiablockchain.zk.real.protocol2.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.BatchUtility;
import com.partisiablockchain.zk.real.protocol2.BitManipulation;
import com.partisiablockchain.zk.real.protocol2.NumberManipulation;
import com.partisiablockchain.zk.real.protocol2.ShareFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** Class for manipulating binary shares of integers. */
public final class BinaryNumberManipulation
    implements NumberManipulation<SecretSharedNumberAsBits, BinaryExtensionFieldElement> {

  private final BitManipulation<BinaryExtensionFieldElement> bitManipulator;
  private final ShareFactory<SecretSharedNumberAsBits, BinaryExtensionFieldElement> factory;

  /**
   * Create an object capable of manipulating bit-wise secret bitManipulator numbers.
   *
   * @param bitManipulator an object that can perform manipulation on secret bitManipulator bits.
   * @param factory a share factory.
   */
  public BinaryNumberManipulation(
      BitManipulation<BinaryExtensionFieldElement> bitManipulator,
      ShareFactory<SecretSharedNumberAsBits, BinaryExtensionFieldElement> factory) {
    this.bitManipulator = bitManipulator;
    this.factory = factory;
  }

  @Override
  public BinaryExtensionFieldElement leSigned(
      SecretSharedNumberAsBits left, SecretSharedNumberAsBits right) {
    verifySize(left, right);
    BinaryExtensionFieldElement carry = cmp(left, right);
    int bitLength = left.size();
    return xor(xor(bitManipulator.not(carry), left.get(bitLength - 1)), right.get(bitLength - 1));
  }

  @Override
  public BinaryExtensionFieldElement leUnsigned(
      SecretSharedNumberAsBits left, SecretSharedNumberAsBits right) {
    verifySize(left, right);
    BinaryExtensionFieldElement carry = cmp(left, right);
    return bitManipulator.not(carry);
  }

  private BinaryExtensionFieldElement cmp(
      SecretSharedNumberAsBits left, SecretSharedNumberAsBits right) {
    int bitLength = left.size();
    List<BinaryExtensionFieldElement> negatedRight =
        right.stream().map(bitManipulator::not).collect(Collectors.toList());
    BinaryExtensionFieldElement carry =
        internalAddIncludingLastCarry(left, factory.number(negatedRight)).get(bitLength);
    return carry;
  }

  private BinaryExtensionFieldElement xor(
      BinaryExtensionFieldElement left, BinaryExtensionFieldElement right) {
    return bitManipulator.multiXor(List.of(left), List.of(right)).get(0);
  }

  @Override
  public List<BinaryExtensionFieldElement> parallelEq(
      List<SecretSharedNumberAsBits> leftElements, List<SecretSharedNumberAsBits> rightElements) {
    verifySizes(leftElements, rightElements);

    // Determine differences
    final List<List<BinaryExtensionFieldElement>> negatedDifferences = new ArrayList<>();
    for (int idx = 0; idx < leftElements.size(); idx++) {
      final List<BinaryExtensionFieldElement> differentBits =
          bitManipulator.multiXor(leftElements.get(idx), rightElements.get(idx));
      final List<BinaryExtensionFieldElement> identicalBits =
          differentBits.stream().map(BinaryExtensionFieldElement::not).toList();
      negatedDifferences.add(identicalBits);
    }

    // Determine whether any differences occured.
    return BatchUtility.parallelFoldByBatchedTree(
        factory.constant(true), negatedDifferences, bitManipulator::multiAnd);
  }

  @Override
  public List<SecretSharedNumberAsBits> parallelAdd(
      List<SecretSharedNumberAsBits> left, List<SecretSharedNumberAsBits> right) {
    verifySizes(left, right);
    List<List<BinaryExtensionFieldElement>> sums =
        parallelInternalAddIncludingLastCarry(left, right);
    return sums.stream().map(sum -> factory.number(sum.subList(0, sum.size() - 1))).toList();
  }

  @Override
  public SecretSharedNumberAsBits mult(
      SecretSharedNumberAsBits left, SecretSharedNumberAsBits right) {
    verifySize(left, right);
    final int bitLength = left.bitLength();
    List<BinaryExtensionFieldElement> runningSum = andBitInto(right.get(0), left);
    final List<BinaryExtensionFieldElement> result = new ArrayList<>();
    result.add(runningSum.get(0));
    for (int i = 1; i < bitLength; i++) {
      int nextBitLength = bitLength - i;
      right = right.slice(1, nextBitLength + 1);
      left = left.slice(0, nextBitLength);
      runningSum = runningSum.subList(1, nextBitLength + 1);
      List<BinaryExtensionFieldElement> toAdd = andBitInto(right.get(0), left);
      runningSum = internalAddIncludingLastCarry(factory.number(runningSum), factory.number(toAdd));
      result.add(runningSum.get(0));
    }
    return factory.number(result);
  }

  @Override
  public SecretSharedNumberAsBits subtract(
      SecretSharedNumberAsBits left, SecretSharedNumberAsBits right) {
    verifySize(left, right);
    throw new UnsupportedOperationException("Subtraction has not been implemented yet");
  }

  private List<BinaryExtensionFieldElement> andBitInto(
      BinaryExtensionFieldElement bitLeft, SecretSharedNumberAsBits bitsRight) {
    final List<BinaryExtensionFieldElement> bitsLeft =
        BatchUtility.repeat(bitsRight.size(), bitLeft);
    return bitManipulator.multiAnd(bitsLeft, bitsRight);
  }

  @Override
  public SecretSharedNumberAsBits div(
      SecretSharedNumberAsBits dividend, SecretSharedNumberAsBits divisor) {
    verifySize(dividend, divisor);
    throw new UnsupportedOperationException("Division has not been implemented yet");
  }

  @Override
  public SecretSharedNumberAsBits selectNumber(
      BinaryExtensionFieldElement selector,
      SecretSharedNumberAsBits trueValue,
      SecretSharedNumberAsBits falseValue) {
    verifySize(trueValue, falseValue);
    return factory.number(bitManipulator.selectBits(selector, trueValue, falseValue));
  }

  @Override
  public BinaryExtensionFieldElement isZero(SecretSharedNumberAsBits number) {
    return bitManipulator.orReducer(List.copyOf(number)).not();
  }

  @Override
  public BinaryExtensionFieldElement isNegative(SecretSharedNumberAsBits number) {
    return number.get(number.size() - 1);
  }

  private List<BinaryExtensionFieldElement> internalAddIncludingLastCarry(
      SecretSharedNumberAsBits left, SecretSharedNumberAsBits right) {
    return parallelInternalAddIncludingLastCarry(List.of(left), List.of(right)).get(0);
  }

  private List<List<BinaryExtensionFieldElement>> parallelInternalAddIncludingLastCarry(
      final List<SecretSharedNumberAsBits> left, final List<SecretSharedNumberAsBits> right) {
    final int inputBitwidth = left.get(0).size();

    final List<List<BinaryExtensionFieldElement>> results =
        left.stream()
            .map(
                l ->
                    (List<BinaryExtensionFieldElement>)
                        new ArrayList<BinaryExtensionFieldElement>(1 + inputBitwidth))
            .toList();
    final List<BinaryExtensionFieldElement> firstLeft = column(left, 0);
    final List<BinaryExtensionFieldElement> firstRight = column(right, 0);

    insertColumns(results, bitManipulator.multiXor(firstLeft, firstRight));

    List<BinaryExtensionFieldElement> carry = bitManipulator.multiAnd(firstLeft, firstRight);

    for (int i = 1; i < inputBitwidth; i++) {
      final List<BinaryExtensionFieldElement> currentLeft = column(left, i);
      final List<BinaryExtensionFieldElement> currentRight = column(right, i);

      final List<BinaryExtensionFieldElement> leftXorCarry =
          bitManipulator.multiXor(carry, currentLeft);
      final List<BinaryExtensionFieldElement> leftXorRight =
          bitManipulator.multiXor(currentLeft, currentRight);
      final List<BinaryExtensionFieldElement> resultBit =
          bitManipulator.multiXor(carry, leftXorRight);
      final List<BinaryExtensionFieldElement> andBeforeXor =
          bitManipulator.multiAnd(leftXorCarry, leftXorRight);

      insertColumns(results, resultBit);

      carry = bitManipulator.multiXor(currentLeft, andBeforeXor);
    }

    insertColumns(results, carry);
    return results;
  }

  private static void insertColumns(
      List<List<BinaryExtensionFieldElement>> results, List<BinaryExtensionFieldElement> column) {
    for (int nrIdx = 0; nrIdx < results.size(); nrIdx++) {
      results.get(nrIdx).add(column.get(nrIdx));
    }
  }

  private static List<BinaryExtensionFieldElement> column(
      List<SecretSharedNumberAsBits> numbers, int columnIdx) {
    return numbers.stream().map(n -> n.get(columnIdx)).toList();
  }

  private void verifySize(SecretSharedNumberAsBits left, SecretSharedNumberAsBits right) {
    int leftLength = left.bitLength();
    int rightLength = right.bitLength();
    if (leftLength != rightLength) {
      throw new IllegalArgumentException(
          "Operands must be of same length: %s vs %s".formatted(leftLength, rightLength));
    }
  }

  private void verifySizes(
      List<SecretSharedNumberAsBits> left, List<SecretSharedNumberAsBits> right) {
    if (left.size() != right.size()) {
      throw new IllegalArgumentException(
          "Operand lists must be of same length: %s vs %s".formatted(left.size(), right.size()));
    }
    for (int i = 0; i < left.size(); i++) {
      verifySize(left.get(i), right.get(i));
    }
  }
}
