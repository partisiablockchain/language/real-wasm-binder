package com.partisiablockchain.zk.real.protocol2.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.RealEnvironment;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElementFactory;
import com.partisiablockchain.zk.real.protocol2.BitManipulation;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import com.partisiablockchain.zk.real.protocol2.RealProtocolImpl;
import com.partisiablockchain.zk.real.protocol2.ShareFactory;

/** Real protocol factory for {@link SecretSharedNumberAsBits}. */
public final class BinaryProtocolFactory {

  private BinaryProtocolFactory() {}

  /**
   * Create new {@link RealProtocol} for {@link SecretSharedNumberAsBits}.
   *
   * @param storage Share storage for protocol
   * @param multiplier {@link ElementMultiplier} for protocol.
   * @return Newly created {@link RealProtocol}.
   */
  public static RealProtocol<
          SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
      create(
          SharedNumberStorage<SecretSharedNumberAsBits> storage,
          ElementMultiplier<BinaryExtensionFieldElement> multiplier) {
    final ShareFactory<SecretSharedNumberAsBits, BinaryExtensionFieldElement> shareFactory =
        new BinaryShareFactory();
    final BitManipulation<BinaryExtensionFieldElement> bits =
        new BinaryBitManipulation<>(multiplier, shareFactory, BinaryExtensionFieldElement::xor);
    return new RealProtocolImpl<
        SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>(
        storage,
        bits,
        shareFactory,
        new BinaryNumberManipulation(bits, shareFactory),
        new BinaryExtensionFieldElementFactory());
  }

  /**
   * Create new {@link RealProtocol} for {@link SecretSharedNumberAsBits}.
   *
   * @param realEnvironment Environment to create new protocol from.
   * @return Newly created {@link RealProtocol}.
   */
  public static RealProtocol<
          SecretSharedNumberAsBits, BinaryExtensionFieldElement, BinaryExtensionFieldElement>
      from(
          final RealEnvironment<SecretSharedNumberAsBits, BinaryExtensionFieldElement>
              realEnvironment) {
    return create(realEnvironment.storage(), realEnvironment.multiplier());
  }
}
