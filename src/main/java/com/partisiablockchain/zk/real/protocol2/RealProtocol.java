package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.SharedNumberStorage;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElementFactory;

/**
 * Primary interface of a REAL protocol instantiation.
 *
 * @param <NumberT> the representation of a secret-shared number.
 * @param <BitT> the representation of a secret-shared bit.
 */
public interface RealProtocol<
    NumberT extends SharedNumber<NumberT>,
    BitT extends SharedBit<BitT>,
    ElementT extends FiniteFieldElement<ElementT>> {

  /**
   * A factory for creating shares of either numbers or bits.
   *
   * @return the factory.
   */
  ShareFactory<NumberT, BitT> shareFactory();

  /**
   * A class for reading and writing shares to storage.
   *
   * @return the storage class.
   */
  SharedNumberStorage<NumberT> storage();

  /**
   * Object for operating on shares of bits.
   *
   * @return the {@link BitManipulation} object.
   */
  BitManipulation<BitT> bits();

  /**
   * Object for operating on shares of numbers.
   *
   * @return the {@link NumberManipulation} object.
   */
  NumberManipulation<NumberT, BitT> numeric();

  /**
   * A factory for creating finite field elements.
   *
   * @return the factory.
   */
  FiniteFieldElementFactory<ElementT> elementFactory();
}
