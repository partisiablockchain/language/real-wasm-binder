package com.partisiablockchain.zk.real.protocol2.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.ElementMultiplier;
import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol2.BatchUtility;
import com.partisiablockchain.zk.real.protocol2.BitManipulation;
import com.partisiablockchain.zk.real.protocol2.ShareFactory;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Class for manipulating binary shares of bits.
 *
 * @param <BitT> type of a secret shared bit.
 */
public final class BinaryBitManipulation<BitT extends SharedBit<BitT>>
    implements BitManipulation<BitT> {

  private final ElementMultiplier<BitT> multiplier;
  private final ShareFactory<?, BitT> factory;
  private final BiFunction<BitT, BitT, BitT> xor;

  /**
   * An object that can perform manipulation of secret shared bits.
   *
   * @param multiplier an object that can perform manipulation on secret shared bits.
   * @param factory a share factory.
   * @param xor How to perform xor.
   */
  public BinaryBitManipulation(
      final ElementMultiplier<BitT> multiplier,
      final ShareFactory<?, BitT> factory,
      final BiFunction<BitT, BitT, BitT> xor) {
    this.multiplier = multiplier;
    this.factory = factory;
    this.xor = xor;
  }

  @Override
  public List<BitT> multiXor(Iterable<BitT> left, Iterable<BitT> right) {
    return BatchUtility.pairwiseMap(this.xor, left, right);
  }

  @Override
  public List<BitT> multiAnd(Iterable<BitT> left, Iterable<BitT> right) {
    return multiplier.multiplyPairwise(left, right);
  }

  @Override
  public BitT not(BitT bit) {
    return multiXor(List.of(factory.constant(true)), List.of(bit)).get(0);
  }

  @Override
  public BitT andReducer(List<BitT> bits) {
    return BatchUtility.foldByBatchedTree(factory.constant(true), bits, this::multiAnd);
  }
}
