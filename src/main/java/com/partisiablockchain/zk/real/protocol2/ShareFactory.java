package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.function.IntFunction;

/**
 * Interface for generating or manipulating secret shares. This interface provides methods for
 * generating random bits, numbers/bits from constants, and for converting between secret shared
 * bits and numbers.
 *
 * @param <NumberT> type of a secret shared integer.
 * @param <BitT> type of a secret shared bit.
 */
public interface ShareFactory<NumberT, BitT> {

  /**
   * Convert a constant open boolean to a shared bit.
   *
   * @param value the public value
   * @return a shared representation of the value
   */
  BitT constant(boolean value);

  /**
   * Convert a constant number to a shared number.
   *
   * @param value the value to share
   * @param bitLength the number of bits to use from the value
   * @return a shared twos complement number with bitLength bits
   */
  NumberT constant(long value, int bitLength);

  /**
   * Convert a long to a shared number.
   *
   * @param value the constant.
   * @return a shared twos complement number of the constant.
   */
  NumberT constant(long value);

  /**
   * Generate shared random bits.
   *
   * @param count the number of bits to generate
   * @return the generated bits
   */
  List<BitT> randomBits(int count);

  /**
   * Read a single shared bit from a shared number.
   *
   * @param number the number to get a bit from
   * @param bit the bit index (counting from least significant)
   * @return the bit at index bit in number
   */
  BitT bit(NumberT number, int bit);

  /**
   * Construct a new number with the supplied bits.
   *
   * @param bits the bits in the number with least significant bit at index 0
   * @return the shared number
   */
  NumberT number(List<BitT> bits);

  /**
   * Construct a new number with <code>size</code> of the bits from <code>bits</code>. If <code>size
   * </code> is larger, then 0s are added in the most significant bit parts. If <code>bits.size()
   * </code> is larger, then only <code>size</code> of the bits are considered.
   *
   * @param size the bit size of the final integer.
   * @param bits the bits in the number with least significant bit at index 0.
   * @return a new number of bit size <code>size</code>.
   */
  NumberT number(int size, List<BitT> bits);

  /**
   * Construct a new number with the supplied bits.
   *
   * @param size the amount of numbers to be present in the pack, must be larger than 0
   * @param bitCreator a dynamic reference to create the numbers
   * @return the shared number
   */
  NumberT number(int size, IntFunction<BitT> bitCreator);
}
