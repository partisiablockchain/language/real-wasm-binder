package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Collection;
import java.util.List;

/**
 * Interface for operating on secret-shares of bits.
 *
 * @param <BitT> the bit representation.
 */
public interface BitManipulation<BitT> {

  /**
   * Compute the AND of two bits.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &and; right</code>
   */
  default BitT and(BitT left, BitT right) {
    return andReducer(List.of(left, right));
  }

  /**
   * Compute the AND of two bits.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &and; right</code>
   */
  List<BitT> multiAnd(Iterable<BitT> left, Iterable<BitT> right);

  /**
   * Compute the OR of two bits.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &or; right</code>
   */
  default BitT or(BitT left, BitT right) {
    return orReducer(List.of(left, right));
  }

  /**
   * Compute the XOR of two bits.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &oplus; right</code>
   */
  default BitT xor(BitT left, BitT right) {
    return multiXor(List.of(left), List.of(right)).get(0);
  }

  /**
   * Compute the XOR of two bits.
   *
   * @param left left side of the operation
   * @param right right side of the operation
   * @return <code>left &oplus; right</code>
   */
  List<BitT> multiXor(Iterable<BitT> left, Iterable<BitT> right);

  /**
   * Compute the negation of a bit.
   *
   * @param operand operand
   * @return <code>&not;operand</code>
   */
  BitT not(BitT operand);

  /**
   * Computes whether all bits are true.
   *
   * @param bits the operand
   * @return true whether all bits true, false otherwise
   */
  BitT andReducer(List<BitT> bits);

  /**
   * Computes if any bit is true.
   *
   * @param bits the operand
   * @return true if any bit is true, false otherwise
   */
  default BitT orReducer(List<BitT> bits) {
    return not(andReducer(bits.stream().map(this::not).toList()));
  }

  /**
   * Select either trueValue or falseValue based on the selection bit.
   *
   * @param selector bit indicating the value to select
   * @param trueValue the value to return if selector is true
   * @param falseValue the value to return if selector is false
   * @return trueValue if selector is true, falseValue otherwise
   */
  default BitT selectBit(BitT selector, BitT trueValue, BitT falseValue) {
    return selectBits(selector, List.of(trueValue), List.of(falseValue)).get(0);
  }

  /**
   * Select either trueValue or falseValue based on the selection bit.
   *
   * @param selector bit indicating the value to select
   * @param trueValue the value to return if selector is true
   * @param falseValue the value to return if selector is false
   * @return trueValue if selector is true, falseValue otherwise
   */
  default List<BitT> selectBits(
      BitT selector, Collection<BitT> trueValue, Collection<BitT> falseValue) {
    final List<BitT> selectors =
        BatchUtility.concat(
            BatchUtility.repeat(trueValue.size(), selector),
            BatchUtility.repeat(falseValue.size(), not(selector)));
    final List<BitT> values = BatchUtility.concat(trueValue, falseValue);
    final List<List<BitT>> results =
        BatchUtility.split(this.multiAnd(selectors, values), trueValue.size(), falseValue.size());
    return this.multiXor(results.get(0), results.get(1));
  }
}
