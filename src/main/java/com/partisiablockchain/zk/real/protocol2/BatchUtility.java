package com.partisiablockchain.zk.real.protocol2;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/** Utility class for implementing batching. */
public final class BatchUtility {

  private BatchUtility() {}

  /**
   * Function interface for batched functions to be used with {@link
   * BatchUtility#foldByBatchedTree}.
   *
   * @param <T> Type of function.
   */
  @FunctionalInterface
  public interface BatchedFoldingFunction<T> extends BiFunction<List<T>, List<T>, List<T>> {}

  /**
   * Efficient functional fold function, over batched associate functions.
   *
   * <p>Folds over the given {@code inputs} by repeatedly applying {@code batchFn} to half of the
   * {@code inputs} with the other half, eventually reducing to a single value, which is then
   * output.
   *
   * <p>Due to the unpredictability of the application order, {@code batchFn} should be associative
   * and commutative, as it might otherwise produce unexpected outputs.
   *
   * <p>Additional requirements:
   *
   * <ul>
   *   <li>{@code batchFn} must be a batched function (a function implementing a basic function
   *       {@code fn} more efficiently by doing the same function repeatedly).
   *   <li>{@code batchFn} must take two lists of equal length {@code N}, and returning a new list
   *       of the same length {@code N}. {@code batchFn} will never be called with differently sized
   *       lists.
   *   <li>The basis function {@code fn}, should be associative.
   *   <li>{@code identity} should be an identity element over {@code fn}.
   * </ul>
   *
   * @param <T> Type to fold over.
   * @param identity Identity element, to use if {@code inputs.isEmpty()}.
   * @param inputs Inputs to fold over.
   * @param batchFn Batched function. Unbatched function must be associative.
   * @return Single element
   * @see <a href="https://en.wikipedia.org/wiki/Fold_%28higher-order_function%29">Fold
   *     (higher-order function)</a>
   * @see <a href="https://en.wikipedia.org/wiki/Associative">Associative property</a>
   */
  public static <T> T foldByBatchedTree(
      final T identity, final List<T> inputs, final BatchedFoldingFunction<T> batchFn) {
    // Return identity if no inputs
    if (inputs.isEmpty()) {
      return identity;
    }

    return parallelFoldByBatchedTree(identity, List.of(inputs), batchFn).get(0);
  }

  /**
   * Generalization of {@link foldByBatchedTree} that allows multiple folds in parallel.
   *
   * <p>Each list is folded with its own elements (elements from different folds are not mixed
   * together,) but the batchFn will be called on an amalgamed list of elements from different
   * folds.
   *
   * @param <T> Type to fold over.
   * @param identity Identity element, to use if {@code inputs.isEmpty()}.
   * @param inputs List of inputs for each fold. All lists of inputs must have the same length.
   * @param batchFn Batched function. Unbatched function must be associative.
   * @return List of resulting elements.
   */
  public static <T> List<T> parallelFoldByBatchedTree(
      final T identity, final List<List<T>> inputs, final BatchedFoldingFunction<T> batchFn) {

    final List<ArrayList<T>> queues = inputs.stream().map(ArrayList::new).toList();

    while (queues.get(0).size() > 1) {
      int queueSize = queues.get(0).size();

      // Determine batch input
      // First input is skipped if the queue is an odd size; will be included
      // in one of the later rounds.
      final int batchSideSize = queueSize / 2;

      final int[] outputSizes = queues.stream().mapToInt(q -> batchSideSize).toArray();

      final List<T> batchLeft =
          queues.stream()
              .flatMap(
                  queue ->
                      queue
                          .subList(queueSize - 2 * batchSideSize, queueSize - batchSideSize)
                          .stream())
              .toList();
      final List<T> batchRight =
          queues.stream()
              .flatMap(queue -> queue.subList(queueSize - batchSideSize, queueSize).stream())
              .toList();

      // Compute batch
      final List<T> amalgamBatchResult = batchFn.apply(batchLeft, batchRight);

      if (amalgamBatchResult.size() != outputSizes.length * batchSideSize) {
        throw new RuntimeException("batchFn must produce a list with same size as input sizes");
      }

      final List<List<T>> batchResults = split(amalgamBatchResult, outputSizes);

      // Bookkeep queue
      for (int queueIdx = 0; queueIdx < queues.size(); queueIdx++) {
        final var queue = queues.get(queueIdx);
        queue.subList(queueSize - 2 * batchSideSize, queueSize).clear();
        queue.addAll(batchResults.get(queueIdx));
      }
    }

    return queues.stream().map(q -> q.get(0)).toList();
  }

  /**
   * Produces list containing the given {@code element} {@code count} times.
   *
   * @param <T> Type of element.
   * @param element Element to repeat.
   * @param count Number of repetitions.
   * @return Unmodifiable list of size {@code count}, containing only {@code element}.
   */
  public static <T> List<T> repeat(int count, T element) {
    return Stream.generate(() -> element).limit(count).toList();
  }

  /**
   * Combines the given lists into a single list.
   *
   * @param <T> Type of elements.
   * @param lists Lists to concat.
   * @return Unmodifiable combined list.
   */
  @SafeVarargs
  @SuppressWarnings("varargs")
  public static <T> List<T> concat(Collection<T>... lists) {
    return Stream.of(lists).flatMap(Collection::stream).toList();
  }

  /**
   * Splits the given {@code list} out into lists of given {@code sizes}.
   *
   * @param <T> Type of elements.
   * @param list List to split.
   * @param sizes Sizes of the desired lists.
   * @return List of split lists.
   */
  public static <T> List<List<T>> split(List<T> list, int... sizes) {
    final ArrayList<List<T>> output = new ArrayList<>();
    int offset = 0;
    for (int size : sizes) {
      output.add(list.subList(offset, offset + size));
      offset += size;
    }
    return output;
  }

  /**
   * Pairwise map function over given iterables.
   *
   * @param <T> Element type of left iterable.
   * @param <S> Element type of right iterable.
   * @param <R> Element type of output list.
   * @param fn Function to appl
   * @param left Iterable of left operands.
   * @param right Iterable of right operands.
   * @return List of outputs.
   */
  public static <T, S, R> List<R> pairwiseMap(
      final BiFunction<T, S, R> fn, final Iterable<T> left, final Iterable<S> right) {
    final ArrayList<R> result = new ArrayList<>();
    final Iterator<T> iterLeft = left.iterator();
    final Iterator<S> iterRight = right.iterator();
    while (iterLeft.hasNext() && iterRight.hasNext()) {
      result.add(fn.apply(iterLeft.next(), iterRight.next()));
    }
    return result;
  }
}
