package com.partisiablockchain.zk.real.protocol2.binary;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol2.ShareFactory;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/** Share factory for binary numbers. */
public record BinaryShareFactory()
    implements ShareFactory<SecretSharedNumberAsBits, BinaryExtensionFieldElement> {

  @Override
  public BinaryExtensionFieldElement constant(boolean value) {
    return value ? BinaryExtensionFieldElement.ONE : BinaryExtensionFieldElement.ZERO;
  }

  @Override
  public SecretSharedNumberAsBits constant(long value, int bitLength) {
    List<BinaryExtensionFieldElement> bits = new ArrayList<>();
    BigInteger bigInteger = BigInteger.valueOf(value);
    for (int bit = 0; bit < bitLength; bit++) {
      bits.add(constant(bigInteger.testBit(bit)));
    }
    return number(bits);
  }

  @Override
  public SecretSharedNumberAsBits constant(long value) {
    return constant(value, Long.SIZE);
  }

  @Override
  public List<BinaryExtensionFieldElement> randomBits(int count) {
    throw new UnsupportedOperationException("ShareFactory#randomBits is not supported");
  }

  @Override
  public BinaryExtensionFieldElement bit(SecretSharedNumberAsBits number, int bit) {
    return number.get(bit);
  }

  @Override
  public SecretSharedNumberAsBits number(List<BinaryExtensionFieldElement> bits) {
    return SecretSharedNumberAsBits.create(bits);
  }

  @Override
  public SecretSharedNumberAsBits number(int size, List<BinaryExtensionFieldElement> bits) {
    int numberOfBits = bits.size();
    // doesn't matter if size - numberOfBits is negative.
    List<BinaryExtensionFieldElement> extraZeros =
        IntStream.range(0, size - numberOfBits)
            .mapToObj(i -> BinaryExtensionFieldElement.ZERO)
            .toList();
    List<BinaryExtensionFieldElement> newBits =
        Stream.concat(bits.stream(), extraZeros.stream()).toList();
    return number(newBits);
  }

  @Override
  public SecretSharedNumberAsBits number(
      int count, IntFunction<BinaryExtensionFieldElement> bitCreator) {
    final List<BinaryExtensionFieldElement> bits =
        IntStream.range(0, count).mapToObj(bitCreator).toList();
    return number(count, bits);
  }
}
