package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.contract.zk.ZkComputationState;
import com.partisiablockchain.language.zkcircuitlowering.LoweringPublicExecutionEnvironment;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.contract.RealClosed;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

record WasmRealLoweringEnvironment(
    ZkComputationState<?, LargeByteArray, RealClosed<LargeByteArray>> zkState,
    Map<Integer, Integer> variableIdIteratorMap)
    implements LoweringPublicExecutionEnvironment {

  WasmRealLoweringEnvironment {
    requireNonNull(zkState);
    requireNonNull(variableIdIteratorMap);
  }

  /**
   * Creates a new {@link WasmRealLoweringEnvironment} from the given {@link ZkComputationState}.
   *
   * @param zkState Zk state to create lowering environment from.
   * @return Newly created lowering environment
   */
  public static WasmRealLoweringEnvironment from(
      ZkComputationState<?, LargeByteArray, RealClosed<LargeByteArray>> zkState) {
    // Create variable id iterator map
    final Map<Integer, Integer> variableIdIteratorMap = new LinkedHashMap<>();
    int prevVarId = 0;
    for (final var variable : zkState.getVariables()) {
      variableIdIteratorMap.put(prevVarId, variable.getId());
      prevVarId = variable.getId();
    }
    variableIdIteratorMap.put(prevVarId, 0);

    // Create lowering environment
    return new WasmRealLoweringEnvironment(zkState, variableIdIteratorMap);
  }

  @Override
  public int numberOfSecretVariables() {
    return variableIdIteratorMap().size() - 1;
  }

  @Override
  public int nextVariableId(int variableId) {
    return variableIdIteratorMap.getOrDefault(variableId, 0);
  }

  @Override
  public BigInteger getSecretVariableMetadata(final int variableId) {
    // Load variable, if present
    final ZkClosed<LargeByteArray> variable = zkState().getVariable(variableId);
    if (variable == null) {
      return null;
    }

    // Extract metadata as integer
    return bigIntegerFromLittleEndianBytes(variable.getInformation().getData());
  }

  static BigInteger bigIntegerFromLittleEndianBytes(byte[] bytes) {
    for (int i = 0; i < bytes.length / 2; i++) {
      final int j = bytes.length - 1 - i;
      byte tmp = bytes[j];
      bytes[j] = bytes[i];
      bytes[i] = tmp;
    }
    return new BigInteger(bytes);
  }
}
