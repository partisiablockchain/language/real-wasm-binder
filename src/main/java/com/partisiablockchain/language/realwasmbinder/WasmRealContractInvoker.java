package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.partisiablockchain.language.wasminvoker.Configuration;
import com.partisiablockchain.language.wasminvoker.Leb128Header;
import com.partisiablockchain.language.wasminvoker.SemVer;
import com.partisiablockchain.language.wasminvoker.WasmInvoker;
import com.partisiablockchain.language.wasminvoker.WasmInvokerImpl;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTrees;
import com.partisiablockchain.language.wasminvoker.avltrees.ReadOnlyAvlTreeMapper;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.oracle.ConfirmedEventLog;
import com.partisiablockchain.oracle.CreateEvmEventSubscription;
import com.partisiablockchain.oracle.EvmEventSubscription;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.zk.real.binder.AttestationImpl;
import com.partisiablockchain.zk.real.binder.PendingInputImpl;
import com.partisiablockchain.zk.real.binder.RealContractInvoker;
import com.partisiablockchain.zk.real.binder.SerializationAvlTree;
import com.partisiablockchain.zk.real.binder.ZkBinderContextWithGas;
import com.partisiablockchain.zk.real.binder.ZkClosedImpl;
import com.partisiablockchain.zk.real.binder.ZkStateImmutable;
import com.partisiablockchain.zk.real.binder.ZkStateMutable;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.contract.RealContract;
import com.secata.stream.LittleEndianByteOutput;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HexFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Contract invoker for WASM-based REAL contracts. */
public final class WasmRealContractInvoker implements RealContractInvoker {

  private static final Logger logger = LoggerFactory.getLogger(WasmRealContractInvoker.class);

  /** This is 0xFFFFFFFF encoded in LEB128: 0xFFFFFFFFF0. */
  public static final byte[] INIT_HEADER = HexFormat.ofDelimiter(":").parseHex("FF:FF:FF:FF:0F");

  private final WasmInvoker wasmInvoker;
  private final WasmContract contract;

  /**
   * Create binder for a REAL contract.
   *
   * @param wasmInvoker invoker for a wasm file
   * @param contract the contract
   */
  WasmRealContractInvoker(WasmInvoker wasmInvoker, WasmContract contract) {
    this.wasmInvoker = wasmInvoker;
    this.contract = contract;
  }

  private static final SemVer SUPPORTED_BINDER_VERSION = new SemVer(11, 5, 0);
  private static final Configuration SUPPORTED_CONFIGURATION = new Configuration(true, true);
  private static final int PENDING_INPUT_TREE_ID = -1;
  private static final int VARIABLES_TREE_ID = -2;
  private static final int DATA_ATTESTATION_TREE_ID = -3;
  private static final int SUBSCRIPTION_TREE_ID = -4;
  private static final int EXTERNAL_EVENT_TREE_ID = -5;

  /**
   * /** Create Wasm contract with binary state.
   *
   * @param contract Wasm contract definition
   * @return Wasm contract with binary state
   */
  public static WasmRealContractInvoker fromWasmBytes(
      final WasmRealContractInvoker.WasmContract contract) {
    // Parse
    final WasmParser parser = new WasmParser(contract.wasmBytes);
    final WasmModule wasmModule = parser.parse();

    // Determine configuration
    final SemVer parsedVersion = Configuration.determineBinderVersion(wasmModule);

    if (!isSupported(parsedVersion)) {
      final StringBuilder builder =
          new StringBuilder("Unsupported binder ABI version ")
              .append(parsedVersion)
              .append(" in contract \"")
              .append(contract.wasmFilename)
              .append("\".")
              .append(" Supported version is ")
              .append(SUPPORTED_BINDER_VERSION);
      throw new RuntimeException(builder.toString());
    }

    // Produce invoker
    final WasmInvoker wasmInvoker = WasmInvokerImpl.fromModule(wasmModule, SUPPORTED_CONFIGURATION);
    return new WasmRealContractInvoker(wasmInvoker, contract);
  }

  static boolean isSupported(SemVer parsedVersion) {
    return parsedVersion.major() == SUPPORTED_BINDER_VERSION.major()
        && parsedVersion.minor() == SUPPORTED_BINDER_VERSION.minor();
  }

  /** A wrapper for a WASM-based contract, containing bytes and a filename. */
  public static final class WasmContract
      extends RealContract<WasmRealContractState, LargeByteArray> {

    /** Null allowed. */
    public final String wasmFilename;

    final byte[] wasmBytes;

    /**
     * Constructs wrapped WASM-based contract.
     *
     * @param wasmFilename Filename of the file where the WASM bytes came from. Nullable.
     * @param wasmBytes Bytes of the WASM module to use.
     * @param computation Zero-knowledge computation for contract.
     */
    public WasmContract(
        final String wasmFilename,
        final byte[] wasmBytes,
        final RealComputationEnv<WasmRealContractState, LargeByteArray> computation) {
      super(requireNonNull(computation));
      this.wasmFilename = wasmFilename;
      this.wasmBytes = requireNonNull(wasmBytes);
    }

    /**
     * Creates a new wasm contract instance with an alternative computation.
     *
     * @param computation New computation to use.
     * @return New instance of wasm contract, using the given computation instead.
     */
    public WasmContract withAlternativeComputation(
        final RealComputationEnv<WasmRealContractState, LargeByteArray> computation) {
      return new WasmContract(this.wasmFilename, this.wasmBytes, computation);
    }
  }

  ///// Hooks /////

  @Override
  public WasmStateAndEvents onCreate(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final SafeDataInputStream rpc) {
    byte[] headerBytes = rpc.readBytes(5);
    if (!Arrays.equals(headerBytes, INIT_HEADER)) {
      throw new RuntimeException("Contract initialization bytes should start with 0xFFFFFFFF0F");
    }
    logger.debug("Contract {} onCreate", context.getContractAddress());

    final WasmInvoker.WasmResult wasmResult =
        wasmInvoker.executeWasm(
            context.availableGas(),
            "init",
            context,
            null,
            new AvlTrees(AvlTree.create(), getZkSpecialTrees(zkState.getState())),
            serializeInput(zkState.getState(), rpc.readAllBytes()));

    context.registerCpuFee(wasmResult.usedWasmGas());
    WasmRealContractState fixedState = fixState(zkState, wasmResult);
    return new WasmStateAndEvents(fixedState, wasmResult.eventResult());
  }

  @Override
  public WasmStateAndEvents onOpenInput(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final SafeDataInputStream rpc) {

    final byte[] action_shortname_bytes = Leb128Header.readHeaderBytes(rpc);
    final String action_shortname =
        Hex.toHexString(action_shortname_bytes).toLowerCase(Locale.ENGLISH);

    logger.debug("Contract {} onOpenInput#{}", context.getContractAddress(), action_shortname);

    final WasmInvoker.WasmResult wasmResult =
        wasmInvoker.executeWasm(
            context.availableGas(),
            "action_" + action_shortname,
            context,
            null,
            new AvlTrees(
                zkState.getState().getOpenState().avlTrees(),
                getZkSpecialTrees(zkState.getState())),
            zkState.getState().getOpenState().getData(),
            serializeInput(zkState.getState(), concat(new byte[][] {rpc.readAllBytes()})));

    context.registerCpuFee(wasmResult.usedWasmGas());
    WasmRealContractState fixedState = fixState(zkState, wasmResult);
    return new WasmStateAndEvents(fixedState, wasmResult.eventResult());
  }

  @Override
  public WasmStateAndEvents onCallback(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final CallbackContext callbackContext,
      final SafeDataInputStream rpc) {

    final byte[] action_shortname_bytes = Leb128Header.readHeaderBytes(rpc);
    final String action_shortname =
        Hex.toHexString(action_shortname_bytes).toLowerCase(Locale.ENGLISH);

    logger.debug("Contract {} callback#{}", context.getContractAddress(), action_shortname);

    final WasmInvoker.WasmResult wasmResult =
        wasmInvoker.executeWasm(
            context.availableGas(),
            "callback_" + action_shortname,
            context,
            callbackContext,
            new AvlTrees(
                zkState.getState().getOpenState().avlTrees(),
                getZkSpecialTrees(zkState.getState())),
            zkState.getState().getOpenState().getData(),
            serializeInput(zkState.getState(), rpc.readAllBytes()));

    context.registerCpuFee(wasmResult.usedWasmGas());
    WasmRealContractState fixedState = fixState(zkState, wasmResult);
    return new WasmStateAndEvents(fixedState, wasmResult.eventResult());
  }

  @Override
  public WasmStateAndEventsAndVariableDefinition onSecretInput(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final SafeDataInputStream rpc) {

    final byte[] action_shortname_bytes = Leb128Header.readHeaderBytes(rpc);
    final String action_shortname =
        Hex.toHexString(action_shortname_bytes).toLowerCase(Locale.ENGLISH);

    logger.debug("Contract {} onSecretInput#{}", context.getContractAddress(), action_shortname);

    final WasmInvoker.WasmResult wasmResult =
        wasmInvoker.executeWasm(
            context.availableGas(),
            "zk_on_secret_input_" + action_shortname,
            context,
            null,
            new AvlTrees(
                zkState.getState().getOpenState().avlTrees(),
                getZkSpecialTrees(zkState.getState())),
            zkState.getState().getOpenState().getData(),
            serializeInput(zkState.getState(), concat(new byte[][] {rpc.readAllBytes()})));

    // Get variable definition and use it on the builder
    final WasmInvoker.WasmResultSection varDefSection =
        wasmResult.getSection(SECTION_ZK_VARIABLE_DEFINITION);
    final VariableDefinition variableDefinition = readZkVarDefSection(varDefSection);
    context.registerCpuFee(wasmResult.usedWasmGas());
    WasmRealContractState fixedState = fixState(zkState, wasmResult);
    return new WasmStateAndEventsAndVariableDefinition(
        fixedState, wasmResult.eventResult(), variableDefinition);
  }

  @Override
  public WasmStateAndEvents onVariableInputted(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final int variableId,
      final byte[] onVariableInputtedShortname) {

    final String shortnameString =
        Hex.toHexString(onVariableInputtedShortname).toLowerCase(Locale.ENGLISH);

    logger.debug("Contract {} onVariableInputted", context.getContractAddress());
    return executeWasmZkNotification(
        "zk_on_variable_inputted_" + shortnameString,
        context,
        zkState,
        state,
        serializeId(variableId));
  }

  @Override
  public WasmStateAndEvents onVariableRejected(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final int variableId) {

    logger.debug("Contract {} onVariableRejected", context.getContractAddress());
    return executeWasmZkNotification(
        "zk_on_variable_rejected", context, zkState, state, serializeId(variableId));
  }

  @Override
  public WasmStateAndEvents onComputeComplete(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final List<Integer> createdVariables,
      final byte[] onComputeCompleteShortname) {

    final String shortnameString =
        Hex.toHexString(onComputeCompleteShortname).toLowerCase(Locale.ENGLISH);

    logger.debug("Contract {} onComputeComplete", context.getContractAddress());
    return executeWasmZkNotification(
        "zk_on_compute_complete_" + shortnameString,
        context,
        zkState,
        state,
        serializeIds(createdVariables));
  }

  @Override
  public WasmStateAndEvents onVariablesOpened(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final List<Integer> openedVariables) {

    logger.debug("Contract {} onVariablesOpened", context.getContractAddress());
    return executeWasmZkNotification(
        "zk_on_variables_opened", context, zkState, state, serializeIds(openedVariables));
  }

  @Override
  public WasmStateAndEvents onAttestationComplete(
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final Integer attestationId) {
    logger.debug("Contract {} onAttestationComplete", context.getContractAddress());
    return executeWasmZkNotification(
        "zk_on_attestation_complete", context, zkState, state, serializeId(attestationId));
  }

  @Override
  public WasmStateAndEvents onExternalEvents(
      ZkBinderContextWithGas context,
      ZkStateMutable zkState,
      WasmRealContractState state,
      int subscriptionId,
      int eventId) {
    logger.debug("Contract {} onExternalEvents", context.getContractAddress());
    return executeWasmZkNotification(
        "zk_on_external_event",
        context,
        zkState,
        state,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeInt(subscriptionId);
              s.writeInt(eventId);
            }));
  }

  @Override
  public RealContract<WasmRealContractState, LargeByteArray> getContract() {
    return contract;
  }

  ///// Utility /////

  static final byte SECTION_ZK_STATE_UPDATE = 0x11;
  static final byte SECTION_ZK_VARIABLE_DEFINITION = 0x12;

  /**
   * Executes the WASM-code that handles a notification from the ZK-nodes that are engines in the
   * MPC computation.
   *
   * <p>If the WASM function exists and the call succeeds the new state is returned and the passed
   * zkState is modified with any changes specified by the WASM code. If the WASM function does not
   * exist or the WASM function throws an exception an unchanged state is returned and the zkState
   * is not modified.
   *
   * @param functionName The name of the WASM-function to execute.
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param rpcBytes Additional input RPC data for the function
   * @return The updated open WASM state.
   */
  private WasmStateAndEvents executeWasmZkNotification(
      final String functionName,
      final ZkBinderContextWithGas context,
      final ZkStateMutable zkState,
      final WasmRealContractState state,
      final byte[]... rpcBytes) {
    if (!wasmInvoker.doesFunctionExist(functionName)) {
      return new WasmStateAndEvents(state, EventResult.create());
    }
    final WasmInvoker.WasmResult wasmResult =
        wasmInvoker.executeWasm(
            context.availableGas(),
            functionName,
            context,
            null,
            new AvlTrees(
                zkState.getState().getOpenState().avlTrees(),
                getZkSpecialTrees(zkState.getState())),
            zkState.getState().getOpenState().getData(),
            serializeInput(zkState.getState(), concat(rpcBytes)));

    context.registerCpuFee(wasmResult.usedWasmGas());
    WasmRealContractState fixedState = fixState(zkState, wasmResult);
    return new WasmStateAndEvents(fixedState, wasmResult.eventResult());
  }

  /**
   * Fixes the state of the contract by applying zk state updates and by adding any new public input
   * variables.
   *
   * @param previousState the previous state of the contract
   * @param wasmResult the result of executing a wasm invocation
   * @return the updated state of the contract
   */
  private WasmRealContractState fixState(
      final ZkStateMutable previousState, final WasmInvoker.WasmResult wasmResult) {

    // Defaults
    WasmRealContractState.ComputationInput computationInput = null;

    // Zk state update
    final WasmInvoker.WasmResultSection zkStateUpdateSection =
        wasmResult.getSection(SECTION_ZK_STATE_UPDATE);

    if (zkStateUpdateSection != null) {
      computationInput =
          applyZkStateUpdatesSection(previousState, zkStateUpdateSection.sectionData());

      if (computationInput != null) {
        final FunctionId wantedFunctionId =
            new FunctionId(computationInput.calledFunctionShortname());

        final MetaCircuitComputation computation =
            (MetaCircuitComputation) contract.getComputation();
        final List<FunctionId> functionIds = computation.functionIds();
        if (!functionIds.contains(wantedFunctionId)) {
          throw new RuntimeException(
              "No ZK functions with id %s; found %s"
                  .formatted(
                      wantedFunctionId.functionId(),
                      functionIds.stream().map(FunctionId::functionId).toList()));
        }
      }
    }

    // Construct state
    return new WasmRealContractState(
        new LargeByteArray(wasmResult.state().getState()),
        computationInput,
        wasmResult.state().getAvlTrees());
  }

  private AvlTree<Integer, ReadOnlyAvlTreeMapper> getZkSpecialTrees(ZkStateImmutable state) {
    return AvlTree.create(
        Map.of(
            PENDING_INPUT_TREE_ID,
            new SerializationAvlTree<>(
                state.getPendingInput(),
                PendingInputImpl::serialize,
                PendingInputImpl::serializedByteLength),
            VARIABLES_TREE_ID,
            new SerializationAvlTree<>(
                state.getVariablesTree(),
                ZkClosedImpl::serialize,
                ZkClosedImpl::serializedByteLength),
            DATA_ATTESTATION_TREE_ID,
            new SerializationAvlTree<>(
                state.getAttestationsRaw(),
                AttestationImpl::serialize,
                AttestationImpl::serializedByteLength),
            SUBSCRIPTION_TREE_ID,
            new SerializationAvlTree<>(
                state.getExternalEvents().getSubscriptionsRaw(),
                EvmEventSubscription::serialize,
                EvmEventSubscription::serializedByteLength),
            EXTERNAL_EVENT_TREE_ID,
            new SerializationAvlTree<>(
                state.getExternalEvents().getConfirmedEventsRaw(),
                ConfirmedEventLog::serialize,
                ConfirmedEventLog::serializedByteLength)));
  }

  static final class ZkStateChangeDiscriminants {

    private ZkStateChangeDiscriminants() {}

    /**
     * Triggers the execution of the MPC computation, starting in function id 0.
     *
     * <p>Now fully deprecated.
     */
    static final byte START_COMPUTATION_1 = 0x00;

    static final byte DELETE_PENDING_VARIABLE = 0x01;
    static final byte TRANSFER_VARIABLE = 0x02;
    static final byte DELETE_VARIABLES = 0x03;
    static final byte OPEN_VARIABLES = 0x04;
    static final byte OUTPUT_COMPLETE = 0x05;
    static final byte CONTRACT_DONE = 0x06;
    static final byte REQUEST_ATTEST_DATA = 0x07;

    /**
     * Triggers the execution of the MPC computation, starting in function id 0, with the given
     * input arguments.
     *
     * <p>Now fully deprecated.
     */
    static final byte START_COMPUTATION_2 = 0x08;

    /**
     * Triggers the execution of the MPC computation, starting in a given function id, and with some
     * given input arguments.
     */
    static final byte START_COMPUTATION_3 = 0x09;

    /** Add a subscription to external events. Events are automatically sent by real nodes. */
    static final byte SUBSCRIBE_TO_EXTERNAL_EVENTS = 0x0A;

    /**
     * Cancel an existing event subscription. Real nodes will no longer send events for the
     * subscription once called.
     */
    static final byte UNSUBSCRIBE_FROM_EXTERNAL_EVENTS = 0x0B;

    /** Delete external events from state. */
    static final byte DELETE_EXTERNAL_EVENTS = 0x0C;
  }

  /**
   * Reads and executes the operations encoded within a Zk State Update Section.
   *
   * @param previousState State that is modified
   * @param sectionBytes Bytes to parse and execute from
   */
  static WasmRealContractState.ComputationInput applyZkStateUpdatesSection(
      final ZkStateMutable previousState, final byte[] sectionBytes) {
    return SafeDataInputStream.readFully(
        sectionBytes,
        stream -> {
          WasmRealContractState.ComputationInput computationInput = null;

          final int numActions = stream.readInt();
          for (int actionIdx = 0; actionIdx < numActions; actionIdx++) {
            final byte actionId = stream.readSignedByte();
            if (actionId == ZkStateChangeDiscriminants.START_COMPUTATION_3) {

              // Read metadata for output variables
              final ArrayList<LargeByteArray> outputVariableMetadata = new ArrayList<>();
              final ArrayList<LargeByteArray> calledFunctionArguments = new ArrayList<>();

              final int calledFunctionShortname = stream.readInt();

              final int numExpectedOutputVariables = stream.readInt();
              for (int idx = 0; idx < numExpectedOutputVariables; idx++) {
                outputVariableMetadata.add(new LargeByteArray(stream.readDynamicBytes()));
              }

              final int numInputValues = stream.readInt();
              for (int idx = 0; idx < numInputValues; idx++) {
                final byte[] bytes = stream.readDynamicBytes();
                calledFunctionArguments.add(new LargeByteArray(bytes));
              }

              final LargeByteArray onComputeCompleteShortname = readOptionalShortname(stream);

              previousState.startComputation(outputVariableMetadata, onComputeCompleteShortname);
              computationInput =
                  new WasmRealContractState.ComputationInput(
                      calledFunctionShortname, FixedList.create(calledFunctionArguments));

            } else if (actionId == ZkStateChangeDiscriminants.DELETE_PENDING_VARIABLE) {
              previousState.deletePendingInput(stream.readInt());
            } else if (actionId == ZkStateChangeDiscriminants.TRANSFER_VARIABLE) {
              final int varId = stream.readInt();
              final BlockchainAddress newOwner = BlockchainAddress.read(stream);
              previousState.transferVariable(varId, newOwner);
            } else if (actionId == ZkStateChangeDiscriminants.DELETE_VARIABLES) {
              final int numVariables = stream.readInt();
              for (int idx = 0; idx < numVariables; idx++) {
                previousState.deleteVariable(stream.readInt());
              }
            } else if (actionId == ZkStateChangeDiscriminants.OPEN_VARIABLES) {
              final int numVariables = stream.readInt();
              final var variables = new ArrayList<Integer>();
              for (int idx = 0; idx < numVariables; idx++) {
                variables.add(stream.readInt());
              }
              previousState.openVariables(variables);
            } else if (actionId == ZkStateChangeDiscriminants.OUTPUT_COMPLETE) {
              throw new UnsupportedOperationException(
                  "OUTPUT_COMPLETE is no longer supported. Use DELETE_VARIABLES to delete"
                      + " variables");
            } else if (actionId == ZkStateChangeDiscriminants.CONTRACT_DONE) {
              previousState.contractDone();
            } else if (actionId == ZkStateChangeDiscriminants.REQUEST_ATTEST_DATA) {
              final byte[] dataToAttest = stream.readDynamicBytes();
              previousState.attestData(dataToAttest);
            } else if (actionId == ZkStateChangeDiscriminants.SUBSCRIBE_TO_EXTERNAL_EVENTS) {
              CreateEvmEventSubscription createSubscriptionRpc =
                  CreateEvmEventSubscription.read(stream);
              previousState.subscribeToExternalEvents(createSubscriptionRpc);
            } else if (actionId == ZkStateChangeDiscriminants.UNSUBSCRIBE_FROM_EXTERNAL_EVENTS) {
              int subscriptionId = stream.readInt();
              previousState.cancelSubscription(subscriptionId);
            } else if (actionId == ZkStateChangeDiscriminants.DELETE_EXTERNAL_EVENTS) {
              int externalEventCount = stream.readInt();
              for (int i = 0; i < externalEventCount; i++) {
                int eventId = stream.readInt();
                previousState.deleteExternalEvent(eventId);
              }
            } else {
              throw new UnsupportedOperationException("Unknown action with id: " + actionId);
            }
          }
          return computationInput;
        });
  }

  private static LargeByteArray readOptionalShortname(SafeDataInputStream stream) {
    final var shortnameBytes = stream.readOptional(Leb128Header::readHeaderBytes);
    return shortnameBytes == null ? null : new LargeByteArray(shortnameBytes);
  }

  static RealContractInvoker.VariableDefinition readZkVarDefSection(
      final WasmInvoker.WasmResultSection varDefSection) {
    if (varDefSection == null) {
      throw new RuntimeException("Expected variable definition section, but got null");
    }
    return SafeDataInputStream.deserialize(
        stream -> {
          // Bit lengths
          final int numBitLengths = stream.readInt();
          final var bitLengths = new ArrayList<Integer>();
          for (int idx = 0; idx < numBitLengths; idx++) {
            bitLengths.add(stream.readInt());
          }
          // Is Sealed?
          final boolean seal = stream.readBoolean();

          // Read shortname
          final LargeByteArray onInputtedShortname = readOptionalShortname(stream);

          // Read remaining metadata.
          final byte[] metadata = stream.readAllBytes();
          return new RealContractInvoker.VariableDefinition(
              bitLengths, seal, onInputtedShortname, metadata);
        },
        varDefSection.sectionData());
  }

  private static byte[] serializeZkState(final ZkStateImmutable zkState) {
    return SafeDataOutputStream.serialize(
        out -> {
          // Computation state
          out.writeEnum(zkState.getCalculationStatus());
          // Special tree ids. Little endian as AvlTreeMap is only ReadWriteState.
          out.write(
              LittleEndianByteOutput.serialize(
                  le -> {
                    le.writeI32(PENDING_INPUT_TREE_ID);
                    le.writeI32(VARIABLES_TREE_ID);
                    le.writeI32(DATA_ATTESTATION_TREE_ID);
                    le.writeI32(SUBSCRIPTION_TREE_ID);
                    le.writeI32(EXTERNAL_EVENT_TREE_ID);
                  }));
        });
  }

  static byte[] serializeId(final int id) {
    return SafeDataOutputStream.serialize(out -> out.writeInt(id));
  }

  static byte[] serializeIds(final List<Integer> ids) {
    return SafeDataOutputStream.serialize(
        out -> {
          // Pending variables vector
          out.writeInt(ids.size());
          for (final int id : ids) {
            out.writeInt(id);
          }
        });
  }

  static byte[] serializeInput(final ZkStateImmutable zkState, final byte[] rpcBytes) {
    return concat(serializeZkState(zkState), rpcBytes);
  }

  private static byte[] concat(final byte[]... bytearrays) {
    if (bytearrays.length == 1) {
      return bytearrays[0];
    }

    int outLength = 0;
    for (final byte[] bytearray : bytearrays) {
      outLength += bytearray.length;
    }

    final byte[] out = new byte[outLength];
    int offset = 0;
    for (final byte[] bytearray : bytearrays) {
      System.arraycopy(bytearray, 0, out, offset, bytearray.length);
      offset += bytearray.length;
    }
    return out;
  }
}
