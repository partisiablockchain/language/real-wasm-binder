package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.zk.ZkComputationState;
import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitEngine;
import com.partisiablockchain.language.zkcircuit.ZkCircuitSharedNumberExecution;
import com.partisiablockchain.language.zkcircuit.ZkCircuitValidator;
import com.partisiablockchain.language.zkcircuitlowering.MetaCircuitLowering;
import com.partisiablockchain.language.zkcircuitlowering.opt.ZkCircuitOptClustering;
import com.partisiablockchain.language.zkcircuitlowering.opt.ZkCircuitOptCommonInstructionElimination;
import com.partisiablockchain.language.zkcircuitlowering.opt.ZkCircuitOptTreeBalance;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitValidator;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.contract.RealClosed;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.partisiablockchain.zk.real.protocol.RealEnvironment;
import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.binary.SecretSharedNumberAsBits;
import com.partisiablockchain.zk.real.protocol.binary.field.BinaryExtensionFieldElement;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import com.partisiablockchain.zk.real.protocol2.binary.BinaryProtocolFactory;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * A REAL computation using a {@link MetaCircuit} to implement dynamic functionality.
 *
 * @param metaCircuit {@link MetaCircuit} that the computation should run over.
 */
public record MetaCircuitComputation(MetaCircuit metaCircuit)
    implements RealComputationEnv<WasmRealContractState, LargeByteArray> {

  /**
   * The maximum number of branch jumps that will be executed before timing out, and aborting the
   * lowering process, in order to avoid being stuck in infinite loops.
   */
  private static final long MAXIMUM_NUMBER_OF_MC_BRANCHES = 100_000;

  /**
   * Create new {@link MetaCircuitComputation}.
   *
   * @param metaCircuit {@link MetaCircuit} that the computation should run over.
   */
  public MetaCircuitComputation {
    MetaCircuitValidator.validateMetaCircuit(metaCircuit);
  }

  /**
   * List of all {@link FunctionId}s in computation's {@link MetaCircuit}.
   *
   * @return List of all function ids.
   */
  public List<FunctionId> functionIds() {
    return metaCircuit.functions().stream().map(MetaCircuit.Function::functionId).toList();
  }

  /**
   * Create a RealProtocol from {@link RealEnvironment}. WARNING: This only works for binary
   * environments.
   *
   * @param <NumberT> Number type.
   * @param <BitT> Bit type.
   * @param <ElementT> Element type.
   * @param environment Environment to create protocol from.
   * @return Produced {@link RealProtocol}.
   */
  @SuppressWarnings("unchecked")
  public static <
          NumberT extends SharedNumber<NumberT>,
          BitT extends SharedBit<BitT>,
          ElementT extends FiniteFieldElement<ElementT>>
      RealProtocol<NumberT, BitT, ElementT> realProtocol(
          RealEnvironment<NumberT, BitT> environment) {
    return (RealProtocol<NumberT, BitT, ElementT>)
        BinaryProtocolFactory.from(
            (RealEnvironment<SecretSharedNumberAsBits, BinaryExtensionFieldElement>) environment);
  }

  @Override
  public <
          NumberT extends SharedNumber<NumberT>,
          BitT extends SharedBit<BitT>,
          ElementT extends FiniteFieldElement<ElementT>>
      void compute(
          final ZkComputationState<
                  WasmRealContractState, LargeByteArray, RealClosed<LargeByteArray>>
              zkState,
          final RealEnvironment<NumberT, BitT> realEnvironment) {
    final RealProtocol<NumberT, BitT, ElementT> protocol = realProtocol(realEnvironment);

    final FunctionId calledFunctionShortname =
        new FunctionId(zkState.getOpenState().computationInput().calledFunctionShortname());

    // Create list of input variables
    final FixedList<LargeByteArray> calledFunctionArguments =
        zkState.getOpenState().computationInput().calledFunctionArguments();

    // Generate circuit to run
    final MetaCircuitLowering.Result loweringResult =
        MetaCircuitLowering.executeMetaCircuit(
            this.metaCircuit(),
            calledFunctionShortname,
            WasmRealLoweringEnvironment.from(zkState),
            publicInputValuesFrom(calledFunctionArguments),
            MAXIMUM_NUMBER_OF_MC_BRANCHES);

    ZkCircuit zkCircuitResult = loweringResult.zkCircuit();
    zkCircuitResult = ZkCircuitOptTreeBalance.optimizeZkCircuit(zkCircuitResult);
    zkCircuitResult = ZkCircuitOptClustering.optimizeZkCircuit(zkCircuitResult);
    zkCircuitResult = ZkCircuitOptCommonInstructionElimination.optimizeZkCircuit(zkCircuitResult);

    // Run generated circuit
    runCircuit(zkCircuitResult, protocol);
  }

  static List<BigInteger> publicInputValuesFrom(FixedList<LargeByteArray> variables) {
    final ArrayList<BigInteger> publicInputValues = new ArrayList<>();
    for (final LargeByteArray v : variables) {
      publicInputValues.add(
          WasmRealLoweringEnvironment.bigIntegerFromLittleEndianBytes(v.getData()));
    }

    return List.copyOf(publicInputValues);
  }

  static <
          NumberT extends SharedNumber<NumberT>,
          BitT extends SharedBit<BitT>,
          ElementT extends FiniteFieldElement<ElementT>>
      void runCircuit(
          final ZkCircuit zkCircuit, final RealProtocol<NumberT, BitT, ElementT> protocol) {
    // Validate given circuit
    ZkCircuitValidator.validateZkCircuit(zkCircuit, null);

    // Initialize zk circuit engine
    final ZkCircuitSharedNumberExecution<NumberT, BitT, ElementT> executionModel =
        new ZkCircuitSharedNumberExecution<>(protocol);
    final ZkCircuitEngine<NumberT> zkCircuitEngine =
        new ZkCircuitEngine<>(executionModel, zkCircuit);

    // Run given circuit
    final List<NumberT> results = zkCircuitEngine.executeZkCircuit();

    // Save results
    for (NumberT result : results) {
      protocol.storage().saveNumber(result);
    }
  }
}
