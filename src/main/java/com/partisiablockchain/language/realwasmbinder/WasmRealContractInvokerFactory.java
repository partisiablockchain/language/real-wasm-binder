package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasminvoker.SectionParser;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitSerializer;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.zk.real.contract.RealComputationEnv;
import com.secata.stream.SafeDataInputStream;
import java.util.Map;

/** Contract invoker for WASM-based REAL contracts. */
public final class WasmRealContractInvokerFactory {
  private WasmRealContractInvokerFactory() {}

  /**
   * Creates a wasm invoker from a byte array input, parsing sections.
   *
   * @param inputBytes The bytes to create a binder from.
   * @return Newly created invoker.
   * @see #parseSectionedContractBytesToContract
   */
  public static WasmRealContractInvoker invokerFromBytes(final byte[] inputBytes) {
    final WasmRealContractInvoker.WasmContract contract =
        parseSectionedContractBytesToContract(null, inputBytes);
    return WasmRealContractInvoker.fromWasmBytes(contract);
  }

  /** Section identifier for contract WASM bytecode. */
  static final byte SECTION_ID_WASM = 0x02;

  /** Section identifier for contract {@link MetaCircuit} bytecode. */
  static final byte SECTION_ID_META_CIRCUIT = 0x03;

  /**
   * Parses invoker information from a byte array input. Input should be a sectioned byte array,
   * containing a wasm binary in section 0x02 and a {@link MetaCircuit} in section 0x03.
   *
   * @param wasmFilename Filename of the WASM contract.
   * @param sectionedContractBytes The bytes to create a binder from.
   * @return Newly created instance of {@link WasmRealContractInvoker.WasmContract}.
   */
  public static WasmRealContractInvoker.WasmContract parseSectionedContractBytesToContract(
      final String wasmFilename, final byte[] sectionedContractBytes) {

    final Map<Byte, byte[]> sections = SectionParser.parseSections(sectionedContractBytes);

    final RealComputationEnv<WasmRealContractState, LargeByteArray> computation =
        computationFromBytes(sections.get(SECTION_ID_META_CIRCUIT));

    return new WasmRealContractInvoker.WasmContract(
        wasmFilename, sections.get(SECTION_ID_WASM), computation);
  }

  /**
   * Transforms a serialization of a meta circuit into a {@link RealComputationEnv}. May throw
   * various errors if:
   *
   * <ul>
   *   <li>Bytes doesn't represent encode a Meta Circuit.
   *   <li>Meta Circuit is invalid.
   * </ul>
   *
   * @param metaCircuitBytes Meta-Circuit serialization to convert.
   * @return Newly created computation. Never null.
   */
  public static RealComputationEnv<WasmRealContractState, LargeByteArray> computationFromBytes(
      final byte[] metaCircuitBytes) {
    // Deserialize
    final MetaCircuit metaCircuit =
        SafeDataInputStream.deserialize(
                MetaCircuitSerializer::deserializeMetaCircuitFromStream, metaCircuitBytes)
            .metaCircuit();

    // Store as computation
    return new MetaCircuitComputation(metaCircuit);
  }
}
