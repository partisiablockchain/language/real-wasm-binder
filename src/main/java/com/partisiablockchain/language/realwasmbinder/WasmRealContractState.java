package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTreeWrapper;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

/**
 * State data for REAL WASM contracts.
 *
 * @param openState Current Rust-serialized open state.
 * @param computationInput Input for an upcoming computation.
 * @param avlTrees External Avl Trees for the open state.
 */
@Immutable
public record WasmRealContractState(
    LargeByteArray openState,
    ComputationInput computationInput,
    AvlTree<Integer, AvlTreeWrapper> avlTrees)
    implements StateSerializable {

  /**
   * Input for an upcoming computation.
   *
   * @param calledFunctionShortname Initial function id for the computation.
   * @param calledFunctionArguments Arguments for the initial function.
   */
  @Immutable
  public record ComputationInput(
      int calledFunctionShortname, FixedList<LargeByteArray> calledFunctionArguments)
      implements StateSerializable {}

  /**
   * Current Rust-serialized open state as byte array.
   *
   * @return The state as byte array
   */
  public byte[] getData() {
    return openState().getData();
  }
}
