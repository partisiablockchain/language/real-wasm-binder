package com.partisiablockchain.language.realwasmbinder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.binder.RealContractBinder;

/** Factory class for creating WASM-based REAL contract. */
public final class WasmRealContractBinder extends RealContractBinder {

  /**
   * Create Wasm contract binder from bytes. This is required for the binder to be loadable from the
   * blockchain.
   *
   * @param binderBytes Bytes to create invoker from.
   */
  public WasmRealContractBinder(final byte[] binderBytes) {
    super(WasmRealContractInvokerFactory.invokerFromBytes(binderBytes));
  }

  /**
   * Create Wasm contract binder with binary state.
   *
   * @param wasmRealContractInvoker Invoker for contract binder.
   */
  public WasmRealContractBinder(final WasmRealContractInvoker wasmRealContractInvoker) {
    super(wasmRealContractInvoker);
  }
}
