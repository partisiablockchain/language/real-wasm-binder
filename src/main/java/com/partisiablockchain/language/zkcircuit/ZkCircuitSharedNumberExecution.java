package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.zk.real.protocol.SharedBit;
import com.partisiablockchain.zk.real.protocol.SharedNumber;
import com.partisiablockchain.zk.real.protocol.field.FiniteFieldElement;
import com.partisiablockchain.zk.real.protocol2.BatchUtility;
import com.partisiablockchain.zk.real.protocol2.RealProtocol;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

/** An executor capable of executing a ZK circuit. */
public record ZkCircuitSharedNumberExecution<
        NumberT extends SharedNumber<NumberT>,
        BitT extends SharedBit<BitT>,
        ElementT extends FiniteFieldElement<ElementT>>(
    RealProtocol<NumberT, BitT, ElementT> protocol) implements ZkCircuitExecution<NumberT> {

  @Override
  public NumberT executeGateConstant(final ZkOperation.Constant gate) {
    final List<BitT> bits =
        gate.constantBits().stream().map(protocol().shareFactory()::constant).toList();
    return protocol().shareFactory().number(bits);
  }

  @Override
  public NumberT executeGateLoad(final ZkOperation.Load gate) {
    final int zkVariableId = gate.variableId().variableId();
    final List<NumberT> numberValues = protocol().storage().loadNumber(zkVariableId);
    if (numberValues == null) {
      throw new RuntimeException(
          "Could not load %s value with id %s".formatted(gate.type().pretty(), zkVariableId));
    }
    final NumberT value = numberValues.get(0);
    assertValueOfCorrectBitlength(gate.type(), value, zkVariableId);
    return value;
  }

  @Override
  public NumberT executeGateSignExtend(final ZkOperation.SignExtend gate, final NumberT input1) {
    if (input1.bitLength() == 0) {
      throw new UnsupportedOperationException("Cannot sign extend from zero-width operand!");
    }

    // Determine sign bit
    final BitT signBit = protocol().shareFactory().bit(input1, input1.bitLength() - 1);

    // Determine extension bits
    final int numExtensionBits = ((ZkType.Sbi) gate.resultType()).bitwidth() - input1.bitLength();
    final List<BitT> extensionBits =
        IntStream.range(0, numExtensionBits).mapToObj(i -> signBit).toList();

    // Concat bits and output
    final ArrayList<BitT> bits = new ArrayList<>();
    bits.addAll(toBits(input1));
    bits.addAll(extensionBits);
    return protocol().shareFactory().number(bits);
  }

  @Override
  public NumberT executeGateUnary(final ZkOperation.Unary gate, final NumberT input1) {
    // Bitwise not
    if (gate.operation() == ZkOperation.UnaryOp.BITWISE_NOT) {
      return bitwise(input1, protocol().bits()::not);

      // Arithmetic negate sign
    } else if (gate.operation() == ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED) {
      return negateNumber(input1);

      // Unknown
    } else {
      throw new UnsupportedOperationException("Unknown unop: " + gate.operation());
    }
  }

  private NumberT negateNumber(final NumberT number) {
    return protocol()
        .numeric()
        .parallelAdd(
            List.of(bitwise(number, protocol().bits()::not)),
            List.of(protocol().shareFactory().constant(1, number.bitLength())))
        .get(0);
  }

  @Override
  public List<NumberT> executeGatesBinary(
      final ZkOperation.BinaryOp binop, final List<NumberT> inputs1, final List<NumberT> inputs2) {
    // Arithmetic addition
    if (binop == ZkOperation.BinaryOp.ARITH_ADD_WRAPPING) {
      return protocol().numeric().parallelAdd(inputs1, inputs2);

      // Bitwise AND
    } else if (binop == ZkOperation.BinaryOp.BITWISE_AND) {
      return parallelBitwise(inputs1, inputs2, protocol().bits()::multiAnd);

      // Bitwise XOR
    } else if (binop == ZkOperation.BinaryOp.BITWISE_XOR) {
      return parallelBitwise(inputs1, inputs2, protocol().bits()::multiXor);

      // Comparison: Equals
    } else if (binop == ZkOperation.BinaryOp.CMP_EQUALS) {
      final var results = protocol().numeric().parallelEq(inputs2, inputs1);
      return results.stream().map(this::fromBit).toList();

      // Non-parallel operations
    } else {
      return executeGatesBinaryInternal(binop, inputs1, inputs2);
    }
  }

  private List<NumberT> executeGatesBinaryInternal(
      final ZkOperation.BinaryOp binop, final List<NumberT> inputs1, final List<NumberT> inputs2) {
    final ArrayList<NumberT> outputs = new ArrayList<>();
    for (int idx = 0; idx < inputs1.size(); idx++) {
      outputs.add(executeGateBinary(binop, inputs1.get(idx), inputs2.get(idx)));
    }
    return outputs;
  }

  private NumberT executeGateBinary(
      final ZkOperation.BinaryOp binop, final NumberT input1, final NumberT input2) {

    // Bitwise or
    if (binop == ZkOperation.BinaryOp.BITWISE_OR) {
      return bitwise(input1, input2, protocol().bits()::or);

      // Arithmetic multiplication signed
    } else if (binop == ZkOperation.BinaryOp.ARITH_MULTIPLY_SIGNED) {
      return protocol().numeric().mult(input1, input2);

      // Arithmetic subtraction
    } else if (binop == ZkOperation.BinaryOp.ARITH_SUBTRACT_WRAPPING) {
      return protocol()
          .numeric()
          .parallelAdd(List.of(input1), List.of(negateNumber(input2)))
          .get(0);

      // Bit: Concat
    } else if (binop == ZkOperation.BinaryOp.BIT_CONCAT) {
      return concat(input1, input2);

      // Comparison: Less Than
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED) {
      return fromBit(protocol().numeric().gtSigned(input2, input1));

      // Comparison: Less than or equals
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_SIGNED) {
      return fromBit(protocol().numeric().leSigned(input1, input2));

      // Comparison: Less Than
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_UNSIGNED) {
      return fromBit(protocol().numeric().gtUnsigned(input2, input1));

      // Comparison: Less than or equals
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_UNSIGNED) {
      return fromBit(protocol().numeric().leUnsigned(input1, input2));

      // Unknown
    } else {
      throw new UnsupportedOperationException("Unknown binop: " + binop);
    }
  }

  @Override
  public NumberT executeGateExtract(
      final ZkOperation.Extract gate, final NumberT inputToExtractFrom) {
    final ArrayList<BitT> bits = new ArrayList<>();
    final int endOffset = gate.bitOffset() + gate.bitwidth();
    for (int bitIdx = gate.bitOffset(); bitIdx < endOffset; bitIdx++) {
      bits.add(protocol().shareFactory().bit(inputToExtractFrom, bitIdx));
    }

    return protocol().shareFactory().number(bits);
  }

  @Override
  public NumberT executeGateSelect(
      final ZkOperation.Select gate,
      final NumberT inputCond,
      final NumberT inputThen,
      final NumberT inputElse) {
    return protocol().numeric().selectNumber(bool(inputCond), inputThen, inputElse);
  }

  private BitT bool(final NumberT numberToExtractBitFrom) {
    return protocol().shareFactory().bit(numberToExtractBitFrom, 0);
  }

  private NumberT fromBit(final BitT bit) {
    return protocol().shareFactory().number(List.of(bit));
  }

  /**
   * Performs a bitwise unary operation on a single input number, performing the same bit operation
   * on each bit of the input.
   *
   * @param input1 Number input.
   * @param operation Bit operation to perform.
   * @return Result of applying operation to each bit individually.
   */
  private NumberT bitwise(final NumberT input1, final Function<BitT, BitT> operation) {
    final ArrayList<BitT> bits = new ArrayList<>();
    for (int bitIdx = 0; bitIdx < input1.bitLength(); bitIdx++) {
      final BitT input1Bit = protocol().shareFactory().bit(input1, bitIdx);
      final BitT bit = operation.apply(input1Bit);
      bits.add(bit);
    }
    return protocol().shareFactory().number(bits);
  }

  /**
   * Performs a bitwise binary operation on a two numbers, performing the same bit operation
   * pairwise on each bit of the inputs.
   *
   * @param input1 Left-side number input.
   * @param input2 Right-side number input.
   * @param operation Bitwise operation to perform.
   * @return Result of applying operation to each bit pairwise.
   */
  private NumberT bitwise(
      final NumberT input1, final NumberT input2, final BiFunction<BitT, BitT, BitT> operation) {
    final int len = Integer.max(input1.bitLength(), input1.bitLength());

    final ArrayList<BitT> bits = new ArrayList<>();
    for (int bitIdx = 0; bitIdx < len; bitIdx++) {
      final BitT input1Bit = protocol().shareFactory().bit(input1, bitIdx);
      final BitT input2Bit = protocol().shareFactory().bit(input2, bitIdx);
      final BitT bit = operation.apply(input1Bit, input2Bit);
      bits.add(bit);
    }

    return protocol().shareFactory().number(bits);
  }

  /**
   * Functional type for functions that can perform a single binary operation over a large amount of
   * individual bits at the same time.
   *
   * <p>Examples: {@link BitManipulation#multiAnd} and {@link BitManipulation#multiXor}.
   */
  @FunctionalInterface
  interface ParallelBinaryBitOperation<BitT> {

    /**
     * Applies function to two lists of bits at once.
     *
     * @param leftArguments Left-side arguments.
     * @param rightArguments Right-side arguments. Must have same length as {@code leftArguments}.
     * @return A list of the underlying operation applied to each pair of bits.
     */
    List<BitT> apply(Iterable<BitT> leftArguments, Iterable<BitT> rightArguments);
  }

  /**
   * Performs a bitwise operation in parallel over two lists of numbers.
   *
   * <p>Can be thought of as a SIMD (Single-Instruction Multiple-Data) generalization of {@link
   * ZkCircuitSharedNumberExecution#bitwise}, which can perform one operation in parallel over a
   * large number of numbers.
   *
   * <p>This can be performed very efficiently, by concatting all the input numbers into a single
   * number, and then applying the parallel bitwise operation.
   *
   * @param inputs1 Left-side arguments.
   * @param inputs2 Right side arguments.
   * @param operation Operation to perform pairwise on arguments.
   * @return A list of the underlying operation applied to each pair of numbers.
   */
  private List<NumberT> parallelBitwise(
      final List<NumberT> inputs1,
      final List<NumberT> inputs2,
      final ParallelBinaryBitOperation<BitT> operation) {

    final List<BitT> inputAmalgam1 =
        inputs1.stream().map(this::toBits).flatMap(List::stream).toList();
    final List<BitT> inputAmalgam2 =
        inputs2.stream().map(this::toBits).flatMap(List::stream).toList();

    final int[] sizes = inputs1.stream().mapToInt(SharedNumber::bitLength).toArray();

    final List<BitT> outputAmalgam = operation.apply(inputAmalgam1, inputAmalgam2);

    final List<List<BitT>> outputs = BatchUtility.split(outputAmalgam, sizes);

    return outputs.stream().map(protocol().shareFactory()::number).toList();
  }

  /**
   * Joins two numbers to a singular even larger number by placing the number's bits after each
   * other.
   *
   * @param inputUpper The number which's bits will constitute the "upper" part e.g. the most
   *     significant bits of the resulting number.
   * @param inputLower The number which's bits will constitute the "lower" part e.g. the least
   *     significant bits of the resulting number.
   * @return The result number
   */
  private NumberT concat(final NumberT inputUpper, final NumberT inputLower) {
    final ArrayList<BitT> bits = new ArrayList<>();
    for (int bitIdx = 0; bitIdx < inputLower.bitLength(); bitIdx++) {
      bits.add(protocol().shareFactory().bit(inputLower, bitIdx));
    }
    for (int bitIdx = 0; bitIdx < inputUpper.bitLength(); bitIdx++) {
      bits.add(protocol().shareFactory().bit(inputUpper, bitIdx));
    }

    return protocol().shareFactory().number(bits);
  }

  private static void assertValueOfCorrectBitlength(
      final ZkType type, final SharedNumber<?> value, int variableId) {
    final int expectedBitLength = ((ZkType.Sbi) type).bitwidth();
    if (expectedBitLength != value.bitLength()) {
      throw new RuntimeException(
          "Loaded secret variable %d had incorrect bit length %d; expected %d bits"
              .formatted(variableId, value.bitLength(), expectedBitLength));
    }
  }

  /**
   * Converts a number to a list of bits.
   *
   * @param num Number to convert.
   * @return The produced list.
   */
  private List<BitT> toBits(NumberT num) {
    return IntStream.range(0, num.bitLength())
        .mapToObj(idx -> protocol().shareFactory().bit(num, idx))
        .toList();
  }
}
